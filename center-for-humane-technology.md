# Center for Humane Technology

> the Center for Humane Technology ultimately functions not as a solution to our technologically exacerbated problems, but simply as a way of making those problems slightly more palatable.
> 
> &#x2013; [Be Wary of Silicon Valley’s Guilty Conscience: on The Center for Humane Technology](https://librarianshipwreck.wordpress.com/2018/02/13/be-wary-of-silicon-valleys-guilty-conscience-on-the-center-for-humane-technology/) 

