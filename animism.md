# animism

> Harvey opined that animism's views on personhood represented a radical challenge to the dominant perspectives of modernity, because it accords "intelligence, rationality, consciousness, volition, agency, intentionality, language, and desire" to non-humans.  Similarly, it challenges the view of human uniqueness that is prevalent in both Abrahamic religions and Western rationalism.
> 
> &#x2013; [Animism - Wikipedia](https://en.wikipedia.org/wiki/Animism#Socio-political_impact) 

[[Miyazaki Hayao]]

> Animism is the most important tenet of Miyazaki’s signature films, including [[Spirited Away]], [[Nausicaä of the Valley of the Wind]](1984), [[My Neighbour Totoro]](1988), [[Princess Mononoke]](1997), and [[Ponyo]] (2008).
> 
> -   [Miyazaki Hayao’s Animism and the Anthropocene](https://journals.sagepub.com/doi/full/10.1177/02632764211030550)


## Resources

-   [Miyazaki Hayao’s Animism and the Anthropocene](https://journals.sagepub.com/doi/full/10.1177/02632764211030550)

