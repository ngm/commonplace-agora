# Police licence

> Police officers would need to hold and renew a “licence to practice” every five years and be subject to strict conditions under proposals to boost confidence in policing
> 
> [Police officers should work under ‘licence’ to restore trust – report for Eng&#x2026;](https://www.theguardian.com/uk-news/2022/mar/07/police-officers-should-work-under-licence-to-restore-trust-report-for-england-and-wales)

