# Ulverston Repair Cafe, February 2024

First [[Ulverston Repair Cafe]] in a few months for me.

I helped a chap making the connection between his MacBook Air and his BBC Micro:Bit.

And I looked at an old slow Dell Vostro laptop for someone - but didn't get much time on this, beyond explaining to them how to back up some old files.

I wonder what's the best app for benchmarking the performance of a Windows machine - to then report whether your changes have made a difference.

