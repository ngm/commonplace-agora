# programmable notes

> Agent-based note-taking systems that can prompt and facilitate custom workflows
> 
> &#x2013; [Programmable Notes](https://maggieappleton.com/programmatic-notes)

