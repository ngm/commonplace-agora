# NO ONE WAY WORKS: Political Organisation for the 21st Century

URL
: https://novaramedia.com/2021/06/25/no-one-way-works-political-organisation-for-the-21st-century/

Publisher
: [[Novara Media]]

Featuring
: [[Rodrigo Nunes]] / James Butler

> The cycle of struggle that erupted in [[2011]] – from the [[Arab Spring]] to [[the movement of the squares]] to the [[anti-austerity demonstrations in Britain]] – was characterised by stress on [[horizontalism]] and decentralised leadership. But as that movement grew and changed, it found itself running into the limits of that form, and entered into political parties – or founded new ones. But it found both resistance and limits there, too – and now, especially in many countries in Europe, it finds itself at a crossroads. All of these questions are questions of [[political organisation]], fraught with urgency because of the [[climate crisis]], and – after the 20th century – without any easy answers or faith in the inevitability of history. What should come next?

(NO ONE WAY WORKS is a reference to Revolutionary Letter #8 from [[Revolutionary Letters]])

