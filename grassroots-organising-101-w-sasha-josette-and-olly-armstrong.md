# Grassroots Organising 101 w/ Sasha Josette and Olly Armstrong

A
: [[podcast]]

URL
: https://tribunemag.co.uk/2022/09/93-grassroots-organising-101-w-sasha-josette-and-olly-armstrong

> This week, Grace talks to Natasha Josette and Olly Armstrong about their community organising project, [[Breathe]]. They discuss challenges and opportunities associated with [[community organising]], how it can be linked up with other elements of political strategy like the labour movement and electoral politics, and how you can begin this sort of grassroots work in your own area.

[not all the timestamps seem to be correct&#x2026; they don't always share correctly from [[AntennaPod]]]

-   ~00:03:19  Breathe heavily inspired by Black Panthers.
-   ~00:03:19  "Community wealth building in action".
-   ~00:03:19  Grace mentions [[Blaenau Ffestiniog]] as place where the town is having some agency.
-   ~00:10:58  It is insulting and patronising to assume working class just don't care about or understand [[climate change]].  Working class well understand climate change, but already under cosh from so many angles it is hard to give it a look in.
-   ~00:30:29  Just transitions have failed many times on the past - so we really have to make it actually mean something this time.

