# Praxis for the hacker class

[[Hacker class]].

Really liked reading Wark say (in [[Capital is Dead]]) how that to understand the current informatics-based forces of production, comrades with technical knowledge are needed to collaborate with those with the theoretical knowledge.  I feel like I sit in the space between theoretical and technical where I could have something to contribute here.

See also [[principles for ethical technology]].  See [[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]].

> This has not stopped some interesting and promising signs of hacker self-organization in technical and creative industries, from the [[unionization of creatives at Vice Media]] to the Google walkout to refusal to work on border control or military projects across the tech industry.
> 
> &#x2013; [[Capital is Dead]]

Where could useful praxis lie?


## Liberation

-   Accessibility
-   Access to [[Free software]] and [[free culture]]


## Cooperation

-   [[Sharing information systems is praxis for the hacker class]]
-   Interoperability


## Agency

-   [[IndieWeb]]
-   [[Information Civics]]


## Sustainability

-   [[Repair]] and [[maintenance]]
-   [[Sustainable tech]]
-   [[Low-tech]]
-   [[Green computing]]

