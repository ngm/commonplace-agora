# Technics

'technics' includes both technical and cultural aspects of technology.

> For [[Mumford]], technology is one part of technics. Using the broader definition of the Greek tekhne, which means not only technology but also art, skill, and dexterity, technics refers to the interplay of social milieu and technological innovation—the "wishes, habits, ideas, goals" as well as "industrial processes" of a society.  
> 
> &#x2013; [Lewis Mumford - Wikipedia](https://en.wikipedia.org/wiki/Lewis_Mumford)

[[Authoritarian and Democratic Technics]]

