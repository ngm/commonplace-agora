# Felt

-   https://felt.social

Worker-owned coop building "free and open source tools for building customizable self-governing communities".  Do wonder it could be better/more quickly achieved by using/contributing to existing software.

> It's a customizable open source software platform, a community and business, a co-op of worker-owners, and hopefully one day a platform co-op (communities owning themselves? absurd) — all with a purpose that puts people first. If we do it right, Felt is tech that feels good.

