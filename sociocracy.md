# Sociocracy

URL
: https://sociocracy30.org


"free social technology for growing agile and resilient organizations"

"a patterns-based approach to organisationl change"

> Small teams using Sociocracy can be nested within a larger “parent circle” that has broad oversight and decision making responsibilities.
> 
> &#x2013; [[Free, Fair and Alive]]

