# The overarching solution to our energy needs is to electrify everything we can

The overarching solution to our energy needs is to [[electrify everything]] we can:

> from heating buildings to transport, and power everything using clean renewables and storage. We are getting a huge helping hand from great leaps forward in clean technology.
> 
> [Scientists have just told us how to solve the climate crisis – will the world&#x2026;](https://www.theguardian.com/commentisfree/2022/apr/06/scientists-climate-crisis-ipcc-report)

-   Between 2010 and 2019:
    -   the cost of solar energy plummeted by 85%
    -   wind energy decreased by 55%
    -   lithium-ion batteries decreased by 85%

