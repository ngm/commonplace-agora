# intensive revolution

[[Temporary Autonomous Zone]]s being an example of intensive revolution.  i.e. spatially and temporally local.  Very radical but also short-lived and bypassing the difficulty of building something bigger.

[[Extensive revolution]] being a worldwide systemic shift.

[[Rodrigo Nunes]] mentions this in [[Organising for Revolution With Rodrigo Nunes]], I can't find much mention of it in his book [[Neither Vertical Nor Horizontal]] though.

