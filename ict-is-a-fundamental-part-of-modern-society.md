# ICT is a fundamental part of modern society

[[Information and communication technology]] is a fundamental part of modern society.

> networked digital information technology has become the dominant mode through which we experience the everyday. **In some important sense this class of technology now mediates just about everything we do.**
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Networked digital information technology looms ever larger in all of our lives. It shapes our perceptions, conditions the choices available to us, and remakes our experience of space and time.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Computing, robotics, digital technologies, networking and electronic communications have transformed our social world.
> 
> &#x2013; [[Twenty-First Century Socialism]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

Intuitively yes, although could do with a bit more to back it up.

