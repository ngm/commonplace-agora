# The Death of Nature

Carolyn Merchant.

> As soon as I came across Carolyn Merchant’s book The Death of Nature (first published in 1980), I immediately saw that the [[ecofeminist]] perspective was a kind of parallel line of argument to the critique of Eurocentrism. She’d done something similar to the Africanist scholars like [[W. E. B. Du Bois]] and [[Cheikh Anta Diop]], in uncovering a whole strand of history which the mainstream suppressed, and which forces us to rethink our relations to the world.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

