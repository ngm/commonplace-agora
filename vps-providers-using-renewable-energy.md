# VPS providers using renewable energy

Very handy list of [[VPS providers]] using [[renewable energy]] here https://office.uniteddiversity.coop/s/oZjArwQDtPEgGFd from @jdaviescoates.

As of 2022 I personally use both [[Hetzner]] and [[GreenHost]].  Hetzner is big, cheap and slick, if you're used to Digital Ocean it's very similar.  GreenHost is small, super-ethical, less slick.

On the list as of Dec 2020 are:

-   https://www.netcup.eu/vserver/vps.php
-   https://www.hetzner.com/cloud
-   https://www.vpsserver.com/london/
-   https://1984hosting.com/product/pricelist/
-   https://greenhost.net/products/vps/
-   https://krystal.uk/cloud-vps
-   https://cloudabove.com/servers
-   https://www.gandi.net/en-GB/cloud
-   https://www.greennet.org.uk/internet-services/hosting/starter-virtual-dedicated-server
-   https://www.webarchitects.coop/virtual-servers

I found 1984 (https://1984.is) through it, who look really good.


## Comparison notes


### 1984

-   good ethics
-   I like their interface, nice and simple.
-   They don't seem to do automated backups?  You have to do them manually?


### hetzner

They feel a bit more pro, a bit bigger scale.  Not quite so explicit about having good ethics.

Their cloud console is much more slick, and feels pretty familiar if you're used to Digital Ocean's.

Automatic backup snapshots provided.  They do them daily rather than DO's weekly, which is a HUGE win.

Very quick to spin up a new VPS.

