# microblurting

As microblogging is to blogging, so microblurting is to blurting.

Like microblogging, but for blurting.

i.e. [[blurting]], but with less characters.

Blurt.


## What?

Using your microstream for [[active recall]].


## Why?

Active recall is a useful learning tool. Your stream is a natural place for blurting.


## Why not?

Is blurting in public such a good idea?

