# Eroding capitalism

This is the [[solidarity economy]] approach I feel.

> One way to challenge capitalism is to build more democratic, egalitarian, participatory economic relations in the spaces and cracks within this complex system wherever possible, and to struggle to expand and defend those spaces.  The idea of eroding capitalism imagines that these alternatives have the potential, in the long run, of expanding to the point where capitalism is displaced from this dominant role.
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> The strategic vision of eroding capitalism imagines introducing the most vigorous varieties of emancipatory species of noncapitalist economic activity into the ecosystem of capitalism, nurturing their development by protecting their niches, and figuring out ways of expanding their habitats
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> The only hope for an emancipatory alternative to capitalism — an alternative that embodies ideals of equality, democracy, and solidarity — is to build it on the ground and work to expand its scope.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


## Pros

> It is enticing because it suggests that even when the state seems quite uncongenial for advances in social justice and emancipatory social change, there is still much that can be done. We can get on with the business of building a new world, not from the ashes of the old, but within the interstices of the old.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


## Cons

> It is far-fetched because it seems wildly implausible that the accumulation of emancipatory economic spaces within an economy dominated by capitalism could ever really displace capitalism, given the immense power and wealth of large capitalist corporations and the dependency of most people’s livelihoods on the well-functioning of the capitalist market. Surely if noncapitalist emancipatory forms of economic activities and relations ever grew to the point of threatening the dominance of capitalism, they would simply be crushed.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

