# What is to be done? Who is going to do it and where?

A
: [[podcast]]

URL
: https://anticapitalistchronicles.libsyn.com/what-is-to-be-done-who-is-going-to-do-it-and-where

Featuring
: [[David Harvey]]


One of the biggest problems with capitalism is the commoditization and marketisation of everything.

Thus one important and immediate thing to be done is the decommodification of things.

