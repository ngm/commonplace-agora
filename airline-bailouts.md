# Airline bailouts

> UK airlines are asking for a government bailout. The terms should be as follows: (1) money goes first to workers to cover lost wages; (2) purchase happens at discounted rate; (3) government gets a commanding share, so we can manage the industry in line with climate objectives
>   SCHEDULED: <span class="timestamp-wrapper"><span class="timestamp">&lt;2020-03-17 Tue&gt;</span></span>
> 
> https://twitter.com/jasonhickel/status/1239506700190744576

