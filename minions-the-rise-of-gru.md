# Minions: The Rise of Gru

Fun.  Really well-done animation and actiony bits.  I enjoyed the 70s disco vibe and soundtrack.

I like the fact that there was a kind of teamwork/community vibe to it&#x2026; Gru tried to make it on his own, ditching his Minions, but it turns out he needed them all along&#x2026;

