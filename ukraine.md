# Ukraine

> "What does Ukraine look like?" In the allotted five minutes I tried to give an idea: it’s not flat and covered in dark pine forests like much of Russia; it’s green and gently rolling, and dotted with medieval fortresses, romantically neglected baroque palaces and monasteries, and quiet, pretty towns and little cities, much like those of Austria or the Czech Republic.
> 
> &#x2013; [Ukrainian heritage is under threat – and so is the truth about Soviet-era Rus&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/15/ukrainian-heritage-under-threat-truth-soviet-era-russia)

<!--quoteend-->

> Kyiv itself is a grand Belle Époque metropolis with up-and-down cobbled streets and chestnut trees. There are funny little back alleys and courtyards full of coffee shops and art galleries, leafy parks with views over the sprawling river Dnipro, and an array of glorious churches, the grandest of them the 11th-century Saint Sophia Cathedral.
> 
> &#x2013; [Ukrainian heritage is under threat – and so is the truth about Soviet-era Rus&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/15/ukrainian-heritage-under-threat-truth-soviet-era-russia)

