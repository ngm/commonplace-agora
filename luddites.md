# Luddites

>   If [the Luddites] had their way we wouldn’t be living in a world with ‘no technology’, we’d be living in a world where communities have a say in the technological decisions that will impact them.
> &#x2013;  [Why the Luddites Matter | LibrarianShipwreck](https://librarianshipwreck.wordpress.com/2018/01/18/why-the-luddites-matter/)
>   (via Harold Jarche https://twitter.com/hjarche/status/1455264342174273550)

<!--quoteend-->

> Discontented weavers, croppers, and other textile workers had begun a protracted insurgency against property and the state. At issue were new types of machines —the stocking frame, the gig mill, and the shearing frame— that could produce and finish cloth using a fraction of the labor time previously required, transforming a skilled profession into low-grade piecework.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> In addition to their notorious raids, the so-called Luddites launched vociferous public protests, sparked chaotic riots, and continually stole from mills—activities all marked by an astonishing level of organized militancy.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> the height of their activity in Nottingham, from November 1811 to February 1812, disciplined bands of masked Luddites attacked and destroyed frames almost every night
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Byron captured in “Song for the Luddites,” his 1816 encomium to the movement, which portrayed it as heroically doomed, but successful at laying the groundwork for future emancipatory struggles
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> To say they fought machines makes about as much sense as saying a boxer fights against fists.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> the Luddite rebellions were never simply against technology, but “what that machinery stood for: the palpable, daily evidence of their having to succumb to forces beyond their control.”
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Marx himself makes some dismissive remarks about the “crude form” of the revolts of the Luddites. “It took both time and experience before the workers learnt to distinguish between machinery and its employment by capital, and therefore to transfer their attacks from the material instruments of production to the form of society which utilizes those instruments.”
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> What workers bitterly opposed was “industrial concentration” that demolished their way of life by undermining the autonomy they possessed in small-scale home-based manufacturing, which “paced its activities according to its needs” so that workers controlled the hours and intensity of their work.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> “Mechanization took place not merely because of technical or economic necessities but because of conflicts of authority.”
> 
> &#x2013; [[Breaking Things at Work]]

