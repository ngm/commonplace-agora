# Kaizen

-   Practice your personal Kaizen

https://lifehacker.com/practice-your-personal-kaizen-207029


## GTD


### Resources

-   Weekly Review: https://lifehacker.com/5908816/the-weekly-review-how-one-hour-can-save-you-a-weeks-worth-of-hassle-and-headache


## GTD and org, my process


### working on a task

-   REFILE it to the appropriate location (why this first?)
-   NARROW the current task so I'm feeling focused on it
-   ask myself, WHY is this task/project even here?  What goal does it fit into?
-   DO IT if it's doable
-   if it's a PROJECT
    -   TAG it if it makes sense
    -   give it a PRIORITY
    -   Give it a sensible DEADLINE (and probably remove the SCHEDULED date for the overall project
    -   BREAK it down into subtasks
        -   do this with some freeform journalling
        -   if nothing else, make sure there is a NEXT ACTION
        -   ESTIMATE how long each task will take


## Misc


### org-mode reporting on tasks: https://stackoverflow.com/questions/22394394/orgmode-a-report-of-tasks-that-are-done-within-the-week

