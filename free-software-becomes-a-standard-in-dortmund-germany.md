# Free software becomes a standard in Dortmund, Germany

> The [[Dortmund]] Council has declared digitalisation to be a political leadership task in its Memorandum 2020 to 2025.

<!--quoteend-->

> “Use of open source software where possible.”
> “Software developed by the administration or commissioned for development is made available to the general public.”

[[Public Money, Public Code]]

> With this resolution, city policy takes on the shaping of [[municipal digital sovereignty]] and [[digital participation]].

