# Political climate education with Momentum

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/political-climate-education-with-momentum

> This week we are joined by [[Momentum]] co-chair Gaya Sriskanthan (@gayasktn). We discuss [[the importance of joining class struggle with climate action]], what British environmentalists can learn from communities on the front line of climate breakdown and Momentum's new climate justice education program.

~00:12:08  Climate change is fundamentally a working class issue.

~00:14:14  The working class bear the brunt of climate change issues.

