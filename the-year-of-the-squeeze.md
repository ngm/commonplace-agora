# the year of the squeeze

[[Britain]] [[2022]]. [[Cost of living crisis]].

-   [[Britain has soaring energy bills]]

> Households already grappling with rising energy bills, petrol prices, tax, mortgage payments and rent can also expect to pay more for groceries this year
> 
> &#x2013; [Cost of living crisis: how are UK supermarkets limiting price rises? | Superm&#x2026;](https://www.theguardian.com/business/2022/feb/08/cost-of-living-crisis-supermarkets-prices-tesco) 

