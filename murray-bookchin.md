# Murray Bookchin

I find interesting the tack he took of defining something in between [[Anarchism]] and [[Marxism]].  That appeals to me, part of the whole [[Horizontalism vs verticalism]] thing.  Bookchin had chops in both of them so was well-placed for a synthesis I reckon.  And I like his municipalist take on things ([[Libertarian municipalism]]) and his ecological concerns ([[Social ecology]]).

I listened to an interview ages back on 
[[Revolutionary Left Radio]] with his daughter [[Debbie Bookchin]] ([[The Philosophy of Murray Bookchin: An Interview with Debbie Bookchin]]).  I remember he sounded like a fun Dad - taking her to the cinema, but waiting outside writing political tracts while she watched the films.

He seems a bit cantankerous.

> The only other author I can think of who similarly combines brilliant analysis with bad faith caricatures of his perceived adversaries is Murray Bookchin.
> 
> &#x2013; [Center for a Stateless Society » Review: Srnicek and Williams, Inventing the &#x2026;](https://c4ss.org/content/50849) 

His ideas inspired [[Abdullah Öcalan]].  [[Rojava]].

O.G. [[solarpunk]].

[[Communalism]].  

> Rejecting ecological arguments that blame individual choices, technology, or population growth, Bookchin argues that the ecological crisis is caused by an irrational social system governed by the cancerous logic of capitalism, driven by its competitive grow-or-die imperative and its endless production directed not toward meeting human needs but accumulating profit
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Bookchin’s proposal is by far the most sophisticated radical proposal to deal with the creation and collective use of the [[commons]] across a wide variety of scales
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> In the late 1950s, he began to elaborate the importance of environmental degradation as a symptom of deeply entrenched social problems. Bookchin’s book on the subject, [[Our Synthetic Environment]], appeared six months before Rachel Carson’s [[Silent Spring]], while his seminal 1964 pamphlet [[Ecology and Revolutionary Thought]] introduced the concept of [[ecology]] as a political category to the [[New Left]].
> 
> &#x2013; [[The Next Revolution]]

