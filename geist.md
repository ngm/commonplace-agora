# geist

> Geists are little bots that live in your Subconscious. They do useful things&#x2026; finding connections between notes, issuing oracular provocations and gnomic utterances.
> 
> &#x2013; [SCAMPER is a procedural idea generator - by Gordon Brander](https://subconscious.substack.com/p/scamper)

