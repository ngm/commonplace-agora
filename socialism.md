# Socialism

Socialism is a set of political and economic ideas for how to run society in a fair way.

Socialism's primary goals are an equitable distribution of wealth and resources, and for social rather than private ownership of the means of production.

There are different variations of socialism in practice, for example democratic vs revolutionary, utopian vs scientific. But common elements are usually some form of economic planning, worker self-management and provision of social welfare.

Key points:

-   [[social ownership]] of the [[means of production]], as opposed to private ownership
-   fair distribution of the fruits of labour

I am a socialist.

Plenty of different flavours as to how you achieve a socialist society in practice e.g. [[democratic socialism]], [[revolutionary socialism]] ([[Anarchism]], [[Communism]]).

[[Communism, anarchism, socialism]]

> Marx and Engels saw socialism as the movement for a society that is based on the principles of equality, justice, and solidarity. 
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

Another definion:

> The socialist goal is to bring about a society free of class divisions.
> 
> &#x2013; [[Red-Green Revolution]]


## Property and production

> In a socialist system, property is held in common. The means of production are directly controlled by the workers themselves through worker coops, and production is for use and need rather than exchange, profit and accumulation.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> A key characteristic of a socialist society is that the means of production are the common property of the producers
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]


## Socialism and communism

> Marx (1875) spoke of a first and second phase of communism. In the first phase, private property and capital cease to exist and the ownership of the means of production is socialised, but wage-labour, money, the state and exchange continue to exist. In the second phase, wages, wage-labour, money, the state, exchange-value and all forms of alienation cease to exist. But Marx did not call the first phase socialism and the second phase communism. He rather spoke of two stages or phases of communism.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> They distinguish different types of socialism, of which communism is one, while reactionary socialism, bourgeois socialism, and critical-utopian socialism are others.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> In the Manifesto of the Communist Party, Marx and Engels (1848) speak of communism as a type of socialist movement. Besides communism they identify reactionary socialism, bourgeois socialism, and critical-utopian socialism as types of socialism. When one speaks of communism, one therefore means a type of socialism that aims at the abolition of class society and a democratic, worker-controlled economy within a participatory democracy. 
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> socialism, Marxists often use the terms socialism and communism interchangeably. Strictly speaking, socialism is broader than communism.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> In the 19th century, the socialist movement experienced a split between reformist revisionists and revolutionary socialists. On the one side, the revisionists believed in the evolutionary transition to socialism through victories in elections and an automatic breakdown of capitalism. On the other side, revolutionary socialists stressed the importance of class struggle, street action, mass political strikes, and fundamental transformations of society in order to establish a free society. In the Second International (1889-1916), the various factions of socialism were part of one organisation. The Second International collapsed during the First World War. Socialists were split between those who supported the War and those who radically opposed it.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> Marx saw communism as a radical, democratic movement and form of socialism.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> For Marx and Engels, socialism is not an abstract idea, but has as its precondition the development of productive forces and the socialisation of work, is grounded in class antagonisms that it wants to overcome, and can only become real through class struggles that aim at abolishing class society and establishing a classless society.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> Socialism’s conditions are class society’s antagonisms, its method is communist class struggle, and its goal is a classless, socialist society.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

Common ownership.


## Socialism and ecology

-   [[Socialism and Ecology]]
-   [[Eco-socialism]]
-   [[Social ecology]]


## Misc

> If capitalism is a society characterized by unconscious control, then socialism must be the restoration of human consciousness as a historical force. In practice, this means that the market must be replaced by planning.
> 
> &#x2013; [[Half-Earth Socialism]]

