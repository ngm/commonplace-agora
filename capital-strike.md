# Capital strike

Capital threatening a state by for example witholding its business.  Used to pressure the state into doing what the business wants, i.e. something that's probably bad for the people but good for profits.

> The idea of a “capital strike” has been around forever. It’s the obvious complement to the labor strike: just as workers can shut down production by refusing to come to work, capital can shut down the economy by refusing to invest and hire workers
> 
> &#x2013; [The Right’s Favorite Strike](https://www.jacobinmag.com/2011/09/the-rights-favorite-strike/) 

<!--quoteend-->

> The capital strike threat prevents policymakers from seriously considering many popular and sensible reforms.
> 
> &#x2013; [When Capitalists Go on Strike](https://www.jacobinmag.com/2017/02/capital-strike-regulations-lending-productivity-economy-banks-bailout) 

