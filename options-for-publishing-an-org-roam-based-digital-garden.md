# options for publishing an org-roam-based digital garden

My org-publish build ([[How I publish my wiki with org-publish]]) is so horrendously slow that I always have an eye out for alternatives.  I like the idea of using something native to Emacs, but it's just ridiculously slow to build my static site.  It's 6000 odd org files but it still shouldn't be **so** slow.

Alternatively I'd dig in to how org-publish works and see if I can speed it up.  I've tried that a little before though and didn't get very far ([[Speeding up org-export and org-publish]]).


## Investigations


### firn

[[using firn for publishing my org-roam digital garden]].

Conclusion: doesn't like id: links.  Not gonna happen.


### hugo

[[using ox-hugo for publishing my org-roam digital garden]].

Conclusion: Seems to prefer one post per org subtree rather than per file.  Not going any further with it for now.  But others are using it successfully, so check in to it again in future.


### weblorg

Looks promising.  Seen in the wild here:

-   https://gitlab.com/thecashewtrader/thecashewtrader.gitlab.io/-/blob/main/publish.el
-   https://thecashewtrader.gitlab.io/blog/in-appreciation-of-weblorg


### emanote

https://emanote.srid.ca/demo/orgmode


### org-roam-ui

Some discussion around letting you publish it for the web here:
https://github.com/org-roam/org-roam-ui/discussions/109

Seems to have been achieved: https://ikoamu.github.io/publish-org-roam-ui/


## Discussion

-   [Package dedicated to workflows publishing digital garden on web - Development&#x2026;](https://org-roam.discourse.group/t/package-dedicated-to-workflows-publishing-digital-garden-on-web/536/)

