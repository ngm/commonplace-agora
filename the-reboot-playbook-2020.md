# The Reboot Playbook 2020

URL
: https://rebootproject.uk/download-the-free-guide/


## Getting Started

> During this stage, you’ll think about who you can help and what kind of devices they need. You’ll also learn more about accessories and connectivity.


### Identify potential recipients

Possible recipients

-   school children
-   unemployed
-   elderly
-   lonely
-   refugees

Ask in the community, e.g. Citizens Advice, Localgiving, local social media.  Schools, libraries, online learning centres.

Understand recipient needs.  This is made easier if you work with local community organisations.  You might choose to own the relationship with recipients, or work with community partners.

Community partners: schools; hospitals; jobcentres; libraries.


### Identify suitable devices

> Next, you need to think about the types of devices your recipients may need. We’ll also explain how you can make the restoration process fit your capabilities by restricting what people can donate

Laptops, smartphones, and tablets.
Basically minimum requirements of age / functionality.

Option 1: settings strict guidelines.  Be clear about what you can take on.
Option 2: relaxed guidelines.  You'll get more donations, but may end up needing to get rid of the stuff that really isn't up to snuff.


### Sourcing accessories

i.e. chargers, usb connectors, mice, stylus pens, dongles

Three options:

-   ask donors to donate accessories with their devices
-   procure new accessories at a cost
-   take a mixed approach

Seems simplest to require that chargers are required.  Maybe take it on a case by case basis.


### Choose the right connectivity

> Many vulnerable recipients won’t have any form of internet access available to them other than free public networks, such as those at libraries. This means that recipients with mobility issues can find it really difficult to get online.

Can try to partner with another org who can offer mobile data, broadband of dongles.  Or simply find out where free public networks are in the area - that's probably a good one to begin with.


## Receiving devices

> During this stage, you’ll find out more about launching your project. You’ll explore what information you need to give to potential donors and how you can maximise donations.


## Readying devices

> During this stage, you’ll discover more about diagnostics, repairs and data wiping.


## Going live

> In this stage, you’ll decide how to deliver devices and give their new recipients ongoing support. We’ll also show you how to dispose of unsuitable devices.

