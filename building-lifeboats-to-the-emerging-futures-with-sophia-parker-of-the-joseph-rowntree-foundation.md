# Building Lifeboats to the Emerging Futures with Sophia Parker of the Joseph Rowntree Foundation

A
: [[podcast]]

Found at
: https://accidentalgods.life/building-lifeboats-to-the-emerging-futures-with-sophia-parker-of-the-joseph-rowntree-foundation/

Featuring
: Sophia Parker

Of the Emerging Futures project at the [[Joseph Rowntree Foundation]].

Really interesting discussion.  Lots of quotable moments (but I was listening while doing yard work so didn't jot them down).

[[Philanthropy]].

[[Theory of change]].  [[Margaret Wheatley]] and the [[Two Loop Model]].

Some discussion on [[hope]] in a troubled world. Around 9m, some different types of hope: pre-tragic hope; tragic hope; and post-tragic (emergent?) hope. Recommend's Rebecca Solnit writings.  I  liked the definition of hope Sophia gave based on that, something like: Hope is not optimism.  Hope leads to action. "It's the space of existence where you don't know that the actions you take are going to make the difference, but you believe that they might."

Also a mention of the [[New Constellations]] project, which I had forgotten, did something in [[Barrow]] a few years back.

