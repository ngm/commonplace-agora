# Compost

-   https://compost.digital

> COMPOST is a magazine about the [[digital commons]].

<!--quoteend-->

> Our team of contributors and collaborators work in and for solidarity economics, co-operatives, community networks, copyleft, free and open source development, IndieWeb, and DWeb.
> 
> -   [COMPOST Issue 01: Foreword](https://one.compost.digital/foreword/)

