# Enclosure of the gardens

I was thinking - [[digital gardens]] are an amazing, unique feature that the big [[silos]] don't really have.

The [[stream]] stuff, to some degree we're playing catchup.  But other than a photo album here, a pin there, an 'on this day' here, there's very little mainstream investment in letting you turn your stream into a commonplace books, your knowledge base over time.

There's big platforms that do note-taking things, of course, like Evernote, etc, but the big companies in charge of the streams, I thought, don't seem to see the link between what goes in your stream being the seeds to grow a garden over time.  You'd think they'd be all over that.

But&#x2026; of course, the silos do have a digital garden for everyone.  It's just it is enclosed.  You don't have access to your own garden.  They are reaping the benefits of the seeds you are sowing, collecting, connecting your dots, and then selling your fruit. [[People farming]], as Aral Balkan would have it.

