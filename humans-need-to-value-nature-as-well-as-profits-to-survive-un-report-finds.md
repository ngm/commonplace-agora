# Humans need to value nature as well as profits to survive, UN report finds

URL
: https://www.theguardian.com/environment/2022/jul/11/humans-value-nature-survive-un-report-age-of-extinction

[[climate crisis]]

The report is by [[Ipbes]].  

Markets and growth are the problem.  A reconnection with nature is required.  States the super quantitative rational approach to science can be lacking, and other forms of knowing should be honoured.

All good.  But how do you effect that shift?

> Focus on market has led to climate crises, with spiritual, cultural and emotional benefits of nature ignored

<!--quoteend-->

> Taking into account all the benefits nature provides to humans and redefining what it means to have a “good quality of life” is key to living sustainably on Earth, a four-year assessment by 82 leading scientists has found.

<!--quoteend-->

> **A market-based focus on short-term profits and economic growth means the wider benefits of nature have been ignored**, which has led to bad decisions that have reduced people’s wellbeing and contributed to climate and nature crises, according to a UN report. To achieve sustainable development, qualitative approaches need to be incorporated into decision making.

<!--quoteend-->

> This means properly valuing the spiritual, cultural and emotional values that nature brings to humans

<!--quoteend-->

> “There has been a dominant way of taking decisions based on things that look more simple, super-quantitative, and more scientific, and we’re saying: ‘No, that’s not good science.’ There are a lot of social sciences and humanities, and other knowledge systems, that can also tell us how to do things.”

<!--quoteend-->

> The review highlights four general perspectives that should be taken into account; “ **living from nature** ” which refers to its ability to provide us with our needs like food and material goods; “ **living with nature** ”, which is the right of non-human life to thrive; “ \*living in nature\*”  which refers to people’s right to a sense of place and identity, and finally, “\*living as nature\*”, which treats the world as a spiritual part of being human.

