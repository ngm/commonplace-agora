# CERFI

Centre d'Études de Recherche et de Formation Institutionnelle
(in English, the Centre for Institutional Study, Research and Development)

A research cooperative. Created in 1967, active until 1987.

> collective research practices, analysis of the social unconscious, and militant action-research in institutional programming. 
> 
> &#x2013; [CERFI: Militant Analysis, Collective Equipment and Institutional Programming &#x2026;](https://www.rca.ac.uk/research-innovation/projects/cerfi-militant-analysis-collective-equipment-and-institutional-programming/)

<!--quoteend-->

> was a transdisciplinary research cooperative formed of psychiatrists, sociologists, video artists, educators, urbanists, architects and economists.
> 
> &#x2013; [CERFI: Militant Analysis, Collective Equipment and Institutional Programming &#x2026;](https://www.rca.ac.uk/research-innovation/projects/cerfi-militant-analysis-collective-equipment-and-institutional-programming/)

<!--quoteend-->

> Some of its core members and collaborators included [[Félix Guattari]], Anne Querrien, François Pain, Liane Mozère, Olivier Querouil, Michel Rostain, Gaëtane Lamarche-Vadel, François Fourquet, Florence Pétry, Lion Murard, Hervé Maury, [[Michel Foucault]] and [[Gilles Deleuze]], among many others.
> 
> &#x2013; [CERFI: Militant Analysis, Collective Equipment and Institutional Programming &#x2026;](https://www.rca.ac.uk/research-innovation/projects/cerfi-militant-analysis-collective-equipment-and-institutional-programming/)

<!--quoteend-->

> CERFI was concerned with the programming of local collectivities, organisations of cultural activity and social institutions, referred to as 'collective equipment'. 
> 
> &#x2013; [CERFI: Militant Analysis, Collective Equipment and Institutional Programming &#x2026;](https://www.rca.ac.uk/research-innovation/projects/cerfi-militant-analysis-collective-equipment-and-institutional-programming/)

<!--quoteend-->

> Under the heading of 'institutional programming,' its ambition was to convert institutional processes into mechanisms of empowerment.
> 
> &#x2013; [CERFI: Militant Analysis, Collective Equipment and Institutional Programming &#x2026;](https://www.rca.ac.uk/research-innovation/projects/cerfi-militant-analysis-collective-equipment-and-institutional-programming/)

