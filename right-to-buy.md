# Right to Buy

> bringing social housing that was sold off under Right-to-Buy back into the public sector.
> Thousands of tenants all over the country have found themselves paying extortionate rents to private landlords in homes that were once built and managed by the state — effectively **a state-backed handout to the rich**. Investing in social housing is the only way to solve our housing crisis
> 
> &#x2013; [[The Rent Is Too Damn High]]

<!--quoteend-->

> Between 1946 and 1979, 50 per cent of all houses completed in the UK were council houses. Between 1979 and 2020, that figure drops to 5 per cent
> 
> &#x2013; [[The Rent Is Too Damn High]]

<!--quoteend-->

> In England in 1949 close to 140,000 council houses were built. In 1999 this number was fifty
> 
> &#x2013; [[The Rent Is Too Damn High]]

