# Degrowth vs Eco-Modernism

A
: [[podcast]]

Part of
: [[Upstream]]

[[Degrowth]] vs [[Eco-modernism]].

Robert reads through an article by [[Kai Heron]] - [[Forget Eco-Modernism]], adding his own commentary along the way.

It's a really good episode. I like Kai Heron, and I've found the debate between various strands of [[ecosocialist]] thought very interesting. Robert's commentary is very beneficial, shining a light on the topics discussed.

But on the subject itself: I lament the time spent disagreeing amongst ourselves.  Is it ultimately useful?  What if all this intellectual effort could be spent on bringing about a transition away from capitalism, and towards ecosocialism?

Perhaps the debate is contributing to that transition, in part, in a roundabout way.  And I suppose that if we don't know what we stand for, we can't meaningfully work towards it.

But still.  It has vibes of [[The People's Front of Judea]] vs the Judean People's Front.

At the end, Kai starts to mention how the debate is ultimately fruitless, and we need to focus more on strategy. But that's after the majority of the article being about what's wrong with ecomodernism.

