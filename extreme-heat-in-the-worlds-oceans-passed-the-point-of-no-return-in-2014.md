# Extreme heat in the world's oceans passed the "point of no return" in 2014

-   [Extreme heat in oceans ‘passed point of no return’ in 2014 | Oceans | The Gua&#x2026;](https://www.theguardian.com/environment/2022/feb/01/extreme-heat-oceans-passed-point-of-no-return-high-temperatures-wildlife-seas?CMP=Share_AndroidApp_Other)

[[Global heating]].

This is bad because [[The ocean plays a critical role in maintaining a stable climate]].

