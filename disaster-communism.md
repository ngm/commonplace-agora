# Disaster communism

See e.g. [[A Paradise Built in Hell]]

> Solnit’s studies show that in the immediate aftermath of disasters people are more likely to set aside differences and self-interest than they are to descend into Mad Max scenarios. Community kitchens, gifting, solidarity funds, and the borrowing of essential items to survive and rebuild create a deeper sense of collectivity and sociality.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> But disaster communities are fleeting things. The capitalist state, oriented toward the protection of private property, the wage form, and raced and gendered hierarchy, invariably steps in to reimpose its order, attacking self-organization and solidarity
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

e.g. [[Walkaway]]. Calais jungle.

> “revolutionary process of developing our collective capacity to endure and flourish that emerges from these struggles. Disaster communism is a movement within, against, and beyond ongoing capitalist disaster.”
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> For the rest of us, it came down to this: we had to trust our society to work well enough to save us. As it had never worked very well before, this was a big leap of faith, for sure. No one was confident. But it was the system or death. So we congregated in the places announced over the radio and online and on the streets themselves, and waited our turn.
> 
> &#x2013; [[The Ministry for the Future]]

<!--quoteend-->

> Fuck [[Margaret Thatcher]], I said when I could catch my breath. And I say it again now: fuck Margaret Thatcher, and fuck every idiot who thinks that way. I can take them all to a place where they will eat those words or die of thirst. Because when the taps run dry, society becomes very real. A smelly mass of unwashed anxious citizens, no doubt about it. But a society for sure. It’s a life or death thing, society, and I think people mainly do recognize that, and the people who deny it are stupid fuckers, I say this unequivocally. Ignorant fools. That kind of stupidity should be put in jail.
> 
> &#x2013; [[The Ministry for the Future]]

