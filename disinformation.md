# Disinformation

> The city’s mayor, Peter Soulsby, has partially blamed social media disinformation about recent events for stoking further division – and drawing people from outside the local area.
> 
> &#x2013; [Wednesday briefing: What’s behind the violent clashes in Leicester | Leiceste&#x2026;](https://www.theguardian.com/world/2022/sep/21/wednesday-briefing-whats-behind-violent-clashes-in-leicester)

