# Facebook share drop

> [[Facebook]] shares fell 25% on Thursday – wiping over $200bn (£147bn) off its value – after the company reported its first ever drop in daily user numbers
> 
> [Facebook’s first ever drop in daily users prompts Meta shares to tumble | Fac&#x2026;](https://www.theguardian.com/technology/2022/feb/02/facebook-shares-slump-growth-fourth-quarter)

