# Aurora Rising

&#x2013; Alistair Reynolds

The precursor to [[Elysium Fire]].  I liked this one too.  Easy to read, keeps you turning the pages, not too taxing.  The kind of book that is perfect for me for reading to help me sleep.  I mean that in a positive way.  Something I look forward to reading and helps me unwind.

This one explored a bit more some of the idea of the Glitter Band and its varied habitats.  Some of them exploring extreme views or lifestyles.  I think that's a good idea in the books, but it isn't really explored that much.  Maybe that's fine, in that it serves as an intriguing backdrop to the thriller/detective style plot, and it's not a dense exposition of political theories.

For some reason I found the main characters a bit less likeable in this one, even though they're the same set as in Elysium Fire.  They felt a bit&#x2026; sarcastic, righteous, full-of-themselves at times.  Also there's not so much in the way of ambiguity.  You know exactly who the goodies and the baddies are.

I like the idea of the 100 million wide constant [[participatory democracy]] that takes place.  Another very interesting idea.  Left to your imagine somewhat again, but really interesting.

Finished: November 2019.

