# Rise Up

A
: game

Made by
: [[Tesa Collective]]

Available at
: 

https://store.tesacollective.com/collections/games/products/rise-up

A board game where you work cooperatively as part of a [[movement]] to fight [[the system]].

Each turn you get to educate, agitate or organise ([[Educate, agitate, organise]])

It's a lot of fun. I like the fact that they include a storytelling element to it - you choose at the beginning what system it is that you're fighting, and certain cards you get along the way get you to think of an accompanying story to go with that action.

