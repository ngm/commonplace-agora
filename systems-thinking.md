# Systems thinking

> Systems thinking is concerned with expanding our awareness to see the relationships between parts and wholes rather than looking at just discrete, isolated parts.
> 
> &#x2013; [[Systems Thinking and How It Can Help Build a Sustainable World: A Beginning Conversation]]

<!--quoteend-->

> Systems thinking – the term given to the modern rebirth of holistic thinking in academic and professional fields – compels us to listen to our instincts, break down barriers, see the bigger picture, explore possibilities, and relearn much of what we’ve already known.
> 
> &#x2013; [[Systems Thinking and How It Can Help Build a Sustainable World: A Beginning Conversation]]

<!--quoteend-->

> Russell Ackoff put it well, “Managers are not confronted with problems that are independent of each other, but with dynamic situations that consist of complex systems of changing problems that interact with each other. I call such situations messes.”[7] Horst Rittel called them “wicked problems.”[8]
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> Churchman outlined four approaches to systems: 1) The approach of the efficiency expert (reducing time and cost); 2) The approach of the scientist (building models, often with mathematics); 3) The approach of the humanist (looking to our values); and 4) The approach of the anti-planner (accepting systems and living within them, without trying to control them).[12] We might also consider a fifth approach: 5) The approach of the designer, which in many respects is also the approach of the policy planner and the business manager, (prototyping and iterating systems or representations of systems).
> 
> &#x2013; [[A Systems Literacy Manifesto]]


## What's the point?

> Today, systems thinking is needed more than ever because we are becoming overwhelmed by complexity,” said Peter Senge, a leading thinker on systems dynamics modeling, in his book The Fifth Discipline. “Perhaps for the first time in history, humankind has the capacity to create far more information than anyone can absorb, to foster far greater interdependency than anyone can manage, and to accelerate change far faster than anyone’s ability to keep pace
> 
> &#x2013; [Systems Mapping: A Vital Ingredient for Successful Partnerships - RMI](https://rmi.org/systems-mapping-a-vital-ingredient-for-successful-partnerships/) 


## Framing

> Since “systems” are human constructions and can be thought of in infinitely many complex ways, we have to be clear about how we’re framing any particular system of interest. For example, what are its boundaries? What perspective are we taking when talking about it? How do its parts interact? And so forth.
> 
> &#x2013; [[Systems Thinking and How It Can Help Build a Sustainable World: A Beginning Conversation]]

<!--quoteend-->

> It helps when framing a system to know why we’re even talking about it in the first place! Sure, we can wax poetic about abstract notions of “systemsness,” but ultimately, thinking about things as systems is useful because it helps us to understand the world and solve problems. When analyzing or discussing systems, try to ground them in the practical context of real-world problems or phenomena, or the conversation will likely go nowhere fast.
> 
> &#x2013; [[Systems Thinking and How It Can Help Build a Sustainable World: A Beginning Conversation]]


## Links

-   [Systems Thinking Resources - The Donella Meadows Project](https://donellameadows.org/systems-thinking-resources/)
-   [Systems Scribing: Resources for Visuals and Systems Thinking - Drawing Change](https://drawingchange.com/systems-scribing-resources-for-visuals-and-systems-thinking/)
-   [Systems Thinking 101 | Leyla Acaroglu on System Change for the Circular Econo&#x2026;](https://www.unschools.co/journal-blog/2019/8/11/week-14-systems-thinking-101)

