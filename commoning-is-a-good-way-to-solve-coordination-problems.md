# Commoning is a good way to solve coordination problems

[[Flancian]] asked: 

> -   wdyt about commons/[[commoning]] as a possible solution to solving [[coordination problems]]?
> -   it seems like adding to the value of a commons, and thus unlocking further (e.g. integration) value for one and others, might be an actual incentive to agree on coordination points

I replied:

-   Oh interesting! Yes I think commoning is a fantastic way of solving coordination problems.
-   [[Peer Governance]] is one of the main spheres of commoning.
-   Commoning feels in a nutshell like agreeing that a shared goal/resource is important, then figuring out the rules needed to keep that shared goal from being lost.
-   Rules and activities
-   I guess the counterargument is the [[Tragedy of the Commons]]. Which feels pretty much like a prime example of failure in a coordination problem.
-   Of course that's what [[Ostrom]] and other scholars debunked so thoroughly.
-   I suppose, and I don't know how this is approached usually in a game theoretical sense, is the problem in a coordination problem the question of how do you actually communicate in the first place that you have a shared goal?
-   Like in the [[prisoner's dilemma]] is the problem that people just don't have a means to communicate a shared goal to each other? Or is it the assumption that even with a clearly communicated shared goal a rational individual would still choose selfishness.
-   Another thing to add is that B&amp;H describe commons as [[complex systems]], so I think not something that could easily be modelled to 'prove' it could solve a given problem.

> I suppose, and I don't know how this is approached usually in a game theoretical sense, is the problem in a coordination problem the question of how do you actually communicate in the first place that you have a shared goal?

-   ^ Re: this question, I feel that commoning kicks in once you have a shared goal. I don't know how much commoning helps to actually define the shared goal in the first place&#x2026; need to look in to that.
-   Heh one of the patterns is called [[Cultivate Shared Purpose and Values]] so maybe it does help in that regard too.
-   [[Anarchist Cybernetics]] had a lot to say about communication of shared goals too.


## Epistemic status

Type
: [[notion]]

Gut feeling
: 8

Confidence
: 5

Confidence middling because I haven't researched what a coordination problem is formally defined as.  So I'm not sure how much a coordination problem revolves around deciding what to coordinate on, or doing the coordinating once you've decided that.  But gut feeling high, because coordination seems to be a core principle of successful commoning.

