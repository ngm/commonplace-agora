# NetBlocks

> NetBlocks is a global internet monitor working at the intersection of digital rights, cybersecurity and internet governance. Independent and non-partisan, NetBlocks strives for an open and inclusive digital future for all.
> 
> &#x2013; [Our Work - NetBlocks](https://netblocks.org/about)

