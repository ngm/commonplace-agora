# 2020-06-26



## [[rclone]]

Finding rclone super handy for backups and things where cloud services are involved.

Especially useful as my internet connection is currently so poor that downloading from a cloud service to then upload to a server somewhere is terribly slow.

