# Primary literature

> Primary literature is often written by specialists for specialists, so the authors assume that the audience has a high level of understanding of the subject and of the specific terminology. 
> 
> &#x2013; 
> https://learn2.open.ac.uk/mod/oucontent/view.php?id=2008267&section=1.3.1

