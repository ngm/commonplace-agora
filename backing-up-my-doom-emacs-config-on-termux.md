# Backing up my Doom Emacs config on Termux

I use [[Doom Emacs]] on [[Termux]].  (See [[Setting up Doom Emacs in termux on Android]]).


## Why?

Backing up is always a good idea. And I've spent enough time configuring it now that it would be a pain to have to redo it.

Also, although it's for Doom running on my phone, I'd like the option of editing it from my laptop, so it'd be good to sync it there.


## How?

I guess I'll just create a git repo of the `.doom.d` folder.  Or, perhaps, I should add it to my dotfiles repo?  Yeah, that probably makes sense.  (Even though I've not using the majority of those dotfiles on Termux?)

That'll do for now.  But I do wonder how I pull in changes to Doom's defaults for the files in there.

OK, I did the following:

```shell
cd ~
git clone https://github.com/ngm/dotfiles
mv .doom.d ~/dotfiles/doom.d
ln -s dotfiles/doom.d .doom.d
cd dotfiles
```

Setup my gitlab ssh key on termux&#x2026; then:

```shell
git add . && git commit -m "Add doom dotfiles for termux" && git push
```

