# The hackerist perspective

> The hackerist perspective is an attempt to alter technology for political reasons by repurposing technological artifacts without concerning oneself with altering the process that produced said technology.
> 
> &#x2013; [Work notebooks: Against Hackerism, pt. 1 — simone-robutti](https://write.as/simone-robutti/work-notebooks-against-hackerism-pt) 

