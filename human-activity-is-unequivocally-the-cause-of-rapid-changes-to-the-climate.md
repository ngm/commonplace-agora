# Human activity is unequivocally the cause of rapid changes to the climate

[[Human activity]] is unequivocally the cause of rapid changes to the climate.  i.e. [[Climate change]].

So sayeth the [[IPCC Sixth Assessment Report]] WGI.

> found that human activity was “unequivocally” the cause of rapid changes to the climate, including sea level rises, melting polar ice and glaciers, heatwaves, floods and droughts.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

<!--quoteend-->

> Human activity is changing the Earth’s climate in ways “unprecedented” in thousands or hundreds of thousands of years
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn)

