# For an ecosocialist transition that breaks from capitalism: Arguments and proposals

URL
: http://www.globalecosocialistnetwork.net/2021/04/13/for-an-ecosocialist-transition-that-breaks-from-capitalism-arguments-and-proposals/

[[Eco-socialism]].  [[ecosocialist transition]].

> Technology, and especially information and communication technology, plays an increasingly important role in almost every field, including production, manufacturing, transport, medicine and the media. It is essential to distinguish this conception of technology fromthe Californian obsession with innovation, aiming only at profit by means of a proliferation of ‘start-ups’;

[[Eco-socialism and technology]].

> without denying the obvious benefits that,first, industrialisation and later information and communication technologies have brought with them, in the future it will be necessary to develop technologies in accordance with the criteria of social usefulness and environmental compatibility.

'social usefulness' -&gt; [[socially useful production]]

