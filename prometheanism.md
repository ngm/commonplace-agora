# Prometheanism

I first came across it in [[Half-Earth Socialism]].  If I understand - the idea that the Earth is there to be exploited for human needs.

Began with Hegel?

[[The humanization of nature]].

> Human labour is nature acting upon itself so it can become self-conscious, or, as Hegel puts it in the Encyclopedia, ‘The goal of Nature is to destroy itself and to break through its husk of immediate, sensuous existence, to consume itself like the phoenix in order to come forth from this externality rejuvenated as spirit. On this basis, Hegel and his heirs fostered the belief that the domination of nature was both feasible and historically necessary
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Although recent exegesis suggests that Marx blunted his harsh Promethean edge in old age, it is nevertheless the Promethean Marx who overshadows the socialist tradition.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Trotsky believed that a communist society could ‘cut down mountains and move them … and repeatedly make improvements in nature’ so that eventually ‘[man] will have rebuilt the earth … according to his own taste
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> actually existing Prometheanism was still a disaster. To cite but one example, the world’s fourth-largest lake, the [[Aral Sea]], was destroyed by [[Nikita Khrushchev]]’s ‘virgin lands’ campaign in the late 1950s, which diverted the Syr and Amu rivers to irrigate fields of cotton and wheat in central Asia.57 By the 1980s the lake had shrunk by half as the rivers dried up, and today a mere tenth remains. For years now, the dying giant has shed ‘dry tears’ of pesticides and salt, wind-scoured from the dry lakebed, raining death on farmers and former fishers.58
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Prometheanism is so ingrained in Marxist thought that it must be confronted, refuted, and extirpated so that socialism can be made fit for an age of environmental catastrophe
> 
> [[Half-Earth Socialism]]

