# Water privatisation

> Private-sector efficiency did not provide better service, but it did allow companies to be milked for cash.
> 
> &#x2013; [The Guardian view on water companies: nationalise a flawed private system](https://www.theguardian.com/commentisfree/2022/aug/10/the-guardian-view-on-water-companies-nationalise-a-flawed-private-system)

<!--quoteend-->

> water companies have enriched investors and senior executives but failed to adequately invest in infrastructure.
> 
> &#x2013; [The Guardian view on water companies: nationalise a flawed private system](https://www.theguardian.com/commentisfree/2022/aug/10/the-guardian-view-on-water-companies-nationalise-a-flawed-private-system)

Soaring prices and lack of investment in infrastructure.

> The move was hailed by [[Margaret Thatcher]] and her ministers as one that would ensure soaring investment in the industry while bringing down consumer prices. In fact, the opposite has occurred.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]

Ownership by foreign investors.

> At the same time, a national resource has ended up in the ownership of foreign investors. Hefty chunks of Thames Water have been bought by Chinese, Abu Dhabi and Kuwait finance groups, for example.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]

<!--quoteend-->

> Thus, something that should be treated as a national resource and a core defence against climate change has been sold off for short-term financial gain.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]

<!--quoteend-->

> England and Wales are the only countries in the world to have fully privatised their water supplies.
> 
> &#x2013; [After this drought, there will be another one: here are 10 things you should &#x2026;](https://www.theguardian.com/commentisfree/2022/aug/15/drought-parched-gardens-fire-ravaged-fields-10-things-you-should-know) 

<!--quoteend-->

> The evidence suggests that privatisation in England and Wales has led to higher bills, little or no reduction in pollution or waste, and no greater water security. 
> 
> &#x2013; [After this drought, there will be another one: here are 10 things you should &#x2026;](https://www.theguardian.com/commentisfree/2022/aug/15/drought-parched-gardens-fire-ravaged-fields-10-things-you-should-know) 

<!--quoteend-->

> Water for households costs more in England and Wales than in most regularly drought-ravaged countries in Europe.
> 
> &#x2013; [After this drought, there will be another one: here are 10 things you should &#x2026;](https://www.theguardian.com/commentisfree/2022/aug/15/drought-parched-gardens-fire-ravaged-fields-10-things-you-should-know) 

<!--quoteend-->

> For the last three decades, water companies have cut investment in upgrading the wastewater and sewage infrastructure by a fifth, despite increasing water bills by 31% in real terms since the 1990s. The basic logic of privatisation – that profits would be reinvested so that the government would not have to pay out of pocket to keep the system running – is not panning out: the companies have paid billions of pounds in dividends to shareholders instead of stopping leaks and sewage dumping
> 
> &#x2013; [Friday briefing: Britain’s rivers and oceans are filling with sewage – with n&#x2026;](https://www.theguardian.com/world/2023/apr/07/friday-briefing-britains-rivers-and-oceans-are-filling-with-sewage-with-no-sign-of-stopping)

