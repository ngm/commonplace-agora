# Malthusianism

[[Thomas Malthus]]

> The second ur-text from 1798, Malthus’ Essay on the Principle of Population, was written to attack William Godwin, a proto-anarchist utopian who rose to prominence in 1793 following the publication of Enquiry Concerning Political Justice and Its Influence on Morals and Happiness
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The same year The Population Bomb came out, population biologist and strident white nationalist Garrett Hardin published his short article ‘[[The Tragedy of the Commons]]
> 
> [[Half-Earth Socialism]]

