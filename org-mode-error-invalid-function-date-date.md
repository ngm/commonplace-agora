# org-mode error: Invalid function (date date)

When trying to open org-agenda agenda, I'm getting an empty agenda with the message

```elisp
Invalid function (date date)
```

Someone has a similar thing here for Doom Emacs: https://github.com/doomemacs/doomemacs/issues/4862

Someone has good advice here:

> I think it's because you have an updated org-mode was installed (actually compiled, I think) while the built-in org-mode package was already loaded. I've had exactly that problem when trying to access the agenda.
> 
> &#x2013; [Invalid function: (date date) : orgmode](https://www.reddit.com/r/orgmode/comments/qxmbvx/comment/hlayvfv/?utm_source=reddit&utm_medium=web2x&context=3)

Recompiling org locally seems to be the suggestion.

```shell
cd ~/.emacs.d/elpa/27.2/develop
rm org* -rf
```

Probably could have removed just the org package specifically, but just blased them all away.

Restart Emacs.  It downloads and installs/compiles the packages I removed.

Agenda is back in business!

