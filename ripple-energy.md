# Ripple Energy

URL
: https://rippleenergy.com

Company that orchestrates new wind farm and solar park projects, building and managing them on behalf of a co-op.

I joined as part of [[Derril Water Solar Park]].

If you want to join, do it via this link and we both get £25 saving once your project goes live :)

https://rippleenergy.com?ogu=27738

