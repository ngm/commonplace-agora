# Ulverston Repair Cafe, March 2024

[[Ulverston Repair Cafe]].

Second month back after being away for a few.

Looked at a couple of slow laptops.


## Laptop 1

A Dell Vostro. A bit of a long-term project for one of the other volunteers - today just backing up important files, which took about 2 hours running in the background.


## Laptop 2

An Asus running Windows 10.


### Reported issue

-   Reported as had been slowing down for a while.
-   But had recently started really creaking.


### Work carried out

-   At startup, Teams reports not having enough free space.
-   This message also appeared at startup "instruction at &#x2026; referenced memory at xxxx. The required data was not placed into memory because of an I/O error status of xxx  Click on OK to to terminate the program".
-   First thing, based off the messages, looked at available drive space.
    -   Windows Explorer -&gt; right-click C: -&gt; Properties.
    -   There was only 9Mb available.
    -   Full drive space only 60Gb.
-   Have a look for where the space is being used up.
    -   Settings -&gt; System -&gt; Storage ([How to see what's taking up space on a hard drive on Windows 10 - Pureinfotech](https://pureinfotech.com/whats-taking-up-space-hard-drive-windows-10/))
-   Mostly apps and features.  A few Gb on temporary files (mostly downloads folder).  User documents only about 5Gb.
-   Temporary files -&gt; Cleared temporary files.
-   Apps &amp; features -&gt; Uninstalled a few apps.  (3 of which were different virus checkers.)
-   After this, had 9Gb free.
-   Installed AVG Free. (You have to pay for Windows Defender now? Oh - the free version is actually called Windows Security on Windows 10.  [Stay protected with Windows Security - Microsoft Support](https://support.microsoft.com/en-gb/windows/stay-protected-with-windows-security-2ae0363d-0ada-c064-8b56-6a39afb6a963)).
-   Turned off some apps starting at startup.
    -   Settings -&gt; Apps -&gt; Startup ([How to Disable Startup Programs in Windows](https://www.howtogeek.com/74523/how-to-disable-startup-programs-in-windows/))
-   Restarted.
    -   Was forced to install some Windows updates which was annoying and wasted some time.
-   Much faster at startup!
-   Still not the speediest machine.  Said that installing more memory and an SSD could possibly help, but that they were not easy to access.
-   Wrote down instructions for checking space and removing unneeded files, and how to check startup apps periodically.  Advised to run AVG scan regularly.

