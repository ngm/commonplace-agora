# privatisation of the internet

[[Privatisation]] of the [[Internet]].

> Understanding how privatization made the modern internet is essential for any movement that seeks to remake it.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> History shows why privatization is the right target, how it forms the common foundation for the diverse dysfunctions and depredations of the modern internet. History shows us the face of the enemy.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The process of privatization started in the internet’s basement, with the pipes. In the 1990s, the US government gave the private sector a network created at enormous public expense. Corporations took over the internet’s physical infrastructure and made money from selling access to it
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> They pushed privatization up the stack. The profit motive came to organize not only the low-level plumbing of the network but every aspect of online life
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The 1990s is when the internet became a business. The government ceded the pipes to a handful of corporations while asking nothing in return.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> NSF’s actions were akin to “giving a federal park to Kmart
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> Finally, on April 30, 1995, the NSF terminated the NSFNET backbone. Within the space of a few short years, the privatization of the internet’s pipes was complete.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> In 1995, the [[NSFNET]] backbone was decommissioned; in 1996, the Telecommunications Act was passed; and in 1997, the Clinton administration released its “Framework for Global Electronic Commerce,” formally committing the federal government to a market-dominated internet, one in which “industry self- regulation” would take priority and the state would play a minimal role.
> 
> &#x2013; [[Internet for the People]]

