# Innovation

> The alternative to "innovation" is not this binary opposite, however, but creative adaptation to ever-changing needs in ways that are shared and convivial.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In the capitalist economy, companies are said to shoulder the risks of creating and marketing a product, even though their research and development budget is often subsidized by taxpayers and even though they often displace risks and expenses on to consumers, the environment, and future generations. 
> 
> &#x2013; [[Free, Fair and Alive]]

