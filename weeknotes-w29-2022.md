# Weeknotes W29 2022

tl;dr: A bit of action around [[systems thinking]], primarily as it relates to the [[environment]] and [[climate crisis]].  


## Sunday

-   Listening: [[Aaron Benanav on Associational Socialism and Democratic Planning]]

-   The [[biosphere]] is just one part of the Earth's [[climate system]].

-   I've not had so much time for the stream of late.  Most stuff is going in the garden.  Of course, changes to the garden are a stream of themselves.  I just mean I haven't been posting much to the social media streams, where others can more easily discover and interact with it.

-   Bright the hawk's flight on the empty sky.
    -   [[A Wizard of Earthsea]].

-   Yesterday we watched:
    -   [[Minions: The Rise of Gru]] at the cinema.
    -   [[Minority Report]] on TV.


## Monday

-   [[ProtonMail Android app takes a long time to load messages]]

-   Started [[Nature matters: systems thinking and experts]]


## Tuesday

-   Read: [[Humans need to value nature as well as profits to survive, UN report finds]]
    -   [[Ipbes]]


## Wednesday

-   Lots of good stuff in [[A Systems Literacy Manifesto]]
    -   [[systems literacy]]


## Thursday

-   What actually is a [[system]]?
    -   [[Thinking in Systems]].

-   [[Zen and the Art of Motorcycle Maintenance]].  [[System change]]

> If a factory is torn down but the rationality which produced it is left standing, then that rationality will simply produce another factory. If a revolution destroys a government, but the systematic patterns of thought that produced that government are left intact, then those patterns will repeat themselves. . . . There’s so much talk about the system. And so little understanding.

-   via [[Thinking in Systems]]

<!--listend-->

-   [[The Limits to Growth]]


## Saturday

-   [[Sand battery]]
    -   [[energy storage]]

