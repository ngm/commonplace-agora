# scientific socialism

> Scientific socialism refers to a method for understanding and predicting social, economic and material phenomena by examining their historical trends through the use of the scientific method in order to derive probable outcomes and probable future developments.
> 
> &#x2013; [Scientific socialism - Wikipedia](https://en.wikipedia.org/wiki/Scientific_socialism)

Compare with [[utopian socialism]].

