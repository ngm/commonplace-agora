# To FILE

Fleeting notes that I **probably** want to make permanent in the wiki.  Keeping them here to avoiding clogging up rest of org&#x2026;


## oggcamp


### day 1

-   openbenches.org
    -   nice use of peer production
    -   pulled it together with a few simple technologies
    -   people like the leaderboard&#x2026;
-   mqtt
    -   what's it for?
        -   messaging queue
        -   streams of data from sensor devices?
    -   mosquitto
    -   used with IoT things?
    -   sceptical about overuse of IoT
    -   but liked the use of it around energy poverty
    -   and saving energy from fridges - detecting when seal was wasting energy (however the answer was replacement??)
    -   saving energy from detecting positions of animals in enclosures
-   I gave a talk on community repair
    -   it was international repair day
    -   talked about what community repair is
    -   talked about open data we publish
    -   global movement
    -   talked about right to repair
-   ubuntu snaps
    -   interesting, how does it compare/relate to nix/guix?
    -   sounds similar in some ways
    -   universal packaging system, allow for multiple versions side by side
    -   easy rollback
-   astralship
    -   really liked this
    -   a small alternative living space, mix of permaculture and technology
    -   comes from pirate politics to some degree
    -   liked the ship metaphors.. you don't go on workshops, you go on voyages
    -   idea is to enable deep flow states to try and solve some of current global problems
    -   the geodesic domes are also interesting
-   video downloads
-   panel discussion
    -   taking up linux more in schools: main problem is awareness of its existences
    -   computing curriculum is apparently currently rubbish
        -   boring
        -   would be better if it was project based
    -   microsoft - good or bad?
        -   for me, focused too much on whether its good or bad for open source
        -   didn't look at ethics of microsoft overall
            -   ICE, AI for fossil fuel yields


## your undivided attention


### the dictator's handbook

-   Dec of human rights as basis of content moderation
-   FreeBasics is colonialism


## Politics


### Refugee Crisis


#### No one puts their children in a dinghy unless the water is safer than the land     :quote:


### Energy Policy     :green:


#### Onshore wind and solar will be as cheap as or cheaper than gas by 2020


#### Increased role for renewewables, particularly due to improvements in battery storage


#### Environment Plan


##### Don't want to lose environmental protections from EU law


##### UK used to be known as the dirty man of Europe


### Inventing the future


#### Catalogue of sci-fi matching political ideas I'm interested in


### Information goods are increasingly used as a way of enforcing purchase of market goods


### My politics

What are my values?  And from there, what are my politics?
I believe in the solidarity economy.
I believe in dual power as a means of transition.
I believe in community organising.  The avoidance of unnecessary hierarchy.
I believe in cooperativism and worker self-direction.
I would say that I am a social anarchist, but not sure of specific tendency.
I like Bookchin's writings, so perhaps a communalist.


## To investigate


### Repowering london


### Population increases two people every second?


### 2050 Nigeria pop equal to China?


## Politics


### Castells


### Tiqqun


### Shipping container vs dock workers


### Outrage on the internet


### If you took out tv signal would it affect anyone negatively?


### 3 aspects = automation?, surveillance and control?, commoditization


### Right look for converts, left look for traitors


## Getting outside the social industry

What are some ways to get outside the social industry?  To break the pattern of railing against the spectacle just being part of the spectacle.  Like some of the stuff Adorno chats about as being outside the culture industry.  Maybe the cyberflaneur stuff.  That kind of stuff, draw on some of these movements and look for parallels. 

Let's say that the connection is axiomatic.  Everything else (likes, replies, etc) is spectacle unless proven otherwise. What new forms can we find that are completely outside of what has gone before? Avoid the formula of the social industry.


## TODO Return to group chats

Interesting. Mentioned on Joanne interview twsu. 


## TODO OECD

Juan podcast 


## TODO Global tech taxes

Juan podcast 


## TODO I don't want my free time to be labour for a big tech firm


## TODO How do you define value of big tech firms?


## TODO Social media addiction is attacking people's freedom of thought


## TODO Solidarity across tech supply chains

Mentioned in grace blakely podcast 


## TODO Attwntion econony and the sociert of the spectscle


## TODO Cloud apps are a bit like supply chains

Global products, we have little agency.


## TODO Solidarity without class consciousness

That article says it can be dangerous. Parallel to libre software and use by big tech firms? 


## TODO BDC

Imagine if various States made interoperable platforms. With content from each.


## TODO Doesn't decentralization just mean you can't track fascists etc?

They have somewhere to hide themselves?  I got asked this question.


## TODO You needed access to a networked computer, and the knowledge–neither of those have ever been evenly distributed–but the technical architecture was incredibly exciting.


## Tools for support

-   into tools for thought, but they feel always quite individualistic.  what about tools for support?  community building? Microsolidarity?


## GPT-3 and me

How to set up little GPT-3 prompts using my wiki as a corpus?


## TODO economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst. It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers. If we want to take back control over Big Tech, we need to pay attention to more than just data.


## TODO Existing arguments about how large tech companies freely use open-source software as a foundation to build their proprietary empires must also be supplemented with the ways in which free – and waged – labour are brought into the ambit of companies via things like open-source frameworks


## TODO monopolisation of AI is not just – or even primarily – a data issue. Monopolisation is driven as much by the barriers to entry posed by fixed capital, and the ‘virtuous cycles’ that compute and labour are generating for the AI providers.


## TODO phrase is both a promise and a deflection. It’s a plea for unearned trust — give us time, we are working toward progress. And it cuts off meaningful criticism — yes, we know this isn’t enough, but more is coming


## TODO The architecture of the social network — its algorithmic mandate of engagement over all else, the advantage it gives to divisive and emotionally manipulative content — will always produce more objectionable content at a dizzying scale.


## TODO “You see lots of people putting forth a hopeful idea of a new, humane social media platform to rescue us — one that respects privacy or is less algorithmically coercive,” Siva Vaidhyanathan, a professor of media studies at the University of Virginia, told me recently. “But if we’re being honest, what they’re really proposing at that point is not really social media anymore.”  In other words, the architecture is the problem


## TODO Tiera comun


## TODO Furness Line

One of the most scenic lines on the UK that I've been on&#x2026;


## TODO Superintelligence

If you think that a superintelligencee would mean total domination and subordination, that shows what you view to be intelligent behaviour.


## TODO Economic Networks — Write.as

https://write.as/economic-networks/


## TODO Frameworks

But once you are given a tool that operates effortlessly — but only in a certain way — every choice that deviates from the standard represents a major cost.


## TODO http://www.mindorg.com/hypertext/Echt93.htm


## TODO Schools in England told not to use material from anti-capitalist groups | Education | The Guardian

https://www.theguardian.com/education/2020/sep/27/uk-schools-told-not-to-use-anti-capitalist-material-in-teaching


## TODO Ethics and agency

Agency and subjection
Four /3 software freedoms
Change from kantian to agency based framework


## TODO https://twitter.com/R2REurope/status/1313021379088994304


## TODO https://twitter.com/R2REurope/status/1313408296552980480


## TODO Tethered economy https://twitter.com/RestartProject/status/1314137772945481728


## TODO https://twitter.com/RestartProject/status/1317546759367856128


## TODO Connected thought

In short, increasing the rate of innovation can be achieved by fostering an environment with enough sociability (so we can collaborate and combine ideas), enough transmission fidelity (so we don’t keep on re-inventing the wheel), and enough transmission variance (so we can incrementally improve the wheel instead).


## TODO Interconnected zettelkasten

And threaded twitter (or threaded mastodon?)


## TODO https://twitter.com/RestartProject/status/1276172562419060739


## TODO https://twitter.com/RestartProject/status/1276158155576299521


## TODO https://twitter.com/RestartProject/status/1276157958636933120


## TODO https://twitter.com/RestartProject/status/1276150550225190914


## TODO The law, sometimes heralded as “ the ‘Magna Carta’ of the internet,” was passed in 1996 to provide websites with incentive to delete pornography, but it has since evolved. It is now effectively a shield websites use to protect them from responsibility for all sorts of activity on their platforms, from illegal gun sales to discriminatory ads.  


## TODO Citron has worked for nearly two decades to find legal and social strategies to combat the cyber harassment and invasions of sexual privacy that women, sexual minorities, and people of color disproportionately experience online


## TODO IndieWeb seems like one of most active ways to.be political in tech? Or at least **an** active way.. Right to repair too.


## TODO I decided to hand-code everything in plain HTML and CSS, manually link all the pages and even hand-write the RSS feed. And to be honest, I haven’t had this much fun making a website since when I first started playing around with Microsoft Frontpage and Adobe Photoshop 4.0 in the late 90s and early 2000s.


## TODO It was all terribly exciting. Unlike traditional media, you could now speak back and participate. It was the first interplanetary communication system where anyone, anywhere in the world, could make a page and share their thoughts and ideas with the world.


## TODO But if you wanted your web page to be "on the web", where would you put it?  You needed a web host of some sort to store the pages and share a public address so other people could visit. It would ideally also be free so you could try things for fun without having to think about it too much.

Beaker makes this.change?


## TODO One way of doing so was by browsing directories, like the Geocities neighbourhoods: lists of websites often arranged by categories and sub-categories. In fact, most search engines were also directories, or portals as some were called back then.

Cyberflaneur ism?


## TODO https://blog.cjeller.site/correspondence?pk_campaign=rss-feed


## TODO Gated community more than a walled garden

Emphasis on the gated, less on the community&#x2026;

With the same hostility to the outsider. .

Interesting how architrctural metapjors come up so much. Jane Jacobs.


## “The system had to have one other fundamental property: It had to be completely decentralized. That would be the only way a new person somewhere could start to use it without asking for access from anyone else.” (Berners-Lee and Fischetti 1999, p. 16).


## Six experiments in social housing     :wiki:writing:

-   https://www.vam.ac.uk/event/e6xQ4gL3/a-home-for-all-six-experiments-in-social-housing


## https://ournetworks.ca/recorded-talks/     :wiki:


## TODO One of the hardest things about recycling is that you are not sure how [the manufacturers] made it,” says Kirkman.     :wiki:


## TODO Starting from March 2021, manufacturers selling certain household appliances will have to ensure that spare parts are available for a number of years after their product has launched; that their items can be easily disassembled (and so use screws not glue); and that they provide access to technical information to repair professionals.      :wiki:


## TODO Buyerarchy of needs     :wiki:


## Postmodernism, or, The Cultural Logic of Late Capitalism

https://www.listennotes.com/e/p/1a2fd2117bea476895d55bea024544c8.mp3 [00:57:24]

Chronological timelines and remembering the past, collapsing of time 


## Episode #116 &#x2026; Structuralism and Mythology pt. 1

https://www.listennotes.com/e/p/577501f5c1544f7cb2f06549350c7823.mp3 [00:10:34]

Barthes mythology mass media


## Structuralism and Mythology pt. 2

https://www.listennotes.com/e/p/da931452999d44c18a55032326a86f34.mp3 [00:04:18]

Social media timelines we.make. for ourselves are a mythology?


## Facebook ad boycott

When you look at the list of names of companies it is kind of depressing. And you partly think.- how can something instigated by these kinds of groups actually be good.

I guess if they rrpedent the demands of citizens, then good. But not convinced.


## Four ways to change the world Lessig


## https://generativeartistry.com


## http://a9.io/glue-comic/


## Unflattening — Nick Sousanis | Harvard University Press


## TODO https://twitter.com/julian0liver/status/1264731014011793413


## TODO Share via

The age of low-tech: towards a technically sustainable civilisation


## TODO Emacs Chat: Conversations about an awesome text editor –

https://sachachua.com/blog/emacs-chat/


## TODO https://mobile.twitter.com/Roamfu/status/1331201193100525569


## TODO https://mobile.twitter.com/kcorazo/status/1252669453961031680


## TODO https://www.roambrain.com/in-search-of-the-literature-x-ray/


## TODO Indie web is interconnected zettelkasten


## TODO European Parliament Votes for Right to Repair - iFixit

https://www.ifixit.com/News/47111/european-parliament-votes-for-right-to-repair


## TODO Death via ipad

https://twitter.com/roto_tudor/status/1334534101265682434


## TODO GIU designing freedom


## TODO Tech workers have unique access to current means of production


## TODO Less is more hickel


## TODO LikeWar: The Weaponization of Social Media


## TODO https://cydharrell.com/book/table-of-contents/


## http://meetingexpectations.surge.sh/#/


## TODO Refugees and coronavirus

The coronavirus pandemic that threatens to overwhelm the camps will have “catastrophic consequences for the refugees, Greek inhabitants and the rest of European society,” says the petition, launched last week by Dutch medical professors and public health experts.

It is an illusion to think that a COVID-19 outbreak in these camps could be kept under control: 40,000 people are living on a few square kilometres, and there are only a handful of doctors present. Many children and adults are already ravaged by physical and mental traumas.

If Europe looks away now, this situation could escalate to become a medical disaster, which would represent a serious violation of the norms and values of European healthcare. It is our duty to prevent this from happening.
https://www.theguardian.com/world/live/2020/mar/28/coronavirus-live-news-cases-in-italy-overtake-china-us-infections-pass-100000


## TODO Police and prison

Recent story where someone was promoting longer sentence lengths for murder of emergency workers?


## TODO How Facebook changedd

“We do not and will not use cookies to collect private information from any user,” vowed an early privacy policy.


## TODO Share via

Mandating interoperability, for example, would make it easier for new entrants to attract users. “We really want Facebook to have to compete based on the quality of their product,” she said.


## TODO http://hangaroundtheweb.com/2019/07/mind-maps-in-spacemacs/


## TODO Reading involves knowing what to skip

https://rosano.hmm.garden/01et5dvwp93keqfk30kb4da392


## TODO media.ccc.de - Funkwhale and the Importance of Decentralized Podcasting

https://media.ccc.de/v/rc3-520410-funkwhale_and_the_importance_of_decentralized_podcasting

Really don't want a Facebook of podcasts. Listennotes is a bit like that.


## TODO media.ccc.de - Building Blocks of Decentralization

https://media.ccc.de/v/rc3-11400-building_blocks_of_decentralization


## TODO design-system.service.gov.uk


## Anbox

run android on linux


## carbon divestment


## microrebellion / micropraxis

social account posting micro acts of dissent, revolution and rebellion. Maybe a mastadon bot? Could use book of dissent for some quotes.


## community technology - karl hess


## [Alan Kay - Quora](https://www.quora.com/profile/Alan-Kay-11)


## [New breed of local food halls in UK towns offer grub and a hub | Commercial p&#x2026;](https://www.theguardian.com/business/2021/feb/21/new-breed-of-local-food-halls-offers-grub-and-a-hub?CMP=Share_AndroidApp_Other)


## nb https://xwmx.github.io/nb/

command line note taking tool


## org-babel indentation

https://github.com/syl20bnr/spacemacs/issues/13255


## org mode / syncthing encryption

https://www.reddit.com/r/emacs/comments/mdjtyq/how_do_you_sync_your_org_files_securely/


## Appropriation of nature (and terminology) by technology

-   e.g. Apple
-   e.g. cloud
-   is there a danger of the left doing it too?
    -   e.g. [[Seeding the Wild]] &#x2026; great article, but leans on nature.  Is it OK?
-   is it probably OK as analogy
-   any time we reappropriate
    -   what are we hiding or being blind to?


## TODO Munipal ownership Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:24:50]


## TODO Benefits of coops Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:32:11]


## TODO Transition to coop economy Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:36:16]


## TODO Permanent job losses from Covid-19 are estimated to hit approximately a million by the end of 2022 and about 2m in the next decade


## TODO the immediate shock of the Covid recession could be offset by employment in low-CO2 sectors


## TODO Transparency International UK said its analysis indicated “apparent systemic biases in the award of PPE contracts that favoured those with political connections to the party of government in Westminster”, contrary to denials by civil servants and Conservative ministers


## TODO Wales to launch pilot universal basic income scheme     :wikify:

https://www.theguardian.com/society/2021/may/14/wales-to-launch-universal-basic-income-pilot-scheme?CMP=Share_AndroidApp_Other


## TODO The Guardian view on taking back the buses: a route to recovery     :wikify:

https://www.theguardian.com/commentisfree/2021/may/14/the-guardian-view-on-taking-back-the-buses-a-route-to-recovery?CMP=Share_AndroidApp_Other


## The Technology and the Society, Raymond Williams


## TODO Zak ove moko jumbie


## TODO Ian Cheng emissiaries


## TODO Betty davis     :research:


## Exarchia


## Hackerfarm, Tokyo


## Theory of change https://www.nesta.org.uk/sites/default/files/theory_of_change_guidance_for_applicants_.pdf


## Transition design http://transitiondesign.net/


## Beautiful trouble


## St Paul principles


## Autonomists like Negri


## Mouffe, deliberative democracy or agonistic pluralism


## Medium of exchange vs store of value


## M15 indignados


## TODO savesomegreen.co.uk


## TODO Securityheaders.com


## fieldready.org - engineers without borders


## Yabo farmers     :research:


## Signlfm podcasts


## Opportunity tree


## Assumptions buffet


## How does freegle image analysis work?


## Progressive WordPress apps


## African fractals


## Second hand market smartphones


## London renewable power


## Chile solar power


## Grenoble and sao Paulo ban advertising


## Food citizenship


## Anarchist Cybernetics is a bit weak on issues around cdm and exclusion, raises them but doesn't go in depth


## Shannon Weaver is good name for protagonist in book


## [An Introduction to Agent-Based Modeling: Modeling Natural, Social, and Engine&#x2026;](https://www.amazon.com/gp/product/0262731894/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=compleexplor-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0262731894&linkId=def1b699f8a5d0473b7a759715fe457a)


## https://hal.archives-ouvertes.fr/hal-00876569/file/iccsa13-416-Online_Analysis_and_Visualization_of_agent_based_models.pdf


## https://drum.lib.umd.edu/bitstream/handle/1903/19050/Ariyaratne_umd_0117N_17609.pdf?sequence=1&isAllowed=y


## https://ccl.northwestern.edu/2017/Abbott.pdf


## Building in the Open. Around the beginning of this year, I… | by Paul Frazee | Jun, 2021 | Medium

https://paulfrazee.medium.com/building-in-the-open-70ac9dccf1aa


## Saying we demand X should be also saying what do we need to do to get X nunes podcast


## Tweet from 🌌🌵🛸Bret🏜👨‍👩‍👧🚙 (@bcomnes)

https://twitter.com/bcomnes/status/1409514648198692869?s=20


## TODO Corralling - how do you decide which pages should and shouldn't be included? Maybe everything could be included, but the more people contribute the more the pages have weight?


## TODO Isn’t it good, Swedish plywood: the miraculous eco-town with a 20-storey wooden skyscraper

https://www.theguardian.com/artanddesign/2021/oct/14/skelleftea-swedish-plywood-eco-town-20-storey-wooden-skyscraper-worlds-tallest?CMP=Share_AndroidApp_Other


## TODO Britain’s early handling of the coronavirus pandemic was one of the worst public health failures in UK history, with ministers and scientists taking a “fatalistic” approach that exacerbated the death toll, a landmark inquiry has found.  “Groupthink”, evidence of British exceptionalism and a deliberately “slow and gradualist” approach meant the UK fared “significantly worse” than other countries, according to the 151-page “Coronavirus: lessons learned to date” report led by two former Conservative ministers


## TODO TiddlyWiki on Twitter: "A demo of dragging tiddlers from one wiki another on an iPhone running iOS 15 https://t.co/C8qD4GWoSK" / Twitter

https://mobile.twitter.com/TiddlyWiki/status/1447576045289345026


## TODO "My big issues with ActivityPub is that the protocol is very big and not very easy to decompose."


## TODO https://social.coop/@mike_hales/107093275450219170

mike<sub>hales</sub> - @nicksellen 
I guess I feel that 'anything goes' openness of exchange is probably not productive, and some moderately sharp focus is called for, to justify infrastructure effort. A contradiction! But there's a difference between a movement organisation and a free market in conversations loosely organised by 'shared values'. Social media can do THAT (at a cost).

OK, maybe something of the vanguardist revolutionary speaking here 🙄 O dear, I better examine my soul 
@bhaugen @organizingInFedi​

Communities of praxis.


## TODO Indieweb metaverse

https://youtu.be/PvUenrIIECU


## TODO MediArXiv Preprints | Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities

https://mediarxiv.org/sf432/


## TODO IndieWeb Thoughts: POSSE ⁑ Wesley’s Notebook

https://notebook.wesleyac.com/indieweb-thoughts-posse/


## TODO Our society is troubled. Beware those who blame it all on big tech

I agree with this mostly. Big tech has huge problems, but you can't lay blame for all of societies ills at its door. https://www.theguardian.com/commentisfree/2021/oct/24/society-blame-big-tech-online-regulation?CMP=Share_AndroidApp_Other


## TODO Isn’t it good, Swedish plywood: the miraculous eco-town with a 20-storey wooden skyscraper

https://www.theguardian.com/artanddesign/2021/oct/14/skelleftea-swedish-plywood-eco-town-20-storey-wooden-skyscraper-worlds-tallest?CMP=Share_AndroidApp_Other


## TODO Austerity in England linked to more than 50,000 extra deaths in five years

https://www.theguardian.com/society/2021/oct/14/austerity-in-england-linked-to-more-than-50000-extra-deaths-in-five-years?CMP=Share_AndroidApp_Other


## TODO https://social.coop/@mike_hales/107093275450219170

mike<sub>hales</sub> - @nicksellen 
I guess I feel that 'anything goes' openness of exchange is probably not productive, and some moderately sharp focus is called for, to justify infrastructure effort. A contradiction! But there's a difference between a movement organisation and a free market in conversations loosely organised by 'shared values'. Social media can do THAT (at a cost).

OK, maybe something of the vanguardist revolutionary speaking here 🙄 O dear, I better examine my soul 
@bhaugen @organizingInFedi​

Communities of praxis.


## TODO TiddlyWiki on Twitter: "A demo of dragging tiddlers from one wiki another on an iPhone running iOS 15 https://t.co/C8qD4GWoSK" / Twitter

https://mobile.twitter.com/TiddlyWiki/status/1447576045289345026


## TODO MediArXiv Preprints | Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities

https://mediarxiv.org/sf432/


## TODO IndieWeb Thoughts: POSSE ⁑ Wesley’s Notebook

https://notebook.wesleyac.com/indieweb-thoughts-posse/


## TODO Indieweb metaverse

https://youtu.be/PvUenrIIECU


## TODO Digital gardening is more like care and maintenance. Less about quickness and performance. Part of a slower Internet


## TODO "https://youtu.be/EbTleMei3Is is one of the better NFT-related videos I've seen"


## TODO Sloan

https://platforms.fyi/


## TODO The big idea: Is democracy up to the task of climate change?

https://www.theguardian.com/books/2021/nov/01/the-big-idea-is-democracy-up-to-the-task-of-climate-change?CMP=Share_AndroidApp_Other


## TODO web 3.0

https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/2021-11-01_XUtxcpsPVjQociNg

-   [Web3 is not decentralization - Articles - InvisibleUp](https://invisibleup.com/articles/38/)


## TODO Agora is ultimately a search engine


## TODO Downtime is fine


## TODO Coops are market formulation of the commons&#x2026; Block social disco pod


## TODO Owen Jones lockdown article


## TODO UK has run out of tests because you can exit isolation early if testing daily, and people stocking up for new years


## TODO Non-violent civil disobedience remains a vital tool for protest


## TODO Magical realism encanto


## TODO Duka. Eudamonia. Lent


## TODO Ecological civilisation lent, similar to solar punk?


## TODO Nollier lent, entangled, the life of trees


## TODO Wood burners cause nearly half of urban air pollution cancer risk – study

https://www.theguardian.com/environment/2021/dec/17/wood-burners-urban-air-pollution-cancer-risk-study?CMP=Share_AndroidApp_Other


## TODO Given the high levels of previous Covid infection and vaccination in the UK, most cases of Omicron are expected to be mild: even if antibodies fail to block infection, T cells are expected to hold up fairly well against severe illness


## TODO At current doubling rates, by New Year’s Day Omicron will be infecting 1 million people per day. PCR testing is a completely pointless exercise,” he said. “The outbreak is doubling quicker than you get a PCR result. It is now a useless tool.”


## TODO Compost issue 2

We explore how knowledge systems and authentic relationships can be made more accessible with digital infrastructure, while noticing how networked tools never fully reflect the boundless beautiful chaos of those living, breathing ecologies.


## TODO Compost issue 2

we imagine networks–online and offline–that resist the global internet clock. In this moment of content abundance, exponential time, planned obsolescence, and rapid product life cycles, what is the value of our metadata, our attention, and our capacity to care for one another


## TODO Meritocracy etymology


## TODO The Fire These Times: Mangal Media: Solarpunk, Climate Change and the New Thinkable https://anchor.fm/thefirethesetimes/episodes/Mangal-Media-Solarpunk--Climate-Change-and-the-New-Thinkable-e190llm

Really good. Like the cultural references and some discussion of what solarpunk can offer the left. Anarchism is a verb.


## TODO Maya reply

Contrast IndieWeb and Agora styles of chorus.
I also have single website. (Less creative than Maya's.)
But like to be part of community of practice.
I would see merit in similar to fediverse views. Federated timeline, probably don't look at. Local timeline as new or external user, then my follows develop over time.
Cacophony vs chorus. See thoughts on IndieWeb wiki.
Canonical page - no. Only the collected subjective views of the group. Maybe try for eventual synthesis, but that's separate from individual voices.
Maybe a different metaphor needed - jazz band rather than choir?
Fed wiki and fed quota, two possible modes perhaps. Link to type of notes e.g. Andy m. 
Not about  reaching consensus for everyrhing. Not objects at least. Maybe also not for questions. But for some goal directed things then yes. E.g. should agora do this?
Come back to timelines equivalents. Use cases. Example: quote toots. I'd like to quickly see views of people I already have some mental model of (not neceassairly agree with) rather than churn through discussion of bunch of randims.
Effective altruism. I want to see same again - opinions of people close to me - not bwause I'll nedessairly agree, but I'll know where they're coming from, their subjectivity.


## TODO https://twitter.com/interstar/status/1466465920545603586


## TODO Triad of commoning analytical tool

http://makecommoningwork.fed.wiki/view/welcome-visitors/view/make-commoning-work/view/using-the-triad-of-commoning-as-analytical-tool


## TODO How to use these patterns

http://patternlanguage.commoning.wiki/view/welcome-visitors/view/how-to-use-these-patterns


## TODO New Book on Commoning – Das Commons-Institut

https://commons-institut.org/2017/new-book-on-commoning


## TODO Tweet from Kate Raworth (@KateRaworth)

https://twitter.com/KateRaworth/status/1461386823087837186?s=20


## TODO https://thoughtshrapnel.com/2021/11/18/information-is-not-knowledge-and-knowledge-is-not-wisdom/


## TODO UK must boost recycling of materials for green industries, report says

https://www.theguardian.com/environment/2021/nov/19/uk-must-boost-recycling-of-materials-for-green-industries-report-says?CMP=Share_AndroidApp_Other


## TODO Rio Tinto’s past casts a shadow over Serbia’s hopes of a lithium revolution

https://www.theguardian.com/global-development/2021/nov/19/rio-tintos-past-casts-a-shadow-over-serbias-hopes-of-a-lithium-revolution?CMP=Share_AndroidApp_Other


## Low spec gaming https://talk.restarters.net/t/low-spec-gaming/7045


## https://www.lexaloffle.com/bbs/?pid=boc_roygbiv-0


## TODO Mass population-based vaccination in the UK should now end.     :wikify:

Guardian article


## TODO you may see the emergence of a new variant that is less severe, and ultimately, in the long term, what happens is Covid becomes endemic and you have a less severe version. It’s very similar to the common cold that we’ve lived with for many years     :wikify:

Guardian article


## TODO The US is “closer to civil war than any of us would like to believe     :wikify:


## TODO democracy was an unstoppable, levelling historical trend that would eventually conquer the worl     :wikify:

Guardian opinion piece


## TODO SCAMPER idea generator     :wikify:


## TODO Design cybernetics


## TODO Knowledge 'work' should have a purpose, which is not just quantity


## TODO England’s north-south divide continues to deepen     :wikify:

Guardian


## TODO Can you separate art from artist?     :wikify:

E.g. Eric Gill.


## TODO Why have sovereignty over your garden.

https://twitter.com/ultimape/status/1031622854612332544?s=20


## TODO Sovereignty seems more important for gardens than streams


## TODO People prefer dystopias they understand to utopias they don't understand


## TODO Any 'radical game theory'? ABMs of phase shift from capitalism?


## TODO Solarpunk could inspire people into climate repair etc as cyberpunk inspired others into virtual reality etc


## TODO The capitalist problem is the environmental problem


## TODO Reply to thrrad about federated wikis

Use cases. One person learning from peers. Peers collaborating on a shared resource or coordination problem. Any reason that federation best supports these use cases?

Activity streams applies best to recent changes, comments, etc? Humanetech good point about e.g. peertube. In peer tube is content federated or just activity around it?

Then - chorus of voices vs shared editing. Depends on use case i think. There is also a fork and merge model to consider.

Then there is perhaps a technical argument for federation.


## Here's a mini-demo of how Agora currently works for me.

-   I want to learn about bananas.
-   I navigate to the node https://anagora.org/banana
-   Flancian's node says: I like bananas.
-   Vera's node says: Bananas are bullshit.

^ heh I meant to write more here, but didn't finish.


## Ranking.

-   I feel like ranking is less important the smaller the instances.
-   Each instance is a prefiltered collection of interesting people.
-   You don't need to rank the local, you know them well enough.


## Namespace collisions.

-   Don't try to fit a particular naming for everything.
-   But do link back to the related topics, so they show in the backlinks.


## TODO estimates that 1.3 million people, or 2% of the population, are living with long Covid, based on people self-reporting symptoms that last more than a month after a Covid infection


## TODO households will pay an average of 54% more for energy this year than in 2020     :wikify:


## TODO Unlike the UK, its government took action within weeks of energy markets reaching record highs to provide a multibillion-euro package of measures to protect households and small businesses     :wikify:


## TODO Tweet from Laurie Voss (@seldo)     :wikify:

https://twitter.com/seldo/status/1486563446099300359?s=20&t=ShSxGj0tC1cuJJAvMfYayw


## TODO Prefer public ownership over nationalisation     :wikify:


## TODO Carson Ellis, Miranda july, situationist intl, for creativity prompts


## This is a dangerous equivalence as we do rely on crypto (and you likely have seen today’s news that the UK government has hired Saatchi to campaign against encryption).

equiv of cryptography and cryptocurrency


## TODO Cuban revolution eo 2. Spanish empire and us empire fighting over Cuba. And other countries. Sugar cane was huge export. Batista.     :wikify:


## TODO Unions don’t call the shots any more – but we’d all be better off if they did     :wikify:


## TODO Study finds link between Alzheimer’s and circadian clock     :wikify:

https://www.theguardian.com/society/2022/feb/10/study-finds-link-between-alzheimers-and-circadian-clock?CMP=Share_AndroidApp_Other


## TODO Maggie Appleton posts on spatial software and metaphors of the web, really interesting     :wikify:


## TODO Add how should agora federate to open agora questions     :wikify:


## TODO How should agora federate? Content like fedwiki, activity like activity pub or IndieWeb     :wikify:


## TODO Platform coops - will they ever be governable fully? In that at the bottom of stack will always be a host somewhere else. Unless peer to peer layer can be used.     :wikify:


## TODO Techgnosis     :wikify:


## TODO Stephen batchekor secular budddhism


## TODO we are about to learn what it’s like to live in an inflationary economy dominated by corporate interests, such as the fossil fuel companies, which can profit from the crisis without being required by the government to pay windfall taxes that might soften it for their customers     :wikify:


## TODO Now the personal financial trade-offs between escapism and realism will be even harder     :wikify:


## TODO https://twitter.com/W_Asherah/status/1492863195857436673     :wikify:


## TODO Information Technology and Socialist Construction: The End of Capital     :wikify:

https://www.routledge.com/Information-Technology-and-Socialist-Construction-The-End-of-Capital-and/Saros/p/book/9780415742924


## TODO Schools in Barcelona move to BBB+Moodle+NextCloud+keycloak - Community / toolstack - The meet.coop Forum

https://forum.meet.coop/t/schools-in-barcelona-move-to-bbb-moodle-nextcloud-keycloak/899


## TODO Digital commons in the city. The case of Barcelona – Free Knowledge Institute

https://freeknowledge.eu/digital-commons-in-the-city-the-case-of-barcelona/


## [BCS: Extend the lifespan of smartphones to tackle chip shortages | IT PRO](https://www.itpro.com/hardware/362251/bcs-extend-lifespan-smartphones-tackle-chip-shortage)


## [Google Lets You Install Chrome OS On PCs And Macs](https://www.forbes.com/sites/barrycollins/2022/02/15/google-lets-you-install-chrome-os-on-pcs-and-macs/)


## People don't understand what this level of infection means. Prison for vulnerable and immuno-comprised people, who make up 1/4 of the population.


## I took my wife’s iPhone into an Apple Centre and a new screen was fitted on site, in minutes… of course, it was probably actually repaired by a techie in a back workshop, and not by the Genius.

That was in fact not a repair, but them copying your wife’s data onto a refurbed device. Jessa Jones explains here
The Restart Project
Restart Podcast Ep. 65: Saving memories and exposing 'branded' repair with&#x2026;
Apple repairs are subcontracted to other companies in mega workshops.


## TODO New guidance on political impartiality in the classroom is confusing and likely to scare teachers in England away from tackling important subjects such as climate change and racism, according to education unions     :wikify:


## TODO End of isolation is political stunt by Johnson     :wikify:


## TODO Hopefully people will continue to observe guidance that is no longer law     :wikify:


## TODO https://www.ncb.coop/blog/ncb-frederick-douglass-and-co-ops-in-1846     :wikify:


## TODO https://www.positive.news/environment/the-project-to-map-mycorrhizal-fungal-networks-and-why-it-matters/     :wikify:


## TODO and openDemocracy has found. There are now 755 all-party parliamentary groups (APPGs) – a number that has ballooned from 560 five years ago. They are chaired by MPs but often run or funded by lobbyists and corporate donors seeking to influence government policy     :wikify:


## TODO The Net Zero Scrutiny Group insists it accepts the facts of the climate emergency. But as the cost of living crisis deepens, they see an opportunity to push back against their own Conservative government’s climate agenda, as Helena Horton reports     :wikify:


## TODO If Nick Clegg wants to fix Meta, he needs to tackle its problem with human rights     :wikify:


## TODO Bitcoin miners revived a dying coal plant – then CO2 emissions soared     :wikify:


## TODO Government-financed support in sectors including agriculture, fossil fuels and water is incentivising the annihilation of the natural world     :wikify:


## 


## pro-text https://karl-voit.at/2022/01/08/text-vs-video-audio-images/


## positive news https://mailchi.mp/2d4d4cc5c326/wales-basic-income?e=24a0f23e10


## TODO Knowledge adventure club

https://joelhooks.com/knowledge-adventure-club


## TODO Is there a Tory playbook like there's foundation of geopolitics for Putin


## TODO Types of revolution     :wikify:

Social, industrial, scientific, etc

Social is main one right? Others are supporting.


## TODO Emergent revolution. Phase transition. Stigmergic?


## TODO Stigmeegic regolution is too simplistic

At least on my understanding of stigmergy.

However, some inspiration to be taken for sure. Just  needs more nuance.


## TODO Venezuela and Chavez history.     :wikify:


## TODO Governable stacks, 4IR, Jackson     :wikify:


## TODO Occupy Wall Street 10 Years Later | The Nation     :wikify:

https://www.thenation.com/content/ows-10-years/


## TODO I like cooperatives because they foster democracy     :wikify:


## TODO Prague spring


## TODO Share via

A nation of increasingly lonely, friendless citizens given outlets to find collective, communal fulfillment online will be a nation spawning a range of radical political factions, groups, or movements defined by and drawing the bulk of their cohesion from their loathing of other factions, groups, or movements.


## TODO Zelenskiy has been constantly on the phone to western leaders, using his Twitter feed to cajole, encourage, scold and praise his allies. In the process, sanctions regarded as unthinkable a week ago have become a moral baseline


## TODO Putin’s nuclear signalling is designed to deter the US and its allies from further intervention in Ukraine and economic measures that he may see as an existential threat. But escalation can also take on a momentum of its own, and because the margins are so thin – leaders have only a few minutes to make decisions if they believe their countries are under attack – the US and its allies will have to tread extremely carefully in their response


## TODO Successive generations have experienced what it is like to feel the shadow of nuclear annihilation loom over their daily lives, from the Cuban crisis of 1962, to the missile standoff in Europe in the 1980s. This is shaping up to be our turn


## TODO It’s difficult for the west to create a de-escalation pathway,” Acton said. “Much presumably depends on how Putin views the domestic consequences of his backing down – something over which the west has no control


## couple of recommend books on political organising

from NVNH.
practical not theory.
look kind of america centric though, if that matters.
Jane McAlevey, No Shortcuts: Organising for Power in the New Gilded Age
Jonathan Matthew Smucker, Hegemony How-To: A Roadmap for Radicals


## TODO Nunes

Starts off great. Totality in line with my interest. The good faith critique of horizontalism is excellent. Very much like the link to second order cybernetics. Organisation from the inside. A synthesis of horizontal and vertical, I think. Given he jokingly called it networked Leninism once, sounds like perhaps starting from vertical and incorporating horizontal. Whereas e.g. Anarchist Cybernetics does it other way around.

Its political theory not necessarily tactics. That's OK. References some books on tactics - should check those. So I feel this and AC is theory, FFA and JR is tactics.


## TODO Center for a Stateless Society » The Desktop Regulatory State

https://c4ss.org/content/45465


## TODO The State: Theory and Praxis

https://thestatetheoryandpraxis.wordpress.com/

Chapter 14 seems to agree a lot with nunes


## TODO Described by the UN secretary general, António Guterres, as “an atlas of human suffering”, it revealed that billions of people now live in parts of the world where they are highly vulnerable to climate change


## TODO Citizens of liberal democracies owe the people of Ukraine a debt of gratitude; they are at the frontline of the fight for freedom and democracy in the face of authoritarianism, for rights that most of us are complacent about


## TODO Share via

Techno-babble explanation: Li-ion batteries have a voltage range in which they operate safely, and this is enforced by the power circuitry. As Li-ions age, their internal resistance grows. When the phone does something processor-intensive, it draws significant current. Pass this current through the increased battery resistance, and you get added heat generation and a substantial voltage drop. This voltage slump trips the power circuitry to cut the battery off in order it to protect it from deep discharge. As a result, your phone shuts off as gracefully as a mirrored cat. It then stops drawing current, which allows the battery voltage to float back up to operating parameters, and the cycle is ready to repeat


## TODO Share via

Convoluted human analogy: A worn Li-ion battery is like a heart encrusted with cholesterol. If you overexert a cholesterol-laden heart in a burst of physical activity, it may not be able to supply adequate blood flow to your organs, and you could pass out. Likewise, an aging battery can’t circulate electrical current fast enough to keep up with regular phone usage—so it may suddenly shut down


## TODO Your Smartphone Battery, Explained | iFixit News

https://www.ifixit.com/News/10990/your-smartphone-battery-explained


## TODO Anti-fracking campaigners have vowed to give energy firms “no peace” if the government lifts the moratorium on fracking. A month ago fracking was declared effectively dead in Britain after Cuadrilla announced plans to concrete up its Blackpool wells. But after Vladimir Putin’s invasion of Ukraine, and expected shortages of gas, some Conservative MPs and energy lobbyists are touting it as the solution.


## TODO One solution that might prove more popular is making all public transport free of charge


## TODO Urban Sensing: Out of the Woods by Dana Cuff, Mark Hansen, Jerry Kang :: SSRN

https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1092932


## TODO Revolution of dignity


## TODO Spadework: Spadework Against the Doomsday Clock https://cdn.stationista.com/feeds/spadework [00:02:08]

Meaning of spadework, metaphor from civil rights movement


## TODO Ukraine’s neighbouring countries are still struggling to provide shelter for the estimated 2.6 million who have fled since the Russians invaded last month


## TODO After opting to become the only country in Europe to introduce a visa regime for the refugees it emerged last Sunday that just 50 visas had been granted in the 10 days since Russia invaded Ukraine. At the same time the EU had given sanctuary to 1.4 million refugees


## TODO Hunger, and consequent political unrest, affecting poorer countries in the Middle East, Africa and Asia is a growing fear as Ukraine’s and Russia’s wheat, grain and vegetable oil exports are cut off. In Tunisia, symbolic birthplace of the Arab spring revolts, bread prices recently hit an unsustainable 14-year high


## TODO Emmanuel Macron’s oft-mocked vision of a sovereign Europe that enjoys “strategic autonomy” and its own military and security capabilities independent of the US has been boosted by the war. Rattled EU leaders meeting at last week’s Versailles summit agreed Europe urgently needed to be better able to defend itself.


## TODO Nato has emerged united and stronger, so far, and there is talk of Finland and Sweden joining (though not Ukraine). But the 30-country, US-led alliance is facing criticism for not doing more to help Kyiv. And the war has revived debate over whether Nato’s eastward enlargement after the Soviet collapse was a blunder that contributed to the current crisis.


## TODO Europe opened its borders amid an epic outpouring of public support. But the EU’s longstanding lack of an agreed, collective refugee policy, and Britain’s shamefully mean-spirited response, suggest trouble ahead as the numbers grow


## TODO Xinjiang, home to China’s persecuted Uyghur Muslim minority, is one of many global troublespots whose urgent problems have been eclipsed by Ukraine. Millions of Afghans enduring a winter of hunger and fear under Taliban rule suddenly seem forgotten. The plight of civilians caught up in Ethiopia’s civil war is another glaring blindspot


## TODO Data Dialogues: 16. Is "more data" the relevant question? https://www.openenvironmentaldata.org/storytelling/datadialogues-s1-eps161718 [00:05:57]

Handing knowledge out rather than taking knowledge in


## TODO Data Dialogues: 16. Is "more data" the relevant question? https://www.openenvironmentaldata.org/storytelling/datadialogues-s1-eps161718 [00:10:51]

Make z sure not just extracting data from a community


## TODO Ovsyannikova was reportedly arrested shortly after her protest and taken into police custody as a wave of praise and support for her actions spread online. She could face prison time under Russian legislation that criminalises speaking out about the invasion of Ukraine.


## TODO Insurrectionary Uprisings: A Reader in Revolutionary Nonviolence and Decolonization - Daraja Press

https://darajapress.com/publication/insurrectionary-uprisings-a-reader-in-revolutionary-nonviolence


## [[Software for counterpower]]


## TODO the US and China will meet amid concern that Beijing could supply arms to Putin’s war machine


## TODO growing numbers of new infections in multiple countries led one expert to prompt speculation that Europe could be at the start of a sixth wave of Covid infections, partly driven by the “stealth” BA.2 Omicron variant


## TODO The neo-Nazi concern almost certainly stems from the reputation of some of the volunteer brigades who fought the separatists in the 2014 war, such as the Azov battalion, which had far-right affiliations. These have since been folded into the Ukrainian national guard.


## TODO “It’s an awkward position for the west,” said Ries. “It is true that the US and Nato have used force when they felt they needed to. Sometimes it was justified, as in the Balkans in 1995, but sometimes it very dodgy like in Iraq. From the Russian perspective, I can see how they can make that argument.”


## TODO Of course, two wrongs don’t make a right. And while there are similarities between Iraq and Ukraine – invasion of sovereign territory, spurious justification, large scale civilian death, no clear plan for endgame – there are differences too.  “Volodymyr Zelenskiy is a democratically elected leader, who has not committed human rights abuses,” Bakke said


## TODO The case for radical change has been well made and is widely accepted, but the Home Office and home secretary are incapable of delivering it


## TODO The US president, Joe Biden, will warn his Chinese counterpart, Xi Jinping, that he will face “costs” if Beijing rescues fellow authoritarian ally Russia from intense western sanctions aimed at punishing Moscow’s invasion of Ukraine.


## TODO The Next System Podcast: Ep. 17: Social Transformation Through ‘The Commons’ (W/ David Bollier) https://nextsystem.libsyn.com/ep-17-social-transformation-through-the-commons-w-david-bollier [00:17:34]

The commons is about encouraging use value by social need


## TODO Red plenty. A contrast of worlds. Bit without certainty which is better. Ambiguous. Bit like dispossessed.


## TODO Syrians join Russian ranks in Ukraine as Putin calls in Assad’s debt


## TODO But to frame our condemnations as a binary clash of rival value systems is to absolve ourselves of our own alleged war crimes, committed as recently as this century in Iraq and Afghanistan. It is to pretend “our” wars are just and only theirs are evil, to make out that Afghan boys seeking asylum from the Taliban are inevitably liars and cheats while Ukrainian kids fleeing Russian bombs are genuine refugees.


## TODO The US and the UK funded, staffed and applauded the programmes meant to “transform” the country’s economy, but which actually handed over the assets of an industrialised and commodity-rich country to a few dozen men with close connections to the Kremlin


## TODO Today, Boris Johnson claims Mohammed bin Salman is a valued friend and partner to the UK, and sells him arms to kill Yemenis and pretends not to notice those he has executed


## TODO Energy and food bills are biggest proportion of poor peoples costs


## TODO David Bollier and John Thackara

https://vidi.noodlemaps.net/watch?v=iiOLJgbCsO4&t=16s
   SCHEDULED: <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-03-09 Wed&gt;</span></span>


## TODO How do we solve a problem like Putin?

https://www.theguardian.com/world/2022/mar/20/solve-problem-like-putin-writers-russia-ukraine-oliver-bullough-peter-pomerantsev
SCHEDULED: <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-03-20 Sun&gt;</span></span>


## TODO Could Ukraine war help end west’s reliance on hydrocarbons?

https://www.theguardian.com/environment/2022/mar/18/could-ukraine-war-help-end-wests-reliance-on-hydrocarbons


## TODO P&amp;O Ferries’ summary sacking of 800 workers in a pre-recorded Teams message so they could be replaced within hours by agency workers at allegedly half the cost offended all three

https://theguardian.com/commentisfree/2022/mar/20/observer-view-p-and-o-ferries-disregard-employment-law


## TODO P&amp;O Ferries’ summary sacking of 800 workers in a pre-recorded Teams message so they could be replaced within hours by agency workers at allegedly half the cost offended all three


## TODO Could Ukraine war help end west’s reliance on hydrocarbons?


## TODO How do we solve a problem like Putin?


## TODO The UN said 3,389,044 Ukrainians had left the country since Russia’s invasion began on 24 February, 90% of them women and children.     :wikify:


## TODO Fracking would have no impact on uk fuel bills

Fracking is also deeply unpopular with the public and given that any shale gas extracted would have to be sold at international market prices, it would have no impact on UK fuel bills


## TODO Ghana music Joe meah     :wikify:


## TODO Slingsby t21


## TODO Slingsby swallow


## TODO Share via

Interestingly, the part of the brain involved in thinking about the future is also associated with recollecting the past; this area is called the hippocampus, a seahorse-shaped portion deep in the brain’s temporal lobe that plays an essential role in consolidating the details of our experiences, including places, people, objects, and actions. The hippocampus is involved with precisely reconstructing historical events and building frameworks for potential events in the future.


## TODO Share via

creativity is not really about building something new out of thin air. It’s about using your past experiences, connecting the dots to visualize the future, and letting your mind wander so you can experience more eureka moments. Maintaining a more creative brain requires building a few habits to tap into the three key aspects of the brain-creativity relationship: memory, imagination, and mind wandering.


## TODO We’re keeping events hybrid: will you do the same? / mySociety

https://www.mysociety.org/2022/03/24/were-keeping-events-hybrid-will-you-do-the-same/


## TODO Civic Tech Surgery #3 – TICTeC

https://tictec.mysociety.org/civic-tech-surgery-3/


## TODO Why are we burning our recycling? – video

https://www.theguardian.com/environment/video/2022/mar/24/why-are-we-burning-our-recycling-video?CMP=Share_AndroidApp_Other


## TODO Read: [A tale of two datasets — Data Orchard](https://www.dataorchard.org.uk/news/a-tale-of-two-datasets)     :read:


## TODO [How to take a thing apart without breaking it too much | by Stefan | Apr, 202&#x2026;](https://medium.com/@stefan_88518/how-to-take-a-thing-apart-without-breaking-it-too-much-de03b684e638)     :read:


## TODO [What Next Summit - Transition Bounce forward](https://transition-bounceforward.org/what-next-summit/)     :read:


## TODO Look at UNU-KEYs product categories


## TODO Recent polls have shown a dramatic U-turn in public opinion in Finland, with the majority now favouring joining Nato after the invasion of Ukraine.


## TODO Less is more Jason hixkel


## TODO Socialism is the path from capitalism to communism


## TODO [“It's time to end the model of ‘take, make, break, and throw away' that is so&#x2026;](https://eutoday.net/news/business-economy/2022/its-time-to-end-the-model-of-take-make-break-and-throw-away-that-is-so-harmful-to-our-planet-says-eu-commissioner-frans-timmermans?utm_source=ActiveCampaign&utm_medium=email&utm_content=It+s+Earth+Day%21&utm_campaign=April+newsletter)


## TODO both sides are girding for a trial of strength in Donbas after Russia was forced to abandon its attempt to capture Kyiv


## TODO Russia’s current military activity appears to back up what are in effect reduced war aims: targeting the entire Donetsk and Luhansk oblasts claimed by the so-called separatist republics


## TODO Plans to ditch Boris Johnson as Tory leader and prime minister “sooner rather than later”, without waiting for a clear and obvious successor to emerge, are being advanced by a growing number of senior Conservatives.


## TODO Nearly 5.2 million people have fled Ukraine due to the war


## TODO Russian forces will likely increase the scale of ground offensive operations in the coming days


## TODO The awful truth is dawning: Putin may win in Ukraine. The result would be catastrophe Simon Tisdall A Russian victory would herald a new age of instability, economic fragmentation, hunger for millions and social unrest


## TODO General Intellect Unit: 087 - Designs for the Pluriverse, Part 1 Starting from: 00:00:00  Episode webpage: http://generalintellectunit.net/e/087-designs-for-the-pluriverse-part-1/  Media file: https://mcdn.podbean.com/mf/web/xtshbn/GIU_087_Designs-for-the-Pluriverse-Part-1.mp3#t=0

Designing your apps to manage your personal life as part of everyday design we all have to do


## TODO In fact, both Goldman Sachs and the noted astrophysicist Neil deGrasse Tyson have predicted that the world’s first trillionaire will be the person who figures out how to harness and exploit natural resources on asteroids.


## TODO Social Media and Capitalism: People, Communities and Commodities - Daraja Press

https://darajapress.com/publication/social-media-and-capitalist-accumulation


## TODO Nasa has identified over 12,000 asteroids within 45m kilometers of Earth that contain iron ore, nickel, precious metals and other minerals. Just a single 3,000ft asteroid may contain platinum worth over $5tn. Another asteroid’s rare earth metals could be worth more than $20tn alone. According to the Silicon Valley entrepreneur Peter Diamandis, “There are twenty-trillion-dollar checks up there, waiting to be cashed!”


## TODO [A Day in the Life 2035 - Regen](https://www.regen.co.uk/publications/day-in-the-life-2035/)     :read:


## TODO [England's tallest onshore wind turbine to power 3,000 homes by 2023 | Busines&#x2026;](https://www.businessgreen.com/news/4048404/england-tallest-onshore-wind-turbine-power-homes-2023)     :read:


## Net Zero Check twitter bot


## TODO [Rentals of iPhones Might Improve Device Longevity - Bloomberg](https://www.bloomberg.com/opinion/articles/2022-04-20/rentals-of-iphones-might-improve-device-longevity)


## TODO https://earthpercent.org/


## TODO https://twitter.com/i/web/status/1518994666175221760


## TODO https://www.washingtonpost.com/world/2022/04/23/ukraine-belarus-railway-saboteurs-russia/     :read:


## TODO Microsoft and right to repair   https://talk.restarters.net/t/microsoft-s-shareholders-demand-right-to-repair/5172/8


## TODO [When decolonization meets post-capitalism: the third annual post-capitalism c&#x2026;](https://www.shareable.net/post-capitalism-conference/)     :read:


## TODO [When decolonization meets post-capitalism: the third annual post-capitalism c&#x2026;](https://www.shareable.net/post-capitalism-conference/)     :read:


## TODO [Reports - Big Repair Project](https://www.bigrepairproject.org.uk/reports)     :read:


## TODO [Could Anglesey’s tidal energy project drive a new energy revolution? | Renewa&#x2026;](https://www.theguardian.com/environment/2022/apr/24/could-angleseys-tidal-energy-project-drive-a-new-energy-revolution?utm_term=626a73b45ee120d673fdf9760b325a66&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)     :read:


## TODO [‘What we now know … they lied’: how big oil companies betrayed us all | Docum&#x2026;](https://www.theguardian.com/tv-and-radio/2022/apr/20/what-we-now-know-they-lied-how-big-oil-companies-betrayed-us-all?utm_term=626a73b45ee120d673fdf9760b325a66&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)     :read:


## https://bardseagreenfilms.com/nadur/


## [Unpacking the Myth of Web3: Decentralization of What? by Alice Yuan Zhang](https://aliceyuanzhang.com/decentralization)


## ['Conservation is no longer enough': The Wildlife Trusts plots major nature re&#x2026;](https://www.businessgreen.com/news/4048846/conservation-wildlife-trusts-plots-major-nature-restoration-drive)


## [Decarbonization as a Service](https://logicmag.io/clouds/decarbonization-as-a-service/)


## TODO https://news.sky.com/story/cost-of-living-the-four-simple-changes-you-can-make-to-save-400-on-your-energy-bills-12596293


## TODO https://nitter.noodlemaps.net/elipariser/status/1518594942356054016#m


## TODO https://www.otherlife.co/pkm/


## https://subconscious.substack.com/p/credible-exit?token=eyJ1c2VyX2lkIjo2MzEwNjc0LCJwb3N0X2lkIjo0Nzk2NDg3NCwiXyI6Im9GWTZHIiwiaWF0IjoxNjUxNTA2NTM2LCJleHAiOjE2NTE1MTAxMzYsImlzcyI6InB1Yi0zMDc5OTMiLCJzdWIiOiJwb3N0LXJlYWN0aW9uIn0.U-c8dn-Un2pk5NSdOO2N8D4xLm3Y8tMdBtmcVKi3aI8&s=r


## TODO https://repair.eu/news/austria-makes-repair-more-affordable/ https://twitter.com/i/web/status/1520052362110509061


## TODO https://arstechnica.com/science/2022/05/elephant-in-the-room-clean-energys-need-for-unsustainable-minerals/


## TODO https://grist.org/technology/apple-just-launched-its-first-self-repair-program-other-tech-companies-are-about-to-follow/?mc_cid=b2531294b5&mc_eid=9767c713d7


## TODO [Eco-tourist visits all Wainwrights by public transport | The Westmorland Gazette](https://www.thewestmorlandgazette.co.uk/news/20080250.eco-tourist-visits-wainwrights-public-transport/)


## Focus on municipal technology (not just municipal social media)


## How can municipalism approach climate and technology?


## How can I be involved in climate and technology in my local area?


## [Charity Digital - Topics - Data is the new plastic](https://charitydigital.org.uk/topics/topics/data-is-the-new-plastic-10062)


## [Understand how your council works: Types of council - GOV.UK](https://www.gov.uk/understand-how-your-council-works)


## [‘Record after record’: Brazil’s Amazon deforestation hits April high, nearly &#x2026;](https://www.theguardian.com/world/2022/may/07/record-after-record-brazils-amazon-deforestation-hits-april-high-nearly-double-previous-peak?CMP=Share_iOSApp_Other)


## [Environment tipping points fast approaching in UK, says watchdog | Green poli&#x2026;](https://www.theguardian.com/environment/2022/may/12/environment-tipping-points-fast-approaching-in-uk-says-watchdog)


## [Session#5 discussion - commons.hour - The meet.coop Forum](https://forum.meet.coop/t/session-5-discussion/890)  - Regionalising and infrastructuring the coop economy - A federated vision for digital infrastructure


## [Ciska Ulug (@ciskarae): "Join us May 24 15:30-17:00 for a hybrid transdiscipl&#x2026;](https://nitter.noodlemaps.net/ciskarae/status/1524389123729018881#m)


## [Feminist Futures Programme (@femfutprog): ""We must not limit ourselves to th&#x2026;](https://nitter.noodlemaps.net/femfutprog/status/1525079414677159938#m)


## [James Muldoon (@james<sub>muldoon</sub>\_): "Was asked in a NEF podcast whether we shoul&#x2026;](https://nitter.noodlemaps.net/james_muldoon_/status/1525059462515539971#m)


## [MANCEPT | Digital Democracy: Governance and Resistance in a Digital Era - MAN&#x2026;](https://sites.manchester.ac.uk/mancept/mancept-workshops/programme-2022-panels/digitaldemocracy/)


## [Schumacher Center for a New Economics (@Center4NewEcon): "Kees Klomp intervie&#x2026;](https://nitter.noodlemaps.net/Center4NewEcon/status/1525104106360164353#m)


## [Demos (@Demos): "Join us on May 25th for the presentation of @james<sub>muldoon</sub>\_&amp;&#x2026;](https://nitter.noodlemaps.net/Demos/status/1525130343161942016#m)

\*[Climate limit of 1.5C close to being broken, scientists warn | Climate crisis&#x2026;](https://www.theguardian.com/environment/2022/may/09/climate-limit-of-1-5-c-close-to-being-broken-scientists-warn?utm_term=627ce8b420f9af5dd38d0224b13bb9cc&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email) 
\*[The secret world beneath our feet is mind-blowing – and the key to our planet&#x2026;](https://www.theguardian.com/environment/2022/may/07/secret-world-beneath-our-feet-mind-blowing-key-to-planets-future?utm_term=627ce8b420f9af5dd38d0224b13bb9cc&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email) 
\*[How worried should we really be about ‘insectageddon’? | Jane Hill | The Guar&#x2026;](https://www.theguardian.com/commentisfree/2022/may/08/worried-about-insectageddon-insect-decline?utm_term=627ce8b420f9af5dd38d0224b13bb9cc&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email) 
\*[How Sweden Sends Just 1% of Its Trash to Landfills](https://reasonstobecheerful.world/waste-to-energy-sweden-power-plants/) 
\*[Why whales could be the key to storing our carbon emissions - Positive News](https://www.positive.news/environment/why-whales-could-be-the-key-to-storing-our-carbon-emissions/) 
\*[The social media platform designed to stop you ‘scrolling through bile’ - Pos&#x2026;](https://www.positive.news/society/the-new-social-media-platform-designed-to-stop-you-scrolling-through-bile/) 
\*[Why the Guardian is putting global CO2 levels in the weather forecast | Green&#x2026;](https://www.theguardian.com/environment/2019/apr/05/why-the-guardian-is-putting-global-co2-levels-in-the-weather-forecast?utm_term=6257ffe1e4ea3bf91bed7870de632c77&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email) 
\*[What went right this week: hope for the climate, plus more positive news - Po&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-15-of-2022/) 
\*[As a science journalist I’m reconsidering having kids. I’m not the only one |&#x2026;](https://www.theguardian.com/books/2022/apr/10/as-a-science-journalist-im-reconsidering-having-kids-im-not-the-only-one?utm_term=6257ffe1e4ea3bf91bed7870de632c77&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email) 
\*[MAKE A CHANGE](https://www.nalc.gov.uk/makeachange) 
\*[New research: joining up local government and citizens in climate action / my&#x2026;](https://www.mysociety.org/2022/04/21/new-research-joining-up-local-government-and-citizens-in-climate-action/) 
\*[UK offshore wind will be “more valuable” than North Sea oil ever was - New St&#x2026;](https://www.newstatesman.com/environment/2022/05/uk-offshore-wind-will-be-more-valuable-than-north-sea-oil-ever-was) 
\*[What went right: a new approach to drugs, plus more positive news - Positive &#x2026;](https://www.positive.news/society/positive-news-stories-from-week-19-of-2022/) 
\*[Debunking Demand (IPCC Mitigation Report, Part 1)](https://www.drilledpodcast.com/debunking-demand-ipcc-mitigation-report-part-1/) 
\*[Boavizta API : automated evaluation of environmental impacts of ICT services &#x2026;](https://boavizta.org/en/blog/boavizta-api-automated-evaluation-of-ict-impacts-on-the-environment) 
\*[How much energy does cryptocurrency use?](https://davidmytton.blog/how-much-energy-does-cryptocurrency-use/) 
\*[Redecentralize Digest — April 2022 — Redecentralize.org](https://redecentralize.org/redigest/2022/04/) 
\*[Private and secure communications attacked by European Commission's latest pr&#x2026;](https://edri.org/our-work/private-and-secure-communications-put-at-risk-by-european-commissions-latest-proposal/) 


## TODO Revealed: the ‘carbon bombs’ set to trigger catastrophic climate breakdown

https://www.theguardian.com/environment/ng-interactive/2022/may/11/fossil-fuel-carbon-bombs-climate-breakdown-oil-gas?CMP=Share_AndroidApp_Other


## TODO Citizens’ Assemblies Can Help Repair Dysfunctional Democracies

https://www.noemamag.com/a-movement-thats-quietly-reshaping-democracy-for-the-better/


## TODO More than 2 million adults in the UK have gone without food for a whole day over the past month because they cannot afford to eat


## TODO the majority of people infected with Covid-19 are now at a marginally lower risk of dying than people who catch the seasonal flu


## TODO Effective altruism: the movement that helps people give away their salary - Positive News

https://www.positive.news/society/effective-altruism-the-people-who-give-some-of-their-salary-away/


## TODO Understanding the Digital Services Act | Revue

https://www.getrevue.co/profile/themarkup/issues/understanding-the-digital-services-act-1148415?utm_campaign=Issue&utm_content=view_in_browser&utm_medium=email&utm_source=Hello+World


## TODO Address pollution

https://addresspollution.org/results/71ee2022-aaee-48eb-9d16-fc688a34cc07


## TODO Nathan Schneider (@ntnsndr): "I have been seeing some pretty disturbing conflations between @exittocommunity and the Elon Twitter buyout, between democracy and absolutism. Case in point, @balajis: https://balajis.com/elondrop/" | nitter

https://nitter.noodlemaps.net/ntnsndr/status/1520533651884101632#m


## TODO 10% of budget on fuel means fuel stress


## TODO UN says up to 40% of world’s land now degraded


## TODO The really crucial thing to understand about the cost of living crisis is this: its biggest impacts are on the worst off. Even if the impacts were evenly spread, the poorest would obviously feel them most – but this time “it’s particularly severe because lower income households spend more of their income on energy,” Richard said.


## TODO “The most powerful way of targeting people who need it is the benefits system.”


## TODO UK: Dark day for civil liberties as 'deeply-authoritarian' Policing Bill passed by Lords | Amnesty International UK

https://www.amnesty.org.uk/press-releases/uk-dark-day-civil-liberties-deeply-authoritarian-policing-bill-passed-lords


## TODO Arctic shipping lanes


## TODO Change is now: Support for civil resistance is growing - Extinction Rebellion UK

https://extinctionrebellion.uk/2022/04/20/change-is-now-support-for-civil-resistance-is-growing/


## TODO Cumbria coal mine: What is the controversy about? - BBC News

https://www.bbc.co.uk/news/explainers-56023895


## TODO A win for Macron will not be a complete defeat for France’s far right

https://www.theguardian.com/commentisfree/2022/apr/20/france-emmanuel-macron-president-far-right-marine-le-pen?CMP=Share_AndroidApp_Other


## [‘Cash, coal, cars and trees’: what progress has been made since Cop26? | Cop2&#x2026;](https://www.theguardian.com/environment/2022/may/14/cash-coal-cars-and-trees-what-progress-has-been-made-since-cop26)


## [The Potential for Green Jobs in Cumbria - CAfS](https://cafs.org.uk/the-potential-for-green-jobs-in-cumbria/)


## [Sustainable Carlisle (@suscarlisle): "🌎Pushing for more local action to addre&#x2026;](https://nitter.noodlemaps.net/suscarlisle/status/1526479786075381762#m)


## [National Device Bank &amp;#x2d; Good Things Foundation](https://www.goodthingsfoundation.org/national-device-bank/)


## TODO Hayekian markets have lots of similarities to complex systems / agent based modelling? How to resolve. Perhaps Commoning.


## [Peter Kalmus: ‘As a species, we’re on autopilot, not making the right decisio&#x2026;](https://www.theguardian.com/environment/2022/may/21/peter-kalmus-nasa-scientist-climate-protest-interview?utm_term=628f5db346db630dad4b537ce00bbc06&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [Apocalypse now? The alarming effects of the global food crisis | World news |&#x2026;](https://www.theguardian.com/world/2022/may/21/apocalypse-now-the-alarming-effects-of-the-global-food-crisis?utm_term=628f5db346db630dad4b537ce00bbc06&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [A shorter workweek may increase worker productivity — but that’s not why we n&#x2026;](https://www.shareable.net/a-shorter-workweek-may-increase-worker-productivity-but-thats-not-why-we-need-one/)


## [What would happen if all vacant public spaces in UK became mini farms? - Posi&#x2026;](https://www.positive.news/environment/what-would-happen-if-unloved-public-spaces-became-community-farms/)


## Reports have already emerged of elderly people riding the bus all day, or staying in library, just to keep warm without turning on the heating. &#x2013; the cortado newsletter


## The Cortado would like to offer Sunak our sincere congratulations on being the first frontbench politician to make it onto The Sunday Times’ Rich List, sharing a £730 million fortune with his wife Akshata Murty. &#x2013; the cortado newsletter


## [Revealed: the ‘carbon bombs’ set to trigger catastrophic climate breakdown | &#x2026;](https://www.theguardian.com/environment/ng-interactive/2022/may/11/fossil-fuel-carbon-bombs-climate-breakdown-oil-gas?utm_term=628623338c88eff30fe4d51acdac7e6d&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [Poisoned legacy: why the future of power can’t be nuclear | Science and natur&#x2026;](https://www.theguardian.com/books/2022/may/14/poisoned-legacy-why-the-future-of-power-cant-be-nuclear?utm_term=628623338c88eff30fe4d51acdac7e6d&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [Joy for environmentalists as California blocks bid for $1.4bn desalination pl&#x2026;](https://www.theguardian.com/environment/2022/may/13/california-desalination-plant-huntington-beach?utm_term=628623338c88eff30fe4d51acdac7e6d&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [‘It’s a bribe’: the coastal areas that could become the UK’s nuclear dump | N&#x2026;](https://www.theguardian.com/environment/2022/may/17/its-a-bribe-the-coastal-areas-that-could-become-the-uks-nuclear-dump?utm_term=628623338c88eff30fe4d51acdac7e6d&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [The banks collapsed in 2008 – and our food system is about to do the same | G&#x2026;](https://www.theguardian.com/commentisfree/2022/may/19/banks-collapsed-in-2008-food-system-same-producers-regulators?utm_term=628623338c88eff30fe4d51acdac7e6d&utm_campaign=GreenLight&utm_source=esp&utm_medium=Email&CMP=greenlight_email)


## [‘Without it, I might be dead’: the garden that saves lives - Positive News - &#x2026;](https://www.positive.news/society/the-rio-de-janeiro-garden-that-saves-lives/)


## [Cost of living crisis: six creative ways communities are responding - Positiv&#x2026;](https://www.positive.news/society/cost-of-living-six-creative-ways-that-communities-are-responding/)


## https://butterfly-conservation.org/our-work/conservation-projects/england


## https://nitter.noodlemaps.net/ulverstonfest/status/1529736867570237440#m


## https://cafs.org.uk/solar-pv-faqs/


## https://nitter.noodlemaps.net/ulverstonfest/status/1531199906790232065#m


## https://cafs.org.uk/events/?civiwp=CiviCRM&q=civicrm/event/info&id=238


## https://nitter.noodlemaps.net/BeckettUnite/status/1532287139039301632#m


## https://www.in-cumbria.com/news/20147965.applications-powering-communities-fund-75-000-accepted/?ref=ebln


## https://nitter.noodlemaps.net/theforumbarrow/status/1531999011846115329#m


## ![[https://nitter.noodlemaps.net/pic/media%2FFULlgg0XEAIATm5.jpg%3Fname%3Dorig]]


## https://www.art-gene.co.uk/news/low-carbon-barrow-grants/


## [Book Writing inside of Emacs](https://christopherfin.com/writing/emacs-writing.html)


## TODO Future Histories: S01E31 - Daniel E. Saros on Digital Socialism and the Abolition of Capital (Part 1) Starting from: 00:33:27  Episode webpage: https://futurehistories.podbean.com/e/s01e31-daniel-saros/  Media file: https://chtbl.com/track/9E1572/mcdn.podbean.com/mf/web/nov6wp/Future_Histories_S01E31_Daniel_Saros.mp3#t=2007

Market socialism appears as the best of both worlds - freedom of individual choice, but avoising calital accunulation, etc. But without  central planning.  Not necessarily the case though


## TODO Criminal acts of violence at GP surgeries across the UK have almost doubled in five years, new figures reveal, as doctors’ leaders warn of a perfect storm of soaring demand and staff shortages


## TODO This gap in recruitment in the UK is, of course, exacerbated by Brexit. “The aviation industry traditionally hires a lot of people from Europe,” Paul Charles, CEO of travel PR firm the PC Agency tells me. “But that talent pool has shrunk, because access to the UK has been tightened and there are fewer Europeans who want to work here. And increasing inflation rates means that it costs more to hire people too, the industry is in competition in a squeezed labour market


## TODO What used to be the planning is incorporated in the accounting as the limits of what you can do


## TODO Bills are mounting, wages are falling in real terms and inflation has hit a 40-year high


## TODO I think we would still be waiting, but for the fact they wanted to knock the Sue Gray report off the headlines tomorrow


## TODO REA for household ERP


## [Solar Microgrids Are Keeping Ukraine’s Hospitals Running](https://reasonstobecheerful.world/solar-microgrids-are-keeping-ukraines-hospitals-running/)


## [The English city using community energy to drive positive change - Positive News](https://www.positive.news/environment/energy/the-english-city-using-community-energy-to-drive-positive-change/)


## [Work less, live more: 10 arguments for a shorter working week - Positive News](https://www.positive.news/economics/10-arguments-shorter-working-week/)


## [The UK is poised to get its first community-run shopping centre - Positive News](https://www.positive.news/society/the-uks-first-community-run-shopping-centre/)


## [What went right: a ‘conservation success story’, plus more positive news - Po&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-22-of-2022/)


## [What you said about smart meters - CAfS](https://cafs.org.uk/2022/05/27/what-you-said-about-smart-meters/)


## [Eco-Socialism: Economics For The Climate Crisis](https://www.noemamag.com/planning-an-eco-socialist-utopia/)


## TODO You can preserve the Earth's livability with open source - Open Sustainable Technology

https://opensustain.tech/blog/you_can_preserve_the_earth_s_livability_with_open_source/


## [Thousands of UK workers begin world’s biggest trial of four-day week | Produc&#x2026;](https://www.theguardian.com/business/2022/jun/06/thousands-workers-worlds-biggest-trial-four-day-week)


## TODO The Guardian view on a four-day week: policies needed to make it a reality

https://www.theguardian.com/commentisfree/2022/jun/06/the-guardian-view-on-a-four-day-week-policies-needed-to-make-it-a-reality?CMP=Share_AndroidApp_Other


## [What went right this week: the future of work, plus more positive news - Posi&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-23-of-2022/)


## [Saving the high street: 10 quickfire solutions for the UK and beyond - Positi&#x2026;](https://www.positive.news/society/saving-the-high-street-10-quickfire-solutions-for-the-uk-and-beyond/)


## [The Library of Things: coming to a town near you - Positive News - Positive News](https://www.positive.news/society/the-library-of-things-coming-to-a-town-near-you/)


## [Britain’s first wetland ‘super reserve’ – and why it’s so significant - Posit&#x2026;](https://www.positive.news/environment/britains-first-wetland-super-reserve-and-why-its-so-significant/)


## [The English city using community energy to drive positive change - Positive News](https://www.positive.news/environment/energy/the-english-city-using-community-energy-to-drive-positive-change/)


## TODO Is the Prime Directive and some of The Culture Contact stuff related to digital inclusion and paternalism?


## TODO It’s been sold with this disingenuous claim that it’s about parliamentary sovereignty, but the truth is it’s about sidelining judicial and parliamentary scrutiny in favour of the executive being allowed to do what they want.”


## TODO A World to Win with Grace Blakeley: Half-Earth Socialism w/ Troy Vettese and Drew Pendergrass Starting from: 00:35:14  Episode webpage: https://blubrry.com/aworldtowin/86751691/half-earth-socialism-w-troy-vettese-and-drew-pendergrass/  Media file: https://media.blubrry.com/aworldtowin/content.blubrry.com/aworldtowin/wtw061522-Drew-Troy.mp3#t=2114

Planning is not synonymous with socialism. You can have socialist planning


## TODO A World to Win with Grace Blakeley: Half-Earth Socialism w/ Troy Vettese and Drew Pendergrass Starting from: 00:36:15  Episode webpage: https://blubrry.com/aworldtowin/86751691/half-earth-socialism-w-troy-vettese-and-drew-pendergrass/  Media file: https://media.blubrry.com/aworldtowin/content.blubrry.com/aworldtowin/wtw061522-Drew-Troy.mp3#t=2175

Lucas Plan as an attempt at democratic, not top down  planning?


## TODO Software Freedom Podcast: SFP#15: All about Upcycling Android Starting from: 00:18:34  Episode webpage: https://fsfe.org/news/podcast/episode-15.en.html  Media file: https://download.fsfe.org/audio/podcast/SFP015.mp3#t=1114

Difference between custom ROM and alternative OS. Custom ROM means basically a slightly customised version of Android. As opposed to alternative OS which would be basically gnu/Linux from scratch for the mobile.


## Under Britain’s first-past-the-post electoral system a vote split between Labour, the Lib Dems, the SNP and the Greens has meant long periods of Tory hegemony.


## TODO Sea level rise in England will force 200,000 to abandon homes, data shows

https://www.theguardian.com/environment/2022/jun/15/sea-level-rise-in-england-will-force-200000-to-abandon-homes-data-shows?CMP=Share_AndroidApp_Other


## TODO Profiteering bosses, not workers, are pushing up inflation


## TODO Plans to put Cumbria at the forefront of UK's climate battle | News and Star

https://www.newsandstar.co.uk/news/20198732.plans-put-cumbria-forefront-uks-climate-battle/


## TODO CAfS carbon footprint calculators - CAfS

https://cafs.org.uk/sustainable-living-guide/cafs-carbon-calculators/


## https://green-alliance.org.uk/publication/the-bigger-picture-addressing-the-uks-hidden-carbon-footprint/


## human rights

Sharing this as I missed the extent of the update on the consultation - key sentences I've seen include&#x2026;
'restore common sense to the application of human rights in the UK' (common sense is subjective)
'make sure our common law traditions and Parliamentary sovereignty are respected' (ie. not questioned?)
'restrain the ability of the UK courts to use human rights law to impose ‘positive obligations’ onto our public authorities without proper democratic oversight' ('positive obligations are, broadly speaking, obligations "to do something" to ensure respect and protection of human rights.' according to the United Nations Office for Drugs and Crime)
'We want to protect our armed forces from human rights claims for actions taking place overseas'
'Equally, our system must strike the proper balance of rights and responsibilities, individual liberty and the public interest, rigorous judicial interpretation, and respect for the authority of elected law-makers.' (Whose interests will be considered as essential to individual liberty?)
'We examine problematic areas, including the challenges in deporting foreign national offenders.'


## TODO The average British household is £8,800 a year worse off than those in France or Germany, according to a report by the Resolution Foundation. It also found that Britain’s income inequality is higher than all other large European countries.


## [Cost of living crisis: six creative ways communities are responding - Positiv&#x2026;](https://www.positive.news/society/cost-of-living-six-creative-ways-that-communities-are-responding/)


## [What went right this week: Spain’s free trains, plus more positive news - Pos&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-28-of-2022/)

-   free train travel


## [Spain announces free rail journeys from September until the end of the year |&#x2026;](https://www.theguardian.com/money/2022/jul/15/spain-announces-free-rail-journeys-from-september-until-the-end-of-the-year)


## [Intersections of Digital Rights and Environmental and Climate Justice / Ford &#x2026;](https://www.fordfoundation.org/work/learning/learning-reflections/intersections-of-digital-rights-and-environmental-and-climate-justice/)


## [Solar Protocol Hackathon Tickets, Sat, Aug 13, 2022 at 10:00 AM | Eventbrite](https://www.eventbrite.com/e/solar-protocol-hackathon-tickets-383526487047)


## TODO Johnson has achieved very little,” Fiona said. “But nobody else in the higher echelons of the Conservative party gives a stuff. He was the only champion green Tories had


## TODO The oil and gas industry has made £2.3bn a day in profit for the last 50 years, new analysis has found. The analysis, of World Bank data, finds that petrostates and fossil fuel companies have made a total of $52tn since 1970.


## TODO Postcards from a 40C future: what extreme heat means for the UK

https://www.theguardian.com/uk-news/2022/jul/20/postcards-from-a-40c-future-what-extreme-heat-means-for-the-uk?CMP=Share_AndroidApp_Other


## TODO What Is the Water Cycle? | NASA Climate Kids

https://climatekids.nasa.gov/water-cycle/

And also cloud formation


## TODO Centrica reports operating profits of $1.34bn for first half of year despite facing ‘most challenging energy crisis in living memory’


## TODO New Zealand’s government has argued that the climate crisis is of “insufficient weight” to stop it issuing oil and gas exploration permits – despite declaring a climate emergency and committing to eliminate offshore exploration.


## [The Stack as an Integrative Model of Global Capitalism | tripleC: Communicati&#x2026;](https://www.triple-c.at/index.php/tripleC/article/view/1343)


## http://goatech.org/


## TODO Rod Oram: 'If everybody ate the average NZer’s diet we'd need another planet to sustain us' | Newsroom


## TODO Read Repair Cafe wikipedia article (mentions Commoning and conviviality)


## TODO https://podviaznikov.com

Communities? Post is interesting


## TODO Find monbiot quote on 'when alternative becomes possible, status quo becomes indefensible', he calls it techno-ethical gap


## TODO Hunger will be the “single biggest challenge” schools face as children return to classrooms in the coming weeks, sparking calls for the government to introduce universal free school meals to help tackle the crisis


## TODO Workers have been walking out to take less stressful, better-paid jobs in supermarkets, hospitality, hairdressing and factory work, care managers report. Low pay worsened by high inflation and burnout are among the most common reasons given for quitting.


## TODO It comes as the latest forecasts from the energy consultants Auxilione suggested energy prices could go above £6,000 a year for the average household next year, and Ofgem, the regulator, is expected to lift the price cap from £1,971 to £3,576 this week.


## TODO The science is clear: animal-based foods account for 57% of agricultural greenhouse gases versus 29% for food from plants.


## The revolution will be podcast


## https://twitter.com/_wearepossible/status/1566753538453245953


## Such positive tipping points are crucial, says Prof Tim Lenton at the University of Exeter: “We need to go more than five times faster than we are at decarbonising the global economy. So finding and triggering positive tipping points is a way to create the necessary acceleration of change.”     :wikify:


## For years, major nations like the US have blocked such funding, fearing unlimited liability. But there is movement, with Denmark this month becoming the first national government to commit loss and damage funds. There is also movement in funding green energy transformations in specific countries, with European nations providing $8.5bn to South Africa.     :wikify:


## “The fossil fuel industry is feasting on subsidies and windfall profits, while household budgets shrink and our planet burns,” he said last week. “I call on all developed economies to tax the windfall profits.”     :wikify:


## Guterres has become extraordinarily blunt: “The fossil fuel industry is killing us.” Another leader speaking out against fossil fuels is Pope Francis. Such attacks provide hope by eroding the legitimacy of the powerful sector, which has continually worked to block climate action.     :wikify:


## However high global temperature rises, every tenth of a degree that is avoided means someone somewhere suffers less. “We need to knuckle down as much as we can to prevent every 0.1C rise,” says Prof Bill McGuire, at University College London     :wikify:


## the extreme heat has also killed 3 million livestock     :wikify:


## Scientists from Nasa say that the drought has been caused by the La Niña weather pattern in the Pacific Ocean – which refers to the wide-range cooling of ocean surface temperatures – and the climate crisis. Rainfall has been declining in east Africa for decades as a result of the climate crisis, and this has been made even worse during the La Niña event     :wikify:


## TODO The National Grid warned on Thursday that households could experience three-hour electricity outages this winter.


## TODO Recent protests have highlighted the political, social and economic crises that have been engulfing Iran for many years.


## Every one of us will love someone who is still alive in 2100, says climate campaigner Ayisha Siddiqa. That loved one will either face a world in climate chaos or a clean, green utopia, depending on what we do today


## TODO “The vast majority of solutions have a really significant benefit to our health and wellbeing, income and standard of living around the world     :wikify:


## The tax cuts planned look set to burn a £60bn hole in the public coffers, which will be filled by borrowing.     :wikify:


## Earth’s wildlife populations have plunged by an average of 69% in just under 50 years, according to a leading scientific assessment, as humans continue to clear forests, consume beyond the limits of the planet and pollute on an industrial scale. Four years ago, the figure stood at 60%     :wikify:


## Upstream: Ep. 14: The Green Transition – The Problem with Green Capitalism Part 1 (Documentary) Starting from: 00:00:02  Episode webpage: https://www.upstreampodcast.org/greentransitionpt1  Media file: https://traffic.libsyn.com/secure/bb336368-b933-4b06-88b6-e51256ac6d81/Green_New_Deal_Pt1.mp3?dest-id=3632352#t=2     :wikify:

IMF control of Argentina via debt and structural adjustment


## jargon  ppm   The ratio of carbon dioxide molecule concentrations in the atmosphere is counted in parts per million – or ppm. Before the industrial revolution, CO2 levels were around 280 ppm. By February 2022 they stood at 419 ppm, the highest level since the Pliocene era four million years ago, when temperatures were between 3-4 degrees hotter, and sea levels were 5-40 metres higher     :wikify:


## The new environment secretary, Ranil Jayawardena, is understood to oppose solar panels being placed on agricultural land, arguing that it impedes his programme of growth and boosting food production     :wikify:


## https://indianexpress.com/article/explained/explainer-how-are-the-myanmar-protests-being-organized-7181610/


## https://theangrycleanenergyguy.com/podcast/episode-55/?utm_source=climateActionTech&utm_medium=email


## https://www.bbc.co.uk/news/world-africa-63280518?utm_campaign=Carbon+Brief+Daily+Briefing&utm_content=20221017&utm_medium=email&utm_source=Revue+Daily


## https://www.ecowatch.com/carbon-offsets-net-zero.html?utm_source=climateActionTech&utm_medium=email


## https://interactive.carbonbrief.org/q-a-should-developed-nations-pay-for-loss-and-damage-from-climate-change/?utm_source=climateActionTech&utm_medium=email


## https://climatejustice.uk/cop27/


## Manchester Green New Deal podcast: The Fossil Politics of Texas Starting from: 00:26:18  Episode webpage: http://www.gndmedia.co.uk  Media file: https://www.buzzsprout.com/919177/11376531-the-fossil-politics-of-texas.mp3#t=1578     :wikify:

IRA and its issues


## Manchester Green New Deal podcast: The Fossil Politics of Texas Starting from: 00:42:08  Episode webpage: http://www.gndmedia.co.uk  Media file: https://www.buzzsprout.com/919177/11376531-the-fossil-politics-of-texas.mp3#t=2528     :wikify:

Climate embedddd vs climate focussed 


## New Books in Science, Technology, and Society: Alex Williams and Jeremy Gilbert, "Hegemony Now: How Big Tech and Wall Street Won the World (And How We Win it Back)" (Verso, 2022) Starting from: 00:36:40  Episode webpage: https://newbooksnetwork.com  Media file: https://traffic.megaphone.fm/NBN4640192599.mp3?updated=1663793472#t=2200     :wikify:

Wealthiest companies on the world operate platforms


## The new environment secretary, Ranil Jayawardena, is understood to oppose solar panels being placed on agricultural land, arguing that it impedes his programme of growth and boosting food production     :wikify:


## Communicative capitalism, surveillance capitalism - existing sites of tech struggle against capitalism  for socialism.  Useful, but not spexififally ecosocialist.     :wikify:


## Earth’s wildlife populations have plunged by an average of 69% in just under 50 years, according to a leading scientific assessment, as humans continue to clear forests, consume beyond the limits of the planet and pollute on an industrial scale. Four years ago, the figure stood at 60%     :wikify:


## Manchester Green New Deal podcast: The Fossil Politics of Texas Starting from: 00:26:18  Episode webpage: http://www.gndmedia.co.uk  Media file: https://www.buzzsprout.com/919177/11376531-the-fossil-politics-of-texas.mp3#t=1578     :wikify:

IRA and its issues


## Manchester Green New Deal podcast: The Fossil Politics of Texas Starting from: 00:42:08  Episode webpage: http://www.gndmedia.co.uk  Media file: https://www.buzzsprout.com/919177/11376531-the-fossil-politics-of-texas.mp3#t=2528     :wikify:

Climate embedddd vs climate focussed 


## New Books in Science, Technology, and Society: Alex Williams and Jeremy Gilbert, "Hegemony Now: How Big Tech and Wall Street Won the World (And How We Win it Back)" (Verso, 2022) Starting from: 00:36:40  Episode webpage: https://newbooksnetwork.com  Media file: https://traffic.megaphone.fm/NBN4640192599.mp3?updated=1663793472#t=2200     :wikify:

Wealthiest companies on the world operate platforms


## Since “systems” are human constructions and can be thought of in infinitely many complex ways, we have to be clear about how we’re framing any particular system of interest. For example, what are its boundaries? What perspective are we taking when talking about it? How do its parts interact? And so forth.     :wikify:


## It helps when framing a system to know why we’re even talking about it in the first place! Sure, we can wax poetic about abstract notions of “systemsness,” but ultimately, thinking about things as systems is useful because it helps us to understand the world and solve problems. When analyzing or discussing systems, try to ground them in the practical context of real-world  problems or phenomena, or the conversation will likely go nowhere fast     :wikify:


## Systems thinking is concerned with expanding our awareness to see the relationships between parts and wholes rather than looking at just discrete, isolated parts.     :wikify:


## Systems thinking – the term given to the modern rebirth of holistic thinking in academic and professional fields – compels us to listen to our instincts, break down barriers, see the bigger picture, explore possibilities, and relearn much of what we’ve already known.     :wikify:


## One, we have to organize a mass base within the working class, particularly around the job-focused side of the just transition framework. We have to articulate a program that concretely addresses the class’s immediate and medium-term need for jobs and stable income around the expansion of existing “green” industries and the development of new ones, like digital fabrication or what we call community production, that will enable a comprehensive energy and consumption transition     :wikify:


## We have to weaken their ability to extract, and this entails stopping new exploration and production initiatives. This is critical because it will weaken their power, particularly their financial power, which is at the heart of their lobbying power. If we can break that, we won’t have to worry about the centrists, as you put it.     :wikify:


## Platforms make neoliberlaism normal. E.g. social media. Every social interaction has a metric.     :wikify:

From hegemony now podcast


## Pledges to cut greenhouse gas emissions will lead to global heating of 2.5C, a level that would condemn the world to catastrophic climate breakdown, according to the United Nations     :wikify:


## The UN’s environment agency has said that there’s no credible pathway to limiting global warming to 1.5C because progress to reduce carbon emissions has been “woefully inadequate”. In a report, it stated that the only way to limit the worst impacts of the climate crisis is a “rapid transformation of societies”.     :wikify:


## This bizarre legal dispute is perhaps an indicator of where social media more broadly is headed (“The old guard is falling apart,” Alex says). Following reports that Meta’s profits halved during the third quarter of the year, investors wiped more than $65bn (£56bn) off its market value on Wednesday, all while Mark Zuckerberg’s company haemorrhages $15bn a year on building Metaverse technology and its ads business shrinks. Meanwhile, Twitter’s value has been falling for a couple of years.     :wikify:


## World close to ‘irreversible’ climate breakdown     :wikify:


## ICT in rojava, cool Jackson, other game changers     :wikify:


## Software Freedom Podcast: SFP#17: Citizen participation through Free Software with Petter Joelson Starting from: 00:00:15  Episode webpage: https://fsfe.org/news/podcast/episode-17.en.html  Media file: https://download.fsfe.org/audio/podcast/SFP017.mp3#t=15     :wikify:

Decidim seems like an example of socially useful software?


## Software Freedom Podcast: SFP#17: Citizen participation through Free Software with Petter Joelson Starting from: 00:00:15  Episode webpage: https://fsfe.org/news/podcast/episode-17.en.html  Media file: https://download.fsfe.org/audio/podcast/SFP017.mp3#t=15     :wikify:

Local companies making local tweaks to Decidim


## TODO Software Freedom Podcast: SFP#17: Citizen participation through Free Software with Petter Joelson Starting from: 00:00:15  Episode webpage: https://fsfe.org/news/podcast/episode-17.en.html  Media file: https://download.fsfe.org/audio/podcast/SFP017.mp3#t=15     :wikify:

With input from the citizens too


## TODO Coordinated strike action by eg SREs could have a major impact on big tech firms. Trouble is, they are very well compensated - what would make them strike?


## The Burning Case: Organising or campaigning: how to mobilise for radical change? Starting from: 00:00:00  Episode webpage: https://www.buzzsprout.com/1250759  Media file: https://www.buzzsprout.com/1250759/11625913-organising-or-campaigning-how-to-mobilise-for-radical-change.mp3#t=0     :wikify:

Contrast of NGOs, stakeholder dialogues and civil disobedience. Both needed.


## The Burning Case: Organising or campaigning: how to mobilise for radical change? Starting from: 00:00:00  Episode webpage: https://www.buzzsprout.com/1250759  Media file: https://www.buzzsprout.com/1250759/11625913-organising-or-campaigning-how-to-mobilise-for-radical-change.mp3#t=0     :wikify:

Try not to piegeon hole the different strands of activism too mich


## TODO Controversial £360m NHS England data platform ‘lined up’ for Trump backer’s firm     :NHS:England:Palantir:

https://www.theguardian.com/society/2022/nov/13/controversial-360m-nhs-england-data-platform-lined-up-for-trump-backers-firm?CMP=Share_AndroidApp_Other


## TODO A just transition depends on energy systems that work for everyone     :energy:community:

https://www.theguardian.com/environment/2022/nov/12/cop27-dash-for-gas-africa-energy-colonialism?CMP=Share_AndroidApp_Other


## There are many global factors that affected this economic downturn, from the pandemic hangover to Europe’s energy crisis, but the UK’s chaotic approach to post-Brexit government and a decade of austerity has meant that the country is struggling more than others     :wikify:


## What this tends to mean in real terms is rising unemployment, stagnating wages and businesses struggling to keep their doors open. The Bank of England has said it expects the coming recession in the UK to be the longest since records began in the 1920s, bracing for a two-year recession that could lead to unemployment doubling. It should be noted that this is likely a worst case scenario.     :wikify:


## “The wider politics of this is that it seems to me to be extremely unlikely at this point that we get another Conservative government,” Heather says. “The question then is to what extent do a Labour government feel obliged to play to the tune that Jeremy Hunt will set out this week.”     :wikify:


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:00:00  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=0     :wikify:

Any existing socialist projects today are situated within an overall capitalist system.


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:15:18  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=918     :wikify:

Capitalism can transition to fascism as it fails people, and the poor are scapegoated amongst themselves rather the capitalist class


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:31:46  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=1906     :wikify:

Socialism is fundamentally about democracy. Participation. In society and in the work place.


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:09:43  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=583

Digital revolution has to be covered with democratic revolution


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:15:25  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=925

Data commons


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:15:50  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=950

Participatory democracy rather than just consultation


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:19:52  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=1192

Community mesh networks in red hook Brooklyn and in Detroit


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:22:26  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=1346

Mesh was only network that worked in red hook during hurricane sandy


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:23:01  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=1381

Red hook mesh works in emergency and to help those who have no connection otherwise


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:36:30  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=2190

Connectivity is fundamental to human life these days


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:37:22  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=2242

The deep value of something like WiFi is in empowering people (same true of repair cafes?)


## Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 42. TECH FOR GOOD Starting from: 00:40:32  Episode webpage: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood  Media file: https://sphinx.acast.com/reasonstobecheerful/episode42.techforgood/media.mp3#t=2432

Bristol approach, dampsensing


## Communicative capitalism, surveillance capitalism - existing sites of tech struggle against capitalism  for socialism.  Useful, but not spexififally ecosocialist.     :wikify:


## Upstream: Ep. 14: The Green Transition – The Problem with Green Capitalism Part 1 (Documentary) Starting from: 00:00:02  Episode webpage: https://www.upstreampodcast.org/greentransitionpt1  Media file: https://traffic.libsyn.com/secure/bb336368-b933-4b06-88b6-e51256ac6d81/Green_New_Deal_Pt1.mp3?dest-id=3632352#t=2     :wikify:

IMF control of Argentina via debt and structural adjustment


## Manchester Green New Deal podcast: The Fossil Politics of Texas Starting from: 00:23:37  Episode webpage: http://www.gndmedia.co.uk  Media file: https://www.buzzsprout.com/919177/11376531-the-fossil-politics-of-texas.mp3#t=1417     :wikify:

In the us, solar and wind jobs are dangerous, don't pay well, and aren't unionised.


## At Manston, another military base along the coast, which provides basic temporary accommodation in a series of marquees, the Home Office started to process small boat arrivals in February. That site too has been hit by a series of scandals including reports of infectious diseases such as diphtheria, drug-selling by guards to asylum seekers and some new arrivals released from Manston and dumped in central London. That site is locked and on 30 October protesters filmed footage of young children shouting “Freedom!” through the fence. Since then that site too has been wrapped in swathes of tarpaulin.


## There are many global factors that affected this economic downturn, from the pandemic hangover to Europe’s energy crisis, but the UK’s chaotic approach to post-Brexit government and a decade of austerity has meant that the country is struggling more than others     :wikify:


## What this tends to mean in real terms is rising unemployment, stagnating wages and businesses struggling to keep their doors open. The Bank of England has said it expects the coming recession in the UK to be the longest since records began in the 1920s, bracing for a two-year recession that could lead to unemployment doubling. It should be noted that this is likely a worst case scenario.     :wikify:


## “The wider politics of this is that it seems to me to be extremely unlikely at this point that we get another Conservative government,” Heather says. “The question then is to what extent do a Labour government feel obliged to play to the tune that Jeremy Hunt will set out this week.”     :wikify:


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:00:00  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=0     :wikify:

Any existing socialist projects today are situated within an overall capitalist system.


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:15:18  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=918     :wikify:

Capitalism can transition to fascism as it fails people, and the poor are scapegoated amongst themselves rather the capitalist class


## Green Socialist Notes: Ecosocialism 101 (Session 3) Starting from: 00:31:46  Episode webpage: https://anchor.fm/green-socialist/episodes/Ecosocialism-101-Session-3-e1mucef  Media file: https://anchor.fm/s/4d48f40c/podcast/play/56618895/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-7-25%2F4741ddbd-8d82-9c91-cd31-f2fc75437663.mp3#t=1906     :wikify:

Socialism is fundamentally about democracy. Participation. In society and in the work place.


## [Six smart ideas that your council should copy to combat climate change - Posi&#x2026;](https://www.positive.news/society/councils-leading-on-climate-action/)


## [The ‘solar punks’ creating a community power station in London - Positive News](https://www.positive.news/society/the-solar-punks-building-a-london-power-station/)


## Zahawi insisted it was up to union leaders to call off the strike and suggested they were playing into the Russian president’s agenda as he uses high energy prices fuelling inflation as a “weapon” in his war against Ukraine.


## I have found that turning off any real-time notification of boosts and likes is a good way of avoiding social media overwhelm.     :wikify:


## Also feeling the need to revisit some of tons info strats. For mastodon feeds now.     :wikify:


## Millions of households will be spending nearly third of income on fuel by spring     :wikify:


## Some of the country’s GP are advising patients requiring urgent hospital care to “get an Uber” or use a relative’s car because of the worst ever delays in the ambulance service in England.  Patients with breathing difficulties and other potentially serious conditions are being told in some cases that they are likely to be transferred more quickly from a general practice to accident and emergency if they travel by cab or private vehicle     :wikify:


## Guillermo del Toro’s Pinocchio review – a superbly strange stop-motion animation     :wikify:

https://www.theguardian.com/film/2022/nov/27/guillermo-del-toros-pinocchio-review-a-superbly-strange-stop-motion-animation?CMP=Share_AndroidApp_Other


## Turn off phone notifications.     :wikify:


## If no replies, to avoid feeling sad at lack of interactions&#x2026; Make sure posts have some intrinsic benefit to yourself.     :wikify:


## A la design patterns, there is no one answer. Just options for different contexts.     :wikify:


## [How to Harness Digital Technologies to Accelerate Climate Action - David Jens&#x2026;](https://www.youtube.com/watch?v=JRw3B1arnkM)


## TODO Greenhouse gas emissions rose again in 2022. At a time when global carbon dioxide output needs to come down by at least 7% a year between now and 2030, to give the world a chance of holding temperatures within the vital threshold of 1.5C above pre-industrial levels, that is certainly bad news


## TODO This year has been momentous in climate terms. Extreme weather has been unignorable, from record temperatures above 40C in the UK, heatwaves in China, the US, Europe and both poles, to devastating floods in Pakistan and the worst drought in Africa for 40 years, that is pushing 150 million people to starvation


## TODO Branch mag number 4

We wanted to know: how is sustainable technology tied to community governance, to the knowledge commons and digital sovereignty? What does an internet look like that takes a craft approach—honoring local knowledge, local materials, and sustainable practices.


## TODO Branch number 4 editorial

We wanted to turn away from the daily inundation of hot takes that often privilege doom and despair. We wanted to prioritize initiatives that are community-centered, place-based and contribute to the commons, as we felt strongly that this was imperative for a more just future. We wanted to consider together how we could harness the tools of the open movement and apply them to climate justice and more rapid climate action, while also stewarding the knowledge commons and accounting for its environmental impact.


## TODO Branch magazibe 4 editortial

In this issue you will find explorations of hi-craft rather than hi-tech. You will read about the hope of seed libraries and repair shops. You will learn about the leading open projects on measuring the internet’s carbon emissions and mitigating environmental damage from manufacturing hardware. You will be invited to walk along the rivers of India and to consider a handmade computer. You will be delighted in the alternative computing environments that have always been here: in rural places, among sovereign communities and with people prioritizing sustainability over reckless speed. 


## TODO Conversation With Branch's Cover Artists - Branch

Answer some of the questions here&#x2026;

https://branch.climateaction.tech/issues/issue-4/conversation-with-branchs-cover-artists/


## TODO In Envisioning Real Utopias (2010, p. 57), American analytical Marxist sociologist Erik Olin Wright suggests that much of our writings on the political economy fall into three categories. The vast majority of books not only analyse the problems and pathologies of contemporary capitalism – they challenge capitalism. The second group of books deals with, as Wright says, ‘What is the alternative to capitalism?’; and, the third group of books is about ‘How do we get from here to there?’.


## TODO The world’s oceans were the hottest ever recorded in 2022, demonstrating the profound and pervasive changes that human-caused emissions have made to the planet’s climate.


## TODO Ideas like ecology come from right wing ideologies originally, like j schmutz (?)


## TODO My Exocortex - Facets of BrettW

https://www.brettwitty.net/exocortex.html


## TODO Implementing the PARA Method in Org-mode - Wai Hon's Blog

https://whhone.com/posts/para-org-mode/


## TODO FTSE 100 bosses paid more in three days than average UK worker for whole year


### TODO https://social.coop/@ntnsndr/109888895970713114

ntnsndr - I have a new draft out, would love comments.

Innovation Amnesia: A General Theory

ntnsndr.in/Amnesia


### TODO https://social.coop/@natematias/109952243102112722

natematias - Watching the struggle to protect society from blatant tech harms, I've been asking how to maintain an affirmative vision.

It's something I was wrestling with in 2015 when I wrote this article about what we can learn from environmental citizen science, which has managed to merge love and joy and play as the heart of societal protections &amp; corporate accountability:

https://www.theatlantic.com/technology/archive/2015/06/the-tragedy-of-the-digital-commons/395129/


### TODO https://social.coop/@jdaviescoates/109927492546167922

jdaviescoates - I absolutely love We Can Make! Such an intelligent response to the #housing crisis

https://www.youtube.com/watch?v=UZdGnM8BdRw


### TODO https://ravenation.club/@childovrarn/109920884350357487

childovrarn@ravenation.club - I copped both versions of this release as they came out. Brian Bamanya is a modular artist out of Uganda who built Africa's 1st DIY synthesizer around 2019. His first release is available digitally on Hakuna Kulala, 1st and second run of cassettes sold out quickly.
\#blackfriday #afrorack #modularsynth #diy #diysynth #electronicmusic #cassette #music #musique 
https://hakunakulala.bandcamp.com/album/the-afrorack


### TODO So, er, what even is a co-op? | WAO Wiki

https://wiki.weareopen.coop/en/misc/what


## TODO https://scholar.google.co.uk/scholar_url?url=https://www.researchgate.net/profile/Llewellyn-Thomas/publication/372344425_Distinguishing_digitization_and_digitalization_A_systematic_review_and_conceptual_framework/links/64b0f3cb95bbbe0c6e31ff30/Distinguishing-digitization-and-digitalization-A-systematic-review-and-conceptual-framework.pdf&hl=en&sa=X&d=10235713902857797007&ei=GDG2ZMqNJoWM6rQPu5ygkAw&scisig=ABFrs3wVQ_VnGDzv_2D7sbFrl477&oi=scholaralrt&hist=JDWjIiEAAAAJ:8297968036082786995:ABFrs3wWMNBxeGUsuzaP2GssXjGJ&html=&pos=2&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://link.springer.com/article/10.1007/s11356-023-28332-z&hl=en&sa=X&d=3359045041671729091&ei=GDG2ZMqNJoWM6rQPu5ygkAw&scisig=ABFrs3y3aOGlI4hIh_B1jPTJqL6i&oi=scholaralrt&hist=JDWjIiEAAAAJ:8297968036082786995:ABFrs3wWMNBxeGUsuzaP2GssXjGJ&html=&pos=0&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://www.sciencedirect.com/science/article/pii/S1364032123003398&hl=en&sa=X&d=368824434950809902&ei=GDG2ZLGpKMr2mgG3oqnADQ&scisig=ABFrs3xchGgCFT4Nx3YjeFgJitk4&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:ABFrs3y2Y1dFtJSpCm8WOhkjJ1Zv&html=&pos=0&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://link.springer.com/article/10.1007/s10639-023-12025-8&hl=en&sa=X&d=16858440340423240096&ei=GDG2ZLGpKMr2mgG3oqnADQ&scisig=ABFrs3xYzs7KEAHb6F0IU-CmP0fv&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:ABFrs3y2Y1dFtJSpCm8WOhkjJ1Zv&html=&pos=1&folt=kw-top


## TODO Cumbria Recycling Limited


## TODO Share via

Thames Water is collapsing because it has racked up £14 billion of debt. In 1989 it started out with zero debt. It has spent the last 34 years profiting at our expense. The company has paid out £2.7 billion in dividends and in 2022 paid out £37 million of “internal dividends” to its parent company.


## TODO Share via

The largest stake in Thames Water is owned by a Canadian pension fund. Other shareholders include companies owned by the governments of Kuwait, Abu Dhabi and China.


## TODO Share via

The English model of privatisation is not normal - 90% of the world runs water in public ownership. Water is a natural monopoly, there is no market for consumers


## TODO Do some timelines of Albert paper, similar to Ministry one • And Half-Earth Socialism game?


## TODO One in three people on the planet hit by ‘monster Asian heatwave’ | The Independent

https://www.independent.co.uk/climate-change/news/asia-heatwave-india-china-thailand-b2323666.html?utm_source=climateActionTech&utm_medium=email


## TODO Climate Change in Jennifer Mills Dyschronia: An Eco-Social Critical Study


## TODO https://en.osm.town/@SeeandMap/110570079077962787


## TODO https://scholar.google.co.uk/scholar_url?url=https://assets.pubpub.org/8ywvpdep/51686070698433.pdf&hl=en&sa=X&d=2500406419773984557&ei=Xr2TZMW2CM2G6rQPhYSs-A4&scisig=AGlGAw9E3aprOOz3c2mMbEj9BRrX&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:AGlGAw8N3UUSjlFbgy0KKPEywnTQ&html=&pos=5&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://aaltodoc.aalto.fi/bitstream/handle/123456789/121719/master_Ahonen_Emmaleena_2023.pdf%3Fsequence%3D1%26isAllowed%3Dy&hl=en&sa=X&d=6546597088009769507&ei=Xr2TZP7IBqTGsQLfxanwCQ&scisig=AGlGAw-iv2XRWSx_9XSb2VM_uhGn&oi=scholaralrt&hist=JDWjIiEAAAAJ:8297968036082786995:AGlGAw9gEcRZjPiHA7GyBe_B7WQh&html=&pos=6&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://dspace.vutbr.cz/bitstream/handle/11012/210440/final-thesis.pdf%3Fsequence%3D-1&hl=en&sa=X&d=12670783530993686442&ei=qzqWZPTQKYqNygTF4ZrYAg&scisig=AGlGAw9vCs99VG6jGNFBPGpvUwET&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:AGlGAw8N3UUSjlFbgy0KKPEywnTQ&html=&pos=1&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://www.semantic-web-journal.net/system/files/swj3486.pdf&hl=en&sa=X&d=14002502632778146518&ei=qzqWZPTQKYqNygTF4ZrYAg&scisig=AGlGAw_UhRDQCWWqDSZpvWJedm0U&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:AGlGAw8N3UUSjlFbgy0KKPEywnTQ&html=&pos=3&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://journals.co.za/doi/abs/10.31920/2752-602X/2023/v3n1a8&hl=en&sa=X&d=13770067306932716028&ei=qzqWZOOEI5GM6rQPypipuAc&scisig=AGlGAw-7ipBEFIi6viK3KEENNFCz&oi=scholaralrt&hist=JDWjIiEAAAAJ:8297968036082786995:AGlGAw9gEcRZjPiHA7GyBe_B7WQh&html=&pos=8&folt=kw-top


## TODO Library of Things Co-Lab - Shareable

https://www.shareable.net/library-of-things-co-lab/


## TODO https://jbdpbt92.r.us-east-1.awstrack.me/L0/https:%2F%2Fgreenio.gaelduez.com%2Fe%2F286jxpk8-20-e-waste-friend-foe-in-circular-economy-with-jaqueline-mukarukundo-and-vanessa-forti%3Futm_source=climateActionTech%26utm_medium=email%26utm_campaign=cat-newsletter-174-2023-06-11/1/01000188cdd1695f-1d1b4084-8d36-4d63-b39f-631fadcf6605-000000/kvHQkOLf1_4EN9MFZ5sLEt9GP8s=326


## TODO https://jbdpbt92.r.us-east-1.awstrack.me/L0/https:%2F%2Fwww.euractiv.com%2Fsection%2Fcircular-economy%2Fnews%2Flawmakers-back-eu-ban-on-planned-obsolescence-destruction-of-unsold-goods%2F%3Futm_source=climateActionTech%26utm_medium=email%26utm_campaign=cat-newsletter-174-2023-06-11/1/01000188cdd1695f-1d1b4084-8d36-4d63-b39f-631fadcf6605-000000/i1VVm4eiU3rFfN3DZ9wSTDA2Nyg=326


## TODO Evaluating the (ir)relevance of IoT solutions with respect to environmental limits based on LCA and backcasting studies · Ninth Computing within Limits 2023

https://limits.pubpub.org/pub/8ld7lmdf/release/1


## TODO “But most of the blame for what’s happened here is being laid at the door of an Australian investment bank called Macquarie who’ve been accused of asset-stripping Thames,” Alex says. “They do not have a good reputation in the UK. Their nickname is ‘the vampire kangaroo’ because they buy companies, load them with debt and suck out as much money as possibl


## TODO https://foundation.us5.list-manage.com/track/click?u=ddc99c7db248c3df0ef4f7d24&id=24ecc36291&e=6d7bcbc7b1


## TODO https://foundation.us5.list-manage.com/track/click?u=ddc99c7db248c3df0ef4f7d24&id=4e4ef0dcfc&e=6d7bcbc7b1


## TODO Joe got a job as an annotator — the tedious work of processing the raw information used to train artificial intelligence


## TODO Remotasks is the worker-facing subsidiary of a company called Scale AI, a multibillion-dollar Silicon Valley data vendor that counts OpenAI and the U.S. military among its customers.


## TODO The current AI boom — the convincingly human-sounding chatbots, the artwork that can be generated from simple prompts, and the multibillion-dollar valuations of the companies behind these technologies — began with an unprecedented feat of tedious and repetitive labor


## TODO In 2018, an Uber self-driving test car killed a woman because, though it was programmed to avoid cyclists and pedestrians, it didn’t know what to make of someone walking a bike across the street


## TODO There are people classifying the emotional content of TikTok videos, new variants of email spam, and the precise sexual provocativeness of online ads. Others are looking at credit-card transactions and figuring out what sort of purchase they relate to or checking e-commerce recommendations and deciding whether that shirt is really something you might like after buying that other shirt. Humans are correcting customer-service chatbots, listening to Alexa requests, and categorizing the emotions of people on video calls. They are labeling food so that smart refrigerators don’t get confused by new packaging, checking automated security cameras before sounding alarms, and identifying corn for baffled autonomous tractors


## TODO We don't enjoy 'looking at our phones', its information hit of some kind that were after


## TODO https://scholar.google.co.uk/scholar_url?url=http://eprints.utar.edu.my/5711/1/ALEX_CHENG_HENG_SIANG_20UKB04147.pdf&hl=en&sa=X&d=1871710824150208841&ei=huWsZKj-Bcr2mgH68LmoAg&scisig=ABFrs3zFIXYZBa6bv8n-faBwdPAb&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:ABFrs3y2Y1dFtJSpCm8WOhkjJ1Zv&html=&pos=2&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=http://eprints.utar.edu.my/5717/1/YAP_LE_TING_19UKB01852.pdf&hl=en&sa=X&d=10868601094368745152&ei=huWsZKj-Bcr2mgH68LmoAg&scisig=ABFrs3x_nYJSTcktpaaad_LycQQB&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:ABFrs3y2Y1dFtJSpCm8WOhkjJ1Zv&html=&pos=4&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://www.sciencedirect.com/science/article/pii/S2590051X2300031X&hl=en&sa=X&d=17559190469887671862&ei=i-GnZJ7tE5KcygSVpr24BA&scisig=ABFrs3wvZ_xGtjDWqBCdBNRGk9Pd&oi=scholaralrt&hist=JDWjIiEAAAAJ:13657997283191549898:ABFrs3xeZQxAglEFEm3b97BSS06P&html=&pos=0&folt=kw-top


## TODO https://scholar.google.co.uk/scholar_url?url=https://www.researchgate.net/profile/Velibor-Bozic-2/publication/372143342_The_Relationship_Between_ESG_and_ICT/links/64a6aff48de7ed28ba7d2446/The-Relationship-Between-ESG-and-ICT.pdf&hl=en&sa=X&d=14122247866592019443&ei=8QWyZJ3aKcaMy9YPhvKA-A4&scisig=ABFrs3x0V-bQumBcLQQGVOmZwN_v&oi=scholaralrt&hist=JDWjIiEAAAAJ:8352173935128542101:ABFrs3y2Y1dFtJSpCm8WOhkjJ1Zv&html=&pos=4&folt=kw-top


## TODO AI Is Hurting the Climate in a Number of Non-Obvious Ways – The Markup

https://themarkup.org/news/2023/07/06/ai-is-hurting-the-climate-in-a-number-of-non-obvious-ways

