# using firn for publishing my org-roam digital garden

https://theiceshelf.com/firn.html

> Firn is a static site generator for org-mode. It is tailored for wikis, knowledge-bases, simple static sites &amp;tc.

Just investigating it at the moment.

My org-publish build is so horrendously slow that I'm looking at alternatives.  I like the idea of using something native to Emacs, but it's just ridiculously slow to build my static site.  It's 4000 odd org files but it still shouldn't be so slow.

Alternatively I'd dig in to how org-publish works and see if I can speed it up.  I've tried that a little before though and didn't get very far.


## Issues


### Space usage

First issue I've encountered with firn: while my commonplace folder is about 423M, the firn build folder reaches around 2.9G.  I have so little space currently available on my drive that I run out of space.  Time to free up some space&#x2026;

Why is it so big?  Each file is around 720K, nearly a megabyte.  I think it's because each page has the sitemap rendered in the site, and it's huge.

If I remove the sitemap from `_firn/layouts/default.html` then it's a much more sensible size at 59M.


### id: links

Second issue seems to be that all the links in my org files are id links, not files links.  The rendered output is using those id links, which don't resolve to anything a webserver can serve.

According to [theiceshelf/firn#107 {Documentation} Any plans for more documentation on \`org&#x2026;](https://github.com/theiceshelf/firn/issues/107) , this is kind of by design.

Think my journey with firn ends here for now.

