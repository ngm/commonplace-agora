# Thick time

> All the same, it’s surely never been harder to make the requisite psychological shift than it is today—to pause in your work for long enough to enter the coherent, harmonious, somehow thicker experience of time that comes with being “on the receiving end” of life, the feeling of stepping off the clock into “deep time,” rather than ceaselessly struggling to master it
> 
> &#x2013; [[Four Thousand Weeks]]

<!--quoteend-->

> On those occasions when synchronized action does pierce through the isolation, as during the worldwide demonstrations that followed the killing of George Floyd by Minneapolis police in 2020, it’s not unusual to hear protesters describe experiences that call to mind William McNeill’s “strange sense of personal enlargement”—a feeling of time thickening and intensifying, tinged with a kind of ecstasy
> 
> &#x2013; [[Four Thousand Weeks]]

