# All you need is links

URL
: https://subconscious.substack.com/p/all-you-need-is-links

Author
: [[Gordon Brander]]

Article.  Also a [[claim]] I guess.

Basically saying that you can get a long way with just links.

The can provide functionality for:

-   [[tags]]
-   [[folders]]
-   [[interactions]] (likes, stars, etc)
-   [[comments]]
-   [[outlines]]
-   [[semantic triples]]
-   [[topic modeling]]

