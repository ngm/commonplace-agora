# calculus of grit

&#x2013; [The Calculus of Grit](https://www.ribbonfarm.com/2011/08/19/the-calculus-of-grit/) 

First came across this on [Tom Critchlow's website tour](https://awarm.space/slow/personal-website-tours).

I've not read the full article yet, but sounds like it's a way of [[tending to the garden of your wiki]].

It boils down to: 

-   release work often
-   reference your own thinking
-   rework the same ideas again and again

I'm trying this out at the moment - putting thoughts in the [[stream]], linking them back to ideas in the wiki, and updating those wiki pages as I go along.

(P.S. finding some of these terms for basically 'doing stuff on your website' a _teensy_ bit overwrought&#x2026; but fair play, naming concepts does give you something to refer to and discuss).

