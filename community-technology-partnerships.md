# Community Technology Partnerships

[[Geeks for Social Change]]

"[…] establish a national network of Community Technology Partnerships (CTPs), which we hope will revolutionise the relationships between people, place, technology and culture."

CTPs seem to have a place-based focus.

"GFSC have an established track-record of helping communities transform themselves and their environments by developing digital solutions to support community action. However, there needs to be a step-change in supporting the digital autonomy of local communities."

