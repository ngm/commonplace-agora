# system archetype

Common patterns in [[system dynamics]].  Looking for system archetypes might help identify [[leverage points]].


## List of archetypes

(as listed in [[System Archetypes I]])

-   Drifting Goals
-   Escalation
-   Fixes That Fail
-   Growth and Underinvestment
-   Limits to Success
-   Shifting the Burden/Addiction
-   Success to the Successful
-   Tragedy of the Commons

