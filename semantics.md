# semantics

> the way in which words and language ascribe meaning to the objects being represented is the focus of study in semantics
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

