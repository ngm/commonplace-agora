# Climate Assembly UK

> Climate Assembly UK brought together people from all walks of life to discuss how the UK can reduce [[greenhouse gas emissions]] to [[net zero]] by [[2050]]. At the assembly, participants learnt about [[climate change]] and how the UK can address it, took time to discuss this with one another, and then made recommendations about what should happen.
> 
> [About - Climate Assembly UK](https://www.climateassembly.uk/about/)

<!--quoteend-->

> Climate Assembly UK had 108 members selected through a process known as ‘[[sortition]]’ or a ‘civic lottery’ to be representative of the UK population. They included include engineers, health workers, parents and grandparents.
> 
> [Who took part? - Climate Assembly UK](https://www.climateassembly.uk/detail/recruitment/index.html)

<!--quoteend-->

> Key recommendations in the report included:
> 
> -   Frequent flyer tax for individuals who fly furthest and most often.
> -   Increased government investment in low carbon buses and trains.
> -   An early shift to electric vehicles.
> -   An urgent ban on selling heavily polluting vehicles such as SUVs.
> -   Grants for people to buy low-carbon cars.
> -   A reduction in the amount we use cars by 2–5% per decade.
> -   Making wind and solar energy a key part of how the UK reaches net zero.
> -   Greater reliance on local produce and local food production.
> -   A change in diet – driven by education – to reduce meat and dairy consumption by between 20% and 40%.
> 
> [UK climate assembly: tax frequent flyers and ban SUVs as part of Covid recove&#x2026;](https://www.theguardian.com/world/2020/sep/10/uk-climate-assembly-economic-recovery-from-covid-must-tackle-global-heating) 

Wonder how many of these have been put in place.  The recent [[UK energy strategy]] didn't honour the centrality of wind and solar.

