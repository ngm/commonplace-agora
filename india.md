# India

> in a world powered by solar power, India is indeed blessed. More sunlight energy falls on India than on any other nation on Earth
> 
> &#x2013; [[The Ministry for the Future]]

