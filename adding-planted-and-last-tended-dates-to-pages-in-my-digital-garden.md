# Adding planted and last tended dates to pages in my digital garden



## Why

It's a nice practice to display the date that a page was created, and, more importantly, last modified, in your digital garden.  It gives the reader a clue as to the status of the information contained in the page.


## How

I already have 'This page last updated: &#x2026;' at the very bottom of every page.

But I'd prefer it right at the top.  And I'd also like to indicate when the page was created, too.  I don't want it to be be too prominent/distracting, but at the same time I have some pretty old pages knocking around now and I'd like people to be aware that they might be outdated.

It'll be a matter of (a) pulling the times out from somewhere; and (b) being able to render them.


### Pulling times out

I'm currently using the date value for modified time that you get when you use the `:with-date` flag in org-publish.  But it doesn't provide creation time.

I have ctimes and mtimes on all files(?), courtesy of the [[org-roam-timestamps]] package. So in theory I can use those.
They are file-level properties name `ctime` and `mtime`.  As far as I can tell, they don't exist in the info plist that's passed around during the export process.

So, for now, I've cobbled together a way to pull them out from the buffer using `org-property-values`.

```emacs-lisp
(let* ((ctime-property (car (org-property-values "ctime")))
       (mtime-property (car (org-property-values "mtime")))
       (ctime-date (if ctime-property
		       (car (split-string ctime-property))
		     nil))
       (mtime-date (if mtime-property
		       (car (split-string mtime-property))
		     nil))
       )
```


### Rendering them

So the rendering is going to be in my override of `org-html-template` (of which I have a note to self saying that I shouldn't really be overriding it&#x2026;)

As a simple example:

```emacs-lisp
(defun commonplace/format-date (date-str)
  (let ((year (substring date-str 0 4))
	(month (substring date-str 4 6))
	(day (substring date-str 6 8)))
    (format "%s/%s/%s" day month year)))

(defun commonplace/org-html-template (contents info)
  (let* ((ctime-property (car (org-property-values "ctime")))
	 (mtime-property (car (org-property-values "mtime")))
	 (ctime-date (if ctime-property
			 (car (split-string ctime-property))
		       nil))
	 (mtime-date (if mtime-property
			 (car (split-string mtime-property))
		       nil))
	 )
    (concat "<html>"
	    (if ctime-date (concat "ctime: " (commonplace/format-date ctime-date)))
	    " "
	    (if mtime-date (concat "mtime: " (commonplace/format-date mtime-date)))
	    "</html>")))
```

Check out my publish.el for the real function that I'm using.

