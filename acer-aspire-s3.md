# Acer Aspire S3

One of the [[19 broken laptops]] from Graham.


## Basics

Brand
: Acer

Model
: Aspire S3 (MS2385)


## Problems

-   Powers on but reports 6GB RAM
-   No hard drive
-   Few screws missing
-   Dead key?


## Specs

-   Core i5-4200U


## Resources

-   Also seems to be referred to as the S3-392G, which is on iFixit: https://www.ifixit.com/Device/Acer_Aspire_S3-392G
    -   has a decent teardown link, but it stops at keyboard and screen

