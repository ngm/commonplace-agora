# Freedom

> Before the dominance of a liberal understanding of negative liberty, emancipatory groups strived for a conception of freedom as collective self-determination
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Freedom in this sense is understood as an ongoing collective struggle and must be practised rather than enjoyed as a passive condition
> 
> &#x2013; [[Platform socialism]]

