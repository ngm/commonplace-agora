# Issue when updating spacemacs: Failed to checkout 'evil-impaired'

Updated [[spacemacs]] today, and then updating the packages, got an error about 'evil-unimpaired'.


## The problem

-   `git pull` in .emacs.d to get the latest spacemacs develop branch - no problems
-   `SPC f e D` to get my config and the template config in sync - no problems
-   `SPC f e U` to try and update packages - problem.
    -

> condition-case: Failed to checkout ‘evil-unimpaired’: ‘Creating directory: No such file or directory, _home/neil_.emacs.d/.cache/quelpa/build/evil-unimpaired/’


## The fix

A few other people seem to have had a similar problem.

-   [syl20bnr/spacemacs#10645 Error installing quelpa packages; complaining missin&#x2026;](https://github.com/syl20bnr/spacemacs/issues/10645)

-   check you're on `develop` branch and not `master` (I already was)
-   try removing .cache and starting again
    -   error still occurs for me
-   I didn't see this solution listed anywhere else, but `mkdir -p .cache/quelpa/build/evil-unimpaired` seemed to fix it for me.

Afterwards I'm still getting this warning:

> &#x2013;&gt; Warning: cannot update 2 package(s), possibly due to a temporary network problem: phpcbf php-auto-yasnippets

But let's leave that for now.

