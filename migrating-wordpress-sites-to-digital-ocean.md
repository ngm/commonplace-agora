# Migrating WordPress sites to Digital Ocean

Here's a rough outline of the steps I do at the moment to migrate a site based on a Duplicator backup.  


## Notes

It's all fairly laid back, until you get to the point of setting up Let's Encrypt, because you have to have the DNS pointing at the site before you can get the cert.  

If you have access to it, one way to do that is with a copy of an existing LE set of certificates, and set that up in advance.


## Steps

-   First, if not done already [[provision a WordPress server at Digital Ocean]]
-   copy latest backups to the WordPress server
    -   [[Duplicator]] Pro copies these to Google Drive, and I use [[rclone]] to pull them down from there
-   set up the site in Apache
    -   usually make a copy of what's in an existing .conf - should probably have a template file though
    -   check that it works on port 80
        -   I usually do this with an edit to /etc/hosts, so I can browse the new site without making any live DNS changes
-   setup DB
    -   create database &lt;dbname&gt;;
        -   &lt;dbname&gt; : &lt;shortname&gt;_&lt;8randomchars&gt;
    -   create user &lt;user&gt;@localhost identified by '&lt;pass&gt;';
        -   &lt;user&gt;: &lt;shortname&gt;_&lt;8randomchars&gt;
        -   &lt;pass&gt;: 32 char pass from 1password
    -   grant all on &lt;dbname&gt;.\* to &lt;user&gt;@localhost;
-   install while on http
    -   run the Duplicator installer.php
-   update the DNS
-   run certbot
-   check email settings
    -   send a test email
-   readd Duplicator Pro settings
    -   weirdly, of all things, I lose the Duplicator Pro storage and schedule settings, and have to set them up again


## Automation

Would be nice to have this automated via e.g. [[Ansible]].


## Simple Apache .conf template

This will get amended when running certbot &#x2013;apache.

```nil
<VirtualHost *:80>
	ServerAdmin {{ admin_email }}
	ServerName {{ domain }}
	ServerAlias www.{{ domain }}

	DocumentRoot /var/www/{{ domain }}

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	<Directory /var/www/{{ domain }}>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride all
		Order allow,deny
		allow from all
	</Directory>
</VirtualHost>
```

