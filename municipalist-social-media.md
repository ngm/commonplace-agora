# Municipalist social media

[[Municipalist]] [[social media]].

A way of helping people and organisations to transition away from [[legacy social media]].  Providing easy on-ramps to P2P or federated social media.  Municipally-hosted platforms that any resident can get an account on.  Any local business, too.

Municipal instances.  Allows for local residents and businesses to have a friction-free place to microblog without maintaining own infrastructure.  Based on a protocol that works P2P/federated.  Anyone is free to move to any other hosting option anywhere else should they ever wish to.  Data sovereignty.

Councils work with local tech co-ops to maintain / host.  Co-ops contribute to upstream software development, so that it grows.  That combined labour multiplied across munipalities around the world allows for genuine alternative to big tech firms.  But it's not state-controlled, it's via a federation of municipalities.

Refer to [[A Cooperative Vision for Technological Innovation w/ Dan Hind]] for some thoughts.

[[The Preston Model]] approach of having anchor institutions invest in local community providers might make senes here.

