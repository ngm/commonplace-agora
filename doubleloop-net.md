# doubleloop.net

URL
: https://doubleloop.net

doubleloop.net is my personal website.


## Name

I've had the domain since 2014-03-03.

Before that I used noodlemaps.net.  I can't remember why I switched.

I'm going off the name doubleloop.net a bit, and preferring noodlemaps again.  But anyway.


## What it currently is

It's a personal website, with three main parts:

-   stream
-   garden
-   about

It's an [[IndieWeb]] site.  With the social interaction bells and whistles of that.


## What I'd like it to become

I'd like it to feel more like one integrated space.  There's a bit of a disjunct between stream and garden at the moment.  It's not too bad, but I'd like to tighten it up.

I'd like to move from the commonplace subdomain for my notes, to a subfolder, I think.  


### branding

I'd like to redefine the strapline: 'tech + politics + nature + culture'.  That's a reasonably accurate list of what I post about, but I'd like something a bit more related to my unique synthesis.

I'd like it to be more visual, and to immediately have some visual signifiers of what I am about.

