# knowledge commoning

[[Commoning]] a [[knowledge commons]].

If [the number of Evergreen notes written per day is the leading indicator for yourself activity as a knowledge worker](https://notes.andymatuschak.org/Evergreen%20note-writing%20as%20fundamental%20unit%20of%20knowledge%20work), what's the equivalent for knowledge commoning?

Perhaps the same.  But a number for numbers' sake is not a great metric.  What purposes are your notes/claims/statements for?  What problem are they solving?  What task are they helping to coordinate?  Who are they helping?

> If you do go all the way, concentrate on writing out just two short solid ideas every day (Luhmann averaged about 6 per day and Roland Barthes averaged 1 and change).
> 
> &#x2013; [[Reframing and simplifying the idea of how to keep a Zettelkasten]]

