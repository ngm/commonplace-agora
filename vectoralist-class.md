# Vectoralist class

As per [[McKenzie Wark]], part of [[Vectoralism]] - information has empowered a new kind of ruling class - the vectoralist class.

> Information, like land or capital, becomes a form of property monopolised by a class of vectoralists, so named because they control the vectors along which information is abstracted, just as capitalists control the material means with which goods are produced, and pastoralists the land with which food is produced.
> 
> &#x2013; [[A Hacker Manifesto]]

<!--quoteend-->

> I call the emerging ruling class the vectoralist class, because their class power derives from ownership and control of the vector of information
> 
> &#x2013; [[Capital is Dead]]

The vectoralist class sits as a layer built on top of the [[capitalist]] class.

And are antagonists to the [[hacker class]].

> If one takes a look at the top Fortune 500 companies, it is surprising how many of them are really in the information business. I don’t just mean the technology and telecommunication companies like Apple or Google or Verizon or Cisco or the drug companies like Pfizer. One could also think of the big banks as a subset of the vectoralist class rather than as “finance capital.”
> 
> &#x2013; [[Capital is Dead]]

