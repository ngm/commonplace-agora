# Black Panther Party

As via [[The Black Panthers: Vanguard of the Revolution]]

Start by arming themselves and protecting black people from police intimidation.

"Black panther will not attack.  The first time it is attacked, it will retreat.  If it is continually attacked, it will fight back."

Food for schools.

The Panther paper was key to spreading the message to places they wouldn't have been able to go otherwise.

[[Emory Douglas]] artwork.

Nixon elected.  J. Edgar Hoover cracks down further on the Panthers.  "Justice is merely incidental to law and order"

Panther 21.

Trials consume most of the party's energy. Puts people off joining.

[[Fred Hampton]].  You can jail a revolutionary, but can't jail a revolution.  Building a broad-based coalition.  **That** was a threat.

Hoover afraid of a black messiah.  Hampton assassinated.

Raid in Los Angeles.

Huey P. Newton released.

Tension in the party.  Stoked by FBI.  Split.

Bobby Seale runs for mayor.  Chapters all come to Oakland.  Left a void across the country.

Huey P. Newton goes off the rails.

Party falls apart.

