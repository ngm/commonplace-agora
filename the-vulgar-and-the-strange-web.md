# The vulgar and the strange (web)

In the frame of digital urban planning, I think this quote from [[Jane Jacobs]] is very [[IndieWeb]].

> What a wonderful challenge there is! Rarely has the citizen had such a chance to reshape the city, and to make it the kind of city that she likes and that others will too. If this means leaving room for the incongruous, or the vulgar or the strange, that is part of the challenge, not the problem. Designing a dream city is easy; rebuilding a living one takes imagination.
> 
> &#x2013; Jane Jacobs

Says O’Shea:

> We need to protect space in our minds for the vulgar and the strange, for the unpredictable experiences of living free from the influence of commercialism. Like the flâneur or flâneuse, we should aim to cultivate curiosity through this liberated lens.

