# Cooperation Jackson

> Cooperation Jackson is one of the most radical, political cooperative projects in the world. It’s also a beacon for radical Black cooperativism. 
> 
> &#x2013; [Cooperation Jackson – DisCO.coop](https://disco.coop/labs/cooperation-jackson/)

<!--quoteend-->

> Cooperation Jackson is at the forefront of [[eco-socialist]] organizing to create a new society and economy from the bottom up
> 
> &#x2013; [["It's Eco-Socialism or Death"]]

<!--quoteend-->

> We believe that the participatory, bottom-up democratic route to economic democracy and [[eco-socialist]] transformation will be best secured through the anchor of [[worker self-organization]], the guiding structures of [[cooperatives]] and systems of [[mutual aid]] and communal solidarity, and the democratic ownership, control, and deployment of the ecologically friendly and labor liberating technologies of the [[fourth industrial revolution]].
> 
> &#x2013; [[Jackson Rising]]

