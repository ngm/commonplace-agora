# Linear programming

> The method was then independently reinvented in the United States by Tjalling Koopmans and by George Danzig, who while working on transport and allocation problems for the US Airforce during the war coined the phrase 'linear programming'
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> These are known as ‘linear’ equations, because if graphed they produce straight lines, and it is a property of linear equations that you can only solve them if you have as many equations to work with as there are variables
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Otherwise, they are ‘undetermined’ – there are an infinite number of possible solutions, and no way to decide between them. 
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Linear programming was not only a quintessentially socialist kind of mathematics, ‘characterized by a constant overlap of theory and practice’, it also offered a new kind of socialist political economy
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Linear programming offered a systematic way to allocate resources, so that it optimized some metrics of overall national well-being
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> the method is ubiquitous in contemporary applied mathematics, including in planning renewable energy systems
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Rather than relying on a Platonic elite of logicians, Neurath thought that a visual language could democratize reason by making the essence of an economic problem apparent to non-experts
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> There were two main currents that shaped planning debates in the Soviet Union over the following decade: the theory of mathematical optimization (e.g., linear programming) and the cybernetic theory of control, built around differential equations
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> For all its pedagogical and democratic value, linear programming alone will not suffice to plan something as complex as the global economy
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Kantorovich’s linear programming will not be enough in itself to create a global in natura economy
> 
> [[Half-Earth Socialism]]

