# third industrial revolution

> The Third Industrial Revolution (3IR), also known as the [[digital revolution]], started in the 1960s, but exploded in the late 1980s and 1990s, and is still expanding today.  This revolution refers to the advancement of technology from analog electronic and mechanical devices to the digital technologies we have now.  The main technologies of this revolution include the personal computer, the internet, and advanced information and communications technologies, like our cell phones.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]

The beginning of the [[Information Age]].

-   "refers to the advancement of technology from analog electronic and mechanical devices to the digital technologies we have now"

-   computers
-   internet
-   mobile phones

