# David Bollier

Scholar and activist with a focus on the [[Commons]] and [[commoning]].

Co-author with [[Silke Helfrich]] of the amazing [[Free, Fair and Alive]].

Used to work for [[Ralph Nader]].

