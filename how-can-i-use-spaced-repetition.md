# How can I use spaced repetition?

-   [How to build a spaced repetition system in Roam Research - Ness Labs](https://nesslabs.com/spaced-repetition-roam-research)
-   [Spaced repetition may be a helpful tool to incrementally develop inklings](https://notes.andymatuschak.org/z7iCjRziX6V6unNWL81yc2dJicpRw2Cpp9MfQ)

