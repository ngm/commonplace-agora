# ABS

acrylonitrile butadiene styrene

> Like almost every form of plastic on the planet, ABS is made from petrochemicals that are derived from petroleum, the fossil fuel we commonly refer to as crude oil. The substance materialized over millions of years as fossilized organisms like zooplankton and algae were covered by stagnant water and further layers of these dead animals and plants. Try to imagine not only how slow that process is (geologists call this “deep time”) but also the near-instantaneous speed at which the oil was extracted from the earth. Now consider its carbon residue just sitting in the atmosphere, slowly helping make the planet hotter. As I stared at the plastic, these head-spinning thoughts flashed through my mind.
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

