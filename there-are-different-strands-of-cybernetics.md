# There are different strands of cybernetics

Interesting to learn about the different strands of [[cybernetics]] from [[Cybernetic Revolutionaries]].

There is/(was?) a kind of top-down militaristic command and control strain (Wiener-ish?), and a more holistic, distributed complex systems strain (Beer-ish?).

