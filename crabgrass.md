# Crabgrass

> Crabgrass is a web application designed for activist groups to be better able to collaborate online. Mostly, it is a glorified wiki with fine-grain control over access rights.
> 
> &#x2013; [liberate / crabgrass · GitLab](https://0xacab.org/liberate/crabgrass) 

