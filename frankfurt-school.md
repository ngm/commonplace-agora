# Frankfurt School

> The other inheritor of the unorthodox Left was the Frankfurt School, whose theorists criticized the destruction wrought by the mindless conquest of nature. [[Adorno]], [[Horkheimer]], and [[Benjamin]], as well as [[Alfred Schmidt]] and [[Herbert Marcuse]], all contributed to the foundations of [[eco-socialism]]
> 
> &#x2013; [[Half-Earth Socialism]]

