# Web 1.0

> In contrast, Web 1.0 required users to manually code, design, and manage their own spaces on the net and interact with others on the web in ways such as clicking links, copying an email address, or manually linking to other webpages.
> 
> &#x2013; [[404 Page Not Found]]

<!--quoteend-->

> this Web 1.0 relationship, which joined the user to a new, exciting, naive, futuristic, and quite limited internet as a creative medium in and of itself
> 
> &#x2013; [[404 Page Not Found]]

<!--quoteend-->

> The Web 1.0 was essentially read-only. Organizations started to create content and connect them via the HTML hyperlinks: click on the underlined (mostly) blue link, and it will take you to a different document or Web page. 
> 
> &#x2013; [[Can the Real Web 3.0 Please Stand Up?]]

