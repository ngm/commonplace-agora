# Problem logging in to YunoHost after upgrade

Having an issue after upgrading [[YunoHost]] from 4.0.8.3 to 4.1.4.4. I can't log in to the SSO section.

It just reloads the page - no suggestion that password is wrong.

-   With an unknown user
    -   "Wrong username or password"
-   With my user, and the wrong password
    -   "Wrong username or password"
-   With my user, and the correct password
    -   just reloads the login screen
-   I can still log in to admin fine.
-   can't see anything relevant in the permissions section
-   if I create a new user, same issues.
-   updated the server


## Logs

-   nothing in /var/log/nginx/ssowat.log
-   can't see anything relevant in /var/log/nginx/site.domain-access.log or /var/log/nginx/site.domain-error.log

-   the accesses are there, but no errors


## Misc

-   Updated timezone while I was at it in case some time mismatch thing

-   the timezone was Europe/Berlin, which is wrong for me

-   `sudo timedatectl set-timezone Europe/London`


## Resolution

-   Ah, fixed it!  Deleting all the cookies for my yunohost domain sorted it out.

