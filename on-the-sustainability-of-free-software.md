# On the Sustainability of Free Software

URL
: https://fsfe.org/freesoftware/sustainability/sustainability.html

Authors
: Erik Albers / FSFE
    
    > A definition of [[software sustainability]] is introduced and its characteristics. It is shown how the inherent characteristics of [[Free Software]] are sustainable as well as their impact on IT infrastructures. [[Software obsolescence]] is explained and the possibilities of using Free Software to save natural resources by extending hardware usage lifetime and through energy consumption savings. Finally, four politically necessary directives for a more sustainable digital society are outlined.  
    
    <!--quoteend-->
    
    > the [[FSFE]] demands the publication of a device's underlying source code under a free licence 30 at the end of support for any software necessary to run or modify the initial functioning of the device.

