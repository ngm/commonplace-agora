# Anti-authoritarian networking

-   from [[Information Civics]]

Ways in which you can design a network system to reduce imbalances of authority.

> This makes the design of the network a civic practice as much as it is technical. The developers must consider the political ramifications of their designs.
> 
> &#x2013; [[Information Civics]]

[[Anti-authoritarianism]].

