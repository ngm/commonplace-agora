# Brian Merchant, "Blood in the Machine: The Origins of the Rebellion Against Big Tech"

A
: [[podcast]]

Found at
: https://newbooksnetwork.com/blood-in-the-machine

Great chat about Brian's book on the [[Luddites]].

Brian highly recommends E.P Thompson's book [[The Making of the English Working Class]].

[[Luddism]] was a tactic of last resort. They had tried other means such as parliamentary route, but been rejected.

Luddism was one step beyond [[strike action]] - rather than just withholding labour, actively destroying capital. [[direct action]]. [[Sabotage]].

Luddite tribunal.

