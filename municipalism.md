# municipalism

Generally speaking, devolving power to local levels.

But also see [[Libertarian municipalism]] for a more specific political stance.

> building popular assemblies to make decisions at the community level.
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

<!--quoteend-->

> There are those who suggest municipalism means simply ‘local self-government’, which implies that cities and towns should have the capacity to decide on their own affairs. Others claim it is more of a political strategy that prioritizes local action over other levels. 
> 
> &#x2013; [[Which municipalism?  Let's be choosy]]

[[Popular assembly]]

