# general intellect

> —an assemblage of accumulated technical knowledge that could be an anticipation of the digital networks of the internet
> 
> &#x2013; [[Breaking Things at Work]]

