# The UK should have a national programme of home insulation

[[Insulation]].


## Because

-   The UK has some of the most poorly insulated housing stock in Europe
    -   It uses more energy to keep us warm throughout the winter.
-   Insulation could more than halve a household's energy bills and annual emissions

> huge chunk of this money is paid by households for heat energy produced from fossil fuels that is wasted by poor insulation. 
> 
> &#x2013; [[End of the World, End of the Month - One Fight]]

