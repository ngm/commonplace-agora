# Facebook outage 2021

It's fascinating that it may have taken longer to resolve because engineers couldn't get to where they needed to be to fix the thing that was broken, because of access cards that relied on the thing that was broken.

Also the amount of knock-on effect and the fragility of some key bits of the internet is fascinating/worrying - this thread on Hacker News is good - https://news.ycombinator.com/item?id=28751441 - basically millions of devices around the world doing a denial of service by constantly asking 'where is facebook.com' and slowing down services for everyone else.

And then sites that rely on 'Log in with Facebook' (edited) 

And parts of the world that can only connect via Facebook Free Basics.


## Knock-on effects

> Yikes! Since the DNS records of Facebook/Instagram/WhatsApp can't be resolved anymore, every device with one of their apps installed is now actively DDoSing recursive DNS resolvers. This may cause problems&#x2026; for the entire internet.
> 
> https://mastodon.social/@fribbledom/107044316939773613


## Articles

-   [What Happened to Facebook, Instagram, &amp; WhatsApp? – Krebs on Security](https://krebsonsecurity.com/2021/10/what-happened-to-facebook-instagram-whatsapp/)
-   [Facebook Is Down - Schneier on Security](https://www.schneier.com/blog/archives/2021/10/facebook-is-down.html)
-   [Understanding How Facebook Disappeared from the Internet](https://blog.cloudflare.com/october-2021-facebook-outage/)

