# Doing the Doughnut.tech

URL
: https://doingthedoughnut.tech/

Looks great - workshops around digital tech sector's impact on global sustainability.

> This is great progress, but the industry is in danger of overlooking the big, more difficult questions.
> 
> Questions like: what exactly are those paradigms, systems and root causes that have got us into a rapidly warming climate? And how has this industry been culpable? And how does it continue to be culpable? What do we need to change?

<!--quoteend-->

> We are part of an industry that can afford to do more. An industry that can look much more deeply at the part it plays in perpetuating the systems and root causes of climate breakdown. 

<!--quoteend-->

> We’re using the excellent [[Doughnut Economics]] framework as a vehicle to generate these discussions and understanding.

