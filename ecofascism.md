# ecofascism

> Ecofascism is present in the environmental movement, from [[Garrett Hardin]] to Dave Foreman of [[Earth First!]], and is latent in the eco-Malthusianism of mainstream environmentalism
> 
> &#x2013; [[For a Red Zoopolis]]

