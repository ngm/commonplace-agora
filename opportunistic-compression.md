# opportunistic compression

> summarizing and condensing a piece of information in small spurts, spread across time, in the course of other work, and only doing as much or as little as the information deserves.
> 
> &#x2013; [Progressive Summarization: A Practical Technique for Designing Discoverable N&#x2026;](https://fortelabs.co/blog/progressive-summarization-a-practical-technique-for-designing-discoverable-notes) 

