# Revolutionary Letters

A
: [[book]]

Author
: Diane di Prima

> The American leftist poet Diane di Prima wrote her ‘Revolutionary Letters’ for over forty years, filling them with both advice and anger
> 
> &#x2013; [No One Way Works](https://tribunemag.co.uk/2022/03/no-one-way-works)

