# Neither Vertical Nor Horizontal

A
: [[book]]

Subtitle
: A Theory of Political Organisation

Author
: [[Rodrigo Nunes]]

[[Political organisation]].

![[images/neither-vertical-nor-horizontal.jpg]]

> Nunes develops a grammar that eschews easy oppositions between ‘[[verticalism]]’ and ‘[[horizontalism]]’, and offers a fresh approach to enduring issues like spontaneity, leadership, democracy, [[strategy]], [[populism]], [[revolution]], and the relationship between movements and parties.
> 
> &#x2013; [Verso](https://www.versobooks.com/books/3810-neither-vertical-nor-horizontal) 

Has some good (and good faith) [[critiques of horizontalism]].  And some good also good faith [[critiques of verticalism]].


## Types of action

-   [[aggregate action]] - "the ways in which people act together outside of organisations"
-   [[collective action]] - "people not only perceive themselves as participating in a broader common identity but also intentionally come together and engage in processes of deliberation, planning, assessing, intervening, and so forthhow they do so with or without them"
-   [[distributed action]] - "the common space in which collective and aggregate action combine, communicate, relate and establish positive and negative feedback loops with one another"


## Raw notes

Plonking them here until I process and probably move to other related pages.


### Introduction

> in 2011, Alain Badiou wrote that, ‘however brilliant and memorable’ in themselves, they ultimately came up against the ‘universal problems of politics that remained unresolved in the previous period. At the centre of which is to be found the problem of politics par excellence – namely, organization

<!--quoteend-->

> Jodi Dean, herself a prominent advocate of a return to communism and the party-form, has summarised the question thus: ‘the idea of communism pushes toward the organization of communism

<!--quoteend-->

> Either they call for a search for new forms but are frustratingly reticent when it comes to spelling out what those might be; or they are in fact pleas for a return to some redefined notion of the party, the contours of which tend nonetheless to be left equally vague

Anarchist Cybernetics is a non-vague attempt. 

> to drag organisation out of that pseudo-conceptual state and to dissolve its presumed synonymy with the party-form are certainly two goals to which this book aspires

<!--quoteend-->

> Instead of prescribing a determinate result, in short, it should set out to specify as precisely as possible the variables involved in the problem, mapping the choices, trade-offs and thresholds that determine the points at which different possible solutions diverge from one another

Sounds like [[a pattern language for political organisation]].

> is not just that the party ceases to be presumed as the telos of all organisation, its most advanced form and the point towards which all paths converge. ‘Organisation’ comes to refer to a much broader range of phenomena, many of which are not contained in any single organisation, let alone one of a specific kind

<!--quoteend-->

> The second principle I set myself for this book was therefore not to make intentional political organisation into an ‘empire within an empire’, but to conceive it as part of, and in fundamental continuity with, ‘organisation’ in the broadest possible sense – that is, natural organisation (if we understand ‘nature’ in a Spinozan sense)

<!--quoteend-->

> Finally, picturing political organisation as a branch of a more general theory of (self-)organisation enables us to seek inspiration in different fields of knowledge that deal with self-organised processes

[[cybernetics]], [[complex systems]], perhaps.

> To avoid being just another verticalist or horizontalist arguing their position, it was necessary to invent some other perspective to occupy

<!--quoteend-->

> After all, even if there is no ‘right’ way to organise in absolute terms, there are still better and worse choices to be made here and now

<!--quoteend-->

> Thus, for example, an analogy between ant colonies and human societies will argue that ‘if one ant began to somehow assess the overall state of the whole colony, the sophisticated behaviour would stop trickling up from below, and swarm logic would collapse’.12 But to say this is not just oblivious to the fact that humans differ from ants (as far as we know) in that they form their own notions of what constitutes justice and a good life; it also ignores that statements like ‘individuals in a society should refrain from assessing it as a whole’ are themselves global assessments of a society.

Good critique of over-reaching use of analogies with nature for political organising.

> The second flaw, then, concerns the practical consequences of this lack of self-reflexivity. If we believe ourselves in possession of a knowledge that sets legitimate limits to the actions of agents – even if this is a knowledge that according to our own premises no agent should legitimately have – we are justified in taking actions that, according to our premises, no agent should take

<!--quoteend-->

> Self-organisation’ is not some transcendent reality existing apart from our actions, like some blind logic that will unfold independently of what anyone does or a benign providence that our best intentions can only hinder. Precisely because it is contingent on the actions of the agents that participate in it, its fate cannot be determined in advance. Self-organisation is the emergent effect of what those agents do and nothing more. That includes ‘local’ decisions as much as efforts to influence the behaviour of the system at a higher scale. For exactly that reason, it makes no sense for agents to renounce acting on anything but the smallest scale on a priori grounds

<!--quoteend-->

> My third principle for this book was therefore that it should provide an account of self-organisation not as seen ‘from above’ – from a supposedly objective perspective – but as seen from the inside

<!--quoteend-->

> That is, by agents with limited information and capacity to act, for whom the future is unknown and open, and who wish to increase the probability of certain outcomes over others without ever having absolutely certain knowledge of what might be the best way to do so

<!--quoteend-->

> repeating the gesture that [[second-order cybernetics]] had made in relation to first-order cybernetics, and that Lenin and Luxemburg had made in relation to Second International orthodoxy

<!--quoteend-->

> In a nutshell, this gesture consists in resituating the observer in the world regarding which an observation is made, exposing the falsehood of assuming a contemplative stance

<!--quoteend-->

> In Lenin and Luxemburg, it entailed arguing that, dialectically understood, historical materialism was not a scientific prognosis of how history would pan out independently of what anyone did, but an instrument to guide the actions of those who made history happen


#### Wariness of party-form

> There are, of course, perfectly good reasons why people grew so wary of action and organisation above a certain scale that they began to rationalise that mistrust into arguments demonstrating their superfluousness


#### Organising for climate catastrophe

> The prospect of global environmental catastrophe makes both building a single global collective force and hoping that the aggregate effects of countless local actions will eventually coalesce into a solution appear as equally far-fetched solutions

<!--quoteend-->

> tackle a problem of that magnitude and complexity, the most plausible alternative seems to be some kind of distributed action combining organisation at different levels and scales

<!--quoteend-->

> Chapter 3 travels further back in time in order to trace the main ways in which the idea of revolution has changed since the eighteenth century

<!--quoteend-->

> Thus, talk of ‘revolution’ disappears altogether, or the word is associated with small-scale modifications that in the past would have been seen at best as parts of a revolution

<!--quoteend-->

> The point is not, however, to dismiss the notion of social self-organisation, but to reframe it in the only way in which we can experience it: from the inside

<!--quoteend-->

> politics that implicates itself subjectively; a politics in the first person plural or a politics with the subject in

<!--quoteend-->

> The antidote to the fantasies of omnipotence that plagued the revolutionary tradition cannot be to simply renounce our power to influence the course of events in the hope that history or nature will be on our side. Rather, it must consist in situating political subjects within a world inhabited by other perspectives and agents that are connected to one another by complex causal circuits that exceed the calculating capacities of any single one of them

<!--quoteend-->

> chapter 5 opens with a discussion of the concept of organisational ecology

<!--quoteend-->

> we cannot apply to an ecology the same logic we apply to an organisational space with defined boundaries like a party or an assembly

<!--quoteend-->

> In order to explain the logic according to which an ecology operates, I introduce in chapters 5 and 6 the concepts of distributed leadership, vanguard-functions (not to be confused with their counterparts in Marxist theory), platforms and organising cores

<!--quoteend-->

> Chapter 7 delves into the current debate on populism to make the case that what matters the most about it is not the question of populism as such, but a problem that it has helped put back on the agenda. I call this the problem of fitness; it concerns the qualities that a political project must have in order to gather support and produce change within a determinate conjuncture, instead of merely demarcating a position that neither resonates widely nor has any immediate applicability

<!--quoteend-->

> Changing the quantity and the quality of the local input into those networks seemed to require engaging in modalities of political action (workplace and community organising, local base building, and so on) that many in the ‘horizontalist’ camp had pronounced outdated and rejected as ‘Leninist'


## Chap 1: Towards a Theory of Political Organisation


### The Meanings of Organisation

> One is inflexibly substantival: an organisation is some concrete assemblage of people, structures, practices, procedures, resources, roles, identities, analyses, directives and so on. An organisation might be a party, a trade union, a workers’ council, a more or less formally structured campaign or social movement; a collective, a network, an affinity group. Their contours and membership may be more or less defined, their internal workings more or less constant. Above a certain threshold of stability over time, which is itself dependent on the scale of analysis, all of these can be understood as constituting ‘organisations

<!--quoteend-->

> we can say that the problem is that ‘spontaneous’ cannot function as the opposite of ‘organised’ because even what we describe as spontaneous is organised in some way

<!--quoteend-->

> Organisation thus entails a nested structure of ever-expanding relations in which what counts as an organisation on one level can be taken as an element on a higher level: atoms organise as molecules, molecules as proteins, proteins as cells, cells as organisms, organisms in ecosystems, and so forth

<!--quoteend-->

> This allows us to see why, from what Bogdanov calls ‘the organisational point of view’ – ‘the only monistic understanding of the universe’ – everything is organised, and the universe itself appears as ‘an infinitely unfolding fabric of all types of forms and levels of organization’, from the minutest scale to entire star systems which, ‘in their interlacement and mutual struggle, in their constant changes, create the universal organizational process, infinitely split in its parts, but continuous and unbroken in its whole

[[Potestas and potentia]]

> Yet each individual’s potentia on its own is not much, and certainly not enough to face down potestas

<!--quoteend-->

> It is therefore imperative that individuals come together, the capacity to act of each multiplying the capacity of all others. That is why the subject of politics is always collective

<!--quoteend-->

> in order to overcome the resistance of the powers that be, any major historical transformation will always require the confluence of a large number of individuals – a collective subject or agent, in other words


### Acting Together

> most part as the aggregate result of these manifold small changes, and therefore as an example of the aggregate action of large numbers of individuals

<!--quoteend-->

> By contrast, collective action properly speaking would refer to those cases in which people not only perceive themselves as participating in a broader common identity – that is, as belonging to a collective subject – but also intentionally come together and engage in processes of deliberation, planning, assessing, intervening, and so forth

<!--quoteend-->

> What this means, then, is that a combination of aggregate and collective action is always required

<!--quoteend-->

> Movements are always nebulae or networks; it is only the degree to which they are centralised that varies

<!--quoteend-->

> just because every organisation has an outside that it relates to, but because the organisation itself, upon closer inspection, is decomposable into different parts – a nebula of collective and aggregate action, a network, an ecology.

Vis [[Viable system model]].


### Think and Act Global and Local

