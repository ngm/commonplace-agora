# leverage points

> how do we change the structure of systems to produce more of what we want and less of that which is undesirable?
> 
> &#x2013; [[Thinking in Systems]]

<!--quoteend-->

> These are points within a complex system, such as an economy, an ecosystem, or a community, where a small shift in one place can produce major changes elsewhere.
> 
> &#x2013; [[Leveraging Digital Disruptions for a Climate-Safe and Equitable World: The D<sup>2S</sup> Agenda]]

Leverage points as listed by Donella Meadows in [[Thinking in Systems]]:

-   12. Numbers—Constants and parameters such as subsidies, taxes, standards
-   11. Buffers—The sizes of stabilizing stocks relative to their flows
-   10. Stock-and-Flow Structures—Physical systems and their nodes of intersection
-   9. Delays—The lengths of time relative to the rates of system changes
-   8. Balancing Feedback Loops—The strength of the feedbacks relative to the impacts they are trying to correct
-   7. Reinforcing Feedback Loops—The strength of the gain of driving loops
-   6. Information Flows—The structure of who does and does not have access to information
-   5. Rules—Incentives, punishments, constraints
-   4. Self-Organization—The power to add, change, or evolve system structure
-   3. Goals—The purpose or function of the system
-   2. Paradigms—The mind-set out of which the system—its goals, structure, rules, delays, parameters—arises
-   1. Transcending Paradigms

with the caveat:

> But complex systems are, well, complex. It’s dangerous to generalize about them. What you read here is still a work in progress; it’s not a recipe for finding leverage points. Rather, it’s an invitation to think more broadly about system change.
> 
> &#x2013; [[Thinking in Systems]]

