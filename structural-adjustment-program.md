# Structural adjustment program

> The “structural adjustment programs” enforced by the [[World Bank]] on the developing countries caught in the debt crises at the end of the twentieth century set the conditions for what became the world order in the twenty-first century. These SAPs were instruments of the postwar American economic empire, which was unlike the older empires in that it did not insist on ownership of its economic colonies; it only owned their debts and their profits, no more than that. The best empire yet, in terms of efficiency, and the neoliberal order was all about efficiency, in its purest economic definition: the speed and frictionlessness with which money moved from the poor to the rich.
> 
> &#x2013; [[The Ministry for the Future]]

<!--quoteend-->

> So there was a reason it was called the [[Washington Consensus]]. Its SAP requirements, made of any country that wanted a bail-out in the form of further loans, came only by adhering to the following conditions: a reduction in public spending; tax reforms, especially reducing taxes on corporations; privatization of state-owned enterprises; market-based interest and currency exchange rates, with no government controls on these; a set of strong investor rights, so investors could no longer be given haircuts (the long hair provisions, so-called); and the massive deregulation of everything: market activities, business practices, labor and environmental protections.
> 
> &#x2013; [[The Ministry for the Future]]

