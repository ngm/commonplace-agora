# Climate Mao

One of the climate futures from from [[Climate Leviathan]]. This one if the climate crisis response was centrally planned by a non-capitalist nation state. (Or also a non-capitalist planetary body, I think?  Such as that suggested in [[Half-Earth Socialism]].)

I haven't read the book yet, but presumably Climate Mao has some aspects of [[Maoism]].

