# Community land trusts

> Community land trusts are a great way to decommodify land, take land off speculative markets permanently, and mutualise control and benefits of real estate. CLTs help keep land under local control and allow it to be used for socially necessary purposes (e.g. organic local food) rather than for marketable purposes favoured by outside investors and markets.
> 
> &#x2013; David Bollier, Stir to Action Issue 30

<!--quoteend-->

> Community Land Trusts (CLTs) and similar cooperative structures like this are grass roots organisations tackling local land injustices. They are a fair and transparent way of grappling control of houses, shops, industrial land and public spaces away from the forces of gentrification. Giving local groups ownership, a say and rights to the benefits of what goes on in our neighbourhood. 
> 
> -   [[Stokes Croft Land Trust]]

<!--quoteend-->

> A Community Land Trust (CLT) is a mechanism for democratic ownership of land by a local community. Land is taken out of the market and separated from the possibility of speculation so that the impact of land price inflation is removed, thereby enabling long-term affordable and sustainable local development.
> 
> &#x2013; [Help the Stokes Croft Land Trust purchase its first building and bring it int&#x2026;](https://www.fundsurfer.com/community-share/stokes-croft-land-trust) 

<!--quoteend-->

> Community Land Trusts allow local people to ‘manage the commons’ democratically. Compared to the dominance of private and public ownership of land, ‘common land’ in the UK constitutes under eight per cent of the land area and most of this is ‘waste land'.  Reclaiming and extending the ‘[[commons]]’ is possible when people act collectively.
> 
> &#x2013; [Help the Stokes Croft Land Trust purchase its first building and bring it int&#x2026;](https://www.fundsurfer.com/community-share/stokes-croft-land-trust) 


## History

> The first CLTs emerged from the [[Civil Rights Movement]] in the USA during the 1970s, and the idea has now spread round the world.
> 
> -   [[Stokes Croft Land Trust]]


## Examples

-   [[Stokes Croft Land Trust]]

