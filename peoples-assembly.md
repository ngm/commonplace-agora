# People's Assembly

> What validates the Assembly as an institution more than its staying power is its social weight, which is its ability to act as a “[[dual power]]” or counterweight to the policies and actions of the government and local and regional business interests (i.e. capital).
> 
> &#x2013; [[Jackson Rising]]

<!--quoteend-->

> During pre-revolutionary periods an Assembly can function as a genuine “dual power” and assume many of the functions of the government (state). Perhaps the best example of this over the past 10 years comes from the revolutionary movement in Nepal, where the revolutionary forces stimulated and organized Assemblies to act as a direct counterweight to the monarchial government and the military. Ultimately, resulting in the establishment of a constitutional democracy and a more “representative” legislative body. Another recent example comes from Chiapas, Mexico from 1994 until the mid- 2000’s when the Zapatistas were able create extensive zones of “self- rule” and “autonomous production” that was governed by Assemblies.
> 
> &#x2013; [[Jackson Rising]]

