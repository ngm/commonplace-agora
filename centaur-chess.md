# centaur chess

Where a human plays with an artificial intelligence against another human with an artificial intelligence.  Interesting this is something being championed by Gary Kasparov,  who was famously beaten by IBM's Deep Blue AI at chess a few decades back.  It opens up new possibilities where AI is complementary and not a replacement.

> These centaurs blend human intuition and creativity with the brute force calculation of moves and countermoves that computers do so easily.
> 
> &#x2013; [The Age of Centaurs | Psychology Today UK](https://www.psychologytoday.com/gb/blog/seeing-what-others-dont/201710/the-age-centaurs) 

