# Subconscious

I certainly like the sound of [[Noosphere]] and [[Subconscious]].  Collective knowledge management that is local-first and with data sovereignty.  Discovery, feeds and follows of others is on the way apparently, which would be a great set of features I think.

It sounds kind of like a slicker [[Agora]].  But I don't necessarily use 'slick' as meaning 'better'.  I love Agora's ramshackle and homebrew approach.

I haven't come across much from Noosphere that suggests it has any politics of any kind.  The beta announcement is signed off with "Let’s 10x humanity’s collective intelligence", which, absent of any political direction, is kind of problematic to me.

Though Gordon does say:

> We have planetary challenges ahead: climate change, global pandemics, mass extinctions, increasing geopolitical tension… We need to learn how to think together, with our planet, as a whole planet.
> 
> &#x2013; [[Tools for thought: the first 300,000 years]]

Which is more promising.

