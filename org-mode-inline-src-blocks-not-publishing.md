# org-mode inline src blocks not publishing

I'd like to know why the outputs of my little bits of executable code blocks aren't getting correctly generated when my garden gets published to the web.

e.g. I have one on [[Neil's Digital Garden]] that's supposed to list how many nodes in the garden.

```text
As of src_sh[:results raw]{date +"%A, %B %e, %Y"}, I have src_bash{echo -n $(sqlite3 ./org-roam.db "select count(*) from nodes")} nodes in my garden.
```

They work if I run them locally, just not during the remote build on my server.

I have `(setq org-confirm-babel-evaluate nil)` which means no prompting to confirm evaluation should occur.

In the logs for index.org I have
"Executing Emacs-Lisp unknown at position 153"

