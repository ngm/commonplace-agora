# neocolonialism

> broadly defined as the use of economic, political, cultural, or other pressures to control or influence other countries, especially former colonies.
> 
> &#x2013; [[How the North Plunders the South w/ Jason Hickel]]

<!--quoteend-->

> The practice of widespread crude, cruel, brute force that marked direct [[colonialism]] may not exist in the same exact form as it once did—but the outcome is still the same: mass extraction and exploitation from the [[Global South]] which has resulted in a staggering net transfer of resources, wealth, and labor to the [[Global North]].
> 
> &#x2013; [[How the North Plunders the South w/ Jason Hickel]]

<!--quoteend-->

> But what does it actually look like in practice? How is the imperial core still plundering and pillaging the periphery?
> 
> &#x2013; [[How the North Plunders the South w/ Jason Hickel]]

