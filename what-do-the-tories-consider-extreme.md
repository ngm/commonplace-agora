# What do the Tories consider extreme?

A
: [[podcast]]

Found at
: https://www.theguardian.com/news/audio/2024/mar/15/what-do-the-tories-consider-extreme-podcast

Part of
: [[Today in Focus]]

> [[Michael Gove]] is rewriting the government’s definition of ‘[[extremism]]’ but his actions have drawn criticism from across the political spectrum.

Aimed at stopping state funding of extreme groups. For some government-defined definition of extreme.

In reality mostly targeted at Islamicism, a hobby horse of Gove's.

Their definition:

> Extremism is the promotion or advancement of an ideology based on violence, hatred or intolerance, that aims to:
> 
> 1.  negate or destroy the fundamental rights and freedoms of others; or
> 2.  undermine, overturn or replace the UK’s system of liberal parliamentary democracy and democratic rights; or
> 3.  intentionally create a permissive environment for others to achieve the results in (1) or (2).
> 
> &#x2013; [Government strengthens approach to counter extremism - GOV.UK](https://www.gov.uk/government/news/government-strengthens-approach-to-counter-extremism)

