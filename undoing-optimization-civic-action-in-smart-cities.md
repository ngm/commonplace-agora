# Undoing Optimization: Civic Action in Smart Cities

URL
: https://bookshop.org/books/undoing-optimization-civic-action-in-smart-cities/9780300223804

Author
: Alison Powell

So I read a book by Alison Powell, who is a former guest on the podcast and was in fact a speaker at our first Fixfest event in 2017. And her new book that I read is: "Undoing Optimization: Civic Action in Smart Cities".  It's a detailed and principled critique of the notion of smart cities: what they are, who creates them, and who they are for.  And it's more broadly a look at the tension between corporate power, state power, and citizen power, with arguments that I think are equally applicable beyond the city.

Powell starts with a historical view of what a smart city actually is.   She traces this back to the origins of the idea when it was more around the notion of connectivity and widespread internet access, and she outlines the different ways in which cities could be made this way - sometimes more corporate - and sometimes more bottom up, like the [[Freifunk]] peer to peer mesh network in Berlin.  Although clearly against the idea of a corporatized smart city, I like how Powell fairly outlines the pros and cons of the different ways of organising - Freifunk, for example, while on the surface more egalitarian, could suffer from being exclusionary to those who didn't understand how it worked.

And so once abundant city-wide networking became commonplace, the smart city then moved on to a focus on the collection and use of '[[big data]]'.  This is the sensing and recording of city activities at scale and using this data for altering the way the city runs.  And like with the connected city, the data-based city can play out in a few different ways.  It could be a harvesting of data from above, where citizens every moves are tracked all in the name of efficiency and optimization (for example, tracking your public transport activity to power planning apps like [[CityMapper]]) Or city data can be more bottom-up, like the examples Powell gives of neighbourhood air monitoring in London, or the Bristol community group that monitored levels of damp in their homes to help get landlords to do something about it.

And so this tension, and particularly the idea of "[[Optimization]]" of a city is a key theme, and as the title suggests, something Powell wants to undo.  Optimization in the smart city is kind of that idea of constantly improving services - more efficiency, more performance, more speed, etc.  Which maybe superficially sounds fine - but Powell does a great job of surfacing the pitfalls of this constant optimization.  Who decides what to optimize?  Who gets excluded from the process?

And to quote Powell:

> It is relatively straightforward to optimize transportation or the collection of recycling but more difficult to optimize volunteering, knowing neighbors, or creating local capacity to take care of people in a crisis.

Related to this I really like Powell's idea around "friction".  This is part of her "undoing" of optimization.  Messy data, missing information, conflicting accounts or ideas, is not always a bad thing.  It can be these moments of uncertainty and friction that bring people together to discuss their city, and help build community resilience and work out a solution together.  Powell argues for "[[minimum viable datafication]]" of the city - just enough to surface the things that matter to a populace, and ideally with the citizen data held as part of a stewarded [[knowledge commons]].

So I really liked the book - it touches on some key themes we're interested in here at Restart - like community action, open data, digital access and the effect of technology on the environment and society.  I would say that it's a pretty dense read, and while there's lots of real world examples, it is more for those already with some grounding in these areas.  I think it could have had more actionable, practical advice on how to achieve a minimum viable smart city - and maybe some more discussion of some of the citizen participation platforms that already exist.  But all that said, it's a great provocation, with some good ideas on how to push back against optimization, and introduce some productive friction into the places where we live.  

Relation to Restart: just good enough data.  Sensors everywhere?

[[Civic action]] is discussed with nuance.  In general, the ability of citizens to be involved in the life of the city.  Not just co-opted by platforms.

Felt like a missed opportunity to discuss some of the explicitly defined [[citizen participation platform]]s.  Though they would still fall in to the critique of things like FixMyStreet and CycleStreets - perhaps exclusive in terms of who can access them, and only solving very particular defined set of problems.

[[Smart cities]].  Not something I've ever been hugely interested in.  More so now I no longer live in a big one.  What's the equivalent of a smart town?


## Raw Notes

***

Alison B. Powell - Undoing Optimization: Civic Action in Smart Cities

***

Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:36]</span></span>:
When we get to the other side, my daughter asks, “How do they decide when the lights should change

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:39]</span></span>:
Some of this advocacy used networked online communication sources to bring together more people than might have signed a paper petition

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:41]</span></span>:
Most of it, though, was fairly traditional—seeking media attention and using personal connections or political representation to raise awareness

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:45]</span></span>:
Perhaps the next step for the local activists should be to install their own sensors to measure how many people are stopped from crossing the road or to measure pedestrian congestion at different times of the day

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:45]</span></span>:
people who notice how companies and governments talk about networks, data, and sensors and who try to respond using the same kinds of technologies—and arguments.
● the foci kthe book people who notice how companies and governments talk about networks, data, and sensors and who try to respond using the same kinds of technologies—and arguments.

Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:47]</span></span>:
By trying to understand what the smart city was, is, and is becoming, it is possible to see how citizen power, government power, and corporate power are shifting.
● smart cty as lens By trying to understand what the smart city was, is, and is becoming, it is possible to see how citizen power, government power, and corporate power are shifting.

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:50]</span></span>:
the fields of communication studies and science and technology studies, these ways of thinking about, building, and structuring systems are referred to as [[technosocial imaginaries]]

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:50]</span></span>:
modernity is built up from a way of thinking that first circulated among influential groups of people and then became widely accepted

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:52]</span></span>:
There is a long history of abstracting the function of the city and trying to understand it as if it were a system

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:53]</span></span>:
“big data optimized city” because it depends on seeing connectivity not as an end in itself but as a precondition for the production of data that, in turn, can be processed to generate efficiencies—to “optimize” transport, city service delivery, or the exercise of civic participation.

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-04 Sun 23:57]</span></span>:
However, the evidence set forth in this book suggests that creating a strong opposition between dominant and alternative sociotechnical imaginaries may not be serving us well.

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:13]</span></span>:
One of my key goals in this book is to describe the genesis of this particular techno-systemic imaginary of the smart city. Another is to describe how it has intensified and with what consequences

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:14]</span></span>:
This infrastructure has itself been the subject of contestation, reimagining, and mutual appropriations of different modes of techno-systemic thinking
● its quite an academkc read. terms used. lck of aalogy and stores, apart from nce one a beɣnning. This infrastructure has itself been the subject of contestation, reimagining, and mutual appropriations of different modes of techno-systemic thinking

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:16]</span></span>:
Sensing the environment, animals, and urban experience in new ways may prove to be a generative counterpoint to a focus on optimization

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:16]</span></span>:
In short, the data-based smart city comes from somewhere and has the potential to go somewhere unexpected

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:16]</span></span>:
focus on the relationships between networks of communication, data for optimization, and sensing the other

Page 25 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:18]</span></span>:
and practices of neoliberalism, the relationship between state and citizen has shifted from “command and control” to coercive governance

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:19]</span></span>:
Sociotechnical imaginaries, like the idea of techno-systems as generative of urban order, play important roles in governance processes by making some kinds of realities and actions visible and present

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:21]</span></span>:
The right to communicate—an abstract concept mobilized by scholars and activists much the way David Harvey makes a claim about the “right to the city”—connects with the idea that communication is a fundamental exercise of citizenship: becoming informed, participating, being heard

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:23]</span></span>:
Sociologist [[Manuel Castells]] made an explicit connection between the power of the network as a social form and the networked architecture of the internet

Page 29 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:24]</span></span>:
Network architectures inspired certain types of political claims

Page 29 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:25]</span></span>:
This concatenation of technology and political argument also appears with a group of young people in Montreal who explicitly claimed that their volunteer-built wireless internet access network was “hacking the built city

Page 30 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:25]</span></span>:
In these threads and negotiations we see consistent techno-systems thinking: the imagining of specific capacities for networks

Page 30 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:28]</span></span>:
Accompanied by some of the same language and enthusiasm, a new version of smartness emerged that focused on the value and significance of data
● originally smart meant connected.. Accompanied by some of the same language and enthusiasm, a new version of smartness emerged that focused on the value and significance of data

Page 32 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:31]</span></span>:
Here, information, communication, and participation become norms of publicity rather than the means to the political ends that they were presumed to serve

Page 32 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:32]</span></span>:
contribution to Twitter can be a form of personal expression or a data point in an analysis predicting future demand for services—or the potential for illegal activity

Page 32 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:33]</span></span>:
**Depending on how networked communication technologies are architected and governed, they can constrain or enable emergent forms of citizenship**

Page 33 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:34]</span></span>:
I identify how these aims are associated with the shift toward “platform governance” in cities, where power, decision-making, and information resources are brokered by intermediaries

Page 35 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:39]</span></span>:
The final techno-systemic way of imagining the city that I examine here is the idea of the smart city as a sensory space, where data-collecting technologies model the seen and unseen in real time

Page 36 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:40]</span></span>:
However, sensing also intensifies the focus on and efforts to control risk that the big data optimized smart city builds up. Sensing cities can therefore be surveillance cities

Page 36 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:40]</span></span>:
**where ideal citizen-subjects consent to data collection in exchange for the privilege and pleasure of improved information and the certainty of well-managed urban systems.**

Page 36 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:41]</span></span>:
In Chapter 4 I investigate the gap between what sensor data can be made to say, that is, who it might speak on behalf of, and what institutional and social contexts this voice might connect with

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 05:46]</span></span>:
As anthropologist Anna Tsing writes, “Human exceptionalism blinds us. Science has inherited stories about human mastery from the great monotheistic religions. These stories fuel assumptions about human autonomy, and they direct questions to the human control of nature, on the one hand, or human impact on nature, on the other, rather than to species interdependence

Page 39 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 23:37]</span></span>:
Across all of the chapters of this book, the idea of citizenship resonates, as does the idea of the commons

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 23:39]</span></span>:
how this view has shifted to identify citizens as consumers of city services optimized by big data analytics or to celebrate them as “auditors” who hold public officials to account

Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 23:41]</span></span>:
The commons that appear in smart cities not only are human-produced networks from which it is difficult to exclude people but can also be information commons

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 23:42]</span></span>:
commons. In the final section of the book, I suggest some radical ways of considering the city as a commons co-produced by many others

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-05 Mon 23:43]</span></span>:
How and to what extent can the structures put in place by the dominant actors within a capitalist techno-system be effectively reappropriated

Page 47 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:03]</span></span>:
The visions of smart cities derive from a myth about technology in general and optimization of civic life in particular

Page 50 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:11]</span></span>:
When a smart city is focused on network connectivity, people can also participate in claiming communication rights by self-organizing to create different means of access to networks
● what are communication rights? When a smart city is focused on network connectivity, people can also participate in claiming communication rights by self-organizing to create different means of access to networks

Page 52 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:13]</span></span>:
**The value of political participation is replaced with the promise of political speech via access to communication technology.** Communication theorists Sonia Livingstone and Peter Lunt call the exercise of civic participation under these conditions “[[consumer citizenship]]

Page 52 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:14]</span></span>:
For example, in 1996 the Canadian government advocated for a “national strategy for access to essential services.” By the early 2000s, with a Conservative government in power, the policy reframed citizens as consumers of communication services

Page 54 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:17]</span></span>:
Because some routers used open-source software, they were especially interesting to hackers and tinkerers who reprogrammed them to transmit in new ways and often joined with community organizations to share internet connectivity within communities

Page 57 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:20]</span></span>:
Why build a free Wi-Fi network in a small town surrounded by what one resident called “moose-infested forests” and located five hundred miles from the nearest large metropolitan area?

Page 58 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:21]</span></span>:
Delivering on the promise of being a knowledge-based community was understood to require internet connectivity

Page 61 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:27]</span></span>:
In this early 2000s version of urban smartness, access to email on the go was almost magical, and connectivity across a city could be offered as a family’s a reason to settle

Page 62 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:28]</span></span>:
At the time the network was built, the city’s leaders were as reflective about how their broadband network acted as public infrastructure as they were about its potential to drive business creation

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:29]</span></span>:
In Fredericton in 2007, citizens (or perhaps taxpayers) were framed as users of services—walkers on sidewalks, consumers of water, people accessing an information network

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:30]</span></span>:
Smart” citizens in this particular historical vision were anticipated as potentially able to collectively benefit from a shared infrastructure, but this collective benefit was always framed and justified in relation to business and economic expansion

Page 64 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:32]</span></span>:
These projects illustrated different ways of claiming communicative citizenship in different city spaces through “peer-to-peer” architectural processes aimed at creating communication commons

Page 68 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:37]</span></span>:
Self-organization is conceived as an active process, whereby economically and legally unencumbered participants voluntarily enter into collaborative relationships

Page 69 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:38]</span></span>:
personal energy, time and labor is made on the basis of joint striving to achieve a larger whole that is more than the sum of its parts: the network commons

Page 69 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:39]</span></span>:
Over the decade of the 2000s, hundreds or perhaps thousands of ad hoc or community wireless networks were established, bringing together people interested in experimenting with open wireless technologies and those interested in improving civic life

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:46]</span></span>:
The members of [[Île Sans Fil]] held a sense of themselves as active citizens that contrasted with the way that people using the Wi-Fi network positioned themselves as passive consumers

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:46]</span></span>:
One volunteer described ISF as “primarily a social club for geeks . . . a club of passionate workers

Page 75 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:47]</span></span>:
However, Michael Warner, a scholar of publics, argues that a public must continually extend its discourse to “indefinite strangers” if it is to be sustained: otherwise, the would-be public remains a closed group

Page 77 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:49]</span></span>:
The fact that the service was free—as in free of charge—was considered more important than the fact that ISF’s technical and social structure were open to participation

Page 77 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:53]</span></span>:
While the geek-public could maintain its convivial connections by meeting to talk about and build Wi-Fi networks, the community-public never shared the same space of public engagement
● indieweb smilarities While the geek-public could maintain its convivial connections by meeting to talk about and build Wi-Fi networks, the community-public never shared the same space of public engagement

Page 78 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:54]</span></span>:
These stories show how the promise of technology influenced the ways that citizens were invited to participate in early-2000s versions of the smart city
● chapter is about few ways in which cnnected cities csn play kt. corporate, dentralksed, r somewhere in the mkddle? These stories show how the promise of technology influenced the ways that citizens were invited to participate in early-2000s versions of the smart city

Page 78 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:57]</span></span>:
Advocates in Berlin built a mesh network to provide access when state and market failed, but their model of governance through physical network maintenance was too burdensome to maintain as commercial access became more available

Page 79 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:58]</span></span>:
Communication scholar Andrew Herman explains how so many technology activists evoking commons or collaboration using technical means are disappointed when the social outcomes do not easily follow

Page 80 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 07:59]</span></span>:
Medosch makes a similar point: that the network commons discourse is separate from the discourse on economies of solidarity that would facilitate an economic transformation to the commons, and separate also from forms of radical collective engagement and mutual aid

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 08:00]</span></span>:
Drawing on the identification of the gaps between the geek-public of advocates like ISF and the potential for broader community participation, democratic technology researcher Greta Byrum has investigated ways to explicitly connect community-based technology provision to social goals, reinvigorating the idea of the communicative commons

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:26]</span></span>:
She identifies how the [[Allied Media Project]] based in Detroit facilitated the development of local knowledge and social capacity by adopting models of distributed mesh networks

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:29]</span></span>:
Nucera built the curriculum using a popular education method grounded in the history of the Civil Rights–era Citizenship Schools and Paulo Freire’s Pedagogy of the Oppressed

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:31]</span></span>:
this adoption of mesh networking technology and participatory strategy, community capacity building has come first
● different ends gols for buiding networks this adoption of mesh networking technology and participatory strategy, community capacity building has come first

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:32]</span></span>:
These portable networking kits (PNKs) bring a punk DIY approach to community-led technology work, focusing on technical work as a type of care and maintenance, similar to that of the Freifunk project. However

Page 83 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:34]</span></span>:
These commons visions, like the ideas that promised transcendence through the embedding of global IT in cities, sometimes overstated the connection between the organizational capacity of the technology and the economic or cultural practices underlying them

Page 84 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:36]</span></span>:
Reorganizing provision of network infrastructure based on principles of mutual aid and self-sufficiency demonstrates another direction away from commercial monopoly. However, focusing too much on the requirements for technical knowledge as a contribution to maintenance took the Berlin mesh network project away from its community-supporting roots

Page 84 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:37]</span></span>:
As activists learn from these early experiments and reposition connectivity technology against a backdrop of community resilience in the face of economic instability and climate change, the notion of the commons reappears, this time challenging a state of urban smartness that is increasingly tied up with the possibility for pervasive connectivity to facilitate pervasive data collection and “surveillance capitalism


### Data Cities and Visions of Optimization

Page 46 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:41]</span></span>:
Network Access and the Smart City of Connectivity

Page 85 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:44]</span></span>:
the past decade, the dominant vision for the smart city has shifted from a communicative city equipped with access to an information society and toward a pervasively connected city where governments and commercial enterprises can use data to increase efficiency

Page 85 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 09:44]</span></span>:
Connectivity is no longer the goal; now it is a precondition for the intensification of data collection

Page 86 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:06]</span></span>:
The adoption of platforms causes a clash between stakeholders and public values.”1 Here I explore this clash and its consequences through the example of the emergence of the big data optimized city.

Page 87 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:08]</span></span>:
These intermediaries, and their use of calculation and optimization, create frameworks for communication and citizenship that fit an “optimization frame” that pre-assumes connectivity and absorbs data.3

Page 88 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:09]</span></span>:
[[information asymmetry]]

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:11]</span></span>:
platform has promised to make cities into cybernetic systems, self-optimizing and oriented around data and predicated on information asymmetry

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:11]</span></span>:
DATAFICATION AND THE CYBERNETIC CITY

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:11]</span></span>:
Underpinning claims for economic rationality of datafication lies a cultural assumption that the world can best be understood through data

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:16]</span></span>:
Under governance regimes that include budget cuts and diminished responsibilities for public institutions, urban citizens are repositioned as consumers of city services and producers of the data that make platform models profitable

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:22]</span></span>:
These consumer-citizenship positions support a vision of a city as an integrated service-delivery platform, with services and processes streamlined

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:33]</span></span>:
CYBERNETIC CONTROL AND DATA ASSEMBLAGES

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:36]</span></span>:
New visions of cybernetic control are now made concrete in projects like city dashboards that combine real-time urban data indicators to “visualize” city functions like public transit, road congestion, crime statistics, and weather information

Page 94 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:39]</span></span>:
The cybernetic model learns from data, but feedback loops are usually imagined as closed, which produces a different set of relations from the generative, horizontal ones in the distributed mesh network

Page 94 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:40]</span></span>:
**From the citizen perspective, being a source of data for someone’s dashboard is different than receiving access to the internet and being invited to use that connectivity to express oneself**

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 10:41]</span></span>:
big companies (sometimes the same ones), as well as governments and third-sector organizations, are now developing ways to benefit from or intervene in data collection by using the promise of algorithmic insight and the architecture of the platform

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 11:53]</span></span>:
Datafication, intermediation, and platformization act as affordances for civic action

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 11:56]</span></span>:
The cybernetic systems that underpin the framework of the data-optimized smart city are intended to reduce complexity in various aspects of city life

Page 97 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 11:59]</span></span>:
Technology researcher Tarleton Gillespie identifies four “semantic territories” for platforms: architectural, figurative, political, and computational

Page 98 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:13]</span></span>:
The concept of “[[platform government]]” was developed by digital advocate Tim O’Reilly in 2011.16

Page 98 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:14]</span></span>:
Throughout the 2000s, the idea of rendering government into a form that would expand openness, participation, and efficiency in service delivery inspired efforts to model smart cities as platforms—and citizens as data sources.

Page 100 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:20]</span></span>:
CORPORATE ACTORS AND THEIR PLATFORM STRATEGIES

Page 102 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:29]</span></span>:
Since smart-city intermediary companies benefit most when they succeed in becoming the main platform integrating all streams of data, each company seems to make promises more overblown than the last

Page 105 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:32]</span></span>:
Like Urban Engines and other data-based brokers, in the mid-2010s dozens of companies promised to mitigate risk for urban governments

Page 106 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:33]</span></span>:
[[open source intelligence]] ([[OSINT]]) repository

Page 106 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:34]</span></span>:
The final element of a cybernetically optimized big data system is the ability to predict or make claims about future states based on the data gathered

Page 109 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:37]</span></span>:
citizenship in a big data optimized city remains constrained, since no engagements are invited or expected beyond communication of information in the form of data. 

Page 112 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:42]</span></span>:
A system that concentrates financial power in the hands of centralized platforms is what sociologists Paul Langley and Andrew Leyshon call “negarchical captalism

Page 113 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:44]</span></span>:
**It is relatively straightforward to optimize transportation or the collection of recycling but more difficult to optimize volunteering, knowing neighbors, or creating local capacity to take care of people in a crisis**

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:44]</span></span>:
Datafication, intermediation, and platformization appear in civic strategies as well as corporate ones, suggesting that a techno-systemic frame oriented toward optimization has shaped ideas about active citizenship as well

Page 115 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:47]</span></span>:
**the distortion that crowd-sourced demands for government response can produce, where local governments fix potholes identified by affluent residents with smartphones and eliminate core funding for services such as libraries whose value is not as easily and immediately datafied**

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:49]</span></span>:
So the civic-minded data citizen imagined by CycleStreets, who volunteers data and advocates for transparent processes of building applications, is displaced by the consistently data-producing Citymapper customers, who benefit from better optimized experiences of navigation

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:50]</span></span>:
Because these civic actions are directing energy at a narrow range of potential optimizations, they valorize narrow notions of citizenship

Page 119 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:52]</span></span>:
When we assume that cities should be networked and citizens should claim rights of access, we create the underpinning of a form of techno-systems thinking based on the assumption that data collection is necessary for optimizing systems

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:54]</span></span>:
Platforms are shaped by actors with civic interests, and open data activism intervenes in the flattening and commodification of datafied citizenship


### Entrepreneurial Data Citizenships, Open Data Movements, and Audit Culture

Page 122 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 12:58]</span></span>:
Listening to [[open data]] advocates talk about their work and observing how they have questioned both data and the way that it is used in governance decisions show the potential of these movements but also some of their constraints—including how difficult it is to challenge ideas about platform governance that come from narrow parts of technology culture

Page 124 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:00]</span></span>:
analytics systems, automated decision-making systems like machine learning can be applied to large-scale, variegated sets of data produced by citizens who accept that their movements, buying habits, energy use, and biorhythms are collected as implicit participation in the civic project of expanding efficiency

Page 126 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:03]</span></span>:
I had heard the same arguments before about open networking systems and their power to allow people to communicate—but this time, instead of the openness being connected with open access to information over the internet, it was connected to an invitation to examine data produced by governments

Page 128 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:05]</span></span>:
citizen auditors who could hold government to account

Page 133 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:10]</span></span>:
Auditing citizenship transforms civic action into data processing without fundamentally disrupting the frameworks that valorize predictability and data production

Page 134 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:13]</span></span>:
This nonprofit company, limited by guarantee and funded by the government, aims, in its own words, to “connect, equip and inspire people around the world to innovate with data

Page 137 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:17]</span></span>:
Antonio Gramsci’s theory of trafismo [[[trasformismo]]?], she argues that many of the potentially disruptive and critical positions of open data advocates were repositioned to serve government interests

Page 137 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:19]</span></span>:
Like the community-networking advocates who used visions of the smart city to advance the prospect of community-owned infrastructure, data advocates use the language of openness and platform-based optimization to advocate for the production of public information infrastructures and the transformation or recapture of the function and consequences of platforms.

Page 139 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:21]</span></span>:
Bath: Hacked

Page 141 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:23]</span></span>:
Even when data is available and expertise can be found to interpret it, the responsibility for bringing concerns forward sits between the individual and the collective

Page 142 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:23]</span></span>:
Despite years of data activism identifying problems with the buildings, **the voice of the people was heard only when their warnings were not heeded and their home was consumed by fire**; some of the people involved in identifying the relevant data died.  Faced with the reality of class-based and racial discrimination, data do not automatically generate the kind of stories that garner attention, and data that questions the decisions of the powerful can still be ignored.

Page 142 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:23]</span></span>:
Faced with the reality of class-based and racial discrimination, data do not automatically generate the kind of stories that garner attention, and data that questions the decisions of the powerful can still be ignored

Page 143 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:24]</span></span>:
The way you deal with that is make sure you publish all the context that goes with it. So, you describe how it’s been collected. You want to help people understand where there are limits in using the data, ways that you can usefully use it or ways that you shouldn’t use

Page 146 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:27]</span></span>:
INTERVENING IN THE DATA PLATFORM: STANDARDS ACTIVISM

Page 147 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:29]</span></span>:
-based data to civic scrutiny and competition for contracts, their goal is not so much to make themselves accountable as to make them efficient. How can advocates intervene in this process to make these processes accountable or just

Page 148 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:30]</span></span>:
They understood technical standards as a kind of “Trojan Horse” for policy advocacy, using standards to advance their goals of increasing accessibility, reuse, and the use of data commons

Page 149 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:32]</span></span>:
The standards work of Open Digital Services Co-operative offers ways of bringing back particular forms of data into public ownership or consideration and of bringing civic advocacy perspectives into policy-making conversations

Page 150 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 13:32]</span></span>:
OPEN REFERRAL: STANDARDIZING DATA, ADVOCATING FOR DATA COMMONS

Page 151 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:02]</span></span>:
He started The [[Open Referral]] project when he noticed that within a single city, different community service organizations maintained separate lists of information about where people in need could access services

Page 153 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:04]</span></span>:
Open Referral can therefore be viewed as a type of data commons

Page 153 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:05]</span></span>:
The [[data commons]], like the wireless commons, can be a way of figuring technical resources as shared sources of value and significance.

Page 154 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:06]</span></span>:
Bloom’s conclusion on the necessity of **a public data commons, where rights to access and use information are negotiated by rules and standards, may be a productive critique on the inequities of the platformed big data city**

Page 155 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:07]</span></span>:
TRANSPARENCY OBSCURES INJUSTICE

Page 155 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:09]</span></span>:
If data activism requires, at a minimum, expert knowledge as well as a social position that gives volume and immediacy to the advocate’s voice, critiques will be limited. The

Page 156 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:10]</span></span>:
Collaboration and participation on city platforms have produced a cadre of linked-in experts and consultants who can move between advocacy and city administration

Page 156 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:10]</span></span>:
Forging standardization agreements that allow data to be placed in commons and creating agreements that specify transparency and public ownership of data even in big data optimized cities are unique and possibly transformative interventions

Page 158 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:13]</span></span>:
Greg Bloom of Open Referral calls them commons, and so do others, but they don’t always mean the same thing


### Rethinking Civic Voice in Post-Neoliberal Cities

Page 160 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:16]</span></span>:
This was one of the first projects to foreground the capacity of citizens to collect the data that they chose, rather than having data passively collected and “crowdharvested” from commercial organizations or governments.

Page 160 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:17]</span></span>:
**A generation of civic sensing projects have been undertaken under the assumption that creating and interpreting data can allow citizens to produce different—oppositional—knowledge from that produced by authorities and experts**

Page 161 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:18]</span></span>:
Citizen action using sensors can often mean that citizens are sensing things that governments or corporations won

Page 162 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:19]</span></span>:
In Bristol, data commons, rather than being only stocks or stores of citizen-generated data, became sites to develop relationships of solidarity through the tolerance of friction, tension, and dispute about how data connects with things that matter to people. 

Page 163 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:21]</span></span>:
Splintering processes break up the common infrastructures that serve cities, such as water, electricity, and internet, and allow different operators to deliver them in different areas

Page 165 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:24]</span></span>:
CITIZEN AS SENSOR: CIVIC VOICE

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:25]</span></span>:
In a participatory sensing project at a series of hydraulic fracking sites in Pennsylvania, Gabrys and her team found that the **civic sensors weren’t as well calibrated as the official sensors, but that by measuring air quality, noise, and water quality in different kinds of locations than the fracking companies or the Environmental Protection Agency did, people generated “just good enough data” to start conversations with policy-makers**

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 14:38]</span></span>:
But what are the risks of the “just good enough”? One outcome of citizen science and civic data collection can be that government agencies like the US Environmental Protection Agency start installing more of their own sensing equipment and use their regulatory powers

Page 169 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:08]</span></span>:
CIVIC ACTION IN A SENSOR CITY: MAKING COMMONS, ENDURING FRICTIONS

Page 172 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:11]</span></span>:
For the past twenty years, Knowle West has also been known for the [[Knowle West Media Centre]] (KWMC), which has addressed social justice issues by providing media training, technology education, and opportunities for creative expression and civic action, all connected through knowledge sharing and technology skills development

Page 173 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:12]</span></span>:
Citizen sensing, they said, “is about empowering and enabling people to use technology for social good

Page 173 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:12]</span></span>:
Between 2016 and 2017, KWMC and Ideas for Change tested the [[Bristol Approach]], exploring the potential of a data commons as a tool for social change

Page 174 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:14]</span></span>:
She saw the Bristol Approach as a way to involve the local government not just in producing data for citizens to use but in responding to citizen needs as presented by new technologies

Page 179 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:40]</span></span>:
COMMONS VISIONS

Page 179 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:41]</span></span>:
an early definition of “data commons,” human-computer-interaction researchers Dana Cuff and colleagues described them as “repositories generated through decentralized collection, shared freely, and amenable to distributed sense-making” and noted that these repositories “have been proposed as transforming the pursuit of science but also advocacy, art, play, and politics.”

Page 180 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:43]</span></span>:
Along with scale, civic data commons are perceived as gaining value when the data they contain is amenable to calculation by the entities that manage smart urban systems

Page 181 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:45]</span></span>:
collaboratories

Page 182 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:46]</span></span>:
The protocols or agreements thus enact the commons and mobilize resources for discussion, self-representation, and action

Page 182 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:46]</span></span>:
**The idea here is to move back to the notion, closer to Ostrom’s view, that some of the value of a commonly held resource is developed through the collaborative relationships required to sustain the resource**

Page 183 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:48]</span></span>:
WHAT CAN SENSING SAY? CIVIC KNOWLEDGE IN THE POST-NEOLIBERAL CITY

Page 183 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:50]</span></span>:
Although sensing promises to create a better conversation with some “good-enough” data, the shifts in organization and governance that come along with transforming a city into the image of an agile software platform have social consequences

Page 185 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:54]</span></span>:
In the Bristol Approach pilot, sensor data were combined with other data, including open government data on housing types and data generated and processed at the project’s Data Jams, supported by the city government. However, the mechanisms to trigger specific service deliveries were never clearly connected with these data

Page 186 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 15:59]</span></span>:
**The risk of making data collection, manipulation (through hackathons or other means), and presentation by citizens to decision-makers the de facto process for gaining civic voice is that these activities don’t provoke action for change and, worse, may serve as excuses for further limiting collectively provided services**

Page 188 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:02]</span></span>:
some investigations of data commons, for example, researchers contrast the way that expansions of intellectual property create “anticommons” that produce conflictual relationships around the use of data, with the production and management of data commons providing an alternative.27

Page 192 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:07]</span></span>:
might be possible to use the Bristol Approach experience to reposition data commons as structures that support solidarity—as what philosopher Richard Rorty describes as “the imaginative ability to see strange people as fellow sufferers

Page 193 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:09]</span></span>:
contrast to the smooth optimization that constrains citizenship, [[friction]] slows down the process and **requires negotiation, balance, and acceptance of similarity within difference**

Page 194 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:11]</span></span>:
Is there any way for the frictions and emergent solidarities to push past a **vague idea that collecting data is a new form of civic participation**


### The Ends of Optimization

Page 201 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:22]</span></span>:
This kind of knowledge is imagined as making the city “sentient

Page 202 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:23]</span></span>:
[[sentient city]]

Page 205 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:54]</span></span>:
Theories of democracy that assume a world of active subjects and passive objects begin to appear as thin descriptions at a time when the interactions between human, viral, animal, and technological bodies are becoming more intense.”4
For

Page 206 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:57]</span></span>:
The promise is that the bees become knowable to the keeper—that this data, this knowledge, is what produces action, rather than the action of interacting with the hive to see how heavy it has become. We want to know more; we want to be able to transform sense into data to log the changes to the hive over time

Page 208 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 16:58]</span></span>:
With the rise of the modern smart city, informational worlds have come to replace sensory or relational worlds—what design theorist and decolonial theorist Arturo Escobar calls the “[[pluriverse]]” of different being and different ways of seeing things

Page 209 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:00]</span></span>:
FRAGILE SENSING: CONNECTED SEEDS
Since smart-city schematics represent the fullest extension and incursion of capitalism into everyday life

Page 211 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:02]</span></span>:
Seed libraries and commons are an ancient form of shared knowledge

Page 213 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:10]</span></span>:
**If we want Smart Cities to be something else, or to be something more . . . then we don’t just need more design, what we need is a different design**

Page 218 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:18]</span></span>:
The communal and social benefits of gardening, as well as the nascent political potential of gardens and other common urban spaces, are often celebrated. Researchers looking at Glasgow’s [[community gardens]] saw the gardens as spaces of social transformation that held the potential for a new political practice. “Enabled by an interlocking process of community and spatial production, this form of citizen participation encourages us to reconsider our relationships with one another, our environment, and what constitutes effective political practice.”

Page 219 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:20]</span></span>:
In subsequent work she develops the concept of “data stories,” arguing that the points of expression and contention that sensing data make possible do not necessarily only depend on the validity or calibration of data but are instead significant because of the narratives they permit

Page 222 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:22]</span></span>:
invitations to focus on hybridizing knowledge and as indications that datafication in service of optimization can displace the potential for data and other types of knowledge to interconnect. Datafication can’t occur without making some

Page 223 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 17:24]</span></span>:
d like to propose an ethics of sense that draws on two key principles: [[solidarity forged in difference]] and the political power of situated knowledge

Page 227 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 18:00]</span></span>:
These concepts of relationality, contextuality, and place are what ground indigenous forms of knowledge to the territory from which they proceed: knowledge of territory is developed in order to guide others for everyone’s well-being

Page 229 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 18:06]</span></span>:
An important organizing concept in the Arctic, its conceptual relative Gaia has been discussed since the 1960s by European philosophers as a way to understand ecological patterns as self-regulatory

Page 230 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 18:08]</span></span>:
Once again, the promise of a [[knowledge commons]] is best made evident in the disagreements and difficulties in determining who and how it should be managed

Page 234 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 18:19]</span></span>:
The Right to Minimum Viable Datafication

Page 236 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:30]</span></span>:
argue that it is no longer adequate only to advance normative critiques of smart-city projects and ask for more transparency or accountability

Page 236 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:31]</span></span>:
minimum viable datafication—to collect data to develop functions in the present rather than to intensify future use—as a way to model an alternative approach

Page 238 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:33]</span></span>:
share this example here to identify the many well-connected and well-established interests that seek to extend the optimizing capacities of smart cities in ways that actively seek to curtail individual or collective rights in the name of reducing disorder—which is, of course, another way of thinking about optimizing a system

Page 240 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:35]</span></span>:
One of the features of the [[Sidewalk Labs]] development was that it, like other greenfield smart cities, was meant to be built from the ground up

Page 241 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:36]</span></span>:
for [[Sidewalk Toronto]], and as I mentioned in Chapter 4, the plans originally specified that features would be unavailable to people who refused to register their personal data and accept monitoring across the entire Sidewalk development

Page 247 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:43]</span></span>:
Optimization precludes and narrows the capacity for citizenship that uses technology to bring forward diverse knowledges in diverse ways and submits civic decision-making to narrow corporate interest

Page 247 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:43]</span></span>:
optimization of service delivery through data analytics is part of a trajectory toward private interests in

Page 248 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:44]</span></span>:
These sites of difference and tension, always in motion, are places where civic knowledge is produced and where the background work of democracy takes place

Page 248 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:46]</span></span>:
Legal theorist Julie Cohen suggests that one way to push back against datafication is to support the potential for “semantic discontinuity” where it is impossible or very difficult to link together different types of data and draw immediate conclusions from them

Page 250 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:54]</span></span>:
In the name of innovation, smart cities have focused on technological frameworks that produce optimization

Page 250 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:55]</span></span>:
Accompanied first by enthusiasm about the capacity of networking and then by promises of inclusion, accountability, and greater participation by virtue of access to data, these frameworks have produced and reproduced techno-systems thinking

Page 251 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 20:59]</span></span>:
These practices also help to situate citizenship, helping to demonstrate ways that civic action transcends universality and to identify ways to reconceive citizenship as a set of practices that seek justice, equity, and also spaces for flourishing relationships between people and others

Page 252 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 21:01]</span></span>:
Not only does datifying the economy have tremendous social consequences; it also has environmental ones, for **the energy required to store and process digital data impacts climate and development in the cities and towns where data centers are located**

Page 253 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 21:02]</span></span>:
This does not mean never collecting data; it does not mean never employing AI to parse patterns, or sensing devices to contribute to decision-making. It means following a strategy of minimizing, rather than maximizing, this kind of data, and

Page 253 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 21:02]</span></span>:
means seeking to employ decision-making strategies that may appear to be more costly on the surface but that leave space for different kinds of knowledge, as well as for data to decay over time, for frictions to be identified and addressed

Page 254 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-06 Tue 21:03]</span></span>:
The vision and promise of the smart city aligns the goals of optimizing connectivity, data collection, and sensing processes with civic actions that sustain them

