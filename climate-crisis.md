# climate crisis

> The intensification of extreme weather events is linked directly to the climate crisis, according to a new Guardian analysis. Some 71% of 500 events examined were made more likely or more severe, with at least a dozen all but impossible without human-caused global heating.
> 
> &#x2013; [Friday briefing: What the interest rate spike means for the country – and for&#x2026;](https://www.theguardian.com/world/2022/aug/05/friday-briefing-interest-rates-bank-of-england)

<!--quoteend-->

> If emissions continue to rise at the current rate, the average surface temperature could heat up by 3.3° to 5.7° C by the end of the century. That would be tantamount to a complete collapse of the climate system. The last time [[global surface temperature]] was 2.5°C or higher than 1850-1900 was over 3 million years ago.  What is happening now is of immeasurable Earth-historical significance.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

<!--quoteend-->

> Large parts of the Earth, including numerous megacities, will probably become uninhabitable in just a few decades.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

<!--quoteend-->

> Excluding migration, one-third of the world's population is projected to be exposed to mean annual temperatures exceeding 29°C, currently found on only 0.8% of the Earth’s land surface and concentrated mainly in the Sahara. The regions potentially most affected are among the poorest in the world.  Immeasurably more severe than the consequences of the ongoing Covid 19 pandemic, this process will amount to the extermination of a portion of the world’s poor population.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

-   [[Capitalism is the cause of the climate crisis]]


## 2022

> In mid-June, a ferocious “heat dome” brought roasting temperatures to much of the US, placing around a third of Americans under hazardous-heat warnings. It came soon after a record heatwave in India so brutal birds fell from the sky. Floods, meanwhile, have been the worst in a century in Bangladesh and so fierce in America’s Yellowstone national park that entire bridges and buildings were washed away.
> 
> &#x2013; Down to Earth newsletter

<!--quoteend-->

> António Guterres told ministers from 40 countries meeting to discuss the climate crisis on Monday: “Half of humanity is in the danger zone, from floods, droughts, extreme storms and wildfires. No nation is immune. Yet we continue to feed our fossil fuel addiction.”  He added: “We have a choice. Collective action or collective suicide. It is in our hands.”
> 
> &#x2013; [Humanity faces ‘collective suicide’ over climate crisis, warns UN chief | Cli&#x2026;](https://www.theguardian.com/environment/2022/jul/18/humanity-faces-collective-suicide-over-climate-crisis-warns-un-chief) 

<!--quoteend-->

> In March, April and May this year, India and its neighbours endured repeated heatwaves that exposed more than a billion people to dangerously hot conditions. India broke several temperature records. The warmest March in more than a century was recorded across the country and a new high of more than 49C was hit in Delhi in May
> 
> &#x2013; [Why you need to worry about the ‘wet-bulb temperature’ | Climate science | Th&#x2026;](https://www.theguardian.com/science/2022/jul/31/why-you-need-to-worry-about-the-wet-bulb-temperature) 

