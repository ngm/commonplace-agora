# Five Principles of a Socialist Climate Politics

Author
: [[Matthew Huber]]

URL
: https://www.the-trouble.com/content/2018/8/16/five-principals-of-a-socialist-climate-politics

> Too often the notion of “system change” doesn’t emphasize that we are fighting a class of people rather than a vague, impersonal “system.”

