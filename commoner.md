# commoner

> Commoner is an identity and social role that people acquire as they practice [[commoning]].  It is associated with actual deeds, not an assigned legal or social title.  Anyone is potentially a commoner.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Commoners can improve their long-term independence by withdrawing as much as possible from dealings with the market/state system.
> 
> &#x2013; [[Free, Fair and Alive]]

