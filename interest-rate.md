# interest rate

> Most economists regard the interest rate solely as a policy lever to be used for controlling [[inflation]].
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)


## Trying to control inflation

You can use interest rates as a way to nudge people into certain behaviours, in an attempt to alter the rate of inflation.


### If inflation is too low

-   interest rates are decreased
-   to get people to borrow and to spend
-   in theory: boosts economic growth -&gt; inflation goes up

> Thus, when inflation falls below the central bank’s target, which in most developed countries is set at 2%, interest rates come down and stay down. Low rates encourage individuals, companies and countries to borrow, and to spend, boosting economic growth. This is what happened over the past decade.
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)


### If inflation is too high

-   interest rates are increased
-   to get people to save rather than to spend or borrow
-   in theory: demand for goods goes down -&gt; prices go down -&gt; inflation goes down

> But when inflation climbs above the target level, as it has done in recent months, rates are hiked, incentivising saving, rather than spending or borrowing. In theory, as demand for goods falls, so do prices, bringing down inflation.
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)


## Interest rates and recession

> The Bank usually hates to put rates up if it thinks a recession is coming, because doing so makes one more likely.
> 
> &#x2013; [Friday briefing: What the interest rate spike means for the country – and for&#x2026;](https://www.theguardian.com/world/2022/aug/05/friday-briefing-interest-rates-bank-of-england)

