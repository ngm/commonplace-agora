# BitTorrent

> BitTorrent’s launch in 2001 enabled file-sharing on a massive, efficient and resilient scale. By embedding the [[decentralised]] ideology beneath the desktop client, and within the protocol on which the client runs, the act of file-sharing became much more resistant to legislative attack. To many of its supporters and combatants, BitTorrent seemed unstoppable.
> 
> &#x2013; [[This is Fine: Optimism and Emergency in the Decentralised Network]] 

