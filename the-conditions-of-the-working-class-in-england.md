# The Conditions of the Working Class in England

> In 1845, at 25, Engels would write eloquently and powerfully of working-class lives in his book The Condition of the Working Class in England. He described one [[Manchester]] slum as follows: “Masses of refuse, offal and sickening filth lie among standing pools in all directions; the atmosphere is poisoned by the effluvia from these, and laden and darkened by the smoke of a dozen tall factory chimneys. A horde of ragged women and children swarm about here.”
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

