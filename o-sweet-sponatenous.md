# O sweet sponatenous

A
: [[Poem]]

Author
: e.e. cummings

<div class="verse">

O sweet spontaneous<br />
earth how often have<br />
the<br />
doting<br />
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp;fingers of<br />
prurient philosophers pinched<br />
and<br />
poked<br />
&nbsp;<br />
thee<br />
,has the naughty thumb<br />
of science prodded<br />
thy<br />
&nbsp;<br />
&nbsp;&nbsp;beauty  .how<br />
often have religions taken<br />
thee upon their scraggy knees<br />
squeezing and<br />
&nbsp;<br />
buffeting thee that thou mightest conceive<br />
gods<br />
&nbsp;&nbsp;&nbsp;(but<br />
true<br />
&nbsp;<br />
to the incomparable<br />
couch of death thy<br />
rhythmic<br />
lover<br />
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp;thou answerest<br />
&nbsp;<br />
&nbsp;<br />
them only with<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spring)<br />

</div>

