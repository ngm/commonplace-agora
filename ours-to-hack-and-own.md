# Ours to hack and own



## Overall thoughts

-   enjoying it muchly so far
-   good topics, good selection of writers
-   kleiner, bollier, schneider, scholz
-   others that I haven't heard of but with cool ideas
-   platform cooperativism and platform capitalism are the big themes
-   but with other threads and strands within that
-   e.g. worker rights, capitalism, equality


## Chapters


### 11: Portable reputation in the on-demand economy


#### lessons from offline gig economy


#### e.g. screen actors guild where they all pay into a common pot


##### so a strong union helps


#### reputation portability


#### unions for gig workers are important


#### good points - we can look to existing examples for more info on how we can improve the lot of precarious gig workers.


### 12: Counteranti-disintermediation


#### "capital is not a thing but a social relation between persons, established by the instrumentality of things."


##### so you are only a capitalist if you somehow have the means to exploit labour


##### just having the means of production is not enough, if you can't use it to subjugate labourers


#### "like the colonies, the Internet needed to be systematically colozied in order to create the conditions needed by capital.  This was accomplished by enclosure.  The original infrastructure was taken over and brought under capital control, and decentralized systems were displaced with centralized systems."


#### uber/airbnb "own no vehicles or real estate"


##### not stricly true


#### "The most effective way to ensure consent is to ensure that all user data and control of all user interaction resides with the software running the users' own computer, and not on any intermediary servers"


#### "The flaw was, to some degree, a result of the early architecture of the Internet.  The systems that people used in the early Internet were mainly cooperative and decentralized, but they were not end-to-end."


##### being dependent on servers run by others is a problem?


#### "Just as systematic colonialization was developed to estlbish the capitalist mode of production in the colonies, antidisintermediation was developed to coloize cyberspace."


#### "platforms must not depend on servers and admins even when cooperatively run, but must, to the greatest degree possible, run on the computers of the platform users."


#### Really interesting but is complete distrubtion the only method of counterantidisintermediation?


#### will the federation/decentralized approach always be prone to antidisintermediation?


#### IndieWeb is closer to fully distrubted


##### although using on specific set of protocols


## Resources

-   [Ours to Hack and to Own | openDemocracy](https://www.opendemocracy.net/en/ours-to-hack-and-own/)

