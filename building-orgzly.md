# Building orgzly



## Why?

It's not had a release for nearly a year, but a bunch of features have been added in that I'd like.  Specifically that of relative paths working as they do in Emacs, so that they work across both.  Also webdav sync could be handy.


## Building

-   [GitHub - orgzly/orgzly-android: Outliner for taking notes and managing to-do &#x2026;](https://github.com/orgzly/orgzly-android#building--testing) 
    
    Seems I need gradle, which seems to then need SDKMAN!
-   [Home - SDKMAN! the Software Development Kit Manager](https://sdkman.io/) 
    ```bash
    source "/home/neil/.sdkman/bin/sdkman-init.sh"
    sdk install gradle
    ```

<!--listend-->

```bash
cd Code/
git clone https://github.com/orgzly/orgzly-android
cd orgzly-android/
./gradlew build
```

> &gt; SDK location not found. Define location with an ANDROID<sub>SDK</sub><sub>ROOT</sub> environment variable or by setting the sdk.dir path in your project's local properties file at '/home/neil/Code/orgzly-android/local.properties'.

https://developer.android.com/studio
Download command line tools only.

```bash
 unzip commandlinetools-linux-6858069_latest.zip 
 mkdir ~/android
 mv cmdline-tools/ ~/android
 cd android/cmdline-tools
mkdir latest
mv latest/ cmdline-tools/
cd Code/orgzly-android/
./gradlew build
ANDROID_SDK_ROOT=/home/neil/android/ ./gradlew build
scp app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk dloop:
scp app/build/outputs/apk/fdroid/debug/app-fdroid-debug.apk dloop:
```

New settings:

-   Root for absolute links (e.g. `file:/readme.txt`)
-   Root for relative links (e.g. `file:readme.txt`)
    
    Files now open properly!  I open them either in orgro, or images open in the file manager.

