# The era of fixing your own phone has nearly arrived

URL
: https://www.theverge.com/23017361/ifixit-right-to-repair-parts-google-samsung-valve-microsoft

> Fresh off big wins with Google, Samsung and Valve, iFixit says more parts deals are nigh 

[[Google]], [[Samsung]] and [[Valve]] have deals to provide [[spare parts]] via [[iFixit]].

> “The thing that’s changing the game more than anything else is the French repairability scorecard,” says Wiens, referring to a 2021 law that requires tech companies to reveal how repairable their phones are — on a scale of 0.0 to 10.0 — right next to their pricetag. 

^ [[Repairability standard]]

Very interesting that the legislation in Europe seems to be making a difference.

