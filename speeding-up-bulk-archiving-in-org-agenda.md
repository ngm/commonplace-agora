# Speeding up bulk archiving in org-agenda

Speeding up bulk archiving in [[org-agenda]].

Marking items in bulk in the agenda, then bulk archiving them, had gotten really slow for me.  Turns out it was because my \_GTD.org<sub>archive</sub> file had gotten so big (&gt;30,000 lines).

I copied \_GTD.org<sub>archive</sub> to \_GTD.org<sub>archive</sub>-2021-03-25, then emptied \_GTD.org<sub>archive</sub>.  Now it's nice and fast to archive in bulk again.

