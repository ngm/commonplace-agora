# Sunak Comes Unstuck

A
: [[podcast]]

Series
: [[TyskySour]]

URL
: https://novaramedia.com/2022/03/25/sunak-comes-unstuck/

Bit of stuff about [[Rishi Sunak]]'s public gaffes recently (pretending to drive a much cheaper car than he does; not knowing how to use a contactless card; struggling to name the bread he eats; getting in a flap about being asked his wife's ties to a business operating in Russia). Showing how out of touch with the public he is.  Of course he fucking is though, most politicians are. I find this stuff a bit pointless really, I'm more bothered about him trampling on the poor of the country while being a multi-millionaire.  But I suppose it matters in some sense of his public standing and whether he might become PM one day.  

Also some stuff about the [[Spring Statement 2022]].

And some stuff about Graham Linehan and his bizarre anti-trans campaign.  Sad.

