# Cooperatives

I'm very much into the idea of cooperatives.  I don't have any experience of working in one myself (yet), but have worked with some, and try to buy goods/services from them when I can.

> A co-operative is an autonomous association of persons united voluntarily to meet their common economic, social, and cultural needs and aspirations through a jointly-owned and democratically-controlled enterprise.
> 
> &#x2013; [Cooperative identity, values &amp; principles | ICA](https://www.ica.coop/en/cooperatives/cooperative-identity)

<!--quoteend-->

> A cooperative is an association chartered to provide a product or service, much like any other company. What sets cooperatives apart from more conventional enterprises is that they are entirely owned by the people who work for them—their members—and that their policies and growth strategies are steered by the membership itself, via some kind of democratic decision-making process. All the benefits and rights attendant on the ownership of capital are distributed equally within the organization.
> 
> &#x2013; [[Radical Technologies]]


## [[Cooperative values]]


## [[Coops and commoning]]


## [[Coops and socialism]]


## Are they a challenge to capitalism?

> …if cooperatives are islands in a sea of capitalism, we need better catamarans, bridges, and data lines to connect them to each other and to other transformative economies.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> I think the same could be said about [[free software]]. Both coops and free software are non capitalist, but in and of themselves aren't a strategy to end capitalism or even a real threat to the capitalist system. In the worst vein, they provide a strategy for a privileged few to avoid some exploitative aspects of capitalism. There is enormous potential to organize within these movements to actively struggle against capitalism. But I'm not sure how to do this.
> 
> &#x2013;  [@jamiem@social.coop](https://social.coop/@jamiem/105650661094861507) 

<!--quoteend-->

> Though participants are often drawn to the cooperative movement by a concern for basic tenets of fairness and economic justice, there’s nothing in it that conflicts with private ownership, property rights or the market as a mechanism of resource allocation.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Co-operatives should also strive not to silo themselves off from broader social struggle. Once in competition with other firms, there is a tendency to pursue goals that would advance the interests of the co-operative and to become detached from social issues. The platform co-operative movement works best alongside other forms of activism and political change. This would involve working through trade unions, municipal associations and political parties
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> change in ownership model is no guarantee that other forms of social power – from racism to sexism and ableism – will not continue to be reproduced within the space
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Are co-operatives just a better business model or do they stand for an alternative way of organising the economy?
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> As Rosa Luxemburg saw, the issue with workers’ co-operatives is that they are a ‘hybrid form in the midst of capitalism’.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> This is why platform co-operativism cannot lose sight of its broader political struggle against capitalism and remain content to build small co-operative islands in a sea of corporate giants. If platform co-operativism is reduced to simply a better way of doing business for members it loses its emancipatory potential.
> 
> &#x2013; [[Platform socialism]]


## [[Worker cooperatives]]


## [[Open cooperativism]]


## [[Open-value cooperatives]]


## [[Distributed Cooperative Organisations]]


## Copper lane cohousing

Novara Media https://youtu.be/yjbtCVTWCas​

> Cooperatives formalize the practice of commoning and facilitate legally regulated operations in the marketplace.
> 
> &#x2013; [[The DisCO Elements]]

<!--quoteend-->

> Cooperatives worldwide have a combined turnover of US$3 trillion, which is similar to the aggregate market capitalization of Silicon Valley’s greatest players (Microsoft, Amazon, Google, Apple and Facebook).
> 
> &#x2013; [[The DisCO Elements]]


## Links

-   [Co-ops in Spain’s Basque Region Soften Capitalism’s Rough Edges - The New Yor&#x2026;](https://www.nytimes.com/2020/12/29/business/cooperatives-basque-spain-economy.html)
-   [GitHub - hng/tech-coops: A list of tech coops and resources concerning tech c&#x2026;](https://github.com/hng/tech-coops/)
-   [#PlatformCoop Directory – The Internet of Ownership](https://ioo.coop/directory/)

<!--listend-->

-   [What is a Co-operative? - Invidious](https://vidi.noodlemaps.net/watch?v=90FL_bBE4mw)

