# Tor and Wordfence

I use Wordfence on some sites. It seems that [[Wordfence]] blocklists certain exit nodes from [[Tor]] if it associates those nodes with bad activity.  Fair enough, I guess, though a shame that Tor users get bounced as a result.

If it happens to someone, they can start a new Tor circuit to find an exit node that isn't blocklisted.

