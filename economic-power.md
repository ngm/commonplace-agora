# economic power

Economic [[power]].

In addition to coercion (violence) and consent (ideology), there is the [[mute compulsion]] of economic power.

