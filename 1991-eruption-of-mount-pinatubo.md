# 1991 eruption of Mount Pinatubo

> Pinatubo volcanic eruption of 1991. That lowered global temperature by about a degree Fahrenheit, for a year or two
> 
> &#x2013; [[The Ministry for the Future]]

Relates to some [[geoengineering]] / [[solar radiation management]] techniques.

