# org-jira

Connects Jira to [[org-mode]].


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-15 Mon] </span></span> Giving org-jira a go.

First impressions are that it's an impressive library but a bit too temporamental to use as a daily driver.  Maybe with a bit of getting used to it it'll be beneficial.

-   as we're on Jira Cloud, have to use an API token rather than password.  This isn't clear from the README.  Someone points it out in a comment [here](https://github.com/ahungry/org-jira/issues/21#issuecomment-271148237).
-   sometimes \`org-jira-progress-issue\` works, sometimes it brings up an empty list and you can't progress it to a new status
-   you can't create/edit the sprint it is in, the epic it is in, or the story points


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-16 Tue]</span></span>.

Trying out org-jira today for creating new Jira tickets, and for leaving comments on existing ones.  These are working fairly well so far, mainly because I just much prefer typing in Emacs :D  org-jira still has a couple of quirks around these.


#### Creating new issues

-   it doesn't like it if you have an empty Assignee, even though this is an option when selecting Assignee.
-   you need to use Jira's weird markup format, not markdown
-   creating an epic via this method results in
    
    >    [error] request&#x2013;callback: peculiar error: 400
    >    [error] request-default-error-callback: https://therestartproject.atlassian.net:443/rest/api/2/issue error
    > jiralib&#x2013;rest-call-it: Wrong type argument: stringp, nil

<!--listend-->

-   on one board, I'm getting the same error as above just when creating a bug.


#### Adding comments

-   I'd prefer it if you could edit them in a temporary buffer, rather than in the minibuffer.  It's a bit restrictive working there.
-   Not so keen on the default keybinding of `C-c c`, as this is what I use for my org-capture template.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-17 Wed]</span></span>.

Finding today that creating any new issue is resulting in `Wrong type argument: stringp, nil`.  Shame.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-18 Thu]</span></span>.

Not always useful to only see your own tickets.  Is it possible to see others?

Possibly through a custom JQL query?  


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-22 Mon]</span></span>.

-   Sometimes the whole issue doesn't display.  Parts of it are excluded with a '&#x2026;', but then there seems to be no way to get it to display, even cycling through all the various headline display options.  Seems to be resolved by calling `org-jira-get-issues` again.

-   I'm currently doing a weird hodge-podge of some actions in org-jira, others in Jira web interface.  Anything backlog or board view related is better done in the web interface for now.  Anything around creating, editing the description, or commenting on issues (anything more text-based, I guess), I'm trying to do in org-jira.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-23 Tue]</span></span>

-   Re: the problem with creating descriptions in the minibuffer.  I've started just creating the issue summary, and the 'SO THAT&#x2026;' part of the user story in the description, when creating the issue.  Then adding further information to the description in the buffer once the created issue that been returned. Then I have normal editing capabilities there in the buffer.

-   There is an annoying bug where if you update an issue that is currently unassigned, it will just go ahead and assign it to the first person in the list of assignees.

-   Running `SPC-u SPC a o J i g` runs `org-jira-get-issues` with the universal argument, which then lets you run a JQL query to filter the issues that are pulled back in to the org-jira buffers.  This is really handy.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-02 Tue]</span></span>

The thing with it giving a random assignee to unsigned issues is a real pain, and could become a showstopper.  I've logged an issue: https://github.com/ahungry/org-jira/issues/242

This would seem to be the line in question:
https://github.com/ahungry/org-jira/blob/master/org-jira.el#L2103

I think the reason why Bugs don't create correctly is because we have a custom field for the Bug description.  Bit annoying.  I can't edit that in org-jira.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-23 Tue]</span></span>

Still trying this hybrid approach.  A bit of web Jira, a bit of org-jira.

When I go through my daily plan, I have a link that takes me straight to my 'overview' view in Jira.   If there's something there with my face on it that I think I'll work on today, I'll then go find the corresponding item in org-jira, and add an org task for the day that links through to the org-jira item.

When I'm starting the task in the day, I can click through to it from org-agenda, and then do stuff in org-jira, and finally click through easily enough to Jira web if there's something I need to do there at the end.

Feels convoluted.  But I guess it's this mix of needing some of the Jira board / overview views, and then using org-jira for the daily work on particular items.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-25 Thu]</span></span>

Have changed `org-jira-working-dir` to be a subdir of my main org dir now.  This is nice because I can do a quick `SPC /` (`spacemacs/helm-project-smart-do-search`) and search on ticket numbers / names while in org.

Things from org-jira weren't included in the search previously, as they were off in the `~/.org-jira` directory.

