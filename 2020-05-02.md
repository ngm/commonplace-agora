# 2020-05-02



## Article: [[Publishing your org-roam wiki map to the web: overview]]

I want my wiki to be a [[sensemaking]] aid - after collecting the dots, to help me then [[connect them]] and see the [[constellations]].

One sensemaking tool in Roam and [[org-roam]] is the [[wiki graph]] - a graph (in the nodes and edges sense) of all of the notes in your wiki and their links to each other.  Not quite a mind map.  More of a hypertext map.  I've been playing around with publishing this map to my public site.

([[more]])


## [[Bliki tooling]]

Thinking about '[[bliki]]' (blog and wiki, garden and stream, stock and flow, etc etc) tooling a bit.

[Aside: perhaps if I made some bliki software, I would call it 'Flock'.. flow and stock&#x2026;]


### What I'm currently doing

For the garden bit, I'm using [[org-roam]].  I actually write my stream bits first in org-roam, publish it to HTML, then just manually copy that HTML to [[WordPress]] and publish there for all the public stream stuff. As it's [[IndieWeb]]-enabled, WP gets me feeds for people to follow, and all the interactions you'd expect from streams - replies, likes, etc.

So it is manual until it hurts, but it doesn't hurt too much at the moment.  In fact, writing and hyperlinking with org-roam then copying it over is for me a lot more pleasant than writing straight in to WordPress.

But obviously there's quite a lot of redundancy there.


### Where I could go with it

I could use WordPress pages as my interlinked garden.  This would have the great benefit of also having all of the stream functionality OOTB.  I haven't explored WP for wiki pages much, but I know that [Ton does it](https://www.zylstra.org/blog/2018/04/adding-a-wiki-like-section/).   I think I personally won't do it this way as I find WordPress too much friction for me for writing, but having everything in one system is obviously a big boon.

I think if I could use [[Arcology]] combined with org-roam, that'd get me a pretty sweet bliki setup.  (With more on top, including some of the [[note-taking]] and [[sensemaking]] bits too).

But I think it'll be a while before I'm set up with Arcology, and even then, given it is static, it's missing a lot of the building blocks of the IndieWeb that would also need adding.  So I'll keep it as this manual Rube Goldberg device for now.

But, good to have a long-term goal!


## [[Blikis]]

Really interesting to learn ([via Desmond](https://desmondrivet.com/2020/05/01/wikis-and-blogs)) that Martin Fowler has a [post about the link between blogs and wikis](https://martinfowler.com/bliki/WhatIsaBliki.html), from 2003(!).

> Beyond the name, however, there's the very ephemeral nature of blog postings. Short bursts of writing that might be interesting when they are read - but quickly age. I find writing too hard to want to spend it on things that disappear. 

**"I find writing too hard to want to spend it on things that disappear"** - I love that as a little epigram for why you might want a [[digital garden]].

Martin calls [[the blog and wiki combo]] his bliki.

> Like a blog, it allows me to post short thoughts when I have them. Like a wiki it will build up a body of cross-linked pieces that I hope will still be interesting in a year's time.

As a word, I'm not so keen on 'bliki' (although back then Martin didn't like the word 'blog', and well here we are now, I don't give it a second thought).  

But blikis as a concept - I'm all in.


## Improving my wiki publish step

Tinkered around with my [[org-publish]] steps a little bit - moving the config out of my .spacemacs file and into a dedicated [publish.el](https://gitlab.com/ngm/commonplace/-/blob/master/publish.el) in my wiki project, along with a Makefile to run it as a &#x2013;batch process.  

This way, it can be better shared with others, and I can also run it in the background - org-publish is pretty slow sadly, and blocks all of Emacs when you run it interactively.

This also takes me in the direction of having the publish step actually happen as a post-receive hook on a git remote somewhere, if I wanted to do that.

It was a bit of a faff, but I learned a bit more about Emacs and org-publish in the process, and had to do some basic elisp debugging to figure out why the org-roam backlinks stuff wasn't working.  Pleased to have learned some new things!


## [[Selectively publishing to the stream]]

As I try the [[stream-first]] approach, a comment from [Bruno](http://winck.org/) at the [Garden and Streams](https://indieweb.org/2020/Pop-ups/GardenAndStream) session sticks in my head - along the lines that he had experimented with software where pretty much everything was written in to his wiki first, with simply a flag to say 'also publish this to my public stream'. 

I find that interesting as I just posted something to my stream in my wiki (a tech note to myself about Chromium disk usage), that I don't feel a particular benefit to posting to a public stream - I can't imagine anyone really wanting it popping up in their social readers.  

BUT I do want it in my own chronological timeline (as well as my longer-term garden), as I find it useful to be able to look back when something first happened.  I want to [[record the journey as well as the destination]], so to speak.

You see quite a few IndieWeb people do something along these lines, with a full 'firehose' stream you can follow, but also a more restricted subset of 'stuff I think other people will be most interested in'.


## Chromium disk usage

I just noticed that Chromium (which I very rarely use) was taking up 1.3GB of space in my .config folder.

Here's how to clean that up: [disk usage - Why does Chromium take up 1 GB in my .config and can I reduce th&#x2026;](https://askubuntu.com/questions/1038150/why-does-chromium-take-up-1-gb-in-my-config-and-can-i-reduce-that-size)

Odd that it was taking up so much space, given that my daily driver is Firefox.

