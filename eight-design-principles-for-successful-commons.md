# Eight design principles for successful commons

> -   Clearly defined the group boundaries (and effective exclusion of external un-entitled parties) and the contents of the common pool resource;
> -   The appropriation and provision of common resources that are adapted to local conditions;
> -   Collective-choice arrangements that allow most resource appropriators to participate in the decision-making process;
> -   Effective monitoring by monitors who are part of or accountable to the appropriators;
> -   A scale of graduated sanctions for resource appropriators who violate community rules;
> -   Mechanisms of conflict resolution that are cheap and of easy access;
> -   Self-determination of the community recognized by higher-level authorities; and
> -   In the case of larger common-pool resources, organization in the form of multiple layers of nested enterprises, with small local CPRs at the base level.
> 
> -   [Elinor Ostrom - Wikipedia](https://en.wikipedia.org/wiki/Elinor_Ostrom#Design_principles_for_Common_Pool_Resource_(CPR)_institution)

