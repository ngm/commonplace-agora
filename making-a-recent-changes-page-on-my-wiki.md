# Making a recent changes page on my wiki

I've changed my sitemap page to be a RecentChanges page - the pages that have changed recently, ordered anti-chronologically.  [[Here it is]].


## Why

I've found that [[Phil]]'s [recent changes](http://thoughtstorms.info/view/RecentChanges) page is a nice [[entry point]] in to ThoughtStorms.  It's a very low-effort [[stream]] - it just falls out of the changes that you are already making.

I already have a kind of recent changes list, that was just the [activity stream from the git repo of the source of my site](https://gitlab.com/ngm/commonplace/activity).  It worked OK, but I prefer this new list.

Interesting to compare 'recent changes' to my [[Recent noodlings]] - these are more curated and closer to the normal stream you would get on a Twitter-like microblog (and that I repost on my IndieWeb stream, in fact). 

I guess I'll have both, can't hurt.  Would be interesting to reflect on the difference between the two a bit more though.


## How

[[org-publish]] has a bunch of features for [automatic sitemap generation](https://orgmode.org/manual/Site-map.html).  I was already using it to list all pages, but just sorted alphabetically.  I changed it to sort 'anti-chronologically' (i.e. reverse date ordered).  I also changed the `sitemap-format-entry` function to include the last modification date.

This post was helpful:  [Blogging with Org publishing](https://vicarie.in/posts/blogging-with-org.html).


## Tweaks


### Updated date

I think the date currently comes from mtime?  Which sometimes gets changed when I do bulk changes and haven't actually touched the content.  Would be better to have something which is only changed when the content changes, perhaps pulling it out of git logs, or based on a property in the front matter of the notes themselves.


### Everything?

Currently it lists **all pages** in the garden.  Doesn't affect loading time, so not a big problem, but would it make more sense to just show the most recent X changes?

