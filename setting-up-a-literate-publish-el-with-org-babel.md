# Setting up a literate publish.el with org-babel

I've been migrating my publish.el file to use [[literate configuration]] with [[org-babel]].

For two main reasons: to make it a bit more understandable to others; and as a push for me to tidy it up and remove the bits of cruft that have accumulated over the years.

You can view it at at [[My publish.el configuration]].


## What I did

I copied the basic org-babel config that I'm using in [[My Spacemacs User Config]], which is essentiall just this property in the file header:

```emacs-lisp
#+property: header-args:emacs-lisp :tangle publish.el :comments org
```

Then, just to get the ball rolling, I copied the entire existing publish.el into one big src block and tangled that as a test.

Weirdly, I couldn't get it to tangle at first, but it worked fine after restarting spacemacs.

Now I'm in the process of splitting up that one mega src block into smaller blocks with literate annotations describing what they're for.  There's a rather excellent built-in function called `org-babel-demarcate-block` for this, which basically splits your block into two wherever your cursor currently is.

