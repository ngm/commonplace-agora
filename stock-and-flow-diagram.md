# stock and flow diagram

Stock and flow diagrams are used in [[system dynamics]] and [[systems thinking]].

They provide a more quantitative model than causal loop diagrams.  They're often a next step after a [[causal loop diagram]].

