# How We Can Encode Human Rights In The Blockchain

An
: [[article]]

URL
: https://www.noemamag.com/how-we-can-encode-human-rights-in-the-blockchain/

Author
: [[Nathan Schneider]]

[[Blockchain]].  [[Human rights]].

Nathan is going all in on blockchain of late.  Particularly the affordances it might bring for community governance.

> The same crypto tools presently being used to bypass the international order could instead become the means of architecting a better one.

A nice sentiment.

> Anyone claiming to build the infrastructure for the next world order should be expected to tell us: Will this expand or undermine the rights we have already struggled to win?

Agree.

