# Communal luxury

A term coined by the artists' section of the [[Paris Commune]].

> It means a full mobilisation of the two watchwords of the Commune: **decentralisation and participation**. It means art and beauty deprivatised, fully integrated into everyday life, and not hidden away in private salons or centralised into obscene nationalistic monumentality.
> 
> &#x2013; [[The Radical Imagination of the Paris Commune]]

<!--quoteend-->

> At its most speculative reaches, ‘communal luxury’ implies a set of criteria or system of valuation other than the one supplied by the market for deciding what a society values, what it counts as precious. Nature is valued not as a stockpile of resources but as an end in itself.
> 
> &#x2013; [[The Radical Imagination of the Paris Commune]]

