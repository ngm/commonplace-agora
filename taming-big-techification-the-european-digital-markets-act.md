# Taming Big Techification? The European Digital Markets Act

Found at
: https://projects.itforchange.net/state-of-big-tech/taming-big-techification-the-european-digital-markets-act/


[[The EU has no homegrown Big Tech companies of its own]]

the [[Brussels effect]]

> the case of the DMA serves as a learning ground for activists and policymakers concerned about the all-pervasive nature of Big Techification

<!--quoteend-->

> Although sparked by massive public investments, emergent ICT infrastructures like the internet were opened up to private corporations in the course of the neoliberal 1990s

[[Internet for the People]] goes into a lot of detail about this privatisation ([[privatisation of the internet]]).

> Despite mounting efforts, most states have thus far exhibited serious deficiencies in their capacity to rein in the advance of Big Tech

