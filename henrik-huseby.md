# henrik huseby

-   tags: [[right to repair]]

-   https://gogetfunding.com/support-the-right-to-repair/

> In 2017, Norwegian customs officials seized 63 imported, refurbished mobile screens destined for Huseby. He runs a very small repair shop in a town just outside of Oslo. Apple alleged the screens were “counterfeit”. [[Apple]] demanded Huseby sign an unfair letter, admitting to wrong-doing.
> 
> He refused. He won in his first court appearance, but Apple then won in a court of appeals. Huseby was not deterred and took the extraordinary step of appealing to the Supreme Court, which has agreed to hear his case in 2020.
> 
> &#x2013; [Restart Podcast Ep. 48: Henrik versus Goliath Corporation - The Restart Project](https://therestartproject.org/podcast/henrik-huseby/) 

