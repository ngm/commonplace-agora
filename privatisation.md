# Privatisation

"Selling state-owned businesses to private investors."


## Why?

> the same nonsensical line that we constantly hear about all privatisations—it will bring better management and financial investment. As is always the case, the opposite turned out to be true.
> 
> &#x2013; [[It's Time to Bring Energy into Public Ownership]]

