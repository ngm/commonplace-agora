# A Brief History & Ethos of the Digital Garden

URL
: https://maggieappleton.com/garden-history

Author
: [[Maggie Appleton]]


## The Six Patterns of Gardening

1.  Topography over timelines
2.  Continuous growth
3.  Imperfection &amp; learning in public
4.  Playful, personal and experimental
5.  Intercropping &amp; content diversity
6.  Independent ownership

