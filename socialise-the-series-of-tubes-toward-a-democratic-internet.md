# Socialise the Series of Tubes: Toward a Democratic Internet

A
: [[podcast]]

URL
: https://newbooksnetwork.com/socialise-the-series-of-tubes-toward-a-democratic-internet

[[Socialize the internet]]. Really enjoyed this interview with [[Ben Tarnoff]].

Gives a shoutout to cooperatively-owned social media such as certain Mastodon instances, doesn't mention by name but surely must be a reference to [[social.coop]].

At the end he outlines a rough idea of a kind of federated [[municipalist social media]] that I really like, chimes with my own thoughts.

> Recently a major outage took nearly a third of Canada offline. No phone, no internet… even access to 911 got shut down in some places. So why does one company get so much control over a vital service… the internet?
> 
> This is the story in the USA as well as Canada, so at Darts and Letters we wanted to look for a different way. We also don’t necessarily believe the market is the solution… so what is? How do we make a more democratic, socially driven internet?
> 
> Gordon Katic interviews Ben Tarnoff, author of [[Internet for the People]], to help us answer these questions - and most importantly, we ask whether the internet is indeed a series of tubes.

~00:09:40  Rogers outage.

~00:09:48  Profit motive and connectivity.

~00:12:47  What even is the internet?

~00:29:05  The privatisation of the internet.

