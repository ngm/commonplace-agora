# Battery energy storage systems

> Battery energy storage systems hold electricity generated from renewable sources such as wind turbines and solar farms before releasing it at times of high customer demand.
> 
> &#x2013; [[Cottingham: Europe's biggest battery storage system switched on]]

