# org-roam

> A plain-text personal knowledge management system.

I use org-roam for my [[PKM]] (note-taking, digital garden, etc).  Pretty much all of my digital garden is made using org-roam in fact. I started using it in March 2020.

I love it.  


## Whut

> It is a note-taking tool for [[networked thought]]. The tool encourages a non-hierarchical [[note-taking]] workflow. With Org-roam, note-taking becomes effortless and fun: write notes wherever you want, and link them up however you want. 
> 
> &#x2013; [Introducing Org Roam · Jethro Kuan](https://blog.jethro.dev/posts/introducing_org_roam/) 

Some of my customisations of org-roam are [[here]].

One downside, or at least something to mull upon - org-roam doesn't make much use of org's heading structure.  It's all file-based, each note is a file.  No problem with that right now, just it goes a little against how org mode tends to work?


## Using roam-refs

-   [Do you find Roam-Refs useful? - Meta - Org-roam](https://org-roam.discourse.group/t/do-you-find-roam-refs-useful/1965)


## Resources

-   https://www.orgroam.com/
-   https://org-roam.discourse.group

