# Updating to Emacs 28 on Linux Mint

This time, using a PPA rather than building from source like last time ([[Installing Emacs from source]])

I used this for reference: [How to Install GNU Emacs on Linux Mint 21 or 20 - LinuxCapable](https://www.linuxcapable.com/how-to-install-gnu-emacs-on-linux-mint/#install-gnu-emacs-on-linux-mint-21-or-20-via-apt).

With really the main bit of info of use being which PPA to use.

```shell
sudo add-apt-repository ppa:kelleyk/emacs -y
```

I had to manually, naughtily, uninstall the old Emacs binaries from _usr/local/bin_ that had been built from source in order for the new emacs28 binaries from the PPA to be picked up.

And, of course, not it's a new version of Emacs, spacemacs has to get all the packages from MELPA again&#x2026;

Got some errors while this was happening:

> (Spacemacs) Error in dotspacemacs/user-config: Symbol’s function definition is void: org-assert-version
> 
> Problems while trying to load feature ‘ol-bbdb’
> Problems while trying to load feature ‘ol-bibtex’
> Problems while trying to load feature ‘ol-docview’
> Problems while trying to load feature ‘ol-eww’
> Problems while trying to load feature ‘ol-gnus’
> Problems while trying to load feature ‘ol-info’
> Problems while trying to load feature ‘ol-irc’
> Problems while trying to load feature ‘ol-mhe’
> Problems while trying to load feature ‘org-protocol’
> Problems while trying to load feature ‘ol-rmail’
> Problems while trying to load feature ‘ol-w3m’
> Problems while trying to load feature ‘ol-bbdb’
> Problems while trying to load feature ‘ol-bibtex’
> Problems while trying to load feature ‘ol-docview’
> Problems while trying to load feature ‘ol-eww’
> Problems while trying to load feature ‘ol-gnus’
> Problems while trying to load feature ‘ol-info’
> Problems while trying to load feature ‘ol-irc’
> Problems while trying to load feature ‘ol-mhe’
> Problems while trying to load feature ‘org-protocol’
> Problems while trying to load feature ‘ol-rmail’
> Problems while trying to load feature ‘ol-w3m’
> Skipping check for new version (reason: dotfile)
> widget-apply: Args out of range: #&lt;buffer \*spacemacs\*&gt;, #&lt;marker (moves after insertion) at 2180 in \*compilation\*&gt;, #&lt;marker at 2192 in \*compilation\*&gt;

Which is odd, as I don't seem to have org-assert-version anywhere in my userconfig function.

Seems to be a common issue: [syl20bnr/spacemacs#15896 \`org-mode\` is broken after restarting emacs on lates&#x2026;](https://github.com/syl20bnr/spacemacs/issues/15896) 

The following worked for me:

```shell
cd ~/.emacs.d/elpa/28.1/develop
rm org-9*
```

plus a restart of spacemacs.

And we're back to working again!  The org-projectile error seems to have gone.

So, [[What's new in Emacs 28?]]

