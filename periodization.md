# periodization

> Perhaps periodization makes it easier to remember that no matter how massively entrenched the order of things seems in your time, there is no chance at all that they are going to be the same as they are now after a century has passed, or even ten years.
> 
> &#x2013; [[The Ministry for the Future]]

