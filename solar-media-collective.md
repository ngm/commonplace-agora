# Solar Media Collective

URL
: https://www.solar-media.net/

> The Solar Media Collective is a research-creation group that explores the affordances of [[solar energy for digital communication systems]] through learning by doing.

