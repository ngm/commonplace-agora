# Steve Baker

> Baker is a key member of the [[Net Zero Scrutiny Group]] (NZSG), a Tory grouping set up by his fellow Conservative MP Craig Mackinlay last summer.
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

<!--quoteend-->

> The self-described [[Brexit]] hardman, who as the then head of the [[European Research Group]] had relentlessly harried Theresa May’s government over Brexit and then become a thorn in Boris Johnson’s side with his anti-lockdown [[Covid Recovery Group]], had a new – and he believed potentially explosive – target: the government’s climate agenda.
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

<!--quoteend-->

> Baker is a trustee of the [[Global Warming Policy Foundation]] (GWPF).
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

A proponent of [[Austrian School]] economics.

