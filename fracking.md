# Fracking

> Fracking is also deeply unpopular with the public and given that any shale gas extracted would have to be sold at international market prices, it would have no impact on UK fuel bills
> 
> [The Observer view on Ukraine and the climate emergency | Observer editorial |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency)

