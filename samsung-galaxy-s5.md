# Samsung Galaxy S5

I got this in [[December 2019]].  £100 from Computer Exchange.  It was first released in 2014.  Over 6 years old and still works great.

Pros:

-   easily replaceable battery (just pop off the back)
-   ~~replacement batteries only £12 e.g. ([from ReplaceBase](https://www.replacebase.co.uk/official-samsung-galaxy-s5-replacement-battery-eb-bg900bbc))~~
    -   replacement batteries only £17.90 (from [[PolarCell]])
-   one of the top 5 devices supported by [[Lineage OS]]


## Deets

-   Battery: 2800 mAh
-   Memory: 2Gb
-   Camera: 16MP
-   5.1" screen, 1080x1920 pixels


## Retirement

I had to retire it as a daily driver in 2023.  Almost got it to a 10 year phone!

It was just creaking too much.  It ran slow, I was permanently having to delete things, clear caches in order to make space.  16Gb storage is not enough space for me it turns out.  Photo quality was really poor.  I would have liked to have kept it - I like the form factor of it, nice size, physical button, back just pops off to replace the battery.  But hey ho.

