# Eco-apartheid

> like the elites of other countries in the capitalist core, they plan to defend themselves from the worst of global heating while strengthening their borders against the inevitable wave of climate refugees. This is a world of eco-apartheid: an imperialist regime of capital accumulation predicated on the exploitation of non-human nature and racialized peoples in sacrifice zones stretching from peripheries to centers.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

