# Britain's early handling of the coronavirus pandemic was one of the worst public health failures in UK history

URL
: https://www.theguardian.com/politics/2021/oct/12/covid-response-one-of-uks-worst-ever-public-health-failures

So sayeth a report from the Commons science and technology committee.

> “Groupthink”, evidence of [[British exceptionalism]] and a deliberately “slow and gradualist” approach meant the UK fared “significantly worse” than other countries, according to the 151-page “Coronavirus: lessons learned to date” report led by two former Conservative ministers.
> 
> &#x2013; [Covid response ‘one of UK’s worst ever public health failures’ | Health polic&#x2026;](https://www.theguardian.com/politics/2021/oct/12/covid-response-one-of-uks-worst-ever-public-health-failures)


## Epistemic status

Type
: [[claim]]

Gut feeling
: 8

Confidence
: 7

Given it's a report from the government, led by two former Tory ministers, seems pretty legit. 

