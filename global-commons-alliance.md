# Global Commons Alliance

URL
: https://globalcommonsalliance.org/

> The Global Commons Alliance (GCA) is a network of organizations working together to ensure that societies and the global economy thrive, sustained by healthy global commons, on a stable planet.

