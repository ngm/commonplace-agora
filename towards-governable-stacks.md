# Towards Governable Stacks

Name of [[Nathan Schneider]]'s presentation at Commons.Hour https://forum.meet.coop/t/signup-commons-hour-session-10/1156/.

https://cloud.meet.coop/s/xaDByCiMkwEQtEL


## What happened to peer production?

-   [[Peer production]]
    -   range of activities that include OSS, crowdsourcing, etc
-   peer production has enabled corporate capture and consolidation of wealth
-   contributors is depressingly homogenous (white male hegemony)


## [[The Tyranny of Structurelessness]]

-   [[The Tyranny of Openness: What Happened to Peer Production?]]
-   unintentional structures arise in the absence of intentional structures
-   corporate capture and white male dominance
    -   an expression of ToS


## [[Social provisioning]]

![[Towards_Governable_Stacks/2022-09-26_18-14-55_screenshot.png]]

-   hidden forms of labour
-   human well-being alongside other metrics of wealth
-   correcting for unequal access to power and agency
-   asserting the validity and inescapability of ethical judgement
-   intersecting gender analysis with that of race, class and other forms of identity


## current state of open source and FOSS communities

![[Towards_Governable_Stacks/2022-09-26_18-16-21_screenshot.png]]

-   "identity politics and vulgar Marxism"
-   these are actually attempts of social provisioning


## the ethical challenge

![[Towards_Governable_Stacks/2022-09-26_18-18-21_screenshot.png]]

-   empowering developers
-   freedom and agency required to ensure it is used for social good
-   put it in the licenses


## the economic challenge

(in social provisioning)

![[Towards_Governable_Stacks/2022-09-26_18-20-16_screenshot.png]]

-   trying to have a sustainable business model
-   and preventing corporate capture
-   e.g. AWS monetising FOSS hosted on AWS
-   aspects of social provisioning here again


## fron tyranny to governable stacks

-   another approach to social provisioning
-   a crucial leverage point in peer production
-   moving from assuming that we don't need to bother with governance
-   towards toolsets that are designed to be governed
-   gandhi and the spinning wheel
    -   appropriate technology capable of being governed at the village level
    -   pre-digital version of the governable stack


## govern across the stack

![[Towards_Governable_Stacks/2022-09-26_18-23-51_screenshot.png]]

-   insuregency "entering into conflict with more colonial forces"
-   how do we make our tech stacks more governable
    -   to facilitate [[social provisioning]]
-   we don't have the infrastructure to really even think about


## what kinds of provisioning would we need to govern our stacks?

-   [[metagov]] project
-   governance layer for the internet
-   governable spaces is an upcoming book from Nathan


## Question

Re: insurgency and the original governable stack of the spinning wheel - networked social media was touted (inflatedly) as major tools in supporting the various short-lived uprisings of 2011 (Occupy, Arab Spring, etc). Twitter revolution, Facebook revolution etc. These tools have a distinct lack of self-governance built-in but supposedly helped facilitate revolutions. Would governable stacks have offered something to these movements that big tech didn't?  Stopped them from petering out?

