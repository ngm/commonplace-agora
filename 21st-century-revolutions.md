# 21st-century revolutions

[Category:21st-century revolutions - Wikipedia](https://en.wikipedia.org/wiki/Category:21st-century_revolutions)

> Yet the 21st century has already seen a succession of near-revolutionary upheavals – the rising that forced the president of Ecuador to flee the country in January 2000, the uprising that drove out the Argentine president in December 2001, the spontaneous insurgency that brought Hugo Chavez of Venezuela back to power after a right-wing coup in April 2002, the uprising that drove out the president of Bolivia in October 2003, the risings that drove out presidents in Ecuador and Bolivia in 2005, and the mass movement that overthrew the government in Nepal in the spring of 2006
> 
> &#x2013; [[Revolution in the 21st century - Chris Harman]]

