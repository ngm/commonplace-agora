# The environmental impact of a PlayStation 4

URL
: https://www.theverge.com/2019/12/5/20985330/ps4-sony-playstation-environmental-impact-carbon-footprint-manufacturing-25-anniversary

"Deconstructing Sony’s video game console is a mind-bending trip into carbon-intensive global electronics"

Carbon-intensive global electronics.

> it occurs to me that the PlayStation 4 has the most dazzling and problematic parts of global capitalism purring in unison.

<!--quoteend-->

> It is an exquisite, leanly designed machine pulsing with the exploitation of Earth and its people.

<!--quoteend-->

> In an effort to explore both the environmental and human impacts of Sony’s current video game console, I decided to take one apart. Under its plastic hood, I discovered a machine that spans continents and deep time, touches thousands of lives (for better and worse), and leaves an indelible, measurable stain on Earth and its atmosphere.

-   The case is made from [[ABS]] (so is Lego apparently)
-   steel
-   POM (cogs for the blue ray drive)
-   aluminium (heatsink)
-   gold (PCB)
-   tin (PCB)
-   copper (PCB)
-   neodynium
-   [[lithium]] (battery in controller)
    
    > It’s quite possible that the tin-based solder in your PlayStation 4, which is the glue holding its vital computerized parts together, originated from such deep exploitation.

> “You miniaturize the product, and you maximize your carbon footprint,” Barlow said. “Even though it’s very little material, the processing energy is absolutely huge, and the end of life is almost impossible. It’s basically single-use.”

<!--quoteend-->

> The equivalent of 89 kilograms of carbon dioxide is emitted into the atmosphere with the production and transportation of every PlayStation 4.

<!--quoteend-->

> Since the PlayStation 4’s release in 2013, approximately 8.9 billion kilograms of carbon dioxide have been generated and subsequently released into the atmosphere.

