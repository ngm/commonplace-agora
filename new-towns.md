# New Towns

> Like the current trend for mid-century modern design, the idea (perhaps more than the material reality) of new towns is attractive, because they’re associated with a perceived utopianism and optimism of the post-war period in Britain
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Proposed by Sir Patrick Abercrombie in his reconstruction plans of 1944–45, new towns were a bold solution to overcrowding and poor housing through decentralisation of inhabitants from London and the other big cities
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Attlee’s Labour government passed the 1946 New Towns Act among the other key pieces of legislation framing the welfare state. Stevenage was the first new town to be chosen
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Stevenage was the first pedestrian town centre in England. Not even the nearby garden cities of Letchworth and Welwyn, with their carefully planned green space, prioritised the pedestrian shopper over the car user
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Other than a discussion of the leisure activities, there is little recognition of the people who migrated to live in Stevenage, east-enders from blitzed London, and the Irish builders who physically built the houses, churches, and community centres, and then stayed put
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> The radical novelty of the post-war new towns programme lay in the centralised control of planning and finance through public development corporations
> 
> &#x2013; [[Tribune Winter 2022]]

