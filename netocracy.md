# Netocracy

> the most valuable form of economic wealth is a network of good connections, through which flow information and influence. 
> 
> &#x2013; http://thoughtstorms.info/view/NetoCracy 

Stumbled on via [[Phil Jones]]. 

Describing some of the current state of the world dominated by the Internet. 

Philosophically inspired by [[Deleuze &amp; Guattari]]?

Any relationship to McKenzie Wark [[Vectoralist class]] stuff?

