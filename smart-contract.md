# Smart contract

> It’s that the smart contracts on which DAOs are built, by their very nature, render decisions in the present on situations that were conceptualized at some arbitrary point in the past.
> 
> &#x2013; [[Radical Technologies]]

