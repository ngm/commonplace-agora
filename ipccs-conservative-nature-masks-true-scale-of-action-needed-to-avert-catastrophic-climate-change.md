# IPCC's conservative nature masks true scale of action needed to avert catastrophic climate change

> The IPCC offers many “scenarios” of future low-carbon energy systems and how we might get there from here. But as the work of academic Tejal Kanitkar and others has made clear, not only do these scenarios prefer speculative technology tomorrow over deeply challenging policies today (effectively a greenwashed business-as-usual), they also systematically embed colonial attitudes towards “developing nations”.

<!--quoteend-->

> Rather than relying on technologies such as direct air capture of CO₂ to mature in the near future, countries like the UK must rapidly deploy tried-and-tested technologies.  Retrofit housing stock, shift from mass ownership of combustion-engine cars to expanded zero-carbon public transport, electrify industries, build new homes to [[Passivhaus]] standard, roll-out a zero-carbon energy supply and, crucially, phase out fossil fuel production.

