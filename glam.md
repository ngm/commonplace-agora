# GLAM

> GLAM stands for Galleries, Libraries, Archives and Museums, a community of [[digital commons]] advocates and projects working to digitise public domain works of our cultural heritage without unnecessary legal (Mazzone, 2011), economic, or technical restrictions to their access and reuse by the public.
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

