# knowledge cycle

1.  research,
2.  read,
3.  take note,
4.  write.

&#x2013;  [Use a Short Knowledge Cycle to Keep Your Cool • Zettelkasten Method](https://zettelkasten.de/posts/knowledge-cycle-efficiently-organize-writing-projects/)

