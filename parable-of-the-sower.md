# Parable of the Sower

Author
: [[Octavia E. Butler]]

Reading this at the moment and really liking it.  It's pretty grim, but really compellingly written.

Sower seems quite explicitly about what happens following human-induced climate collapse.

In some ways I see similarities in [[84K]] to Parable.  In that you have corporations running emergency services, and people not able to afford them. And the very dispossessed going a bit wild and feral.  (Those with the 'flame' drug in Parable, the ragers in 84K).


## <span class="timestamp-wrapper"><span class="timestamp">[2021-06-30 Wed]</span></span>

The [[God is Change]] stuff is interesting.  That the most universal and consistent force in the universe is that of change.  

The flamers are interesting too.  They are sometimes presented as drug-addicted psychos.  But sometimes referenced as those who are burning the property of the rich in some kind of class struggle.

Also interested that the end game of Lauren's religion is to leave Earth and start again out in space.  I'll see how that pans out, and I'm sure it's not the intent here, but it smacks a bit of the school of thought of 'we're fucking Earth up, let's just try again somewhere else' rather than let's fix it.  


## <span class="timestamp-wrapper"><span class="timestamp">[2021-07-01 Thu]</span></span>

There's slavery in the world of Parable.  [[Debt slavery]].  At times parallels are drawn to the history of slavery in America.

