# My personal wiki setup

-   [[org-mode]], [[org-roam]] and [[org-publish]]

See also: [[Flock]].  (I should merge these pages?)

I started with just org-mode and org-publish.  As of [[March 2020]], I started using org-roam, too.

This has been a nice progression.  Using just plain org-mode got me started with minimal effort.  Then org-roam has been a great extra layer on top, when I realised I wanted more to follow the [[zettelkasten]]/[[tiddler]] approach of smaller notes linked together.  It provides a list of backlinks, as well as basic graphing/visualisation, so it goes some way to helping find the constellations, too.


## How it meets my requirements

As per [[My personal wiki requirements]]. 


### Low-friction writing

I am very tied to text-editing in Emacs (with vi bindings), so big win here so far.  To be honest I'm not a huge fan of org-mode markup for some reason, it's a bit ugly in places (I mean `#+BEGIN_QUOTE`, what the hell kind of syntax is that??), but I'm familiar with it and of course it integrates very nicely into Emacs.  


### Low-friction connections

org-roam makes it very easy to link between notes.


### Finding [[constellations]]

org-roam has backlinks and graphing capability.  To be honest, although I've kicked the tires, I haven't made much active use of the sense-making features yet though.

[[org-roam-ui]] adds a lovely graphical layer on top of org-roam.  


### Publishing capabilities

With org-publish you can pretty easily (requires a bit of [[config]]) output the static HTML.  org-roam taught me the nice idea of adding extra hook as well for org-publish, and has one for pulling in all your backlinks.  I may switch to ox-hugo at some point, for improved publishing.

See my [[personal wiki config]] for some config bits and pieces.


### Plain-text

Yep.


### Backed by version control

Yep - I use git.


### Local-first

I can write in org and org-roam without any connection.  If I want to pull in some links then I need connection, but I can just use the title.


### Libre software and longevity

Indeed so.  Emacs, org, they're FOSS++ and have stood the test of time much longer than most.  org-roam is a new kid, very active at the moment, but to be seen how long it sticks around.  But if org-roam vanished, I'd just lose some features, not all of my thoughts and ideas.  All my plain-text would still be right there.


### Interwiki linking

I added simple incoming [[webmention]] support to enable comments ([[Receiving webmentions on my digital garden]]).

I syndicate copies of all my pages to [[Anagora]].  ([[2021-11-28]])


### Misc

I like this as an approach because it uses and leverages all of the existing functionality of the Emacs ecosystem.  I'm not learning a new tool (although of course it was a new tool to me at one point, and I wouldn't really recommend all this to someone not already familiar with Emacs, it's a steep learning curve - worth it though!).


## Could be improved

-   **search** ing the HTML version. Not a big deal to be honest - I can search my local version easily enough, so it would only really be useful if I want other people to be able to search.  Or sometimes I do want to search just so I can share a specific page with someone. (At the moment I go to /sitemap and that works well enough though).  If I want improved publishing, I could use ox-hugo.  Lately I just search on [[Anagora]].
-   **structured data**, e.g. I can see the list of [[books]] I've read begging for something more structured than plain text, or the big old list of [[tracks]].  But let's see how we go.  I'm not trying to build a database.
-   could mark it up with **microformats**&#x2026;
-   **transclusion** could perhaps be nice, or some of that block include stuff Roam has.  In org-roam itself, some discussion here https://github.com/jethrokuan/org-roam/issues/317.  fedwiki/tiddlywiki style transclusion in the publish would also be nice.  A mention of that here: https://github.com/jethrokuan/org-roam/pull/263.
-   an easy to navigate **edit history** / **permalinks** - it doesn't affect my own usage of the wiki, and I do have it versioned in git, but my URLs aren't very [cool](https://www.w3.org/Provider/Style/URI) - other people might experience linkrot and struggle to see history

<!--listend-->

-   misc [[Wiki itches]].

