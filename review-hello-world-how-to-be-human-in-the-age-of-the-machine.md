# Review: Hello World - How to Be Human in the Age of the Machine

I read [[Hello World]] - How to Be Human in the Age of the Machine by Hannah Fry.  It's about the increasing pervasiveness of [[algorithmic decision-making]] in everyday life, and how much we should rely on them.

![[2020-07-11_18-23-34_screenshot.png]]

It's a really good book - very engagingly written and easy to read, on what could potentially be a pretty dense topic.  It's  full of real-world stories to ground the more abstract questions, and it also weaves into that a nice basic overview of what [[algorithms]] are, and how the latest crop of machine-learning algorithms work. 

So briefly - very broadly an algorithm is just a set of step-by-step logical instructions that show, from start to finish, how to do something. However generally the world algorithm is used a bit more specifically, still in some sense a set of step-by-step instructions, but a more mathetmatical and defined series of steps, and usually run by a computer.

And when people talk about whether algorithms are good or bad, they pretty much always mean decision-making algorithms - something that makes a decision that affects a human in some way.  So for example long division is an algorithm, but it's not really having any decision making effect on society.  We're talking more about things like putting things in a category, making an ordered list, finding links between things, and filtering stuff out.  And they might be 'rule-based' expert systems, in that the creator programs in a set of rules that the system then executes, or more recently machine learning algorithms, where you train an algorithm on a dataset by reinforcing 'good' or 'bad' behaviour.  Often with these we can't always be sure how the algorithms has come to a conclusion.

So what the book is really focused on is the effect our increased use of decision-making algorithms like these is having on things like power, advertising, medicine, crime, justice, cars and transport, basically stuff that makes up the fabric of society,  and where we're starting to outsource these decisions to algorithms.

The book does a really good job of explaining some of the problems in outsourcing those decisions.

One big problem being that we have a tendency to trust the decision made by a computer.  But we have to really aware of the biases in these systems.  Part of this bias is part of the bigger problem endemic in the tech industry - that's it's overrepresented by white men who have a very limited world view and a particular set of biases.  The system is often going to be made in the image of its creator, right.  

But aside from that ML can also biased in that if the data that goes in to them is biased, so will the outcomes be.  Garbage in, garbage out.  And there's a lot of biases and garbage statistics in the world.  So say if policing disproportiately  targets a particular group in arrests and justice treats them differently in sentencing, then they're more likely to be targeted by an algorithm based on existing policing and crime stats. You have to really challenge existing biases, not build them in to the system.  

The book is very even-handed, and isn't a polemic against machine learning by any regards. There are plenty positives, like image classification of tumours where ML at great speed cases that a pathologist should look at in more detail.  

I really liked the conclusion that we should not see machine learning decision making as an either or.  Like either we hand it over to machine learning, or we keep everything.  It gives the great example of '[[centaur chess]]', where a human plays with an artificial intelligence against another human with an artificial intelligence.  Interesting this is something being championed by Gary Kasparov,  who was famously beaten by IBM's Deep Blue AI at chess a few decades back.  It opens up new possibilities where AI is complementary and not a replacement.

I think my criticism with the book would be that it doesn't really challenge the framing of the debate around ML.  So its lettting the current arbiters of ML set the agenda to some degree, and then the critcism is in the details and not the higher level.  So I mean there's a whole chapter on whether we have [[driverless cars]] or not? But no mention of whether we should rather be endeavouring to take cars off the road completely.  And with regards to things like [[predictive policing]] there is no questioning of the idea of policing as an institution in the first place, just a question of how we should use algorithms within it.  And there isn't a single mention of [[climate change]] which I found pretty amazing.  

But still it does a great job of outlining the positives and pitfalls of decision-making algorithms.  I'd recommend it, I'd just like the follow up book to be how we can use them for more [[liberatory]] purposes!

