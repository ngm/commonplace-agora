# Capitalist Catastrophism

Author
: [[Kai Heron]]

.

-   [[Capitalist Realism]]
-   [[End of history]]
-   Capitalist catastrophism is a mutation of capitalist realism.

> Unless there is a radical break from capitalism — a revolution — what will supplant capitalist realism is not the ability to imagine and fight for a post-capitalist future as Mason, Uetricht and Milburn had hoped, but something more ambiguous and perhaps ultimately worse. I call this something worse “capitalist catastrophism.”

<!--quoteend-->

> Capitalist catastrophism is what happens when capitalist realism begins to fray at the edges. It describes a situation in which capitalism can no longer determine what it means to be “realistic,” not because of the force of movements assembled against it but because capital’s self-undermining and ecologically destructive dynamics have outstripped capitalism’s powers to control them.

