# Dune

&#x2013; Frank Herbert

-   tags: [[books]]

Read this for the first time in Feb/[[March 2020]].  

I started off really enjoying it. 

> It feels a bit like a fantasy book, but with little hints of sci-fi thrown in here and there. I like the way it’s written. I like how the characters have these little thoughts as asides now and then.
> 
> Feuding dukes doesn’t really get me going plot wise, but some of this CHOAM company stuff seems like it could be interesting.

About two thirds through my interest was waning a bit.

> I’m about two thirds of the way through Dune.  Still well written but all this stuff about the machinations of Fremen culture and Paul going off his nut and having visions all the time, I’m finding a bit less compelling plot-wise.  Also I don’t like things that are underground or in caves.  Makes me feel claustrophobic.

I finished it off overall enjoying it, but not **loving** it, like a few people seem to.  I'd probably read the next in the series - maybe you get more out of it as a series - but I'm not rushing to.  I'm keen to see the film though.

> Finished Dune off the other night. It was a bit Shakespearian, duelling families, noble characters, sweeping arcs of history and all that. Soliloquys. I did enjoy it and finished it off quickly. But on reflection I can’t say I really actually **liked** any of the characters very much. Interesting yes, do I care what happens to them, not so much.
> 
> On the plus side I can watch the David Lynch film now.

