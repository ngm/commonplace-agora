# Frank Jacob, "Wallerstein 2.0: Thinking and Applying World-Systems Theory in the 21st Century"

URL
: https://newbooksnetwork.com/wallerstein-2-0

> [[Immanuel Wallerstein]]'s [[world-systems theory]] can help to better understand and describe developments of the [[21st century]]. The contributors of Wallerstein 2.0: Thinking and Applying World-Systems Theory in the 21st Century (Transcript Publishing, 2023) address ways to reread Wallerstein's theoretical thoughts in the humanities and social sciences. The presented interdisciplinary approach of this anthology intends to highlight the broader value of Wallerstein's ideas, even almost five decades after the famous sociologist and economic historian first expressed them.

Very interesting.  Apparently in some ways world-systems theory is very related to [[Marxism]].  Has a lot on core and periphery ideas.

At the end says that Wallerstein and [[Walter Rodney]] had a lot in common.

