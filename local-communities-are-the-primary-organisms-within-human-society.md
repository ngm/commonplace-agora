# Local communities are the primary organisms within human society

So sayeth [[Community-oriented life at human scale]]

-   In contrast to individuals, corporations, and nation states
-   According to the lenses of [[evolutionary biology]] and [[cultural evolution]]
-   Especially small groups of 20 to 100 people


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

