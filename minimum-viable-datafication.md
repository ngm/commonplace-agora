# minimum viable datafication

> minimum viable datafication—to collect data to develop functions in the present rather than to intensify future use—as a way to model an alternative approach
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Minimum viable datafication, then, is a necessary but not sufficient condition for facilitating relationships between people, governments, and spaces of urban encounter and securing and expanding communication rights. It is not necessary to assume that digital data is a store of value or a material for training future automated systems.
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> This does not mean never collecting data; it does not mean never employing AI to parse patterns, or sensing devices to contribute to decision-making.
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> It means following a strategy of minimizing, rather than maximizing, this kind of data, and it means seeking to employ decision-making strategies that may appear to be more costly on the surface but that leave space for different kinds of knowledge, as well as for data to decay over time, for frictions to be identified and addressed, and for different forms of democratic participation and accountability, including but not limited to data audit, sensing citizenship, and autonomous networking.
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

