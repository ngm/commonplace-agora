# Common: On Revolution in the 21st Century

URL
: [Common: On Revolution in the 21st Century: Pierre Dardot: Bloomsbury Academic](https://www.bloomsbury.com/uk/common-9781474238618/)
    
    > only a practical activity of commoning can decide what will be shared in common and what rules will govern the common's citizen-subjects
    
    <!--quoteend-->
    
    > This re-articulation of the common calls for nothing less than the institutional transformation of society by society: it calls for a revolution.


[Interview with Pierre Dardot and Christian Laval on the Politics of the Commo&#x2026;](https://blog.p2pfoundation.net/interview-with-pierre-dardot-and-christian-laval-on-the-politics-of-the-common/2015/07/24)

