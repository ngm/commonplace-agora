# Anarchism and right to repair

[[Anarchism]].  [[right to repair]].

> Anarchists often get caught up in the world of ideas, but right to repair gives us a chance to engage practically and I’d encourage all of us to get involved.
> 
> &#x2013; [Center for a Stateless Society » Right to Repair](https://c4ss.org/content/55471) 

