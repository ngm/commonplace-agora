# Cloudflare and Stitcher

A site I work on was having problems with the podcast listings no longer updating in Stitcher.

It turned out that Stitcher's crawler was being blocked by the [[Cloudflare]] firewall.

Initially I tried adding a firewall rule to always Allow requests from the StitcherBot.  However, this didn't work.

Turning off Bot Fight Mode temporarily is what ultimately worked.  It seems Bot Fight Mode overrides your own firewall rules?  A bit of a shame.

I don't want to leave Bot Fight Mode turned off, so have turned it on again.  Either I get to the bottom of why the firewall rules were not being respected, or I just turn it off periodically to allow StitcherBot to get what it needs, then turn it back on again.

