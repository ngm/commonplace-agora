# Friedrich Engels

[[Marxism]].

> Marx's friendship with another fellow-German, Frederick Engles, had the greatest effect on him.
> 
> &#x2013; [[Marx for Beginners]]

<!--quoteend-->

> The son of a rich textile manufacturer, he left Prussia in the 1842 to work as a business agent for his father's branch office in [[Manchester]].  Engles was a restless 'left' Hegelian anyway, but first-hand contact with working class misery affected him deeply.
> 
> &#x2013; [[Marx for Beginners]]

