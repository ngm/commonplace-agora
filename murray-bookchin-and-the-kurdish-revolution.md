# Murray Bookchin and the Kurdish Revolution

&#x2013; [Murray Bookchin and the Kurdish resistance | ROAR Magazine](https://roarmag.org/essays/bookchin-kurdish-struggle-ocalan-rojava/) 

-   good article
-   bit of history of [[Bookchin]], bit of history of Ocalan/PKK
-   "[[Libertarian municipalism]] promotes the use of direct face-to-face assemblies in order to 'steal' the practice of politics back from the professional, careerist politicians and place it back in the hands of citizens."
-   "many anarchists refused to adopt his ideas, unwilling to accept that, in order to remain politically relevant and be able to make a real revolution, they would have to participate in local government."
-   "The concept of [[dual power]] has been one of the main reasons why Bookchin's body of work was rejected by anarchist, communist and syndicalist groups.  Rather than advocating the abolition of the state through an uprising of the proletariat, he suggested that by developing alternative institutions in the form of poular assemblies and neighborhood committees - and notably by taking part in municipal elections - the power of the state could be 'hollowed out' from below, eventually making it superfluous."
-   very interesting to see dual power again
-   apparently it was Bookchin's conception?
-   basically this idea of working within the current system, but hollowing it out from the inside.  And then having something ready to go in it's place when it inevitably fails.
-   interesting also the mention that a lot of traditional kurdish values don't necessary fit with democratic confederalism (and some of the feminist side of it in particular) - but the young people involved in it are trying to bring those ideas in to society.
-   three main elements from Bookchin that influenced Kurdish ideas of democratic modernity: dual power; confederalism; social ecology
-   politics vs statecraft.  Politics is like citizen engagement; statecraft is career politicians.

