# Social industry

I think I heard this term first on the interview with Richard Seymour on Novara FM about his book [[Twittering Machine]].

It's probably important to make a distinction between social media per se and the **social industry**.  (See:  [[culture industry]]).  Social software can be great.  It's industrialisation is not great.


## What's the problem?

This sums up my gripes with the current form of social media I think.  It is:

> A web seen as a tool for self-expression rather than a tool for thought.
> 
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

<!--quoteend-->

> If Debord were alive today, he would almost certainly extend his analysis of the spectacle to the Internet and social media. Debord would no doubt have been horrified by social media companies such as Facebook and Twitter, which monetize our friendships, opinions, and emotions. Our internal thoughts and experiences are now commodifiable assets. Did you tweet today? Why haven’t you posted to Instagram? Did you “like” your friend’s photos on Facebook yet?
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Platforms optimize for chatter as “engagement,” not decision, resolution, or consensus. Community control is not in the spec.
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]


### Corporate power

I do not like the power that big corporations such as Facebook and Twitter have.  They have a monopoly over the social graph, and have centralised everything.

The activities of Cambridge Analytica, for example. See also: [[screen capitalism]].

> Facebook has three defining attributes that make Facebook Facebook. Its scale of 2.4 billion people uploading content in more than 150 languages make it too big to filter. Its algorithmic design that amplifies content judged to attract attention and interaction (clicks, shares, likes, comments) favors extremism and powerful emotions over rational and measured expression. And the cheap and effective advertising system is monumentally profitable and thus starves other sources of good information of needed revenue.
> 
> &#x2013; [Mark Zuckerberg doesn’t understand free speech in the 21st century](https://www.theguardian.com/commentisfree/2019/oct/18/mark-zuckerberg-free-speech-21st-century) 

<!--quoteend-->

> Unsurprisingly the chaos in Washington DC, and the response of Twitter, Facebook, and other platform corporations have brought new urgency to the growing debates about social media in particular and the digital economy more generally. 
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

^ problem with big platforms


### Personal benefits of an independent web

The IndieWeb wiki has a [why](https://indieweb.org/why) page with a good list of personal reasons why it's good to have an open platform for social interaction.


### Our collective data should be used for good

Whoever controls the media, controls society.

> The data we can collect to make better decisions about our environment, transport, healthcare, education, governance and planning should be used to support the flourishing of all people and our planet.  Right now, so much of this data, so much of our social digital infrastructure is owned, designed and controlled by a tiny elite of companies, driven by profit.
> 
> &#x2013; [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Irina Bolychevsky

Alienation.

> Convinced that they had a moral duty to shape their own lives, and without a language to share the sense of guilt that their struggles often gave them, a large proportion of those we met felt that separation and stoic acceptance was the natural order of things.
> 
> &#x2013; [Microattunement: a Pattern Language to Wake Up Our Humanity. Part 1: Why the the time is now](https://medium.com/between-us/a-unity-of-unmerged-voices-a-proposal-to-synchronise-our-efforts-through-microattunement-870922e7082f)


### A counterpoint

-   https://twitter.com/vgr/status/1047925106423603200


## What's so good about social media anyway?

Communication is a huge part of being human.

> as a social species, our social and digital infrastructure is of vital importance
> 
> &#x2013; [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Irina Bolychevsky

One of the huge possibilities of social media: allowing previously silenced voices to be made public, and surfacing injustices previously hidden.  

> over the past 10 to 15 years, ordinary individuals have been radically empowered with the ability to record, publish, and broadcast information to millions around the world, at minimal cost.
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

<!--quoteend-->

> The loss of gatekeeping authority held by legacy media institutions has opened up opportunities for long-suppressed groups to have their narratives heard: Palestinians, African-American activists, feminists, environmentalists, and dissident groups working in authoritarian societies can all find ways, not always without some trouble, to be heard.
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

<!--quoteend-->

> Citizen journalists and accidental activists helped change the course of history during uprisings in Egypt, Bahrain, Tunisia, Syria, and Libya — as well as during Israel’s 2014 war against Palestinians in the occupied Gaza Strip. Very quickly, people who were once considered to be victims of war and great-power politics have become empowered as political actors.
> 
> &#x2026; 
> 
> Broadcast directly onto the global public spheres of Twitter and Facebook, however, accounts of Palestinian suffering and resistance became impossible for the world to ignore.
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

<!--quoteend-->

> Social media’s ability to bypass traditional media gatekeepers also blew apart the biggest barriers to marginalized voices being heard: political and corporate control over publishing
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

The power of democratizing media access makes it feel all the more important to stop it from coming under the control of a minority of organisations.   It is mediated again, and marginalised voices are regularly being excluded from the discourse again.

> Through social media’s ability to give accounts from multiple separate sources on the ground, verify information, and share evidence, outside observers can better evaluate the credibility of reports from the ground.
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

Theoretically, a plurarity of voices can lead to more verifiable reports.  We've seen the spread of fake news on social be a bit of a counter to that though.  Improved media education is probably needed for that.


## Is the aim to get rid of social media?

No.  Just get it out of the hands of a few corporations.

> &#x2026;the old media environment in which billions of people had little access to getting their stories told – in which entire classes of people were effectively deemed by media institutions as not worth reporting on – is not something that we should want to return to. We should address the problems that exist with new media, not try to turn back the clock and deem this all a failed experiment.
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)


## What can we do about it?


### Make alternatives

Alternatives exist.  I am active on the [IndieWeb](https://indieweb.org) and the [Fediverse](https://en.wikipedia.org/wiki/Fediverse), for example. 

-   interesting POV on what the goals of an alternative social network should: https://social.mecanis.me/@er1n/103121562671518126


### Remove monopolies

The chief reason it's hard to move the masses away from Facebook, for example, is simply that they have a monopoly.  People don't want to move if that's where all their friends are.

> People aren't choosing the platforms because they are better anymore, they default to them because that's where everyone else is.
> 
> &#x2013; [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Irina Bolychevsky

<!--quoteend-->

> can choose services which serve my needs better, not based on the fear of social exclusion or missing out
> 
> &#x2013; [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Irina Bolychevsky

<!--quoteend-->

> As soon as people have meaningful choice, exploitation and abuse become much harder and more expensive to maintain
> 
> &#x2013; [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Irina Bolychevsky


#### How?

Make them interoperate (e.g. [Instead of breaking up big tech, let’s break them open](https://medium.com/@shevski/instead-of-breaking-up-big-tech-lets-break-it-open-7535b59dc2f6), Katarzyna Szymielewicz's talk at MozFest 2019 on regulating APIs - sadly [the video](https://www.youtube.com/watch?v=bSzC1By1Dnc) seems to be missing audio&#x2026;)

It's currently hard to interoperate.  For example, Kicks Condor has an app that tries to do it, and has to resort to scraping for many of the silos, which isn't sustainable.

> The Shittiest Thing And, actually, the worst part is that all of these sites are tough to crack into. For most blogs, I use RSS. No problem—works great. Wish I didn’t have to poll periodically—wish I could use Websockets (or Dat’s ‘live’ feature)—but not bad at all.  For Soundcloud and Twitter, I have to scrape the HTML. I’m even trying to get Facebook (m.facebook.com) scraping working for public pages. But this is going to be a tough road—keeping these scrapers functional. It sucks!  I wish there was more pressure on these sites to offer some kind of API or syndication. But it’s just abyssmal—it’s a kind of Dark Ages out there for this kind of thing. But I think that tools like this can help apply pressure on sites. I mean imagine if everyone started using ‘reader-like’ tools—this would further development down the RSS road
> 
> &#x2013; Kicks Condor

Ryan Barrett builds lots of bridges, and has to employ lots of MacGuyver techniques in order to connect to Facebook and Instagram.  


## The user experience myth

I think it’s a bit of a misused and overused trope to say that people use centralised services because they have a better user experience.

The only definition of user experience that puts Facebook or Twitter ahead of alternatives is a broad definition that includes the network effect (people I know are on it) and familiarity (it’s what I’m used to).

But in such a broad definition, I would then include things such as ‘you manipulate me with ads’, ‘you steal my attention’. This is bad UX.

In a narrow definition of user experience – how easy is it for me to sign up; how easy it is for me to share an image; how easy is it for me to share a note; there is nothing special to Facebook or Twitter in these regards.

> Think about that for a minute — how much time we’ve all spent arguing, promoting our ideas, and how little time we’ve spent contributing to the general pool of knowledge.
> 
> Why? Because we’re infatuated with the stream, infatuated with our own voice, with the argument we’re in, the point we’re trying to make, the people in our circle we’re talking to.
> 
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

