# knowledge commons

"the mutualization of productive knowledge"

> The term "knowledge commons" refers to information, data, and content that is collectively owned and managed by a community of users, particularly over the Internet.
> 
> &#x2013; [Knowledge commons - Wikipedia](https://en.wikipedia.org/wiki/Knowledge_commons)


## Examples of knowledge commons

> The knowledge commons is a model for a number of domains, including **Open Educational Resources** such as the MIT OpenCourseWare, free digital media such as **Wikipedia**,[4] **Creative Commons** –licensed art, open-source research,[5] and open scientific collections such as the **Public Library of Science** or the **Science Commons**, **free software** and **Open Design**.[6][7]
> 
> &#x2013; [Knowledge commons - Wikipedia](https://en.wikipedia.org/wiki/Knowledge_commons) 

-   [[Open Educational Resources]]
-   [[Wikipedia]]
-   [[Creative Commons]]
-   [[Public Library of Science]]
-   [[Science Commons]]
-   [[Free software]]


## Knowledge commoning

> Once again, the promise of a knowledge commons is best made evident in the disagreements and difficulties in determining who and how it should be managed
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Knowledge commons is a misnomer bcos there is no such thing as knowledge. (!!)
> 
> What there IS/ARE is/are **practices** of knowing, communicating and organising.
> 
> So a 'knowledge commons' is a commons of literacy and (collective) labour power, thro which  commoners are able to capably understand and organise their practical life as a commons, in a world of commons. It's a cultural commons.
> 
> &#x2013; [[Mike Hales]] https://social.coop/@mike_hales/107430510590782176


## Resources

-   [[Understanding Knowledge as a Commons]]

