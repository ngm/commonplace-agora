# Complex Adaptive Systems, Systems Thinking, and Agent-Based Modeling

URL
: https://link.springer.com/chapter/10.1007/978-3-319-47295-9_1

> Systems thinking and complex adaptive systems theories share a number of components, namely emergence, self-organization, and hierarchies of interacting systems.

