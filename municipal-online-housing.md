# Municipal online housing

Been thinking lately that it could be a good municipal function to provide people with access to an ‘online home’, analogous to ensuring provision of physical homes.

In the same way it could be social, affordable, in a co-op, heck even (but hopefully not) private and rented. The municipality provides some infrastructure and codes/regulations to make sure there’s a home for everyone and that everyone can move freely if they want. But equally you can build your own home or move into an intentional community if you want and have the wherewithal to do so.

Not talking about a [[StateBook]] – if the state has any function in it, I think it should be regulating for open protocols and standards, or even just bare minimum access to data and data portability (newsocialist.org.uk/do-we-really-need-a-statebook/). I’m thinking more like [[Indienet]] – (indienet.info/) – the project in Ghent (coordinated by @aral@mastodon.ar.al) to provide each denizen with their own connected node in a wider p2p/federated network. I mean municipal more in the sense of libertarian municipalism, self-determination and federation of villages, towns, cities.

Obviously access to physical housing is a mess, at least where I’m currently living, so maybe not the best reference point. But I’m finding it an interesting framing. Every Facebook or Twitter profile is currently a home on the web, and it’s as if billions of people all have the same corrupt landlord.

This is kind of implicitly assuming that everyone **needs** a home on the web. That is certainly a debatable point. It is definitely becoming more of a part of the fabric of everyday life, and you could argue that it shouldn’t be.  I vacillate on this a bit but overall I tend to think that the benefits can outweigh the negatives, when it has a social motive and not a profit motive.

