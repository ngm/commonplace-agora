# Culture, Power and Politics: Ecosocialism and Degrowth

A
: [[podcast]]

URL
: https://culturepowerpolitics.org/2023/06/05/ecosocialism-and-degrowth/

Featuring
: [[Richard Seymour]] / [[Jeremy Gilbert]]

[[Ecosocialism and degrowth]].

Very erudite-sounding philosophical reflection on degrowth by Seymour at the beginning.  Sounded good, think I'd need to relisten a few times to grasp it all.  Mentions [[Degrowth communism]].

-   ~00:31:30: Gilbert reiterates here that socialism and any politics has to be environmentally-minded in the 21st century. Hence ecosocialism.  [[To be a 21st century socialist is to be an eco-socialist]].

Interesting stuff from Gilbert, explaining '[[totality]]'.  He says at some point that it's not that much different from [[hegemony]] and [[conjuncture]].

At points the discussion (I think around totality) sounded not dissimilar to [[systems thinking]].  There was chat about [[ecology]], [[relationality]], an attempt to understand the whole based on relationships among the parts.  Would like to relisten to that bit.

Seymour says the theory around [[metabolic rift]] is very useful.

They mention [[Post-Growth Living]] as a book they really rate.

