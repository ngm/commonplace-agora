# Freifunk

[[mesh networking]].

> self-hosted and community-maintained mesh network
> 
> &#x2013; [[Decentralized and rooted in care: envisioning the digital infrastructures of the future]]

<!--quoteend-->

> In 2003, Freifunk, the first experiments with mesh networks at a neighborhood scale, started in Friedrichshain, in what had been East Berlin.
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Using a radio technology called [[mesh networking]] that generated stronger links the more radios were added to the network, they sought to create a local communication network that would proliferate and self-perpetuate
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> These buildings could have dozens of apartments, and at K9, apartments were inhabited in rotation so that all building residents would have an equal chance to live in the larger or more beautiful spaces
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Like free software, the mesh radio networks that Freifunk used needed not only maintenance but also knowledge about how to keep the radio gear working so that people away from the internet access link would still be connected: in practice, this involved climbing up to the rooftops of the seven-story blocks of the neighborhood, checking cables, and sometimes writing (free) software to solve a problem
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

