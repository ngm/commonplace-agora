# Vanguard stack

[[Vanguard stack]]s became [[Governable stacks]].

> a framework for self-governing as resistance to digital colonialism
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

-   > Durable organizing among digital subjects, I argue, will benefit from a recouperation of vanguardism: the out-of-fashion doctrine that marshaling effective discontent against a repressive regime requires disciplined, self- organizing cadre
    > 
    > &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

[[Nathan Schneider]].
[[Self-governance]].
[[Digital self-governance]].
[[Digital colonialism]].

> The lifeblood of the vanguard stack is not its tools but the self-governance surrounding them. Communities, families, and movements can assemble and adjust their stacks over time, wherever possible seeking to make their technological lives ever more vanguardist.
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]


## Traits

A vanguard stack can be evaluated on its support for 
[[Sovereignty]], [[Democracy]], and [[Insurgency]].


## Some vanguard stacks

Nathan mentions a few examples in the article:

-   [[May First Movement Technology]]
-   [[framasoft]]
-   [[Archive of Our Own]]

