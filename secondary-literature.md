# Secondary literature

> Secondary literature refers to material in which information that has appeared in another form is repackaged and disseminated more widely. Examples are [[review article]]s, academic textbooks and course materials. Secondary literature is a useful way into a new research area, and it’s generally easier to read than a primary research paper, although it’s harder to evaluate in terms of authenticity and potential bias. 
> 
> &#x2013;
> https://learn2.open.ac.uk/mod/oucontent/view.php?id=2008267&section=1.3.1

