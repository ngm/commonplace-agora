# framing device

> a particular way of perceiving the world that makes sense of other symbols and concepts within it and provides meaning to the person using the framing device
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

<!--quoteend-->

> Maps are perhaps the most common type of framing
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

