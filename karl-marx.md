# Karl Marx

> [[capitalism]]’s greatest analyst and critic
> 
> &#x2013; [[Breaking Things at Work]]

Marx was attracted to Hegel's philosophy of history.

Whatever you think of Marx, you can't deny his work has had a profound impact on history - so worth knowing about from this perspective alone.

