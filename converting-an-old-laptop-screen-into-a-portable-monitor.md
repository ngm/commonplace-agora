# converting an old laptop screen into a portable monitor

I have [[19 broken laptops]].  One of the smaller ones should do the job for this.  Which one?

-   Seems like I'll need an LVDS controller board.
    -   possibly also called LCD controller?
    -   will need to know the model number for the LCD screen
-   will need a power adapter too
-   something to attach it to - an old clipboard could do&#x2026;


## Resources

-   [How to turn Old Laptop Screen into External Desktop Monitor - YouTube](https://www.youtube.com/watch?v=6L0TPJEXiAI)
-   [How to Convert a Laptop LCD Into an External Monitor. : 15 Steps (with Pictur&#x2026;](https://www.instructables.com/How-to-Convert-a-Laptop-LCD-into-an-External-Monit/)
-   [Recycle your old laptop display and turn it into a monitor](https://louwrentius.com/recycle-your-old-laptop-display-and-turn-it-into-a-monitor.html)
-   [How To Convert Your Old Laptop Screen Into An External Monitor | Slashdigit](https://www.slashdigit.com/convert-old-laptop-screen-external-monitor/)

