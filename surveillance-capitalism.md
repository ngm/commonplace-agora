# Surveillance capitalism

[[Surveillance]] [[Capitalism]].

A specific type of [[information capitalism]].

> The declaration thus established a radically disembedded and extractive variant of information capitalism that can I label  “surveillance capitalism.”
> 
> [&#x2026;]
> 
> Does [[information capitalism]] have to be based on surveillance. No. But surveillance capitalism has emerged as a leading version of information capitalism.
> 
> &#x2013; [Shoshan Zuboff on “Big Data” as Surveillance Capitalism](https://www.faz.net/aktuell/feuilleton/debatten/the-digital-debate/shoshan-zuboff-on-big-data-as-surveillance-capitalism-13152525.html?printPagedArticle=true#pageIndex_2)

.

