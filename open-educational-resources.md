# Open Educational Resources

> In 2001 the OER movement began in earnest, when the [[Massachusetts Institute of Technology]] (MIT) announced its Open CourseWare initiative. MIT’s goal was to make all the learning materials used by its 1800 courses available via the internet, where the resources could be used and repurposed as desired by others, without charge.
> 
> &#x2013; Open University

<!--quoteend-->

> Many resources on the web are ‘free’, and while ‘no cost’ is a key component of this, what is also central to this definition is the stress on the licence that permits free use and re-purposing. In order to satisfy the definition, it is not enough to simply be free (‘no cost’), it also has to be reusable.
> 
> &#x2013; Open University

