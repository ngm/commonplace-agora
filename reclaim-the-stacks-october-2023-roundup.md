# Reclaim the Stacks: October 2023 roundup

I've been reading [[The Internet Con]] by [[Cory Doctorow]].  The strapline is [[Seize the means of computation]], which is very Reclaim the Stacks adjacent.

> This is a book for people who want to destroy Big Tech

Doctorow's main weapon for this destruction is [[interoperability]].  There's three types: voluntary interoperability (often achieved through standards), indifferent interoperability (not because anyone particular cares, but because they don't care enough to block it), and [[adversarial interoperability]] (the guerilla warfare of interoperability, AKA  [[competitive compatibility]]).

I'm all in for adversarial interoperability.  But as discussed in [[The Material Power That Rules Computation (ft. Cory Doctorow)]], the technology to build competitive compatibility is one thing - coming up against the laws that have been built to protect [[intellectual property]] is another.  ([[We should phase out intellectual property]]).

Degrowth is a big part of (some flavours of) [[ecosocialism]].  Therefore, [[digital degrowth]] might also be a part of [[digital ecosocialism]].

