# Peer Governance

Part of the [[Triad of Commoning]].


## Patterns

As include in [[Free, Fair and Alive]].

-   Bring Diversity into Shared Purpose
-   Create Semi-Permeable Membranes
-   Honor Transparency in a Sphere of Trust
-   Share Knowledge Generously
-   Assure Consent in Decision Making
-   [[Rely on Heterarchy]]
-   Peer Monitor &amp; Apply Graduated Sanctions
-   Relationize Property
-   [[Keep Commons &amp; Commerce Distinct]]
-   [[Finance Commons Provisioning]] / [[Choose Commons-Friendly Financing]]


## What is it?

> As we thought about how coordination works in a commons, we hesitated to use the term “governance” because it is so closely associated with the idea of collective interests overriding individual freedom.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> peer governance rather than just governance. It points to an ongoing process of dialogue, coordination, and self-organization.
> 
> &#x2013; [[Free, Fair and Alive]]

^ sounds quite similar to ideas in [[Anarchist Cybernetics]].

> Our analysis of Peer Governance therefore moves beyond Ostrom’s landmark design principles in several ways. First, we look at all sorts of contemporary commons — social, digital, and urban, among others — not just at natural resource-based commons. We also attempt to go beyond resource management and allocation as primarily economic matters, and instead emphasize commoning as a social system. Any assessment of governance in commons must deal squarely with the systemic threats posed by markets and state power, so we look to Peer Governance as a form of moral and political sovereignty that works in counterpoint to the market/state.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> by Elinor Ostrom is helpful, but ultimately not enough. The principles do not provide sufficient guidance for people to respond flexibly to feedback
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> we cannot depend upon structures to do the work of culture. 
> 
> &#x2013; [[Free, Fair and Alive]]

[[Money-lite commoning]].  

> how can I organize my life in such a way that I become less dependent on money? How do I decommodify daily life? Similar questions should be asked at the level of a project, initiative, infrastructure, or platform.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Commoning provided what we would now call a [[basic income]] — access to resources that ensure one’s basic survival.
> 
> &#x2013; [[Free, Fair and Alive]]

