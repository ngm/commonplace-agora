# David Ehrlichman, "Impact Networks: Creating Connection, Sparking Collaboration, and Catalyzing Systemic Change"

A
: [[podcast]]

URL
: https://newbooksnetwork.com/impact-networks

.

-   Praises [[Donella Meadows]] and her work on where to intervene in a system.
-   Relationship between networks and systems.
-   Impact networks - it's all about coordination.
-   Networked organisation.  Tension between the parts and the whole.
-   Only through divergence do we find convergence.
-   Planning for emergence. (Perhaps relates to Social tipping points?)
-   [[Network weaving]].
-   Infusing energy in a network.
-   Pockets of possibility.
-   Building a network is like cultivating a garden.
-   Creating systemic change is a marathon not a sprint.
-   David is working in the Web3 / DAO space lately.  He sees it as a new system to challenge the old.
-   One option is to focus locally, grow pockets of possibility, try to weave networks.  Web3 can give tools and infrastructure to allow people to coordinate at larger scales than before.  Trustless systems - coordinating when we don't know anything about each other.
-   Some of it feels a bit incrementalist.
    
    > Solving complex problems like climate change or homelessness demands intense collaboration between diverse organizations and individuals. In his book, David argues that a network approach combines the strategic rigor and agility of modern organizations with the deep connection and shared purpose of communities.

> Drawing on his experience working with over fifty impact networks over the past decade, David describes how to cultivate a network mentality. He then goes deeply into the five Cs of creating impact networks: \* clarify purpose and principles \* convene the people \* cultivate trust \* coordinate actions \* collaborate for systems change. Given the increasing urgency of the issues we face, impact networks have never been more essential.

<!--quoteend-->

> What I love about this book—and what I enjoyed so much about our conversation—is the opportunity for exploring the potential of human networks (and networks of networks!) to bring about significant systemic change. On the relationship between systems and networks, David writes that "the networks that underlie systems—organizational, social, planetary—have a huge influence on how healthy and effective these systems are". I enjoyed getting to ask David about his thoughts on network leadership—and what it means for this work to be grounded in the wisdom of living systems. 

