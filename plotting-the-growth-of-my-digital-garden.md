# Plotting the growth of my digital garden

![[2022-02-05_14-36-11_screenshot.png]]

March 2020 was when I started using org-roam.

This post has a handy script for pulling out a count of the files in your git repo over time: [{Git} Graph file count vs time | Good Code Smell](https://blog.benoitblanchon.fr/git-file-count-vs-time/)

```shell
#!/bin/bash

OUTPUT=stats.csv

# create output file with a CSV header
echo "date,files" > $OUTPUT

# function that counts files matching the specified regex
count() {
    git ls-tree -r --name-only $COMMIT | grep -e $1 | wc -l | sed 's/ //g'
}

# for each commit in log
git log --pretty="%H %cd" --date=short | while read COMMIT DATE
do
    # skip commits made on the same day
    [ "$PREV_DATE" == "$DATE" ] && continue
    PREV_DATE="$DATE"

    # count files
    FILES=$(count ".*\.org$")

    # print to console
    echo $DATE
    echo " $FILES	.org files"

    # append to CSV file
    echo "$DATE,$FILES" >> $OUTPUT 
done
```

