# Podcasts

[[I love podcasts]].

Here's a list of [[podcast series]] that I'm subscribed to, periodically exported from AntennaPod: [[html]] / [[opml]]

I think it makes more sense to recommend individual episodes, rather than particular series.  Perhaps see the backlinks in [[podcast]] for that.  But regardless:

-   [[Novara Media]]
-   [[Tech Won't Save Us]]
-   [[Revolutionary Left Radio]]
-   [[General Intellect Unit]]
-   [[Philosophize This]]
-   The Partially Examined Life
-   [[Reasons to be Cheerful]]
-   [[Looks Like New]]
-   [[The Fire These Times]]
-   The Restart Project Podcast
-   [[Frontiers of Commoning]]

