# wealth inequality

[[Wealth]] [[inequality]].

> The last time wealth inequality was as pronounced as it is now was during that belle époque of the 1920s. Then, it was bad enough as a cause of social misery and international instability. Today, it is arguably much worse because the gulf between the haves and have-nots extends to their carbon emissions, which heightens suffering from the climate crisis and impedes efforts to find a solution.
> 
> &#x2013; [[The great carbon divide]]

