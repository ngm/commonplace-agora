# Federated wikis

Couldn't you do federated wikis using IndieWeb building blocks?  So each wiki entry is an h-entry say.  And if I want to copy to my wiki from your wiki, I have an action that parses your wiki entry microformats,  and posts the copy to my wiki via micropub, ready for me to edit.  And maybe sends a webmention to your wiki perhaps, to let it know where other copies exist that have been taken.

Not sure if that's 100% the idea behind say [[FedWiki]] - what does it actually mean by federation?

Isn't the web just one big federated wiki?

And maybe, just maybe, it's not so bad to have a wiki in a central place.  The [indieweb wiki](https://indieweb.org) is, for example.


## See also

-   [[Interlinking wikis]]
-   [[Distributed digital gardens]]
-   [[Wikity]].
-   [[A Platform Designed for Collaboration: Federated Wiki]]

