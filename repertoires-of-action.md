# Repertoires of action

> For anarchism, then, the kind of repertoires of action that are of interest are those that aim at changing reality in the here and now, at creating alternatives to what presently exists and, where judged to be necessary, directly challenging the dominance or even existence of what exists.
> 
> &#x2013; [[Anarchist Cybernetics]]

