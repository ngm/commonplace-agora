# TCP/IP

> It is no exaggeration to say that TCP/IP is the internet: without its rules, the world’s networks would be a Babel of mutually unintelligible tongues.
> 
> &#x2013; [[Internet for the People]]

