# The Comma Strike

I read about this in [[October (book)]].  Print workers demanding pay for punctuation led to a [[general strike]] in Russia in 1905.

> In September 1905, printers in Ivan Sytin’s Moscow publishing house went on strike, demanding pay for punctuation marks. Discontented workers in other trades and other cities soon joined them in sympathy: bakers, railroad workers, lawyers, bankers, even the Imperial Ballet. Without the railroad, steel and textile mills were forced to shut down; soon nearly the entire adult population of Petrograd had ceased work. The general strike led Tsar Nicholas II to issue the October Manifesto, granting a constitution to Russia for the first time in its history. 
> 
> &#x2013; [The Comma Strike - Futility Closet](https://www.futilitycloset.com/2013/11/15/the-comma-strike/) 

Highlights the power of strong [[unions]] and [[strike action]].

