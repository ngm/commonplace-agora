# The Missing Revolution w/ Vincent Bevins

Featuring
: [[Vincent Bevins]]

On the uprisings of the 2010s ([[Occupy]], [[Arab spring]], etc) and their failure to manifest as leftist revolutions in the long-term.

On the problems of horizontalism. Bit of Horizontalism vs verticalism.

Interviewer is a bit one-sided, Marxist-Leninist, missing the nuance of Neither Vertical Nor Horizontal I think.

Still, really interesting, and based on on the ground accounts of what happened, so definitely to take heed of.

