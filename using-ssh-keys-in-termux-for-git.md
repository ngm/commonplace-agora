# Using SSH keys in Termux for Git

[[termux]]. ssh. git.

I need to install a couple of packages.

```sh
pkg install openssl-tool
pkg install openssh
```

Set up your ssh key.  I use passphrases.

To have gitlab.com in your git config be recognised by ssh, and to use the particular key for gitlab:

```nil
Host gitlab.com
	HostName gitlab.com
	User git
	Port 22
	IdentityFile ~/.ssh/gitlab
```

To make sure you don't have to keep entering it:

```nil
Host *
	AddKeysToAgent yes
```

And at the start of new Termux session:

```nil
eval $(ssh-agent)
ssh-add
```

