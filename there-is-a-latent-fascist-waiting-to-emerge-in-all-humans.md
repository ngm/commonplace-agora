# There is a latent fascist waiting to emerge in all humans

A claim from [[George Orwell]] (according to this article: [Trump has birthed a dangerous new ‘Lost Cause’ myth. We must fight it | David&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/08/trump-has-birthed-a-dangerous-new-lost-cause-myth-we-must-fight-it))

[[Fascism]]. 


## Because

&#x2026;?


## So&#x2026;

If this were true, then it needs to come with a 'so'.

I guess one (one that I don't like) is [[Hobbesianism]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

I can imagine it, but it feels kind of bleak.  I suppose it probably comes with a corollary, like 'unless we do X' in which case it's useful.

Also it's more of a belief than a claim I suppose (what's the exact difference?) as it's not something you could prove.

