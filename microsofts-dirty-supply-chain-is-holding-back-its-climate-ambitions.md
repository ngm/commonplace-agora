# Microsoft's dirty supply chain is holding back its climate ambitions

URL
: https://www.theverge.com/23745933/microsoft-fossil-fuels-suppy-chain-emissions-climate

[[Microsoft]].  [[supply chains]].  [[carbon dioxide emissions]].

96.71% of Microsoft's emissions are scope 3, i.e. lifecycle emissions from products.

Emissions from many of Microsoft's suppliers have in fact been going up.  Mostly, they seem to say, as a result of increase in demand.

Seems like a reduction in demand (i.e. degrowth) would be a good way to go.

The problem of [[carbon emissions]], counter to [[planetary stability]], at all layers of the stack.

