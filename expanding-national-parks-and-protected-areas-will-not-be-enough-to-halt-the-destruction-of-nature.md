# Expanding national parks and protected areas will not be enough to halt the destruction of nature

> An analysis of the draft [[UN Convention on Biological Diversity]] (CBD) agreement by more than 50 leading scientists has found that, while expanding protected areas will, if done well, help slow the destruction of the natural world, much more is needed to stop it.
> 
> &#x2013; [Expanding national parks not enough to protect nature, say scientists | Biodi&#x2026;](https://www.theguardian.com/environment/2022/jan/19/expanding-national-parks-not-enough-to-protect-nature-say-scientists-aoe)

[[Biodiversity loss]].


## Because

-   We also need action on overconsumption, harmful subsidies and the climate crisis


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Seems pretty obvious and I'm happy to take those scientists words for it.

