# Greenhouse Gas Protocol

URL
: https://ghgprotocol.org/

> GHG Protocol establishes comprehensive global standardized frameworks to measure and manage greenhouse gas (GHG) emissions from private and public sector operations, value chains and mitigation actions.

This is what defines the different scopes of emissions.

