# How an eccentric English tech guru helped guide Allende’s socialist Chile

URL
: https://www.theguardian.com/world/2023/jul/22/stafford-beer-chile-allende-technology-cybernetics

Short article on Chile, [[Project Cybersyn]], [[Stafford Beer]], and [[Evgeny Morozov]]'s new podcast series on the topic, [[The Santiago Boys]].

