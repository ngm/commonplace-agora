# Task management with org-mode and orgzly

I use org-mode for my task management.

I document here a few aspects of my system that make it function more as a web app of sorts, with syncing and mobile access.  You might use this as an alternative to something like todoist or wunderlist, and perhaps an alternative to Evernote.  This setup is kind of esoteric, but org-mode is very powerful so I've persevered and it's worth it.  For something a bit simpler, maybe using NextCloud’s tasks app might work.  Or you might find something [here](https://github.com/Kickball/awesome-selfhosted#task-managementto-do-lists) or [here](https://github.com/Kickball/awesome-selfhosted#note-taking-and-editors).


## Platforms/protocols

It has the benefit of being primarily text based, so format-wise it’s very open.  It’s editable primarily in emacs (or a variant, I use [[spacemacs]]).  Although using org-mode in emacs provides a huge amount of functionality, in essence it’s simply a structured text file.


## Hosting/web access

There’s no need for hosting as such.  It’s just a bunch of text files.  However, I do sync it to my server.   For web access, I can then just ssh into the server and run org-mode with a headless emacs, so that works pretty well.  I have contemplated also allowing some kind of web-based access to those files so I can view/edit them from anywhere, but haven’t really needed it, ssh has worked whenever I’ve needed to do this.  (As an aside, the sync to the server also gives me an always-on relay for syncthing, which is handy.)


## Mobile and syncing

I sync my org files between devices with syncthing.  syncthing is awesome, but also frustrating when it isn’t working.  But it gets the job done for me, it just needs a bit of handholding now and then.  You could sync with other things like dropbox or resilio.

For mobile usage I use orgzly.  This is a great libre Android app.  It doesn’t do everything org-mode can do (that would be a massive ask), but it does a good majority of the stuff that’s important.

orgzly has it’s own syncing options.  I use the one that syncs with a folder on the filesystem.  This folder is the folder that syncthing watches.  One extra bit of tweaking here – in order to make sure orgzly is syncing regularly, I set up a tasker task.  Read more about that here.  Newer versions of orgzly have the option to autosync when you add new tasks, etc.  But I found that a little slow when I tried to use it.

