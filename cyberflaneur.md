# cyberflâneur

A [[flâneur]] on the web. 

-   https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/
-   https://www.nytimes.com/2012/02/05/opinion/sunday/the-death-of-the-cyberflaneur.html
-   https://www.theatlantic.com/technology/archive/2012/02/the-life-of-the-cyberfl-neur/252687/
-   https://the-syllabus.com/cyberflaneur/

