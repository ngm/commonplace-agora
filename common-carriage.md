# Common carriage

> Common carriage is an idea with a long history in the legal traditions of different countries. In the US, it descends from English common law, but the underlying elements date back to the Romans. The basic concept is that if a business is open to the general public, then it must serve everyone the same way. It must remain neutral, in other words, and not discriminate against certain users or uses of its services.
> 
> &#x2013; [[Internet for the People]]

