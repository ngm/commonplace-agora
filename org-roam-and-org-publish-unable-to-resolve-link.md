# org-roam and org-publish "Unable to resolve link"

I get this message "Unable to resolve link" all the time at the moment when trying to publish my org-roam files via org-publish.  It's getting annoying now.  Time to find a long-term fix.

It'll be something like:

> Unable to resolve link: "7d3f6206-476a-48ce-a43b-982ec7d87ffc"

Where that GUID is the ID of some other file in my org-roam files that this file is linking to.

One current, laborious workaround: navigate to the file identified by that ID, then run:

```elisp
(org-id-update-id-locations (list (buffer-file-name (current-buffer))))
```

That updates the id locations file with the id of the missing file.  Then I navigate back to the original file and it'll publish OK.

That's not too bad, but sometimes it either:

a) takes ages to execute because I have over 2000 files, and it seems to have forgotten about them
b) the original file has loads of missing ID links, so I have to repeat the process loads of times, which is tedious

Another option is to run:

```elisp
(org-roam-update-org-id-locations)
```

That seems more robust, as it'll pick up all files.  But that takes **ages** to finish.  And sometimes seems to get reset and you have to do the same again.

I think possibly the reason it gets reset is because I have two org-roam DBs/folders (a private and a public garden)?  I've noticed that beyond the first horribly long run of `(org-roam-update-org-id-locations)`, it's a lot quicker on subsequent runs.

