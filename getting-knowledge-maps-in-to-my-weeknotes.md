# getting knowledge maps in to my weeknotes

Why?  a) To give me a visual aide memoire of the week notes. b) To give others a more visual entry point in to my digital garden.

I started with very visual and very time consuming slides made in LibreOffice Impress.  See: [[Captain's Log]].  I still really like those, but I couldn't find the time to make them each week.

Then I fiddled a little bit with [[PlantUML]]. Marginally less work than building from scratch, but still time-consuming, having to write the PlantUML markup.  Good fun, but if I'm spending actual real time on it, I'd rather the creation process itself be visual and free-flowing.

Next up I've been trying graphs built automatically with org-roam-graph.  I put the links to the daily journals in the weeknotes file, generate the graph, have a peruse of it and write my weeknotes from then.  Then remove the journal links and just generate the graph from the weeknote links. It works as a nice easy-to-generate visual aide memoire for me, though sadly it's not really very visually compelling for anyone else.

