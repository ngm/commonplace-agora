# Digital socialism

> A system of [[digital socialism]] would phase out intellectual property, socialize the means of computation, democratize data and digital intelligence and place the development and maintenance of the digital ecosystem into the hands of communities in the public domain.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

-   phase out intellecual property
-   socialise the means of computation
-   democratise data and digital intelligence
-   place the development and maintenance of the digital ecosystem into the hands of communities in the public domain

> Many of the building blocks for a socialist digital economy already exist. Free and Open Source Software ([[FOSS]]) and [[Creative Commons]] licenses, for example, provide the software and licensing for a socialist mode of production.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> As James Muldoon notes in [[Platform Socialism]], city projects like [[DECODE]](DEcentralised Citizen-owned Data Ecosystems) provide open source public interest tools for community activities where citizens can access and contribute data, from air pollution levels to online petitions and neighborhood social networks, while retaining control over data shared.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> [[Platform coops]], such as the Wings food delivery platform in London, provide a prominent workplace model whereby workers organize their labor through open source platforms collectively owned and controlled by the workers themselves.

<!--quoteend-->

> There is also a socialist social media alternative in the [[Fediverse]], a set of social networks that interoperate using shared protocols, that facilitate a decentralization of online social communications.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> But these building blocks would need policy change to thrive.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> Projects like the Fediverse, for example, are not able to integrate with closed systems or compete with the massive concentrated resources of the likes of Facebook. A set of radical policy changes would therefore be needed to force big social media networks to interoperate, decentralize internally, open up their intellectual property (e.g. proprietary software), end forced advertising (advertising people are subjected to in exchange for “free” services), subsidize data hosting so that individuals and communities — not the state or private companies — can own and control the networks and perform content moderation. This would effectively strangle tech giants out of existence.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> The socialization of infrastructure would also need to be balanced with robust privacy controls, restrictions on state surveillance and the roll-back of the carceral security state.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

[[Governable stacks]].

