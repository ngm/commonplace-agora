# digital finance

Just some definitions I found to get started on this topic.  So all sound pretty techno-utopian, so take with a pinch of salt.

> From a practitioner's viewpoint, digital finance is financial services delivered through mobile phones, personal computers, the internet or cards linked to a reliable digital payment system
> 
> https://www.sciencedirect.com/science/article/pii/S2214845017301503

<!--quoteend-->

> While there is no standard definition of digital finance, there is some consensus that digital finance encompasses all products, services, technology and/or infrastructure that enable individuals and companies to have access to payments, savings, and credit facilities via the internet (online) without the need to visit a bank branch or without dealing directly with the financial service provider.
> 
> https://www.sciencedirect.com/science/article/pii/S2214845017301503

<!--quoteend-->

> Artificial intelligence, social networks, machine learning, mobile applications, distributed ledger technology, cloud computing and big data analytics have given rise to new services and business models by established financial institutions and new market entrants.
> 
> &#x2013; [Digital finance | European Commission](https://ec.europa.eu/info/business-economy-euro/banking-and-finance/digital-finance_en) 

<!--quoteend-->

> All these technologies can benefit both consumers and companies by enabling greater access to financial services, offering wider choice and increasing efficiency of operations.
> 
> &#x2013; [Digital finance | European Commission](https://ec.europa.eu/info/business-economy-euro/banking-and-finance/digital-finance_en) 

