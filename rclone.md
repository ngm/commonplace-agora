# rclone

Finding [rclone](https://rclone.org/) super handy for backups and things where cloud services are involved.

Especially useful as my internet connection is currently so poor that downloading from a cloud service to then upload to a server somewhere is terribly slow.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-25 Thu]</span></span>

When setting up a remote on GSuite/Workspace, you can select a particular team drive for your remote, which is handy.  The root of that remote is then specifically that drive.

When I set it up first of all against our GSuite, I created our own client, so we're not using the generic rclone one.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-12 Mon]</span></span>

Not tried it yet, but it looks as if you can get a basic export of Google Drive files from rclone.

See: https://rclone.org/drive/#drive-export-formats

Also some discussion here: https://github.com/rclone/rclone/issues/49 (quite old now though)

