# The Sunset of the Californian Ideology? w/ Richard Barbrook

URL
: https://www.buzzsprout.com/1004689/8635143-the-sunset-of-the-californian-ideology-w-richard-barbrook

Publisher
: [[Tech Won't Save Us]]

Featuring
: [[Richard Barbrook]]

Really excellent interview with Richard Barbrook.  He 
talks about the [[Californian Ideology]] and where it stemmed from.

Also really interesting to hear about his work on games and how they can be used for political purposes and training.  Talks about [[Guy Debord]] and his [[A Game of War]].  Makes me think of some of the [[Tesa Collective]] games.

