# What is possible in the information age is in direct conflict with what is permissible

What is possible in the [[Information Age]] is in direct conflict with what is permissible.

The [[forces of production]] are in conflict with the [[relations of production]].

True&#x2026; although just because something is possible doesn't necessarily mean it **should be** permissible.  You still have to assess and justify whether the possible should be permissible.  Lots of things are **possible** that probably should be restricted.

That said, there's load of things where [[DRM]] is added and it is completely bogus.

> The non-hierarchical relations made possible by a peer network, such as the internet, are contradictory with capitalism’s need for enclosure and control.
> 
> &#x2013; [[The Telekommunist Manifesto]]

Agree with that.

> Publishers, film producers and the telecommunication industry conspire with lawmakers to bottle up and sabotage free networks, to forbid information from circulating outside of their control.
> 
> &#x2013; [[The Telekommunist Manifesto]]

True again but I want to see the argument for **why** it should be entirely free.

> Marx concludes, ‘no social order ever perishes before all the productive forces for which there is room in it have developed; and new, higher relations of production never appear before the material conditions of their existence have matured in the womb of the old society itself’.
> 
> &#x2013; [[The Telekommunist Manifesto]]

