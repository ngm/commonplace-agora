# GDP

> Kuznets developed GDP as a means of measuring the impact of the great depression. It enabled governments to track any increase or decrease in their nation’s wealth as represented by the value of goods and services produced, and became increasingly important as governments estimated the cost of waging the second world war.
> 
> &#x2013; [Beyond GDP: here’s a better way to measure people’s prosperity](https://theconversation.com/beyond-gdp-heres-a-better-way-to-measure-peoples-prosperity-168023)

The developer of the concept, Simon Kuznets, didn't think it was a good measure of national welfare. ([[Doughnut Economics]])

> Nobel prize-winner Simon Kuznets declared in 1934 that “the welfare of a nation can scarcely be inferred from a measurement of national income”
> 
> &#x2013; [Beyond GDP: here’s a better way to measure people’s prosperity](https://theconversation.com/beyond-gdp-heres-a-better-way-to-measure-peoples-prosperity-168023)

