# projects and activities

Things I am doing or would like to do.


## Contributing to the knowledge commons


### Why?

-   Because [[commoning represents a profound challenge to capitalism]].


### How?

-   Creating and sharing my digital garden here and on [[Anagora]].


## Climate activism


### Why?

-   [[To be a 21st century socialist is to be an eco-socialist]]


### How?

-   &#x2026;

