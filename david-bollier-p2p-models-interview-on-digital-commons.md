# David Bollier, P2P Models interview on digital commons

URL
: https://p2pmodels.eu/digital-contexts-commons-adequate-attention/

[[David Bollier]].

> many people still approach [[digital Commons]] from the resource perspective, and so they don’t adequately address the social dimension

<!--quoteend-->

> a lot of open platforms are conflated with a Commons, meaning if it is an open platform, there is an assumption that anybody should be able to use it, without paying attention to the social dimensions of stewardship and curation of a Commons

<!--quoteend-->

> A Commons doesn’t take care of itself, but that’s essentially what the idea of an “open platform” presumes.

