# Public interest technology

> Public interest technology (PIT) is a growing field made up of technologists who work to ensure technology is created and used responsibly.
> 
> &#x2013; [[Public interest technology and its origins]]

<!--quoteend-->

> These technologists call out where technology can improve for the public good, and sometimes question whether certain technologies should be created at all.
> 
> &#x2013; [[Public interest technology and its origins]]

<!--quoteend-->

> Technologists are defined not only as engineers, but also include those individuals and organizations with strong perspectives and expertise on the creation, governance and application of technology—from designers to legal experts to artists, activists, and members of communities where technology is deployed.
> 
> &#x2013; [[Public interest technology and its origins]]

