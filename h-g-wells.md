# H. G. Wells

> Best known today for his science fiction novels like War of the Worlds and The Time Machine, Wells was also a prolific essayist and committed [[socialist]] who believed passionately that new information technologies would one day usher in an era of social equality and world peace.
> 
> &#x2013; [The Secret History of Hypertext - The Atlantic](https://www.theatlantic.com/technology/archive/2014/05/in-search-of-the-proto-memex/371385/) 

