# Revisiting the Calculation Debate: A Call for a Multiscale Approach

URL
: 

https://www.tandfonline.com/doi/full/10.1080/08935696.2022.2051374?scroll=top&needAccess=true

I'm interested in wha the multiscale approach is.

> because local and macro initiatives are equally important and feed one another positively, advocates for adopting a multiscale approach.

<!--quoteend-->

> critically reviewing discussions of an [[ecosocialist]] society and its economic organization through revisiting the “[[calculation debate]]”

<!--quoteend-->

> It underlines the importance of repoliticizing the economic sphere to reembed the economy in society rather than society being subordinated to the economy.

<!--quoteend-->

> To that aim, it emphasizes the gravity of (1) planning our future, (2) coordinating our decisions before we embark on any action, (3) relying on knowledge in different forms and formats as articulated at both the individual and societal level, and (4) generating real democracy via participatory and deliberative mechanisms. 

