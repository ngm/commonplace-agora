# Indigenous culture

> Indigenous cultures, for example in North America, are [[non-binary]] and [[gender-fluid]], and this is precisely what has enabled them, over millennia, to think (dream) outside the box, and surmount immense challenges to environmental change.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

