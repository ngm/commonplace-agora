# Cottingham: Europe's biggest battery storage system switched on

URL
: https://www.bbc.co.uk/news/uk-england-humber-63707463

> What is thought to be Europe's biggest [[battery energy storage system]] has begun operating near Hull.

<!--quoteend-->

> The site, said to be able to **store enough electricity to power 300,000 homes for two hours**, went online at Pillswood, Cottingham, on Monday.

<!--quoteend-->

> The facility was **developed by North Yorkshire renewable power firm Harmony Energy using technology made by Tesla**.

<!--quoteend-->

> The Pillswood facility has the capacity to **store up to 196 MWh energy in a single cycle**.

