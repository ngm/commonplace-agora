# Nathan Schneider - Cooperatives, the Commons and Ownership

URL
: https://voices-of-the-commons.sounder.fm/episode/nathan-schneider-cooperatives-the-commons-and-o

Publisher
: [[Guerrilla Media Collective]]

Featuring
: [[Nathan Schneider]], [[Stacco Troncoso]]

> In this podcast, Nathan talks about:
> 
> -   How [[cooperatives]] address accountability crisis of the mainstream economy
> -   [[What is the relationship between cooperatives and the commons?]]
> -   The vulnerabilities of the [[Commons]] when it marginalizes ownership
> -   How coops radicalize workers across the political spectrum
> -   The breadth and width of the cooperative economy
> -   Latest trends in digital and crypto-cooperativism

Useful stuff for me on how coops relate to the commons, and also about the different scale of coops out there - from your huge rural farm coops, to your local coffee shop coop.  And the potential disconnect between the two groups.

