# free market

> This non-correspondence of useful and profitable-to-produce goods and services occurs because under [[capitalism]] “the primary method used to allocate things is the free market"
> 
> &#x2013; [[Review: People's Republic of Walmart]]

