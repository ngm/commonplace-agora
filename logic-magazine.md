# Logic Magazine



## Themes

> Like you, we are both insiders and outsiders. Luckily, this is exactly the position you need to be in to observe and describe a system. In the social sciences they call it “logic”: the rules that govern how groups of people operate. For engineers, “logic” refers to the rules that govern paths to a goal. In the vernacular, “logic” means something like being reasonable. Logic Magazine believes that we are living in times of great peril and possibility. We want to ask the right questions. How do the tools work? Who finances and builds them, and how are they used? Whom do they enrich, and whom do they impoverish? What futures do they make feasible, and which ones do they foreclose?
> 
> We’re not looking for answers. We’re looking for logic.
> 
> [Disruption: A Manifesto](https://logicmag.io/intelligence/disruption-a-manifesto/)


## Pitching

https://logicmag.io/pitch-us/

