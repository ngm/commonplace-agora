# sixth extinction

[[Mass extinction]].

> Species extinction is happening at a rate between 100 and 1000 times the historic “background rate.” Increasingly, scientists call this the sixth mass extinction
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> The ‘Sixth Extinction’, which will likely lead to the extermination of half of all animal and plant species by the end of the century, seems to bother few outside of the tiny conservation movement
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Earth’s wildlife populations have plunged by an average of 69% in just under 50 years, according to a leading scientific assessment, as humans continue to clear forests, consume beyond the limits of the planet and pollute on an industrial scale.
> 
> &#x2013; [Animal populations experience average decline of almost 70% since 1970, repor&#x2026;](https://www.theguardian.com/environment/2022/oct/13/almost-70-of-animal-populations-wiped-out-since-1970-report-reveals-aoe)  

