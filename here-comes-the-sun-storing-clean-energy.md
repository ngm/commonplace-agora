# HERE COMES THE SUN: storing clean energy

A
: [[podcast]]

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/here-comes-the-sun

Series
: [[Reasons to be Cheerful]]

On [[energy storage]].

Energy storage can be lots of different things.  From small(ish) scale batteries for the home, or storage in big infrastructure projects.  You could have storage at the wind turbine, for example.

Stuff like [[Vehicle-to-grid]] is interesting.

The grid was designed for one-way flow of electricity from big power stations to cities and homes.  But now it needs to work both ways and be made much smarter.

