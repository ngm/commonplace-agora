# Upgrading Linux Mint 19.2 to 19.3

I was on 19.2 of [[Linux Mint]].  Quite out-of-date now, although receiving security updates until 2023, so no problem as such.

God&#x2026; I remember the days when I used to distro hop and would be switching to an entirely new Linux distro like once a month&#x2026; those days are long gone, I just don't really have to the time for that anymore.

Anyway, I've upgraded to 19.3 via the 'System Reports' app, and that was all very seamless and easy.    It's quite handy that System Reports thing - alerting you about various small issues you might have.

No noticeable issues so far.  So I might be bold and update straight to 20 now.

Ah no - 19.3 to 20 appears to be a different kettle of fish - it takes you here - https://linuxmint-user-guide.readthedocs.io/en/latest/upgrade-to-mint-20.html - and expect much more prep work to be done.  Fair enough, it's probably a much bigger upgrade.

Looks like it will take a while to create a system snapshot before the upgrade, so I'll leave it for now.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-20 Sat]</span></span>

Initial upgrade.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-22 Mon]</span></span>

Had a problem running a PHP app locally.  It seems that PHP was update to version 8 during the upgrade.  The app was expecting 7.3.

I set things back to use 7.3 with:

```bash
sudo update-alternatives --set php /usr/bin/php7.3
```

That works for apps running php from the CLI, but I wonder if I'll hit some problem for apps going via Apache.

phpmyadmin seems to be OK with things - it's still running 7.3 - so maybe it's fine.

