# Markets vs planning

> Schoolchildren in the US are commonly taught to conceive of the broad variety of political-economic systems, both those extant and those possible, as divisible into two essential and opposing categories: “[[markets]]” and “[[planning]].”
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> “Markets,” in this formulation, offer opportunities for commerce which make people free, while “planning” oppresses people through inefficient resource rationing.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> It is taken for granted that “markets” and “[[capitalism]]” are synonymous; likewise “planning” and “[[socialism]].” The problems with this formulation are legion, but particularly egregious is its utter ahistoricity: inconveniently for the schoolteachers formulation, markets predate capitalism by thousands of years.
> 
> &#x2013; [[Markets in the Next System]]

See also [[socialist calculation debate]].

I would (surprise surprise) prefer something that has a little bit of each - e.g. [[P2P accounting]].

