# G. D. H. Cole

> [[Representation]] for Cole should be ‘specific and functional’ rather than ‘general and inclusive’.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Members should be able to exercise their rights over important issues they care about without necessarily voting on every small issue.
> 
> &#x2013; [[Platform socialism]]

