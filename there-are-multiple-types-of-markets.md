# There are multiple types of markets

> The key to imagining what [[post-capitalist markets]] might look like, according to David Schweickart, is to discard the unitary idea of “markets.” Too general a term to be useful, markets, Schweickart suggests, should be divided into three types: [[markets for goods and services]], [[capital markets]], and [[labor markets]].
> 
> &#x2013; [[Markets in the Next System]]

[[types of markets]]

