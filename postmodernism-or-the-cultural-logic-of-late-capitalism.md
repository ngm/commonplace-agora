# Postmodernism, or, The Cultural Logic of Late Capitalism

[[Fredric Jameson]].

A critique of modernism and postmodernism from a Marxist perspective.

![[images/postmodernism-jameson.jpg]]

Constant recycling of the past.

