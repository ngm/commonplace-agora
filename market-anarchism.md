# Market anarchism

> market anarchism is advocacy of replacing the state with civil society while pointing to free market economics to explain the workability and/or desirability of such.
> 
> &#x2013; [Center for a Stateless Society » What is market anarchism?](https://c4ss.org/what-is-market-anarchism)

Never quite sure what to make of it.  

> Some market anarchists label their views as “[[anarcho-capitalism]],” while others prefer to identify with “anti-capitalism” or “libertarian socialism.” Still others reject both the labels “capitalism” and “socialism” as too hopelessly distorted in the public consciousness to be used meaningfully in reference to what they advocate.
> 
> &#x2013; [Center for a Stateless Society » Are market anarchists for or against capital&#x2026;](https://c4ss.org/are-market-anarchists-for-or-against-capitalism)

