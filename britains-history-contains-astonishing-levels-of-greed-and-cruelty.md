# Britain's history contains astonishing levels of greed and cruelty

[[Britain]]


## Evidence

-   Colonialism
-   Slavery
-   Empire


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Without a doubt]]

Source (for me)
: [The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down)

My historical knowledge isn't up to scratch enough to articulate the atrocities, but very confident that they are there.  Colonialism.  Slavery.  Empire.

