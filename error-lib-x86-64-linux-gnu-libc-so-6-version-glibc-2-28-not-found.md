# Error: /lib/x86

I've gotten this error a couple of times when trying to install things recently.

First was the latest versio of node.

And just now, [[Acorn]].

Guessing I'm on a too old version of Mint. I am certainly due an upgrade.

Yeah, this would suggest so: https://unix.stackexchange.com/questions/653465/how-to-install-glibc-2-28-on-linux-mint-19-3-cinnamon

> libc is pretty much the fundamental library for nearly all your programs and libraries. It's very central, and Mint won't offer an easy way of updating to a newer version of it without a distro upgrade.

I'll just bite the bullet and upgrade then.

Yup, [[Upgrading Linux Mint 19.3 to Linux Mint 20]] seem to have sorted.  I can run Acorn now anyway.

