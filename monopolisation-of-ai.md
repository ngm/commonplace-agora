# Monopolisation of AI

> The monopolisation of AI is not just – or even primarily – a data issue. Monopolisation is driven as much by the barriers to entry posed by fixed capital, and the ‘virtuous cycles’ that compute and labour are generating for the AI providers.

Nick Srnicek talks about how imbalanced access to fixed capital and labour are as big issues as access to large datasets when it comes to the big tech monopolies.

> economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst.

I think the argument being that something like the EU’s data strategy focuses too much on the data itself, and neglects the hardware, capital and labour needed to do useful things with that data.

> It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers.

[Data, Compute, Labour | Ada Lovelace Institute](https://www.adalovelaceinstitute.org/data-compute-labour/).

