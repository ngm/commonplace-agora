# Free software economics

How do FOSS projects sustain themselves?

Should we be donating more to the authors?

-   https://www.jvt.me/posts/2019/11/06/take-take-take-foss

Does that favour more public facing projects?  Who donates to the authors of libraries?  Maybe creators of the public facing projects pay it forward.

That Roads and Bridges report talks about this.

Also that long thread we have on the social.coop Loomio.


## Non-profits and free software

> But non-profit funding is typically project-focussed, short-term, focusses on features over maintenance and core product development, and development over sales and marketing – arguably key early stage investments for successful software platforms.
> 
> &#x2013; [The Truth about ‘FOSS’: Purpose, License and Business Model for Tech in Socia&#x2026;](https://www.techchange.org/2018/03/30/the-truth-about-foss-purpose-license-and-business-model-for-tech-in-social-change-arent-all-the-same-thing/) 

<!--quoteend-->

> And in any case, getting investment in a platform when you’ve already given away your competitive advantage is tough. Most colleagues I’m aware of have supported their FOSS platforms through consulting and custom implementations or feature development, another kind of project-based funding that keeps your developers busy providing bespoke services to paying clients and draws them away from spending time on the core of the platform. Or, unable to get repeat grants for maintenance and continued improvement, platform providers continually ‘innovate’, developing new products or new features rather than consolidating and improving the original. So we end up with proliferating, confused, feature-rich, poorly-maintained platforms.
> 
> &#x2013; [The Truth about ‘FOSS’: Purpose, License and Business Model for Tech in Socia&#x2026;](https://www.techchange.org/2018/03/30/the-truth-about-foss-purpose-license-and-business-model-for-tech-in-social-change-arent-all-the-same-thing/) 

-   [How markets coopted free software's most powerful weapon (LibrePlanet '18 Key&#x2026;](https://www.youtube.com/watch?v=vBknF2yUZZ8)

