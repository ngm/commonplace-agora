# Walter Benjamin

> Those who are able to face the looming reality can easily feel trapped. Walter Benjamin’s prescient metaphor of our being caught in a train without brakes—his image of “progress”1—comes easily to mind.
> 
> &#x2013; [[Red-Green Revolution]]

