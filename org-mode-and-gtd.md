# org-mode and GTD

I use [[org-mode]] and [[orgzly]] for a [[Getting Things Done]]-ish system.

I'm taking rough notes here on the way I do it.

It's probably a bit erroneous to call this GTD, as it's probably a poor approximation to it.  Let's just call it GTD-inspired.


## Projects, actions, and epics

As per GTD, I tag 'projects', anything that is going to take a few things to get it actually done, with `:project:`.  A project will then have a bunch of items underneath it which are next actions - small chunks of work that take me towards getting it done.

Occasionally, projects seem so big that they would span months and be made up of hundreds of actions.  It feels a bit of a drag to have these sitting around for review after review, so I tag them `:epics:` and break them up into multiple projects.

> I usually have around 30 active projects at the same time.
> 
> -   [Orgmode for GTD](https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html)


## Next actions

> Each task is written to begin with a "Next Action" Verb and an object or target of the verb. It is much easier to take action if you know what you need to do without reassessing the task. For example, "Weekly Report" is unclear whereas "Compile weekly call statistics" tells me what needs to be done. 
> 
> &#x2013; [How I use Emacs and Org-mode to implement GTD](http://members.optusnet.com.au/~charles57/GTD/gtd_workflow.html)


## Day planning

> It’s important not to overwhelm yourself at the daily level. If you do, you’ll most likely begin to suffer from a consistent sense of failure; a feeling that you “can’t ever get ahead”. Such a depressive influence may cause you to avoid your task list altogether, and then you won’t get anything done – or at least, not the things you intended to! Thus it’s crucial to keep your daily task list small and achievable. Start out really small, in fact: leave yourself as much free time as you can. Once you discover your natural balance, you’ll know instinctively what constitutes an unreasonable day and what an achievable one looks like.
> 
> &#x2013; [Lost in Technopolis](http://newartisans.com/2007/08/using-org-mode-as-a-day-planner/) 


### Priorities

I quite this as the method for prioritising your tasks: A for urgent and important, B for either urgent or important, and C for not really either, but still something you want to do.

> Your main goal each day should be to finish all your A tasks. If you can manage this, it means you’re on top of all the important things in your life. Remember: not all urgent tasks are important. If a task is urgent but not important, consider downgrading it to a B or a C. If you can’t get to it in time, its window of opportunity may “close” – but then if it wasn’t really important that shouldn’t matter much. Try to mark as “A” only those tasks which are both important and have to be done on the day you schedule them for
> 
> &#x2013; [Lost in Technopolis](http://newartisans.com/2007/08/using-org-mode-as-a-day-planner/) 

I like this little talk from [[EmacsConf 2020]] about using [[org-mode]] to prioritise tasks based on a vision for life.  Dead simple, just add a tag to each of the headlines that flags what the long-term impact of doing a particular thing will be.  I've not tried doing this yet though.

It looks like you could run the risk of never doing daily chores though, if they all factor in as 0 towards a long-term goal.  Maybe it needs to be a mix of urgency and importance that you tag things with.


### Effort estimates

I try to make sure every task has an effort estimate.

The main reason being is just to see how much I've committed to in a given day. In the agenda view I run `org-agenda-columns` with `C-x C-c C-x`, then at the top I get the sum of how much the total estimate of all the tasks in the day would take up.

Also, anything where I estimate an hour or more on it, it's a trigger to think about whether this should be broken down in to something smaller.

I know around some areas of agile there is a 'no estimates' movement, so it's perhaps worth reading in to that a bit for alternatives.  It might be that I could just count the number of items for a rough idea of how much the commitment is.


### Remaining work for the day

As mentioned above, I run `org-agenda-columns` with `C-x C-c C-x` in the agenda view to get the column view.  At the top you get the sum of how much the total estimate of all the tasks in the day would take up.

Before running that, I do `M-x :` and then evaluate `(setq org-agenda-skip-scheduled-if-done t)` in order to hide DONE tasks, as at this point I don't want to include the done tasks in the time estimate.

My columns config for this is as follows:


It's the `%10Effort(Estimated Effort){:}` that does the summing up of estimates.  See [Column attributes (The Org Manual)](https://orgmode.org/manual/Column-attributes.html#Column-attributes) for more info.


## Weekly review

I have this checklist for my weekly review for now - I can't remember where the original from, and I've tweaked it a bit:

-   [ ] Get Clear
    -   [ ] Get Inbox to zero
        -   [ ] email
        -   [ ] org
    -   [ ] Archive DONE org items
-   [ ] Get Current
    -   [ ] Review upcoming calendar
    -   [ ] Review Waiting For list
    -   [ ] Review Project (and larger outcome) lists
    -   [ ] Review any relevant checklists
    -   [ ] Review Action Lists
-   [ ] Get Creative
    -   [ ] Review Someday/Maybe
    -   [ ] Be creative and courageous
-   [ ] Brain dump


### Review project lists

For review project and larger outcome lists, I do `SPC m a m` which is an agenda view filtered by tags, and I filter it by the `project` tag.

I also make sure to set `(setq org-tags-match-list-sublevels nil)`, so I only see the top-level list of projects, not all of their children.  If you see the children it's a lot of unnecessary noise I find.  

I feel like maybe I need to incorporate epics in there somehow.


### TODO Use Nicolas Petton's function for showing just the first next action in projects.

https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html


## Filing system

Where you keep your reference stuff.  Mine is a private org-roam at the moment.


## Misc

-   one of the single most important things, I've found, is keeping the list tidy.  Once it gets cluttered, it addes this mental friction and you want to look at it less and less.  So regular list tidying is really important.


## Tickler file

> The tickler is one of the best concepts of GTD in my opinion
> 
> -   [Orgmode for GTD](https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html)

I don't have a dedicated tickler file.  maybe I'm achieving the same thing just through deadline dates?


## Links

-   [Some Org Mode agenda views for my GTD weekly review](https://dindi.garjola.net/org-agenda-weekly.html)

