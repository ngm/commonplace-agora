# The policing bill is an attack on some of the most basic democratic rights of citizens

The [[Policing bill]] is an attack on some of the most basic [[democratic rights of citizens]].

For example: the right to peacefully assemble.  The right to speak freely.

> More than 350 organisations, including human rights groups, charities and faith bodies, have written to Patel and justice secretary Robert Buckland this weekend complaining that the measures would have a “profound impact” on freedom of expression, and represent “an attack on some of the most basic democratic rights of citizens”.
> 
> &#x2013; [Patel faces widening revolt over policing bill’s restrictions on protest | Po&#x2026;](https://www.theguardian.com/uk-news/2021/sep/12/patel-faces-widening-revolt-over-policing-bills-restrictions-on-protest) 


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Signs point to yes]]

