# titling evergreen notes

[[Evergreen notes]].

How to title them?  

> Make titles that are statements, clear instructions, commands, orders or directions.
> 
> &#x2013; [Growing the Evergreens](https://maggieappleton.com/evergreens) 

