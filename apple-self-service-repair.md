# Apple Self Service Repair

-   [Apple announces Self Service Repair - Apple](https://www.apple.com/newsroom/2021/11/apple-announces-self-service-repair/)
-   [Everyone Is a Genius: Apple Will Offer Parts and Tools for DIY Repairs | iFix&#x2026;](https://www.ifixit.com/News/55370/apple-diy-repair-program-parts-tools-guides-software)
-   [Too good to be true? Apple announces giving access to (some) spare parts and &#x2026;](https://repair.eu/news/too-good-to-be-true-apple-announces-giving-access-to-some-spare-parts-and-repair-information-to-consumers/)

Announced 17th November 2021.
Arrived 27th April 2022: https://support.apple.com/self-service-repair

[[Apple]].  [[self-repair]].  [[Apple and right to repair]].


## Announcement, delay

> The idea behind Self Service Repair is pretty simple. Instead of buying unofficial parts or visiting expensive “authorized” repair technicians, customers can just buy parts for the iPhone, Mac, iPad, or Apple Watch on the Apple website. Self Service Repair will also provide official repair guides to customers, plus a recycling and reimbursement service for broken parts you return to the company.
> 
> &#x2013; [[Apple Promised Us a Repair Program, Where the Hell Is It?]]

-   generally regarded as a bit of a PR stunt, however:
    -   good news, because previously Apple were campaigning heavily against right to repair
        -   even [[Apple]] can change its tune

-   lots of unknowns
    -   how costly will the parts be?
    -   will they continue to use software locks and preventing use of 3rd party spare parts?

> Self Service Repair is hardly the best solution for customers who want to fix their Apple products at home. This program still gives Apple full control over every part of the repair process—and I’m not just talking about pricing.
> 
> &#x2013; [[Apple Promised Us a Repair Program, Where the Hell Is It?]]


## Launch

[[Part pairing]] is a problem.

> Unfortunately, this program expands the freedom to repair with one hand, while locking the door with the other. Integrating a serial number check into their checkout process is a dire omen and could allow Apple the power to block even more repairs in the future. Building the technology to provision individual repairs easily sets Apple up as the gateway to approve—or deny—any repairs in the future, with parts from any source.
> 
> [[Apple's Self-Repair Vision Is Here, and It's Got a Catch]]

