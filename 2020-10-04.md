# 2020-10-04



## Just reinstalled koreader on my kobo

I just reinstalled [[koreader]] on my [[kobo]] ereader.  It's come on so much since last time I used it (a year or two ago?)  The fact that it's a built-in menu in Nickel (Kobo's default system) makes it so much easier to launch.

Dark mode is so nice.  And now it has its own [[Wallabag]] plugin, for reading articles you've saved online, which is awesome.  (I was using Wallabako, which was cool but no longer needed).

My notes here: [[Installing koreader on my Kobo]].


## Listening: [[An ethics of agency]]

Listening to the FOSS and Crafts episode on [[an ethics of agency]], Chris Webber's ethical framework.  Liking it so far, and the links to the recent [[Declaration of Digital Autonomy]]. 

https://fossandcrafts.org/episodes/11-an-ethics-of-agency.html

