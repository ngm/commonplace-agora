# Labour theory of value

[[Value]].

> The one property that all commodities have in common, and through which their “value” can be determined, is that each is a product of human labor
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Commodities can exchange according to the relative amount of labor-time that it takes to produce them
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> That’s why mainstream economists today are known as neoclassical, rather than classical, economists. They profess dedication to the likes of Smith and Ricardo but have discarded the troubling labor theory of value. After all, if the producers of wealth are laborers and not the bosses, then the popular slogan of May 1968, “The boss needs you; You don’t need the boss,” is not abstract hyperbole, but a concrete roadmap for the future
> 
> &#x2013; [[A People's Guide to Capitalism]]

