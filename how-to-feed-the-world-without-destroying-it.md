# How to feed the world without destroying it

URL
: https://www.theguardian.com/environment/audio/2022/jun/03/how-to-feed-the-world-without-destroying-it-podcast

> When [[farming]] is degrading our soils and people are still going hungry, is it time to change how we get our [[food]]?
> 
> “Ninety-nine percent of our calories come from [[soil]]. Everything we are, everything we have built, everything in our lives comes from the soil. Without it, we’re finished,” says the author and environmentalist [[George Monbiot]]. “And yet, we treat it with extreme disrespect and disregard.”
> 
> Monbiot tells Michael Safi how we need to start appreciating the wonders of the soil beneath our feet. In his book Regenesis: Feeding the World Without Devouring the Planet, Monbiot argues we need to pursue revolutionary ways of getting our sustenance. He argues that by using a method called [[precision fermentation]], where proteins and fats are produced in breweries, we could feed the world and save our soils.

This one is on crisis and food supply, it seems to be said that since 2015, the number of people going hungry in the world started to increase, despite the fact that I think it says we're producing more food than ever before. And also the cost of that food is going down.

And interestingly, they described it as a weird sort of emergent behaviour, what they call the flickering in the complex system of the global food supply.

I've never heard that in complex systems before, but [[flickering]], strange behaviours that you potentially see before a collapse or a phase transition. Basically hinting that something big is about to happen.

