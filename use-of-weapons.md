# Use of Weapons

Author
: [[Iain M. Banks]]

Reading more Iain M. Banks. Use of Weapons. It’s good. His writing style has gone from super linear and rip roaring to totally nonlinear and introspective in the space of a couple of books. I’ve enjoyed all of them so far. This one is much more comical too. The Culture is getting more morally ambiguous by the book as well, which I like. Love a bit of moral ambiguity.

