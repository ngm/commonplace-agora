# North America and Europe have made the biggest contribution to the climate crisis

[[North America]] and [[Europe]] have made the biggest contribution to the [[climate crisis]].


## Because

-   they have produced by far the most [[carbon dioxide emissions]] since the [[industrial revolution]] ([Scientists have just told us how to solve the climate crisis – will the world&#x2026;](https://www.theguardian.com/commentisfree/2022/apr/06/scientists-climate-crisis-ipcc-report))
-   the average North American emits 16 tonnes of carbon dioxide each year from fossil fuel use, compared to just 2 tonnes for the average African ([Scientists have just told us how to solve the climate crisis – will the world&#x2026;](https://www.theguardian.com/commentisfree/2022/apr/06/scientists-climate-crisis-ipcc-report))
-   the climate crisis is driven by how the world’s wealthy – which includes much of the UK’s population – currently live, consume and invest ([Scientists have just told us how to solve the climate crisis – will the world&#x2026;](https://www.theguardian.com/commentisfree/2022/apr/06/scientists-climate-crisis-ipcc-report))

