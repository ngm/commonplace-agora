# Repairing a Canon Pixma TS3150

Yesterday I repaired a Canon Pixma TS3150 at our repair cafe.

Another volunteer at the cafe had looked at it previously and the display was reporting a paper jam (error E03). They opened it up and found a pen inside. They removed this, closed up the printer, and - hey presto! It no longer turned on :confused: .

I’ve come across on other Pixma’s before that they sometimes have some ribbon cables that if you’re not careful slide out of their connector the moment you open the printer up, sometimes such that you’d barely notice.

That was the same here. Though fair play at least to get in it was all Philips screws and some spudgable plastic clips. We identified and spent a while trying to get this fiddly ribbon cable back in… turned back on… and power! But the LCD display was no longer working - not displaying anything. Though it didn’t seem directly related to the display we fiddled around a bit more with the original ribbon cable, tried connecting it a bit more tightly. Put stuff back together and turned on again and the LCD was back on - maybe just a loose connection somewhere.

However still reporting E03. We looked around again inside the printer. Came across a white part with a hoop that looked skewiff and wasn’t sitting properly on a small black pole. Somewhere near the drive belt / sliding rod. Fitted that back on, tried again.

No error! And we successfully printed a test page. :raised<sub>hands</sub>:

Thanks to this guy on YouTube for his disassembly video: https://www.youtube.com/watch?v=3-YpuL8_nIs

also on: https://talk.restarters.net/t/today-i-repaired/716/208

