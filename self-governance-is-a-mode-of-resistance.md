# Self-governance is a mode of resistance

[[Self-governance]] is a [[mode of resistance]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Outlook good]]

Source (for me)
: [[Nathan Schneider]] (https://twitter.com/ntnsndr/status/1481494708761153538)

