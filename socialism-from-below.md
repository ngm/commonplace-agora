# socialism from below

-   [Socialism from Below – New Socialist](https://newsocialist.org/socialism-from-below/)
-   [Socialism from Below | The Anarchist Library](https://theanarchistlibrary.org/library/george-woodcock-socialism-from-below)

> 'Socialism From Below' was the most important principle of the [[Prague Spring]]. ‘Socialism from below’ was similarly a central principle of [[Salvador Allende]]’s Chilean movement and government, so cruelly destroyed by the US-backed coup in 1973, which left such a profound impression on the young Jeremy Corbyn
> 
> &#x2013; [[the revolution will be networked]]

