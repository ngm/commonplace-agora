# Trivial and non-trivial machines

[[Heinz von Foerster]].

> A trivial machine is a machine whose operations are not influenced by previous operations. It is analytically determinable, independent from previous operations, and thus predictable. For non-trivial machines, however, this is no longer true as the problem of identification, i.e., deducing the structure of the machine from its behavior, becomes unsolvable.
> 
> &#x2013; [The Heinz von Foerster Page](https://www.univie.ac.at/constructivism/HvF.htm)  

