# commons.hour number 2

> the political economy of **community organising in digital spaces**.

<!--quoteend-->

> The ways that commitments and contributions to organising may hinge on economic privilege; and the importance of care work as a radical way into this tangle of challenges.

-   [[Ela Kagel]]
    -   [[Supermarkt Berlin]]


## How to foster a culture of openness?

![[2021-10-25_19-23-56_screenshot.png]]


### Economic

![[2021-10-25_19-25-05_screenshot.png]]

activist commitments.
giving up personal time in order to engage.
child care, collective meals.
be very clear about pro bono work. time banking.  how is value accounted for in communities.
measuring value flows - where is value being generated in communities?
paid work opportunities.


### safety.

![[2021-10-25_19-29-52_screenshot.png]]
 meeting in a safe space.
 privacy friendly.
 code of conduct (a living document)


### online governance

![[2021-10-25_19-33-09_screenshot.png]]


### collective leadership


### privilege

![[2021-10-25_19-35-21_zfoChuA.png]]


### time

![[2021-10-25_19-36-50_screenshot.png]]

-   levels of involvement


### language

maybe have a main language for intro, but breakout rooms in different languages.
(language may not just be about national languages, but understanding of concepts)
the facilitators need to be up front about acknolwedging difficults of language barriers, and making sure that things are done to help include others.


### participation fatigue

how to manage encouraging feedback from everyone, but avoid fatigue?

