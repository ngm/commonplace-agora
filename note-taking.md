# note taking

> Notes aren’t a record of my thinking process. They are my thinking process. 
> 
> – Richard Feynman

Note-taking is one of the foundations of [[personal knowledge management]].

-   [[zettelkasten]]
-   [How To Take Smart Notes: 10 Principles to Revolutionize Your Note-Taking and &#x2026;](https://fortelabs.co/blog/how-to-take-smart-notes/)
-   [Sönke Ahrens - How to take smart notes on Vimeo](https://vimeo.com/275530205)

> The primary purpose of note-taking should not be for storing ideas, but for developing them. When we take notes, we should ask: “In what context do I want to see this note again?"
> 
> &#x2013; [How To Take Smart Notes With Org-mode · Jethro Kuan](https://blog.jethro.dev/posts/how_to_take_smart_notes_org/) 

-   [How to Use Roam to Outline a New Article in Under 20 Minutes - YouTube](https://www.youtube.com/watch?v=RvWic15iXjk)

