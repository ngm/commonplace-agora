# semiotics

> The study of the use of words as symbolic representations is known as semiotics
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

