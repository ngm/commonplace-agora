# Critical Perspectives on Digital Capitalism: Theories and Praxis. Introduction to the Special Issue

An
: [[article]]

Found at
: https://www.triple-c.at/index.php/tripleC/article/view/1498/1543

> Abstract: [[Digital capitalism]] matters. Digital capitalism shapes our lives. Digital capitalism needs to be better understood. We need critical theories of digital capitalism. We need to better understand praxes that challenge digital capitalism and aim at fostering digital democracy and [[digital socialism]]. tripleC’s special issue on “Critical Perspectives on Digital Capitalism: Theories and Praxis” wants to contribute to establishing foundations of critical theories and the philosophy of praxis in the light of digital capitalism. This article introduces the topic and provides an overview of the special issue.

-   We need critical theories of digital capitalism.
-   We need to better understand praxes that challenge digital capitalism and aim at fostering digital democracy and digital socialism.


## Examples of digital capitalism

-   Exploitation of digital labour
-   [[Tech layoffs]]
-   [[Algorithmic bias]]
-   [[Digital colonialism]]
-   [[Fake news]]
-   Polarisation of digital public sphere


## Examples of digital socialism

-   Digital workers protesting at Foxconn
-   Workers founding the Amazon Labor Union
-   [[Mastodon]]
-   [[The Public Service Media and Public Service Internet Manifesto]]
-   [[Democracy Now!]]

