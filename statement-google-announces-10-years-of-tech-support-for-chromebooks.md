# STATEMENT: Google announces 10 years of tech support for Chromebooks

URL
: https://pirg.org/media-center/statement-google-announces-10-years-of-tech-support-for-chromebooks/

Good news! Longer support for [[Chromebook]]s.  [[Google]].

> Google announced on Thursday that it would extend the automatic update “expiration date” to 10 years for all Chromebook models released since 2021.

Good for the environment as reduces the number of new devices needed.  Good for schools as they don't have to pay out for new devices.

> U.S. PIRG Education Fund’s Chromebook Churn report found that doubling the life of Chromebooks sold in just 2020 could cut emissions equivalent to taking 900,000 cars off the road for a year, and could result in $1.8 billion dollars in savings for schools, assuming no additional maintenance costs.

