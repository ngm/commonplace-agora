# Lichen

Lichen are a combo of [[fungi]] and [[algae]].

The fungus provides water and minerals, shape and form, while the algae makes food through photosynthesis.

They are able to grow on leaves and branches, bare rock, including walls and gravestones.

They can survive extreme environments.

