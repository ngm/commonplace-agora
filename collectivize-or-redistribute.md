# Collectivize or redistribute?

> I was reminded today of this scene in Ken Loach's Land and Freedom, one of the only film scenes I know of in which you see people debating over [[land reform]]: to [[collectivize]] or re-distribute? 
> 
> https://youtu.be/njWMkZYazCM​

