# I strongly dislike the policing bill

[[I strongly dislike]] the [[Policing bill]].


## Because

-   [[The policing bill is an attack on some of the most basic democratic rights of citizens]]
-   [[The policing bill increases the risk of peaceful demonstrators being criminalised]]
-   [[The policing bill will undermine the right to protest at a critical moment in the fight to avoid climate breakdown]]
-   [[The policing bill is about state control]]
-   [[The policing bill is an embrace of a new authoritarianism]]


## Epistemic status

Type
: [[feeling]]

Strength
: 9

