# The Double Objective of Democratic Ecosocialism

URL
: https://monthlyreview.org/2023/09/01/the-double-objective-of-democratic-ecosocialism/

Author
: [[Jason Hickel]]

> As all of this should make clear, degrowth—the framework that has cracked open the imagination of scientists and activists over the past decade—is best understood as an element within a broader struggle for ecosocialism and anti-imperialism.

^ that's a huge statement.  [[Degrowth is an element within the broader struggle of ecosocialism]].

