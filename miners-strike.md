# Miners' strike

Huge [[strike action]] in the UK in 1984-1985.

> threatened the British state as nothing had since the second world war
> 
> &#x2013; [[For years, I suspected MI5 interfered in the miners’ strike. The truth was even more shocking than I thought]]

Biggest police operation in British history. ([[Miners’ Strike 1984: The Battle For Britain]])

