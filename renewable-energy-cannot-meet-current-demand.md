# Renewable energy cannot meet current demand

> 100% renewables is improbable within the short window of time we have. One influential report estimated that a totally renewable world in 2030 would need to generate 17.1TW of solar energy and 13TW of wind energy. That would require 3.8 million new wind turbines, 49,000 new concentrated solar plants, and 1.7 billion rooftop PV systems
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> A study by UK Fires found that even with 100% renewables, the UK would have to use 60% of its current energy levels, have 60% of its current road traffic, and no shipping or aviation.
> 
> &#x2013; [[For a Red Zoopolis]]

