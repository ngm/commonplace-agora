# Collective Knowledge Management

What Athens Research is calling what it wants to do.  Plenty of prior art on the term too.

> CKM is a way for groups of people to create, connect, and compound knowledge, from research and documentation to ideas and conversations.
> 
> &#x2013; [Season 2 of Athens — A Collective Vision](https://athensresearch.ghost.io/season-2/) 

