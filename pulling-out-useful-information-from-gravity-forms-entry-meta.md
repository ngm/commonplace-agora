# Pulling out useful information from Gravity Forms entry meta

This is always painful.

Plonking a query here that helped me out when accessing info from Gravity Forms SQL.

```sql
select
  (select meta_value from wp_gf_entry_meta where meta_key = 1 and entry_id=wp_gf_entry.id) as email,
  (select meta_value from wp_gf_entry_meta where meta_key = 3 and entry_id=wp_gf_entry.id) as message,
  source_url, date_created
from wp_gf_entry inner join wp_gf_entry_meta on wp_gf_entry.id = wp_gf_entry_meta.entry_id
where wp_gf_entry.id in (select wp_gf_entry_meta.entry_id from wp_gf_entry_meta where form_id = 17 and wp_gf_entry_meta.meta_value like '%offers%')
 group by email, message, source_url, date_created
order by date_created asc
```

