# How should you structure a personal wiki?



## Paths and threads

How do you show someone around your garden?
[[Connecting the dots]].
You can provide some paths to some degree in the wiki itself.  But also turn it into articles from time to time, your own curated path for the here and now.


## Wild or tended?

> h0p3 has a home page entry point that is carefully curated and groomed, but which is several layers up from a complete chaos of link dumps, raw drafts and random introspections [&#x2026;] These layers run a spectrum of accessibility—there is always a learning curve before you hit the bottom. You start with a doorway before entering a maze.
> 
> &#x2013; [Notes: We’ve Got Blog (2002)](https://www.kickscondor.com/stenos/we've-got-blog/), Kicks Condor

I think this relates to paths and threads.  People can take any route around, but it's good to provide some kind of path.  


## [[Page size]]

