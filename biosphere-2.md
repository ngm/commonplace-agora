# Biosphere 2

> monument to pharaonic ecology was built in 1989 by members of a commune established twenty years before – the Synergia Ranch – where everyone was to some extent a thespian, cybernetician, gardener, sailor, and entrepreneur
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Biosphere 2 belonged to a long line of Cold War environmental research focused on escaping Earth’s surface
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> CO2 ‘scrubbing’ technology, which underpins ‘carbon capture and sequestration’ systems today, was originally developed for the USS Nautilus, the first nuclear-powered submarine
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Within the gleaming walls of Biosphere 2, one could find five ecosystems reproduced in miniature: tropical rainforest, coastal fog desert, mangrove wetland, savannah, and ocean (with a coral reef)
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Given such immense resources, its purpose seemed modest enough: to keep eight ‘biospherians’ alive for two years without allowing anything (not even air) in or out of the complex
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Before the end of the first mission, the biospherians had inadvertently managed to replicate many of the different facets of the environmental crisis in miniature – an appropriate outcome for an experiment conducted in a place named Oracle
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Some 19 of its 25 vertebrates went extinct, along with a majority of the 125 insect species
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The most important lesson salvaged from the wreckage of Biosphere 2 is the impossibility of controlling ecological systems even of a modest size
> 
> [[Half-Earth Socialism]]

