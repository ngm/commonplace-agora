# Spare parts

> White goods repairers highlight the cost of spare parts as the main reason pushing people to replace a product, rather than repair it. 
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

