# Github

> While git is a distributed version control system, GitHub created a centralised collaboration platform around it, with such popularity that for many people they have become synonymous. People manage their software projects on GitHub, no longer just because it is a helpful platform, or because it is gratis, but also because they effectively have to be there for the rest of the developer world to discover and contribute to their project — a network lock-in, like with other social networks
> 
> &#x2013; [Redecentralize Digest — March 2020 — Redecentralize.org](https://redecentralize.org/redigest/2020/03) 

