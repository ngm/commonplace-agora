# GND vs Degrowth. Live Panel from Labour Conference 2021

A
: [[podcast]]

Comparing and contrasting [[degrowth]] and [[Green New Deal]].

-   ~00:08:21  Growth in socially useful industries, degrowth in harmful industries.
-   ~00:11:54  Something like Insulate Britain straddles both degrowth and green new deal.
-   ~00:12:46  Green new deal straddles left from far left to centrists.

