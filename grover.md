# Grover

> With Grover, subscribers get access to a wide range of over 3,000 tech products, including smartphones, laptops, virtual reality (VR) gear, wearables and smart home appliances on a flexible monthly rental basis. Grover’s service allows its users to keep, switch, buy, or return products depending on their individual needs and budget.
> 
> -   [Berlin-based unicorn Grover secures over €303 million for its pioneering circ&#x2026;](https://www.eu-startups.com/2022/04/berlin-based-unicorn-grover-picks-up-over-e303-million-as-it-pioneers-circular-approach-to-consumer-tech/)

