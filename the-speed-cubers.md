# The Speed Cubers

Documentary about the 2019 [[Rubik's Cube]] world championships, and the relatinship between Feliks Zemdegs and Max Park.

I really liked this.  Got a little bit teary eyed at the friendship between Feliks and Max, and what a genuinely lovely person Feliks seems like.

Some of the discussion around [[autism]] made me slightly uncomfortable at times.  It felt represented as mostly negative and as a problem to be solved.  Having autistic friends I didn't like this.  But this language was mostly from Max's parents so they have plenty more insight than I do, I guess.  

