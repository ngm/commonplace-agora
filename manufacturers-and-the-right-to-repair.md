# Manufacturers and the right to repair

[[Right to repair]].

-   [[Apple and right to repair]]
-   [[Google and right to repair]]
-   [[Samsung and right to repair]]

> From a legal standpoint, you can crack open a phone or laptop without any consequences. But manufacturers, especially smartphone and vehicle makers, have knowingly made product repairs difficult through software design, engineering, warranties, authorized repair programs, and an unavailability of replacement parts.
> 
> &#x2013; [[Apple Promised Us a Repair Program, Where the Hell Is It?]]

<!--quoteend-->

> While it’s nice to see manufacturers dip their toes into repair programs, it’s clear that they won’t take this subject seriously without legal or economic pressure. If we could boycott cars or phones, the latter option could be a great choice. But Right to Repair legislation seems to be the only realistic path toward universal device repairability, which will benefit the environment and consumers’ bank accounts.
> 
> &#x2013; [[Apple Promised Us a Repair Program, Where the Hell Is It?]]

