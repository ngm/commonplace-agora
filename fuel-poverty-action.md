# Fuel Poverty Action

URL
: https://www.fuelpovertyaction.org.uk/about-us/

> Fuel Poverty Action campaigns to protect people from fuel poverty. We challenge rip-off energy companies and unfair policies that leave people to endure cold homes. We take action for warm, well-insulated homes and clean and affordable energy, under the control of people and communities, not private companies.

