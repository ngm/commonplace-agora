# Bayesian analysis

> derives degrees of certainty which are interpreted as a measure of subjective psychological belief. 
> 
> &#x2013; [Certainty - Wikipedia](https://en.wikipedia.org/wiki/Certainty) 

