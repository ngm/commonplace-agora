# Apple  MacBook A1181

This is from 2006.

There's a few different versions of this on EveryMac - https://everymac.com/ultimate-mac-lookup/?search_keywords=a1181

Not sure which one this is.  No more info on the exterior beyond A1181.  Maybe some bits inside will give more info. 

Remarkably, seems to potentially go for about £20 on eBay spares / repair.  [link](https://www.ebay.co.uk/sch/i.html?_nkw=macbook+a1181&_dcat=111422&_sacat=111422&vbn_id=7023628225&LH_ItemCondition=7000&mag=1&_fsrp=1&LH_PrefLoc=2&rt=nc&LH_All=1)

Not checked what's wrong with it.

