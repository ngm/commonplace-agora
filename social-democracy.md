# Social democracy

> With the rise of neoliberalism, social democracy turned towards the right and increasingly adopted neoliberal policies. When [[Tony Blair]] became British Prime Minster in 1997, his neoliberal version of social democracy influenced social democracy around the world. The consequence was that social democracy became in many respects indistinguishable from conservative parties, especially in respect to class politics. We need a left social democracy that struggles for [[democratic socialism]].
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> If we look at historical examples of the radical edges of social democracy in terms of employee ownership schemes and gradualist plans for socialisation – the [[Mitterand government]] in the early 1980s and the [[Meidner Plan]] in Sweden – these were brought to a halt by threatened or actual [[capital strikes]].
> 
> &#x2013; [[Platform socialism]]

