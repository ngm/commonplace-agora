# Zoopolis

> Animal geographies have emphasised the moral and ethical obligations towards the inclusion of animals within urban community spaces. Jennifer Wolch’s (1998) theory of zoopolis makes a case for a multi-species city (i.e. not just people): one that strives for the inclusion of animals in the political sphere and indeed their citizenship rights within the nation states they inhabit.
> 
> &#x2013; [zoopolis | valuing animal geographies to create nature-based solutions in urb&#x2026;](https://urban-habitats.com/2022/07/01/zoopolis/) 

<!--quoteend-->

> there is an increasing literature on the possibility of a zoopolis: a polity in which animals – insofar as we can communicate with them – are taken seriously. Not just as moral patients, but as having interests and preferences. In which their thriving is of value in itself, and a cause of human thriving and pleasure
> 
> &#x2013; [[For a Red Zoopolis]]

