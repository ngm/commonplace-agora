# waste picker

> Around the world, communities of waste pickers from Brazil to Bangladesh to Mozambique make a living by sorting and collecting things other people have thrown away. The term ‘waste picker’ was first adopted at the First World Conference of Waste Pickers in 2008, as a non-derogatory title that recognises people’s contribution to public health and environmental sustainability.
> 
> &#x2013; [[Can urban mining help to save the planet?]]

