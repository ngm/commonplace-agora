# Bolivia

> It is also increasingly clear that American corporations have designs on Bolivia’s lithium reserves, which are the largest held by any individual country. Morales had already signalled his plans to nationalise the lithium industry – which will become an even more serious market as electric cars begin to become more widely used – and compete on the international market rather than offer the resource up at bargain prices to multinationals.
> 
> &#x2013; [Bolivia’s Far-Right Coup](https://tribunemag.co.uk/2019/11/bolivias-far-right-coup/) 

