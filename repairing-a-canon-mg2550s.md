# Repairing a Canon MG2550S

A Canon Pixma MG2550S inkjet printer brought to the Repair Cafe.

https://www.canon.co.uk/support/consumer_products/products/fax__multifunctionals/inkjet/pixma_mg_series/pixma_mg2550s.html


## Reported problem

-   was working fine
-   then warned about colour ink being out
-   replaced ink catridge
-   now series of flashing lights no printing or copying


## Diagnosis

-   turns on fine
-   on lamp and alarm lamp flashing 7 times sequence

According to the [manual](https://gdlp01.c-wss.com/gds/5/0300042115/01/MG2500ser_OnlineManual_Mac_EN_V03.pdf#l5B01), that means either error 5B00 or 5B01.  Which the manually just says to contact your service centre.

However, plenty comes up if you search for those error codes online.

> Here are a few reasons you might be getting the error:
> 
> -   Ink leakage: If you installed a damaged cartridge, chances are there was an ink leakage that led to the error.
> -   Saturated ink absorber pads: Over time, the ink absorber pads get saturated in cleaning excess ink, and that’s when you see the error 5B00 on Canon printers.
> -   Waste Ink Counter (WIC) has hit the limit: When WIC hits the maximum limit, you will have trouble using the Canon printer.
> 
> -   [How to Fix Error 5B00 on Canon Printers In No Time](https://windowsreport.com/printer-error-5b00/)

We tried option 1: resetting the waste ink counter.  It sounds like often the ink absorber pads aren't actually full, even though the printer might report that they are - the printer has a counter with which it takes a guess that the pads are probably full by now.  Apparently you can reset this counter, and squeeze a bit more life out of the printer.  When you do this, it's recommended to then run your printer on a piece of cardboard or similar, because you might get ink leaking out of the printer if the absorber pads actually are full.

I followed the instructions to go into service mode, try to print a service page, and reset the counter.  Getting into service mode works fine, but the service page didn't print, and the warning lights continue to flash once you reboot the printer, so seems like the reset didn't work either.

> -   First, turn off the printer.
> -   Now, press and hold the Stop button, and then simultaneously hold the Power button to turn the printer on.reset WIC
> -   With the Power button still held, press the Stop button 5 times to make the printer enter the Service Mode, and then release the Power button.
> -   Next, press the Stop button thrice and then the Power button once to print a Service Page.
> -   Finally, press the Stop button 5 times, followed by the Power button one last time.
> -   You can now turn off the printer for a while, and when you turn it back on, things should be up and running.
> 
> -   [How to Fix Error 5B00 on Canon Printers In No Time](https://windowsreport.com/printer-error-5b00/)

There seems to be a few sites offering slightly ropey looking software, and suggesting you pay for a reset key, which sounds very dodgy.

I think first I'll try cleaning the pads and see where that gets us.

These videos have a bit of info on disassembly and cleaning this particular printer:

-   [Cannon MG2520 Service Part 1 - YouTube](https://www.youtube.com/watch?v=lGk03b8e-xk&t=0s)
-   [Canon MG2520 service part 2 - YouTube](https://www.youtube.com/watch?v=8g0GAKSWc4A)

The guy ultimately doesn't seem to have any joy in fixing the problem (according to a comment in the second video).

Also one here: [Taking Apart Canon Pixma MG2520 Printer to Clean or Repair - YouTube](https://www.youtube.com/watch?v=uh53E4RRbGg)


## Resources

-   [How to Fix (Reset) Canon Printer Error Code 5B00 - YouTube](https://www.youtube.com/watch?v=2wo_suktXOE)
-   [SOLVED: How do I clean the ink absorber pads - Canon Printer - iFixit](https://www.ifixit.com/Answers/View/255298/How+do+I+clean+the+ink+absorber+pads)
-   [Taking Apart Canon MG6220 Printer for Parts or to Fix - YouTube](https://www.youtube.com/watch?v=Ok75s1eWaKI)

