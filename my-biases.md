# My biases

I try to recognise that I have a number of things about myself that have given and continue to give me privileges and/or unconscious biases in my view of the world.   Such as the fact I am a white cisgender man; I was born in England, grew up in England, currently I live in England.  I am a Millenial. My parents were socialists. I had a university education.

I try to work beyond these ingrained biases and question them and also view the world from the perspective of others.  But it's worth anyone who reads what I write knowing of them.

