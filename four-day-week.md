# Four day week

> More than 3,000 workers at 60 companies across Britain will trial a four-day working week, in what is thought to be the biggest pilot scheme to take place anywhere in the world
> 
> &#x2013; [Thousands of UK workers to take part in four-day week trial | Productivity | &#x2026;](https://www.theguardian.com/business/2022/apr/04/thousands-of-uk-workers-to-take-part-in-four-day-week-trial)

<!--quoteend-->

> According to pilot studies, workers reported anywhere from a 25% to 40% increase in [[productivity]], as well as an improved [[work-life balance]], less need to take sick days, more time to spend with family and children, less money spent on [[childcare]], and a more flexible working schedule which leads to better morale
> 
> &#x2013; [Thousands of UK workers to take part in four-day week trial | Productivity | &#x2026;](https://www.theguardian.com/business/2022/apr/04/thousands-of-uk-workers-to-take-part-in-four-day-week-trial)

Funny how increased productivity is seen as one benefit. Not my goal. But yeah, probably needed to get businesses on board.

> A four-day week would clearly be beneficial for workers, but there are good reasons, even within a capitalist system geared towards maximising worker exploitation, to abandon the five-day week. Studies suggest those who work 55 hours a week perform worse on some tasks than those who work 40. Evidence also suggests productivity increases, rather than decreases, when a four-day week is implemented. What’s more, the think tank Autonomy suggests that a four-day week can reduce emissions by as much as 16%
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]

