# Make a map, not a tracing

> The rhizome is altogether different, _a map and not a tracing_. Make a map, not a tracing. The orchid does not reproduce the tracing of the wasp; it forms a map with the wasp, in a rhizome.
> 
> &#x2013; [[Deleuze &amp; Guaterri]], [[A Thousand Plateaus]]

-   [[The map is not the territory]].

