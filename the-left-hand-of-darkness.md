# The Left Hand of Darkness

URL
: [[Ursula K. Le Guin]]


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-08-05 Thu]</span></span>

Finished.  It was great.  The journey across the glacier did go on for a fair while, but still.  Really enjoyable all the way through.  I feel like I should give it a proper, considered, review, but I don't have so much time for that.  Maybe I'll read a few other existing reviews and build off them.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-08-01 Sun]</span></span>

Nearly finished now I think.  Very enjoyable.  The musings on gender are really interesting.  I've read it quite quickly when I've been reading, I think perhaps missing quite some depth (perhaps how the sections with mythology link to the present day, for example).  And some of the ideas within the Handdara religion.

I like Estraven.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-22 Thu]</span></span>

Continuing to enjoy muchly.  Lots of great observations about how societies work, but not in a heavy-handed way, fits nicely in to the narrative.  I like the quote from Le Guin, that she "eliminated gender, to find out what was left".


### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-14 Wed]</span></span>

Enjoying this muchly so far.  There's something melliflous about the way Le Guin writes.

It feels similar to [[The Dispossessed]] so far in that it's about a stranger in a strange land.  The strange land seems to be one involving monarchy.  With another interesting characteristic being the absence of gender.


## Quotes

> …Tell me, Genry, what is known? What is sure, predictable, inevitable—the one certain thing you know concerning your future, and mine?”
> 
> “That we shall die.”
> 
> “Yes. There’s really only one question that can be answered, Genry, and we already know the answer… **The only thing that makes life possible is permanent, intolerable uncertainty: not knowing what comes next.** ”

<!--quoteend-->

> "[&#x2026;]But we in the Handdara don’t want answers. It’s hard to avoid them, but we try to.”
> 
> “Faxe, I don’t think I understand.”
> 
> “Well, we come here to the Fastnesses mostly to learn what questions not to ask.”
> 
> “But you’re the Answerers!”
> 
> “You don’t see yet, Genry, why we perfected and practice Foretelling?”
> 
> “No—”
> 
> “\*To exhibit the perfect uselessness of knowing the answer to the wrong question.\*”

<!--quoteend-->

> There is no division of humanity into strong and weak halves, protected/protective. **One is respected and judged only as a human being**. You cannot cast a Gethenian in the role of Man or Woman, while adopting towards “him” a corresponding role dependent on your expectations of the interactions between persons of the same or opposite sex. It is an appalling experience for a Terran…

<!--quoteend-->

> Winter is an inimical world; its punishment for doing things wrong is sure and prompt: death from cold or death from hunger. No margin, no reprieve. A man can trust his luck, but a society can’t; and cultural change, like random mutation, may make things chancier. So they have gone very slowly. At any one point in their history a hasty observer would say that all technological progress and diffusion had ceased. Yet it never has. **Compare the torrent and the glacier. Both get where they are going**.

<!--quoteend-->

> How does one hate a country, or love one? Tibe talks about it; I lack the trick of it. I know people, I know towns, farms, hills and rivers and rocks, I know how the sun at sunset in autumn falls on the side of a certain plowland in the hills; but what is the sense of giving a boundary to all that, of giving it a name and ceasing to love where the name ceases to apply?

