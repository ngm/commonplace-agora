# Adding a new connections page to my garden

I wonder if I can just rehash most of the stuff from the [[Well-connected]] page.

Kind of - the links table of org-roam.db has neither a timestamp nor an id column.  Can make use of sqlite's rowid field, but that's not particularly reliable.  Hmm.

Thinking about it, not really possible. Or not simple at least. It would require amending org-roam internals to allow for link annotations.

See chat at  https://org-roam.discourse.group/t/recording-the-date-that-connections-were-made/3379

