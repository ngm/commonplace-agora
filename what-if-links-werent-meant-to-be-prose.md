# What if links weren't meant to be prose?

URL
: https://subconscious.substack.com/p/what-if-links-werent-meant-to-be

Author
: [[Gordon Brander]]

Interesting thoughts on wikilinks and other ways of linking.

Thoughts from Agora matrix:

> @flancian
> 
> -   it seems /they-can-be-slugs?
> -   at which point they seem actually the same shape as `[[wikilinks which are phrases]]`, but harder to write the longer they are
> -   I agree that the synonym problem is there for wikilinks, but I think doing things like `[[canonical | what you want to render]]` is just one way of 'solving' that
> -   in the Agora I've been trying to not care about plurals/variations, assuming we will make them coalesce semantically/socially
> -   that feels like the best solution to me right now and could work as a counter-argument to Gordon's position? not that this needs to be a debate, more like a dialogue :)
> 
> @doubleloop
> 
> -   I don't fully grok what the suggested problem is with wikilinks - but Gordon's got lots of good thoughts/provocations so worth considering!  Like the idea of [[modeless markup]] is a good one, but no reason why wikilinks can't be that.
> -   I don't think I would use a wikilink to link to an actual URL like in his example
> -   My own current 'issue' is that wikilinks tend to push me to linking specifically to a concept, rather than linking a chunk of text to a concept&#x2026; and quite often that text is an interesting piece of meta-info on the link
> -   I used to do that but then stopped doing it in order to link specifically to node names
> -   But maybe I should go back to [[wikilinks which are phrases]] and see what happens, I like your idea that these things will coalesce over time one way or another

Wikilinks which are phrases is kind of in line with [[Andy Matuschak]]'s ideas on [[titling evergreen notes]], also.

