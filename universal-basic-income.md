# universal basic income

> Research has shown repeatedly that such programs—known as Universal Basic Income—are remarkably effective in improving quality of life in communities around the world, in both the Global North and South.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> Programs consistently report reduction in crime, child mortality, malnutrition, truancy, teenage pregnancy, and alcohol consumption, along with increases in health, gender equality, school performance—and even entrepreneurial activity.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

Some prefer [[Universal basic services]] over [[universal basic income]].  e.g. [[Universal Basic Income or Universal Basic Services?]]

> As the name suggests, most UBI plans—and the variants are many—propose that the state furnish all of its citizens with some kind of sustaining stipend, regardless of means tests or other qualifications. Most versions propose a grant at least equal to the local poverty line, in theory liberating recipients from the worst of the want and gnawing fear that might otherwise beset them in a time of mass disemployment.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Held up to sustained inspection, the UBI can often seem like little more than a neoliberal giveaway.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Its proponents on the market right clearly anticipate it as a pretext to do away with existing benefits, siphoning whatever transfers are involved in it back into the economy as fees for a wide variety of educational, healthcare and family services that are now furnished via social provision.
> 
> &#x2013; [[Radical Technologies]]

