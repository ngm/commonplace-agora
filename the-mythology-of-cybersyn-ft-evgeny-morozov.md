# The Mythology of Cybersyn (ft. Evgeny Morozov)

A
: [[podcast]]

URL
: https://soundcloud.com/thismachinekillspod/269-the-mythology-of-cybersyn-ft-evgeny-morozov

Series
: [[This Machine Kills]]

[[Evgeny Morozov]] talking about [[Project Cybersyn]] and his podcast [[The Santiago Boys]].

Really excellent, thought-provoking discussion.

-   ~00:24:13: You can have right-wing [[technological sovereignty]].  It's not by default a preserve of the left.
-   ~00:30:46: Morozov says that [[management cybernetics]] was really more grounded in [[operational research]] than it was cybernetics.
-   ~00:39:05: Morozov deliberately made The Santiago Boys  storytelling-based and narrative non-fiction - to help promote and mobilise people around the ideas contained within in a way other than just more dry analysis.  For comparison, he references Ayn Rand as having done more to circulate neoliberal ideas than the Chicago Boys themselves.
-   ~00:42:05: Morozov says Cybersyn works better as a myth than as an STS study.  Compares Cybersyn to Paris Commune as a motivational piece of mythology for the left.
-   ~01:18:13: Ideological battles have to fought in places beyond New Left Review and Verso. These are great outlets, but primarily preaching to the choir, and we need to move beyond that if we're to make a difference.

