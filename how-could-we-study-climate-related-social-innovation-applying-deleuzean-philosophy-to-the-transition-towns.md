# How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns

-   https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1970241

[[Deleuze and Guattari]].  [[Transition town]].  [[Climate change]].

> This is seen as a means of re-creating locally, spatially based identities: ‘rethinking the market along the line of the [[agora]] means re- inscribing it within time and space, embedding it within local contexts so that it has a more immediate reality to participants’
> 
> &#x2013;  [[How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns]]

