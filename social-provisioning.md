# social provisioning

> -   recognizing hidden care and domestic labour in economic life
> -   prioritizing human well being alongside other metrics of wealth
> -   correcting for unequal access to power and agency
> -   asserting the validity and inescapability of ethical judgement
> -   intersecting gender analysis with that of race, class and other other forms of identity
> 
> &#x2013; [[Towards Governable Stacks]]

