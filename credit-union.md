# Credit union

> Research from Italy shows that the presence of cooperative banks reduces income inequality, and a Europe wide comparison shows they are beneficial to the success of small and medium sized businesses.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]

