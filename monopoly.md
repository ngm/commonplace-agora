# Monopoly

> How big tech uses monopoly power against long-lasting products 
> • Apple trying for a monopoly on repairs 
> • Amazon sells disposable tablets with no repair possible 
> • Amazon removed small refurbishers of Apple products 
> • Google blocks indie repair business from advertising
> 
> https://twitter.com/RestartProject/status/1288371183654707201

