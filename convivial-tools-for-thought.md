# Convivial tools for thought

> Building [[convivial tools]] means breaking with much of the standard SaaS/Aggregator playbook, where a company owns and controls the software and the data. I don't want to store my brain on someone else's computer. What if we instead had a small tool that was personal, multiplayer, distributed, evolvable?
> 
> &#x2013; [[Building a Second Subconscious]]

The criteria are pretty [[IndieWeb]] I would say.

> Personal: Your data is yours. You can move it in, move it out, take it with you. The tool is also yours. You can use it as you like, and combine it with other tools.
> 
> &#x2013; [[Building a Second Subconscious]]

<!--quoteend-->

> Multiplayer: Personal doesn’t have to mean solitary. One of the most interesting ways of thinking is thinking together. “Knowledge production is a group activity, not an individual one.” (Engelbart).
> 
> &#x2013; [[Building a Second Subconscious]]

^ see [[FedWiki]], [[Agora]]

> Distributed: It’s a bummer that SaaS companies own most of our infrastructure for thought. I think it would be valuable if some of our infrastructure for thought was not-owned, or rather, owned by you, owned by everyone, interdependently, through some mix of federation, or p2p, maybe.
> 
> &#x2013; [[Building a Second Subconscious]]

^ See [[IndieWeb]]

> Evolvable: Everyone thinks differently. I would like a tool with building blocks you can remix and evolve for new ways of thinking, new use-cases. An open-ended tool you can repurpose to your creative needs.
> 
> &#x2013; [[Building a Second Subconscious]]

^ see [[IndieWeb]]

