# system change

> If a factory is torn down but the rationality which produced it is left standing, then that rationality will simply produce another factory. If a revolution destroys a government, but the systematic patterns of thought that produced that government are left intact, then those patterns will repeat themselves.
> 
> &#x2013; [[Zen and the Art of Motorcycle Maintenance]] 

