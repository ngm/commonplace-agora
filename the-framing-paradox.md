# The framing paradox

https://www.open.edu/openlearn/pluginfile.php/631068/mod_resource/content/1/moore.pdf

> [[Framing]] is an important and inevitable aspect of our common human endeavour to make experience intelligible.

![[2022-07-18_21-22-05_screenshot.png]]

![[2022-07-18_21-24-33_screenshot.png]]

![[2022-07-18_21-27-32_screenshot.png]]

![[2022-07-18_21-32-05_screenshot.png]]

Reminds me a bit of the [[Total Perspective Vortex]].

sensory and conceptual frames.

![[2022-07-18_21-36-53_screenshot.png]]

Makes me think a bit of 'true names' from [[A Wizard of Earthsea]].

