# Wind power

> Blustery winter weather helped Great Britain’s windfarms set a record for clean power generation, which made up more than 40% of its electricity on Friday
> 
> &#x2013; [Windfarms in Great Britain break record for clean power generation](https://www.theguardian.com/environment/2020/dec/19/windfarms-in-great-britain-break-record-for-clear-power-generation) 

<!--quoteend-->

> A massive expansion of wind farms across the UK is now needed for national security reasons, the business secretary has declared, as, following Russia’s [[invasion of Ukraine]], the government considers sweeping changes to planning laws to improve Britain’s energy independence.
> 
> [Tories plan big expansion of wind farms ‘to protect national security’ | Rene&#x2026;](https://www.theguardian.com/environment/2022/mar/13/tories-plan-big-expansion-of-wind-farms-to-protect-national-security)

