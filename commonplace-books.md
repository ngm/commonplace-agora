# Commonplace books

> Commonplace books (or commonplaces) are a way to compile knowledge, usually by writing information into books. [&#x2026;] Such books are essentially scrapbooks filled with items of every kind: recipes, quotes, letters, poems, tables of weights and measures, proverbs, prayers, legal formulas.
> 
> https://en.wikipedia.org/wiki/Commonplace_book

