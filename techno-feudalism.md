# Techno-feudalism

> Durand argues that [[Big Tech]] - Google, Amazon, Facebook, Apple, Microsoft - recreates the political and economic logic of feudal times. The big platforms and digital environments have become like "medieval" fortresses that colonise cyberspace and prey on it: they gain all the land of their business and acquire the competition and complementary companies. In [[feudalism]], the peasant serf was attached to the land: he did not belong to the feudal lord, but to the land, which belonged to that lord. When we use Facebook or Google, we become inseparable from the [[data]] we generate on the digital earth. From these traces, a relationship of extreme dependence is created, from which it is difficult to escape because they make our lives easier.
> 
> &#x2013; [[Digital Capitalism online course]]

<!--quoteend-->

> Durand states that, far from favoring the autonomy of individuals, the most striking aspect of the digital economy is the return to relationships of dependency.
> 
> &#x2013; [[Digital Capitalism online course]]

