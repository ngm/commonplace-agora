# Cory Doctorow on Why the Internet Broke and How to Fix It

A
: [[podcast]]

URL
: https://www.vice.com/en/article/dy3w4k/cyber-cory-doctorow-on-why-the-internet-broke-and-how-to-fix-it

[[Cory Doctorow]].  [[The Internet Con]].

Very enjoyable podcast - Cory Doctorow is always an engaging speaker.

He says at one point that the [[Digital Markets Act]] is genuinely good piece of legislation, it has just been used on the wrong thing - E2EE.

