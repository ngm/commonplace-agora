# Creatively Adapt & Renew

Pattern for [[Provisioning Through Commons]] from [[Free, Fair and Alive]].

> In a commons, there is no imperative to constantly expand production and profit, and so creativity can be focused on what really matters — ameliorating quality, durability, resilience, and holistic stability. Innovation need not be linked to boosting market sales and ignoring planetary health.
> 
> &#x2013; [[Free, Fair and Alive]].

