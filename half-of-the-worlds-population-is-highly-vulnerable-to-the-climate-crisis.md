# Half of the world's population is highly vulnerable to the climate crisis

Half of the world's population is highly vulnerable to the [[climate crisis]].

So sayeth the [[IPCC Sixth Assessment Report]].

[IPCC: Half of global population 'highly vulnerable' to climate crisis impacts&#x2026;](https://www.edie.net/ipcc-half-of-global-population-highly-vulnerable-to-climate-crisis-impacts/)

-   Extreme heatwaves, floods and droughts far outside the normal tolerances of cities and towns will destroy lives and livelihoods globally.
-   The knock-on effects of crop failures, migration and economic disruption could then overload political institutions and our abilities to respond to unfolding events.

