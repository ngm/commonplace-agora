# Maidan Revolution

> [[Yanukovych]] favoured closer ties with Russia. Protesters on Maidan square in central [[Kyiv]] wanted [[Ukraine]] to join the [[European Union]]
> 
> &#x2013; [Is there any justification for Putin’s war? | Ukraine | The Guardian](https://www.theguardian.com/world/2022/mar/13/is-there-any-justification-for-putins-war)

