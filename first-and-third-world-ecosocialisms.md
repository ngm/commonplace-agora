# First and Third World Ecosocialisms

An
: [[article]]

Subtitle
: Improving Contemporary Ecosocialist Debates

URL
: https://spectrejournal.com/first-and-third-world-ecosocialisms/

Author
: David Camfield

Publication
: [[Spectre Journal]]

Date
: 2022-10-05

Read time
: 00:30:49

Processing
: ~45m


## Thoughts

Looks at some recent debate around [[ecosocialism]]. Specificically articles [[Mish-Mash Ecologism]] ([[Matt Huber]]) and [[The Great Unfettering]] ([[Kai Heron]]) articles.  Finds merit and issue in both.

Pros with Huber: goal of appeal to the working class.
Issue with Huber: usual issues with [[Eco-modernism]].

Pros with Heron: [[Anti-imperialism]].
Issue with Heron: [[Third Worldism]], I think - i.e. the idea that working class in 'First World' are not part of the international proletariat.

What's his synthesis?  Not 100% clear (to me), but seems to likes the appeal to the working class argument and the [[Green New Deal]].  Anti-imperialist, but finds Third Worldism unconstructive.  Seems not enamoured with state and party form.  Likes democracy in work and in community, base-building, networked protest.  Evokes [[Rosa Luxemburg]], [[William Morris]], [[Gilets jaunes]].  So: seems for an [[ecology of organisational forms]].


## Raw highlights

> Huber embraces the term “socialist eco-modernism” in his contribution. Heron proposes “an anti-imperialist eco-communism,” but since that can be interpreted as suggesting that other ecosocialisms are not anti-imperialist I do not find it a useful descriptor. The former strand is commonly found expressed in articles in Jacobin, while the latter is frequently found in Monthly Review

<!--quoteend-->

> There has been a “permanent, foundational shift in how human society and economy can operate in the future,”


### Capitalism’s Productive Forces

> “we do need to develop the productive forces – but ecologically. A socialist eco-modernism should make the transformation of production and the productive forces the fulcrum of any new relation to the planet.”

<!--quoteend-->

> Heron rightly responds that technologies do not exist outside of the social relations within which they are developed, and that “instead of seeing capital’s abolition as the unfettering of productive forces, it is better to view it as freeing the world’s producers to choose from a richer and more diverse array of technologies and socio-ecological relations than capitalist industrialization can offer… It is about adopting appropriate technologies and collectively managing energy and food systems at relevant scales.”

<!--quoteend-->

> Ecomodernist socialism perpetuates a longstanding idea within the marxist tradition that the productive forces themselves are neutral; the problem is the social relations that hinder their deployment for progressive ends.

<!--quoteend-->

> This also tends to involve a narrow conception of what  productive forces are – as just technology, and not also forms of social cooperation – and to see them as  wholly distinct from the relations of production.

<!--quoteend-->

> However, if we follow Marx in understanding the productive forces as the productive powers of human labour, then when thinking about these as they exist today we must recognize them as the productive powers of alienated human labour as developed by capital.

<!--quoteend-->

> This is not just a matter of the purposes to which they are put, but also about which technologies and forms of cooperation are developed and in what ways.

<!--quoteend-->

> “precision agriculture” – “a new paradigm of capital-intensive industrial agriculture that integrates digital technologies to improve crop yields and manage populations”

<!--quoteend-->

> the mass manufacture of digital devices that are difficult or impossible to [[repair]] both reflect specifically capitalist imperatives

<!--quoteend-->

> How we think about forces of production matters today and will become even more important as ruling-class responses to climate change come to rely on technological fixes, including ones like solar radiation management and nuclear fusion that exist barely or not at all.


### Imperialism

> Unfortunately, there are also serious problems in Heron’s anti-imperialism, as in those of the Third Worldist strand of ecological marxism more broadly.

<!--quoteend-->

> Thus in various ways the working class in imperialist countries loses more than it benefits from imperialism.

<!--quoteend-->

> How ecosocialists understand imperialism has clear political consequences for how they approach movement organizing, their stance to states in the imperialist chain that descends from the US to the UK, Germany, France and China to lesser imperialist powers including Canada and Russia, and their orientation to imperialized states.

<!--quoteend-->

> Yet ecological marxism must also avoid the pitfalls of “the enemy of my enemy is my friend” politics to which Third Worldism often succumbs.


### Ecological Limits

> Materialists ought not to ignore the biophysical limits often theorized as planetary boundaries, which, as Ian Angus helpfully puts it, “can be compared to guard rails on mountain roads, which are positioned to prevent drivers from reaching the edge, not on the edge itself.”


### Strategy

> As I hinted above, a great strength of Huber’s approach is its argument for a class-struggle strategy for ecosocialism based on “analysis of the concrete class relationships that both inhibit… transformations or might bring them about” as an alternative to utopian leaps into abstraction when it comes to how transformation could be achieved.

<!--quoteend-->

> His insistence that “we need a climate politics that aims outward, beyond the already converted – towards the exploited and atomized working class” is vitally important.

<!--quoteend-->

> In that book Huber argues for working-class struggle for a Green New Deal (GND), a “politics of more that explains how much we have to gain from a climate program.”

<!--quoteend-->

> reforms that reflect a decommodifying internationalist climate justice politics of better rather than more, of public luxury and private sufficiency.

<!--quoteend-->

> So oriented, ecological marxist theory can guide the practice of mass movement climate justice politics in both paid workplace and community organizing.

<!--quoteend-->

> In addition to digging in where people work and live, its supporters need to be ready to intervene constructively in unexpected upsurges of protest and resistance like the “yellow vests” movement in France in 2018-19 and the uprising against racism in the US in 2020. Even if ecological concerns are not what is putting people into motion, ecological marxists risk irrelevance if they cannot respond to and become part of social eruptions in ways that allow them to build political relationships and help people to make connections and bring out the ecological dimensions of struggles against injustice.


### What Would Ecosocialism Be?

> Breaking with capitalism and beginning a transition to ecosocialism is not a short-term prospect anywhere in the world today, but this does not mean that the character of ecosocialism or what would be required to begin to reconstruct society in its direction are irrelevant matters.

<!--quoteend-->

> Ecological marxists in the tradition of Rosa Luxemburg and William Morris insist that the prerequisite for initiating this revolutionary transition would be a self-emancipatory rupture that establishes the democratic rule of the direct producers themselves, not the taking of state power by a party acting on their behalf.

<!--quoteend-->

> Of course, the dire state of the world ought to encourage collaboration among adherents of all strands of ecological marxism wherever possible. 

