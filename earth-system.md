# Earth system

> The Earth system comprises cycles and processes in sub-systems or “spheres” such as the [[atmosphere]], [[hydrosphere]], [[cryosphere]], [[geosphere]], [[pedosphere]], [[lithosphere]], [[biosphere]], and even the [[magnetosphere]] as well as well as the impact of human societies on these components. Earth systems research mostly analyses changes in these systems on a planetary scale. It brings together researchers from many disciplines. In comparison: The narrower notion of ecosystem describes the interrelationships between living organism and their environment in a particular unit of space.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

Any different from the [[climate system]]?

