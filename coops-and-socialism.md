# Coops and socialism

> You can start thinking about [[co-ops]] as places where people can start developing the skills they need under [[socialism]].
> 
> &#x2013; [What a Socialist Society Could Actually Look Like](https://www.jacobinmag.com/2019/08/socialist-society-future-cooperatives)

