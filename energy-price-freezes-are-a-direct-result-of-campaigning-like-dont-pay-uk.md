# Energy price freezes are a direct result of campaigning like Don't Pay UK

Claim
: [[Energy price freeze]]s are a direct result of campaigning such as [[Don't Pay UK]].

> After months of uncertainty, the government has finally acted on rising domestic energy prices, pledging £150bn over the next two years to partially freeze domestic energy prices. We should be clear this is a direct result of campaigns like Don’t Pay, which has nearly 200,000 pledges to refuse bill increases in October
> 
> &#x2013; [We’ve Won an Energy Bills Freeze. Now Let’s Demand More | Novara Media](https://novaramedia.com/2022/09/08/weve-won-an-energy-bills-freeze-now-lets-demand-more/)


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

I don't know enough.  But I can't imagine the Tories having had done it otherwise. It seems very likely it is only in response to pressure.

