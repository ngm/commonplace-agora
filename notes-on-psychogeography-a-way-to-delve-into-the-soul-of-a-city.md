# Notes on "Psychogeography: a way to delve into the soul of a city"

&#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

> [[Psychogeography]], as the term suggests, is the intersection of psychology and geography. It focuses on our psychological experiences of the city, and reveals or illuminates forgotten, discarded, or marginalised aspects of the urban environment. 

<!--quoteend-->

> Both the theory and practice of psychogeography have been around since 1955, when French theorist [[Guy Debord]] coined the term. While it emerged from the [[Situationist]] International movement in France, the practice has far-reaching implications. It’s relevant, for instance, in contemporary Sydney. 

<!--quoteend-->

> Psychogeographers advocate the act of becoming lost in the city. This is done through the [[dérive]], or “drift”.

<!--quoteend-->

> Because **purposeful walking has an agenda**, we do not adequately absorb certain aspects of the urban world. This is why the drift is essential to psychogeography; it better connects walkers to the city.

<!--quoteend-->

> Psychogeographers idolise the [[flâneur]], a figure conceived in 19th-century France by [[Charles Baudelaire]] and popularised in academia by [[Walter Benjamin]] in the 20th century. A romantic stroller, the flâneur wandered about the streets, with no clear purpose other than to wander. 

<!--quoteend-->

> Psychogeography has other uses besides drifting or re-enchanting marginalised spaces. It has a historical use as well. In cases where the landscape has been affected by crime or suffering, psychogeographic readings are especially poignant.

<!--quoteend-->

> The transition of a space from one use to another undergirds much of psychogeography’s preoccupation; the notion of a [[palimpsest]] – an object or piece of writing with new material superimposed over earlier writings – is particularly important. 

<!--quoteend-->

> Psychogeography thrives as an interrogation of space and history; it compels us to abandon – at least temporarily – our ordinary conceptions of the face value of a location, so that we may question its mercurial history.

