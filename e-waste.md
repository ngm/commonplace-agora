# e-waste

Electronic [[waste]].

Any discarded product that has a battery or plug. i.e. discarded [[EEE]].

> EEE becomes e-waste once it has been discarded by its owner as waste without the intent of reuse
> 
> &#x2013; [[The Global E-waste Monitor 2020]]

<!--quoteend-->

> A record 53.6 million metric tonnes (Mt) of electronic waste was generated worldwide in 2019, up 21 per cent in just five years
> 
> -   [GEM 2020 - E-Waste Monitor](https://ewastemonitor.info/gem-2020/)

<!--quoteend-->

> It is estimated that the amount of e-waste generated will exceed 74Mt in 2030. Thus, the global quantity of e-waste is increasing at an alarming rate of almost 2 Mt per year.
> 
> &#x2013; [[The Global E-waste Monitor 2020]]


## Resources

-   [GEM 2020 - E-Waste Monitor](https://ewastemonitor.info/gem-2020/)

