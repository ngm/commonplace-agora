# Personal sites

> My personal site is a repository for my memories, experiences, feelings, recipes, tips, photos, and more. [&#x2026;] it is an ever-growing extension of myself that I have total control over, my mirror and memory aid. I want to be able to look back at this when I’m eighty and thank my past self for surfacing things that I otherwise would have forgotten.
> 
> &#x2013;  [On personal sites, and adios analytics — Piper Haywood](https://piperhaywood.com/on-personal-sites-adios-analytics/)

Hmm, reading this and also Amy Hoy's post recently ([How the Blog Broke the Web](https://stackingthebricks.com/how-blogs-broke-the-web/)) is making me think a bit different about how I refer to my site(s). Think I'll think of it a bit more as having a [[personal site]], rather than framing it as I have a 'blog' or a 'wiki'.  Both of which are great technologies, but I want to be a little bit freer about how I think about what my home on the web is and how I structure it.

