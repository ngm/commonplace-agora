# Replacing Google Fonts with Bunny Fonts

Switching from [[Google Fonts]] to [[Bunny Fonts]].

([[What's wrong with using Google Fonts?]])

Easy peasy.  Much simpler than [[Replacing Google Fonts remote server usage with a locally-served font]].

All you have to do is find and replace https://fonts.googleapis.com/css with https://fonts.bunny.net/css and it works straight off.

