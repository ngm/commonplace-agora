# Amazon employees plan to walk off the job as tech worker tension rises

URL
: https://www.washingtonpost.com/technology/2023/05/22/tech-layoffs-amazon-walk-out-meta/

> In messages sent out via Slack and email, employee organizers urged their colleagues to walk out on May 31 — one week after the company’s annual shareholder meeting — in response to frustration over layoffs and the return-to-office mandate, as well as concerns about Amazon’s climate commitments.

