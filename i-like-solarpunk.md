# I like solarpunk

[[I like]] [[solarpunk]].


## Because

-   [[Solarpunk is about technologies that de-abstract human relationships with the material world]]
-   &#x2026;


## Epistemic status

Type
: [[feeling]]

Strength
: 6

I definitely do, I just haven't figured out yet how much it's a fad, a trend, a lifestyle thing, how much it can be genuine praxis.

I think it's more than an aesthetic.  Though to be fair if it's just an aesthetic that's fine, so long as it doesn't **claim** to be more.

