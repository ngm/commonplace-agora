# Information Civics

An
: [[article]]

Written by
: [[Paul Frazee]]

Found at
: https://infocivics.com/

Great article by [[Paul Frazee]].  Primarily about how to distribute authority more equally in social computing networks.  Also touches on how [[civics]] applies to technology.

-   [[Authority in networks]].
-   [[Centralised applications are authoritarian]].
-   [[Anti-authoritarian networking]].
-   [[Distributed applications can only do what their protocols support]].
-   [[Architectural rights]].

