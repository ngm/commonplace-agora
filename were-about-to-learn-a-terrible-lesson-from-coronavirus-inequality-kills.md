# We're about to learn a terrible lesson from coronavirus: inequality kills

-   source: [We're about to learn a terrible lesson from coronavirus: inequality kills | O&#x2026;](https://www.theguardian.com/commentisfree/2020/mar/14/coronavirus-outbreak-inequality-austerity-pandemic)
-   tags: [[reading]]

[[Austerity]].

> Like every crisis, this one is **likely to affect working-class and poor people worst**. That is not inevitable. It’s a choice – and one **within our power to stop, if only we had the will to do so**.

<!--quoteend-->

> Men living in the poorest communities in the UK have an average of 9.4 years shorn off their life expectancies compared with those in the richest areas;


## super rich can avoid it

> The super-rich are fleeing on private jets to luxury boltholes in foreign climes, while the well-to-do may deploy their private health insurance to circumvent our already struggling and soon to be overrun National Health Service


## blue collar workers can't

> Uber drivers, Deliveroo riders, cleaners: all in low-paid jobs, often with families to feed. **Many will feel they have no choice but to keep working**. While many middle-class professionals can protect themselves by working from home, supermarket shelves cannot be stacked remotely, and the same applies from factory workers to cleaners


## health inequality already exists

> British Heart Foundation found that working-class Tameside in the north-west has a heart disease mortality rate more than three times higher than well-to-do Kensington and Chelsea

same with asthma, diabetes


## stress and depression weaken immune system

> We know that depression and stress weaken our immune systems, and the research is clear: those on low incomes are disproportionately likely to suffer from poor mental health.

<!--quoteend-->

> **A decade of austerity, and a social order that deprives millions of citizens of a comfortable existence**, will mean many more deaths in the coming weeks and months that could have been avoided. 

