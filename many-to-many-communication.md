# many-to-many communication

> The agorae and assemblies of Ancient Greece could similarly be understood as facilitating forms of many-to-many communication, and doing so in ways that, albeit imperfectly, embody some of the democratic processes common to anarchist and radical politics
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Traditional marketplaces, sports stadiums, graffiti and community notice boards can all be seen as instances of many-to-many communication
> 
> &#x2013; [[Anarchist Cybernetics]]

