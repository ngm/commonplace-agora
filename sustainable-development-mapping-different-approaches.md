# Sustainable development: mapping different approaches

A
: [[journal article]]

URL
: https://doi.org/10.1002/sd.244

Year
: 2005


## Abstract

> [[Sustainable development]], although a widely used phrase and idea, has many different meanings and therefore provokes many different responses. In broad terms, the concept of sustainable development is an attempt to combine growing concerns about a range of environmental issues with socio-economic issues. To aid understanding of these different policies this paper presents a classification and mapping of different trends of thought on sustainable development, their political and policy frameworks and their attitudes towards change and means of change. Sustainable development has the potential to address fundamental challenges for humanity, now and into the future. However, to do this, it needs more clarity of meaning, concentrating on sustainable livelihoods and well-being rather than well-having, and long term environmental sustainability, which requires a strong basis in principles that link the social and environmental to human equity.

They have a handy chart for getting a bit of a visual orientation of the different trends that you could situate within an overarching notion of 'sustainable development'.

![[Abstract/2022-11-17_23-21-54_1f059030d4b364bb.png]]

It has the axes:

-   x: 'increasing environmental concerns', range runs from 'virtually none' through 'techno-centred' to 'eco-centred'
-   y: 'increasing socio-economic well-being &amp; equality concerns', range runs from 'inequality' to 'equality'

You also have bands named 'Status Quo', 'Reform', and 'Transformation'.
Various movements are placed on the chart.

At the top right, those which are most eco-centred and most desiring of socio-economic well-being and equality, are [[ecofeminism]], [[ecosocialism]], [[social ecology]] and indigenous/'south' movements.  They all fall into the 'Transformation' band.

