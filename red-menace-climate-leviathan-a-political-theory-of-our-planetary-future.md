# Red Menace: Climate Leviathan: A Political Theory of Our Planetary Future

A
: [[podcast]]

URL
: https://redmenace.libsyn.com/climate-leviathan

Great discussion of [[Climate Leviathan]] by the [[Red Menace]] crew. Very engaging overview of the book. Definitely need to get around to reading it.

Alyson and Breht both thought it a very worthwhile book and liked much of its analysis. They veer more to [[Climate Mao]] than [[Climate X]], but still found value in X.

I do think there's a  strong argument that you'd need a [[planetary sovereign]] of some kind to tackle the urgent and global [[polycrisis]].

