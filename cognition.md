# Cognition

> Cognition refers to the way in which external information from the environment is processed. 
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

