# Looks Like New: Can Software Handle Ethics?

URL
: https://news.kgnu.org/2021/01/looks-like-new-can-software-handle-ethics/

> Free and open-source software is a kind of miracle. It’s community-created technology, a commons that anyone can contribute to and use. But those communities are not always the utopias they’re made out to be. Many have been havens for exclusionary cultures, and their tools have been used to perpetrate human-rights abuses.
> 
> Coraline Ada Ehmke is a software developer working to change that. She created the [[Contributor Covenant]], a widely adopted code of conduct for open-source projects. Now she is leading the [[Ethical Source Movement]], which enables projects prevent their tools from being used for purposes they oppose.

