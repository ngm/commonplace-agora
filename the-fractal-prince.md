# The Fractal Prince

Sequel to [[The Quantum Thief]].

Also very good.  I think I prefer Quantum Thief slightly better.  Fractal Prince is even harder to get a solid grasp on the underlying ideas.  The Aun, Wildcode, Jinni, etc, it's all really interesting but I couldn't tell you right now what general concepts they're hinting at.

