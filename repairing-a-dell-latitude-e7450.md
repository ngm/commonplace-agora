# Repairing a Dell Latitude E7450



## Problem

It turned off out of the blue, and now no signs of life.


## Diagnosis

-   doesn't power on with power button
-   no LEDs come on at all, either with charger in or out
-   there's an LED on the charger that is not coming on, either - perhaps the charger is the issue?
-   thinking that perhaps the power supply stopped working, and then the battery drained
-   however, owner doesn't think there was any reports from Windows of low battery before it died&#x2026;


## Work undertaken

Things to try

-   [ ] probe the power supply for signs of life


### Testing the power supply

It's listed as: 19.5 V output, 3.34A

Testing the power supply with the multimeter, it actually was reading out around 19.5.  And lo and behold, the LED is actually back on again now.

So let's see if it'll power the laptop again OK.

Wait, weird, it's gone again.  Is it a loose connection?  Or the laptop shorted it?
Yeah, the laptop shorts it out it seems?  The light goes off when you plug it into the laptop.

[My laptop charger turns off as soon as I plug it in to the laptop. It's not t&#x2026;](https://www.quora.com/My-laptop-charger-turns-off-as-soon-as-I-plug-it-in-to-the-laptop-Its-not-the-charger-thats-the-problem-Could-it-be-a-bad-motherboard)

It sounds like it might be a short circuit on the motherboard&#x2026; oh dear, not good.


### Problem with the motherboard

[Dell power supply light goes out when plugged into the laptop computer #short&#x2026;](https://www.youtube.com/shorts/l2-ckzT7NP8)
[‎Laptop shorting out charger | DELL Technologies](https://www.dell.com/community/en/conversations/inspiron/laptop-shorting-out-charger/647fa099f4ccf8a8de5a2f16)
[laptop adapter light turns off when plugged in - YouTube](https://www.youtube.com/watch?v=nOlJ4_4eQso)

Possibly the laptop overheating could have caused it?

[laptop motherboard "shorts" when connected to adapter - Super User](https://superuser.com/questions/509898/laptop-motherboard-shorts-when-connected-to-adapter)

This post suggests a couple of things to try:

[‎Latitude E7450 Power Failure | DELL Technologies](https://www.dell.com/community/en/conversations/latitude/latitude-e7450-power-failure/647f8c85f4ccf8a8decd0523)

-   check the voltage of the internal power cable if possible
    
    This guy has a great video, same symptom:
    
    [Dell Latitude E7450, no power, charger cuts out - LFC#318 - YouTube](https://www.youtube.com/watch?v=WQMQ3tm5Dho) 
    
    He shows how to check the internal DC jack.
    
    I think he's checking the 2nd and 3rd pins there.  If I do that, no continuity.
    
    Here's a handy teardown video for replacing DC jack:

[Dell Latitude E7450 DC Jack Replacement Video Tutorial - YouTube](https://www.youtube.com/watch?v=fkTtzH3IUu8) 


## Resources

-   Owner's manual https://dl.dell.com/topicspdf/latitude-e7450-ultrabook_owners-manual_en-us.pdf

