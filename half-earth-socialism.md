# Half-Earth Socialism

A
: [[book]]

URL
: https://www.versobooks.com/books/3818-half-earth-socialism

An excellent book with a bold programme for [[eco-socialism]].  Very readable.

Touches on [[socialist calculation debate]], [[Markets vs planning]], [[climate science]], [[climate breakdown]], [[eco-socialism]], [[Project Cybersyn]], [[Viable system model]], etc etc.

I like that it has some [[speculative fiction]] and homage to things like [[News from Nowhere]].

The accompanying game that you can play at https://play.half.earth is brilliant.

The main thing that it is lacking is a plan for [[revolutionary transition]].  But they're quite open about that, to be fair, happy to be a work of [[utopian socialism]].  The appropriation of the loaded term "Half-Earth" is questionable, but probably deliberately provocative.

Main nub of it:

-   [[Rewilding]] half the earth - [[Half-Earth]]
-   [[Veganism]]
-   A rapid transition to [[renewable energy]], [[decarbonisation]], and [[energy quotas]]
-   Worldwide socialist planning

> A kind of socialism based on the recognition of ecological limits and the limits of our understanding of the natural world.  Envronmental crisis can only be overcome by socialism, because only socialism allows humans to decide to manage economic growth (impossible under capitalism).  Socialists have to be environmentalists because they have to realise that total control over nature is impossible, the more we intervene in nature the more risks we take, such as pandemics and climate change.  Also arguging for Neurathian conscious control of the economy.  We propose rewilding half of the planet, having energy quotas, and veganism, to reduce our impact on nature, while also ensuring a good life for all.
> 
> &#x2013; paraphrasing from [[Drew Pendergrass and Troy Vettese on Half Earth Socialism]]

In [[Climate Leviathan]] terms, I would suggest that their proposals fit as a mixture of [[Climate Mao]] and [[Climate X]] - i.e. staunchly socialist and non-capitalist, with a mixture of central authority and democratic decentralisation.


## Sections of the book

> Think then of Half-Earth Socialism as a cookbook divided into four courses: the philosophical, the material, the technical, and the imaginative

![[hes-sections.png]]

> Our framework draws on [[ecology]], [[energy studies]], [[epidemiology]], [[cybernetics]], [[history]], [[mathematics]], [[climate modelling]], [[utopian socialism]], and, yes, [[neoliberalism]]

<!--quoteend-->

> We invite everyone from all liberatory traditions to join us in the revolutionary kitchen to think up many new recipes and work together to realize them


### Philosophical


#### The neoliberals and the unknowability of nature.

> The environmental crisis forces us to decide between controlling the market and controlling nature

<!--quoteend-->

> We follow his example of digging deep into the epistemic layers of our beliefs, but whereas Hayek covered the market with the veil of ignorance, we drape that veil over nature

Don't plan nature, plan the market.

> The environmental crisis, however, strains the neoliberals’ foundational postulates because they have to decide what is more complex and unknowable – the market or nature

<!--quoteend-->

> Rather than the conscious control of nature, the neoliberals seek one unconscious realm (nature) to be subdued by another (capital

<!--quoteend-->

> In response, we try to out-Hayek Hayek by arguing that nature is more unknowable than the market, and therefore far more deserving of our awe as an unconscious, decentralized, and unimaginably complex system


#### Hegel, Malthus and Jenner

> Each of these thinkers is a primogenitor of one of the three great lineages in environmental thought: Hegel’s [[Prometheanism]], [[Malthusianism]], and [[Jennerite ecological scepticism]]

<!--quoteend-->

> One can go beyond Boulding by giving his framework a Jennerite twist: Earth is a natural machine, both ancient and alien, whose operating systems we will never fathom, and therefore it is wisest to let the ghost in the shell control the circuitry even if we do not always understand it ourselves

<!--quoteend-->

> The process of nature’s humanization stops not when it is realized, but when our species comes to recognize that this process undermines the basis of human freedom. Climate change, emerging zoonotic diseases, and other environmental crises make a mockery of this pretence of control. To bring the humanization of nature to an end, the collective consciousness (Geist) must become aware of its own limits

<!--quoteend-->

> The task of unbuilding makes clear that environmentalism isn’t so much the idealization of ‘pristine’ nature (though it is vital to protect intact ecosystems) but the recognition that it is still possible to repair our broken world


### Material

![[hes-material.png]]

> The goals of Half-Earth socialism are simple enough: prevent the [[Sixth Extinction]], practise ‘natural geoengineering’ to draw down carbon through rewilded ecosystems rather than SRM, and create a fully [[renewable energy]] system

<!--quoteend-->

> way to save land would be to produce less energy, which is why Half-Earth socialism embraces quotas. The exact number can be debated, but we admire the target of the [[2,000-Watt Society]]

<!--quoteend-->

> An economic system resembling Half-Earth socialism can actually be found in recent history: Cuba’s [[Período Especial]]


### Technical

[[Socialist planning]].

> the third chapter that tackles the difficult problem of organizing production and distribution without the market.

<!--quoteend-->

> If it is necessary to prevent the market commodifying and controlling all of nature, and we also have a sense of what material goals we want to achieve, how then is it possible to organize production and consumption without a market? We draw on a range of influences, including [[Soviet cybernetics]] and mathematics, Chile’s ‘[[Cybersyn]]’ programme, [[meteorology]], and cutting-edge [[integrated assessment models]] (IAMs) used by climate scientists today.

<!--quoteend-->

> Half-Earth socialist planning is inspired by a slew of traditions, including Neurath’s [[in natura calculation]], Kantorovich’s [[linear programming]], Beer’s Cybersyn, and the climate-economy models of Austria’s IIASA

<!--quoteend-->

> necessary to marry Kantorovich’s technical vision to Neurath’s [[democratic socialism]], in which planners lay out their goals and constraints in natural units and then devise different plans that could be chosen by an informed public

similar to [[P2P Accounting for Planetary Survival]]?

> In this chapter, we will detail how to organize a democratically planned economy in an age of ecological crisis

<!--quoteend-->

> Even a wisely managed eco-socialist utopia would still be plagued by some inefficiencies and shortages, as we will discuss later in the chapter. However, we believe that it is worth paying to gain other advantages, such as a [[stable climate]], wondrous [[biodiversity]], and a respite from pandemics. Half-Earth socialism also promises the prospect of a unified humanity, peace, and equality, with an economy

<!--quoteend-->

> Half-Earth socialism requires a similar balancing act, supplying everyone with the material foundations for a good life – sustenance, shelter, education, art, health – while protecting the biosphere from destabilization

[[Doughnut Economics]]-y.

> While the static, one-off calculations made using linear programming are a valuable tool in managing any complicated project – the method is ubiquitous in contemporary applied mathematics, including in planning renewable energy systems – we will need other tools to allow local administrators to meet the needs of the people they serve, while simultaneously achieving global goals such as rewilding or long-distance trade.

<!--quoteend-->

> There were two main currents that shaped planning debates in the Soviet Union over the following decade: the theory of mathematical optimization (e.g., linear programming) and the cybernetic theory of control, built around differential equations

<!--quoteend-->

> Thirdly, and perhaps more importantly, these loosely connected models create space for democracy, diversity, and political initiative, all unified in pursuit of national or even global goals

<!--quoteend-->

> Although Chile in the early 1970s might seem far removed from the global crises in the 2020s, Cybersyn is perhaps the closest historical analogue to Half-Earth socialism

<!--quoteend-->

> In our effort to understand how Half-Earth socialism could work in practice, we have undertaken a tour of socialist planning theory that has travelled from Leningrad to Vienna to Novosibirsk and finally to Santiago. At this point, we return to Siberia some two decades after Kantorovich’s work at CEMI began. 

[[Olga Burmatova]]

> As Kantorovich noted in his vision of multilevel linear programming, a planned society needs a way to balance an overarching national or global vision with sensitivity to local conditions

[[climate science]]. [[Downscaling]].  [[Data assimilation]].

> All this data does not mean that we fully know nature, only that Half-Earth socialist planners would have access to the vital signs of the planet so they could modify humanity’s interchange with nature when necessary

[[János Kornai]], [[shortage economy]]


### Imaginative

> British socialist utopians in this period were a tightly knit group. [[Oswald]] might have been an acquaintance of [[William Godwin]], whose utopian writings provoked Malthus to write his Essay on Population. Godwin, who dabbled in vegetarianism, knew herbivorous radicals [[Robert Owen]] and [[Percy Shelley]]

<!--quoteend-->

> We hope that Orwell spins in his grave when Half-Earth socialism becomes a movement, radiant in its queer, feminist, vegetarian glory

<!--quoteend-->

> The other inheritor of the unorthodox Left was the [[Frankfurt School]], whose theorists criticized the destruction wrought by the mindless conquest of nature. Adorno, Horkheimer, and Benjamin, as well as Alfred Schmidt and Herbert Marcuse, all contributed to the foundations of eco-socialism

<!--quoteend-->

> The point, however, is not simply to substitute socialist utopianism for Promethean Marxism, but rather to strive for a synthesis of the two to create a new, epistemically humble socialism.

<!--quoteend-->

> humanity faces a choice: to futilely try to further the humanization of nature through mad schemes like geoengineering, or to plan an economy within planetary boundaries.


## Criticism

-   [Leigh Phillips (@Leigh<sub>Phillips</sub>): "I am still want to publish a review of the&#x2026;](https://nitter.noodlemaps.net/Leigh_Phillips/status/1532818367026892800?cursor=LBkWgICjzem%2F1cUqJQQRAAA%3D#r)


### Too austere

> By contrast, the authors’ vision is almost as austere as Pol Pot’s
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> And their plan to rewild half the Earth’s habitable surface would require perhaps the most preposterous proposal of all: the imposition of universal mandatory veganism (otherwise the numbers would never add up).
> 
> &#x2013; [[Mish-Mash Ecologism]]

Doesn't seem that preposterous to me.


### Weak on transition

Weak on the question of [[revolutionary transition]], but to be fair they clear from the outset that this text is on programme.

> Vettese and Pendergrass invite us to imagine that ‘the Half-Earth socialist revolution happens tomorrow’, but they do not explain how this might occur. Though they gesture towards a pro-Half Earth political coalition, its members are vaguely delineated: ‘there should be animal-rights activists and organic farmers there, as well as socialists, feminists and scientists’ – **constituencies that make up miniscule fractions of the eight billion-strong population they hope to corral**. As for broader layers such as social classes, Half Earth Socialism is largely silent. Like The Future is Degrowth and much of the left for the last half-century, **the authors assume that a ‘movements of movements’ – uniting various disparate and subaltern groups – will eventually gain enough power to confront capital**.
> 
> &#x2013; [[Mish-Mash Ecologism]]


## Misc


### Transition

> In some countries it might follow a path similar to the rise of Nelson Mandela and the African National Congress in South Africa: a mixture of strikes, divestment, sabotage, elections, boycotts, and violence. In other countries a purely electoral strategy might work, but such a victory would only mark a new phase of struggle.


## Reviews

-   [[Review of Half-Earth Socialism]] - my in process review
-   [Yearning for Utopia: A Review of Half Earth Socialism](https://sentientmedia.org/yearning-for-utopia-a-review-of-half-earth-socialism/)
-   [Book Review: Half-Earth Socialism: A Plan to Save the Future from Extinction,&#x2026;](https://blogs.lse.ac.uk/lsereviewofbooks/2022/05/30/book-review-half-earth-socialism-a-plan-to-save-the-future-from-extinction-climate-change-and-pandemics-by-drew-pendergrass-and-troy-vettese/)
-   https://journals.sagepub.com/doi/abs/10.1177/03098168221101949h

