# Political economy

The interaction of [[power]] and [[value]].

> Political economy is the study of production and trade and their relations with law, custom and government; and with the distribution of national income and wealth.
> 
> &#x2013; [Political economy - Wikipedia](https://en.wikipedia.org/wiki/Political_economy) 

I am am particularly interested in [[technology and political economy]].


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, &amp; Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)


## Flashcards


### Political economy


#### Front

What is political economy?


#### Back

The study of production and trade and their relations with law, custom and government

and with the distribution of national income and wealth.

