# Systems theory

> ‘Theory’ might sound drily intellectual, but actually the systems perspective is a holistic way of reflecting on the world, which involves the whole of our being: in recent research, the mind is not separate from the body, nor from feelings.
> 
> &#x2013; [[How systems theory can help us reflect on the world]]

<!--quoteend-->

> Systems are not fated to acquire a runaway ‘bad’ dynamic, it is possible for them to function sustainably. To understand this, systems theory suggests two complementary conceptual approaches: [[thermodynamics]] and [[information]]. Information applies to internal structure, the extent to which an ensemble ‘makes sense’ and functions coherently. Thermodynamics addresses the energy flows.
> 
> &#x2013; [[The Entropy of Capitalism]]

