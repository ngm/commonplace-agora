# The Tyranny of Openness: What Happened to Peer Production?

URL
: https://mediarxiv.org/ecfpj/

Author
: [[Nathan Schneider]]
    
    About [[Free software licensing]], [[Free software economics]].

