# Vue



## Projects that I like that make use of Vue

-   [iznik-nuxt](https://github.com/Freegle/iznik-nuxt) (powers [[Freegle]] front-end)
-   [[Karrot]] (community food sharing)
-   kinopio (https://pketh.org/how-kinopio-is-made)
-   [YunoHost admin](https://github.com/YunoHost/yunohost-admin/pull/316)
-   [Pixelfed](https://github.com/pixelfed/pixelfed) (federated photo sharing)
-   Some parts of [Nextcloud](https://github.com/nextcloud/nextcloud-vue)


## Concepts


### Data and props

In a nutshell: 

> Data is the private memory of each component where you can store any variables you need. Props are how you pass this data from a parent component down to a child component.
> 
> &#x2013; [Props Versus Data in Vue: The Subtle Differences You Need to Know - Michael T&#x2026;](https://michaelnthiessen.com/vue-props-vs-data/) 

That whole article is a handy overview of the difference between the two: https://michaelnthiessen.com/vue-props-vs-data/


### Reactivity

This is to me so far the nicest thing about using Vue.  You worry about state and logic, and the view is rendered dynamically without having to toggle bits of the DOM all over the place.

> With Vue you don't need to think all that much about when the component will update itself and render new changes to the screen.
> 
> This is because Vue is reactive.
> 
> Instead of calling setState every time you want to change something, you just change the thing! As long as you're updating a reactive property (props, computed props, and anything in data), Vue knows to watch for when it changes.
> 
> &#x2013; [Props Versus Data in Vue: The Subtle Differences You Need to Know - Michael T&#x2026;](https://michaelnthiessen.com/vue-props-vs-data/) 


### Vuex

Vuex is based to some degree on the Elm architecture.

> Most user interface bugs are state-related. Not a surprise when you consider that UI is one big concurrency problem. User interaction, animation, and asynchronous processes such as http requests, all happen constantly within a UI, returning who knows when, interleaving with each other, and mutating state. It’s terribly easy for these processes to step on each other’s toes and produce a broken app states.
> 
> &#x2013; [ObservableStore - by Gordon Brander - Subconscious](https://subconscious.substack.com/p/observablestore)

