# The humanization of nature

> The humanization of nature is the process by which humanity overcomes its alienation from nature by instilling the latter with human consciousness through the process of labour – transforming wilderness into a garden
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> The humanisation of nature is the process by which humanity employs labour as a means to re-direct flows of energy within natural systems to achieve social ends. Or as Marx puts it in Capital: ‘man confronts the material of nature as one of her own forces. He sets in motion arms and legs, head and hands, the natural forces of his body, in order to appropriate the material of nature in a form suitable for his own needs. By thus acting through this motion on the nature which is outside him and changing it, he at the same time changes his own nature’.
> 
> &#x2013; [The Humanisation of Nature and the Naturalisation of Marxism | Historical Mat&#x2026;](https://www.historicalmaterialism.org/book-review/humanisation-nature-and-naturalisation-marxism) 

