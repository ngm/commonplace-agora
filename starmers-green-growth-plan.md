# Starmer's green growth plan

> Starmer made clear that his alternative approach to boosting growth involved tackling three crises jointly – the climate, the cost of living, and the wider economy.
> 
> &#x2013; [Keir Starmer unveils green growth plan to counter Liz Truss’s tax cuts | Labo&#x2026;](https://www.theguardian.com/politics/2022/sep/24/keir-starmer-unveils-green-growth-plan-to-counter-liz-trusss-tax-cuts)

Tackling those three crises jointly is a good angle.  Of course need to dig in to the details - it's a green **growth** message, and probably fairly green capitalist.

But, at least, finally at least one thing that clearly differentiates Labour from the Tories, which Starmer has failed to do so far.

