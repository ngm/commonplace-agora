# Transition Together Summit



## Blizzard of Stories.

Community groups around UK, Europe and beyond.

Inspiration on transformational experiences.


### Gleaners Cafe

-   workers coop in Walthomstow London
-   3 years, started as a pair, now 5
-   pay what you feel
-   we'll all part of one system that needs to loved and nurtured together
-   3 days a week, surplus and organic produce
-   surplus helps reduce waste
    -   also keeps cost down
    -   from shops, farms, charities, wholesalers
-   surplus is fresh produce
-   purchasing power to buy organic
-   worked with over 100 volunteers
-   exchange of skills with volunteers
-   all of it exists within a community centre
-   pay what you feel pricing
    -   4 different prices for everything
        -   choose your shape, pay according to your shape
    -   is it an alternative to a food bank?
        -   no it's not


### Transition Streets

-   simple idea, little money or resources required
-   bring people together and amazing things can happen
-   created in 2009 in Totnes
-   seven session program where neighbouring households come together to work through particular topics
    -   energy use, water, etc
-   fosters connections between neighbours
-   some mini stories
    -   someone had lived on the street for 20 years but didn't know anyone - by the end of the project she was sharing a car
    -   a person who needed someone to be with for 5 days after medical thing - someone from Transition Strees helped her


### transition black isle

-   don't be afraid to steal good ideas from elsewher
-   take your time with planning
-   highland good food


### east marsh community

-   100 houses for 100 years
-   ethex


### transition kentish town

-   think and do
-   pop up shop for 6 weeks
-   became a social enterprise
-   go to the community, don't expect the community to come to you
-   Tenant's Residence Halls (housing estates)
    -   apparently they exist wherever you are?


### Castlemaine Currency Project

-   it's an art project&#x2026;
-   10,000 dollars worth of the coins
-   inspired by totnes pound
-   local currency


### Transition Marlborough

-   2011
-   council became a transition council
-   community fridge, permaculture
-   it's all about "knitting, weaving and seed sowing"
-   bike racks, cycle routes, bulk buying from suma, zero waste shop
-   permaculture group is their 'magnetic centre'
-   allotments are popular
-   drive relationships, link people together


### zero carbon guildford

-   practical solutions for residents, schools, churches, etc
-   started in early 2020 - 18 months to get keys to a building
    -   felt disappointing at the time for it to take so long
-   community led shop
-   lots of cool projects
-   terracycle
-   session on Friday on climate hubs
-   zerocarbonguildford.org


### Transition Bro Gwaun

-   community wind turbine and community climate fund
-   endurance 225 kw
-   £45,000 for planning etc
-   £285,000 purchase and installation cost
-   loans from 27 local individuals and 4 community groups
-   loans will be repaid by Feb 2023


### Bristol Energy Cooperative

-   a community benefit society
-   rooftop solar, solar farms, battery projects, microgrid schemes
-   community focused approach
-   15 million pounds, half from the community, 1500 members
-   stake in community energy projects
-   1. #communityenergy
-   2. invest in BEC
-   3. volunteering (energy champion)
-   4. set up community energy
-   community energy england


### Grenoble

-   city councillor, cities in transition network
-   citizens and political movements in grenoble
-   take the lead from local scale
-   flaubert eco district
-   school meals
-   grenoble biennial cities in transition


### transition telford

grow local


### transition stirling

-   repair cafe, tool libraries, community fridge (very popular), reuse hub (too good to go from the tip)
    
    school uniform reuse
    
    underused community centre


### rapid transition alliance

-   network of networks
-   orgs working towards a sustainable future
-   transition network is one


## Rewilding


### George Monbiot

Sheeps are a bit problem with 'dewilding'.
Could have been temperate rainforest.
All the species we are missing.
We don't allow lynx back in to this country, all our large herbivores.
If we don't rewild, there is no help for our biosphere
Rewilding draws carbon more than any technology
We have to reduce carbon consumption
But we also have to draw carbon down
Grazing by livestock is the most destructive use of land
Most destructive force on earth in terms of biodiversity
28% for grazing, but animals fed on graze just 1% of protein
12% for all crops

employ far more people with better incomes on a nature based economy than traditional land uses
dont' like top down aristorcratic version of rewilding which has become fashionable
trees, wetlands that we're missing in our lives.
improve our own quality of lives and sense of belonging

"rewilding politics"


### Jay Griffiths

our own minds have been colonised by so many issues
mental sovereignty 
our attention is a commodity
i will concentrate on something
we have to think differently from the mainstream
"angels" -&gt; insects
we value iireal angels rather than real insects
the importance of insects
insect collapse in 2018
intensive agriculture is killing us and our insects


### beth collier


#### background

anthropology
grew up in the countryside, rural smallholding in suffolk
what does it mean to be disconnected from nature?
"dirty, boring, alien"
traditional therapy, 2 chairs, indoors
working with children with anger issues, taking them out in to the woods, in to a healthy habitat
nothing wrong with the children, but something wrong with the habitat
does therapy work in woodland and countryside (mostly with adults)
nature is considered a "significant other"
loss and trauma in a disconnect from nature, but may not be consciously aware of it
relational trauma


#### nature allied psychotherapy

"nature-therapy school"
nature allied psychotherapy off the back of these experiences
nature as a co-therapist
a type of eco-therapy
often these are one offs though
NAP is about regular ongoing therapy in nature
for some, natural world is a good place to talk about difficult feelings
a "dynamic container", always changing
nature is a co-therapist, holding and nurtuing the client
therapy in nature
forming a relationship, claiming our place as part of nature
find and foster a secure attachment to nature
we don't feel judged, we feel accepted, we can rely on them
we can find these qualities in relationship with nature ("mother nature")
we are currently very separate from natural cycles of the natural world


#### wild in the city

-   ecotherapy, woodland living skills, natural history
-   why do communities of colour spend less time in nature in the UK than white peopl
-   a feeling of not belonging, of feeling othered.  hence feeling safer in the city
-   stigma around colonialism and slavery, feeling less than from being connected to nature
-   bring back oral tradition for learning about natural world
-   to avoid disenfranchisement
-   "the grandparent model" - a way in which you don't feel like you're having a lesson
-   traditional skills for living in harmony with nature and the natural world
-   reclaiming (rewilding) our relationship with nature
-   feeling as a  PoC that we belong in nature


### frag last

-   buxton
-   stronger roots project
-   developing the inner wild child


#### stronger roots


#### ash dieback

-   beech and ash have been dying naturally also
-   so doing some replanting


#### neolithic revolution

-   manipulation of landscape
-   ideas from the continent
-   disconnect from nature began
-   agriculture came in
-   sheep are a problem
-   field of sheep
-   sheep used to be wild animals
-   you feel connected to nature when you are part of the ecosystem
-   nice grass replaced with rye grass


#### rewilding upsets a lot of people


#### what is it?

-   everyone views it all differently
-   can seem complex and out of reach for a lot of people
-   tied in with various issues
-   a lot boils down to money, land ownership and power


#### how can we rewild ourselves

-   forest school
-   sensory awareness, natural play
-   if you can understand grass

Have to be careful with rewilding.
Support from the left and the right.
'aristocratic rewilding'
community rewilding, community ownership of land

perhaps to appeal to the mainstream - perhaps we should focus on the topic of health.  it can be politically alienating.
greater the diversity of species

i wonder if there is a rough figure on how much we need to rewild for it to be effective - like previous question, probably depends on definition of effective - but I guess at a global level, effective in the sense of preventing biodiversity loss to avoid collapse, and capturing the level of carbon we need it to?

a book i've just started reading is called half-earth socialism, and that suggests as the title suggests, is that half of the planet is required to be rewilded - i think that's intended to be quite provocative

does that sound like a realistic figure?  should we be aiming to rewild half of the UK, half of every region, town, city?

precision fermantation, would allow for rewilding vast tracts of the world

get the councils in the mix

how do we work with farmers?

-   

