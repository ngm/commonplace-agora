# A Platform Designed for Collaboration: Federated Wiki

URL
: https://www.freefairandalive.org/read-it/#8

A section in the book [[Free, Fair and Alive]] on [[Federated wikis]] and the [[commons]].

Discusses [[FedWiki]] in particular but can apply to [[Federated wikis]] in general (e.g. [[Agora]], [[IndieWeb]]).

> Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:27]</span></span>:
> Participants in a Federated Wiki can decide to interconnect their wiki pages into a neutral, shared neighborhood of content, through which a consensus of viewpoints becomes visible.

<!--quoteend-->

> Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:28]</span></span>:
> This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration. But in fact the effect is quite the opposite: giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis results in a richer, more robust commons.

<!--quoteend-->

> Imagine a huge continent of diverse residences. Some have only a few rooms. Others exist within skyscrapers and provide space for hundreds of rental homes with multiple rooms. Some are clustered together as neighborhoods. Others are smaller and more isolated from other flats and houses. These residences are dispersed all over the continent, but there are irregular corridors, pathways, and roads that can potentially interconnect them all.

<!--quoteend-->

> To use another metaphor: people using Fedwiki sites are like gardeners or farmers. They can plant as many fields or gardens as they want, and reap the harvest from their own Fedwiki, but anyone else can also use someone’s harvest to enhance their own fields and gardens. Instead of toiling under a regime of private, competitive exclusion, the system encourages cooperative gains through commoning.

<!--quoteend-->

>  Page 257 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:41]</span></span>:
> In other words, the Fedwiki software creates protected individual spaces for content generation while facilitating diverse permutations of collaborative authoring on a massive scale. It opens countless paths for individuals to organize their own knowledge while easily sharing it with others, and, what’s more, enabling a commons of knowledge to arise, without the intervention of an editor.

<!--quoteend-->

> The Fedwiki platform blurs the supposed duality of the individual and the collective. By design, these two realms in Fedwiki world are integrated in a mutually enlivening way. **Having my personal wiki does not contradict or interfere with my contributing to the commons — quite the opposite**. This is [[Ubuntu Rationality]] at work and a vibrant example of the [[Nested-I]]!

<!--quoteend-->

> Interestingly, the core idea enshrined in the design of Federated Wiki is similar to the design of many monasteries, as discussed in Chapter Five, in providing supportive environments for collective life while also protecting personal choice.

<!--quoteend-->

> Fedwiki represents a new type of network platform that sidesteps the domination/subordination dynamics of conventional property

