# a re-commonification of welfare systems

> “With the [[welfare state]], most of these commons were state-ified, i.e. managed by the state, and no longer by the commoners themselves. […] Today, with the crisis of the welfare state, we see the re-development of new grassroots solidarity systems, which we could call ‘commonfare’, and the neoliberalisation and bureaucratisation of the welfare systems may well call for a re-commonification of welfare systems, based on [[public-commons partnerships]].” 
> 
> &#x2013;  [The History and Evolution of the Commons - Commons Transition](https://commonstransition.org/history-evolution-commons/) 

