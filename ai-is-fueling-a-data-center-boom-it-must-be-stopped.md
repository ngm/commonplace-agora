# AI is fueling a data center boom. It must be stopped.

An
: [[article]]

Found at
: https://disconnect.blog/ai-is-fueling-a-data-center-boom/

Nice overview of the problems with our ever expanding need for [[data centres]] (particularly for [[AI]] purposes) and the corresponding environmental impact.

Claim: [[AI is fuelling a data centre boom]].

