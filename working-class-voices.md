# Working Class Voices

URL
: https://www.gndmedia.co.uk/working-class-voices


## Series 1


## Series 2

Very thought-provoking series.  Discussion with people who identify as working class and are involved in the climate movement.  Plenty thoughts on [[working class and green politics]].

A point Ads makes a few times throughout the series: the working class are the biggest class, so vital they are part of the environment movement in order to effect change.

Also, working class have the most direct experience of state oppression and reaction, so better placed to defened against these things than an inexperienced middle-class.

