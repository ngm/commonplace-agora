# Shackles of Digital Freedom

Really good review of [[Goodbye iSlave.]]  [[iSlavery]] is Jack Qiu's framing of the manufacture and demand for modern devices as akin to a modern international slave trade.  With exploitation in the material manufacture of these devices, as well as exploitation in the deliberate addiction of people to these devices to drive their sales.

The review highly rates it for an unflinching look at the exploitation rife in the manufacture of modern devices.  With caveats on the problems of framing these modern practices as slavery, and some of the modes of resistance to it falling under the brackets of ethical consumerism, and perhaps an uncritical assumption that all technology can be liberatory if harnessed right.

-   http://www.boundary2.org/2018/03/zachary-loeb-shackles-of-digital-freedom-review-of-qiu-goodbye-islave/

