# marginalism

> This theory argues that value is established on the one hand by individuals maximizing the amount of “utility” (or benefit) that they get from purchasing a good, and on the other hand, a corporation maximizing the amount of profit they can achieve through producing and selling those goods. Where these two values meet determines the cost of a product
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The marginal cost then is the cost of producing one more unit of a commodity
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Marx and Engels dismissed marginal utility as just a fancy way of saying that value is determined by supply and demand (prices go up when demand increases and go down when supply increases
> 
> &#x2013; [[A People's Guide to Capitalism]]

