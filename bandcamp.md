# Bandcamp

> Over the last 7 years, Spotify has earned me 4.3% of what Bandcamp has. Over the last 3 months it's 1.6%.
> Getting playlisted by the Spotify lords and heard by 50,000 people is worth as much as ~20 people deciding to pay a fiver for a release on Bandcamp.
> 
> [@datassette](https://twitter.com/datassette/status/1280417867679313920)

<!--quoteend-->

> But Bandcamp isn’t the answer. Moving all music from Spotify to Bandcamp is no more of a solution than moving all of TikTok’s content from ByteDance’s data centers to Oracle’s cloud. The Bandcamp model might offer artists a greater degree of control and revenue share, but Vine has shown that there’s too much to be lost by investing so much in a single platform. Not only are monolithic platforms too much of a single point of failure, but issues of scale mean that those who run them will inevitably end up out of touch with those who create content for them, making that failure ever more likely.
> 
> &#x2013; [Platforms, Creative Communities, and the Need for a Radical Reimagining](https://thereboot.com/platforms-creative-communities-and-the-need-for-a-radical-reimagining/) 

