# 2020-08-28



## Haza Autonomy?

Couple of IndieWeb links - a nice article by [[Ana]] on the [[IndieWeb]] as a space for online [[autonomy]]. [Autonomy Online: A Case For The IndieWeb](https://www.smashingmagazine.com/2020/08/autonomy-online-indieweb/)

It is on Smashing Magazine, so aimed at web developers.  For hosted IndieWeb services, I noticed that [[Malcolm]] has added a way to use Haza.Website without needing to register a domain straight away - https://no.haza.website/.


## The Social Dilemma

A trailer for the upcoming documentary '[[The Social Dilemma]]' from the [[Center for Humane Technology]]:
https://invidious.snopyta.org/watch?v=uaaC57tcci0

I don't know much about CHT. I listened to a few episodes from Your Undivided Attention a while back and they were very interesting, though I have a vague memory of thinking they seemed to skirt around to avoid the C word (capitalism).


## Read: [Incremental progress](https://beesbuzz.biz/blog/3876-Incremental-progress)

> Diversity is absolutely a problem in tech, but IndieWeb folks are, from my experience, absolutely doing what they can to rectify that; bringing in people from all sorts of backgrounds, trying to boost the minority voices, and being supportive of everyone who is trying to make the world, or at least the Internet, a better place. 

This is a really good article by Fluffy on the state of the [[IndieWeb]] and making it more accessible for wider adoption.  Just because we're not there yet, doesn't mean that we're not trying.

https://beesbuzz.biz/blog/3876-Incremental-progress


## Listened:  [Microcast #081 - Anarchy, Federation, and the IndieWeb](https://thoughtshrapnel.com/2020/01/01/microcast-81/)

Related: this is a really fun listen from Doug Belshaw.  He discusses [[IndieWeb]] and the issues he sees with it.

Doug has a preference for the [[Fediverse]] as an approach to an open web, and says the political philosophy of the IndieWeb is a type of right-libertarianism, because it lacks social equality, and without that it is just a focus on individual freedom.

My gut response is that I disagree of course.  But it's a great jumping off point for some thought and reflection&#x2026;

https://thoughtshrapnel.com/2020/01/01/microcast-81/

