# Big win for right to repair with new EU rules for batteries - but legislators must get the implementation right

URL
: https://repair.eu/news/big-win-for-right-to-repair-with-new-eu-rules-for-batteries-but-legislators-must-get-the-implementation-right/

> In an important development for the right to repair, the European Parliament just approved a new battery regulation. The progress made on **battery removability and availability** is a step in the right direction, although the **affordability of repair** must still be addressed – in both the battery regulation and the European Commission’s linked “Right to Repair” proposal.

<!--quoteend-->

> In a big success for the right to repair, all new portable devices and light means of transport put on the market will now have to be **designed with replaceable batteries**. In many cases **users will be able to replace them themselves**. Manufacturers will also have to make batteries **available as spare parts for 5 years** after placing the last unit of a model on the market. The text states that **spare batteries must be sold at a reasonable and non-discriminatory price**, and we will keep a eye on OEMs to make sure that this is actually implemented

<!--quoteend-->

> We also celebrate that manufacturers will **no longer be able to use the unfair practice of [[part-pairing]] in batteries**, which they use to attempt to control what spare parts should be used for repairs.

