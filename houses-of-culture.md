# Houses of Culture

> A new type of leisure facility built by the Soviet government, Houses of Culture (or ‘clubs’) were based on the already existing system of People’s Houses established in Imperial Russia to distract workers from drinking. Installed into the structure of the city, Houses of Culture allowed every district to have its own ‘cultural leisure’ place to meet — in the absence of drinking facilities, such as pubs in the UK
> 
> &#x2013; [[Tribune Winter 2022]]

