# Setting up Doom Emacs in termux on Android



## Setting up Termux and Doom


### Termux

I installed [[Termux]] from [[F-Droid]].

I ran `termux-setup-storage` to get access to filesystem.  ( see https://wiki.termux.com/wiki/Termux-setup-storage)


### Doom

I tried this first: https://github.com/fuxialexander/doom-termacs, which suggests you need some special config for Doom in Termux.

But ended up with the error described here: https://github.com/fuxialexander/doom-termacs/issues/1

So then I followed a couple of the steps in: https://stevebeadleart.com/posts/2023-07-23-how-to-install-doom-emacs-on-termux/

```shell
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install
```

And that all worked just fine.

(Actually I did have an issue the first time around, where my internet connection on the phone disappeared halfway through `doom install`, which resulted in an install that seemed to complete, but actually was borked.  I removed .emacs.d and .doom.d and starting again, and it worked the second time.)

And it works great! So I don't think you need any Termux-specific config, at least not to get it up and running.  Maybe some tweaks after the fact are needed.

I'm having to get used to Doom (I'm familiar with spacemacs), but all seems good.


## Adding org-roam

Now I'm trying to add [[org-roam]] into the mix.


### Adding org-roam to Doom

Initially I added it into packages.el with:

```emacs-lisp
(package! org-roam)
```

https://github.com/doomemacs/doomemacs/blob/master/docs/getting_started.org#installing-packages

But that turns out not the best way - Doom has a config option for enabling org-roam that it seems better to go with.

You can turn that on by amending the relevant line in `init.el` from

```emacs-lisp
org
```

to

```emacs-lisp
(org +roam2)
```


### Running org-roam

Alright.  Now, first time I navigate an org-roam link I get 'Initializing org-roam database&#x2026;'


#### Processing modified files

Then a bunch of errors. Then 'Processing modified files&#x2026; X%', where X is really, really slowly creeping up.  I  guess I have so many files in org-roam (over 6000) that this will take a long time.

And&#x2026; it seems to keep crashing Termux.  Maybe it times out?

So my workaround was: do it in batches of files at a time. Moving most files to another folder temporarily, doing `org-roam-db-sync` on a subset of files in the org-roam folder, and then moving back batches of files at a time to the org-roam folder to process with org-roam-db-sync again.


#### Issue: no node titles displaying in helm

The template for nodes displaying in helm wasn't working - I couldn't see any of the titles.  So I changed the value of `org-roam-node-display-template` back to default, as the one set Doom has an issue it seems.


#### Issue: can't create new org-roam nodes

Next up: encountered this error when trying to create a new node:

> org-capture: Capture template ‘d’: Wrong type argument: markerp, 77

Same seems to be occurring for others here: https://github.com/org-roam/org-roam/issues/2376

They suggest disabling `(spell +flyspell)`.  But I still get the error (someone else in that github thread says the same, too).

OK, possibly relevant messages:

> File 20231104081451-boox<sub>handwriting</sub><sub>recognition</sub><sub>missing</sub><sub>whole</sub><sub>words</sub><sub>out.org</sub> added to project _storage/emulated/0/commonplace_ cache.
> [yas] Prepared just-in-time loading of snippets successfully.
> Error during redisplay: (doom-run-switch-buffer-hooks-h #&lt;frame F1 0xb4000079d1d9a688&gt;) signaled (wrong-number-of-arguments (0 . 0) 1)
> [yas] Committing snippets. Action would destroy a protection overlay. [2 times]
> org-capture: Capture template ‘d’: Wrong type argument: markerp, 77

So maybe something yasnippets related, maybe something overlay related, maybe something related to Doom internals.

Tried running without yasnippets loaded (by commenting `snippets` out in `init.el` to see what happens&#x2026;

And still happens with the same yasnippets errors. (Which seems kind of weird if I've disabled snippets?)

Try running it with `debug-on-error` enabled.

> Debugger entered&#x2013;Lisp error: (error "Capture template ‘d’: Wrong type argument: markerp&#x2026;")
>   signal(error ("Capture template ‘d’: Wrong type argument: markerp&#x2026;"))
>   error("Capture template \`%s': %s" "d" "Wrong type argument: markerp, 77")
>   org-capture(nil "d")
>   org-roam-capture-(:node #s(org-roam-node :file nil :file-title nil :file-hash nil :file-atime nil :file-mtime nil :id "81127d01-6f8b-4ea9-8c23-25c92d17ea02" :level nil :point nil :todo nil :priority nil :scheduled nil :deadline nil :title "Boox handwriting recognition missing whole words o&#x2026;" :properties nil :olp nil :tags nil :aliases nil :refs nil) :info nil :templates nil :props (:region (#&lt;marker at 91 in 2023-11-04.org&gt; . #&lt;marker at 143 in 2023-11-04.org&gt;) :link-description "Boox handwriting recognition missing whole words o&#x2026;" :finalize insert-link :call-location #&lt;marker at 143 in 2023-11-04.org&gt;))
>   org-roam-node-insert()
>   funcall-interactively(org-roam-node-insert)
>   call-interactively(org-roam-node-insert nil nil)
>   command-execute(org-roam-node-insert)

Not much more helpful in that trace, really.

Alright, played with disabling a bunch of other possibly relevant modules via Doom's config, and that seems to have made some progress.

Based on the ediff between ~/.doom.d/init.el and ~/.emacs.d/templates/init.example.el, I've commented out:

-   `(popup +defaults)` (because it mentions something about overlays)
-   `file-templates` (because it mentions something about snippets)
-   `snippets`
-   `(spell +flyspell)` is already commented out in the init.example.el template

So yeah.  Still don't know exactly what was causing it. Right now I'm just happy it's working and want to get on with my notes, so not going to delve further as to which one of those actually made the difference, and exactly why.


## Things to do

-   the org-roam capture template uses the slug with date prefixes and underscores - I'd like to avoid that

