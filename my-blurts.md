# my blurts

Don't assume that anything on this page is accurate - it's a space for learning. See [[blurting]] and [[microblurting]].


## <span class="timestamp-wrapper"><span class="timestamp">[2024-12-03 Tue]</span></span>


### [[colonialism]]


#### Textblurt

Colonialism is the exploitation of a country by another. Usually involving the appropriation of resources and exploitation of people.

Historically colonialism often physical taking of land and people, whereas in modern times it is often solely economic.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-12 Tue]</span></span>


### [[System]]


#### Visblurt

![[blurts/images/System Blurt 2024-11-12.png]]


#### Texblurt

A system is a set of interconnected elements that exhibits some function or purpose.

The system will be composed of various stocks, flows, feedback loops and delays that combine to produce the system's dynamics or behaviour.

Examples of systems range from bathtubs being filled with water, to heating systems and thermostats, all the way to economies, climate systems and human physiological systems.

Notes: say why systems are of interest or why it's important to be able to understand them. Could elaborate elaborate definitions and components a bit more.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-10 Sun]</span></span>


### [[Ecology]]


#### Visual blurt

![[images/ecology-blurt-2024-11-10.png]]
Forgot: interdependence and relationship focus; biotic and abiotic.


#### Visual blurt

![[blurts/images/Socialism Blurt 2024-11-10.png]]
Notes: pretty good.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-09 Sat]</span></span>


### [[digital ecosocialism]]


#### Visual blurt

![[images/digital-ecosocialism-blurt-2024-11-09.png]]

-   forgot: transition to ecosocialist society; the type of action (resist, regulate, recode); principles: degrowth - socially useful / ecologically safe production; adversarial interoperability; innovation from below.  Initiatives: libre software; open hardware / right to repair.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-04 Mon]</span></span>


### [[Ecology]]


#### Blurt

Ecology is the study of organisms and their interactions with other organisms and the environment.

It focuses on the interdependence and relationships between things, including both living (biotic) and nonliving (abiotic) factors.

Ecology looks at different  levels of organisation - from the individual to the population to the community to the ecosystem level, all the way up to the entire biosphere.

Applied ecology is used for environmental planning and management.

Notes: I remembered applied ecology but forgot about processes and patterns.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-03 Sun]</span></span>


### [[digital ecosocialism]]


#### Blurt

Digital ecosocialism is the construction and application of digital technologies to support the ideals of ecosocialism and the transition to an ecosocialist society.

Examples are: libre software; data commons; open hardware and the right to repair; platform cooperatives.


### [[Ecosocialism]]


#### Blurt

Ecosocialism is a set of political, economic and environmental ideas for how to run society in a way that is socially just and environmentally sustainable.

Ecosocialism combines ideas from socialism and environmental activism, critiquing capitalism's impact of society and nature.

Its main goals are an equitable distribution of wealth and resources, provision of social welfare and democratic governance, while protecting the planet and keeping within planetary boundaries.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-11-02 Sat]</span></span>


### [[Socialism]]


#### Blurt

Socialism is a political philosophy with the main goals of equity of resources and shared ownership of the means of production.


#### Blurt

Socialism is a political and economic philosophy for how to run society.

Socialism's primary goals are for a fair and equitable society, where wealth is distributed equitably and there is social ownership of the means of production.

There are different flavours of socialism - e.g. democratic socialism and revolutionary socialism.


#### Blurt

Socialism is a set of political and economic ideas for how to run society in a fair way.

Socialism's primary goals are an equitable distribution of wealth and resources, and for social rather than private ownership of the means of production.

There are different variations of socialism in practice, for example democratic vs revolutionary, utopian vs scientific. But common elements are usually some form of economic planning, worker self-management and provision of social welfare.


### [[Ecosocialism]]


#### Blurt

Ecosocialism is a set of political and economic ideas with a focus on social equity and safe environmental stewardship.

It combines socialist and environmental politics and economics. It's main aims are social equity and democratic governance while remaining within planetary boundaries.


## <span class="timestamp-wrapper"><span class="timestamp">[2024-10-31 Thu]</span></span>


### [[Ecology]]


#### Blurt

Ecology is the study of how organisms interact with each other and with their environment.

It focuses on the interdependence of things.


#### Blurt

Ecology is the study of organisms and how they interact with each other and their environment.

It focuses on the interdependence of things, including those that are alive (biotic) and those that aren't (abiotic).

There are different levels of organisation studied in ecology - from the individual to the population to the community to the ecosystem to the biosphere.

Ecology also looks at the processes and patterns that occur within and between ecosystems, such as flows of energy and materials.


### [[Mass extinctions]]

There have been five major extinctions thus far.

Each time wiping out over 80 percent of species.

Between tens and hundreds of million years between each one on average.

The most recent, the fifth extinction, being the one that did for the dinosaurs.

