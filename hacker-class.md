# Hacker class

> You make the information, but like some kind of info-prole, you don't own the information you produce or the means of realizing its value.
> 
> &#x2013; [[Capital is Dead]]

[[McKenzie Wark]] has said (in an interview somewhere, will need to refind), that she isnt 100% happy with that terminology and how it's aged (came up with it in 2003 or so).  These days it could be misinterpreted as a predominantly white male pursuit.  I feel that, my immediately reaction now is how it evokes like a club of privileged white male [[Free software]] enthusiasts or something, which is certainly not what Wark intended.  Shame.  I recall somewhere else she said she liked glitch from [[Glitch Feminism]] as a term.

