# The Stigmergic Revolution

URL
: https://c4ss.org/content/8914

Author
: [[Kevin Carson]]

Publisher
: [[Center for a Stateless Society]]

[[Stigmergy]].  [[Revolution]].

I always like political metaphors to [[Evolutionary and adaptive systems]] / [[complex systems]] stuff.  (That said: similar to [[Fractals]], they can be fairly easily shoehorned in to topics.)

Fascinating / sad seeing the level of hope (hubris?) around [[Occupy]] at the time it was happening. We know now that it didn't bring revolution long term, which is always one of the main criticisms levelled at it.

Perhaps parallels could be drawn how stigmergy (in nature) is not always enough. How some elements of the vertical might be required.

What would the vertical be for (natural) stigmergy?

Surprisingly I have still never read [[The Cathedral and the Bazaar]].  Probably should, despite issues with Eric Raymond.

