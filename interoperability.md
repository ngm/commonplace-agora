# Interoperability

Technical mechanism for computing systems to work together &#x2013; even if they are from competing firms.

[[Taxonomy of interoperability]].

> Interoperability lowers switching costs. Interoperability allows us, the users of technology, to set the terms on which we use that technology. It allows us to use the parts of products and services that benefit us, and block the  that don’
> 
> &#x2013; [[The Internet Con]]

