# Karl Kautsky

> While many people’s first encounter with Kautsky is through Lenin’s polemical denunciations of him, Kautsky had enormous stature among Marxists, including Lenin himself, before the onset of the First World War.
> 
> &#x2013; [[Breaking Things at Work]]

