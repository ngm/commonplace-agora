# UK recession 2022

> The Bank now thinks that the UK will enter a [[recession]] in October that will last until the end of 2023, the longest since the [[2008 financial crisis]]. It expects inflation to peak above 13% later this year, the highest rate since 1980, and to stay high for much of next year. And it says unemployment is likely to climb to 6.3% by 2025, from 3.7% today.
> 
> &#x2013; [Friday briefing: What the interest rate spike means for the country – and for&#x2026;](https://www.theguardian.com/world/2022/aug/05/friday-briefing-interest-rates-bank-of-england)

