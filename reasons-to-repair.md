# Reasons to repair

> We live in an era of mass overproduction. Offices, apartments, cars, ships, aeroplanes, mobile phones, laptops, batteries, televisions, furniture, air fryers, hot tubs, elevators and escalators. A countless multitude of objects that belong to the anthroposphere – a term for everything that people have made and how it all interacts with the planet.
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> Many of these products end up as waste, buried in landfill, incinerated or dumped – with catastrophic environmental consequences. At the same time, mining companies continue to pollute the planet, exploit local communities and produce huge CO2 emissions in the drive to make more products.
> 
> &#x2013; [[Can urban mining help to save the planet?]]

[[Worker exploitation in electronics manufacturing]].

