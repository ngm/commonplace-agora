# Beyond the horizontal and the vertical

Status
: seedling

**An overview of some of my recent learnings on the theory and practice of political organisation.**

To prevent catastrophic climate change, we have a clear picture of what needs to be done.  The [[IPCC Sixth Assessment Report]] lays it out clearly.  So how do we make it happen?

A really rich vein of learning for me lately has been on the topic of [[horizontalism]] and [[verticalism]].  On the left, these are two long contested modes of [[political organisation]].  But their polarisation is damaging.  We are living in a time when radical action desperately needs to be enacted.  Climate is breaking down, wars are breaking out, inequality is increasing.  A pragmatic synthesis is needed to address these monumental challenges.  How can we get huge, serious things done, and how can we make sure that they stay done?  On a personal note, where should I focus my energy?

-&#x2014;


## Horizontal and the vertical

Firstly, some quick definitions:

> **The horizontal**: think spontaneity; self-organisation; distribution; networks.  Perhaps think [[anarchism]].
> 
> **The vertical**: think structure; hierarchy; concentration; the party form.  Perhaps think [[Communism]].


## A potted history

Something of the tension between horizontal and vertical goes all the way back to Marx and Bakunin at the [[First International]], and probably goes back further.  You can find it alive and well in many arguments on the Internet in the present day.  I stumbled on the topic in the terms of horizontal and vertical when reading [[Inventing the Future]] back in 2017.   For a long while, the topic seemed to arise only in the frame of horizontalism **versus** verticalism.  For example, Inventing the Future advocates verticalism and lays into horizontalism. Kevin Carson vehemently [rebutts](https://c4ss.org/content/50849) the straw man of "[[folk politics]]".  

As a dilettante with little practical experience of organisation proper, I could easily be persuaded by arguments from either side.  I often felt that they both kind of made sense.  While I have an affinity for the non-hierarchical, a completely spontaneous and dislocated network seems doomed to a short lifespan.  And though I recognise the need for structure, a totalising party form is demonstrably prone to corruption.  

But even more evident is that slanging matches don't really help anyone except the reactionaries.  So, thankfully, lately more and more I have been coming across things that entail a [[synthesis of horizontalism and verticalism]], something that is in fact [[neither vertical nor horizontal]].  This synthesis, an [[organisational ecology]], makes strong, intuitive sense.  You actually see the idea of a synthesis pop up regularly in theories and frameworks and action throught history.  The extreme polarisation has been challenged many times.

I'll try and summarise some of what I've come across.  This is a drop in the ocean to what's out there - but it's what is helping me personally navigate all this.


## An ecology

The latest and greatest book to tackle this issue is [[Rodrigo Nunes]]' [[Neither Vertical Nor Horizontal]].  It addresses head on the need for something more nuanced than an either/or, and presents the idea of an organisational ecology.

> **The organisational ecology**: think a mixture of the both horizontal and vertical, at different times, at different scales, and in different contexts, whichever is the most appropriate.

Whichever has the [[requisite variety]], you might say.


## Frameworks

Nunes makes the strong case for an ecological approach.  Some frameworks that adopt this approach: 

-   [[Heterarchy]].  As outlined in [[Free, Fair and Alive]] as an organisational principle of [[commoning]].  It is a very similar ecological approach to organisation, combining top-down hierarchies, bottom-up participation, and peer-to-peer dynamics.
-   [[Viable system model]]. An organisational framework proposed by Stafford Beer in his work on [[cybernetics]].  I first hit it in [[Cybernetic Revolutionaries]], which brilliantly details [[Project Cybersyn]], an attempt at the VSM in practice.  The VSM spans scales and structures.  Recently the VSM appeared again in [[Anarchist Cybernetics]], a fantastic outline for bridging the horizontal and the vertical starting from an anarchist perspective.
-   [[Institutional Analysis and Development]].  [[Elinor Ostrom]]'s [[polycentric]] view of institutional design, also commons-based.
-   [[Communalism]].  Detailed in the [[The Next Revolution]], this is [[Murray Bookchin]]'s line between anarchism and Marxism.
-   [[Autonomism]]?
-   P2P


## Practice

In addition to the theory and the frameworks, the practice.

Real, existing examples of multi-level, polycentric, viable, ecologies of organisation.

-   [[Cooperation Jackson]].  Their book, [[Jackson Rising]], is a handbook of activist tactics in pretty adversarial circumstances in Jackson, Mississippi.  It is eco-socialism in practice, made up worker self-organisation, democratic ownership, cooperatives and mutual aid.  [[People's Assembly]] feature heavily, but a [[dual power]]/[[counterpower]] approach acknowledges the need for engagement with the state and electoral politics.  It is multi-level, acknowledging the need to go beyond the hyperlocal to the municipal and the regional.
-   [[Rojava]].  [[Abdullah Öcalan]]'s [[democratic confederalism]] (in part inspired by [[Communalism]]) put in to practice in the midst of conflict in Syria.
-   [[Socialist Chile]] under Allende.  While at first blush it could look like a centralised [[planned economy]], when you read the history you realise how much both Beer and Allende wanted something that spanned social strata and organisational structures.
-   things from [[Free, Fair and Alive]]


## Where next?

So there is the theory, there are frameworks, and there are examples of struggles on the ground making use of an organisational ecology.

I've not got to the end of [[Neither Vertical Nor Horizontal]] yet, but mentioned early on is the immediate need for both the horizontal and the vertical to be employed to avert climate breakdown.  I feel like the consistant dissemination and application of the ideas from the practical examples that are avowedly focused on climate is a good way to go.

