# information and communication technology

Transmission, storage, processing of information.

Things like computers, the internet, smartphones, and digital platforms.

In modern society it's a big part of social and economic development.


## Key areas

-   computer hardware, computer software, networking technologies, data management, information security, and HC


## Importance

-   [[ICT is a fundamental part of modern society]]
-   [[ICTs have a significant role in the means of production]]

