# A blast in Manhattan

A
: [[podcast]]

Part of
: [[The Santiago Boys]]

Featuring
: [[Evgeny Morozov]]

First episode of [[The Santiago Boys]].

Really well made.

This first episode covers a lot of the geopolitics and general shittery of the [[CIA]] and corporations in South America, and in particular their intervention in [[Salvador Allende]]'s government in [[Socialist Chile]].

