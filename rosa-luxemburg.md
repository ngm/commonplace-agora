# Rosa Luxemburg



## Overview

-   [[Revolutionary socialist]] thinker
-   Part of the [[German Revolution]]
-   Marxist critiques
    -   what are the limits to capitalist expansion
    -   expanded reproduction
    -   surplus value turning into active capital - disagrees that this is endless
    -   there are more than just workers and capitalists in a vacuum
    -   someone has to consume the leftover surplus value - who is it?
        -   social strata or societies that do not engage in capitalist production
-   "[[Socialism or Barbarism]]"
-   Capitalism containing inner contraditions that are seeds of its own self-destruction
    -   Luxemburg makes many accurate predictions off the back of this
    -   metabolism happens exponentially across the world stage
-   Betrayed by the social democratic left
-   Murdered

> Rosa Luxemburg practiced a class struggle social democracy that struggled for radical reforms, and combined mass strikes and parliamentary action. Luxemburg practiced dialectics of party/movements, organisation/spontaneity, intellectual leadership/masses.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

Sounds like much to say on [[Beyond the horizontal and the vertical]].


## Anti-imperialism

-   imperialism: political expression of process of accumulation of capitalism in competitive struggle over unspoiled remains of the non-capitalist world environment
    -   an international expression of capitalism
-   colonialism is a form of imperialism
-   capitalism drives imperialism and colonialism
    -   it's the process of extracting commodities, resources and labour power from primitive social relations and absorbing it in to the capitalist wage system
    -   defile natural economy
    -   so many examples of capitalist-fueled imperialism
    -   the accumulation of capital knows only violence

See [[Anti-imperialism]].


## Internationalism

-   internationalist view of political and economic frameworks of Marxism


## Works

-   The Accumulation of Capital
-   The Mass Strike
-   Reform or Revolution
    -   rejecting Bernstein
    -   overtly reject capitalism through socialist revolution
    -   will be ignited through growing class consciousness
    -   believe in party structure
        -   democratic in how she views that transition
        -   believe in councils
        -   but have to actively and democratically involve majority of oppressed of working class
        -


## Views on vanguardism

> Her longing persists: a vanguard firm enough to gain power yet supple enough to wield it humanely.
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]


## Misc

Her first loves were zoology and botany. ([[Red Rosa]])


## Resources

-   [Red Rosa: the graphic biography of Rosa Luxemburg](https://www.versobooks.com/books/2036-red-rosa)
-   [Rev Left Radio: Red Rosa: The Life and Legacy of Rosa Luxemburg](https://revolutionaryleftradio.libsyn.com/k)
-   [Revolutionary Left Radio: Red Rosa: The Political and Economic Theories of Rosa Luxemburg](https://revolutionaryleftradio.libsyn.com/s)

