# Patterns

> patterns resemble DNA, a set of instructions that are underspecified so that they can be adapted to local circumstances.  "Does the DNA contain a full description of the organism to which it will give rise?" asks [[Christopher Alexander]]
> 
> &#x2013; [[Free, Fair and Alive]]

