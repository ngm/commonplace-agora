# The Ecology of Freedom

> In his major work, The Ecology of Freedom (1982), he elaborated the historical, anthropological, and social roots of hierarchy and domination and their implications for our relationship to the natural world in an expansive theory that he called “[[social ecology]].”
> 
> &#x2013; [[The Next Revolution]]

