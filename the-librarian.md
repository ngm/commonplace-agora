# The Librarian

From [[Snow Crash]].  A [[daemon[[https://www.researchgate.net/publication/251011743<sub>Agent</sub><sub>of</sub><sub>Civility</sub><sub>The</sub><sub>Librarian</sub><sub>in</sub><sub>Neal</sub><sub>Stephenson</sub>'s<sub>Snow</sub><sub>Crash</sub>]]]] of sorts?

> In Neal Stephenson's 1992 cyberpunk novel Snow Crash, hacker Hiro Protagonist, aided by a virtual Librarian, works to save the world from a cybernetic and biological virus that will enslave the information elite (hackers and programmers). The text proposes questions for an information culture like ours facing technological and informational calamity. What will become of the gap between the information rich and poor? What is the future of public access to information? Can librarians in the digital library (cybrarians) be successful educators? If librarians embrace technology, will it be at the expense of humanism? Can the cybrarian continue to act as an agent of culture and enlightenment, as in the past? This article examines the external world that makes living in an internal virtual world necessary, then explores the issue of whether the Librarian is a harmful or beneficial creation, and finally considers the Librarian as a force for civility.
> 
> &#x2013; https://www.researchgate.net/publication/251011743_Agent_of_Civility_The_Librarian_in_Neal_Stephenson's_Snow_Crash

