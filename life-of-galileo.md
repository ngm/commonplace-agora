# Life of Galileo

The play tells the story of [[Galileo]] as he faces opprobrium from the church, for his inconveniently heretical observation that the Earth is not in fact the centre of the universe.

It's a story of science vs religion, change vs status quo, goals vs family, poor vs wealthy, truth vs dogma.

We see Galileo doggedly pursue his arguments in the face of adversity.  By no means a perfect man, he neverless has great strength and courage of conviction in the importance of observational truth over blind faith.  This brings him under the harsh glare of the Catholic church, whom are served very well by the current view of the world and do not appreciate his attempts at change.

For me personally, the relevance of the themes to the modern day were more political than scientific. With minor fluctuations, science is no longer frowned upon, and it is religion that must keep apace with progress. Galileo's story of fighting against an entrenched system felt allegorical to the modern day struggles in pursuing an alternative to capitalism and [[neoliberalism]]. So much a mass hallucination that "it's easier to imagine the end of the world than the end of capitalism" http://k-punk.abstractdynamics.org/archives/005341.html , and with mighty vested interests wanting to keep it that way, there are many parallels to the 17th century's cabal of high priests insisting (with pointed implements) that it is the Sun that moves around the Earth.   Yet strong-arming is the weaker tactic, and in the play the more insidiuous form of allowing tinkering within the system is also alluded to.  As neoliberalism allows manageable treatises and containable protests, the church allows the illusion of science, as long as it doesn't get in the way of God.  Galileo plays the system and sows the seeds to bring it down, but not without inviting tragedy to himself and those around him.

Overall it speaks to the importance of doing that which seems demonstrably right, and suffering the consequences as much you can.

Despite the presence of these weighty themes, the play stays light, entertaining, and thoroughly engaging, with many a laugh out loud moment. The acting and staging are both superb. The play is not only **in the round**, affording every punter an equal view, but some seats are also **in the play**, adding to the fun and immersion. Brendan Sewell gives a real powerhouse performance, full of energy and gravitas and also wit, with all of the cast superb. Throw in beautiful lighting, puppetry and a planetarium style projection of the heavens, and you stay enraptured for the almost 3 hour long performance.

