# Electronics Watch

-   [Electronics Watch](https://electronicswatch.org/en/)

Organisation pushing for responsible [[public procurement]] and the [[Rights of electronics workers]].

-   [Restart Podcast Ep. 60: Helping electronics workers improve conditions](https://therestartproject.org/podcast/electronics-watch/)

[[EPEAT]].

