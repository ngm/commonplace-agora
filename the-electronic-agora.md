# the electronic agora

> The vision of a citizen-designed, citizen-controlled worldwide communications network is a version of technological utopianism that could be called the vision of "the electronic agora." In the original democracy, Athens, the agora was the marketplace, and more&#x2013;it was where citizens met to talk, gossip, argue, size each other up, find the weak spots in political ideas by debating about them. 
> 
> &#x2013; [[The Virtual Community]]

<!--quoteend-->

> Back in the 60s, [[Marshall McLuhan]] preached that the power of big business and big government would be overthrown by the intrinsically empowering effects of new technology on individuals. The convergence of media, computing and telecommunications would inevitably result in an electronic [[direct democracy]] - the electronic agora - in which everyone would be able to express their opinions without fear of censorship. 
> 
> &#x2013; [[The Californian Ideology]]

<!--quoteend-->

> Community activists will increasingly use [[hypermedia]] to replace corporate capitalism and big government with a hi-tech '[[gift economy]]' in which information is freely exchanged between participants. In Rheingold's view, the '[[virtual class]]' is still in the forefront of the struggle for social liberation. Despite the frenzied commercial and political involvement in building the '[[information superhighway]]', direct democracy within the electronic agora will inevitably triumph over its corporate and bureaucratic enemies.
> 
> -   [[The Californian Ideology]]

See also: [[Agora]].

