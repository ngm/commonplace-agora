# Wikis I like

See also [[My garden circles]].

-   [[Phil Jones]]'s [ThoughtStorms](http://thoughtstorms.info/view/RecentChanges).  Phil has been doing it for a long time. It has an old school look and I really like that.  In terms of content, it's precisely what I want to read in someone else's wiki - a hot mix of quick thoughts and long-evolved ideas to stumble through.  It's got wiki [[personality]].
-   [Bill Seitz](http://webseitz.fluxent.com/wiki/FrontPage).  Bill's is chock full of history and content too.  It has a [[bliki]] feel (in fact, Bill calls it a WikiLog) - it has a [[stream]] of recent changes right on the landing page as an entry point to the rest of what's there.
-   [Ton's wiki](https://www.zylstra.org/blog/wiki-frontpage/).  Ton's has a very strong link to his blog (the wiki is embedded in it and mainly an index of posts from the blog).  I like to see [[blogs and wikis]] intertwingled.  You could argue that Ton's whole site is a wiki or digital garden of sorts - most 'blog' posts will link to previous thoughts on the topic.
-   [h0p3's wiki](https://philosopher.life/#Root:Root%20%5B%5BLegal%20Notice%5D%5D). I have not really delved deeply into h0p3's wiki, but there are a friend of Kicks.   Their wiki is some between '[[wiki as personality]]' and wiki as performance art.
-   [Emsenn's Digital Garden](https://emsenn.github.io/index.html).  Their site has gone through a few incarnations, and the latest is a digital garden created and published via org-roam.  Lots of interesting stories to be found when taking a [[drift]] through their rhizomatous writing.
-   [[Nadia Eghbal]] . 'Learning in public' - I like that sentiment.  Not really a wiki (perhaps I should remove from here&#x2026;).  I also like how Nadia's writing combines cultural references with other ideas.  I definitely want to make those same kind of links in my wiki / blog.
-   [[Nick Sellen]]'s [ponderings](https://github.com/nicksellen/ponderings). I like Nick's ponderings as an example of someone thinking out loud to themselves, in a way of interest to others.
-   [Chris Aldrich's wiki](http://tw.boffosocko.com/).  Chris has just started up a TiddlyWiki, and you can be certain he'll do something interesting with it.
-   Anne-Laure's [Mental Nodes](https://www.mentalnodes.com/).  Nice, clean design, and the backlinks and transclusion work really well for browsing around.  Plus I love the content.
-   [gwern.net](https://gwern.net). A [long site](https://www.gwern.net/About#long-site) with [long content](https://www.gwern.net/About#long-content).
-   [Maggie Appleton's Digital Garden](https://maggieappleton.com/garden).  Full of lots of great long-form posts, often on PKM stuff.  Closer to blog/research than a rambling personal wiki though.
-   Weakty. https://weakty.com/

