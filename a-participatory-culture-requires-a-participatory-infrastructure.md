# A participatory culture requires a participatory infrastructure

> Creating a participatory culture requires thinking beyond technology and participation numbers. It entails building an infrastructure that continuously supports citizens in participating. Cities can look to open source and its decades of experience in creating this type of infrastructure to find valuable lessons.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

Though this quote above is about citizen participation in cities, I think the sentiment is equally true for participation in online communities.  [[There is a layer of governance infrastructure missing from the web]].

&#x2026;What exactly is a [[participatory infrastructure]]?

