# How I take notes

When I'm taking some notes on something, I kind of do it like this:

-   Write a few sentences on what I already think about it first, before reading anything specifically elsewhere.
-   The 'subjective' view: see what's been written on it by people I already have a mental model of.  Probably quote a bunch from these.
    -   Friends/comrades: check in Anagora, see what's been written there
    -   'Mentors': [[Free, Fair and Alive]] / https://patternlanguage.commoning.wiki / [[Lean Logic]]
    -   Counterpoints: not sure where to get these from in garden form right now, but basically people I know will probably not agree.  Posting on social media is usually a good way of getting some antithesis action.
-   The 'objective' view: Wikipedia.
    -   Maybe quote a bit just for understanding, but not too much from here.  No point trying to recreate an objective view.
-   Synthesis
    -   Actually - just leave it there if time's up for now.  That's fine.  Can come back to it.
    -   Otherwise, try and synthesise it to understand.

Thinking about this, I want certain circles/groupings of people.  e.g. for politics I'd probably go to FFA, P2P wiki, some peeps on Anagora, etc

For other stuff (let's say knowledge management), I'd probably go to [[Phil Jones]], [[Bill Seitz]], some Anagora peeps, etc

