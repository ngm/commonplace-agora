# Ecosocialism and the ecological crisis

A
: [[podcast]]

URL
: https://www.listennotes.com/podcasts/victors-children/ep-22-ecosocialism-and-the-wYZ69c7BjEK/

Good summary of a variety of points related to [[ecosocialism]]. Useful on some detail on [[degrowth]] and [[Green New Deal]] as tendencies within ecosocialism, where they may disagree and where they have overlaps.

-   00:03:30 Why ecosocialism? Why not just socialism?
-   00:14:31 Degrowth or not. Nuclear or not.
-   00:16:08 Degrowth very roughly anarchist, local based approaches. GND roughly more state based. [Really?]
-   00:18:59  They're not completely separate camps. GND is a tool. Degrowth is an orientation.
-   [[Rosa Luxemburg]], [[André Gorz]], [[non-reformist reforms]]
-   00:22:49  GND is palpable. You can see changes it is doing.
-   00:27:09  Lots of camps within degrowth. But for the speaker, degrowth is absolutely not about austerity. It's about [[buen vivir]].
-   00:29:46 Degrowth was originally an academic movement but it is broadening out.
-   Degrowth would advocate using existing technologies. (Similar to what Medina was suggesting in some sense?)
-   [[Thea Riofrancos]] on [[supply chains]]
-   ~00:49:50  Tie degrowth to a slowing of pace of life plus improvements. It's not austerity. Less flights, but invest in high speed rail. Less work, longer holidays, less need to fly.  Etc.
-   ~01:23:38  Problems with [[nuclear energy]]. Storage. Meltdowns. War. Time and budget to deploy!
-   ~01:27:06 [[solar radiation management]]: if we got hooked on it, then something stops it for some reason - rebound effect catastrophical


## Processing

