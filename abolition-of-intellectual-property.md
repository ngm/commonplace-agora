# abolition of intellectual property

See  [[We should phase out intellectual property]].

> Intellectual property, especially in the form of copyrights and patents, give corporations control over knowledge, culture and the code that determines how apps and services work, allowing them to maximize user engagement, privatize innovation and extract data and rents. Economist Dean Bakerestimates that intellectual property rents cost consumers an additional $1 trillion per year compared to what could be obtained on a “free market” without patents or copyright monopolies. Phasing out intellectual property in favor of a commons-based model of sharing knowledge would reduce prices, widen access to and enhance education for all and function as a form of wealth redistribution and reparations to the Global South.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

