# review article

> A review paper can be thought of as an extended critical essay. The introduction sets out the issue and the way in which it will be tackled, while the main body of the review is taken up by either showing how different perspectives, theories, hypotheses or pieces of work relate to a topic, or by defending or proposing a particular theory or hypothesis above others. The bulk of a review paper is usually closely argued and may appear to a non-specialist in the subject area as being entirely reasonable. The art of the scholar or critic (namely you) is to find out what the author is saying and whether what they are saying is valid or not.
> 
> &#x2013;
> https://learn2.open.ac.uk/mod/oucontent/view.php?id=2008267&section=1.3.1

<!--quoteend-->

> In essence, review articles provide other researchers in the same (and different) subject areas with a quick overview of some of the key papers, findings, arguments and conclusions that have been extracted from a range of sources. Recently published review papers can therefore offer a good introduction to primary research papers that you may want to read yourself.
> 
> &#x2013;
> https://learn2.open.ac.uk/mod/oucontent/view.php?id=2008267&section=1.3.1

