# Greater London Council

> Left-wing leaders of the Greater London Council (GLC) were committed to a radically alternative economic strategy compared to the “free-market” right-wing agenda of the Thatcher government nationally. The GLC quickly instituted a [[Greater London Enterprise Board]] (GLEB) committed to job creation, industrial democracy, and [[socially useful production]].
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The Greater London Council (GLC) between 1981 and 1986 was exemplary in showing how a municipal council could provide shared spaces for economic, social and cultural initiatives.
> 
> &#x2013; [[The Care Manifesto]]

<!--quoteend-->

> Crucially, the GLC made its larger sites more accessible, thereby extending the public commons. Hitherto, London’s vast arts complex, the [[Southbank Centre]], had been the exclusive and pricey preserve of the upper and upper middle classes, until the GLC created a new ‘open foyer’ policy in its flagship building, the [[Royal Festival Hall]]. This allowed anyone, with or without a ticket, to enter and hang out. Today it is still one of the relatively few covered public places in the British capital, besides libraries, churches and museums, where it’s possible to spend time without spending money – which makes it a haven for many, especially those with young children.
> 
> &#x2013; [[The Care Manifesto]]

