# afro-futurism

> Similarly, Afro-futurism is a trend which is very much rooted in this dialectic between deep tradition and an emancipated vision of the possibilities of progressive change.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

