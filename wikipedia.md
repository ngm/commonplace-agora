# Wikipedia

> A final example of an actually existing real utopia is the new forms of peer-to-peer collaborative production that have emerged in the digital era. Perhaps the most familiar example is Wikipedia. Within a decade of its founding, Wikipedia destroyed a three-hundred-year-old market in encyclopedias; it is now impossible to produce a commercially viable, general purpose encyclopedia.  Wikipedia is produced in a completely noncapitalist way by a few hundred thousand unpaid editors around the world contributing to the global commons and making it freely available to everybody. It is funded through a kind of gift economy that provides the necessary infrastructural resources.  Wikipedia is filled with problems — some entries are wonderful, others terrible — but it is an extraordinary example of cooperation and collaboration on a very large scale that is highly productive and organized on a noncapitalist basis.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

