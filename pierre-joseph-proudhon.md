# Pierre-Joseph Proudhon

> In fact, it was Marx’s political and intellectual rival, Pierre-Joseph Proudhon, who treated mechanization as inevitable and ultimately desirable.
> 
> &#x2013; [[Breaking Things at Work]]

