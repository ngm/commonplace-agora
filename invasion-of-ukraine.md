# Invasion of Ukraine

[[2022]] invasion of [[Ukraine]] by [[Russia]].

[[colonialism]].


## <span class="timestamp-wrapper"><span class="timestamp">[2022-04-10 Sun]</span></span>


### Intervention

> So here are some of the hard choices western leaders must urgently consider. First, direct intervention to create a safe haven in western Ukraine, where displaced people may congregate instead of fleeing abroad. Inform Moscow in advance of its location and boundaries. Be clear it will be protected by Nato air power and ground forces invited in by Kyiv.  Second, declare the unoccupied city of Odesa off-limits. Send naval forces into international waters in the Black Sea and warn Russia to cease coastal bombardments or face serious, unspecified consequences. Third, tell Putin that if his artillery and missile units fire on civilians again, as in Kramatorsk, they will be deemed legitimate Nato military targets. Fourth: supply fighter planes and tanks to Kyiv. Fifth: block all Russian fossil fuel exports.
> 
> [The Observer view on the west’s response to war in Ukraine | Observer editori&#x2026;](https://www.theguardian.com/commentisfree/2022/apr/10/observer-view-wests-response-to-fighting-ukraine-vladimir-putin)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-13 Sun]</span></span>


### Historical significance

> Russia’s invasion of Ukraine has been described by politicians and commentators as a watershed moment in modern history, a turning point comparable in importance to the [[9/11]] attacks in the US in 2001, [[the fall of the Berlin Wall]] in 1989, and even [[the assassination of John F Kennedy]] in 1963
> 
> &#x2013; [How Ukraine has become the crucible of the new world order | Ukraine | The Gu&#x2026;](https://www.theguardian.com/world/2022/mar/12/how-ukraine-has-become-the-crucible-of-the-new-world-order) 


### Repercussions

> [[Boris Johnson]], Britain’s prime minister, was on the ropes and almost out for the count in the days before the invasion, vilified for illegal Downing Street partying in breach of Covid lockdown rules. But the war, allowing him to play international statesman, has provided a new lease of political life – for now
> 
> &#x2013; [How Ukraine has become the crucible of the new world order | Ukraine | The Gu&#x2026;](https://www.theguardian.com/world/2022/mar/12/how-ukraine-has-become-the-crucible-of-the-new-world-order) 

Goddam it.

> [[China]] stands to be the big strategic winner if, as seems likely, Ukraine becomes a protracted trial of strength between Russia and the west. Its president, Xi Jinping, appears to have given Putin a green light when they met just before the invasion. Now he’s backing peace efforts. China’s economy has been hurt by rising commodity costs. But it’s a small price to pay for increased global dominance.
> 
> &#x2013; [How Ukraine has become the crucible of the new world order | Ukraine | The Gu&#x2026;](https://www.theguardian.com/world/2022/mar/12/how-ukraine-has-become-the-crucible-of-the-new-world-order) 

<!--quoteend-->

> [[Taiwan]] has been watching events in Ukraine with deep unease. The US refusal to come to Kyiv’s aid with direct military support is especially chilling, given the invasion threat the island faces from Beijing. As with Ukraine, Washington has no legal or treaty obligation to fight for Taiwan. Its position is deliberately ambiguous – and inherently unreliable. China is watching, too
> 
> &#x2013; [How Ukraine has become the crucible of the new world order | Ukraine | The Gu&#x2026;](https://www.theguardian.com/world/2022/mar/12/how-ukraine-has-become-the-crucible-of-the-new-world-order) 

<!--quoteend-->

> [[Venezuela]]’s hard-left government has been in America’s bad books for years. But when US officials visited recently to discuss resumed oil supplies in return for an easing of sanctions, they found a receptive audience
> 
> &#x2013; [How Ukraine has become the crucible of the new world order | Ukraine | The Gu&#x2026;](https://www.theguardian.com/world/2022/mar/12/how-ukraine-has-become-the-crucible-of-the-new-world-order) 


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-12 Sat]</span></span>


### Refugee crisis

> Children are going missing and cases of human trafficking are being reported by aid groups and volunteers along Ukraine’s borders amid the chaos of the refugee crisis triggered by the Russian invasion.
> 
> [Children going missing amid chaos at Ukrainian border, aid groups report | Uk&#x2026;](https://www.theguardian.com/global-development/2022/mar/12/children-going-missing-amid-chaos-at-ukraine-border-report-aid-groups-refugees)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-11 Fri]</span></span>


### [[Putin]]

> Putin is a ruthless killer. He has proved that in the early 2000s when he flattened [[Grozny]], [[Chechnya]]’s capital, killing more than 250 thousand people so as to solidify his hold over the Russian government
> 
> [What we must do in the face of Putin's criminal invasion of Ukraine - a perso&#x2026;](https://www.yanisvaroufakis.eu/2022/03/05/what-we-must-in-the-face-of-putins-criminal-invasion-of-ukraine-a-personal-view-plus-a-heartwarming-manifesto-by-russian-comrades/)


### Sanctions

> A stream of leading international companies such as Apple, Shell, Ikea and McDonald’s have pulled out of Russia; the Kremlin is threatening to retaliate by seizing their corporate assets in Russia.
> 
> [Friday briefing: Biden turns trade screws on Putin | | The Guardian](https://www.theguardian.com/world/2022/mar/11/friday-briefing-biden-turns-trade-screws-on-putin)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-10 Thu]</span></span>


### Chemical weapons

> Vladimir Putin could use chemical weapons on Kyiv as Russian propagandists spread what the US has called “false claims about alleged US biological weapons labs and chemical weapons development in Ukraine
> 
> [Thursday briefing: Fears Russia may use chemical weapons | | The Guardian](https://www.theguardian.com/world/2022/mar/10/thursday-briefing-fears-russia-may-use-chemical-weapons)


### Outcome / escalation

> It has left the west with an impossible dilemma: stand back and watch Ukrainian cities be reduced to rubble with the deaths of many more civilians inevitable. Or intervene and risk a nuclear conflagration of the kind the world has never seen.
> 
> [Could Nato do more to stop the war in Ukraine? | News | The Guardian](https://www.theguardian.com/news/audio/2022/mar/10/why-nato-cant-stop-the-war-in-ukraine-podcast)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-09 Wed]</span></span>


### Outcome

> there are really only two paths toward ending the war: one, continued escalation, potentially across the nuclear threshold; the other, a bitter peace imposed on a defeated Ukraine that will be extremely hard for the United States and many European allies to swallow
> 
> &#x2013; [I’ve studied the possible trajectories of the Russia-Ukraine war. None are go&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/08/russia-ukraine-war-possible-trajectories)

A bit of a grim prognosis.  Based on [[war games]].


### Energy war

> Vladimir Putin is able to finance his war machine thanks to EU payments for Russian gas and oil, the bloc’s foreign affairs chief, Josep Borrell, has said.
> 
> &#x2013; [Buying Russian gas and oil has funded Putin’s war, says top EU official | Eur&#x2026;](https://www.theguardian.com/world/2022/mar/09/russian-gas-oil-vladimir-putin-war-europe-eu-official)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-08 Tue]</span></span>


### Refugee crisis

> Biggest refugee crisis in Europe since second world war
> 
> [Tuesday briefing: Biggest refugee crisis in Europe since second world war | |&#x2026;](https://www.theguardian.com/world/2022/mar/08/tuesday-briefing-refugee-crisis)

<!--quoteend-->

> A million and a half people have been forced to escape their homes in Ukraine
> 
> [The Ukrainian refugees pouring into Europe | News | The Guardian](https://www.theguardian.com/news/audio/2022/mar/08/the-ukrainian-refugees-pouring-into-europe)

<!--quoteend-->

> The number of Ukrainian refugees granted visas to come to the UK under the new family scheme has risen from about 50 to 300, the government has announced, leading Labour to criticise the “shockingly low” figure.
> 
> [Tuesday briefing: Biggest refugee crisis in Europe since second world war | |&#x2026;](https://www.theguardian.com/world/2022/mar/08/tuesday-briefing-refugee-crisis)

<!--quoteend-->

> The humanitarian crisis in Ukraine is deepening as Russian forces intensify their attacks, while food, water, heat and medicine grow increasingly scarce. Some 1.7 million Ukrainians are thought to have fled the fighting and the total could reach 5 million, the EU said.
> 
> [Moscow accused of targeting civilians fleeing Ukrainian cities - as it happen&#x2026;](https://www.theguardian.com/world/live/2022/mar/07/ukraine-news-russia-war-latest-news-vladimir-putin-zelenskiy-kyiv-russian-invasion-civilian-attacks-irpin-ceasefire-live-updates?page=with:block-622692978f08527b36852b99)


### Energy war

> Moscow has stoked fears of an energy war by threatening to close Nord Stream 1, the major gas pipeline to Germany, after the US pushed its European allies to consider banning Russian oil imports
> 
> [Russia threatens Europe’s gas supplies as west mulls oil import ban over Ukra&#x2026;](https://www.theguardian.com/world/2022/mar/08/russia-threatens-europes-gas-supplies-as-west-mulls-oil-import-ban-over-ukraine-invasion)


### China

> China’s foreign minister has called the country’s relationship with Russia “iron clad” as Beijing continues to refuse to condemn the invasion of Ukraine despite growing pressure from the US and European Union to use its influence to rein in Moscow
> 
> [Russia-Ukraine war: what we know on day 13 of the Russian invasion | Ukraine &#x2026;](https://www.theguardian.com/world/2022/mar/08/russia-ukraine-war-what-we-know-on-day-13-of-the-russian-invasion?fr=operanews)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-07 Mon]</span></span>

> Russia has instructed all state-owned websites and services to switch to the Russian domain name system by 11 March [&#x2026;]
> Many fear the move is a sign that Russia is beginning active preparations for disconnection from the global internet.
> 
> [Ukraine war: what we know on day 12 of the Russian invasion | Ukraine | The G&#x2026;](https://www.theguardian.com/world/2022/mar/07/ukraine-war-russia-what-we-know-so-far-day-12-russian-invasion)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-06 Sun]</span></span>


### No-fly zone

> Nato cannot declare war on Russia, which is what a no-fly zone would amount to. This would be a reckless escalation of conflict with a nuclear power
> 
> [The Observer view on Ukraine and western support | Observer editorial | The G&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-ukraine-western-support)


### Outcome

> It may take years for Vladimir Putin to be defeated in his conquest of Ukraine
> 
> [Johnson pledges more defensive equipment for Ukraine | Ukraine | The Guardian](https://www.theguardian.com/world/2022/mar/06/ukraine-putin-dominic-raab-nato-sanctions)


### Information warfare

> Donald Trump, whose rise Moscow supported with a covert operation to undermine US democracy
> 
> [Social media turn on Putin, the past master | Carole Cadwalladr | The Guardian](https://www.theguardian.com/world/2022/mar/06/social-media-turn-on-putin-the-past-master)

<!--quoteend-->

> We knew this because it’s laid out in forensic and exhaustive detail in what may be one of the most misunderstood documents of modern times: the [[Mueller report]].
> 
> [Social media turn on Putin, the past master | Carole Cadwalladr | The Guardian](https://www.theguardian.com/world/2022/mar/06/social-media-turn-on-putin-the-past-master)

<!--quoteend-->

> he’s also being assisted by a crack squad of armchair intelligence officers. Because one of the most remarkable aspects of the war so far is how anyone with a smartphone can play a role in the extraordinary Ukrainian resistance. “Osint” researchers – open source intelligence gatherers – are methodically scouring the internet for the latest photos and videos coming out of Ukraine and verifying and geolocating them in real time.
> 
> [Social media turn on Putin, the past master | Carole Cadwalladr | The Guardian](https://www.theguardian.com/world/2022/mar/06/social-media-turn-on-putin-the-past-master)

<!--quoteend-->

> not since 2010 and the [[Arab spring]] has it felt so acutely like technology could be the people’s friend
> 
> [Social media turn on Putin, the past master | Carole Cadwalladr | The Guardian](https://www.theguardian.com/world/2022/mar/06/social-media-turn-on-putin-the-past-master)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-01 Tue]</span></span>


### No-fly zone

> UK Defence Secretary Ben Wallace has made it clear that Britain would not help enforce a no-fly zone over Ukraine because fighting Russian jets would trigger a "war across Europe".
> 
> [No-fly zone: What it means and why the West won't act - BBC News](https://www.bbc.co.uk/news/world-europe-60576443)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-02-28 Mon]</span></span>


### Outcome

> It’s difficult for the west to create a de-escalation pathway,” Acton said. “Much presumably depends on how Putin views the domestic consequences of his backing down – something over which the west has no control
> 
> [Putin’s nuclear posturing requires west to tread extremely carefully | Russia&#x2026;](https://www.theguardian.com/world/2022/feb/27/putin-nuclear-warning-requires-west-to-tread-extremely-carefully)


### Nuclear threat

> Putin’s nuclear signalling is designed to deter the US and its allies from further intervention in Ukraine and economic measures that he may see as an existential threat. But escalation can also take on a momentum of its own, and because the margins are so thin – leaders have only a few minutes to make decisions if they believe their countries are under attack – the US and its allies will have to tread extremely carefully in their response
> 
> [Putin’s nuclear posturing requires west to tread extremely carefully | Russia&#x2026;](https://www.theguardian.com/world/2022/feb/27/putin-nuclear-warning-requires-west-to-tread-extremely-carefully)

<!--quoteend-->

> Successive generations have experienced what it is like to feel the shadow of nuclear annihilation loom over their daily lives, from the Cuban crisis of 1962, to the missile standoff in Europe in the 1980s. This is shaping up to be our turn
> 
> [Putin’s nuclear posturing requires west to tread extremely carefully | Russia&#x2026;](https://www.theguardian.com/world/2022/feb/27/putin-nuclear-warning-requires-west-to-tread-extremely-carefully)


### Information warfare

> Zelenskiy has been constantly on the phone to western leaders, using his Twitter feed to cajole, encourage, scold and praise his allies. In the process, sanctions regarded as unthinkable a week ago have become a moral baseline
> 
> [The phone has become the Ukrainian president’s most effective weapon | Volody&#x2026;](https://www.theguardian.com/world/2022/feb/28/the-phone-has-become-the-ukrainian-presidents-most-effective-weapon)


## <span class="timestamp-wrapper"><span class="timestamp">[2022-02-25 Fri]</span></span>

-   Listened: [The day Putin invaded Ukraine | News | The Guardian](https://www.theguardian.com/news/audio/2022/feb/25/the-day-putin-invaded-ukraine-russia-war-podcast)

-   "Decisive moment in European history"

-   Putin being in a kind of Covid isolation has been mentioned a few times.  Something that has caused him to ruminate, over legacy in particular.

-   "Russia is now fortress Russia"


## <span class="timestamp-wrapper"><span class="timestamp">[2022-02-24 Thu]</span></span>

> The invasion of Ukraine is set to be the biggest war seen in the world since the US led invasion of Iraq in 2003, in which around 175,000 troops were deployed; and Western military leaders have warned could lead to the most serious fighting in Europe since the end of the Second World War.
> 
> &#x2013; [Russia-Ukraine crisis live news: international outcry as Putin launches ‘full&#x2026;](https://www.theguardian.com/world/live/2022/feb/24/russia-invades-ukraine-declares-war-latest-news-live-updates-russian-invasion-vladimir-putin-explosions-bombing-kyiv-kharkiv?page=with:block-6217515a8f0801fb1abbff5b#block-6217515a8f0801fb1abbff5b)

"it's the scariest geopolitical moment of my lifetime, for sure" &#x2013; a friend who knows her stuff.

Talk of world war and european war is contingent on other countries entering the fray.

> we have to offer all the support that we can to the Ukrainians, short of breaching the threshold that would bring the west into a direct war with Russia
> 
> &#x2013; [Russia’s invasion of Ukraine will change the face of Europe for ever | Timoth&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/24/russia-invasion-ukraine-europe-ukrainians)

<!--quoteend-->

> The US and Nato have made it very clear that they will not intervene directly in Ukraine, but their forces are in ever closer proximity and they have vowed to keep sending arms to Ukrainian forces if they become a guerilla resistance to Russian occupation.
> 
> &#x2013; [Decision to invade Ukraine raises questions over Putin’s ‘sense of reality’ |&#x2026;](https://www.theguardian.com/world/2022/feb/24/putin-russian-president-ukraine-invasion-mental-fitness)


### Nuclear threat

The threat of an unhinged despot in charge of a nuclear arsenal is worrying.

> Vladimir Putin’s decision to launch a catastrophic new European war, combined with the sheer weirdness of his recent public appearances, has raised questions in western capitals about the mental stability of the leader of a country with 6,000 nuclear warheads.
> 
> &#x2013; [Decision to invade Ukraine raises questions over Putin’s ‘sense of reality’ |&#x2026;](https://www.theguardian.com/world/2022/feb/24/putin-russian-president-ukraine-invasion-mental-fitness)

<!--quoteend-->

> “Nuclear weapons are an interesting exception to the general rule that the psychology of world leaders is less important than the systems they work in,” Foley said. “Don’t assume that this could proceed in an orderly fashion. It could spin out of control very easily.”
> 
> &#x2013; [Decision to invade Ukraine raises questions over Putin’s ‘sense of reality’ |&#x2026;](https://www.theguardian.com/world/2022/feb/24/putin-russian-president-ukraine-invasion-mental-fitness)

