# trying out org-timeblock



## Issues

Not necessarily with org-timeblock itself - might just be my setup.

-   requires SVG support - termux emacs doesn't seem to have svg support compiled in
    -   org-timeblock gives message `image-type: Invalid image type 'svg'`
    -   maybe I could try Emacs Android build specifically for this?
    -   I think it's for actual display in Emacs, not just for export.  So a hard dependency.
    -   `(image-type-available-p 'svg)` returns nil, so it's not present.
-   doesn't seem to work at all in `emacs -nw`
    -   fails because `face-font` doesn't return as expected in no window mode
-   doesn't seem to play nicely with evil mode
    -   I disabled it temporarliy in the buffer by toggling `evil-local-mode`, and the bindings work again.
    -   So could turn it off with a hook e.g. https://github.com/ichernyshovvv/org-timeblock/pull/51/files
    -   But would be nice if it worked.  When it's off, shortcuts like `SPC SPC` don't work, which is a pain.
    -   someone here has a useful config: https://github.com/ichernyshovvv/org-timeblock/issues/21
    -   Must be some way to have an evil transient mode like e.g. magit does.
-   various warnings at installation
-   changing the timerange of a task doesn't seem to work


## Thoughts

Looks very nice, but some showstoppers for me.

Might try calfw-blocks instead - is that text only?

