# A Hacker Manifesto

A book by [[McKenzie Wark]].

![[images/a-hacker-manifesto.jpg]]


## Overview

> criticizes the commodification of information in the age of digital culture and globalization

<!--quoteend-->

> mimics the epigrammic style of Guy Debord's [[The Society of the Spectacle]]

Yeah&#x2026; which on first attempt makes it hard to read for me.

> Wark builds on Marx and Engels’ ideas, alongside [[Deleuze and Guattari]], by adding two new classes of workers into the mix - the "hacker class" and the "vectoralist class". 

^ Hmm keen to learn how it is building on D&amp;G.


## Themes

-   [[commodification of information]]
-   [[Vectoralist class]]

