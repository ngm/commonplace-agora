# The Machine Stops

> I see something like you in this plate, but I don’t see you. I hear something like you on this telephone, but I don’t hear you.

<!--quoteend-->

> I just stumbled on EM Forster’s The Machine Stops. Published in 1928. It’s about a distant future in which humanity lives in physically isolated cells, communicating only by video link, controlled by an omniscient global technology platform. This is the opening scene.
> 
> https://twitter.com/jamestplunkett/status/1304784524669313027

