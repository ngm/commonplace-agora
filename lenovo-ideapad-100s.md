# Lenovo Ideapad 100s

It's got a cracked LCD, giving some funky glitch art.

Possibly replaceable, however I'll use this one for my media server for now.  It's fairly low power, so it should do the job until I maybe swap it out for something like a Raspberry Pi.

It'll be fine as a media centre box, as it'll either be connected to the projector, or I'll be ssh'ing into it.  So no screen needed.


## Specs

According to this review: 1.33-GHz Intel Atom Z3735F CPU and 2GB of RAM.  It's got 32Gb eMMC storage built in, and you can expand it via SD card.  That'll do me for now, with a small amount of music.  That's probably enough for a very basic media server, as long as I'm not doing any transcoding of movies, and just playing music, should be fine.  People install it on the 2Gb RPi 4, so let's see how we get on.

Apparently the eMMC storage is a bit slow.

> took 2 minutes and 24 seconds to complete the Laptop File Transfer test, which involves copying 4.97GB of mixed-media files. That's a rate of 31 MBps, which is a little less than you'd get with most mechanical hard drives

Huh, slower than a _mechanical_ hard drive?  Well that's pretty bad.


## Installing Linux

I'm familiar with Debian, so I'll go with that.  Stretch/Buster is supported.  I went with the XFCE iso as XFCE is fairly lightweight and I'm reasonably familiar with it.

Someone here having trouble booting to USB, so let's see how we get on there:

https://askubuntu.com/questions/1090098/how-do-i-get-my-lenovo-100s-to-boot-from-usb

To begin with I'll just install Debian and use that.  Then fiddle around with Kodi.

Ahh.. OK.  Difficult one.  The screen is broken, so I can't navigate the BIOS to do the steps I want to do in order to enable booting from Linux.  Doesn't display over external display in the BIOS, only when booted in to Windows.  Closing the lid doesn't force it to display on external.  It only has HDMI, no 

Could try disconnecting the internal display

