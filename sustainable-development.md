# sustainable development

> In broad terms, the concept of sustainable development is an attempt to combine growing concerns about a range of environmental issues with socio-economic issues. 
> 
> &#x2013; [[Sustainable development: mapping different approaches]]

