# Tags vs folders

> Absolutely. As I’m using a tool like Roam, there are no real limits on “where” information should be. This means that I don’t really need to think in terms of files and folders,  but rather can think of digital locations that map to my understanding of the world. 
> 
> As a result, I can think in “The Gardens”, tinker in “The Lab”, and store books in “The Library”.  This mapping is like the thinking equivalent of using a memory palace for remembering facts. 
> 
> Its rooting in the physical world is what makes it a spatial village, as it occupies real “space” in my mind.
> 
> https://twitter.com/hardy_lisa_a/status/1282014988442333185?s=20
> 
> The joy of wikoid systems is you can create/abandon clusters as you feel like it. And it doesn't require you to destroy an old cluster to make a new one.
> 
> Yes! This is in a way a critique of systems that force you to establish a hierarchy (place things in boxes). The more you invest in one hierarchy the harder it is to have it evolve. Or "grow" in a garden methaphor.
> 
> I believe the alternative is to focus on walking paths (here Lisa Hardy's map metaphor is very useful). Because, as you do in physical spaces, you can over time easily change the trajectory of your paths to support your life or project journey.
> 
> (Digital gardeners discussion)

