# Fediverse governance

> As for governance of the [[fediverse]] I think it's already adequate as a federation of independent instances. I don't think we need complicated voting systems of hierarchical bureaucracy. If there is anything meta I would favour something like a second order cybernetics [[viable system model]] with workers always remaining in control and the ability to coordinate, or not, in a very dynamic real-time manner. But the EU would never accept that, of course.
> 
> https://epicyon.freedombone.net/@bob/106105740775129199


## TODO https://mastodon.social/@humanetech/106153229669362117

