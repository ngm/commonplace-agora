# Digital colonialism: Geopolitics of data and development

A
: video

Found at
: https://www.tni.org/en/video/digital-colonialism-geopolitics-of-data-and-development

Featuring
: Ulises Mejias / Paola Ricuarte / Nandini Chami

Part of the [[Digital Capitalism online course]].

[[Digital colonialism]] - [[geopolitics]] of [[data]] and [[development]].

