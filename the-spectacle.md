# the Spectacle

> The spectacle reduces reality to an endless supply of commodifiable fragments, while encouraging us to focus on appearances. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Debord’s term for the **everyday manifestation of capitalist-driven phenomena; advertising, television, film, and celebrity.**
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Instead, Debord describes the spectacle as **capitalism’s instrument for distracting and pacifying the masses**.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The spectacle takes on many more forms today than it did during Debord’s lifetime. **It can be found on every screen that you look at.** It is the advertisements plastered on the subway and the pop-up ads that appear in your browser. It is the listicle telling you “10 things you need to know about ‘x.’” 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The Spectacle is not a collection of images, but a social relation among people, mediated by images.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> If Debord were alive today, he would almost certainly extend his analysis of the spectacle to the Internet and social media. Debord would no doubt have been horrified by social media companies such as Facebook and Twitter, which monetize our friendships, opinions, and emotions. Our internal thoughts and experiences are now commodifiable assets. Did you tweet today? Why haven’t you posted to Instagram? Did you “like” your friend’s photos on Facebook yet?
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> To be clear, Debord did not believe that new technology was, in itself, a bad thing. He specifically objected to the use of perceptual technologies for economic gain. The spectacle, which is driven by economic interest and profit, replaces lived reality with the “contemplation of the spectacle.”
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The spectacle functions as **a pacifier for the masses, a tool that reinforces the status quo and quells dissent**. “The Spectacle presents itself as something enormously positive, indisputable and inaccessible. It says nothing more than ‘ **that which appears is good, that which is good appears**,’” writes Debord. “It demands […] passive acceptance which in fact it already obtained by its manner of appearing without reply, by its monopoly of appearance.”
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Debord defines two primary forms of the spectacle — the concentrated and the diffuse. The concentrated spectacle, which Debord attributes to totalitarian and “Stalinist” regimes, is implemented through the cult of personality and the use of force. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The diffuse spectacle, which relies on a rich abundance of commodities, is typified by wealthy democracies. The latter is far more effective at placating the masses, since it appears to empower individuals through consumer choice. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The diffuse spectacle of modern [[capitalism]] propagates itself by exploiting the spectator’s lingering dissatisfaction. Since the pleasure of acquiring a new commodity is fleeting, it is only a matter of time before we pursue a new desire — a new “fragment” of happiness. The consumer is thus mentally enslaved by the spectacle’s inexorable logic: work harder, buy more.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> After all, how can society produce new services and products without some form of industrialization? On this particular point, Debord is unrelenting, arguing that capitalism — having already served our most basic survival needs (the means to food, shelter, etc.) — relies on fabricating new desires and distractions in order to propagate itself and maintain its oppression over the working classes
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 


## Currency of the spectacle

> [[the spectacle]] is the thing that is the mediate currency for our social interactions that allows us to value things across differences
> 
> &#x2013; [Episode 170: Guy Debord’s “Society of the Spectacle” (Part One)](https://partiallyexaminedlife.com/2017/08/14/ep170-1-debord/) (at 39m10s)

[[Likes]], followers, impressions, 'engagement', seem to be the manifestation of that currency of the spectacle.  

