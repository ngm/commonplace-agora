# Pulling stats out of org-roam

Inspired by Alex Kehayias' note on [Querying Stats From Org-Roam](https://notes.alexkehayias.com/querying-stats-from-org-roam/), I'm going to use org-babel inline evaluation of sql to pull out and publish some stats too.


## How many notes do I have?

```sqlite
select count(*) as number_of_notes from files
```

```text
| number_of_notes |
|-----------------|
|            6366 |
```


## Notes over time

Hmm.  Alex gets notes per month from the date stamp in the headline.  I've removed that as I don't want it in my published filenames.  That's a bit annoying then, does it remove ability to do querying based on trends over time.

The `files` table does hold mtime and atime.  So maybe I can do something with mtime at least.

