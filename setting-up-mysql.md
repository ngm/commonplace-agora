# Setting up MySQL

-   apt install mysql-server
-   mysql<sub>secure</sub><sub>installation</sub>
    -   On ubuntu, mysql seems to always use auth<sub>socket</sub> for root? So a password here makes no difference, even after running mysql<sub>secure</sub><sub>installation</sub>.  https://serverfault.com/a/912963/25452

-   https://dev.mysql.com/doc/refman/5.7/en/security.html

