# Zarah Sultana

> Since her election in December 2019, Zarah Sultana has been a political phenomenon. A high-profile socialist whose regular interventions against capitalism and war give voice to perspectives rarely heard in the halls of power, she has become one of Westminster’s most effective communicators — building platforms of hundreds of thousands of followers across social media platforms
> 
> &#x2013; [[Tribune Winter 2022]]

