# Equitable Internet Initiative

> Since 2016, organizers with the Equitable Internet Initiative (EII) have been working to end this crisis. A program of the [[Detroit Community Technology Project]] (DCTP), EII uses money raised from foundations and a donated upstream connection to bring broadband to hundreds of homes in Detroit.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The organizers in Detroit are proposing another possibility. They are building a network that brings people into new relationships of trust and support and mutual concern, forged in the course of caring for collective infrastructure and caring for one another.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> But digital stewards aren’t just technicians. They are also trained to be community organizers. They receive both a technical and a political education, with a curriculum that draws on the work of revolutionary thinkers like [[Paulo Freire]] and [[Grace Lee Boggs]]. This points to the deeper purpose of the project, which is to increase not just the connectivity of Detroit’s poorer neighborhoods, but their connectedness. “We are working towards a future where neighbors are authentically connected,” read the Working Principles of the Equitable Internet Initiative, “with relationships of [[mutual aid]] that sustain the social, economic, and environmental health of neighborhoods.”

