# The economics of the coming spaceship earth

> Boulding, a pioneering ecological economist, drew on his knowledge of natural systems and the 1960s fascination with space travel to consider Earth as a finite, closed system – a spaceship.
> 
> &#x2013; [[Degrowth, green energy, social equity, and circular economy]]

