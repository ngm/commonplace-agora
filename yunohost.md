# YunoHost

URL
: https://yunohost.org

Kind of an OS specifically designed for installing and running web apps.  I use it and really like it.  It's a really important project. [[Self-hosting]] and community hosting made a lot easier.

My notes on [[Setting up YunoHost]].

Truth: setting up Yunohost and using it to install [[Invidious]], is easier than just installing Invidious by itself.

Interesting the default preference for subfolders rather than subdomains.  I kind of prefer subdomains, but I feel like that's maybe just familiarity.  Obviously some nice reverse proxy magic being set up by YunoHost too, would be interesting to look at that.

The way the YunoHost SSO straight in to [[NextCloud]] works is pretty sweet.

Reading more about Yunohost's aims - https://yunohost.org/#/faq and https://yunohost.org/#/project_organization - I feel like I can't **not** support them.  

The fact YunoHost has the Anarchist Library as an app tells you something about its ethics :)  (I think it's partly because it's building on top of [[Debian]], which also bundles that library IIRC).


## Maintenance


### Updates

I check for updates via the YunoHost admin section (foo.bar/yunohost/admin/#/update) once a week.  The System updates seem to just be whatever apt finds for updates.  The Application updates are the apps you've installed via YunoHost.


### Backups

Haven't got a good story for this yet.  Just relying on [[Hetzner]] daily server backups for now.


## Misc

-   [[Cloudron vs Yunohost]]

