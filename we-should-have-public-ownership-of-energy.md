# We should have public ownership of energy

We should have [[public ownership]] of the UK [[energy industry]].


## Because

-   Privatised energy costs us more as private companies extract more and more profits
-   Privatised energy is adding to our bills when energy companies go bust
-   It’s hugely wasteful to spend public money rescuing private companies and getting nothing back
-   Privatisation was supposed to cut energy prices for the public, yet energy bills went up by fifty percent in real terms between 1996 and 2018 and are now skyrocketing further, all while the private energy companies have paid out billions in dividends to their shareholders.
-   A publicly owned energy provider can make sure people aren't ripped off; can plan for fluctuations in global energy prices; and can invest in renewable energy.
-   Prices are between twenty and thirty percent lower in systems with public ownership.

^ these from [[It's Time to Bring Energy into Public Ownership]]

> Nationalisation means firms would be obliged to provide an affordable service for consumers rather than maximising shareholder value. It could also help to speed up decarbonisation efforts through what’s known as a cap and adapt strategy.
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]

<!--quoteend-->

> Nationalisation alone, however, wouldn’t necessarily put communities in control of energy systems or empower them to make decisions about how to balance affordable energy bills with the necessity of a green transition. Here, experiments in community owned energy, such as those conducted by Scotland’s Community and Renewable Energy Scheme or Wolfhagen, Germany, are instructive.
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

Mainly because I general agree with public ownership of utilities.

