# independent repair

Ensure independent repair is possible outside of manufacturers' or distributors' networks and not limited through software, hardware or contractual elements.

