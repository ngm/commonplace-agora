# Problem: relative paths in org-publish

These don't work for me at the moment.

If I have a link to say /canadian-politics.org" in e.g. /journal/2021-03-11.org, then the resulting link in /journal/2021-03-11.html points to "canadian-politics.html" which is ultimately /journal/canadian-politics.html.  It should really point to "../canadian-politics.html".

