# Publishing your org-roam wiki map to the web: overview

I want my wiki to be a [[sensemaking]] aid - after collecting the dots, to help me then [[connect them]] and see the [[constellations]].

One sensemaking tool in Roam and [[org-roam]] is the [[wiki graph]] - a graph (in the nodes and edges sense) of all of the notes in your wiki and their links to each other.  Not quite a mind map.  More of a hypertext map.  I've been playing around with publishing this map to my public site.

![[org-roam_graph_publish_web_version/2020-05-02_22-08-47_screenshot.png]]


## Does it make sense?

While fun, I'm not yet 100% convinced with these simple graphs as sensemaking aids.  The full map is pretty messy (org-roam's at least, I haven't seen Roam's).  From browsing around it, I don't feel that enlightened about what I'm thinking.  It **did** make me realise I have a few orphaned pages, but you could do that pretty easily with just a list.

That said, it definitely is interesting browsing around it - I come across pages I forgot about, and do see the beginnings of clusters of thoughts.  I think visualisation, when done right, could possibly bring unexpected insights, and if nothing else, provide an alternative way of navigating around.  These graphs are not there yet, but worth experimenting with.


## Publishing org-roam's map to the web

So anyway, with all that, for my [Garden and Stream](https://indieweb.org/2020/Pop-ups/GardenAndStream) post-session [hack and demo](https://indieweb.org/2020/Pop-ups/Demos), I had a bit of a play with org-roam, and got it so that I can publish my wiki graph to my site, and hacked it so that you can click each node in the graph to take you to the corresponding page.

You can find it [here](https://commonplace.doubleloop.net/graph.svg), and there's also a link back to it at the bottom of every page.

A couple of ways I would improve it if I carry on with it:

-   make it more visually appealing and navigable
-   break it up into a map per page, so you get a local map, too.  To be seen whether this is a useful sensemaking aid or not.  Maybe it would just be another useful navigational aid.

I'll make a separate post shortly with the technical details of how I got it to work (I'm experimenting a bit with [[separating]] the overview from the technical with these things).

