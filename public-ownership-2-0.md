# Public ownership 2.0

A
: [[podcast]]

URL
: https://neweconomics.org/2019/02/weekly-economics-podcast-public-ownership-2.0

[[public ownership]].

-   ~00:05:00  There was a lack of democratic and public purpose in some of the big post-war nationalised industries.  (NHS perhaps an exception?)
-   ~00:10:51  [[New Public Management]] - public services but run in a way akin to private sector management.  Not good.

