# district heating

Heat on tap, so to speak.  You don't generate the heat in the building itself.

> where heat is generated at large, centralised facilities, sometimes as a byproduct of industrial processes such as burning biomass. It’s then distributed to homes, replacing the use of localised generation – boilers and the like
> 
> &#x2013; [How will the UK achieve net zero by 2050? Here are four pathways - Positive News](https://www.positive.news/environment/energy/how-will-the-uk-achieve-net-zero-by-2050/)

