# Metaphor

> Metaphor is a conceptual device without any real-world existence, the value of which might be measured by the purpose it serves – in other words, it has instrumental rather than intrinsic value.
> 
> &#x2013; [[Nature Matters: Systems thinking and experts]]

