# Poems

Some poems that I've liked.

-   [[All Watched Over by Machines of Loving Grace, Richard Brautigan]]
-   [[To Posterity, Bertolt Brecht]]
-   [[East Coker, T.S. Eliot]]

