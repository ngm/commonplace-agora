# End of history

> For nearly thirty years Francis Fukuyama’s contention that “Western liberal democracy” represents “the end point of mankind’s ideological evolution” has measured the ebb and flow of history. The collapse of the World Trade Center; the Iraq and Afghanistan wars; the 2008 financial crisis, the rise of ISIS, the climate crisis and now the coronavirus have each prompted op-eds testing the thesis and finding it confirmed or wanting.
> 
> &#x2013; [[Capitalist Catastrophism]]

<!--quoteend-->

> In 2017 even Fukuyama had a go at becoming an anti-Fukuyamist when he voiced concern for the future of Western democracy, later going as far as to say that socialism “ought to come back” — if only to save capitalism from itself.
> 
> &#x2013; [[Capitalist Catastrophism]]

