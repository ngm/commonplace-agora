# Technology of the Oppressed w/ David Nemer

A
: [[podcast]]

URL
: https://techwontsave.us/episode/120_technology_of_the_oppressed_w_david_nemer

Series
: [[Tech Won't Save Us]]

Featuring
: [[David Nemer]]

Great interview focused around the book [[Technology of the Oppressed]].

> Paris Marx is joined by David Nemer to discuss how residents of Brazil’s [[favela]]s reshape technologies developed in the Global North to serve their needs, and how technology alone does not solve social oppression.

About the use of technology in favelas in Brazil.   Really, really good. Talking about [[digital inclusion]] to some extent, specifically in the favelas of Brazil. But maybe more [[digital autonomy]], less digital inclusion.  I guess my main takeaway so far is how digital inclusion attempts of any kind needs to be very much local and embedded with an understanding of what the actual needs are of the communities into which the digital inclusion is being worked on for.

And ultimately there would be no requirements of external support or parachuting in of digital inclusion, it would just be entirely autonomous and with agency of the local communities. I guess in the absence of that, the place to start at least is to not certainly not do it through some dislocated central body which thinks that it knows what all the requirements are for any given locality. I guess it's the idea of appropriate technology in a way, though I kind of love the reframe from E.F. Schumacher's slightly paternalistic 'appropriate technology' as a noun to 'appropriate technology' as a verb, an act of agency.

Nemer uses the term [[mundane technology]] which I think his definition was something like: ways in which people in the local community use the technology for their own needs and requirements and just their everyday life. Sort of I think in contrast with the idea, the Silicon Valley idea, of this constant churn of innovation and new technology. I think mundane technology is more about technology for everyday life. The uses of it within a local community.

I very  much like the example he gave of the [[telecentre]]s. Sort of like community spaces where people could go in and use computers.  Telecentres existed in the favelas and were widely used in Rio, and then one of the mayors said, I know, we will get rid of these and what we'll do instead is we'll have open wifi access for everyone. Then everyone can have access anywhere.  But it didn't work, it wasn't appropriate for the favela.  The telecentres were closed down too as a cost efficiency and people were worse off.

~00:10:21  [[Technology is a site of struggle]]

~00:15:22  Digital inclusion often comes with prescriptions on how to use the provided technologies. [[Digital inclusion risks being paternalistic]].

~00:16:25  With digital inclusion you have to know what is going on in the field where the technology is being given. Importance of local role in digital inclusion. [[Digital inclusion needs to be locally embedded]].

~00:18:34  Mundane technology. vs Silicon Valley innovation.

~00:22:32  Connectivity for all versus community computing centres.

~00:31:56  Community broadband in Lan Houses.

~00:33:03  The Lan Houses moved on to be about technology repair.

~00:34:54  Repair as a means of understanding and developing a personal relationship to technology.

~00:38:09  Repurposing technology for local needs. Qwerty keyboard example.

