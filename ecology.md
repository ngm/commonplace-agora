# Ecology

Ecology is the study of organisms and how they interact with each other and their environment.

It focuses on the interdependence of things, including those that are alive (biotic) and those that aren't (abiotic).

There are different levels of organisation studied in ecology - from the individual to the population to the community to the ecosystem to the biosphere.

Ecology also looks at the processes and patterns that occur within and between ecosystems, such as flows of energy and materials.

-   interaction between organisms and their  environment.
-   communities, ecosystems, biomes.
-   producers, consumers, decomposers.
-   food chains, food webs.
-   energy flows, trophic levels.
-   nutrient cycles


## Applied ecology

&#x2026;

