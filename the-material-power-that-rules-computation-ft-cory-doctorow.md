# The Material Power That Rules Computation (ft. Cory Doctorow)

A
: [[podcast]]

URL
: https://soundcloud.com/thismachinekillspod/294-the-material-power-that-rules-computation-ft-cory-doctorow

Featuring
: [[Cory Doctorow]]


Very anti-Zuboff's analysis from [[surveillance capitalism]]. Suggest that it's not really materially based.

00:39:24: Talk about Doctorow's analysis as being at intersection of IT and IP. Makes a lot of sense to me - plenty of big tech's power comes from legal might. They say that legal tech like IP is often more powerful than IT.

00:53:48: Doctorow's theory of change is build alternatives right now? And design fiction towards new regulation? Need to relisten to this.

~01:15:23: Make use of governmental procurement in order to mandate important requirements (like reparability etc)

