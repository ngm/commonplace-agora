# Lake Baikal

> By far the world’s largest and deepest lake, Lake Baikal is home to many unique flora and fauna, including an endemic species of seal (a surprising animal to find thousands of kilometres from the nearest ocean).
> 
> [[Half-Earth Socialism]]

