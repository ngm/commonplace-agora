# Mark Jenkinson

> Mark Jenkinson, the MP for Workington, who called those who opposed the [[Cumbria coalmine]] “climate alarmists”, said he was confident it was possible to reach net zero “without making my constituents poorer”.
> 
> He added: “I’m delighted that we’re having the adult discussions that so many outside the Conservative party are afraid of having, around the ongoing need for UK oil and gas for transition to net zero by 2050 and beyond it.”
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

