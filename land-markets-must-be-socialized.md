# Land markets must be socialized

[[de-marketing land]]

> Lastly, there are also already existent vehicles for democratic land management and development that can allow us to undo the initial act of capitalism and de-market land.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> Foremost, in recognition of the fact that the price of a location is determined by the desirability of the surrounding community, the government can impose a fee on land-owners equivalent to the ground rent they derive. Especially elegant would be to have these fees provide the initial investment into the public fund paying out the basic income, thus compensating everyone for their exclusion from the locations in question.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> Additionally, the government should devote public financing and its power of eminent domain to energetically foster [[Community Land Trusts]].
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> Perhaps most importantly, the government should massively grow the public housing stock so that it does not merely provide poor people undignified living conditions, but rather houses major swaths of large cities in modest, comfortable units. 
> 
> &#x2013; [[Markets in the Next System]]

