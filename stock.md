# Stock

> Sometimes, large quantities (stocks) can be tied up (sequestered) so that they are not traveling through the cycles. Large changes in stock levels (sequestering or releasing water or carbon) affect climate as ice or carbon dioxide interacts with the planet’s oceans and weather.
> 
> &#x2013; [[A Systems Literacy Manifesto]]

