# Green New Deal



## Thoughts

I like the Green New Deal at the moment.  The idea of transitioning to an environmentally sustainable economy that stays within global boundaries while providing jobs and prosperity - all sounds pretty good.

However, I'm not really aware of all the details, and I know there are critiques of it, and I'd like to read up on those.  


## As a policy

They make some good points about it [on this episode of NovaraFM](https://novaramedia.com/2019/09/27/the-debrief/):

-   it works best as a policy if its very bold - e.g. 2030 as the target
-   it is superficially quite easy to understand, even if you don't dive into all of the depth behind it
-   it could work as a bold single issue policy that counters the 'get Brexit done' single issue policy of the Tories
-   it needs to be framed with an antagonist - in this case, the 1% who need to pay their taxes in order to provide the money to fund it


## Clean energy jobs

> Coalfield communities built our wealth and influence at great cost. We are owed new clean energy jobs and the infrastructure to create them.
> 
> &#x2013; Lisa Nandy, [Yes, we need climate action; but it needs to be rooted in people’s daily reality](https://www.hopenothate.org.uk/yes-we-need-climate-action-but-it-needs-to-be-rooted-in-peoples-daily-reality/)


## Dencentraliesd ownership

-   Green new deal fits well with decentralised worker ownership


## Criticisms

> The so-called “green new deal” is willing to create hope, but is giving space to companies, corporations and governments dedicated to exploiting and destroying nature.
> &#x2013; [About the climate strike and the dark sides of the “green new deal”](https://makerojavagreenagain.org/2019/09/19/about-the-climate-strike-and-the-dark-sides-of-the-green-new-deal/)


## Misc

> In contrast to the failed neoliberal attempt to address rising CO2 levels by creating a market for carbon credits, the GND puts forward a green [[Keynesianism]] that places public job creation and enhanced social welfare at the center of its decarbonization strategy.


## Related

-   [[Lucas Plan]]


## Useful links

-   https://novaramedia.com/2019/09/27/the-debrief/

