# The French Revolution has historical significance to revolutionary socialism today

> Despite strictly speaking being a [[Bourgeois revolution]]  against monarchy and feudalism, the French Revolution has a clear relevance and lineage for the socialist revolutionary left today.
> 
> It was a left-wing, radical, egalitarian revolution. It was a revolution from below that mobilised the commoners.
> 
> We on the left can claim this as our history and our tradition.  It was a precursor to things we care about and identify with.
> 
> For example: the revolutionary government of the time in Paris was called the Paris Commune.  The latter proletarian uprising named themselves after this earlier revolution.  That proletarian revolution saw it as their lineage.
> 
> &#x2013; paraphrasing [[Breht O'Shea]] in [[Revolutionary Left Radio: The French Revolution]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

