# Editable software

How awesome is it that if some functionality doesn't exist in [[Emacs]], you can just add it in with your own function.  (And usually you don't have to write it yourself - a quick search will find that someone else has done it already.)

e.g., I wanted a way to remove a link in org-mode, but leave the description.  (Kind of surprised that isn't built-in to be honest, but there we go).  The Emacs StackExchange has a [bunch of ways of doing that](https://emacs.stackexchange.com/questions/10707/in-org-mode-how-to-remove-a-link).

