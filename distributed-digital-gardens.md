# Distributed digital gardens

Increasingly convinced that the distributed/federated digital garden approach is the way to go for knowledge commoning.  I say this because I've been doing it for a while and it is working well in practice.

Plenty to crib from the ActivityPub model, I think - where you have local, global, and 'those-you-follow' timelines.  Similarly, you could have local, 'those-you-follow', and global gardens.  I'd in fact prefer it if this model worked more on some kind of liquid democracy style, where there's some rippling out effect that lives between the following and global timelines.  I guess perhaps there's some repost/boost equivalent?

And as with ActivityPub, where it's kind of absent, I'd want for there to be a 'group' concept that isn't based on infrastructure, just on interest groups.

As with streams, my ideal is probably some middleground of Indieweb and ActivityPub approaches.

See [[Interlinking wikis]].

-&#x2014;

Ultimately what I'm getting at I guess is to have different filtered views of a node, different ways of defining which subnodes currently show, if you see what I mean. So not just ranking/ordering subnodes but also filtering.

Insofar as how that works for a group, I guess you would define who are the members of the group, and then allow to filter the subnodes by that group.

Other ways of defining subnode filtered views:

-   following (everyone you've chosen to follow)
-   local (everyone on your instance)
-   lists (everyone you've placed in a particular list)
-   global (everyone in the known universe)

Global would be generally useless, except occassionally for discovery, or if it could be somehow sorted/filtered in a [[liquid democracy]] style where I shown things based on who I already like.  (Is boost a [[meca]] version of this?)

[[Wikipedia]] is the final place of your search, if everything else has come to naught. ([[Prefer the subjective over the objective]].)

Shared groups seems like a hard place to start, because how do you manage who is in a group. Does someone own it, do people self-identify, etc?

I think user-defined lists might be easier to start? [[My garden circles]] is where I'm currently manually loosely defining user-defined lists of gardens in different interest areas.

The reason I say I would like a "[[garden reader]]" is I don't wish to expect everyone I want to follow to all coalesce in to one platform. I wish for people to have their garden wherever they wish, and then in my reader I point it at who I want to follow.

So Agora is kind of a garden reader in this sense to me. I mean reader as in the 'RSS reader' sense. [[Flancian]] calls it integrator which is maybe better. Because RSS readers are really integrators.

