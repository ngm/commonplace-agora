# producing not just consuming

> We should ideally balance our input and output through continuous creation. The information we consume should be turned into evergreen notes—using our own words. To make the most of the generation effect, notes should be collated into original articles, connecting the dots across many pieces of information, and putting original thoughts into the world.
> 
> &#x2013; [Keep your levels of consumption and creation balanced • Mental Nodes](https://www.mentalnodes.com/keep-your-levels-of-consumption-and-creation-balanced) 

