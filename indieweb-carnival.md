# IndieWeb Carnival

Found at
: https://indieweb.org/IndieWeb_Carnival

I like it as a concept for nudging people to write in their blogs.

I would see it as an example of the [[Ritualize Togetherness]] pattern of commoning, too.

