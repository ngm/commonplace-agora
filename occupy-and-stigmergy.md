# Occupy and stigmergy

[[Occupy]] and [[Stigmergy]].

> Others have suggested that social movements such as the Occupy Movement can also be described through this model. 
> 
> &#x2013; [stigmergic emergence](http://wellspring.fed.wiki/view/stigmergic-emergence) 

There's no reference who those others are in the above quote, but I came across this from [[C4SS]]: [The Stigmergic Revolution](https://c4ss.org/content/8914) 

