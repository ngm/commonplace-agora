# an Iterative Inquiry diagram for digital ecosocialism

[[Iterative Inquiry]] for [[digital ecosocialism]].

Very much an experiment and a work in progress.  I am working through [[Design Journeys through Complex Systems]] and trying to explore some of the methods and diagrams as I go.

![[2023-11-04_18-33-52_screenshot.png]]

Finding that the ideas I've been exploring in [[Reclaim the stacks]] are mapping pretty well on to this notion of function, structure, process and context/purpose at the various levels (micro, meso, exo and macro).

