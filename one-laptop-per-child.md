# One Laptop per Child

"The Life, Death, and Legacy of One Laptop per Child. A fascinating examination of technological utopianism and its complicated consequences. " 

-   [The Charisma Machine | The MIT Press](https://mitpress.mit.edu/books/charisma-machine)
    
    [[Technological utopianism]]

