# Red-Green Revolution

A
: [[book]]

Subtitle
: The Politics and Technology of Ecosocialism

URL
: https://www.politicalanimalpress.com/product/red-green-revolution-the-politics-and-technology-of-ecosocialism/


## Why Ecosocialism?

> But the scenario is so vast, the problems so complex, that it’s hard for us to feel that we can respond in any clearly definable way. And the demands or seductions of daily life are too compelling—for most of us—to allow us to envisage sustained and effective resistance.

<!--quoteend-->

> Climate change is the most highly publicized environmental threat, but it is not the only one. Others flow from it and/or feed into it—including ocean acidification, depletion of fresh water sources, pollution of air, water, and soil; deforestation; species-loss. Old illnesses revive as rising temperatures lead to the spread of insect-borne diseases; new illnesses appear with the insertion of toxins into our surroundings.

<!--quoteend-->

> Tying the venerated idea of revolution to the new condition of having to overcome the environmental threat to our existence is what has given birth to the now widely used concept of ecosocialism.


## What Kind of Synthesis?

> The two approaches—that of treating ecology as an addendum to Marx vs. that of treating it as integral to Marx—have been characterized by Foster and Burkett as reflecting successive stages of ecosocialist thought.


## Toward an Ecosocialist Politics

> The critique of technology is central to the discussion of ecological alternatives. Too often, however, the critique limits itself to discussion of alternative energy-sources—an approach which perpetuates the popular assumption, fostered by capitalist culture, that for every problem of addressing people’s material needs, there is a precise technological solution, for which we need only await the latest innovation.

<!--quoteend-->

> Innovation, as discussed in Chapter 3, may or may not be a good thing. But the benefits of any given innovation must be assessed not only in terms of such immediate goals as reducing the amount of labor-time necessary to accomplish a particular task, but also in terms of the total complex of changes that the innovation entails—whether in the form of infrastructure, energy and raw materials requirements, spatial and social impacts, or unpredictable side-effects (including those of a social or psychological nature).


## Building the Movement

> The negative dimension of this process involves our unmasking the mode of thought which resists looking at the totality of complex systems, preferring instead to examine their components one by one.

<!--quoteend-->

> Beyond the indigenous model, the positive alternative has always evolved along two strands which are partly complementary to each other and partly antagonistic. One is the tradition of workers’ self-management, associated with cooperatives, direct action, anarchism, and movements from the bottom up. The other is the tradition of socialist revolution, entailing party organization and a focus on attaining and exercising state power.


## The Present Moment


## Toward Ecological Socialism

> The ecological goal is for humans to live somehow in balance with the rest of nature. I understand “balance” not in any absolute or timeless sense, but rather in the sense of maintaining a maximum of biodiversity while keeping toxic substances and greenhouse emissions to a minimum.

<!--quoteend-->

> What clashes with the goal of natural balance is not human needs as such, but only the particular structuring of human needs that has evolved in conjunction with the requirements of, or the pressures exerted by, capital.

<!--quoteend-->

> Like the notion of natural balance, this vision is one that may be pursued both for its own sake and for the sake of the many other possibilities that such a society—and the struggle to attain it—would open up. These include overcoming inequities of race, gender, and region, and in general offering a favorable life-setting for every human being.


## Adding Green to Red

> ecological critique begins by calling attention to the numerous and often severe ways in which those first-epoch regimes flouted ecological norms. The names of Chernobyl, Lake Baikal, and the Aral Sea have already become synonymous with environmental devastation, as have the images of Chinese city-dwellers circulating with face-masks because of the ubiquitous coal dust

<!--quoteend-->

> These have generally been along the lines of criticizing the productivist strands of Marxist thought (i.e., the inclination to prioritize economic growth) and reaffirming a long-term vision of free, self-sufficient, and self-regulated communities.

<!--quoteend-->

> Worker-control and ecological criteria converge around the goal of decentralization (which entails diversification of economic activity and of species-life within each locality).

<!--quoteend-->

> . It is important to note that an authentic worker-control arrangement would eliminate the threat of unemployment which is used by capital to pit workers against environmentalists.

<!--quoteend-->

> Like the categories of worker and consumer, those of worker and environmentalist are in principle (i.e., outside the distorting perspective of capital) not only non-antagonistic; they simply represent different capacities, concerns, or activities which co-exist in each individual.


## Adding Red to Green

> If “socialism” without ecological awareness produced the disasters we have all heard about, it is equally true that environmental concern without socialist politics has left the natural as well as the human world largely at the mercy of those who are content to treat both as mere factors in their profit projections.

<!--quoteend-->

> The precise implications of these three priorities—overcoming waste, taking a comprehensive approach, and unifying disparate sectors—will emerge more clearly as we consider, in turn, the kind of society we envisage and, as a closely linked question, the process of arriving at


## Contours of an Ecological Socialist Society

> ecological socialist society is, in its simplest sense, one in which there are no class divisions and in which humans live in balance with the rest of nature

<!--quoteend-->

> To accept this is to see immediately that the notion of an ecological socialist society is not something arbitrarily cobbled together from disparate principles but is rather, from the outset, a thoroughly coherent project.

<!--quoteend-->

> The two separate terms we use to name it are, in this sense, redundant. If we retain them both, it is only for the sake of expressing continuity with the diverse movements through which our goals were originally articulated.

<!--quoteend-->

> Class division in its present form signifies the existence of a distinct sector of owners/managers of capital, a sector defined by the drive of each of its private or corporate constituents to maximize profits and thereby to increase the share of overall wealth that it controls.

<!--quoteend-->

> ecological potential of any future socialist revolution rests on firmer ground. It rests on two complementary strands of the actual movement that is taking shape: on the one hand, the felt concerns of the millions who worry daily about the physical conditions that they and their progeny will have to endure (conditions associated with illness, scarcity, social breakdown, and natural disaster), and, on the other, the conscious awareness embodied by those activists who perceive the unrelenting nature of environmental constraints and who then seek to build on such knowledge and to express it in their practical work

<!--quoteend-->

> If this dynamic suggests why a classless society would have to be “green,” it also points to why an ecologically sound society would have to be classless—which at the present stage of history means: free of the rule of capital.


## Natural Balance and Human Needs


## What Can We Do Without?

> Ecological and socialist criteria converge in demanding an end to the wasteful consumption of resources and energy. The determination of what is wasteful must ultimately be a social project.

<!--quoteend-->

> Among human activities, we may view as preeminently wasteful those which are undertaken not in response to universal human needs (which include spiritual or cultural as well as material objectives) but rather in conjunction with institutional imperatives reflecting the power and the interests of a particular class.

<!--quoteend-->

> One can serve both humanity and nature by exposing these forms of waste for what they are, and thereby building a movement to do away with them.


## How Can We Make Better Use of Available Resources?

> For the sake of both prior information and eventual implementation, planning has to be democratic. So much is this so, that if our concern is with the wellbeing of society (rather than with the profits of a company), then undemocratic planning is virtually a contradiction in terms. This was the lesson of the Soviet model. Top-down directives, backed by sanctions (whether financial or penal), lead inescapably to a calculus of deception, against which the only possible check is the involvement of a greater cross-section of society, with lower personal stakes on the part of any single participant.

<!--quoteend-->

> This expansion of the human input refers to both of its possible dimensions: both the numbers of people involved and the percentage of each individual’s energy that is devoted to ecological concerns will have to increase. More time will be devoted to bio-diverse gardening; more local travel will be done on foot or bicycle; time spent in community meetings will be valorized.

<!--quoteend-->

> Moreover, everyone recognizes the risks inherent in revolutionary social change. What is distinctive about the ecological crisis, however, is that it dramatically highlights the even greater risk that lies in keeping things the way they are.


## What Can We Gain that We Don’t Have Now?

> The central point is the one implied in the very definition of ecological socialism, namely, that material needs would be satisfied without further taxing the environment.


## Means and Ends

> our argument up to this point has suggested anything, it is that a transformation of revolutionary proportions is the only way out of an otherwise hopeless social and environmental crisis

<!--quoteend-->

> Minimizing violence, however, does not mean rejecting revolution. The idea that it does have such a meaning rests on a deceptive argument similar to one that is often used against socialism. Just as socialism is linked simplistically to the worst of its first-epoch expressions, so revolution is identified, as though by definition, with the imposition of violence.

<!--quoteend-->

> The best hope for keeping violence to a minimum lies in building the most solidly based, coherent, and effective movement that we can.


## Getting Ourselves Together

> Before being able to supplant institutions so deeply entrenched as the capitalist meat industry, the ecosocialist movement will need to develop into an effective counterhegemonic organization.

<!--quoteend-->

> The popular presence will take the form of protest activity, alternative media, and various kinds of service, community, or production units. The coordinating mechanism will be something in the nature of a political party. In any case, it will be an organization which, while oriented toward contesting for power, does not confine itself to electoral activity but rather engages at the same time in a full span of intellectual, cultural, and mass-organizing work.

<!--quoteend-->

> Ecological concern is widely viewed as just one “identity” among many, when in fact the interests it embraces, although clearly in conflict with those of capital, cut across all other social boundaries.

<!--quoteend-->

> The perspective of ecological socialism offers, potentially, a wealth of arguments to transcend this attitude and, with it, the destructive fragmentation that plagues progressive movements

<!--quoteend-->

> The ecosocialist perspective, for its part, must be ready to enrich and adapt its vision as people from each new sector venture out into a larger world, a world of common struggle


## “Progress” or Progress? Defining a Socialist Technology

> is time for socialists to reclaim the concept of progress. Progress means improvement. Under the hegemonic impact of capital, however, the concept has been steered into a narrowly instrumental/technological groove, with disastrous ecological consequences. First-epoch socialism did not challenge this understanding but mostly bought into it, thinking to overtake capitalism on its own turf. All this has led many people to view “progress” as something to be avoided. Such a response is understandable but self-defeating. A merely negative approach affords no basis on which to challenge the status quo. It moreover forfeits to the enemy a term which, despite almost two centuries of misuse, retains its positive connotation—as in the phrase “making progress”—of change in a desired direction

<!--quoteend-->

> Our task, then, must be to radically alter people’s sense of what genuine progress would entail. This requires us, first, to review the current status of “progress” under conditions of capitalist hyper-development; second, to comment on the technological contradictions of first-epoch socialism; and third, to sketch the contours of an authentically socialist technology.


## Capitalist “Progress”

> Within capitalist society, progress has come to be widely understood in terms of increased levels of information and the improved execution of particular tasks (e.g., producing or moving things more quickly; targeting military objectives or establishing genetic connections more precisely).1 Even generalized notions of progress seem to involve no more than an aggregation of such particular attainments. Thus, the greatest progress is identified with the most advanced and sophisticated machinery, irrespective of what effects it might be having either on the human species or on the natural world as a whole.

<!--quoteend-->

> Such effects, it is further believed, may be either good or bad, depending on how the technology is used.2 This claim of technological neutrality is indeed one of capital’s strongest ideological props, and it has sharply divided the ranks of capitalism’s critics. Even within Marx’s own writings, one can readily

<!--quoteend-->

> This kind of twofold trajectory has marked progressive social movements from the beginning, resulting in situations where the gains of certain constituencies appear partly to offset the further degradation of others.

<!--quoteend-->

> Obviously, the repercussions of capitalist rule are sufficiently vast to encompass certain emancipatory trends.

<!--quoteend-->

> Capitalism has always tended to inflict personal economic insecurity on the working class, but this “normal” tendency had been significantly attenuated in the industrialized countries—if not by the welfare state, then at least by the establishment of labor unions.

<!--quoteend-->

> This last autonomous sphere is now in turn slated for destruction, through the development of technologies, contractual arrangements, and patenting practices designed to assure that no step of the production process could be carried out free of corporate control. The ultimate expression of this corporate sweep is the “terminator gene,” which, when introduced into a seed, assures the non-renewability of the crop.

<!--quoteend-->

> Here, in some ways even more transparently than with nuclear weaponry, is the reductio ad absurdum of capitalist-driven technological innovation: an invention that has a purely negative use-value, with no other purpose than to multiply sales

<!--quoteend-->

> The “terminator” technology is an extreme case of capital’s contempt for natural processes. The more routine expression of this contempt lies in capital’s underlying commitment to growth, accumulation, and profit.

<!--quoteend-->

> Above all, not only the corporations but also capitalist governments, without exception, remain fully committed to an overall strategy of growth

<!--quoteend-->

> New “information age” technologies replace one another at an accelerating pace


## Socialist Progress?

> In the particular Soviet setting, however, given the military dangers flowing from capitalist encirclement, the official view of technology never ceased to be conditioned by short-term requirements.

<!--quoteend-->

> Economic visions, for their part, were left to focus less on the social relations of production than on grandiose construction projects, of which the dream of “people’s palaces on the peaks of Mont Blanc and at the bottom of the Atlantic”22 was only the most extreme case.

<!--quoteend-->

> The Soviet regime’s most decisive shortfall, as a technological model, was its failure to breach the authoritarian structure of the productive enterprise.

<!--quoteend-->

> The corollary to managerial power, in securing plan-fulfillment, was the assignment of financial rewards or penalties to the manager. Such incentives created powerful inducements for managers to try to protect themselves by overstating their input-needs and understating their output-targets, thereby setting in motion an intricate spiral of second-guessing, over-supervision, and corrupt shortcuts.

<!--quoteend-->

> Overall, first-epoch socialism remained unable to transcend the longstanding capitalist contradiction between technological and social progress.

<!--quoteend-->

> we have been slow in doing this, it is partly because not enough of us have grasped the full extent to which the technology developed by capital, far from being neutral, is indeed, no matter who runs it, a preeminently capitalist technology


## Toward a Socialist Technology, 1


## What is socialist technology?

> What defines a technology (on whatever scale) as socialist is simply its compatibility with—and its ability to further—the overall goals of socialism.

<!--quoteend-->

> Insofar as these goals relate to technology, they emerge clearly from what remained deficient in first-epoch practice, namely, commitment to social equality and to ecological health. A socialist technology, then, is one that is grounded in these two requirements, both of which are served by a more collective approach to production and consumption.

<!--quoteend-->

> The class power of capital has been based, from the beginning, on unrestricted access to—and often control over—natural as well as human resources.

<!--quoteend-->

> The expansion of capitalism has been historically coterminous with its subjection of the natural world

<!--quoteend-->

> technology may be discerned in anything from a single device to a whole network of relations, involving, among other things, machines, resources, producers, and users


## Getting There

> Socialist technology, taken as a whole, would give us a different world.

