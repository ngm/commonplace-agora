# Datafication

> Datafication is now understood canonically as a process that transforms many aspects of everyday life into data, and data into value, and it is considered a “legitimate means to access, understand and monitor people’s behavior
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> datafication suggests that data production and circulation are significant in themselves, regardless of what the data might contain
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Put another way, datafication opens out spaces for citizens to participate in by generating data, auditing government data, or creating alternative data analytic systems, but these spaces for action still follow the dominant logic of big data optimization
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Data is not out there waiting to be discovered as if it already exists in the world like crude oil and raw ore. Rather, it is a recorded abstraction of the world, created through technological systems and social, political and economic lenses3. This process, also referred to as ‘datafication’ happens through interconnected and convergent information infrastructures including, smart devices, cloud based platforms, data analytics, network cables, and server farms, all of which allow for the continual capture and circulation of data.
> 
> &#x2013; [[Digital Capitalism online course]]

