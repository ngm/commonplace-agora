# Consumer price index

> The Consumer Price Index (CPI) is the official metric of [[inflation]] used by the government
> 
> &#x2013; [[Tribune Winter 2022]]

