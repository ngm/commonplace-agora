# What's the difference between digital commons and knowledge commons?

My minimal bit of digging in to descriptions of both digital commons and knowledge commons both tended to mention the same kind of projects, e.g. free software projects, open access projects, Wikipedia, etc&#x2026; probably knowledge commons examples just happened to list ones that were facilitated by the digital.

> Latter ([[knowledge commons]]) has nothing necessarily to do with the digital - only contingently, in the present historical epoch of digitally mediated hegemony, digital media and the participation in power circuits of a Professional-managerial class. 
> 
> Former ([[digital commons]]) has nothing especially to do with literacy, organising capacity &amp; labour power - except that organising the digital - like organising **anything** that is even slightly complex - pivots on collective labour power.
> 
> &#x2013; [[Mike Hales]] https://social.coop/@mike_hales/107430540493147092

