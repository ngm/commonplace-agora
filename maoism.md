# Maoism

[[Mass line]].  [[Cultural Revolution]].

> Maoism is “an internal critique of [[Stalinism]] that fails to break with Stalinism,”
> 
> &#x2013; [[First and Third World Ecosocialisms]]

<!--quoteend-->

> A key element of the philosophy of Maoism was the need for intellectuals to become integrated with the labouring classes
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

