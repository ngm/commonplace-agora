# Full of Noises

URL
: https://fonfestival.org/

I love Full of Noises.  They promote weird music in [[Barrow]].

> Full of Noises is a sound art and new music organisation based in a public park on Cumbria’s Furness Peninsula. We produce and commission new work from contemporary composers and sound artists through a programme of residencies, performances and public realm installations.

