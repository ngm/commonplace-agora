# Richard Seymour, "The Disenchanted Earth: Reflections on Ecosocialism and Barbarism"

A
: [[podcast]]

URL
: https://newbooksnetwork.com/the-disenchanted-earth

Featuring
: [[Richard Seymour]]

Fantastic interview.  Feels similar in its prescriptions for [[Eco-socialism]] as [[Half-Earth Socialism]], though without entering in to the [[socialist calculation debate]].

~ 00:06:01  Paternalism and humanitarian interventionism.

~ 00:12:43  Early Marxist approaches towards ecology

~ 00:13:35  Capitalism has created a phenomenal amount of wealth. Unfortunately it has in the process horrendously exploited people and destroyed the planet.

~ 00:14:39  We're in a position now where we can't really entertain the traditional idea of red plenty.

~ 00:17:16 Even with 100% reneweable energy we would still only have 60% of current energy usage.  We have to drastically reduce energy consumption. He mentions meat and agriculture.

[[Climate sadism]].

