# The basic rate of out-of-work benefits is at its lowest for 30 years

[[Britain's welfare system]]. [[Benefits]].

[The Guardian view on benefit levels: the only way is up | Editorial | The Gua&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/24/the-guardian-view-on-benefit-levels-the-only-way-is-up)

