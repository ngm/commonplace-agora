# Connecting Zotero to koreader

I've been reading more academic papers of late in addition to online essays.  And storing them in [[Zotero]] rather than [[Wallabag]] (where I save the online essays).  Now I want to read them on my kobo.  So trying to hook up Zotero in a similar-ish way to how Wallabag is hooked up.

Came across this:  https://forums.zotero.org/discussion/90686/koreader

Which references this: https://github.com/stelzch/zotero.koplugin/

Following the instructions there:

-   Connected my kobo to laptop and created a `Zotero` folder in its root.
-   Copied `storage` and `zotero.sqlite` from my local Zotero folder to the folder on the Kobo.
-   Make a folder on kobo: `mkdir .adds/koreader/plugins/zotero.koplugin`
-   Download the files from https://github.com/stelzch/zotero.koplugin/ (I did it as a zip file) and copy them to the folder on the kobo.
-   Eject the kobo, let koreader restart.
-   Go to `Search > Zotero > Settings` and set the folder to the Zotero folder we copied the storage and sqlite to.
-   Go to `Search > Zotero > Browse database`.
-   Browse your PDFs!

