# error "Invalid type in command series" from org-super-agenda

Somehow I had deleted the line `agenda ""` from my org-super-agenda config.  Which apparently I need, although I'm not sure why.


## Resources

-   https://www.reddit.com/r/orgmode/comments/yn7s3o/easy_20_for_help_with_an_orgmode_agenda_searchview/
-   https://www.reddit.com/r/orgmode/comments/z9jrtq/comment/iytvtk9/

