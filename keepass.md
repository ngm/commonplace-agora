# KeePass

I use [[KeePassXC]] on Linux, and [[KeePassDX]] on Android.  I sync the passwords file between the two with [[syncthing]], and back it up periodically.

