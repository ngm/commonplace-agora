# Right to Repair in Europe

[[Right to repair]] in [[Europe]].

> So far, EU policy related to the Right to repair has been limited to regulating product per product and it’s becoming urgent to secure mandatory rules that could apply to multiple product categories at once (such as [[non-destructive disassembly]], [[access to spare parts]] and [[access to repair information]] for instance). 


## Campaigners

-   https://repair.eu

