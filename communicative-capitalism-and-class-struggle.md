# Communicative Capitalism and Class Struggle

URL
: https://spheres-journal.org/contribution/communicative-capitalism-and-class-struggle/

Author
: [[Jodi Dean]]

Year
: 2014

[[Communicative capitalism]] and [[class struggle]].

Viewing protests of ~2011 as class struggle of the knowledge class.  Communicative capitalism as that which they are struggling against.  Appropriation of value from social interactions and big data surveillance as the thing which is being protested against.

A bit of overlap with [[surveillance capitalism]], which was being formulated around the same time (2014) I think.

> We have entered the first phase of the revolt of the knowledge class. The protests associated with the [[Occupy]] movement, [[Chilean student protests]], the Montreal protests [[[Quebec student protests]]], European anti-austerity protests [[[Anti-austerity movement]]], some components of the protests of the [[Arab spring]], as well as multiple ongoing and intermittent strikes of teachers, civil servants, and medical workers all over the world, are protests of those proletarianized under communicative capitalism.

Dean prefers the party to crowds (see [[Crowds and Party]]), so I think she is aiming to frame the various protests of 2011 onwards as class struggles rather than spontaneous networked protest.

> These revolts make sense as class struggle, as the political struggle of a [[knowledge class]] whose work is exploited and lives are expropriated by communicative capitalism.

Yep, it looks so.

> Looked at most broadly, the demographics of recent protests point to heavy involvement by those who are young, well-educated, and un- or underemployed.

<!--quoteend-->

> That a struggle does not take the form of a classic workplace struggle, in other words, does not mean that it is not class struggle.

Class struggle does not mean only workplace struggle.

> Demographics and workplace struggles support the idea of the revolt of the knowledge class.

<!--quoteend-->

> The revolts of the past few years exemplify class struggle under communicative capitalism. Accepting this position entails rejecting the idea that they are primarily post-political, democratic, or strictly local movements.

<!--quoteend-->

> It entails a recognition of changes in class struggles’ mode of appearance; it looks different under conditions of distributed, precarious, and unpaid communicative labor, from how it appeared during the industrial labor movement.


## Action

> We would expect struggles to extend beyond the workplace, perhaps involving hacking as a kind of contemporary sabotage as well as various kinds of misuse of communicative devices.

<!--quoteend-->

> But more fundamentally given the changes in communication and subjectivity, we would expect the expropriated to face real difficulties in organization, in constructing clear narratives, and symbols.

