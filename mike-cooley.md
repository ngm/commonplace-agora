# Mike Cooley

[[The Lucas Plan]].  [[Socially useful production]].

> Mike Cooley was an industrial designer and trades union shop steward at [[Lucas Aerospace]]. He was aware of arguments and initiatives for industrial democracy, and a firm believer in creativity inherent to all people. It was at Lucas, and through the development of a worker’s alternative plan for the company, that ideas for socially useful production found practical expression. It was a focal experience for many, and gave an impulse for the wider movement.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

