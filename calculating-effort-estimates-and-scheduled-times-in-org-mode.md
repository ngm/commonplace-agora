# Calculating effort estimates and scheduled times in org-mode

Couple of things I'd like to be able to do in org-mode:

-   automatically set the end time of a task calculated from the start time and the effort estimate time
-   automatically set effort estimate to match scheduled start/end time of task.

Why?  I plan my work day out fairly fine-grained and I visualise it with 
https://github.com/Fuco1/org-timeline.  I try to make sure that everything has a sensible time estimate, and that I've not taken too much on for the day.  Having the start and end times and then the visual timeline helps do this.

Quite often I have a start time and an estimate, but no end time.  Or a start and end time, but no estimate.  It'd be nice to have a quick shortcut to set both of these.  Not that it takes **that** long to type it in, but, you know, it's good to reduce friction.

This looks like a good starting point: [emacs - org-mode: creation time range from effort estimate - Stack Overflow](https://stackoverflow.com/questions/23044588/org-mode-creation-time-range-from-effort-estimate)

I'll give it a go - it's quite old though, and someone in 2020 said it didn't work for them with Emacs 26.

It seems to work out of the box, nice!  That's if you run it on the org item directly though, not from agenda view.

Copy pasting here for posterity:

```emacs-lisp
(defun org-schedule-effort ()
(interactive)
  (save-excursion
    (org-back-to-heading t)
    (let* (
	(element (org-element-at-point))
	(effort (org-element-property :EFFORT element))
	(scheduled (org-element-property :scheduled element))
	(ts-year-start (org-element-property :year-start scheduled))
	(ts-month-start (org-element-property :month-start scheduled))
	(ts-day-start (org-element-property :day-start scheduled))
	(ts-hour-start (org-element-property :hour-start scheduled))
	(ts-minute-start (org-element-property :minute-start scheduled)) )
      (org-schedule nil (concat
	(format "%s" ts-year-start)
	"-"
	(if (< ts-month-start 10)
	  (concat "0" (format "%s" ts-month-start))
	  (format "%s" ts-month-start))
	"-"
	(if (< ts-day-start 10)
	  (concat "0" (format "%s" ts-day-start))
	  (format "%s" ts-day-start))
	" "
	(if (< ts-hour-start 10)
	  (concat "0" (format "%s" ts-hour-start))
	  (format "%s" ts-hour-start))
	":"
	(if (< ts-minute-start 10)
	  (concat "0" (format "%s" ts-minute-start))
	  (format "%s" ts-minute-start))
	"+"
	effort)) )))
```

So a couple of improvements:

-   allow running it from org-agenda
-   assign it to a shortcut

This is also similarish: [emacs - org-mode : how to calculate dynamic deadlines - Stack Overflow](https://stackoverflow.com/questions/32479979/org-mode-how-to-calculate-dynamic-deadlines).

