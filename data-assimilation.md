# Data assimilation

> climate scientists and meteorologists have also figured out how to continuously update their models with new information, having developed extraordinarily powerful algorithms in a field known as data assimilation

[[climate science]] [[meteorology]]

