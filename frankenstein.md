# Frankenstein

> Although it is often overlooked, Mary Shelley’s [[Frankenstein]]: Or the Modern Prometheus grapples repeatedly with the problem of vegetarianism. The ‘monster’ chooses not to ‘destroy the lamb and the kid, to glut my appetite; acorns and berries afford me sufficient nourishment
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> The monster is a ‘modern Prometheus’ not only because he is a ‘new man’ born of the mangled [[French Revolution]], but also because he chooses not to eat meat
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Shelley’s monster is made of sewn flesh stolen from proletarian corpses, and Frankenstein’s desire to create a ‘new species’ by instilling human consciousness in inert nature ends in death and chaos
> 
> &#x2013; [[Half-Earth Socialism]]

