# Glitch Gallery

URL
: https://glitchgallery.org/

> an online exhibition of pretty software bugs! This is a museum of accidental art, an enthusiastic embrace of mistakes, a celebration of emergent beauty.

