# Weeknotes W27 2022

Seem to have mostly consumed things about socialism and ecology.

Plus Borish Johnson resigned - now wondering who will be least worse for the environment (as well as everything else) from the leadership contenders.


## Sunday

-   [[Technics]]

-   Listened: [[Turning the Earth into Money w/ John Bellamy Foster]]

-   [[carbon footprint]] is bunk

-   Environmentalism is presently primarily the preserve of the [[professional-managerial class]].  It needs to be inherently valuable to the [[working class]].
    -   [[Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"]]

-   Decarbonise electricity, then electrify everything.

-   [[Climate sadism]]


## Monday

-   [[Free association of producers]] is a goal of socialism/communism.

-   [[Net zero policies would reduce the cost of living]]. That's an incredibly important line of enquiry to pursue.  Talk of [[degrowth]] isn't appealing if you're already on the breadline.

-   'The cost of living' is a wretched phrase.

-   Reading: [[Marx's Vision of Sustainable Human Development]]


## Tuesday

-   Watched: [[Armageddon]].  Pretty terrible.  [[Don't Look Up]] is better&#x2026;


## Wednesday

-   I need to renew my passport.  This is massively over subscribed and delayed in the UK right now.  So I'm following a Twitter bot some guy made that tweets whenever new appointments are available.  World beating service from the UK gov once again!


## Thursday

-   [[Boris Johnson]] is going.  This is good.  ([[Boris Johnson is a liar]] etc).
-   [[Steve Baker]] is in the news.  This is not good.

-   Listened: [[Adrienne Buller, "The Value of a Whale: On the Illusions of Green Capitalism"]]


## Friday

-   Looking at different [[options for publishing an org-roam-based digital garden]] because mine is currently horrendously slow.  Nothing ticking all the boxes yet though.

