# FOLLOW THE (CITY) LEADER: the power of local action on the climate crisis

A
: [[podcast]]

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/follow-the-city-leader

Series
: [[Reasons to be Cheerful]]

The power of [[local action]] on the [[climate crisis]].

Some good examples of [[municipalism]].  Cities and local authorities as drivers of climate action, rather than national governments.

~00:16:09 [[C40 Cities]]. Have been the drivers behind some of the big climate targets. I think they said around 700 million population represented by C40? And a substantial economic amount.

~00:34:36 [[UK100]].  "It supports decision-makers in UK towns, cities and counties in their transition to Net Zero."

~00:38:54  [[Planning permission]] and [[public procurement]] are things local authorities already have plenty of say over.

~00:50:16  [[15-minute city]].  Everything you need should be 15 minutes or less walk away.

