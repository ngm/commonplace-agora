# WCV S2: We've run with it like a dog with a burst ball

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/wcv-s2-weve-ran-with-it-like-a-dog-with-a-burst-ball

Series
: [[Working Class Voices]]

Great interview. Touches on how local [[community organising]] can be the way to get working class communities involved in climate related issues. Where the intersection is people, planet and pocket.

Terry makes lots of references to various community groups that I'd like to listen to again and make note of - practical insights into what works on the ground. One was something like a local reduce, reuse, recycle hub.

