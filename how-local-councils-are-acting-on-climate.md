# How local councils are acting on climate

URL
: https://takeclimateaction.uk/resources/councils-tackling-climate-chaos

Case studies of [[climate action]] from [[councils]].  Seems to be an initiative from [[Friends of the Earth]].

In the areas of:

-   Nature
-   Waste
-   Transport
-   Energy
-   Buildings
-   Climate vulnerability

Overall it's part of the campaign [[Climate Action Plan for Councils]].


## Energy

-   [[How Barnsley's pioneering community energy scheme tackled fuel poverty]]

