# Strategy and tactics in anarchist politics

[[Strategy]]. [[Tactics]].

> For anarchism, then, the kind of [[repertoires of action]] that are of interest are those that aim at changing reality in the here and now, at creating alternatives to what presently exists and, where judged to be necessary, directly challenging the dominance or even existence of what exists.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> A sticking point in anarchist theory and practice, at least since the alterglobalisation movement’s prominence around the turn of the millennium, has been whether the concept of strategy can be applied to anarchism or whether anarchism is, or ought to be in principle, purely tactical.
> 
> &#x2013; [[Anarchist Cybernetics]]


## Strategy

> in the language of Beer’s cybernetics and the VSM, the strategic function in an organisation is concerned with regulating the overall behaviour of the organisation in line with defined goals and in response to change both inside and outside the organisation.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> First, strategy should operate to frame tactical action within the overall goals of the organisation. Second, strategy should be informed by the anarchist politics of selforganisation and participatory democracy discussed throughout this book so far. Third, strategy should be flexible and responsive to change.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> strategy is concerned with both what is happening in the moment inside the organisation and how best to regulate it to achieve set goals as well as what is happening outside in the external environment and with respect to the possible futures of the organisation.
> 
> &#x2013; [[Anarchist Cybernetics]]


## Tactics

> the tactics of anarchist organising cover this range of collective actions that, in one way or another, enact the ideals of anarchist politics – a concept called ‘[[prefiguration]]’
> 
> &#x2013; [[Anarchist Cybernetics]]

