# Socialisation

In economics:

> the process of establishing social ownership of the means of production and/or a system of production for use
> 
> &#x2013; [Socialization (disambiguation) - Wikipedia](https://en.wikipedia.org/wiki/Socialization_(disambiguation)) 

