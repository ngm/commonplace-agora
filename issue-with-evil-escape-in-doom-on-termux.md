# issue with evil-escape in Doom on Termux

I couldn't seem to get [[evil-escape]] to work in [[Doom]] on [[Termux]].

I have become very used to evil-escape in [[spacemacs]] on desktop. It simply let's you use a two character keychord to escape from insert mode to normal mode.

Sounds so simple as to be almost pointless, but when it's gone I really miss it. (Such as when using vi on a sever&#x2026;)

Additionally, on Termux, the escape key is a big stretch away when one handed. So evil-escape would be handy to reduce how much need to do that stretch.

So, a few things:

-   Doom seems to by default rebind evil-escape from fd to jk, so for a little while I was trying the wrong sequence
-   Even with the right sequence, the speed at which you have to press it is just not possible one handed in Emacs in Termux, for me at least

So I customised:

-   evil-escape-key-sequence to jj
-   evil-escape-delay to 0.25

This now works for me.

