# A Common Platform: Reimagining Data and Platforms

Found at
: [A Common Platform](https://www.common-wealth.co.uk/reports/common-platform-tech-utility-antitrust)

> a small group of large platform companies – sitting at the heart of the transactions and engagements of the digital, and increasingly, the physical economy – have become the robber barons and rentier giants of our age. 

<!--quoteend-->

> The challenge is to liberate the democratic and enlivening potential of the platform from the logics of concentrated corporate ownership and profit maximisation.

