# Issue during YunoHost update: An error occurred during the signature verification

I'm getting these errors during a [[YunoHost]] system update:

> W: Some index files failed to download. They have been ignored, or old ones used instead.
> 
> W: Failed to fetch https://packages.sury.org/php/dists/buster/InRelease The following signatures were invalid: EXPKEYSIG B188E2B695BD4743 DEB.SURY.ORG Automatic Signing Key
> 
> W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: https://packages.sury.org/php buster InRelease: The following signatures were invalid: EXPKEYSIG B188E2B695BD4743 DEB.SURY.ORG Automatic Signing Key 

A quick search finds a possible solution here: https://forum.yunohost.org/t/recurring-issues-during-system-updates-these-days-and-how-to-fix-them/15266

Log on to the server and do:

```shell
sudo apt-key del 95BD4743;
wget -nv -O - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
```

OK, yup, that worked.

