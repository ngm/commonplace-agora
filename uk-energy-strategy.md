# UK energy strategy

[[The UK energy strategy is terrible]] (as of April 2022).

The one from March 2023 is also terrible.  What's the difference?
[‘Half-baked, half-hearted’: critics deride UK’s long-awaited climate strategy&#x2026;](https://www.theguardian.com/business/2023/mar/30/half-baked-half-hearted-critics-ridicule-uk-long-awaited-climate-strategy)

> The real lesson from the battlefields of Ukraine is that Britain needs to rid itself of its fossil fuel addiction entirely and become self-reliant on electricity that is generated cleanly and efficiently
> 
> [The Observer view on Ukraine and the climate emergency | Observer editorial |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency)


## Resources

-   [A guide to the UK energy security strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/mar/19/a-guide-to-the-uk-energy-security-strategy-cost-of-living-crisis)


## Letter to my MP

<span class="timestamp-wrapper"><span class="timestamp">[2022-03-23 Wed]</span></span>

I am aware that the government is currently developing a new energy supply plan to reduce our reliance in the UK on Russian fossil fuels. This is very important but also should not be an excuse for the UK to return to environmentally-damaging fossil fuels.

As a constituent I hope you could speak out publicly and in parliament on the following points with regards to this plan:

-   In the face of climate breakdown we should prioritise green energy solutions such as onshore wind farms instead of environmentally-damaging fossil fuels like coal, gas and fracking.
-   We should be supported to rapidly reduce our gas use - by providing grants to help us insulate our homes, and switch to alternative energy sources such as solar panels and electric heat pumps.
-   We should prioritise immediate support for people being hit hardest by soaring energy bills. This could be supported by a windfall tax on fossil fuel firms, which are making an obscene amount of profits right now.

