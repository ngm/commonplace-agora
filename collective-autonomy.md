# collective autonomy

> In contrast to liberal individualism, then, collective autonomy can be defined as follows: Collective Autonomy is the collectively determined capacity and scope an individual or group has to decide and act within the constraints set by collective organisation.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> For Collective Autonomy, freedom is not possible without a collective or organisational framework and is both conditioned and constrained by this as a necessary element of its existence.
> 
> &#x2013; [[Anarchist Cybernetics]]

