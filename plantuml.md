# PlantUML

I use PlantUML for diagramming. Mainly in [[org-mode]].


## Resources

https://www.planttext.com/


## Notes

I was wondering what had happened to the old plantuml default theme in the more recent releases.

Here's the answer:

https://github.com/plantuml/plantuml/issues/946

You just need `skin rose` in there.

I'd quite like the skethcy theme with the rose skin, but that doesn't seem to work.

