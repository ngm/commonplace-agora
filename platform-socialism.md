# Platform socialism

I'm reading the book about platform socialism at the moment after listening to the podcast [[Platform Socialism and Web3]].

Seems pretty good - focused on the community ownership and governance of the tech platforms we use.


## An idea

> the organisation of the [[digital economy]] through the social ownership of digital assets

<!--quoteend-->

> democratic control over the infrastructure and systems that govern our digital lives.

<!--quoteend-->

> Social ownership of digital platforms should also be combined with new structures of democratic governance.

<!--quoteend-->

> Multi-stakeholder governance would therefore be suitable in many cases, which could include consumers/users and other groups affected by the actions of the firm.

<!--quoteend-->

> Drawing on Cole’s idea of a participatory society of active citizens, we could imagine a thriving platform ecosystem of different-sized associations that are socially owned, are democratically managed and provide benefits for their members and the public.


## A book

A
: [[book]]

Author
: [[James Muldoon]]

To bring about a change to big tech, says we need to be _resisting_, _regulating_ and _recoding_.

Resisting is things like unionisation and strikes.
Regulation is support from state legislation.
Recoding is creating alternative platforms.

-   [[platform cooperativism]], [[civic platforms]], [[public-commons partnerships]]
-   [[Guild socialism]], [[G. D. H. Cole]]
-   [[Participatory planning]], [[Decentralized planning]], [[Otto Neurath]]

> A deep sense of [[technological determinism]] pervades our present era

<!--quoteend-->

> Reclaiming our sense of [[collective self-determination]] requires a new kind of [[platform economy]]. How do we imagine an alternative that is neither private oligarchy nor unaccountable state bureaucracy – an alternative outside of rule by [[Big Tech]] or [[Big State]]? The answer lies in new forms of [[participatory governance]] and [[decentralised governance]] which place human freedom over profits and ensure the benefits of technology are equally distributed. It involves citizens’ active participation in the design and control of socio-technical systems rather than their after-the-fact regulation by a technocratic elite. I call this idea platform socialism – the organisation of the digital economy through the social ownership of digital assets and democratic control over the infrastructure and systems that govern our digital lives.

<!--quoteend-->

> Rather than just trying to fix Facebook, we should start to imagine what better alternatives could take its place

<!--quoteend-->

> This movement is not a quest for an ideal or harmonious society but is driven by antagonistic practices and a resistance to commodification and exploitation. It gestures beyond piecemeal reforms and the bland crisis management and troubleshooting that characterises much of our present response to Big Tech

<!--quoteend-->

> It is strategically unsound to always be on the defensive, waiting to protest the latest round of capitalist tech innovation

<!--quoteend-->

> The approach advocated here is a threefold strategy of resisting, regulating and recoding existing digital platforms

<!--quoteend-->

> Platform socialism starts at the local level with services delivered by organisations that promote inclusion, social ownership and community control.

<!--quoteend-->

> We should foster the development of broad-based ownership structures, including workers’ co-operatives, community and municipal ownership and small and medium-sized private enterprises.

Socialising capital funds.

> The socialisation of these large pools of capital could open up new avenues for these funds to be used to finance ambitious infrastructure projects, provide grants for local businesses and promote just and sustainable investment in socially useful projects.

<!--quoteend-->

> In fact, when we look at the options for democratic ownership and control more closely, there are two equally problematic alternatives to be avoided: exclusive workers’ control and top-down nationalisation

<!--quoteend-->

> The concern is that workers left to control individual firms could potentially ignore the interests of others and engage in price gouging or monopoly/rent-seeking behaviour.

<!--quoteend-->

> Even the Swedish Meidner Plan – which would have transferred ownership of large corporations to employees – was planned to be controlled by big unions rather than the workers themselves.

<!--quoteend-->

> With an absence of grassroots participation, nationalisation simply replaces private oligarchs with distant bureaucrats

<!--quoteend-->

> We should strive for a pluralist approach to democratic platform governance in which the ownership and management of platforms are undertaken at a variety of levels: local, regional, national and international.

<!--quoteend-->

> Democratic platforms should be governed by a principle of [[subsidiarity]] – services should be delivered by the most local and proximate level that would be able to undertake the task efficiently, sustainably and in a manner that would maximise its benefit for users.


### Resist, regulate, recode


#### Resist

Things like unionisation and strikes.


#### Regulate

Muncipalism is probably not enough.

> Local and municipal institutions may serve as ideal locations for new participatory structures of certain forms of platform governance, but concerted effort is needed at the level of the state to adequately regulate current multinational companies.

Regulations are needed to supplement.  Regulations are a "necessary but not sufficient condition for the transformation of the platform economy".

> Dozens of mini-Facebooks still operating on a similar model will not fundamentally transform the underlying problems of the digital economy.

<!--quoteend-->

> But the liberal agenda of strengthening markets and enhancing competition runs counter to our ultimate goal of creating and sustaining non-market forms of coordinating economic activities.

<!--quoteend-->

> We need new legislation that makes it clear that companies cannot find loopholes to exploit vulnerable classes of workers and erode the existing protections available to full-time employees.

<!--quoteend-->

> Autonomous craftsmen in late nineteenth-century America safeguarded their collective control over the labour process by refusing to work when their bosses were watching.

<!--quoteend-->

> Despite the limitations of the antitrust cases identified in previous chapters, the US federal government should pursue the unwinding of tech empires such as the mergers between Facebook, WhatsApp and Instagram as a first step towards reducing their monopoly power

<!--quoteend-->

> Predatory and unfair business practices by large tech companies should also be investigated.

<!--quoteend-->

> As a step towards democratic ownership and control, companies that provide a service to the public should be designated as public utilities and regulated as such.

<!--quoteend-->

> Another important regulation would be for platform companies to be compelled to share their data with public authorities.

<!--quoteend-->

> This means that regulation will always have to be pursued through harnessing the power of a bottom-up people’s movement to put pressure on regulators to make decisions in the best interests of the public.


#### Recode

> Throughout this book we have stressed the importance of constructing institutional designs that would empower voluntary associations in civil society to exercise effective control over digital platforms.

<!--quoteend-->

> In order to recode Big Tech, we can’t be satisfied with just fixing the platforms we have. We need to imagine how we can build and support alternatives that will be able to grow and sustain themselves in the long term as centres of social power.

-   draws parallels to real utopias.  a la [[build the new web in the shell of the old]]

> Drawing on the framework of sociologist Eric Olin Wright, this strategy could be characterised as one of interstitial transformation: building radical democratic institutions within the ‘cracks’ of the capitalist system with a view to eroding the dominance of capitalist institutions.

<!--quoteend-->

> Wright’s reflections on theories of transformation are particularly pertinent to platform socialism because many of the examples of emerging ‘real utopias’ he championed were taken from internet-based forms of organisation. He characterises Wikipedia as an example of a non-capitalist form of knowledge production that exists alongside other forms on the internet. He also drew inspiration from online strategies to subvert capitalist intellectual property rights such as music sharing sites and free and open-source software.

<!--quoteend-->

> The primary contradiction of this strategy of interstitial transformation is that the growth of alternatives would be strongly opposed by elites whose interests were threatened by transformation. Small-scale alternatives would be tolerated and even encouraged as convenient pressure release valves for the system. But as soon as alternatives began to threaten real change, elites would exercise their power to block and subvert them.


### Reading log


#### <span class="timestamp-wrapper"><span class="timestamp">[2022-01-29 Sat]</span></span>

First chapter was a good intro, the next two are pretty heavy, focusing on the problems with Facebook and Airbnb. I get that it's necessary for a thesis, if you're going to propose something new then you need to examine what's currently wrong. That's fine but no longer new to me, and sometimes a bit draining just hearing about the problems. But the author stated early on that they will go on to discuss their proposals for how to change things. I'm eager to get to these parts.


#### <span class="timestamp-wrapper"><span class="timestamp">[2022-12-16 Fri]</span></span>

Liking the chaper on guild socialism and democratic planning, heavily featuring Cole and Neurath.  And the chapter on platform cooperativism and new municipalism.


#### <span class="timestamp-wrapper"><span class="timestamp">[2022-12-21 Wed]</span></span>

Finished it.  It was good.  Lots of good food for thought.  Resist, regulate and recode is a good framework to have in mind.  I can try and see how that might be applied to digital ecosocialism.

**Lots** of highlights to incorporate elsewhere in the garden.


## Bookmarks

-   [Regulating Big Tech is not enough. We need platform socialism | openDemocracy](https://www.opendemocracy.net/en/regulating-big-tech-is-not-enough-we-need-platform-socialism/)

