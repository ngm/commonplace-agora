# Problems with ecosocialism

URL
: https://theecologist.org/2022/dec/05/problems-ecosocialism

Gives some critiques of [[ecosocialism]].  I don't necessarily agree, but worth a read and a think about.

Criticisms are roughly:

-   not enough concrete ideas on actual transition (perhaps true, also recognised by ecosocialists themselves)
-   too much focus on the social, not enough on the eco (I'd disagree with that from what I've seen)
-   capitalism is too embedded to overthrow it, need to work within current system (kind of the reformist argument)

