# Syncthing Android doesn't sync until I restart or rescan

I've noticed that on my [[Samsung Galaxy S5]] syncthing often doesn't seem to sync unless I restart it.

Having a bit of a search brings up a few topics:

-   https://forum.syncthing.net/t/syncthing-android-not-detecting-changes/14001/4
-   https://forum.syncthing.net/t/syncthing-andoid-synchronizes-only-after-restart-on-marshmellow/7622/15

One note in there is that a rescan through the web GUI will also trigger a sync.

But either way it's a bit frustrating, as you have to remember to restart it.  It introduces quite a bit of friction for my [[org/orgzly]] setup.

The problem seems to be that 'inotify' doesn't pick up file changes for some reason.

The first thread (from 2016) suggests that turning on an option to foreground the service in the `Experimental` section of the settings helps.  It was introduced in 0.8.0, but no longer seems to exist in the version I'm on (v1.10.0).

It's possible it was turned in to the 'Start service automatically on boot' option in the `Behaviour` section, which tries to run syncthing persistently.  But I already have that on and still have the problem.

I've tried disabling app power saving in the Android/Samsung settings - doesn't seem to make any difference.  Also the 'Keep the CPU awake' setting in `Experimental`, also not making any difference.

