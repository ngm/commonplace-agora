# Chattanooga Gig

> Seventy-five years later, Chattanooga embarked on another experiment in public provision. In 2010, the Electric Power Board, now rebranded EPB, began offering broadband service. More specifically, it began offering the fastest broadband service in the country: 1 gigabit per second, more than two hundred times faster than the national average at the time. These speeds gave the network its nickname: “the Gig.”

<!--quoteend-->

> In the years since, the Gig has become the most famous municipal broadband network in the country.

