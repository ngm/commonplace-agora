# Social housing

> Social housing is any rental housing that may be owned and managed by the state, by non-profit organizations, or by a combination of the two, usually with the aim of providing affordable housing.
> 
> &#x2013; [Public housing - Wikipedia](https://en.wikipedia.org/wiki/Public_housing)

Saw an interesting exhibition about social housing at the V&amp;A: [[A Home for All: Six Experiments in Social Housing]].


## Social housing estates in London

-   Alexandra road estate
-   Adelaide road estate
-   Lion Green Road
-   Spa green
-   Keeling house


## History in the UK

> In the mid-1970s nearly half the population of England and Wales lived in council houses
> 
> &#x2013; [[A Home for All: Six Experiments in Social Housing]]

But as we know…

> with the election of [[Margaret Thatcher]] in 1979 the housing sector underwent rapid [[privatisation]]. Local authority architects’ departments were disbanded, public land was sold off, and millions of council homes were bought[…]
> 
> &#x2013; [[A Home for All: Six Experiments in Social Housing]]

<!--quoteend-->

> Clegg hit out at David Cameron and his Conservative partners in government. He said the former Tory leader or the chancellor – “I honestly can’t remember whom – looked genuinely nonplussed and said: ‘I don’t understand why you keep going on about the need for more social housing – it just creates Labour voters.’
> 
> &#x2013; [Clegg: Osborne casually cut welfare for poorest to boost Tory popularity | Ni&#x2026;](https://www.theguardian.com/politics/2016/sep/02/nick-clegg-george-osborne-cut-welfare-poorest-boost-tory-popularity)

