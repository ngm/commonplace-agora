# People like wind power

-   [[Four out of five members of the public support the use of onshore wind farms]]

-   [[More than three-quarters of the public are in favour of windfarms being built in the UK]]

Why is the first one 4/5, and the second is &gt;3/4?  Either way, both high.

-   [[Wind farms appeal to voters across the parties]]

