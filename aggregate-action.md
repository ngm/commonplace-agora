# aggregate action

> Important societal shifts might come about as a result of the accumulation of multiple uncoordinated individual actions and changes of behaviour over time, many at rather small scales (physical dispositions, ways of dressing, personal preferences and attitudes …).
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

