# The EU battery directive will make it easier to replace batteries

URL
: https://repair.eu/news/the-eu-battery-directive-will-make-it-easier-to-replace-batteries/

> The full text includes substantial changes in the ways batteries will have to be manufactured, serviced and disposed of.

<!--quoteend-->

> With this new regulation, all new products put on the market will have to have replaceable batteries: in many cases users will have to be able to replace them themselves.

All new products?  I think it's only for certain categories.  What does it mean for a battery to be replaceable?

> Also, batteries will become available as [[spare parts]] for 5 years after placing the last unit of the model on the market.

Pricing is still not made clear, which isn't great.

> We also celebrate the ban of the unfair practice of part-pairing, a software trick hindering part replacement, banned from the EU market – at least for batteries.

A ban on [[part-pairing]] for batteries is pretty great.

> software shall not be used to prevent the replacement of a portable battery, including in light means of transport or of their key components with another compatible battery or key components.

Four years before this comes into effect, which isn't great.

> exemptions for products used in wet conditions: the directive won’t require designs with user-replaceable batteries for devices like electric toothbrushes, and potentially wearable ones too.

A disappointing exemption - not user replaceable batteries in some products.  However they will have to be replaceable by independent professional repairers, which is something.

> For light means of transport, the batteries will only need to be removable by independent professionals (and not by end users) but the requirements include the battery cells, meaning that batteries will also become repairable.

