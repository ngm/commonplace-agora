# data centre



## Environment impact

> Data centers already account for around one percent of the world’s greenhouse gas emissions, according to the International Energy Agency. That is expected to rise as demand for cloud computing increases, but the companies running search have promised to reduce their net contribution to global heating.
> 
> &#x2013; [[The Generative AI Race Has a Dirty Secret]]

<!--quoteend-->

> “It’s definitely not as bad as transportation or the textile industry,” Gómez-Rodríguez says. “But [AI] can be a significant contributor to emissions.”
> 
> &#x2013; [[The Generative AI Race Has a Dirty Secret]]

<!--quoteend-->

> It is estimated that data centers account for around 2% of global water usage, with the average facility consuming the same amount of water per day as a large town.
> 
> &#x2013; [Panel: The war on water - Can data centers really consume water sustainably? &#x2026;](https://www.datacenterdynamics.com/en/broadcasts/world-water-day/2023/episode-1/)

<!--quoteend-->

> As the cloud took over, more computation fell into the hands of a few dominant tech companies and they made the move to what are called “hyperscale” data centers. Those facilities are usually over 10,000 square feet and hold more than 5,000 servers, but those being built today are often many times larger than that. For example, Amazon says its data centers can have up to 50,000 servers each, while Microsoft has a campus of 20 data centers in Quincy, Washington with almost half a million servers between them
> 
> &#x2013; [[AI is fueling a data center boom. It must be stopped.]]

