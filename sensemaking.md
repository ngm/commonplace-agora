# sensemaking

> A question that came up for me, musing about the conversation is what it is I am trying to automate or reduce friction for? If I am trying to automate curation (getting from crumbs to articles automagically) then that would be undesirable. Only I should curate, as it is my learning and agency that is involved. Having sensemaking aids that surface patterns, visualise links etc would be very helpful. Also in terms of timelines, and in terms of shifting vocabulary (tags) for similar content.
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

