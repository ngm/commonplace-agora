# Inflation Reduction Act

> The IRA greenlights offshore oil and gas leases in Alaska and the Gulf of Mexico for the next ten years and backs the bitterly contested Mountain Valley pipeline. At its core, it aims to ‘derisk’ private capital investment in the green transition, in line with what Daniela Gabor calls the ‘Wall Street Consensus’. Its major policy tool is its tax-credit programme, available for mostly middle-class homeowners looking to buy EVs or new appliances and private companies that develop and manufacture electric cars, wind turbines, solar panels and batteries.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> The wager for the planet thus appears to be that state-supported green capital can beat fossil fuels on the free market.
> 
> &#x2013; [[Mish-Mash Ecologism]]

