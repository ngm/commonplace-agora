# Soviet economy



## Earlier

> the Soviet economy of the 1930s was exceptional in the degree to which it reinvested, rather than consuming, its production.   
> 
> &#x2013; [[Red Plenty]]


## Later

[[planned economy]].

> Indeed, there was a philosophical issue involved here, a point on which it was important for Soviet planners to feel that they were keeping faith with Marx, even if in almost every other respect their post-revolutionary world parted company with his
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> By counting actual bags of cement rather than the phantom of cash, the Soviet economy was voting for reality, for the material world as it truly was in itself, rather than for the ideological hallucination
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Instead of calculating [[Gross Domestic Product]], the sum of all the incomes earned in a country, the USSR calculated [[Net Material Product]], the country’s total output of stuff – expressed, for convenience, in roubles
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> This kind of ‘extensive’ growth (as opposed to the ‘intensive’ growth of rising productivity) came with built-in limits, and the Soviet economy was already nearing them
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> In effect, they were spraying Soviet industry with the money they had so painfully extracted from the populace, and wasting more than a third of it in the process
> 
> &#x2013; [[Red Plenty]]

