# Humans are eusocial animals

Humans are [[eusocial]] animals. (Like bees and ants).

So sayeth [[Community-oriented life at human scale]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

This is apparently an area of debate:

> An early 21st century debate focused on whether humans are prosocial or eusocial.
> 
> &#x2013; [Eusociality - Wikipedia](https://en.wikipedia.org/wiki/Eusociality#In_humans)

<!--quoteend-->

> [44] Edward O. Wilson called humans eusocial apes, arguing for similarities to ants, and observing that early hominins cooperated to rear their children while other members of the same group hunted and foraged.[3] Wilson argued that through cooperation and teamwork, ants and humans form superorganisms.[45] Wilson's claims were vigorously rejected by critics of group selection theory, which grounded Wilson's argument,[3][46][47] and also because human reproductive labor is not divided between castes.[46] However, it has been claimed that suicide,[2] male homosexuality,[48] and female menopause[49] evolved through kin selection,[50][51] which, if true, would by some definitions make humans eusocial.[1]
> 
> [Eusociality - Wikipedia](https://en.wikipedia.org/wiki/Eusociality#In_humans)

