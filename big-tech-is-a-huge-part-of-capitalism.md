# Big tech is a huge part of capitalism

[[Big tech]] is at the leading edge of contemporary [[capitalism]].

> The information technology sector broadly defined is now at the leading edge of the capitalist system.
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc) 

<!--quoteend-->

> In the second quarter of 2019 the top five firms in the world by market capitalisation were Microsoft, Apple, Amazon, Alphabet and Facebook. 
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc) 

