# Archiving my tweets in my garden with orger

I stopped using Twitter with any regularity a long time back now.  I was never a big user of it.  But I did for a while.  Out of interest, I'm going to get my archive of old tweets and put them into my digital garden for posterity.  I can use [[orger]] for this.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-02 Fri 12:11]</span></span>

-   Install orger
-   Install HPI
-   Setup HPI for Twitter - https://github.com/karlicoss/HPI/blob/master/doc/SETUP.org#twitter
    -   Need to download Twitter archive - https://help.twitter.com/en/managing-your-account/how-to-download-your-twitter-archive
        -   Now I have to wait until they notify me about it being ready.  How long does that take?  Quite a while I guess.  Maybe I come back to this tomorrow.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-04 Sun 12:49]</span></span>

Archive is ready now (I think it was ready somepoint yesterday, so let's say it took about a day to be produced).

-   Place my `twitter-2021-03-04-*.zip` file in `~/Data/twitter-archives/`.
-   Add the details to `~/.config/my/my/config/__init__.py`
-   OK, that's HPI config done.  Now use it with orger.
-   Copy https://github.com/karlicoss/orger/blob/master/modules/twitter.py
-   `import my.twitter.all` -&gt; `import my.twitter.archive`
-   `python3 twitter.py`
    
    And it works!

