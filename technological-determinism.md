# technological determinism

[[determinism]] of [[technology]].

> Technological determinism seeks to show technical developments, media, or technology as a whole, as the key mover in history and social change.
> 
> &#x2013; [Technological determinism - Wikipedia](https://en.wikipedia.org/wiki/Technological_determinism) 

