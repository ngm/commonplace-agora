# Weeknotes W28 2022

Just a summary.  Click the links to find out more on each one.

tl;dr: more eco-socialism. Watching things and trying to get more out of them by thinking about them critically.  Some emacs faffing.  Much more happened of course, but I don't tend to write much about work or my private life here in my digital garden.


## Sunday

-   [[Open Referral]] has been adopted as a UK standard.
    -   https://openreferral.org/uk-government-endorses-open-referral-uk/

-   [[Open Green Map]]

-   Watched: [[Cloud Atlas]].
    -   I thought it was really good.  Entertainingly done and plenty of thoughts about human nature to cogg on.

-   Oh yeah, a couple of days back we watched [[Wonder Woman]].

-   [[Political theory and film]].

-   I did my first [[parkrun]] in about 2.5 years yesterday.  I got a new personal worst, but still pleased to be doing it again&#x2026;

-   We visited [[Walney Island]].


## Monday

-   I appear to be missing in action this day.


## Tuesday

-   We've watched [[The Good Place]], WandaVision,  [[Loki]], [[The Falcon and the Winter Soldier]], and [[Hawkeye]] over the last few months.  Also watching [[Ms. Marvel]] as the episodes come out.  All Marvel ones, apart from The Good Place.  All pretty decent and worth a watch.  Some of the Marvel ones even have a bit of social commentary in them.

-   Reading: [[Technology of the Oppressed]]
    -   [[the structural violence of the information age]]


## Wednesday

-   So cool that [[KeePassXC]] comes with a CLI!  Makes sense but I hadn't realised.  That is very handy for me.

-   Listened: [[The Uber files: the unicorn]]
    -   Generally been avoiding a lot of the [[Uber Files]] stuff so far.  Not quite sure why&#x2026; it's just kind of depressing I guess.  And kind of confirming what was already known or suspected - that a firm built on aggression, growth and toxic masculinity is corrupt and rotten on the inside as well as the outside.

-   [[Growth]].

> What the system has done, as a mechanism to continue with growth at all costs, is actually to burn the future. And the future is the least renewable resource. There is no way that we can reuse the time we had when we started this conversation. And by building up a system which is more debt-driven—where we keep consumption going, but by creating more and more debt—what we’re actually doing is burning or stealing the time of people in the future. Because their time will be devoted to repaying the debt
> 
> &#x2013; [The Infamous 1972 Report That Warned of Civilization&amp;#x27;s Collapse | WIRED](https://www.wired.com/story/the-infamous-1972-report-that-warned-of-civilizations-collapse/) (h/t [Doug Belshaw](https://thoughtshrapnel.com/2022/07/12/the-future-is-the-least-renewable-resource/))


## Thursday

-   I need to find a way to share my spacemacs config in both my personal and my work logins, but have some customisations in both spaces.
    -   [[spacemacs profile-specific configuration]]


## Friday

-   Listened: [[Drew Pendergrass and Troy Vettese on Half Earth Socialism]]


## Saturday

-   Reading: [[A Wizard of Earthsea]]
    -   Can definitely see the climate change / don't interfere with nature subtext.
    -   As a piece of writing, not enjoying it quite so much as [[The Dispossessed]] or [[The Left Hand of Darkness]], but still good.

-   The [[Half-Earth Socialism]] authors really don't like [[geoengineering]], particularly [[solar radiation management]].

-   Did the [[parkrun]] again.  A bit faster than last week; stopped less times.

