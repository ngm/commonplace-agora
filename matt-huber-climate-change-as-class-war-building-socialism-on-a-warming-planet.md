# Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"

A
: [[podcast]]

URL
: https://revolutionaryleftradio.libsyn.com/climate-change-as-class-war

Featuring
: [[Matthew Huber]] / [[Breht O'Shea]]

Based around the book [[Climate Change as Class War: Building Socialism on a Warming Planet]].

[[Marxism]]. [[eco-socialism]].

Emphasis on [[class struggle]], structure building, [[trade unionism]].

Similar to Richard Seymour, mentions [[carbon footprint]] as problematic. (Invented by BP I think he said?)

Environmentalism currently as generally the preserve of the [[professional-managerial class]]. Needs to appeal to working class. A lot of degrowth and rationing stuff doesn't. PM class likes that sort of thing because they are already at a reasonable level of material comfort, so losing a bit doesn't hurt, and makes them feel good.

Lack of appeal for working class makes it possible for right wingers to claim that environmental policies are **against** the working class. Makes me thing of [[Net Zero Scrutiny Group]] for example.

~01:07:40  Decarbonise electricity, then electrify everything.

