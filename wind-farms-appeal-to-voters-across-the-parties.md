# Wind farms appeal to voters across the parties

> In the Opinium poll, 79% of Tory voters said they were strongly or somewhat in favour of [[windfarms]] being installed in the UK, compared with 83% of Labour voters and 88% of Lib Dems. Two-thirds of all voters said they would be happy for a windfarm to be built near them.
> 
> -   [Three-quarters of Britons back expansion of wind power, poll reveals | Wind p&#x2026;](https://www.theguardian.com/environment/2022/apr/10/three-quarters-of-britons-back-expansion-of-wind-power-poll-reveals)

