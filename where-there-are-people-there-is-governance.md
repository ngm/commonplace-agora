# Where there are people, there is governance

Where there are people, there is [[governance]].  ([[Adam Day, States of Disorder, Ecosystems of Governance]])

