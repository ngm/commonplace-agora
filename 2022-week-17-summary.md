# 2022 Week 17 Summary

Don't seem to have journalled much in the garden this week.

I think partly I've gotten a little distracted by the stream.

There has been a new wave of Mastodon users and usage thanks to the possibility of [[Elon Musk's takeover of Twitter]].

Which is cool, and meant I've been a bit more active there. Feels like the new people brings a new diversity of content.  Less niche stuff in a way.

But also I still think stream-y microblogging is not the social media of choice for me.  Or maybe just one component of it.  Great for networking and connecting, but (for me) a bit too much of a time sink and not good for deep engagement.

Keeping up to date with local activities, but annoyed by how it's all on Twitter and Facebook.  Thinking about some kind of [[Municipalist social media]].

Had a go with the [[telescope]].

