# Organising for Revolution With Rodrigo Nunes

A
: [[podcast]]

URL
: https://novaramedia.com/2021/12/01/microdose-organising-for-revolution-with-rodrigo-nunes/

Series
: [[#ACFM]]

Featuring
: [[Rodrigo Nunes]]

Enjoyed a lot.  Relevant to the question of [[Revolution in the twenty-first century]].

Gives nice overview of recent history of political organisation that is the backdrop to Nunes' book [[Neither Vertical Nor Horizontal]].

Nostalgic throwback to [[Critical Mass]].  I remember going on a couple in Leeds when I was at university in the early 2000s.  (Nunes here mentions it as an archetypal horizontalist group in Porto Allegre in the 2010s.)

The ideas of [[intensive revolution]] and [[extensive revolution]] is very interesting.  [[Temporary Autonomous Zone]]s being an example of intensive revolution.  i.e. spatially and temporally local.  Very radical but also short-lived and bypassing the difficulty of building something bigger.  Extensive being a worldwide systemic shift.

Also very interesting: climate breakdown is a moment that needs extensive revolution.  1 million intensive revolutions would still not be enough to avert it.  (Unless they all join up perhaps?)

Some of the podcast does break down in to a bit of old guys grumbling about horizontalist millenials.  They come from a more vertical perspective I would wager.  But I totally get the need for the [[synthesis of horizontalism and verticalism]], so if you take the grumbling as good faith critique then that's OK.

