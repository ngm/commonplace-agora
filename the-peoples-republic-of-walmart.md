# The People's Republic of Walmart

> In The People’s Republic of Walmart, Leigh Phillips and Michal Rozworski argue that large companies like [[Walmart]] and [[Amazon]] already use these digital tools for internal planning—and that they now need only be adapted for socialist use.
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> The authors take the reader on a tour of topics loosely related to the [[socialist calculation debate]] per se and along the way present highly condensed arguments against capitalism and for economic planning with ‘democratic’ features
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> While the patient can be expected to prefer a cure, a revenue stream that endures as long as the patient does might prove more attractive to a drugmaker7. This anxiety about proper standards of conduct for the practioners of some art being subordinated to commercial standards is at least as old as the Platonic dialogues (“Is medicine to be the art of fee-making because the healer collects a fee for healing?")
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> We’re invited to imagine “…Walmart’s operational efficiency, its logistical genius, its architecture of agile economic planning… captured and transformed by those who aim toward a more egalitarian, liberatory society
> 
> &#x2013; [[Review: People's Republic of Walmart]]


## Criticism

> What precisely is understood by ‘planning’ and what might make future implementations ‘democratic’ remains hazy throughout. Important arguments against planning are imperfectly communicated. As a result, People’s Republic is unlikely to persuade the skeptical reader of any political hue. This is lamentable, as the arguments in favor of certain conceptions of planning are much stronger than is commonly supposed today
> 
> &#x2013; [[Review: People's Republic of Walmart]]

