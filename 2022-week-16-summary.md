# 2022 Week 16 Summary

-   Reading a bit more on [[Climate Action Plan for Councils]] via [[Climate Action Plan Explorer]].  I like the checklist of actions they have for councils.
-   Also reading up on the upcoming [[Changes to councils in Cumbria]].
-   And finding a few climate-oriented organisations in Cumbria, like [[Cumbria Action for Sustainability]] and various local XRs.  Need to look at it in a bit more detail but [[Zero Carbon Cumbria]] seems to have a checklist similar to the one from Climate Action Plan for Councils.
-   Apple have published their latest environmental progress report -  [[Apple 2022 Environmental Progress Report]].
-   [[Restart]] have launched our [[Fixing Factory]] in Brent.
-   [[decentralised manufacture]].
-   Had a nice chat with Dan about [[org-roam]], [[zettelkasten]], and brainstorming a way to publish a collaborative [[digital garden]] he's working on.
-   Attended the [[social.coop Spring 2022 Strategy Session]] which was nice.
-   On the personal side, I'm trying to get in the habit of doing morning pages in a handwritten journal.  It's going well so far - a good mental health practice.
-   Reading leaflets from local councillors for the [[2022 Westmorland and Furness Council election]].

