# Ursula K. Le Guin

[[The Dispossessed]] and [[The Left Hand of Darkness]] are high up there in my list of favourite books.

> In this way, Le Guin’s humble [[utopianism]] is similar to the works of [[Morris]], More, and Plato. In her books, observes critic Colin Burrow, ‘civilization isn’t about conquering planets or travelling faster than the speed of light. It’s about keeping going even when you think you’re lost, recognizing that living means keeping children alive, growing fruit trees, watching things change and tending the goats.’ Civilization is about life
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Le Guin was not drawn to old-fashioned science fiction with its visions of technological splendour. Instead, she focused on stories about people learning the lesson of humility. She talked of failure, of despair, of ‘dark places’. Humanity is in a dark place now, with catastrophe after catastrophe piling one upon the other. Le Guin counsels that ‘our roots are in the dark; the earth is our country … what hope we have lies there
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> As Le Guin puts it: there is room enough to keep even Man where he belongs, in his place in the scheme of things; there is time enough to gather plenty of wild oats and sow them too … and still the story isn’t over. Still there are seeds to be gathered, and room in the bag of stars
> 
> &#x2013; [[Half-Earth Socialism]]

