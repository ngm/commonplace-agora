# Tragedy of the Time Horizon

> Having debunked the tragedy of the commons, they now were trying to direct our attention to what they called the tragedy of the time horizon. Meaning we can’t imagine the suffering of the people of the future, so nothing much gets done on their behalf. What we do now creates damage that hits decades later, so we don’t charge ourselves for it, and the standard approach has been that future generations will be richer and stronger than us, and they’ll find solutions to their problems. But by the time they get here, these problems will have become too big to solve. That’s the tragedy of the time horizon, that we don’t look more than a few years ahead, or even in many cases, as with high-speed trading, a few micro-seconds ahead. And the tragedy of the time horizon is a true tragedy, because many of the worst climate impacts will be irreversible. Extinctions and ocean warming can’t be fixed no matter how much money future people have, so economics as practiced misses a fundamental aspect of reality.
> 
> &#x2013; [[The Ministry for the Future]]

