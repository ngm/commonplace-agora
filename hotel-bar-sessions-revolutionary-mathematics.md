# Hotel Bar Sessions: Revolutionary Mathematics

A
: [[podcast]]

Lives at
: https://hotelbarpodcast.com/podcast/episode-78-revolutionary-mathematics-with-justin-joque/

Part of
: [[Hotel Bar Sessions]]

[[Revolutionary]] [[mathematics]].

Good stuff.

Discussion of 'frequentism' and Bayesianism schools of thought in probability.  The guest being against frequentism, a fan of Bayesianism I think, but saying it's about who has control of the knowledge and technology that is the problem.

In addition to probability discussion and Bayes, also wider topics like who controls knowledge creation, political economy of knowledge creation, etc.

I can't remember all the details.  But I recall there a healthy dose of [[Marxist]] concepts in there.  A mentioned of the enclosure of the [[general intellect]] sticks in my head.

