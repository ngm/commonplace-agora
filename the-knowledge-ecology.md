# The Knowledge Ecology

URL
: https://subconscious.substack.com/p/the-knowledge-ecology

Author
: [[Gordon Brander]]

> Bateson line: "Do thermostats lie? Yes-no-yes-no-yes-no. You see, the cybernetic equivalent of logic is oscillation

<!--quoteend-->

> Where does the music live? The cybernetic perspective resolves this paradox by decentering the object

<!--quoteend-->

> The computer is only the arc of a larger circuit which always includes people and an environment.
> 
> &#x2013; [[Gregory Bateson]]

<!--quoteend-->

> the cybernetic shift from object to message

