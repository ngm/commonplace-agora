# Building a second brain

[Building a second brain](https://www.buildingasecondbrain.com/), from Tiago Forte.

Essentially an [[information strategy]].  A way of capturing, connecting, and creating information and knowledge.

Perhaps more a form of [[personal knowledge management]].  It results in the construction of something akin to a [[personal wiki]].

(What's the difference between personal knowledge management and information strategies?)

-   remember ([[collecting the dots]]?)
-   connect ([[connecting the dots]]?)
-   create

> an external, centralized, digital repository for the things you learn and the resources from which they come.
> 
> &#x2013; [Building a Second Brain: An Overview](https://fortelabs.co/blog/basboverview/)

Thinking like a curator is a kind of [[information strategy]].

[[Progressive summarisation]].

PARA.  Projects, areas, resources, archives.

-   https://fortelabs.co/blog/basboverview/

-   don't just consume information passively - put it to use
-   create smaller, reusable units of work
-   share your work with the world


## Resources


### General


#### [Turn Your Notes App Into a Personal Knowledge Base — Tiago Forte on Building &#x2026;](https://douglastoft.com/turn-your-notes-app-into-a-personal-knowledge-base-tiago-forte-on-building-a-second-brain/)


### Emacs


#### [Implementing A Second Brain in Emacs and Org-Mode - Tasshin Fogleman - Medium](https://medium.com/@tasshin/implementing-a-second-brain-in-emacs-and-org-mode-ef0e44fb7ca5)


#### [Building a Second Brain in Emacs and Org-Mode - Forte Labs](https://praxis.fortelabs.co/building-a-second-brain-in-emacs-and-org-mode-faa20ae06fc/)

