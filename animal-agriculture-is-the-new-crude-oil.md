# Animal Agriculture Is the New Crude Oil

A
: [[podcast]]

URL
: https://novaramedia.com/2022/06/05/animal-agriculture-is-the-new-crude-oil-aaron-bastani-meet-george-monbiot-downstream/

[[animal husbandry]].  [[George Monbiot]].

> Since the 1990s, [[decarbonisation]] has been the primary goal for many environmentalists. But there is another threat to life on Earth, one that could be just as dangerous as runaway [[climate change]]: the farming of animals for human consumption.
> 
> In his new book Regenesis: Feeding the World Without Devouring the Planet, George Monbiot warns that we’re approaching a tipping point in the fight to protect the planet. Can changing what we eat bring us back from the brink? Aaron Bastani speaks Britain’s foremost environmental campaigner about what to do when the worst possible people are in charge at the worst possible time.

Really interesting discussion.  

Talks about the [[global food system]] as a [[complex system]] that is showing signs of possible breakdown.

Biggest difference we can make is to switch to a plant-based diet.  **As consumers**, that is.  But we should be citizens more than consumers, and as citizens actively work towards bringing about a new system.

All the technology we might need already exists.  We just need the political wherewithal to utilise it.

Talks about [[precision fermentation]] again.  A bit more in-depth here.

I think he says that about 40% of land worldwide is used for animal farming.  For only 1% of protein production.  &lt;- fact check that&#x2026;

~00:49:48  Profligacy of animal farming

~00:56:11  Precision fermentation

