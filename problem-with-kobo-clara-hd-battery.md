# Problem with Kobo Clara HD battery

The battery on my [[Kobo]] has been draining **really** fast recently. It won't make it a day or two, when it used to last for weeks.

How long have I had it for?

A rew people out there on the interwebs seem to report similar problems:

-   https://www.reddit.com/r/kobo/comments/smxru9/comment/igl1t8g/
-   [Kobo Battery problem - Page 3 - MobileRead Forums](https://www.mobileread.com/forums/showthread.php?t=342090&page=3)
-   [What am I doing to make my Kobo Clara HD battery drain so fast? - MobileRead &#x2026;](https://www.mobileread.com/forums/showthread.php?t=336487)

At the recommendation of some comments in those threads, I've turned off the sleep cover controls in koreader.  Anecdotally that seems to have ameliorated the problem, with the battery back to lasting for days again.

