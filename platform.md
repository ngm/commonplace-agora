# Platform

> A platform is an online application or website used by individuals or groups to connect to one another or to organize services. 
> 
> &#x2013; [Platform Cooperativism Consortium](https://platform.coop/)

