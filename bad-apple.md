# Bad Apple

URL
: https://badapple.tools/

> A collaboration between Priveasy and the Aaron Swartz Day Police Surveillance Project, Bad Apple provides valuable tools and resources with the aim of **holding law enforcement accountable and putting an end to police misconduct**.

