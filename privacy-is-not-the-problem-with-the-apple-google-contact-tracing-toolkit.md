# Privacy is not the problem with the Apple-Google contact-tracing toolkit

-   https://www.theguardian.com/commentisfree/2020/jul/01/apple-google-contact-tracing-app-tech-giant-digital-rights

Centralised control of computing infrastructure.  I had not thought about this problem before in these terms.  As the article says, Interesting article that makes iyou think about the differences between 

> It is possible to be strongly in favour of a decentralised approach, as I am (as a co-developer of the open-source DP-3T system that Apple and Google adapted), while being seriously **concerned about the centralised control of computing infrastructure these firms have amassed**.

<!--quoteend-->

> This simple view might apply to a company collecting data through an app or a website, such as a supermarket, but doesn’t faithfully capture the source of power of the firms controlling the hardware and software platforms these apps and websites run on.

<!--quoteend-->

> This approach is effectively what underpins the Apple-Google contact-tracing system. It’s great for individual privacy, but the kind of infrastructural power it enables should give us sleepless nights. 

<!--quoteend-->

> In all the global crises, pandemics and social upheavals that may yet come, **those in control of the computers, not those with the largest datasets, have the best visibility and the best – and perhaps the scariest — ability to change the world.**

<!--quoteend-->

> A “right to repair” would stop planned obsolescence in phones, or firms buying up competitors just to cut them off from the cloud they need to run. A “right to interoperate” would force systems from different providers, including online platforms, to talk to each other in real time, allowing people to leave a social network without leaving their friends.

