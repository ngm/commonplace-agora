# Syncing my annotations from koreader

I read a lot of articles and books on [[koreader]] on my kobo, and I annotate interesting passages of them as I'm going through.

Mostly highlights, as it's a bit laborious to type on the kobo touch screen, but sometimes with a note added.

It's really great to be able to take these annotations.  My digital garden is full of quotes taken this way.  But I don't love the story for syncing them off the kobo again.   

Presently I use [[KoHighlights]] which is a desktop app.  It's great, but it'd be less friction if everything would sync automatically.  Because it's manual I tend to not always do it, so I end up with annotations that I never go back to.

Ideally I'd like to be able to automatically sync directly from koreader on the kobo, to somewhere easily accessible, without needing to plug in to a computer. 


## Existing options

There are some current options for this:

-   KoHighlights
-   Evernote sync
-   Joplin sync
-   sync to Dropbox?


### KoHighlights

Like I mentioned, what I use currently.


### Evernote sync

It'd probably be the easiest, but would be nice to avoid a closed platform.

Anyway, according to [this thread](https://www.mobileread.com/forums/showthread.php?t=341736), this is deprecated.  


### Joplin sync

Looks promising, but relies on you having desktop Joplin installed somewhere.  Which if was how I was syncing, to be honest I might as well just stick with KoHighlights. Might try it though, just to see the format that it produces.

If it was good, it could potentially be possible to set it up headless on a remote server.


### Dropbox

You seem to be able to sync something to Dropbox.  https://github.com/koreader/koreader/pull/5591
I don't want to use Dropbox.  But might give it a test, just to see what it does.


## What would be ideal solution for me?

Probably something that would automatically sync my raw highlights to a folder that syncthing could pick up.

I'd personally want those raw highlights in org format.

