# Social Transformation Through 'The Commons'

A
: [[podcast]]

URL 
: 

https://thenextsystem.org/learn/stories/episode-17-social-transformation-through-commons-w-david-bollier

Series
: [[The Next System]]

Featuring
: [[David Bollier]]

Great interview with David Bollier on the [[commons]] (natural, urban, digital), [[commoning]], re-recognising the value in things that sit outside of market economics and exchange value. (Contains the bombshell that apparently 20% of the human genome has already been enclosed and patented. WTF?!)

