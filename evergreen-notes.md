# Evergreen notes

> Evergreen notes are written and organized to evolve, contribute, and accumulate over time, across projects.
> 
> &#x2013; [Evergreen notes](https://notes.andymatuschak.org/Evergreen_notes) 

I first came across this term on Andy Matuschak's notes: 
https://notes.andymatuschak.org/Evergreen_notes.  Andy coined the term, in this context at least.

> Andy Matuschak proposed the term Evergreen Notes to describe a system of note-taking that aspires towards cumulative personal knowledge, rather than simply information capture.
> 
> -   [Growing the Evergreens](https://maggieappleton.com/evergreens)


## Resources

-   [Growing the Evergreens](https://maggieappleton.com/evergreens)

