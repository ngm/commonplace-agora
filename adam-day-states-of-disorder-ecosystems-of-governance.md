# Adam Day, States of Disorder, Ecosystems of Governance

A
: [[podcast]]

URL
: https://newbooksnetwork.com/states-of-disorder-ecosystems-of-governance

"Complexity Theory Applied to UN Statebuilding in the DRC and South Sudan"

Adam Day.

This is a very interesting discussion.  The book critiques the UN's traditional approach of 'state-building', i.e. treating what it deems 'failed states' as simple machines where you can simply remove the bad part and replace it with a good part and all will be well.  The author proposes treating them as complex systems, and ultimately as sites of self-governance, not world-building exercises from the outside.

[[complexity science]].  [[Democratic Republic of Congo]].  [[South Sudan]]

[[There is no such thing as an ungoverned space]]. Where there are people, there is governance. It just may not be in a form that appeals to the sensibilities of Western liberals.

~00:04:15  Gives a nice succinct description of [[complicated vs complex]] systems.

> Pursuing a complexity-driven approach instead helps to avoid unintentional consequences, identifies meaningful points of leverage, and opens the possibility of transforming societies from within.

