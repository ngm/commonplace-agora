# Apple expands the use of recycled materials across its products

URL
: https://www.apple.com/newsroom/2022/04/apple-expands-the-use-of-recycled-materials-across-its-products/

"Company also advances new disassembly technology as part of closed-loop goal"

Says [[Apple]] about themselves.

> “As people around the world join in celebrating Earth Day, we are making real progress in our work to address the climate crisis and to one day make our products without taking anything from the earth,” said Lisa Jackson, Apple’s vice president of Environment, Policy, and Social Initiatives. “Our rapid pace of innovation is already helping our teams use today’s products to build tomorrow’s, and as our global supply chain transitions to clean power, we are charting a path for other companies to follow.” 

<!--quoteend-->

> **Recovering more materials for use in future products helps reduce mining**. From just one metric ton of iPhone components taken apart by Apple’s recycling robots, recyclers can recover the amount of gold and copper companies would typically extract from 2,000 metric tons of mined rock.

Reducing the amount of iPhones produced would also help to reduce mining.

> In addition to charting progress in recycling innovation and material stewardship, Apple’s newly released 2022 Environmental Progress Report highlights the company’s significant work to become carbon neutral across its global supply chain and the life cycle of every product, as well as progress reducing waste and promoting the safer use of materials in its products. 

[[Apple 2022 Environmental Progress Report]].

