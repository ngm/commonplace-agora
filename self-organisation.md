# self-organisation

> By confronting a common enemy in neoliberalism, environment and society are united in a common cause.  The principle of self-organisation must be salvaged from the laissez-faire perversion, and restored in a more authentic form: reversing the erosion of commons, and instead expanding them, in their role as stewards of nature.
> 
> &#x2013; [[What is wrong with a system of laissez-faire economics?]]

