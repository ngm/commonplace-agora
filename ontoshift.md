# OntoShift

> So to truly understand the dynamics of the commons, one must first escape the onto-political framework of the modern West. One must make what we call an “OntoShift” — a recognition that relational cat- egories of thought and experience are primary.
> 
> &#x2013; [[Free, Fair and Alive]]

