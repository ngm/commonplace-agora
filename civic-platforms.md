# civic platforms

> There are two traditions that offer important intellectual resources for civic platforms at the level of the firm and the city: [[platform co-operativism]] and the ‘[[new municipalist]]’ movement. Each could play a key role in the political transformation towards platform socialism.
> 
> &#x2013; [[Platform socialism]]

Page 218 <span class="timestamp-wrapper"><span class="timestamp">[2022-12-16 Fri 07:16]</span></span>:
Platform co-operatives are digital platforms owned and controlled by the people who participate in them.

Page 220 <span class="timestamp-wrapper"><span class="timestamp">[2022-12-16 Fri 07:18]</span></span>:
Developing alternatives from the local to the international level will ensure a rich diversity of organisational forms that are able to solve problems at different points in the system.

