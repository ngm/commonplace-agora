# How personal is a personal wiki?

At the beginning, I'm keeping it pretty academic.  I'm not putting anything about my personal life, or more personal thoughts, in here.  Mainly just as I'm hesitant to share that stuff.  But it's a personal wiki, should it have personal thoughts?  I guess somewhere you have to make the distinction between personal thoughts and public and private thoughts.

Stuff that will never get published, but benefits from collecting and connecting the dots.

Ideally I'd like to have some parts of my wiki private/encrypted, so everything could be interlinked, but only what I choose to share is ever shared when I publish. I need [[gevulot]].


## Private wiki

The simplest solution is probably to just separate public/private thoughts out into different wikis. I am currently just using a separate [[org-roam]] private wiki. 

[[Bill Seitz]] has lots to say about private wikis. [Private Wiki - WebSeitz/wiki](http://webseitz.fluxent.com/wiki/PrivateWiki) 

