# techno-fixes

> technological solutions which don’t challenge capitalist social relations
> 
> &#x2013; [[Mish-Mash Ecologism]]

-   [Technofixes: a critical guide to climate change technologies - Corporate Watch](https://corporatewatch.org/product/technofixes-a-critical-guide-to-climate-change-technologies/)

