# Omicron "will ultimately find just about everybody" in terms of exposure

So sayeth [[Anthony Fauci]].

[[Coronavirus]] [[Omicron]]

[‘I have no intention of getting infected’: understanding Omicron’s severity |&#x2026;](https://www.theguardian.com/world/2022/jan/16/no-intention-of-getting-infected-understanding-omicrons-severity)


## But

-   [[Vaccines make an important difference in who develops the illness]]

