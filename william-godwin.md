# William Godwin

> whose utopian writings provoked Malthus to write his Essay on Population. Godwin, who dabbled in vegetarianism, knew herbivorous radicals [[Robert Owen]] and [[Percy Shelley]]
> 
> &#x2013; [[Half-Earth Socialism]]

