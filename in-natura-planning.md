# in natura planning

> Less than a decade later, innovations in mathematics – especially [[linear programming]] – provided tools that would aid in natura planning

<!--quoteend-->

> Nonetheless, it would be Lange and Lerner’s ‘[[market socialism]]’ that would become dominant in socialist thought, and [[Neurath]]’s in natura planning would be largely forgotten

<!--quoteend-->

> Calculation in kind would value each commodity based only on its [[use value]], for purposes of economic accounting. By contrast, in money-based economies, a commodity's value includes an [[exchange value]]. 
> 
> &#x2013; [Calculation in kind - Wikipedia](https://en.wikipedia.org/wiki/Calculation_in_kind)

