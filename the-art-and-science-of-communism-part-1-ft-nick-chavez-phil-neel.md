# The Art and Science of Communism, Part 1 (ft. Nick Chavez, Phil Neel)

A
: [[podcast]]

Found at
: https://soundcloud.com/thismachinekillspod/317-the-art-and-science-of-communism-part-1-ft-nick-chavez-phil-neel

Great discussion. Based around [[Forest and Factory]].  Loads of good stuff.

Their insistence on starting from present conditions and working towards for me thinking about [[complex systems]] and [[chaos theory]], [[sensitive dependence on initial conditions]] in particular. Is it logical to try and completely map the present to then try and cause the future? Maybe.

Maybe an alternative **is** the utopian way of doing it. Think of elements of your desired future as attractors of sorts, then focus on how your can leverage the path of history towards those. Maybe that's a combination of both. It obviously can't hurt to know the present conditions, but to then assume you can trace a clear path from now to the future seems wrong.

Yeah I think you need both. A clear understanding of present conditions. A clear idea of how you want society to function - your attractors. And then you nudge it from A to B, making use of [[shocks]], [[leverage points]], etc.

They make the point that a lot of utopias focus on reproduction rather than production. ([[Superstructure]] rather than [[base]]?).

Description of working in some distribution warehouse was pretty wild.

They dislike [[The Ministry for the Future]], for various reasons they say, but the main one discussed being that the protagonists are mostly from a liberal environmentalist class, and the working class are faceless characters.

