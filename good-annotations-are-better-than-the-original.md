# Good annotations are better than the original

> just as "the map is not the territory" is backwards (good maps are better than naked-senses sensing the territory), good annotations are better than the original (structure, link graphs, annotation graphs, summaries, analyses, corrections)
> 
> &#x2013; SJ [on Matrix](https://matrix.to/#/!WhilafaLxfJNoigHCj:matrix.org/$vOvclBvBNxvrJzpn003nH7CArKfUriCN8pTF9I3nvpg?via=matrix.org&via=t2bot.io&via=fairydust.space)

[[I Can't Believe It's Not Science!]]

