# systems mapping

> Broadly speaking, systems mapping is the creation of visual depictions of a system, such as its relationships and feedback loops, actors and trends. Systems mapping is intended to provide a simplified conceptual understanding of a [[complex system]] that, for collective action purposes, can get partners on the same page
> 
> &#x2013; [Systems Mapping: A Vital Ingredient for Successful Partnerships - RMI](https://rmi.org/systems-mapping-a-vital-ingredient-for-successful-partnerships/) 

[[Systems thinking]].

