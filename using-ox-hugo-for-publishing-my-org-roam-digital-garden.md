# using ox-hugo for publishing my org-roam digital garden

https://ox-hugo.scripter.co/

Seems like a pretty long-lived and stable option for org-mode publishing.


## Issues


### Prefers one post per org subtree

Fair enough, but that's not how org-roam works.  Hopefully having one post per org file doesn't equate to being a second class citizen in ox-hugo.


### hugo frontmatter properties

Turns out that that is a problem.  You seem to need to manage various `#+hugo_` properties per file.  That's fine if you have one file.  Not if you've got over 4000 files.

That said, it has some org-roam files in its test site, and these don't seem to have hugo front matter in them.  Is it done with dir-locals?  https://github.com/kaushalmodi/ox-hugo/tree/main/test/site/content-org/org-roam

Maybe not: https://github.com/kaushalmodi/ox-hugo/issues/101

Huh.  Should give it a whirl to see for myself, but not now.


## Resources

-   [My org-roam workflows for taking notes and writing articles — Dominik Honnef](https://honnef.co/articles/my-org-roam-workflows-for-taking-notes-and-writing-articles/)

