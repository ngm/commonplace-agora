# Climate change

> [[G20]] governments, comprising the world’s biggest economies and including developed and developing countries, are responsible for about 80% of global greenhouse gas emissions, and about 85% of GDP.

<!--quoteend-->

> Since 1988, when human-induced climate change was officially recognized by the establishment of the Intergovernmental Panel on Climate Change ([[IPCC]])
> 
> &#x2013; [[Revolution or Ruin]]


## The response


### Potential social formations

The book [[Climate Leviathan]] gives a nice exposition of four types of climate crisis response, if you class them on the axes of capitalist vs non-capitalist and pro- or anti- planetary sovereignty.  (I don't fully grasp the idea of planetary sovereignty just yet).


### Cost

> The most comprehensive analysis of the path to net zero was published by the [[Climate Change Committee]] (CCC), the UK government’s statutory adviser, which has repeatedly said the costs of action are small and diminishing, at less than 1% of GDP by 2050, while the costs of inaction are large and rising.
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

<!--quoteend-->

> Dr Ajay Gambhir, a senior research fellow at the Grantham Institute – Climate Change and the Environment, said that as innovation accelerated, even the CCC’s costings were probably an overestimate and did not take account of the many co-benefits of climate action, including cleaner air and water, more [[biodiversity]], millions of well-paid [[green jobs]] and lower [[energy bills]].
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

