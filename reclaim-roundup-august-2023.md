# Reclaim Roundup: August 2023

Welcome!

This is the third in a series of regular monthly updates exploring the intersection of ecosocialism and ICT.   In it I look at the problems of [[digital capitalism]] and positive actions we can support now that embody [[digital ecosocialism]].  I also explore readings and thoughts on the theoretical side of things, particularly with an eye on transition: how we get from (digital) capitalism to (digital) ecosocialism.

So Reclaim is doing a little bit of research in to the systemic leverage points that ICT could have to transition to ecosocialism; identifying actually existing initiatives working in that direction; and thinking about the link between the two.

> Philosophers have only interpreted the world, in various ways; the point, however, is to change it.
> 
> &#x2013; Karl Marx


## In theory

Here I report on some explorations around revolutionary theory, through a lens as to how it might relate to ICT.  How can we transition ICT from digital capitalism to digital ecosocialism?  And how can ICT play a part in a wholesale transition to an ecosocialist society?


### Idenfiying points of leverage

One of the things I looked at in my OU work was 'leverage points'.  The intention being to identify what point of leverage different initiatives make use of, ultimately to help ICT workers find the initiatives that they are best placed to work on.

I referred to both 'types of action' as well as 'leverage points'.  Types of action being resist, regulate and recode, taken from Platform Socialism.  And leverage points being specifically those referenced in Digitalization in the Anthropocene - rules and feedback, structures, goal setting and mindset shifts.  These were cherry-picked from Donella Meadows' work.   I need to delve further into these.

So I enjoyed reading the article [[It's not about your footprint, it's about your point of leverage]]. I like this idea.  I've been aware for a while that [[carbon footprint]] isn't a great measure.  This article reiterates that, and suggests that its better to think about your [[leverage points]] instead.  Which obviously resonates with idea of leverage points in [[systems thinking]].  It breaks leverage points into individual, organisational, and country levels.

> In any individual sector, we can identify leverage points: actions that are relatively low cost or low difficulty but that have a high impact in accelerating the transition.

The same authors also talk about [[super-leverage points]]:

> In the whole system, we can identify super-leverage points: we define these as actions that are high leverage in the sectors where they are taken, and that influence transitions in other sectors in a way that is positive in direction, high in impact, and reasonably high in probability.

It would be interesting to contemplate what are the super-leverage points in the ICT sector.  Which are the high leverage actions within ICT, that can have a positive, impactful, and probable influence on the entire system.  [[Leveraging Digital Disruptions for a Climate-Safe and Equitable World: The D<sup>2S</sup> Agenda]] may be one place to start looking at that.

[[A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]] looks interesting as a methodology for surfacing leverage points within a sector. Would be interesting to see how it might be applied to ICT.


## What's the problem?

Now, closer to the ground: some problems from digital capitalism recently in the news.  To help map them out, I'm tagging them with some of the criteria I defined in my OU research ([[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]]).

-   &#x2026;
-   Something about Threads?


## And now, take action

And in response to the problems of digital capitalism, here's some latest news on concrete actions that are part of an ecosocialist ICT movement.

-   [[Extreme heat prompts first-ever Amazon delivery driver strike]].  (A [[strike action]], supporting [[social equity]] and [[agency]], to resist [[worker exploitation]]).


## Inputs

Finally, a few other things I've been reading, listening to, and watching in the last month or so that are adjacent to the topics of ecosocialism and ICT.


### Listening

-   [[Class Politics in a Warming World with Keir Milburn]]

