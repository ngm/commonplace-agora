# Google Pixel 5a

I got one in September 2023.


## Purchasing decision

Mostly researched on [[sustaphones]] and some review sites.  Bought it second-hand refurbished from [[Backmarket]].

I got it because:

-   Excellent support for custom ROMs (https://www.sustaphones.com/#barbet).
-   6/10 repairability from iFixit (https://www.ifixit.com/Device/Google_Pixel_5a).
-   Battery replacement is 'moderate' difficulty.  Meh.  At least it's not 'hard' (https://www.ifixit.com/Guide/Google+Pixel+5a+Battery+Replacement/148930).
-   Screen replacement is 'moderate' difficulty too.  Meh. Often the case. (https://www.ifixit.com/Guide/Google+Pixel+5a+Screen+Replacement/148892)
-   Spare parts provided by [[iFixit]] (https://www.ifixit.com/Parts/Google_Phone)
-   It has a headphone jack.
-   Reasonable-ish price: £170.  I've always tried to go sub-£100 before


## Initial thoughts

-   I don't like the form factor.  Stupidly long and thin, hard to use with one-hand (unless you turn on the stupid one-hand mode).
-   Nice screen.
-   Sooooooooooo. Muuuuuuuuuuch. Fasssssster than my old phone.
-   Battery life is fantastic so far, compared to previous phone.
-   Camera: a lot better.


## Alternatives

I would have like a Teracube 2e or a Fairphone, but both a bit pricey, and seems hard to find them second-hand.

