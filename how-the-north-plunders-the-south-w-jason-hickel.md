# How the North Plunders the South w/ Jason Hickel

A
: [[podcast]]

Found at
: https://www.patreon.com/posts/how-north-south-102977968

Part of
: [[Upstream]]

Featuring
: [[Jason Hickel]]

[[Imperialism]] and [[neocolonialism]].

[[Unequal exchange]].

Some of the discussion around counties reclaiming agency over their trade overlaps with the material on the [[digital trade]] agenda in the [[Digital Capitalism online course]].

