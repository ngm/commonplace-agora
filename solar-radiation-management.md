# solar radiation management

AKA solar radiation modification or solar geoengineering.

Basically reflecting sunlight back to space before it hits the Earth.

> Neoliberals willingly gamble with something as risky as SRM rather than countenance restrictions on their revered market.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> untested technologies like SRM that would create new problems we can ill afford
> 
> &#x2013; [[Half-Earth Socialism]]

