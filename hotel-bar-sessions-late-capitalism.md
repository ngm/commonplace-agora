# Hotel Bar Sessions: Late Capitalism

A
: [[podcast]]

Lives at
: https://hotelbarpodcast.com/podcast/episode-89-late-capitalism-2/

Part of
: [[Hotel Bar Sessions]]

They discuss [[late capitalism]].

[[Capital is Dead]] gets a mention, as well as Jodi Dean's [[neofeudalism]].

