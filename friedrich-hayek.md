# Friedrich Hayek

> These questions are similar to those posed by Friedrich Hayek about the economy in the 1930s and 1940s, in his effort to re-found liberalism as neoliberalism. By arguing that humanity could never comprehend the market, he endowed the economic sphere with ecological attributes – it appeared as a natural self-organizing system.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> According to Hayekian logic, the only solution to any market failure is more markets
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Hayek, who came from a family of biologists, saw the market as a ‘highly complicated organism’. This approach contrasted with that of neoclassical economists, who used Newtonian physics as their point of reference, as if the economy were a billiards game whose outcome could be predicted
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Friedrich A. Hayek (1899-1992) is known for his vision of the market economy as an information processing system characterized by spontaneous order: the emergence of coherence through the independent actions of large numbers of individuals, each with limited and local knowledge, coordinated by prices that arise from decentralized processes of competition. 
> 
> &#x2013; [Retrospectives: Friedrich Hayek and the Market Algorithm - American Economic &#x2026;](https://www.aeaweb.org/articles?id=10.1257/jep.31.3.215)

<!--quoteend-->

> Hayek argued that there would be three types of information a central planning board would require but find impossible to marshal
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> First, a very granular and constantly updated inventory of all production goods, their state of repair (e.g. remaining useful lifetime of a machine) and location
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Second, mathematical functions (think: lookup tables) summarizing the quantities of each input (e.g. X kilograms of steel tubing, Y drill presses, Z rubber tires, etc.) required to produce any quantity of a given commodity (e.g. N bicycles)
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Finally, “complete lists of the different quantities of all commodities which would be bought at any possible combination of prices of the different commodities which might be available” (i.e. an aggregate demand function) would have to be settled upon in order to begin calculating how best to make use of the aforementioned productive resources
> 
> &#x2013; [[Review: People's Republic of Walmart]]

[[Great Society]].

> But perhaps most egregiously, Hayek’s system ignores the power structures that exist between different economic actors and the vast inequalities in wealth and capital that determine who can make important decisions in a market economy.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> major weakness in the modern Austrian School’s emphasis on the need for tacit knowledge to be socially mobilised by entrepreneurs participating in the market process is that participation is restricted to those with access to capital, thus ignoring the tacit knowledge of the majority of people
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Another problematic aspect of Hayek’s model of market-based coordination is that it is entirely non-discursive, leaving no room for rational deliberation, compromise and mutually beneficial agreements.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Hayek’s writings were so influential that by the end of the century even many ‘market socialists’ – as they now called themselves – framed their arguments in terms of his insights into price signals and market systems.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Hayek’s argument against central planning in his book, [[The Road to Serfdom]], was that despite the good intentions of planners, a system involving a central planning agency would inevitably lead to the centralisation of power and decision-making, destroying individual freedom and democracy in the process
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> His fear was that would-be social democratic planners in western Europe would turn their countries into totalitarian dictatorships through submitting all individuals to the authority of a central planning board in the service of abstract notions of the common good and social justice
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> the cornerstone of his critique of socialism can be found in his earlier economic arguments about the superiority of markets over central planning in coordinating the economic activity of different individuals with local and often tacit knowledge of their own unique situations.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> He claimed that no planning agency could ever gather all the necessary information required to make rational economic decisions because a well-functioning economy depended on individual actors continually making on-the-spot judgements based on evolving market factors
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Hayek framed this aspect of his argument in epistemological rather than political terms. The real problem with planning, he insisted, was the dispersion of local knowledge throughout society. Markets proved highly advantageous because they enabled participants to coordinate their actions and respond to surpluses and shortages by following the signals of the price system without ever having full knowledge of the entire situation.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Hayek’s emphasis on the ‘spontaneous order’ of the market challenged the modernist faith many Marxists had in the capacity for human beings to consciously and rationally organise human society to meet basic needs.
> 
> &#x2013; [[Platform socialism]]

