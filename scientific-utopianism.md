# Scientific utopianism

"rigorous daydreaming" ([[Drew Pendergrass]], [[Debating Eco-Socialist Futures]])

Coined by [[Otto Neurath]] (I think).

> For him, ‘utopia’ did not refer to ‘impossible happenings’ but a ‘thoughtful order of life’ that did not yet exist. Whereas utopians from Thomas More to Edward Bellamy had pursued ‘dreamers’ ’ work – a worthy task – Neurath believed that by the early twentieth century such theorizing necessarily transformed into ‘scientific work preparing the shaping of the future’.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Utopias were not so different from the ‘constructions of engineers’, akin to detailed blueprints for an imagined society, and thus the utopians could justifiably be called ‘social engineers’.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> These social engineers would have to know about everything from the ‘psychological qualities of men, to their love of novelty, their ambition, attachment to tradition, willfulness, [and] stupidity’, as well as the ‘natural base, land and sea, raw materials and climate’ upon which society depends. A utopian’s aims might even include ‘nonhuman ideals'.  Although what Neurath had in mind included ‘the greatness of God’ and ‘the nation’, expanding the conception of the good beyond humanity allows planning to incorporate ecological goals.
> 
> &#x2013; [[Half-Earth Socialism]]

