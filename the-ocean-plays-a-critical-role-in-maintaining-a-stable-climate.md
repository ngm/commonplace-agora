# The ocean plays a critical role in maintaining a stable climate

[[Oceans]] play a critical role in maintaining a [[stable climate]].

> Oceans are critical to understanding climate change. They cover about 70% of the planet’s surface and absorb more than 90% of global warming heat,
> 
> &#x2013; [Extreme heat in oceans ‘passed point of no return’ in 2014 | Oceans | The Gua&#x2026;](https://www.theguardian.com/environment/2022/feb/01/extreme-heat-oceans-passed-point-of-no-return-high-temperatures-wildlife-seas?CMP=Share_AndroidApp_Other)

