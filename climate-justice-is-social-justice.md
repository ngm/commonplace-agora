# climate justice is social justice

[[climate justice]] is [[social justice]]

> Campaign groups have already recognised the potential for linking the fight over the cost of living with the fight for serious action to tackle climate change.
> 
> &#x2013; [[End of the World, End of the Month - One Fight]]

