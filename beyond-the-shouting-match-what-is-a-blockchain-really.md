# Beyond the shouting match: what is a blockchain, really?

URL
: https://dustycloud.org/blog/what-is-a-blockchain-really/
    
    [[Blockchain]].

> blockchains are decentralized centralization

<!--quoteend-->

> So again a blockchain is just a single, abstract, sequential machine, run by multiple parties. That's it. It's more general than cryptocurrencies, and it's not exclusive to implementing them either. One is a kind of programming-plus-cryptography-use-case (cryptocurrencies), the other one is a kind of abstracted machine (blockchains).

