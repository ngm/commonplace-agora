# Wonder Woman

Meh.  Enjoyable enough but not as good as all the reviews seemed to suggest.

A mix of Greek mythology and ww1.

Lots of movie tropes.

-   Ragtag crew (an American, a Scotsman, an Arab, a Native American, an ancient demi-God)
-   Seems kinda misogynistic.
-   Pretty tired representation to have the evil woman be physicall disfigured.

At least the idea that there's just one person responsible for war is  challenged.

Apparently "the first ever superhero film with both a female protagonist and director".  Kudos for that.

