# Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind

[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind](https://www.versobooks.com/blogs/4370-frantz-fanon-against-facebook-how-to-decolonize-your-digital-mind)

Lizzie O'Shea discusses [[digital self-determination]] as a means to understand and resist some of the problems with big tech, using the rubric of Fanon's work on self-determination.  How can we have agency and create our own identity under the thumb of the big surveilling platforms?

Digital self-determination will involve: 

-   making use of the technical tools available to communicate freely
-   designing information infrastructure in ways that favour de-centralisation
-   designing online spaces and devices that are welcoming

I definitely like all the conclusions.  At first blush, any comparison between colonialism and racism and the problems of digital platforms feels like it could be a little crass&#x2026; but O'Shea explains her thinking and says she feels Fanon's ideas are so strong that they can be applied to different times and situations.

> "From the Algeria to algorithms, Lizzie O'Shea argues that Frantz Fanon’s ideas have much to offer us as we seek to **understand, and resist, some of the most profound challenges of living in the digital age**."

<!--quoteend-->

> [[Frantz Fanon]], the writer and psychiatrist born in the French colony of Martinique, is perhaps best known for his contribution to our understanding of race and colonialism

<!--quoteend-->

> Fanon’s ideas around **self-determination** have much to offer us as we seek to understand, and resist, some of the most profound personal challenges of living in the digital age.

<!--quoteend-->

> Fanon provided an explanation of how colonialism works – how **both the colonizer and the person being colonized take on roles and practices that make colonialism seem inevitable** and unbeatable.

<!--quoteend-->

> These arguments continue to find applicability after his death because, I argue, the questions Fanon grappled with continually re-present themselves, namely: **how do we create ourselves in a world in which our identity is pre-determined for us?**

<!--quoteend-->

> Those that hold power over our digital lives, from private industry and government, do so in a way that is almost hegemonic

<!--quoteend-->

> There was no agency in how his identity was determined; no way to escape the judgments about him; no sense that he possessed autonomy.

<!--quoteend-->

> **Digital self-determination will involve making use of the technical tools available to communicate freely**.

<!--quoteend-->

> But it will also require that we enjoy autonomy from manipulation by the vast industry of data miners and advertisers, which tries to shape our identity around consumption.

<!--quoteend-->

> We need to change legal structures around data and its use, and **design information infrastructure in ways that favour de-centralisation, rather than centralisation in the hands of government and corporations**. 

<!--quoteend-->

> **Digital self-determination must also be about designing online spaces and devices that are welcoming**, and give us control over our participation rather than creating a dynamic of addiction. 

<!--quoteend-->

> Even in a technologically-saturated world, in which human beings are categorised, surveilled and discriminated against, it is possible for us to carve out space for our own identity and shape our destiny. 

