# Digital Tech Deal

An [[ecosocialist]] programme for digital technologies.

Proposed by Michael Kwet ([[Digital Ecosocialism: Breaking the power of Big Tech]]).

> To this effect, I propose an ecosocialist Digital Tech Deal which embodies the intersecting values of anti-imperialism, environmental sustainability, social justice for marginalized communities, worker empowerment, democratic control and class abolition.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

-   [[Anti-imperialism]]
-   [[sustainability]]
-   [[social justice]]
-   [[worker empowerment]]
-   [[democratic control]]
-   [[class abolition]]

> It draws on proposals for transformation as well as existing models that can be scaled up, and seeks to integrate those with other movements pushing for alternatives to capitalism, in particular the degrowth movement.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]


## Ensure the digital economy falls within social and planetary boundaries

> We would need to establish a scientifically-informed limit on the amount and types of materials that can be used and decisions could be made about which material resources (e.g. biomass, minerals, fossil energy carriers, metal ores) should be devoted to which use (e.g. new buildings, roads, electronics, etc.) in which amounts for which people. Ecological debts could be established which mandate redistributive policies from North to South, rich to poor.


## Phase out intellectual property

[[We should phase out intellectual property]].

> [[Intellectual property]], especially in the form of copyrights and patents, give corporations control over knowledge, culture and the code that determines how apps and services work, allowing them to maximize user engagement, privatize innovation and extract data and rents.

<!--quoteend-->

> Economist Dean Baker estimates that intellectual property rents cost consumers an additional $1 trillion per year compared to what could be obtained on a “free market” without patents or copyright monopolies.

<!--quoteend-->

> Phasing out intellectual property in favor of a commons-based model of sharing knowledge would reduce prices, widen access to and enhance education for all and function as a form of wealth redistribution and reparations to the Global South.


## Socialize physical infrastructure

Similarly [[Internet for the People]],  [[governable stacks]].

[[We should socialise ICT infrastructure]].

> Physical infrastructure such as cloud server farms, wireless cell towers, fiber optic networks and transoceanic submarine cables benefit those who own it. There are initiatives for community-run internet service providers and wireless mesh networks which can help place these services into the hands of communities. Some infrastructure, such as submarine cables, could be maintained by an international consortium that builds and maintains it at cost for the public good rather than profit.


## Replace private investment of production with public subsidies and production.

[[public-commons partnerships]].

> Dan Hind’s [[British Digital Cooperative]] is perhaps the most detailed proposal for how a socialist model of production could work in the present context.

<!--quoteend-->

> Enhanced by open data, transparent algorithms, open-source software and platforms and enacted through democratic participatory planning, such a transformation would facilitate investment, development and maintenance of the digital ecosystem and broader economy.

<!--quoteend-->

> While Hind envisions rolling this out as a public option within a single country — competing with the private sector — it could instead provide a preliminary basis for the complete socialization of tech. In addition, it could be expanded to include a global justice framework that provides infrastructure as reparations to the Global South, similar to the way climate justice initiatives pressure rich countries to help the Global South replace fossil fuels with green energy.


## Decentralize the internet

> Other projects like Solid allow people to host their data in “pods” they control. App providers, social media networks and other services can then access the data on terms acceptable to users, who retain control over their data. These models could be scaled up to help decentralize the internet on a socialist basis.

or [[Indieweb]].


## Socialize the platforms

[[We should socialise the platforms]].

> Services that cannot simply interoperate could be socialized and operated at cost for the public good rather than for profit and growth.


## Socialize digital intelligence and data

> Socialization of data would instead embed values and practices of privacy, security, transparency and democratic decision-making in how data is collected, stored and used. It could build on models such as Project [[DECODE]] in Barcelona and Amsterdam.


## Ban forced advertising and platform consumerism

> Digital advertising pushes a constant stream of corporate propaganda designed to manipulate the public and stimulate consumption.


## Replace military, police, prisons and national security apparatuses with community-driven safety and security services


## End the digital divide

> The [[digital divide]] typically refers to unequal individual access to digital resources like computer devices and data, but it should also encompass the way digital infrastructure, such as cloud server farms and high-tech research facilities, are owned and dominated by wealthy countries and their corporations.

