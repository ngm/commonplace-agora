# net neutrality

> During the FCC’s regulatory push under Obama, when the agency had classified broadband providers as common carriers, it enforced the principle of “net neutrality”: the axiom that ISPs should treat all kinds of data the same way.
> 
> &#x2013; [[Internet for the People]]

