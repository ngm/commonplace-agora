# Participatory planning

maybe merge with see [[Participatory economics]], [[Participatory budgeting]]?

> we arrive at a broader notion of participatory planning, a system through which different associations engage in negotiated coordination as to how resources could be most effectively allocated between them.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Participatory planning today can also take advantage of the mass participatory tools enabled through digital technology that could help facilitate this process and make coordination easier. Evgeny Morozov has written about the technological possibilities of today’s ‘feedback infrastructure’, which make it possible to bring different parties together to help distribute information about economic needs through non-market forms of social coordination.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Responding to Hayek’s theory about markets as the best discovery mechanism, Morozov proposes a form of ‘solidarity as a discovery mechanism’ through the design of new social institutions that could coordinate economic activity more effectively than the market. Digital platforms make it easier than ever before to examine new ways of distributing resources through democratic methods
> 
> &#x2013; [[Platform socialism]]

