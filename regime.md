# Regime

> Regimes (again used in a special sense) refer to non-compulsory structures which serve an accepted goal and operate through reciprocity and mutual benefit.
> 
> &#x2013; [[The Entropy of Capitalism]]

