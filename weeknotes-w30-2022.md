# Weeknotes W30 2022



## Sunday

-   [[Energy storage]] and [[grid balancing]].

-   [[Associational socialism]]

-   The [[Iceberg Model]]

-   The [[water cycle]] is a complex system.


## Monday

-   [[Stokes Croft Land Trust]]
-   [[Community land trusts]]


## Tuesday

-   [[An Essay on Liberation]]


## Thursday

-   Read: [[What Does An Ecological Civilization Look Like?]]


## Friday

-   Listened: [[Cuba's Life Task]]

-   [[Conversations with Gamechangers]] is pretty awesome lineup.


## Saturday

-   Did the [[parkrun]].

-   Read: [[David Bollier, P2P Models interview on digital commons]]

-   Read: [[A Syrian democratic social economy in the making]]
    -   [[Cooperatives in Rojava]].  [[Aborîya Jin]].

