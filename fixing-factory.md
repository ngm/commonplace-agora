# Fixing Factory



## Goals

> An obvious goal is to make electronics last longer, to prevent unnecessary [[e-waste]] and the huge amount of [[carbon emissions]] involved in the production and transport of new devices. 
> 
> [[Fixing Factories: And we’re off]]

Environmental.

> The pandemic helped make clear the importance of [[digital inclusion]]: for school children as well as plenty of others. But the problems are far from solved, as asylum seekers, job seekers and many others still lack access. The rescued and refurbished laptops will help these people gain life-changing digital access. 
> 
> [[Fixing Factories: And we’re off]]

Social.

> We also want to get more people thinking about repair and reuse of electronics. As well as boosting awareness that [[laptop repair]] or donation is an option, we’re also keen to engage young people, who could benefit from learning the skills involved in laptop repair. We’re working with local partners to find out what young people want, we’re offering a training course with partners Mer-It, and people will be able to learn through volunteering. 

Education.  [[Green jobs]].

> And we want to create a blueprint for Fixing Factories around the country. Fixing Factories in waste facilities will work very differently to factories run on a highstreet. Our pilot project will work out the best ways to run reuse and repair centres in both locations. We hope to be able to scale up our learnings, leading to fixing factories popping up across the UK (and hopefully beyond!) in years to come.  

