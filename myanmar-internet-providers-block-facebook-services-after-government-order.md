# Myanmar internet providers block Facebook services after government order

URL
: https://www.reuters.com/article/us-myanmar-politics-facebook-idUSKBN2A32ZE

Year
: [[2021]]

[[Myanmar]].

> Half of Myanmar’s 53 million people use Facebook, which for many is synonymous with the internet.

<!--quoteend-->

> “Currently the people who are troubling the country’s stability &#x2026; are spreading fake news and misinformation and causing misunderstanding among people by using Facebook,” the Ministry letter said.

