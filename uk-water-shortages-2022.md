# UK water shortages 2022

In 2022, the UK is experiencing [[drought]]s and [[water shortage]]s.

There has been low rainfall.  [[Water privatisation]] in the 1980s has led to a lack of investment in the infrastructure of the [[water supply system]] (e.g. reservoirs and pipes), meaning that we don't collect enough and we leak away a huge amount.


## Why?


### Lack of rainfall

> “Whilst it is not unusual to have periods of low rainfall, we have seen an **extended period of below average rainfall**, particularly in the south-east of England where it was the **third driest November to July on record (from 1836)**. Far from being relieved, the dry conditions intensified in July, with less than 10% of the usual July rainfall recorded across much of the south-east of England (Anglian, Thames and Southern regions each saw their driest July on record, from 1836). The situation has continued into August, with south-east England receiving no rainfall so far this month.”
> 
> &#x2013; [UK braced for drought conditions to last until October | UK weather | The Gua&#x2026;](https://www.theguardian.com/uk-news/2022/aug/09/uk-braced-for-drought-conditions-in-october) 


### Lack of investment in infrastructure

> No substantial reservoir has been constructed in England since the Kielder Water dam was built in 1981.  Thus, our capacity to store fresh water has remained static as demand for it has risen steadily with the growth of the population.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]


#### Inability to collect water

> An early impact of climate change has meant that the period 2011-2020 was 9% wetter, in terms of rainfall, than the period 1961-1990. So there is no shortage of water we can collect. Unfortunately, it seems we just lack the urge to do so.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]


#### Inability to store water

3 billion litres lost **daily**

> For the whole of England and Wales, the daily loss – from leaks and other losses – from all of the two nations’ main water companies is **3bn litres of water**, a fifth of their total supply.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]


## Root cause

[[Water privatisation]], essentially.

> Even worse, analyses indicate that, while water bills in England have risen noticeably over the past three decades, spending on improved infrastructure by water companies has, at best, flatlined or declined, depending on how you break down the figures. Hence the intensity of our current drought, it is argued.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]

<!--quoteend-->

> At the same time, huge dividends have been paid out to water company shareholders while their chief executives have been generously rewarded for their work.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]

<!--quoteend-->

> These company chiefs have been the main beneficiaries of the privatisation of England’s water companies, which was imposed by the Conservative government in 1989.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]


## Side effects


### Affecting groundwater sources

> First, it has forced us to pump ever more water from groundwater sources. This has stressed aquifers, lowered the water table in many areas and threatened chalk streams. This last hazard is particularly vexing because chalk streams are some of the planet’s rarest habitats – and the vast majority are in England.
> 
> &#x2013; [[The Observer view on the woeful state of England's water industry]]


### Hosepipe bans

Water companies are asking citizens to reduce their water usage to alleviate the water shortages.

