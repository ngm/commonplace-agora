# Taxonomy of interoperability

By [[Cory Doctorow]].

Voluntary interoperability. Often achieved through a standard.

Indifferent interoperability. Don't get in your way, but don't go out of your way to help, either.

[[Adversarial interoperability]].  The guerilla warfare of interoperability.  Competitive compatibility ("comcom")

