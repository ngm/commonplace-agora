# cap-and-trade

> Cap-and-trade creates a fungible right to inflict environmental harm, and the market rather than scientific expertise or democratic opinion decides who exercises such rights
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Of their many different approaches, cap-and-trade is the neoliberals’ most sophisticated and widespread environmental policy, one that has been applied to everything from dredging cold-water sponges, mercury pollution, acid rain, and carbon emissions
> 
> [[Half-Earth Socialism]]

