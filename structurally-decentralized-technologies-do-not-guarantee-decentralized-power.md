# Structurally decentralized technologies do not guarantee decentralized power

So sayeth [[Nathan Schneider]] in [[Nathan Schneider, Pt. 2 (Blockchain Governance)]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

