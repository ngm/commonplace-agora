# To Posterity - Bertolt Brecht

First heard it mentioned on [Novara FM interview with Peter Frase](https://soundcloud.com/novaramedia/novarafm-peter-frase-life-after-capitalism).  They specifically mentioned the lines: "Alas, we / Who wished to lay the foundations of kindness / Could not ourselves be kind."  Frase's positive reading on it being, remember that we must always try to keep our kindness, even when the actions required for a revolution might lead us astray from it.

<p class="verse">
**To Posterity**<br />
<br />
1.<br />
<br />
Indeed I live in the dark ages!<br />
A guileless word is an absurdity. A smooth forehead betokens<br />
A hard heart. He who laughs<br />
Has not yet heard<br />
The terrible tidings.<br />
<br />
Ah, what an age it is<br />
When to speak of trees is almost a crime<br />
For it is a kind of silence about injustice!<br />
And he who walks calmly across the street,<br />
Is he not out of reach of his friends<br />
In trouble?<br />
<br />
It is true: I earn my living<br />
But, believe me, it is only an accident.<br />
Nothing that I do entitles me to eat my fill.<br />
By chance I was spared. (If my luck leaves me<br />
I am lost.)<br />
<br />
They tell me: eat and drink. Be glad you have it!<br />
But how can I eat and drink<br />
When my food is snatched from the hungry<br />
And my glass of water belongs to the thirsty?<br />
And yet I eat and drink.<br />
<br />
I would gladly be wise.<br />
The old books tell us what wisdom is:<br />
Avoid the strife of the world<br />
Live out your little time<br />
Fearing no one<br />
Using no violence<br />
Returning good for evil —<br />
Not fulfillment of desire but forgetfulness<br />
Passes for wisdom.<br />
I can do none of this:<br />
Indeed I live in the dark ages!<br />
<br />
2.<br />
<br />
I came to the cities in a time of disorder<br />
When hunger ruled.<br />
I came among men in a time of uprising<br />
And I revolted with them.<br />
So the time passed away<br />
Which on earth was given me.<br />
<br />
I ate my food between massacres.<br />
The shadow of murder lay upon my sleep.<br />
And when I loved, I loved with indifference.<br />
I looked upon nature with impatience.<br />
So the time passed away<br />
Which on earth was given me.<br />
<br />
In my time streets led to the quicksand.<br />
Speech betrayed me to the slaughterer.<br />
There was little I could do. But without me<br />
The rulers would have been more secure. This was my hope.<br />
So the time passed away<br />
Which on earth was given me.<br />
<br />
3.<br />
<br />
You, who shall emerge from the flood<br />
In which we are sinking,<br />
Think —<br />
When you speak of our weaknesses,<br />
Also of the dark time<br />
That brought them forth.<br />
<br />
For we went,changing our country more often than our shoes.<br />
In the class war, despairing<br />
When there was only injustice and no resistance.<br />
<br />
For we knew only too well:<br />
Even the hatred of squalor<br />
Makes the brow grow stern.<br />
Even anger against injustice<br />
Makes the voice grow harsh. Alas, we<br />
Who wished to lay the foundations of kindness<br />
Could not ourselves be kind.<br />
<br />
But you, when at last it comes to pass<br />
That man can help his fellow man,<br />
Do no judge us<br />
Too harshly.<br />
<br />
translated by H. R. Hays<br />
</p>

It's from 1938.  But it feels applicable to the here and now.

