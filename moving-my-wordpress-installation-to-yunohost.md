# Moving my WordPress installation to YunoHost

I moved my WordPress install from [[GreenHost]] VPS to a WordPress install on my [[YunoHost]] server.

GreenHost are a great company, but I was almost permanently getting the 'Error establishing a database connection' error on the install.  Not sure why.

I briefly dallied with a fresh VPS at Hetzner just for the purpose of the WordPress install, but it felt a bit overkill, and also when going through some of the steps of [[Ubuntu 20.04 basic server setup]] (but for latest Ubuntu) it all just felt a bit of a faff these days.  Why not let YunoHost take care of the fiddly bits of the server setup (including security aspects like fail2ban, firewall, etc) and just get my a WordPress install to work with?

So I did that.

-   point my doubleloop.net domain at the YunoHost IP
-   add a new domain in YunoHost
    -   self-signed cert to begin with
-   set up SSL with Let's Encrypt on the domain
    -   it wouldn't let me for a while, but I think adding the CAA record for Let's Encrypt helped, or maybe my DNS records just took a little time to propagate
-   Install WordPress app to the new domain in YunoHost
-   Once installed, follow the steps here for migrating a WordPress install: https://www.snel.com/support/how-to-duplicate-a-wordpress-site/
    -   I usually just use [[Duplicator]] for this, but because the MySQL connection was constantly failing, it wasn't working
-   Location of new app in YunoHost was /var/www/wordpress
-   Had to chown -R wordpress:www-data ./\* on all the new files

And it's up and running, relatively painless all told.  Also it's so much faster now.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2023-04-23 Sun]</span></span>

I notice that the SSOwat single sign on doesn't work into Wordpress - I have to log in again with my WP details.  Which makes sense, as I restored from the backup which has different login details to Yuno.  But I wonder if by design it would work out of the box for a fresh install, and whether I can fix it for my restored WP.


### <span class="timestamp-wrapper"><span class="timestamp">[2023-04-30 Sun]</span></span>

-   The upgrade from 6.1 to 6.2 happened via yunohost system update screen, rather than within wordpress itself - interesting.
-   Upgrading from 6.1 to 6.2, noticed that it's running on MariaDB rather than MySQL.  Probably fine, but worth being aware of.
-   Doing a general updates on Yunohost, that knocked out the website briefly, and ungracefully, with the ol classic 'Error establishing a database connection'.  It came back up again after the updates.  It was probably PHP or MariaDB being upgraded, I suppose.

