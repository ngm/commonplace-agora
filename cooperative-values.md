# Cooperative values



## The seven principles

![[images/seven-coop-principles.jpg "by Hallie Zillman, original at https://www.willystreet.coop/pages/seven-cooperative-principles"]]

Open and Voluntary Membership

-   Democratic Member Control
-   Members’ Economic Participation
-   Autonomy and Independence
-   Education, Training, and Information
-   Cooperation Among Cooperatives
-   Concern for Community


## Misc

> Funded by direct member investment, rather than investment from third-party shareholders, co-operative members "decide on the values of the enterprise, which don’t necessarily need to be about the maximization of profits." 
> &#x2013; [[DisCO Manifesto]] 

<!--quoteend-->

> Cooperatives succeed when values are aligned around communal benefit to pooling resources and a shared desire to "avoid anti- competitive or extractive behavior."  
> 
> &#x2013; [[DisCO Manifesto]] 

<!--quoteend-->

> Co-operatives offer a way for people to be rewarded for their labour while committing to shared social values.
> 
> &#x2013; [[DisCO Manifesto]] 

