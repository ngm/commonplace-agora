# Tron Legacy

I have a real soft spot for the first [[Tron]] - it isn't brilliant but it has lots of nostalgia for me.  I think Tron Legacy is a great film too, actually more enjoyable than the first.  It didn't get great reviews, but, bleh.  

I love the visual style.  (Basically lots of neon piping and computer grids).  The [[Daft Punk]] soundtrack is spot on.  I think the direction and acting is pretty good for what it needs to be, too.  Jeff Bridges.

The plot doesn't blow your mind.  Good versus bad in the [[Grid]], a bit of father son bonding&#x2026; genocide.  (That part is pretty heavy and also&#x2026; not explored at all).  There's a tiny bit of philosophising about the pursuit of perfection, and can machines understanding the need for flaws?  Very very tiny bit.

Perhaps unsurprisingly, given it's target market is probably 14 year old boys, the female characters are pretty weak.  The scene with the four women preparing Sam for the games is just embarrassing.  Jem is good looking, jealous, and untrustworthy.  Quorra is cool but kind of a manic pixie cyber girl.

But on the whole the characters are generally fun, the action is good, it pulls you along at a good pace.  It's a blast.

It also has some reasonably realistic Unix command-line action, extra marks for that.

