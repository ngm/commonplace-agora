# POSSE vs PESOS

-   I was thinking I'd like to get my toots on [[social.coop]] pulled in to my garden too.  But ideally, [[POSSE]]ing would be better than [[PESOS]] there.  So again maybe it comes down to sorting out my stream and webmentions etc in the garden.

-   That said - maybe there's an argument for PESOS over POSSE when it comes to a digital garden?  Use the interface of the apps that were specifically designed for the platform rather than try to reverse engineer and rebuild.  Just make sure you've got the data.

The IndieWeb wiki suggests that POSSE is preferably over PESOS, when possible.

