# Communicative capitalism

A concept from [[Jodi Dean]].

> In communicative capitalism, capitalist productivity derives from its expropriation and exploitation of communicative processes.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Other names for communicative capitalism are [[knowledge economy]], [[information society]], and [[cognitive capitalism]].

And [[information capitalism]] too?

> This does not mean that information technologies have replaced manufacturing; in fact, they drive a wide variety of mining, chemical, and biotechnological industries. 
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

There is still a huge material aspect to ICT.

> Nor does it mean that networked computing has enhanced productivity outside the production of networked computing itself. Rather, it means that capitalism has subsumed communication such that communication does not provide a critical outside.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

Not sure what that means.

> Communicative capitalism refers to the form of late capitalism in which values heralded as central to democracy materialize in networked communications technologies.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Ideals of access, inclusion, discussion and participation are realized through expansions, intensifications and interconnections of global telecommunications.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Communication serves capital, whether in affective forms of care for producers and consumers, the mobilization of sharing and expression as instruments for “human relations” in the workplace, or contributions to ubiquitous media circuits.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]


## Where the value is expropriated from in communicative capitalism

> Marx’s analysis of value in Capital helps explain how communication can be a vehicle for capitalist subsumption.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Communicative capitalism seizes, privatizes, and attempts to monetize the social substance without waiting for its crystallization in products of labor. It does not depend on the commodity-thing. It directly exploits the social relation at the heart of value. Social relations don’t have to take the fantastic form of the commodity to generate value for capitalism. **Via networked, personalized communication and information technologies, capitalism has found a more straightforward way to appropriate value.**
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Communicative capitalism subsumes everything we do. It turns not just our mediated interactions, but all our interactions, into raw material for capital.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> [[Big data]] is the capitalists’ name for this material that Marx understood as the social substance.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> First, under communication messages are contributions. The idea of a message as something sent by a speaker to a receiver in order to elicit a response from that receiver no longer holds. Messages are now contributions to circulating content.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> There’s a shift from the primacy of a message’s use value to the primacy of its exchange value, to its capacity to circulate, to be forwarded and to be counted.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Unlike a message, which needs to be understood, a contribution is just an addition. One contributes one’s opinion or idea to whatever discussion is going on. This additive feature of the contribution depends on a fundamental communicative equivalence. As a contribution, each message is communicatively equal to any other. What matters is not what was said but rather that something was said. No opinion or judgment is worth more than any other (they each count as one comment on my blog, one like, one tweet). Each adds something to the flow. Facts, theories, judgments, opinions, fantasies, jokes, and lies circulate indiscriminately.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Words are counted in word clouds, measured by how often they are repeated rather than by their meaning.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Communicative production is for circulation more than use (getting attention not furthering understanding). 
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

