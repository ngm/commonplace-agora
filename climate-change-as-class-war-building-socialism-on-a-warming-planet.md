# Climate Change as Class War: Building Socialism on a Warming Planet

A
: [[book]]

Author
: [[Matthew Huber]]

I've not read, but listened to a couple interviews with Matt Huber.  Interesting take on [[eco-socialism]], some good critiques on what he would call the [[professional-managerial class]] domination of the environmental movement.

> In part an argument for appealing to working-class interests to win a [[Green New Deal]], in part a polemic against [[degrowth]], which Huber associates with the environmentalism of the ‘professional managerial class’, the book aims to further the cause of so-called ‘[[eco-modernism]]’.
> 
> &#x2013; [[The Great Unfettering]]


## Criticism

-   [Environmentalists Need Unions, Unions Need Environmentalists: A Review of “Cl&#x2026;](https://www.the-trouble.com/content/2022/6/7/environmentalists-need-unions-unions-need-environmentalists-a-review-of-climate-change-as-class-war)

