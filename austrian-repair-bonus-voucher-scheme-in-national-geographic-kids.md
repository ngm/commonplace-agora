# Austrian Repair Bonus voucher scheme in National Geographic Kids

![[2024-03-04_21-02-30_screenshot.png]]

[[Repair Bonus voucher scheme]].

Text from the snippet reads:

> Got a broken gadget?  Well, the Austrian government is giving people €200 (£170) to repair electronics like laptops and washing machines instead of buying new ones! That's because repairing things rather than binning them is vital for helping the environment.  It saves loads of energy and reduces electronic waste, which often contains toxic substances that pollute soil and water. Sadly, it's often cheaper and easier to buy new things than to mend old devices, as many tech companies make their products hard to fix.  But it's hoped Austria's Repair Bonus voucher scheme will change that.  Plus, it supports local repair businesses, too.  Great!

