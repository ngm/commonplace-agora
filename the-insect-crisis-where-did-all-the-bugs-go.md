# The insect crisis: where did all the bugs go?

A
: [[podcast]]

URL
: https://www.theguardian.com/news/audio/2022/apr/11/insect-crisis-where-did-all-the-bugs-go-podcast

[[Biodiversity loss]]. [[The Insect Crisis]].

Causes:  [[Habitat loss]]. [[Pesticides]]. [[Climate change]].

