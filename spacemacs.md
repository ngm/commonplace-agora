# spacemacs

URL
: https://www.spacemacs.org/

A flavour of [[Emacs]] built around vi-bindings and lots of preconfigured setup for you ('batteries included').

As the Spacemacs team says:

> The best editor is neither Emacs nor Vim, it's Emacs **and** Vim!

I've used spacemacs since about 2016?

[[My Spacemacs User Config]].


## Look'n'feel

Current dark theme - doom-one.
Current light theme - [homage-white](https://www.reddit.com/r/emacs/comments/ld7mpr/new_doom_themes_homagewhite_and_homageblack/).


## [[org-mode]]


## Log

-   [[Problem: Package phpcbf is unavailable]]

