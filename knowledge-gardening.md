# Knowledge gardening

See [[Wiki gardening]].

> Knowledge gardening, on the other hand, is the emergent result of constructing a feedback system. I recurse over scratch notes, revise them, add to them, refactor them, and combine them with other ideas to form new ideas. Over time, these little recursive acts of watering and weeding accumulate. Useful knowledge grows from the ground-up.
> 
> &#x2013; [Knowledge gardening is recursive - by Gordon Brander - Subconscious](https://subconscious.substack.com/p/knowledge-gardening-is-recursive)

