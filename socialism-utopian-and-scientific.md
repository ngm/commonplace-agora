# Socialism: Utopian and Scientific

[[Friedrich Engels]].

A critique of [[utopian socialism]] in favour of [[scientific socialism]].

> In Socialism: Utopian and Scientific, Engels saw the emergence of 19th-century utopian socialism, signalled by the work of Saint-Simon, Fourier and Owen, as a reaction to the defeated aspirations of the [[French Revolution]].
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> In Socialism: Utopian and Scientific, Engels (1880, 290) writes that utopian socialists are “drifting off into pure phantasies”. He argues that utopian socialism is moralistic and lacks a scientific analysis of capitalism and its contradictions: “The Socialism of earlier days certainly criticized the existing capitalistic mode of production and its consequences. But it could not explain them, and, therefore, could not get the mastery of them. It could only simply reject them as bad” (1880, 305). Engels thinks that Marx’s works, Marx’s approach of historical materialism, and the notion of surplus-value helped to turn socialism into a science:
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Industrial production was barely developed, and the proletariat, wrote Engels, appeared to these radicals as ‘incapable of independent political action’ – ‘an oppressed, suffering order’ which required help from outside.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> In these conditions, the utopian socialists attempted in idealist fashion to evolve the solution to social problems ‘out of the human brain'
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> Engels argued that ‘scientific’ – that is, self-critical, rigorously conceptualized and empirically tested – socialism must be rooted in an investigation of historical development: ‘the process of evolution of humanity.’
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> Georg Lukács argues that although classical utopian-communist literature [in its] step beyond Capitalism follows fantastic paths, its critical-historical basis is nonetheless linked – especially in the case of Fourier – with a devastating critique of the contradictions of bourgeois society. In Fourier, despite the fantastic nature of his ideas about Socialism and of the ways to Socialism, the picture of Capitalism is shown with such overwhelming clarity in all its contradiction that the idea of the transitory nature of this society appears tangibly and plastically before us
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

