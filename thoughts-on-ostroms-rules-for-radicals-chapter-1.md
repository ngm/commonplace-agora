# Thoughts on Ostrom's Rules for Radicals, Chapter 1

[[Elinor Ostrom's Rules for Radicals]].
[[Elinor Ostrom]].

Originally here: https://doubleloop.net/2018/02/11/thoughts-ostroms-rules-radicals-chapter-1/

This chapter starts out with a brief biography of Ostrom and her work, providing some context. I think it’s the right amount – the ideas are more important, but it is interesting to get some biographical context. The patriarchal system she faced early on is pretty galling – difficulties in getting where she got to, just by virtue of being a woman.

Ostrom doesn’t slot into a particular predefined school of thought, with some ties to some conservative right thinkers, yet some radical views. I like that Wall approaches it not so much trying to pin her ideas down to any particular ideology, but looking at what practical effects the ideas have had (and can have).

It’s worth noting that Wall is an ecosocialist, so will most likely present his interpretation of Ostrom through that lens – but he’s very open and clear about his own position.

Despite not having a particular pigeonhole, Ostrom had firm views on the importance of equality, direct citizen participation, ecological concern and promoting sustainable management of shared resources. I can definitely get on board with all of that.

As I was hoping for, the upcoming chapters are being trailed as having a focus on practical action, not ‘broad principles and slogans’. Ostrom’s own work, certainly on the commons at least, was based on concrete examples and observations, e.g. the management of the water basin in Los Angeles.

The focus on the need to analyse neoliberal institutions, and being aware of the limits of horizontalism, are reminding me of Srnicek and Williams’ points in [[Inventing the Future]].

> Being against neoliberalism is insufficient to transform neoliberalism, movements and mobilisations have come and gone but have generally failed to sustain major change. Alternatives based on clear institutional analysis can contribute to solutions that move us beyond our current, widely criticised economic system. — p.16

<!--quoteend-->

> If we want to challenge neoliberalism we need to understand in detail how neoliberal institutions work. If we wish to create an ecological and democratic economy, we need to evolve appropriate institutions. Ostrom’s critical institutionalism is an essential part of her legacy and a source of useful suggestions to radicals who wish to contribute to a more ecological, democratic, diverse and equal future.  –p.19

Looking forward to discovering more about the ideas of Ostrom’s analysis and design framework for institutions.

Ostrom is obviously best known for her work on [[governing the commons]]. I read some of the opening parts of Hardin’s tragedy of the commons paper before starting the book, and found the assumption that individuals are unremittingly selfish a bit hard to swallow. So I’m happy to see Wall say of Ostrom:

> her work shows that the foundational assumption that human beings are rational maximisers, locked into selfish and competitive behaviour, can be challenged — p.17

I don’t know if Ostrom ever presented any thoughts on technology, either in general or as it relates to the commons, so I’m interested to see if anything comes up. (Wall did touch it ever so briefly when he mentioned that free software and the web can be considered as commons). As this is my interest, I’m going to read with technology in mind – how the ideas presented could apply to technology, but also how could technology support some of the ‘rules for radicals’ the book will talk about.

Upcoming chapters will look at commons, ecology, deep democracy, feminism and intersectionality, trust and cooperation, research and education, institutional transformation, and a Marxist critique of Ostrom’s work. Should all be good! 

