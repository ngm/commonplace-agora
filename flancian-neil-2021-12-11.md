# Flancian Neil 2021-12-11

[[Flancian]] [[Neil]]

-   can't remember how it came up but I mentioned [[ValueFlows]]
    -   like ActivityPub but for economic activity
    -   want to understand more so think I'll make it my node club for the week
-   to cooperate, you need a 'thing' as the locus of what you're cooperating on
    -   you have something that you are pushing, and a direction
    -   to move in next direction, you need a question?
    -   direction is potential
    -   made notes vs notions
    -   protopia
-   free, fair and alive - the patterns are crosscutting (interdisciplinary)
-   notes on the synthesis of form (notsof)
-   evolution of patterns: alexander, gang of four, ward, commons? &lt;- we are here!
-   meta-patterns from wiki
-   did ostrom do patterns?
-   ostrom vs b&amp;h - commons vs commoning
-   agora: pulling in external sources
    -   how to import (plus attribution)
    -   how to import content that we didn't write
        -   license checking
        -   should it be attributed to the discover (trust, and recognition)
    -   mapping problem
        -   pattern language
            -   external links vs internal links
-   agora is like search (of the commons)
-   nodes linking to node name or a separate name?
-   info daemons
    -   eliza
    -   train e.g. gpt-3 on philosophers
    -   have a conversation with a book
    -   the diamond age
    -   conversation theory
    -   chat bots
    -   umberto eco - has a 'model reader', challenge the reader, not too much
    -   good reading feels like it isn't a monologue
-   how do a diverse community converge of a definition
    -   patterns for solving problems
    -   how to solve it
    -   systems thinking
    -   ampl
-   rosetta stone paper, category theory (maths, physics, sociology)
-   borges, maps and territory
-   [[Flancia Collective]] should write an article for [[Compost]]

