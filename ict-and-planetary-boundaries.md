# ICT and planetary boundaries

Following a question on [[Climate Action Tech]] Slack:

I suggested that

> https://doingthedoughnut.tech/insights might be a useful starting point

[[Doing the Doughnut.tech]]

For each boundary there are some good thoughts on the effect that ICT have on these boundaries. They mostly cover the direct/lifecycle effects of ICT, but also touch on enabling and systemic effects.

And Gaël Duez suggested:

> Following the main conclusions of the https://digitalcollage.org and my own research, the following boundaries are (mostly) impacted if you look across the entire life cycle of our digital equipment (not only the electricity consumption in the use phase):
> 
> -   Chemical pollution (mining and manufacturing, especially chip industry, and local pollutionin illegale e-waste landfills)
> -   Freshwater use (mining is the n°1 culprit by far but the chip industry is also very thirsty and some data centers also)
> -   Biodiversity loss (mining and land use by hyperscalers)
> -   Climate crisis (because of the electricity mix during the use phase but actually even more due to fossil fuel consumption during manufacturing and mining)
> 
> &#x2013; Gaël Duez

