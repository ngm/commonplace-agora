# Millionaires call on governments worldwide to 'tax us now'

https://www.theguardian.com/business/2022/jan/19/millionaires-call-on-governments-worldwide-to-tax-us-now

[[Patriotic Millionaires]].

[[An annual “wealth tax” on those with fortunes of more than $5m (£3.7m) could raise more than $2.52tr]].

> That would be enough, they said, to “lift 2.3 billion people out of poverty; make enough vaccines for the world and deliver universal healthcare and social protection for all the citizens of low and lower-middle-income countries (3.6 billion people).

<!--quoteend-->

> The proposed tax would see those with more than $5m pay 2%, rising to 3% for those with more than $50m and a 5% rate for dollar billionaires.

