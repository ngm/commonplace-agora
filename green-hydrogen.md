# Green hydrogen

[[Renewable energy]].

> Water’s molecular composition is famously made up of two hydrogen atoms and one oxygen atom (H2O). But if an electrolysis process powered by 100% renewable energy is used to split these molecules apart, it can create a clean hydrogen energy source, without any greenhouse gas emissions. The hydrogen can then be burned to power industrial processes leaving water as its only byproduct.
> 
> Large-scale electrolysis would still be an expensive proposition and hydrogen’s low density and high flammability pose storage and transportation challenges. But the technology is increasingly being looked at as a potential power source for hard-to-decarbonize sectors such as steel and cement. 
> 
> &#x2013; Down to Earth newsletter

