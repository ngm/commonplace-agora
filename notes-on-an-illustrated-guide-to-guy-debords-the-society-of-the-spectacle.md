# Notes on "An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'"

&#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

> is a polemical and prescient indictment of our image-saturated consumer culture. The book examines the “Spectacle,” Debord’s term for **the everyday manifestation of capitalist-driven phenomena; advertising, television, film, and celebrity.**

<!--quoteend-->

> Instead, Debord describes the spectacle as capitalism’s instrument for distracting and pacifying the masses.

<!--quoteend-->

> The spectacle takes on many more forms today than it did during Debord’s lifetime. It can be found on every screen that you look at. It is the advertisements plastered on the subway and the pop-up ads that appear in your browser. It is the listicle telling you “10 things you need to know about ‘x.’” 

<!--quoteend-->

> The spectacle reduces reality to **an endless supply of commodifiable fragments, while encouraging us to focus on appearances**. For Debord, this constituted an unacceptable “degradation” of our lives.

<!--quoteend-->

> Debord was a founding member of the Situationist International (1957–1972), a group of avant-garde artists and political theorists united by their opposition to advanced capitalism. At varying points the group’s members included the writers Raoul Vaneigem and Michèle Bernstein, the artist Asger Jorn, and the art historian T.J. Clark. Inspired primarily by Dadaism, Surrealism, and Marxist philosophy, the SI rose to public prominence during the May 1968 demonstrations during which members of the group participated in student-led occupations and protests. 

<!--quoteend-->

> By paraphrasing Marx, Debord immediately establishes a connection between the spectacle and the economy. The book essentially reworks the Marxist concepts of commodity fetishism and alienation for the film, advertising, and television age.

<!--quoteend-->

> The Spectacle is not a collection of images, but a social relation among people, mediated by images.

<!--quoteend-->

> If Debord were alive today, he would almost certainly extend his analysis of the spectacle to the Internet and social media. Debord would no doubt have been horrified by social media companies such as Facebook and Twitter, which monetize our friendships, opinions, and emotions. Our internal thoughts and experiences are now commodifiable assets. Did you tweet today? Why haven’t you posted to Instagram? Did you “like” your friend’s photos on Facebook yet?

<!--quoteend-->

> To be clear, Debord did not believe that new technology was, in itself, a bad thing. He specifically objected to the use of perceptual technologies for economic gain. The spectacle, which is driven by economic interest and profit, replaces lived reality with the “contemplation of the spectacle.”

<!--quoteend-->

> We no longer live. We aspire. We work to get richer. Paradoxically, we find ourselves working in order to have a “vacation.” We can’t seem to actually live without working. Capitalism has thus completely occupied social life. 

<!--quoteend-->

> **The proliferation of images and desires alienates us, not only from ourselves, but from each other.** Debord references the phrase “[[lonely crowds]],” a term coined by the American sociologist David Riesman, to describe our atomization

Social software.

> The spectacle functions as a pacifier for the masses, a tool that reinforces the status quo and quells dissent. “The Spectacle presents itself as something enormously positive, indisputable and inaccessible. It says nothing more than ‘that which appears is good, that which is good appears,’” writes Debord. “It demands […] passive acceptance which in fact it already obtained by its manner of appearing without reply, by its monopoly of appearance.”

<!--quoteend-->

> Gradually, we begin to conflate visibility with value. If something is being talked about and seen, we assume that it must be important in some way. 

<!--quoteend-->

> Debord defines two primary forms of the spectacle — the concentrated and the diffuse. The concentrated spectacle, which Debord attributes to totalitarian and “Stalinist” regimes, is implemented through the cult of personality and the use of force. The diffuse spectacle, which relies on a rich abundance of commodities, is typified by wealthy democracies. The latter is far more effective at placating the masses, since it appears to empower individuals through consumer choice. 

<!--quoteend-->

> The diffuse spectacle of modern capitalism propagates itself by exploiting the spectator’s lingering dissatisfaction. Since the pleasure of acquiring a new commodity is fleeting, it is only a matter of time before we pursue a new desire — a new “fragment” of happiness. The consumer is thus mentally enslaved by the spectacle’s inexorable logic: work harder, buy more.

<!--quoteend-->

> Today, the integrated spectacle continues to provide abundant commodities while defending itself with the use of misinformation and misdirection. According to Debord, it does this primarily through the specter of terrorism:
> 
> Such a perfect democracy constructs its own inconceivable foe, terrorism. Its wish is to be judged by its enemies rather than by its results. The story of terrorism is written by the state and it is therefore highly instructive. The spectating populations must certainly never know everything about terrorism, but they must always know enough to convince them that, compared with terrorism, everything else seems rather acceptable, or in any case more rational and democratic.

<!--quoteend-->

> First time readers of Debord’s work may prefer to read Comments first, since it is a brisker and more informal read than The Society of the Spectacle. Unlike his original text, Debord refers to contemporary events to illustrate his arguments, including the Iran-Contra affair, Manuel Noriega’s dictatorship of Panama, and the sinking of the Rainbow Warrior.

<!--quoteend-->

> This meta-textual approach places Debord’s work into a lineage of celebrated texts whilst also embodying the SI’s concept of détournement, a term variously translated as “diversion,” “detour,” “reroute,” and “hijack.”

<!--quoteend-->

> The mutual interference of two worlds of feeling, or the juxtaposition of two independent expressions, supersed[ing] the original elements and produc[ing] a synthetic organization of greater efficacy.
> 
> The SI championed [[détournement]] as a means of interrupting the fabric of the everyday — whether it be repurposing old film reels, subverting iconic images or slogans, or devising literature inspired by the works of other writers. 

<!--quoteend-->

> The act of détournement imbues revered and historicized works of art and literature with new life, thereby overcoming their congealment at the hands of the spectacle. As Debord and Wolman write:
> 
> Détournement not only leads to the discovery of new aspects of talent; in addition, clashing head-on with all social and legal conventions, it cannot fail to be a powerful cultural weapon in the service of real class struggle.

<!--quoteend-->

> Although The Society of the Spectacle is recognized as an incisive indictment of the consumerist experience, readers may well reject Debord’s assertion that capitalism has inherently degraded our social lives. After all, how can society produce new services and products without some form of industrialization? On this particular point, Debord is unrelenting, arguing that capitalism — having already served our most basic survival needs (the means to food, shelter, etc.) — relies on fabricating new desires and distractions in order to propagate itself and maintain its oppression over the working classes

<!--quoteend-->

> The most significant criticism that can be leveled at The Society of the Spectacle is Debord’s failure to proffer any convincing solutions for countering the spectacle, other than describing an abstract need to put “practical force into action

<!--quoteend-->

> It depressed him in his later years that [his] insight had long since ceased to be a revolutionary call to arms but the most accurate, if banal, description of modern life […] While Debord’s public life was predicated upon his revolutionary intentions, in private he sought oblivion in infamy, exile and alcoholism.

<!--quoteend-->

> Ask yourself — what compels us to buy the latest tech gadget? Why do we spill our feelings out on Facebook, in posts that are archived on servers deep underground? Which is more important, the expression of the feeling itself, or the knowledge that it will be documented and seen by others? Why do we incessantly take selfies, or record our every moment for posterity?

<!--quoteend-->

> Are we afraid of being a nobody — of being on “the margin of existence?” **If you’re concerned with how you appear, then are you really living?**

