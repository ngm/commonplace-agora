# Clougha Pike

Clougha Pike is a peak in the [[Forest of Bowland]].  I love it.  I like the rather pleasant bleakness of moorland terrain.


## Views

At the top of Clougha Pike, looking out over towards the Yorkshire Dales I think.

![[images/clougha-hike/peak.jpg]]

The heather of the moorland in autumn.

![[images/clougha-hike/heather.jpg]]

A lovely spot at the bottom of Clougha.

![[images/clougha-hike/20201010_141447.jpg]]


## Goldsworthy

There's an [[Andy Goldsworthy]] installation near the peak of Clougha.  

View from a distance of the Andy Goldsworthy sculpture.

![[images/clougha-hike/goldsworthy2.jpg]]

Another angle on the Andy Goldsworthy sculpture.

![[images/clougha-hike/goldsworthy.jpg]]

