# Marx's theory of metabolism

-   [[Marx's Theory of Metabolic Rift]]

> His point is quite simple. He characterizes actually labor, the one of the fundamental category of his critical political economy as the conscious mediation of human metabolic interaction with nature. So human produces many things with certain aims and then they improve new technologies and so on. But this process is a kind of a circular process because human takes out of energy resources from nature and they produce something and then they consume them. And then there will be a waste of energy and resources, they go back to nature. So this is a kind of circular process that is basically everywhere, anywhere in the time of history. So as long as humans live, this process of metabolism will continue. We cannot ignore this. And this is a natural law of our existence, I mean, any living being. But the humans can consciously mediate this metabolism with nature. But the problem is under capitalism, this metabolism is disrupted. Marx called this disruption, [[metabolic rift]].
> 
> &#x2013; [[Kohei Saito on Degrowth Communism]]

-   labour is the conscious mediation of human metabolic interaction with nature
-   under capitalism this metabolism is disrupted (the metabolic rift)

