# How to talk about avoiding waste, with Keep Britain Tidy

A
: [[podcast]]

Found at
: https://therestartproject.org/podcast/keep-britain-tidy/

Most people think that [[recycling]] is all you need to do to prevent [[waste]].

Reframe the message and move people's consciousness up the [[waste hierarchy]].

[[Buy Nothing New Month]]. Really interesting discussion around societal norms and reuse (e.g. what happens when you give second-hand presents at kids' birthday parties&#x2026;)

