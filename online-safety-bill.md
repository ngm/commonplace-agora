# Online Safety Bill

> some of the proposed amendments are indeed excruciatingly bad. but the actual current legislation wording is pretty simple&#x2026; and terrifying: it simply says that the regulator can mandate "content moderation" (i.e scanning) to mitigate CSAM &amp; terrorism. We tried to summarise at https://news.ycombinator.com/item?id=34923537. Even if Apple isn't doing OS-level scanning, this clause sets a massive precedent for other govts to try to follow, even if the US holds out.
> 
> &#x2013; https://mastodon.matrix.org/@matrix/109928007650597023

