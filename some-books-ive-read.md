# Some books I've read

A potted history of books that I've read.

You can also check out my bookshelf: https://noodlemaps.net/calibre/


## Currently reading

-   [[Wasteland]] (audiobook)


### On the pile&#x2026;

I've likely read about 3 chapters, then got distracted.

-   [[Mute Compulsion]]
-   [[Neither Vertical Nor Horizontal]]
-   [[Wasteland]]
-   [[The Entropy of Capitalism]]
-   [[Ours to Hack and To Own]]
-   [[Ways of Being]]
-   [[A People's Guide to Capitalism]]
-   [[The Next Revolution]]
-   [[October (book)]] by China Mieville
-   [[The Twittering Machine]] by Richard Seymour
-   [[Small is Beautiful]] by E. F. Schumacher
-   [[The Shallows]]
-   [[The Society of the Spectacle]]


### Interested to read

-   The Word for World is Forest
-   [[Twitter and Tear Gas]]
-   [[The Garden of Forking Paths]]
-   [[Goodbye iSlave]]
-   Alone Together
-   Pressed for Time
-   [[The Sane Society]]
-   The City, Not Long After
-   [[The Year 200]]
-   [[The Cybernetic Brain]]
-   The Web of Life
-   [[Woman on the Edge of Time]]
-   Technology of the oppressed
-   LaserWriter II
-   Finding the Mother Tree
-   Entangled Life
-   [[Vagabonds]]
-   [[First and Last Men]]
-   How Not to Network a Nation
-   [[Blood in the Machine]]
-   [[The Systems View of Life]]


## Read


### 2024

-   [[A Short History of Nearly Everything]] (abridged, audiobook)
-   [[Talking to My Daughter About the Economy]]
-   [[The Shock Doctrine of the Left]]


### 2023

-   [[Doughnut Economics]]
-   [[The Internet Con]]
-   [[Internet for the People]]
-   [[The Care Manifesto]]
-   A Prayer for the Crown-Shy
-   The Annual Migration of Clouds
-   [[Less is More]]


### 2022

-   [[A Wizard of Earthsea]]
-   [[Look to Windward]]
-   [[Half-Earth Socialism]]
-   [[Red Plenty]]
-   [[The Quantum Thief]] (again)
-   [[The Fractal Prince]] (again)
-   [[Platform socialism]]
-   [[Twenty-First Century Socialism]]
-   [[Radical Technologies]]
-   [[The Ministry for the Future]]
-   [[Cybernetic Revolutionaries]]


### 2021

-   [[Free, Fair and Alive]]
-   [[Anarchist Cybernetics]]
-   [[The Left Hand of Darkness]]
-   [[Undoing Optimization: Civic Action in Smart Cities]]
-   [[Parable of the Sower]]
-   [[Infinite Detail]]
-   [[A Psalm for the Wild-Built]]
-   [[bolo'bolo]]


### 2020

-   [[Future Histories]]
-   [[Hello World]]
-   [[A Closed and Common Orbit]]
-   [[The Long Way to a Small, Angry Planet]]
-   [[84K]]
-   [[Ctrl+S]]
-   [[The Three Body Problem]]
-   [[Dune]]
-   [[A Memory of Empire]]


### 2019

-   [[Autonomous]]
-   [[Aurora Rising]]
-   [[Elysium Fire]]
-   [[Red Mars]]
-   [[Use of Weapons]]
-   [[The Player of Games]]
-   [[Consider Phlebas]]
-   [[Ancillary Justice]]


### 2018

-   [[Jackson Rising]]: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi
-   [[Perdido Street Station]]
-   [[Walkaway]]
-   [[Elinor Ostrom's Rules for Radicals]]: Cooperative alternatives beyond markets and states


### 2017

-   [[Inventing the Future]]
-   [[The Dispossessed]]
-   [[The Quantum Thief]]
-   [[The Fractal Prince]]
-   The Causal Angel


### ????

-   [[The Cassini Division]]
-   [[The City &amp; the City]]
-   [[Oryx and Crake]]
-   [[Cryptonomicon]]
-   [[Snow Crash]]
-   [[Neuromancer]]
-   [[Altered Carbon]]
-   [[Naked Lunch]]
-   [[Four Futures]]

