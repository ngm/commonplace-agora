# iFixit and Lenovo

Seems promising - [[Lenovo]] worked with [[iFixit]] to improve the repairability of the [[ThinkPad T14]]:

> During the development process, our Solutions team—comprised of repairability experts and tech writers—disassembled, documented, and evaluated the internal design of the laptop to provide feedback. Lenovo’s design team engaged with this feedback. They listened to what we had to say about the repairability of their laptop. The result: A ThinkPad that looks deceptively similar to its predecessors from the outside, but features a drastic repairability improvement on the inside.
> 
> --[iFixit And Lenovo Work Together To Make Laptop Repairability The Standard | i&#x2026;](https://www.ifixit.com/News/91217/ifixit-and-lenovo-work-together-to-make-laptop-repairability-the-standard)

iFixit give it a 9 out of 10 repair score now. According to the article, Lenovo is the world’s largest PC vendor, and this is their best-selling laptop.

