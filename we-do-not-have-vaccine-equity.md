# We do not have vaccine equity

We do not have [[vaccine equity]].

> As of 2022, the prospect of anything approaching vaccine equity remains as remote as ever
> 
> &#x2013; [The Guardian view on vaccine justice: the developing world won’t wait | Edito&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/04/the-guardian-view-on-vaccine-justice-the-developing-world-wont-wait)


## Evidence

-   More boosters have been delivered in the developed world than first and second doses in low-income countries
-   In high-income countries more than two in three people have received at least one dose, but in low-income countries only one in nine
-   Richer nations have engaged in [[vaccine nationalism]] – hoarding doses (in some cases, later thrown away unused)


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

