# Hyperland

A documentary by [[Douglas Adams]] about [[hypermedia]].

Every bit as fantastic as you would expect.

They interview [[Ted Nelson]], discuss [[Vannevar Bush]], [[Doug Engelbart]],  [[Xanadu]], [[Kurt Vonnegut]], [[Guernica]] + the [[Spanish Civil War]], the shape of stories and non-linearity&#x2026; and lots more.  It feels a bit like it was directed by Terry Gilliam.

And oh yeah - Tom Baker plays Douglas Adams' hypermedia software agent.

