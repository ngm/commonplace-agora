# Carbon inequality

[[Carbon]] [[inequality]].

A small number of the ultra-rich (the 1%) constitute much of the world's carbon emissions.

(The next 10% also contribute too much too.)

> the richest 1% of the population produced as much carbon pollution in one year as the 5 billion people who make up the poorest two-thirds
> 
> &#x2013; [[The great carbon divide]]

<!--quoteend-->

> At the top is the wide, flat, very shallow bowl of the richest 10% of humanity, whose carbon appetite – through personal consumption, investment portfolios, and share of government subsidies and infrastructure benefits – accounts for about 50% of all emissions.
> 
> &#x2013; [[The great carbon divide]]

