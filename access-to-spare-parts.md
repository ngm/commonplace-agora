# access to spare parts

Access to [[Spare parts]].

Ensure access to spare parts at a reasonable cost. 
To everyone including consumers, non profit repair initiatives and [[independent repairers]].

What's a reasonable cost?

