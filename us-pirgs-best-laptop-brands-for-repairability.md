# US PIRG's best laptop brands for repairability

US PIRG has a short report on what it considers to be the best laptop brands for repairability.

https://pirg.org/articles/best-laptops-of-2024-the-most-repairable-laptops-and-why-it-matters/

They are, in order:

1.  Asus
2.  Acer
3.  Microsoft
4.  Dell
5.  HP
6.  Lenovo
7.  Apple

I’d love a bit more detail on the methodology they used for these rankings. (Perhaps the scores for repair and disassembly come via iFixit?) They hint at weighting favourably for disassembly and access to spare parts, and scoring against manufacturers who are involved in lobbying against right to repair.

It’s a shame that the [[Framework laptop]] isn’t mentioned, as they focus on larger manufacturers.

What do you make

