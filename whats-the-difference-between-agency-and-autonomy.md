# What's the difference between agency and autonomy?

> There is often overlap between the two. When focusing on what differences there might be between the concepts:
> 
> -   agency emphasizes capacity to act/take action, as opposed to being a deterministic automaton; such decisions can be reached through free will and/or through the influence of social structures/social conditioning
> 
> -   autonomy emphasizes capacity to be reflective and self governed (i.e. in the case of autonomy: one is reflective regarding how they reach their decision; one is aware of their inner processes, their consistency, and of outside influence on such processes). In the case of autonomy, one is said to own, to a large extent, such processes/their decisions
> 
> &#x2013; [Is there a difference between agency and autonomy? : Feminism](https://www.reddit.com/r/Feminism/comments/1eomdp/is_there_a_difference_between_agency_and_autonomy/) 

