# The Internet Con

A
: [[book]]

URL
: https://www.versobooks.com/en-gb/products/3035-the-internet-con

Author
: [[Cory Doctorow]]

Cory Doctorow's take on how to reclaim technology from the Big Tech firms - i.e. how to [[seize the means of computation]].  The main mechanism that he explores is  [[interoperability]], with a focus on [[adversarial interoperability]].

> This is a book for people who want to destroy Big Tech.

Bold start!

> It’s not a book for people who want to tame Big Tech. There’s no fixing Big Tech.

Agree.

> It’s not a book for people who want to get rid of technology itself. Technology isn’t the problem. Stop thinking about what technology does and start thinking about who technology does it to and who it does it for.

Also agree.

> This is a book about the thing Big Tech fears the most: technology operated by and for the people who use it.

Agree.  This is a sentiment that chimes with, for example, [[Internet for the People]] (amongst plenty others).

Although there's a lot on interoperability, Doctorow see legislation as an important component too.

> Combining [[comcom]] and mandates (like the Massachusetts right to repair law), could create something more powerful than either is on its own. Mandates and comcom are like two-part epoxy: The mandate is strong but brittle, comcom is flexible but requires constant maintenance to keep it from bending out of shape. Together, they are strong and resilient.
> 
> &#x2013; [[Freeing Ourselves From The Clutches Of Big Tech]]


## Important of tech as a site of struggle

> If we someday triumph over labor exploitation, gender discrimination and violence, colonialism and racism, and snatch a habitable planet from the jaws of extractive capitalism, it will be thanks to technologically enabled organizing.

See perhaps: [[Digital technologies are an important part of movement infrastructure]].

> Tech is the terrain on which our future fights will be fought. If we can’t seize the means of computation, we will lose the fight before it is even joined.

<!--quoteend-->

> Fixing tech isn’t more important than fixing everything else, but unless we fix tech, we can forget about winning any of those other fights.

^ interesting claim - would like to explore more


## Interoperability

The main thrust of the book.  [[Taxonomy of interoperability]].

He describes Microsoft Windows and Office monopoly. And how Apple reverse engineering them is an example of [[competitive compatibility]].

