# Carbon farming

> Soil is the planet’s second-biggest carbon vault after oceans, storing around 51 billion tons of CO2-equivalent in Europe alone. “Carbon farming” aims to augment and preserve underground stores by offering farmers incentives to adopt beneficial techniques. These could include: agroforestry - the greater use of woody trees and shrubs in farming; the restoration of peatlands and wetlands to increase carbon sequestration; or the targeted conversion of cropland to fallow, nature-friendly and biodiverse land. 
> 
> &#x2013; Down to Earth newsletter

