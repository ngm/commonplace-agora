# The information necessary for repair should be easily accessible

A
: [[claim]]

Because relevant repair information is a big part of completing a successful repair.

[[right to repair]]. [[access to repair information]].

