# US intervention

> The United States responded to Allende’s election by adopting a “non-overt course” to prevent Chile from turning socialist. This included funding government opposition parties and opposition-owned media outlets and sabotaging the Chilean economy
> 
> &#x2013; [[Cybernetic Revolutionaries]]

