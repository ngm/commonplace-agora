# Sharing information systems is praxis for the hacker class

-   I kind of see something like Ton's public sharing of the details of his [[PKM]] as part of a body of artifacts for the [[Hacker class]].
    -   That perhaps sounds a bit grandiose, but hey if [[Vectoralism]] is about appropriation of knowledge work and [[commodification of information]], then open descriptions and sharing of how we do knowledge work without [[Vectoralist class]] tools is a small counter to that.

