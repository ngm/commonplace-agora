# real subsumption

> Marx lists some of the more destructive aspects of real subsumption: “production for production’s sake,” overproduction, and the creation of a surplus population unnecessary to existing production
> 
> &#x2013; [[Breaking Things at Work]]

