# Emergent outlines



## Vectoralism, PKMs and IndieWeb


### Notes

-   [[Vectoralism]], [[PKM]]s and the [[IndieWeb]].


### Notions

-   [[Sharing information systems is praxis for the hacker class]]
    -   is it though?  a little bit grandiose.
    -   is anti-drm etc also praxis?


## The last thing we need is more tech growth

[[New UK tech regulator to limit power of Google and Facebook]].

The last thing we need is more tech growth.

The framing of this is all wrong.  It's all about innovation, competition, consumer choice, growth.  It should be about [[liberation]], [[user freedom]], [[agency]] and [[sustainability]].  

-   what's wrong with tech growth?  Reference Hail the maintainers maybe.


## [[Reweirding the web]]


## [[Experiments in community (digital) gardening]]


## Grab bag of ideas I might write about


### [[Between the horizontal and the vertical]]


### [[Municipalist social media]]


### [[Review of Half-Earth Socialism]]


### [[The role of technology in eco-socialism]]


### analysis vs skills when it comes to **online** political organising


### how do political theory and tactics transfer to online space


### some thoughts on what a useful governable stack for online political organising might be


### real existing movements - cooperation jackson, rojava


### the issues we want to address and the demands that we can make as flancia collective


### horizontalism and verticalism as it relates to technological decentralisation

e.g. Fediverse, Indieweb, p2p.  They are mixtures of both horizontal and vertical.


### analogies between political organisation and evolutionary and adaptive systems

Even between [[planned economy]] versus [[free market economy]], at least insofar as encapsulated by [[Soviet Union]] and [[United States]] during the cold war, I feel like there must be a middle-way that's about [[prosperity without growth]], somewhere between planned and free market, neither vertical nor horizontal.

[[The Entropy of Capitalism]] might be a good one for this.  System theory and self-organisation.

