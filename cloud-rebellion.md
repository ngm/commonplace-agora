# cloud rebellion

A term from [[Yanis Varoufakis]] in [[technofeudalism]].

The rebellion is against the big tech feudal lords.

> At the very end of the book, Varoufakis calls for "a cloud rebellion to overthrow technofeudalism." This section is very short – and short on details.
> 
> &#x2013; [[Yanis Varoufakis's "Technofeudalism: What Killed Capitalism?"]]

