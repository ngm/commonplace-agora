# Cuba is a good model of a green society

[[Cuba]] is a good model of a [[green society]].

I've read or listened to a few things lately that have suggested this.
e.g. [[Cuba's Life Task]], [[Socialist States &amp; the Environment w/ Salvatore Engel-Di Mauro]].

I don't know enough to yet state strong agreement with the claim.
It sounds pretty legit, but there's a chance that it could be propaganda or romanticism.


## Because

-   [[Cuba is world-leading in agroecology]]
-   Cuba has enshrined protection of the natural world in its constitution ([[Cuba's Life Task]])
-   Cuba has moved away from fossil fuels ([[Cuba's Life Task]])
-   Cuba works with its citizens to transform the economy ([[Cuba's Life Task]])


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

