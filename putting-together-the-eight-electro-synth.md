# Putting together the EIGHT Electro Synth

The EIGHT Electr Synth (https://eight-innovation.com/electronic-kits/) is a fun little kit where you 'build' your own toy synthesiser.

I got given this as a Christmas (or birthday?) present in Winter 2022/23.

I finally got around to making it in December 2023.

It was loads of fun to do.  Instruction were easy to follow.

Great soldering practice for a soldering newbie.

Lots of different components to solder together in various different ways.

I used my [[Pinecil]] to do it, and it worked great.

The synth itself isn't amazing, in case that's what you're going for.  The fun in this is doing the soldering.

