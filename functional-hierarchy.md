# Functional hierarchy

> [[Occupy Wall Street]] illustrates one way in which a functional hierarchy can be maintained in an organisation which aims to have a nonhierarchical (in terms of structural or anatomical hierarchy) structure.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> According to McEwan, the concept of anatomical hierarchy refers to what we normally understand by the term ‘hierarchy’: different levels in an organisation with a chain of command running between then and with lower levels subordinate to higher ones. This is hierarchy within the structure or anatomy of an organisation, hence McEwan calling it anatomical. Functional hierarchy, on the other hand, concerns a situation where ‘there are two or more levels of information structure operating in the system’
> 
> &#x2013; [[Anarchist Cybernetics]]

