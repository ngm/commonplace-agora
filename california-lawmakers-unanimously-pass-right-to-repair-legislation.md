# California Lawmakers Unanimously Pass Right to Repair Legislation

URL
: https://www.404media.co/california-assembly-unanimously-passes-right-to-repair-legislation/

[[California]]n lawmakers unanimously pass [[right to repair legislation]].
[[Right to repair in the US]].

> The law requires electronics manufacturers to sell repair tools and parts to consumers and to make repair guides available to the general public.

<!--quoteend-->

> If Newsom signs the bill into law, it will be a victory not just for Californians but for consumers everywhere, because so many people live in California that it would be difficult to keep access to parts, guides, and tools limited to people in California only (and it’s not clear that any manufacturer would want to, anyway).

Don't really understand how US politics works though - it still has some steps to go through, I think, before it **really** happens.

