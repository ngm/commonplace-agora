# Future Histories



## Book

A
: [[book]]

URL
: https://www.versobooks.com/books/2960-future-histories

Author
: [[Lizzie O'Shea]]

![[images/future-histories.jpg]]

> What, she asks, can the [[Paris Commune]] tell us about earlier experiments in sharing resources—like the Internet—in common? Can debates over digital access be guided by Tom Paine’s theories of democratic economic redistribution? And how is Elon Musk not a visionary but a throwback to Victorian-era utopians?

Right up my street, in that it is linking fairly leftist ideas from history to modern issues with digital technology.


### [[Review: Future Histories]]


### Thoughts along the way

It’s really nicely written, and weaves together current social, political and economic technological quandaries with a reading of relevant ideas from history. I really like the historical perspective – it gives a nice handle with which to grapple with these problems.

Like a lot of books I’ve read lately though, so far it’s heavy on the diagnosis, and light on the actual treatment.  But I’m only at the beginning so I hope it will flesh out with some concrete action as I go along.

> We need social movements that collaborate—in workplaces, schools, community spaces and the streets—to demand that the development of technology be brought under more democratic forms of power rather than corporations or the state.

True enough.  Although I am unaware of what form it would take. Who is in these social movements? To whom are the demands made? What are they exactly?

> As the planet slides further toward a potential future of catastrophic climate change, and as society glorifies billionaires while billions languish in poverty, digital technology could be a tool for arresting capitalism’s death drive and radically transforming the prospects of humanity. But this requires that we politically organize to demand something different.

Totally agree with the sentiment. But who is we? What organizational form should we take? What is the demand we should be making?

It got a bit wordy at times - repeating the same message over and again.

Each chapter has a few ideas for what to do. More broad strokes of legislative or policy demands, less immediate opportunities for praxis. But good jumping off points.


### Themes


#### [[Technology capitalism]]


#### [[Biased data sets]]


#### [[Keystroke patterns as biometric profiling]]


#### [[The police]]


#### [[Digital urban planning]]


#### [[UBI]], [[UBS]], and a [[job guarantee]]


#### [[Autonomy]] and [[digital self-determination]]


#### Indigenous culture and ownership and governance


#### The [[commons]] / [[digital commons]]


## Podcast series

A
: [[podcast series]]

URL
: https://www.futurehistories.today/

Has a lot of great stuff related to [[planning]], [[socialist calculation debate]], etc.

