# Food strategy for England likely to be watered down

URL
: https://www.theguardian.com/environment/2022/jun/07/food-strategy-for-england-likely-to-be-watered-down

UK [[National Food Strategy]] is going to be a bit rubbish.

> Experts consulted on the strategy pushed for a reduction in intensive animal agriculture and mandatory reporting for retailers on how much [[animal protein]], compared with [[plant protein]], they sell.

<!--quoteend-->

> Greenpeace has called for a shift towards plant-based protein, and the Soil Association agrees, arguing that any meat should be produced in a regenerative system, with more land being used to grow crops for human consumption rather than to be fed to animals or used for intensive animal agriculture

