# Thomas Spence

> Thomas Spence (1750-1814) is all but forgotten today, but at the time of the [[French Revolution]] he was one of the best-known thinkers and activists in the left-wing of the radical democratic movement in England.
> 
> &#x2013; [[A forgotten revolutionary: Thomas Spence on saving the commons]]

<!--quoteend-->

> In Red Round Globe Hot Burning, Peter Linebaugh describes him as “the most consistent among the common communists of the 1790s.” His influence continued after his death: in 1817, Parliament banned political clubs that supported his views, making Spenceanism the only political ideology ever banned by name in England.
> 
> &#x2013; [[A forgotten revolutionary: Thomas Spence on saving the commons]]

