# Drax

> Drax, which owns the vast power station complex in North Yorkshire and is Britain’s single biggest source of carbon emissions
> 
> &#x2013; [Energy bills may rise if government gives Drax more support, say MPs | Drax |&#x2026;](https://www.theguardian.com/business/2022/sep/20/energy-bills-drax-carbon-emissions)

