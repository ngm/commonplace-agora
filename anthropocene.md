# Anthropocene

> In addition to [[global warming]], [[biodiversity loss]], [[ocean acidification]], and land-use change through deforestation, nitrogen and phosphorus inputs into the biosphere and atmosphere have already reached or even exceeded carrying capacity limits.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

[[nitrogen and phosphorus pollution]].

> For more than 200 years, the capitalist mode of production has cultivated a [[social metabolism]] with nature that has changed the Earth system to such an extent that at least since the great acceleration after the Second World War, the Earth has entered a new Earth-historical epoch, what Earth system scientists are calling the Anthropocene.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

