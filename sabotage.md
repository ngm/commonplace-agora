# Sabotage

> “Sabotage” at this time could mean any number of subversive techniques employed by a worker that interfered with production, including absenteeism, work slowdowns, “working to rule” by following only explicit requirements of the job, even striking. It could refer to disrupting equipment— “the displacement of parts of machinery or the disarrangement of a whole machine”—but it could also mean creating better-quality products than owners intended.
> 
> &#x2013; [[Breaking Things at Work]]

