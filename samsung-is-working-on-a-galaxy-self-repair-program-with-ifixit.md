# Samsung is working on a Galaxy self-repair program with iFixit

URL
: https://www.theverge.com/2022/3/31/23004309/samsung-self-repair-service-program-ifixit-right-to-repair-galaxy

[[Samsung]] is working on a Galaxy self-repair program with [[iFixit]].  It will include [[spare parts]], tools, and guides ([[repair information]]).

[[right to repair]].

> Coming this summer for the Galaxy S20, S21, and Tab S7 Plus

<!--quoteend-->

> [[Samsung]] announced a new self-repair program that will give Galaxy customers access to parts, tools, and guides to repair their own devices

Interesting.

But, batteries not included.  Bit rubbish, given how batteries are one of the most common failure modes. (https://openrepair.org/open-data/insights/mobiles/)

