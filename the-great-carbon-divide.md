# The great carbon divide

An
: [[article]]

Found at
: https://www.theguardian.com/environment/ng-interactive/2023/nov/20/the-great-carbon-divide-climate-chasm-rich-poor

[[Carbon inequality]].

> Income gaps – and therefore carbon gaps – may have narrowed between countries but they have widened within them. Responsibility for the ongoing climate crisis is becoming more concentrated, while its impacts are spreading.

<!--quoteend-->

> the richest 1% of the population produced as much carbon pollution in one year as the 5 billion people who make up the poorest two-thirds

<!--quoteend-->

> inequality between people has increasingly become a structural impediment to climate justice and climate action.

<!--quoteend-->

> the climate crisis worsens inequality and inequality worsens the climate crisis

[[polluter elite]]

