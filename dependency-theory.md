# Dependency theory

Part of [[international relations]].

> Dependency theory is the idea that resources flow from a "periphery" of poor and exploited states to a "core" of wealthy states, enriching the latter at the expense of the former.
> 
> &#x2013; [Dependency theory - Wikipedia](https://en.m.wikipedia.org/wiki/Dependency_theory)

Sounds like [[The Divide]] must be inspired by this?

