# Dynabook

> In 1972, [[Alan Kay]] envisioned a “dynamic medium for creative thought” which he called a Dynabook.
> 
> &#x2013; [GitHub - seandenigris/Dynabook](https://github.com/seandenigris/Dynabook) 

