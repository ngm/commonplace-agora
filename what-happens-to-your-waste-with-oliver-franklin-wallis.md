# What happens to your waste? with Oliver Franklin-Wallis

URL
: https://therestartproject.org/podcast/oliver-franklin-wallis/

Featuring
: [[Oliver Franklin-Wallis]]

Discussing [[waste]], [[waste streams]], [[recycling]] etc and his book [[Wasteland]].

