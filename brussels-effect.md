# Brussels effect

> EU’s legislative power often functions as a stepping stone towards global standards, which some refer to as “the Brussels effect”. The term refers to the regulatory power of the EU, which influences companies globally and pressurizes them to adopt EU compliance standards. By implementing EU standards to their global operations, these companies reduce costs and the complication of complying with multiple regulatory regimes.
> 
> &#x2013; [[Taming Big Techification? The European Digital Markets Act]]

