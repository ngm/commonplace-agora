# Mass surveillance

> Under the laws, encryption – which keeps communications on the app private and inaccessible to outside parties – would have to be removed from WhatsApp in India and messages would have to be put into a “traceable” database. The government would then be able to identify and take action against the sender if any content was ruled “unlawful
> &#x2013; [WhatsApp sues Indian government over ‘mass surveillance’ internet laws | Indi&#x2026;](https://www.theguardian.com/world/2021/may/26/whatsapp-sues-indian-government-over-mass-surveillance-internet-laws) 

<!--quoteend-->

> The legal challenge is the latest escalation of a battle between big tech companies which have a huge and growing user base in India, and the Indian government, led by prime minister [[Narendra Modi]], which has brought in increasingly heavy-handed measures to regulate the online sphere, which is seen as a space for dissent.
> 
> &#x2013; [WhatsApp sues Indian government over ‘mass surveillance’ internet laws | Indi&#x2026;](https://www.theguardian.com/world/2021/may/26/whatsapp-sues-indian-government-over-mass-surveillance-internet-laws) 

