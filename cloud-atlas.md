# Cloud Atlas



## Book

WLTR


## Film

A
: [[film]]

I thought it was really good.  Entertainingly done and plenty of thoughts about [[human nature]] to cog on.

Seems to be about some grand themes of humanity.  Mostly bad ones.  [[Greed]], [[power]], [[enslavement]].  But also [[freedom]] and [[resistance]], I guess.

I'm not entirely sure of the relevance of the hints at [[reincarnation]] and past lives.  Maybe just a means to show that these aspects of nature reappear throughout history.

This [Slate article](https://slate.com/culture/2012/10/cloud-atlas-meaning-what-does-the-wachowskis-movie-say-about-reincarnation-revolution-and-human-progress.html) mentions:

-   [[the interconnectedness of life]]
-   [[prejudice]] and [[oppression]]
-   [[revolution]] and [[change]]

References [[Aleksandr Solzhenitsyn]] a bit.

There's also a definite [[climate change]] undertow.  Old Seoul is under water - if levels keep rising, New Seoul will end up the same.  I remember Meronym mentions something about despite their advanced civilisation, the Old'Uns' fatal flaw was of always wanting more.  Greed again.


### Race

Slightly problematic representation of race in the film, with white actors portraying Asian characters - is it yellowface?  The counterpoint being that all actors are made up to represent different races at different points in the film, and that it is meant to represent the interconnectedness of all peoples.  That said - would have been better to have another Asian lead.

