# Meat consumption

Meat production is a major contributor to global heating.

> About 300 million metric tonnes of meat were consumed in 2018. Meat and dairy alone account for 14.5% of all greenhouse gas emissions, but they also account for most deforestation
> 
> &#x2013; [[For a Red Zoopolis]]

