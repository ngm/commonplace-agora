# UK politics



## Labour policies

-   Green New Deal with 2030 net zero target
    -   this sets the paces of it - it means you have to start doing radical things now
    -   you have to start talking about economic transition
    -   it changes the frame of all other policies as well
    -   some of the big unions are on board
    -   syndicalist approach part of it - industrial revolution from below, what does it look like for towns and cities?
-   4 day working week
-   public ownership of pharmaceutical research


## Regional politics and the [[politics and the North-West of England]]


## [[General Election 2019]]

