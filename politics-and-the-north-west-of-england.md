# Politics and the North-West of England



## History

[[Industrial revolution]] and [[labour movements]] are strongly linked to the North-West of England.

[[Engels]] wrote about [[the conditions of the working class in England]].  Some parts (all?) centred around [[Manchester]].

Also the history of the [[textile industry]], a bit part of the history of the industrial revolution, is very closely linked to Lancashire and Manchester.


## [[Salford]]


## Present

-   [Brexit breakdown: a big day in the north](https://www.invidio.us/watch?v=y4uIC0AwD68), Anywhere But Westminster
-   [Yes, we need climate action; but it needs to be rooted in people’s daily reality](https://www.hopenothate.org.uk/yes-we-need-climate-action-but-it-needs-to-be-rooted-in-peoples-daily-reality/), Lisa Nandy

