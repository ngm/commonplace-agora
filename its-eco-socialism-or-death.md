# "It's Eco-Socialism or Death"

URL
: https://jacobin.com/2019/02/kali-akuno-interview-climate-change-cooperation-jackson

Interview with [[Kali Akuno]].  [[Eco-socialism]].

> One, we have to organize a mass base within the working class, particularly around the job-focused side of the [[just transition]] framework. We have to articulate a program that concretely addresses the class’s immediate and medium-term need for jobs and stable income around the expansion of existing “green” industries and the development of new ones, like digital fabrication or what we call community production, that will enable a comprehensive energy and consumption transition

<!--quoteend-->

> We have to weaken their ability to extract, and this entails stopping new exploration and production initiatives. This is critical because it will weaken their power, particularly their financial power, which is at the heart of their lobbying power. If we can break that, we won’t have to worry about the centrists, as you put it.

