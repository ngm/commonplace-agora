# Strategy and tactics

> tactics as that which focuses on specific engagements and strategy as that which brings those engagements together in working towards a common goal.
> 
> &#x2013; [[Anarchist Cybernetics]]

[[Strategy]].  [[tactics]].

