# Preguntando caminamos

Asking, we walk.

[[Prefiguration]].

> In anarchist theory, prefiguration is often presented as a strategy of direct confrontation with the many forms of injustice, repression, and exploitation that characterize the capitalist order. This means in practice that the radical inversion of such relations and forms of repression entails the hypothetical formulation of alternatives and their continuous reformulation through ‘trial and error.’ The famous Zapatista slogan ‘Preguntando caminamos’ (or, ‘Asking, we walk’) perfectly illustrates such an experimental view on radical political processes.
> 
> &#x2013; M Van de Sande quoted in [[Anarchist Cybernetics]]

