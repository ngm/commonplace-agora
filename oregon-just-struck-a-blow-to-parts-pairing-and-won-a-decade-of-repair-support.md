# Oregon Just Struck a Blow to Parts Pairing and Won a Decade of Repair Support

An
: [[article]]

> Today, the US fight for our [[Right to Repair]] won an enormous victory: [[Oregon]] just passed an electronics Right to Repair law that not only protects owners’ right to get our stuff fixed anywhere we want but also limits the anti-repair practices of [[parts pairing]]

