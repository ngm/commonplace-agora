# Networked agency

A concept from [[Ton]]: https://www.zylstra.org/blog/networked-agency/

> I am convinced that embracing distributed technology and distributed methods and processes allows for an enormous increase in agency. A slightly different agency though: networked agency.
> 
> &#x2013; [On Agency pt. 2: The Elements of Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/2016/08/on-agency-pt-2/) 

Agency is a form of empowerment.  

> creating agency is the primary radical political standpoint one can take.
> 
> &#x2013; [On Agency pt. 2: The Elements of Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/2016/08/on-agency-pt-2/) 

<!--quoteend-->

> In our digital, globally networked and hence more complex age, we need a qualitatively different approach to [[agency]]. 
> 
> -   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)

<!--quoteend-->

> This makes an individual _including its meaningful relations to others_, in a _specific and real life context_ the relevant unit of agency.
> This is networked agency.
> 
> -   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)

^ definitely sounds pretty similar to [[assemblage thinking]] and [[actor-network theory]].  Agency is defined in relation to others, and in a context.

What I like about Ton's approach is that it's very practical.  Ton is rooted in things happening in the real (and technological) world, it's not just theoretical.


### Core concepts


#### striking power

> The ability to (collectively) act and create on your own accord. This is where low-threshold tools are important, as is knowledge of working methods and processes.
> 
> -   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)


#### resilience

> The ability to shield oneself against and mitigate negative consequences of other’s behaviour propagating through the network to you. 
> 
> -   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)


#### agility

> The ability to leverage, adapt and respond to opportunities from other’s behaviour propagating through the network to you.
> 
> -   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)

<!--quoteend-->

> Reducing adoption thresholds of truly distributed technology and peer to peer methods allows an increase in agency.
> 
> &#x2013; [On Agency Pt 1.: Embracing Distributedness to Increase Agency – Interdependen&#x2026;](https://www.zylstra.org/blog/2016/03/on-agency-pt-1/) 


## Twin pages

-   [Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/networked-agency/)

