# Hotel Bar Sessions: Breaking Things at Work (with Gavin Mueller)

A
: [[podcast]]

Found at
: https://hotelbarpodcast.com/podcast/episode-128-breaking-things-at-work-with-gavin-mueller

Part of
: [[Hotel Bar Sessions]]

Featuring
: [[Gavin Mueller]]

[[Luddism]] and [[neo-Luddism]]. In relation to [[Breaking Things at Work]].

Claim: [[The free software movement is an example of neo-Luddism]]. Hackers were clearly not anti-technology, they were concerned about the enclosure of technology for capitalism.

They ask: what could technology created under a different value system look like? (Maybe [[Technology Networks]]. Or [[Project Cybersyn]].)

He gives [[right to repair]] as another example of neo-Luddism. ([[The right to repair movement is an example of neo-Luddism]].)
Right on. With that and free software, I seem to fit the description.

