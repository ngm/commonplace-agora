# Benefits need to increase

[[Britain's welfare system]]. [[the year of the squeeze]]


## Because

-   [[The basic rate of out-of-work benefits is at its lowest for 30 years]]
-   Britain has soaring prices of food
-   Britain has soaring prices of rent
-   [[Britain has soaring energy bills]]
-   These are forcing families to choose between basic essentials such as food and heat - [[Heat or eat]]
-   Growing numbers are being forced into debt and relying on [[food banks]]


## Related

-   Very similar to [[Britain's welfare system is "unfit for purpose" and in urgent need of reform]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

I agree.  I wonder where [[Mutual aid]] could also help.  [[Solidarity, not charity]].

