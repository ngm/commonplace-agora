# Telecommunism

> Telekommunisten promotes the notion of a distributed communism: a communism at a distance, a Tele-communism.
> 
> &#x2013; [[The Telekommunist Manifesto]] 

<!--quoteend-->

> Advocates of communism have long described communities of equals; **peer-to-peer networks implement such relations in their architecture.** 
> &#x2013; [[The Telekommunist Manifesto]] 

^ this feels very related to [[info civics]].

> Conversely, capitalism depends on privilege and control, features that, in computer networks, can only be engineered into centralized, client-server applications.
> 
> &#x2013; [[The Telekommunist Manifesto]] 

[[Communism]].

