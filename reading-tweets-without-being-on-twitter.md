# reading tweets without being on Twitter

I want to stay up to date with what's happening in my local area.  But I can't do that via Indieweb, Fediverse, or just plain old RSS right now.  All the local organisations post their updates on Twitter or Facebook.


## Different approaches

-   Currently:  [[Following Twitter people via Nitter and Miniflux]].
-   I have done this in the past in an Indieweb social reader-y way: https://doubleloop.net/2019/03/12/following-twitter-peeps-in-an-indiereader-with-granary-io-and-microsub/

