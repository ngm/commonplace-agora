# Distributed applications can only do what their protocols support

-   from [[Information Civics]]

Distributed applications must work via shared protocols.  It takes a while to change a protocol, and requires a high degree of consensus.  You can see this as a good thing.

> The value of distributed networking is not only that it constrains authority. Distributed applications are designed to transact between peers, and the peers must agree upon the terms of each transaction. Therefore distributed applications can only do what their protocols support.
> 
> &#x2013; [[Information Civics]]

<!--quoteend-->

> Used correctly, the inefficiency of protocol development can be a feature, not a bug. Protocols require a high degree of consensus among stakeholders to change. Any properties which are encoded into the protocol are therefore protected by the inefficiency of deployment. If a change would strip away a property which too many users want, they can refuse the change.
> 
> &#x2013; [[Information Civics]]

