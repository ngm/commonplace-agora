# Manston

> At Manston, another military base along the coast, which provides basic temporary accommodation in a series of marquees, the Home Office started to process small boat arrivals in February. That site too has been hit by a series of scandals including reports of infectious diseases such as diphtheria, drug-selling by guards to asylum seekers and some new arrivals released from Manston and dumped in central London. That site is locked and on 30 October protesters filmed footage of young children shouting “Freedom!” through the fence. Since then that site too has been wrapped in swathes of tarpaulin.
> 
> &#x2013; [Cold comfort Kent: where small-boat arrivals receive a fraying welcome | Immi&#x2026;](https://www.theguardian.com/uk-news/2022/nov/21/cold-comfort-kent-where-small-boat-arrivals-receive-a-fraying-welcome)

