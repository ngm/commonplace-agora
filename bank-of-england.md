# Bank of England

> In the days before that meeting, she read up on the history of the Bank of England, and saw that it was important in the financial history of the world. 1694: Charles II and William III had been borrowing money from private banks and not paying them back, or else levying taxes on all kinds of activities to be able to afford to make their debt payments, and thus making life more expensive for everyone, except for the royals involved, who were less and less liable for their profligate spending. So a Scottish merchant, William Patterson, proposed that 1,268 creditors lend the English king 1.2 million pounds for a guaranteed rate of interest of eight percent, and once William III signed off on that, a big piece of the system of the current world fell into place. The capitalizing of state power now had its roots in private wealth; thus the rich and the state became co-dependents, two aspects of the same power structure.
> 
> &#x2013; [[The Ministry for the Future]]

