# Tools for Conviviality

[[Ivan Illich]]

See [[Convivial Tools]].

> described a vision of a world in which a community of users develop and maintain their own tools
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> I choose the term “conviviality” to designate the opposite of industrial productivity

<!--quoteend-->

> The transition to socialism cannot be effected without an inversion of our present institutions and the substitution of convivial for industrial tools.

