# ecological civilization

A vision for a future society.  [[Jeremy Lent]]'s idea, I think. I think it's a good one.

> This is the fundamental idea underlying an ecological civilization: using nature’s own design principles to reimagine the basis of our civilization.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> An ecological civilization would incorporate government spending and markets, but—as laid out by visionary economist Kate Raworth—would add two critical realms to this framework: households and the commons.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> Technological innovation would still be encouraged, but would be prized for its effectiveness in enhancing symbiosis between people and with living systems, rather than minting billionaires.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> Manufacturing would be structured around circular material flows, and locally owned cooperatives would become the default organizational structure.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> Online networks with scale, such as Facebook, would be turned over to the commons, so that rather than manipulating users to maximize advertising dollars, the internet could become a vehicle for humanity to develop a planetary consciousness.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> Governance would be transformed with local, regional, and global decisions made at the levels where their effects are felt most (known as [[subsidiarity]]).
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> While much decision-making would devolve to lower levels, a stronger global governance would enforce rules on planetwide challenges such as the climate emergency and the sixth great extinction.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

