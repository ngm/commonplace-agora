# Omicron will not fizzle out in the foreseeable future

[What lies on the other side of the UK’s Omicron wave? | Omicron variant | The&#x2026;](https://www.theguardian.com/world/2022/jan/13/what-lies-on-the-other-side-of-the-uks-omicron-wave)

> Not in the foreseeable future. The more likely scenario is that Omicron continues to circulate, with cases rising and falling in line with people’s mixing patterns and changes to measures that prevent transmission. When plan B is lifted and more people return to work, cases may well rise. But in the summer, as people spend more time outdoors, infections may fall again, then rise next winter. It will all come down to human behaviour. People may feel safer and socialise more as cases come down, sending infections up again. The virus won’t be wiped out because waning immunity means there will be a constant supply of people who are newly susceptible to the infection.

