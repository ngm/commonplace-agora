# Austerity

> As figures show from the last decade of austerity, the most vulnerable were hit the hardest and many were dragged into economic hardship while it had little to no effect on the income of the wealthiest.

<!--quoteend-->

> Austerity cuts to the NHS, public health and social care have killed tens of thousands more people in England than expected, according to the largest study of its kind.
> 
> Researchers who analysed the joint impact of cuts to healthcare, public health and social care since 2010 found that even in just the following four years the spending squeeze was linked with 57,550 more deaths than would have been expected.
> 
> &#x2013; [Austerity in England linked to more than 50,000 extra deaths in five years](https://www.theguardian.com/society/2021/oct/14/austerity-in-england-linked-to-more-than-50000-extra-deaths-in-five-years)

