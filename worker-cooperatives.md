# Worker cooperatives

Antidote to long hours, zero hours, crap pay, precarious employment, alienation?

Owned and collectively controlled by the workers.  Run for their benefit.  Organised democratically.  No boss.  Workers are the company directors.  Equal say in how the business is run, how everyone gets paid, and what happens with the profits.

Go against the grain of competition and individual success and workplace hierarchy. 

Can still be a tough gig.  Collective decision-making can be time consuming.  Surviving under capitalism while sticking to your principles can be difficult.

Coops are resilient compared to traditional start-ups.

Nice short video intro to worker coops - [Could Worker Co-ops Fix Our Broken Economy?](https://www.youtube.com/watch?v=yjbtCVTWCas&feature=youtu.be) 

> Worker cooperatives are a real utopia that emerged alongside the development of capitalism. Three important emancipatory ideals are equality, democracy, and solidarity. All of these are obstructed in capitalist firms, where power is concentrated in the hands of owners and their surrogates, internal resources and opportunities are distributed in a grossly unequal manner, and competition continually undermines solidarity.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


### TODO Need to look up Wright's idea of a real utopia again

> In a worker-owned cooperative, all of the assets of the firms are jointly owned by the employees themselves, who also govern the firm in a one-person-one-vote, democratic manner. In a small cooperative, this democratic governance can be organized in the form of general assemblies of all members; in larger cooperatives the workers elect boards of directors to oversee the firm.
> 
> They may also embody more capitalistic features: they may, for example, hire temporary workers or be inhospitable to potential members of particular ethnic or racial groups. Cooperatives, therefore, often embody quite contradictory values.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


## Benefits

> With worker cooperatives being more productive, less likely to go bankrupt and being the only form of enterprise shown to improve trust between workers, 
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Challenges

> Though local coops can be developed, they cannot overturn the national legal system which in the UK that remains hostile to co-operatives. Co-operative development agencies of regional government can undoubtedly boost the co-operative economy but Westminster can unshackle these efforts if used in tandem.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Critiques


### Versus unionism

-   [You can’t win without a fight: Why worker cooperatives are a bad strategy](https://organizing.work/2021/01/you-cant-win-without-a-fight-why-worker-cooperatives-are-a-bad-strategy/)
    -   discussion @ [Worker Co-operatives](https://www.facebook.com/groups/workercoops/permalink/10159408840364101/)
    -   discussion @ [social.coop](https://social.coop/@neil/105644155884883891)
    -   discussion @ [lemmy](https://lemmy.ml/post/51317)

