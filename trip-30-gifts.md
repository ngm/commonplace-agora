# Trip 30: Gifts

URL
: https://novaramedia.com/2022/12/18/acfm-trip-30-gifts/

> Nadia, Jem and Keir unwrap the cultural and economic pressures on doing pressies in a loosely festive edition of ACFM. Talking about Santa’s workshop, energy price controls and the lavish tradition of potlatch, they bring in the works of Marcel Mauss and David Graeber plus music from The Velvet Underground and Lee ‘Scratch’ Perry.

