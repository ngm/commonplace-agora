# Fibonacci numbers

![[2020-06-15_22-08-50_dd4dc1f29bb9cc83bfed0cc015c7557f--spirals-in-nature-fibonacci-spiral.jpg]]

These came up in discussion with E and A today, after we saw a pine cone.  I've long liked the Fibonacci sequence because of how they pop up in nature.

> Fibonacci sequences appear in biological settings, such as branching in trees, arrangement of leaves on a stem, the fruitlets of a pineapple, the flowering of artichoke, an uncurling fern and the arrangement of a pine cone, and the family tree of honeybees.
> 
> &#x2013; [Fibonacci number - Wikipedia](https://en.wikipedia.org/wiki/Fibonacci_number#Nature) 

