# Four out of five members of the public support the use of onshore wind farms

Four out of five members of the public support the use of [[onshore wind]] farms.

So sayeth [The Guardian view on Boris Johnson’s energy strategy: missed opportunities | &#x2026;](https://www.theguardian.com/commentisfree/2022/apr/07/the-guardian-view-on-boris-johnsons-energy-strategy-missed-opportunities)

