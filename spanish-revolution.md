# Spanish Revolution

[[Spanish Civil War]].

> In Barcelona in July 1936, when the generals initiated a coup to impose fascism across Spain, it was the resistance of poorly armed militias in Barcelona set up by anarchist union the CNT and the Marxist party, the POUM, that persuaded a section of the army, the Assault Guards, to join the resistance.
> 
> &#x2013; [[Revolution in the 21st century - Chris Harman]]

