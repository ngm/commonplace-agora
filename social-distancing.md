# Social distancing



## What is social distancing?


### As of 23rd March 2020:

> Social distancing measures are steps you can take to reduce social interaction between people. This will help reduce the transmission of coronavirus (COVID-19).
> 
> They are to:
> 
> -   Avoid contact with someone who is displaying symptoms of coronavirus (COVID-19). These symptoms include high temperature and/or new and continuous cough
> -   Avoid non-essential use of public transport when possible
> -   Work from home, where possible. Your employer should support you to do this. Please refer to employer guidance for more information
> -   Avoid large and small gatherings in public spaces, noting that pubs, restaurants, leisure centres and similar venues are currently shut as infections spread easily in closed spaces where people gather together.
> -   Avoid gatherings with friends and family. Keep in touch using remote technology such as phone, internet, and social media
> -   Use telephone or online services to contact your GP or other essential services
> 
> Everyone should be trying to follow these measures as much as is practicable.
> 
> &#x2013;  [Guidance on social distancing for everyone in the UK - GOV.UK](https://www.gov.uk/government/publications/covid-19-guidance-on-social-distancing-and-for-vulnerable-people/guidance-on-social-distancing-for-everyone-in-the-uk-and-protecting-older-people-and-vulnerable-adults) 

<!--quoteend-->

> You can also go for a walk or exercise outdoors if you stay more than 2 metres from others
> 
> &#x2013;  [Guidance on social distancing for everyone in the UK - GOV.UK](https://www.gov.uk/government/publications/covid-19-guidance-on-social-distancing-and-for-vulnerable-people/guidance-on-social-distancing-for-everyone-in-the-uk-and-protecting-older-people-and-vulnerable-adults) 

<!--quoteend-->

> What am I allowed to do when social distancing?
> 
> -   You can see family and friends if it's essential
> -   You can walk your dog
> -   You can provide essential care for elderly relatives and neighbours if you have no symptoms
> -   You can go to the shops to buy food and groceries
> -   You can exercise at a safe distance from others
> 
> &#x2013; [Coronavirus: What are social distancing and self-isolation? - BBC News](https://www.bbc.co.uk/news/uk-51506729)

