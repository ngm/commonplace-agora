# Upgrading Linux Mint 19.3 to Linux Mint 20

Upgrading [[Linux Mint]] from 19.3 to 20.

All seems to have gone pretty smoothly so far.

Just followed the instructions here: https://linuxmint-user-guide.readthedocs.io/en/latest/upgrade-to-mint-20.html

I feel like some of the removal of 'foreign packages' that it suggested might bite me at some point, but let's see.  I also cleaned out 13G(!) of apparently unused flatpak gubbins.  It may be that I was using something these&#x2026; but `flatpak list` didn't show anything.

A few minor things, like some of my desktop environment preferences seem to have changed / gotten lost.


## Things that needed installing again

-   Signal Desktop https://signal.org/download/
-   syncthing https://apt.syncthing.net/

