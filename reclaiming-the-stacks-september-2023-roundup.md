# Reclaiming the stacks: September 2023 roundup

Life is busy as of late, so this roundup is a little spartan.  But still, read on for some interesting things at the intersection of ecosocialism and ICT.   Articles and thoughts on the problems of [[digital capitalism]], the positive actions of [[digital ecosocialism]], and a look at the systemic side of things and how we can transition from (digital) capitalism to (digital) ecosocialism.

As always my interest is from the angle of [[reclaiming the stacks]] - expropriating information and communications technology from [[Big Tech]] and returning it to the people.


## Systems

I'm still interested in [[qualitative system dynamics modeling]] as an approach to grappling with digital ecosocialism and how to transition to it.  The qualitative system dynamics model used in [[A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]] was built using a triangulation process from individual models.  I'm reading more about that in [[Identifying Strengths and Obstacles to Climate Change Adaptation in the German Agricultural Sector]].

For a qualitative system dynamics model of digital capitalism, digital ecosocialism and the transition from one to the other, one might triangulate from a bunch of different existing models that are out there related to digital ecosocialism. Such as:

-   [[Digital Ecosocialism: Breaking the power of Big Tech]]
-   [[Platform Socialism]]
-   [[Internet for the People]]
-   [[Governable Stacks against Digital Colonialism]]
-   [[Digitalization and the Anthropocene]]
-   [[Leveraging Digital Disruptions for a Climate-Safe and Equitable World: The D<sup>2S</sup> Agenda]]

A new one for the mix that I came across:

-   [[Decentralized and rooted in care: envisioning the digital infrastructures of the future]]

It's perhaps a bit less defined than some of those previous models, but interesting nonetheless, and with a Latin American slant, so a good perspective to have.


## Problems

_Some problems from digital capitalism recently in the news.  To help map them out, I'm tagging them with some of the criteria I [[defined]] in my OU research._

-   [[The environmental impact of a PlayStation 4]]. "It is an exquisite, leanly designed machine pulsing with the exploitation of Earth and its people."
-   [[Google won’t repair cracked Pixel Watch screens]]. [[Google]] offers no [[repair]] options for cracked Pixel Watch screens.


## Actions

> Philosophers have only interpreted the world, in various ways; the point, however, is to change it.
> 
> &#x2013; Karl Marx

_Some latest news on concrete actions that are part of an ecosocialist ICT movement._

-   Lots of interesting initiatives in [[Decentralized and rooted in care: envisioning the digital infrastructures of the future]].
-   Some good right to repair news lately.
    -   [[California Lawmakers Unanimously Pass Right to Repair Legislation]]
    -   [[New EU Rules: Smartphones and Tablets will follow new ecodesign requirements by June 2025!]]
    -   [[STATEMENT: Google announces 10 years of tech support for Chromebooks]]


## Inputs

_Finally, a few other things I've been reading, listening to, and watching that are adjacent to the topics of ecosocialism and ICT._


### Reading

-   [[The Double Objective of Democratic Ecosocialism]]. First I've seen Jason Hickel explicitly mention [[degrowth]] and [[ecosocialism]] together (it's quite likely that he has before, given his outlook - just first time I've noticed it). The prefix of 'Democratic' is interesting though. Deliberate positioning with [[democratic socialism]] I presume, as opposed to say [[degrowth communism]].
-   [[On Technology and Degrowth]].  [[Jason Hickel]] again, on [[green growth]] and [[degrowth]].  "It should be clear from the above that degrowth is best understood as an element within a broader struggle for ecosocialist (and anti-imperialist) transformation."


### Listening

-   [[Kohei Saito on Degrowth Communism]]. Degrowth needs communism, communism needs degrowth.  Perhaps a stronger version of Hickel's democratic ecosocialism.
-   [[W. Brian Arthur (Part 1) on The History of Complexity Economics]]
-   [[What happens to your waste? with Oliver Franklin-Wallis]]. [[Waste]], [[waste streams]], and [[recycling]].


## Until next time

That’s it! See you next month. Until then, you can find latest streams of thoughts over at my [website](https://doubleloop.net).

