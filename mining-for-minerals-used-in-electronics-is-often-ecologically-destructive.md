# Mining for minerals used in electronics is often ecologically destructive

> In addition to overheating the planet, mining for minerals used in electronics — such as cobalt, nickel and lithium — in places like the Democratic Republic of Congo, Chile, Argentina and China is often ecologically destructive.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]


## Resources

-   https://materialsmatter.eu
-   https://therestartproject.org/critical-raw-materials-matter/

