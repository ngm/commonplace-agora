# Israel’s AI Kill List: “Once you go automatic, target generation goes crazy.”

A
: [[podcast]]

Part of
: [[This Machine Kills]]

About [[Israel]]'s AI targeting system, "[[Lavender]]".

Sounds pretty horrific.

Supposedly identities Hamas operatives. But does so very loosely, and casts an overly wide net. Many civilians killed as a result.

Has a [[Project Insight]] vibe.

