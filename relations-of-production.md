# relations of production

Combined with the [[forces of production]], part of a mode of production.

> By "relations of production", Marx and Engels meant the sum total of social relationships that people must enter into in order to survive, to produce, and to reproduce their means of life.
> 
> &#x2013; [Relations of production - Wikipedia](https://en.wikipedia.org/wiki/Relations_of_production) 

