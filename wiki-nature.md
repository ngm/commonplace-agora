# Wiki Nature

> We now understand that this experience of creative emergence is an unfolding that happens through what we might call Stigmergic Iteration, a process that is universal in natural systems that are alive but not found in mechanistic system, those that are artificial. 
> 
> &#x2013; [wiki nature](http://wellspring.fed.wiki/view/wiki-nature) 

