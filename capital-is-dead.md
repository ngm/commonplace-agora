# Capital is Dead

A
: [[book]]

URL
: https://www.versobooks.com/books/3762-capital-is-dead

Expansion and update of ideas from [[A Hacker Manifesto]] e.g. [[Vectoralist class]] and [[Hacker class]].

![[images/capital-is-dead.jpg]]


## Notes

As of 31st October 2020, I've been reading it for a few nights, and enjoying it a lot - much more readable at a first pass, when new to the ideas, than [[A Hacker Manifesto]].

From my totally superficial understanding so far, it's about the idea that there is a new class layered on top of the capitalist class.  Who don't necessarily control the means of production, but that control the vectors of information.

And the hacker class being those that produce information.  Which is being appropriated by the vectoralists.

I feel an affinity to the notion of a [[Hacker class]], of which I am perhaps a member.  I certainly feel antagonism towards the [[Vectoralist class]], and hope for flows of information not controlled by a few.


## Quotes

> You make the information, but like some kind of info-prole, you don't own the information you produce or the means of realizing its value.

<!--quoteend-->

> They gave rise to a strange kind of political economy, one based not only on a scarcity of things but also on an excess of information.

<!--quoteend-->

> The dominant ruling class of our time owns and controls information.

<!--quoteend-->

> It takes about as much infrastructure to organize the information as it does to organize the distribution of the physical stuff that ends up on the shelves

<!--quoteend-->

> There's a world of everyday life the meat grinder doesn't describe from which a surplus is extracted for another's benefit in other ways. You can be someone other than a tenant farmer or an industrial worker and still not be a capitalist or even petit bourgeois.

<!--quoteend-->

> For years I was one of what the so-called alt-right calls a "cul- tural Marxist," interested mostly in what happens in the political and cultural superstructures of modern society, rather than in the technical and economic base. However, trying to understand culture will lead you to understanding media, which will lead you to try to figure out some things about technology. Then it turns out that the genteel forms of Western Marxist thinking taught in universities for several generations now are not good at understanding how the forces of production actually work. That requires some actual technical knowledge and experience, or at least a willingness to concede that others may know about such things and to learn from them. The production of counterhegemonic knowledge can really only be comradely and collaborative.

YES!!!  That's really key for me.  I feel like I'm somewhere in the middle of this theoretical knowledge and technical knowledge.  I'm not going to be the foremost expert in either, but can perhaps work on joining the two.   I hope I can make some kind of contribution there.


## Summaries

My basic summaries.


### Chapter 1: The Sublime Language of My Century

Marxism should not be taken as religion. We need to reread it based on current material conditions. The left sometimes makes it appear as if capitalism is the eternal form, until it transitions to socialism.  But material conditions have changed since the end of the 19th century, so perhaps some of Marxist theory needs to change.

I like the use of  [[détournement]] as a means to break doctrine.

This all makes sense to me, but wondering about how something like e.g. Red Menace would respond to the idea of Marxism being doctrinaire.

I like the references to [[The Matrix]], [[Ancillary Justice]].  Also the Dollhouse, Get Out, and a Cuban sci-fi book called [[The Year 200]] that I would now like to read.

> In our own times this old story was adapted into the belief system of the so-called tech industry, as a part of what Richard Barbrook calls [[the Californian Ideology]]. Into it can be folded certain other variations, about the "[[fourth industrial revolution]]," for example.


## Chapter 2: Capitalism &#x2013; or worse?

I guess Wark's argument is that the forces of production have changed considerably since Marx, being much more information-based than physical now.  And if you analyse the current forces of production, you see that it isn't really capitalism any more (it's something worse).

I guess it hinges on whether you want to call the current situation capitalism with some modifier (neoliberal, late-stage, surveillance, techno, etc) or  look at it as a whole new thing.  I think she makes a pretty compelling argument why we shouldn't just assume that capitalism will continue in perpetuity until we have communism&#x2026; but I'm definitely not a scholar of these things so someone with a bit more theory might be able to counter this idea a bit more.


## Criticism

> I like the point about acknowledging changes in capitalism but I think she overstates the power/freedom to move of the new class. The rise of logistics (which is part of material production) and the very physical fixed-capital-intensive nature of the new economy that Kim Moody talks about in On New Terrain needs to be taken into account, I think.
> 
> &#x2013; [Matt Noyes](https://social.coop/@Matt_Noyes/105091032101011760)

<!--quoteend-->

> I think there have definitely been critical reviews of how she positions it, and obviously there’s a whole discussion growing about [[neofeudalism]] now too and whether we’re actually seeing some kind of formation like that. Maybe it’s my own bias, but I think I’d still say we’re in another stage of capitalism, at least for now.
> 
> &#x2013; Paris Marx (on [[TWSU]] Discord)


## Links

-   [Capital Is Dead: Is This Something Worse? - YouTube](https://www.youtube.com/watch?v=9wBZVEnqocI)
-   https://invidious.tube/watch?v=eiV0wS_in-4


## Raw notes

Page 291 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 05:46]</span></span>:
[Notes]
Computer Professionals for Social Responsibility

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 05:53]</span></span>:
[2. Capitalism—or Worse?]
We don’t spend enough time on how the brain-fryer is a different machine from the meat grinder.

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 05:54]</span></span>:
[2. Capitalism—or Worse?]
They can really only be known through the collaborative production of a critical theory sharing the experiences of many fields. That would include those with a knowledge of information technology, artificial intelligence, supply chain management, material science, computational biology, and much else besides. We’re way past the steam engines that Marx was sketching in his notebooks.

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 05:59]</span></span>:
[2. Capitalism—or Worse?]
It exploits the asymmetry between the little you know and the aggregate it knows—an aggregate it collects based on information you were obliged to “volunteer.”

Page 101 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 06:10]</span></span>:
[2. Capitalism—or Worse?]
In vulgar terms: the capitalist class eats our bodies, the vectoralist class eats our brains.

Page 103 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 06:12]</span></span>:
[3. The Forces of Production]
The Forces of Production

Page 105 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-19 Fri 06:17]</span></span>:
[3. The Forces of Production]
Technology is intimately connected on the one hand to the human and on the other to the nonhuman. Indeed, technology may be the inhuman zone where distinctions between the human and the nonhuman, not to mention anxieties about their permeability, originate.

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:30]</span></span>:
[3. The Forces of Production]
Of course there have been Marxist scientists, even significant ones, despite the Cold War.20 But there has been little since the fifties to match the influence of the Social Relations of Science movement. However, something like it happened later in information science. Richard Stallman, a “red diaper baby” and founder of the free software movement, brought some of his mother’s stubborn militancy to the possibilities and limits of computer science. It was Stallman who made the strongest connection between the everyday life of the hacker and the struggle in and against the commodification of information science.

Page 111 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:30]</span></span>:
[3. The Forces of Production]
Reformulated as a method for organizing knowledge practices, Marxism could be quite compatible with scientific work and could provide procedures for thinking about the place of science within capitalism. This was the basic orientation of the left wing of the Social Relations of Science movement, which was strongest in Britain between the thirties and the fifties.15 This orientation maintained a positive outlook on the potentials of technology, as science applied to the rationalization of social production. Yet it was at the same time highly critical of the subordination of science and technology to the capitalist monopoly firm and the imperialist and militarist state.

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:46]</span></span>:
[3. The Forces of Production]
The Internet came out of the university and took capital by surprise

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:54]</span></span>:
[3. The Forces of Production]
Kleiner’s starting point is the transformation of the forces of production and the pressure this put on the relations of production.

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:55]</span></span>:
[3. The Forces of Production]
Cheap computation plus the Internet vector was supposed to make capitalism more efficient and enable capital to route around the power of workers at the point of production. It did all that, but it also opened the prospect of self-organized peer-to-peer production.

Page 121 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 06:56]</span></span>:
[3. The Forces of Production]
There really could have been a telecommunism (“tele” = “at a distance”). Autonomous producers could cheaply and easily communicate and coordinate. This was a possibility that had to be foreclosed to enable a new kind of capture of the surplus by the rising ruling class that I call the vectoralist class

Page 123 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 07:02]</span></span>:
[3. The Forces of Production]
Drawing on the historical vision of the lapsed socialist Alvin Toffler, the California Ideology proposed a world in which technology itself was the sole transformative force of history.44 The hero of this epical-poetical myth was the entrepreneur, who single-handedly battles against labor, state, and culture to unleash the supposedly “natural” force of technology. Once unbound, technology will show itself to be inherently the vehicle of free markets and a return to Jeffersonian democracy.45 Hence, technology is good in essence.

Page 124 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 07:04]</span></span>:
[3. The Forces of Production]
As Barbrook points out, the development of the forces of production is not magically called into being from the brains of business geniuses. In the case of Silicon Valley, it took a massive amount of state funding, passing through university research labs.47 It may at one point have been quite possible that these developments could have led to a digital agora or commons as well as (or as an alternative to) new forms of class power based on information asymmetries and the surveillance state. What gets erased in both moral fables about technology, the one where its essence is good and the one where its essence is bad, is the struggle over the form the technologies of Internet would take.

Page 126 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 07:10]</span></span>:
[3. The Forces of Production]
What is good or bad about technology is the outcome of class conflict over its form and between more than two classes.

Page 130 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 07:16]</span></span>:
[4. The Class Location Blues]
One has to ask whether the ruling class presiding over this mode of production is still adequately described as capitalist.4 It seems no longer necessary to directly own the means of production. A remarkable amount of the valuation of the leading companies of our time consists not of tangible assets, but rather of information. A company is its brands, its patents, its trademarks, its reputation, its logistics, and perhaps above all its distinctive practices of evaluating information itself.

Page 134 <span class="timestamp-wrapper"><span class="timestamp">[2023-05-20 Sat 07:31]</span></span>:
[4. The Class Location Blues]
The problem with an inherited concept, like inherited money, is that we didn’t make it ourselves and come to take it for granted

