# How to build a new world in the shell of the old

&#x2013; [How to build a new world in the shell of the old](https://theecologist.org/2018/apr/23/how-build-new-world-shell-old) 

-   "As hierarchical society gives way to genuine democracy,

is the institutions we orgnise and experiment with today that will become the replacements."

-   "By meeting basic community needs, such institutions

rupture capitalism's control over people's lives."

-   "Every city has its graveyard of nonprofits, cooperatives,

social clubs and community centres.  Without the more complex infrastructure of a whole solidarity economy ecosystem, our local projects cannot possibly amount to a systemic alternative to capitalism."

-   I like this article.
-   talking about dual power.
-   we build the grassroots movements, but we also have to think about the bigger picture.
-   similar to inventing the future, similar to cooperation

jackson.

-   without a joined up bigger picture, it won't happen.
-   they seem to suggest that both the solidarity economy and social economy/bookchin ideas might be the way of getting

to this point.

