# Using fuzzy links AKA wikilinks in org-roam

Added in org-roam 1.2.2.  These are my notes from when upgrading to that version.

> In this release we support fuzzy links of the form `[[roam:Title]]`, `[[roam:*Headline]]` and `[[roam:Title*Headline]]`. Completion for these fuzzy links is supported via completion-at-point.

I think I might need to set up company-at-point.  Do I have it out of the box?  The double-bracket thing isn't working, anyway.

Hmm, it's because for my `ngm-journal-mode`, my writing mode that I use for files in my org-roam wiki, I turn it off.  I think because it annoyed me with word completions all the time when I was writing.  Maybe there's a way to choose which completions company mode does.  I'll come back to that&#x2026; for now I'll just not turn it off.

From this post,  [How to to get {{Title of the target note}} working with inline autocomplete i&#x2026;](https://org-roam.discourse.group/t/how-to-to-get-title-of-the-target-note-working-with-inline-autocomplete-in-org-roam/782), I can see that

```emacs-lisp
(smartparens-global-mode t)
(setq company-minimum-prefix-length 2)
(setq company-idle-delay 0.25)
(setq company-backends '(company-capf))
```

works kind of.

The company settings are, well, for company, and the smartparens setting is for getting your double square brackets.

The org-manual used to specify a suggested way of getting your `[[` autocompleted and then triggering completion-at-point.

> To easily insert roam links, one may wish to use a package like key-chord.

It's gone now from the manual. It's how I still do it though.

I installed key-chord by adding to spacemacs extra packages.

```emacs-lisp
dotspacemacs-additional-packages '(... key-chord)
```

Then add the recommended function from org-roam docs in to my config.

```emacs-lisp
(key-chord-define org-mode-map "[[" #'ngm/insert-roam-link)

(defun ngm/insert-roam-link ()
    "Inserts an Org-roam link."
    (interactive)
    (insert "[[roam:]]")
    (backward-char 2))
```

OK, so that works OK too.  If I type [[ quickly, I can then start typing the name of the note and get autocompletion.

I think both of those ways work.  I'll stick with the one from the docs.

