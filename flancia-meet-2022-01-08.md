# Flancia Meet 2022-01-08

[[Flancian]] [[Neil]] [[vera]]

-   Happy New Year!
-   Lobby music while waiting for a meeting to start
-   [[one hour for the revolution]]
    -   [[pomodoro praxis]]
-   Agora is still young (started 14 months ago)
-   Flancian would like to make Agora more easily installable
-   Neil has a separate [[Inner garden]] for private life
    -   Using same tool stack for both is good, cross-pollination of tweaks
-   Mobile story for Agora
    -   Social bots are part of this
-   Agora OAuth
    -   nascar problem
-   Twitter as a platform is a jerk, but a lot of good people are on there
    -   So it makes sense to interoperate

