# Public transport

> Close to a hundred cities across the world already have free public transport.
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]

<!--quoteend-->

> well-funded public transport not only benefits those who work on it, but also is essential to decarbonising transport and reducing fossil-fuel-guzzling
> 
> &#x2013; [[End of the World, End of the Month - One Fight]]

