# Software

> Software has become an indispensable resource of modern societies. Whether industrial production, science, public administration, our media consumption or even our everyday communication: almost all areas are now permeated by software. A modern society without the use of software no longer seems conceivable. The existence of and access to software thus becomes a prerequisite for modern social organisation and functioning.
> 
> &#x2013; [[On the Sustainability of Free Software]]

<!--quoteend-->

> But not only our social organization is based on software, so are our machines and our tools as well. Hardware needs software to function and vice versa. All machines and automated systems around us - whether at home, at work, or in public infrastructure - need software as an indispensable resource to do whatever they were designed to do.
> 
> &#x2013; [[On the Sustainability of Free Software]]

<!--quoteend-->

> Both dependencies – the one on the functioning of our machines and the one on the functioning of our social organisation - together make software indispensable for modern, networked societies. 
> 
> &#x2013; [[On the Sustainability of Free Software]]

