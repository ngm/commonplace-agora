# Well-connected

The following are the 42 most well-connected nodes in my garden - that is, those with the most other nodes linking to them.

If a node is well-connected it probably has some importance to me.

I got the idea for listing well-connected nodes from [[Gordon Brander]], here: https://gordonbrander.com/pattern/.

-   [[podcast]] ← 174.
-   [[Free, Fair and Alive]] ← 108.
-   [[claim]] ← 105.
-   [[Half-Earth Socialism]] ← 93.
-   [[IndieWeb]] ← 76.
-   [[book]] ← 65.
-   [[Capitalism]] ← 57.
-   [[Anarchist Cybernetics]] ← 56.
-   [[right to repair]] ← 51.
-   [[org-roam]] ← 51.
-   [[Commons]] ← 51.
-   [[Listened]] ← 50.
-   [[Free software]] ← 49.
-   [[Platform socialism]] ← 42.
-   [[Agora]] ← 41.
-   [[node club]] ← 40.
-   [[The Ministry for the Future]] ← 39.
-   [[Internet for the People]] ← 39.
-   [[article]] ← 38.
-   [[Doughnut Economics]] ← 37.
-   [[Eco-socialism]] ← 35.
-   [[Climate change]] ← 35.
-   [[Degrowth]] ← 35.
-   [[Systems thinking]] ← 32.
-   [[Neither Vertical Nor Horizontal]] ← 31.
-   [[org-mode]] ← 31.
-   [[Governable Stacks against Digital Colonialism]] ← 31.
-   [[YunoHost]] ← 30.
-   [[Nathan Schneider]] ← 29.
-   [[Flancian]] ← 29.
-   [[Ecosocialism]] ← 29.
-   [[Ton]] ← 28.
-   [[Capital is Dead]] ← 27.
-   [[Future Histories]] ← 27.
-   [[cybernetics]] ← 26.
-   [[Cooperatives]] ← 26.
-   [[Emacs]] ← 26.
-   [[commoning]] ← 26.
-   [[Digital garden]] ← 26.
-   [[Viable system model]] ← 25.
-   [[Anarchism]] ← 25.
-   [[Red Plenty]] ← 25.

