# Climate Change as Class War with Matthew T. Huber

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/climate-change-as-class-war-with-matthew-huber

Featuring
: [[Matthew Huber]]

[[Climate change]] as [[class war]].  Based around the book [[Climate Change as Class War: Building Socialism on a Warming Planet]].

-   ~00:08:47  Professional class have a kind of knowledge-based politics, based on study of environment, etc, something not rooted in daily struggles of life as experience by [[working class]] (by the far the majority class).
-   ~00:11:01  Environmental politics can't just be about land, forest, etc, it needs to incorporate something for the mass who struggle to survive in the market.
-   ~00:17:20  Carbon guilt. Consumer-based tactics vs class-based organising.
-   ~00:32:15  Framing politics solely around reduction of levels of affluence is problematic. Sometimes a vision of more or better is needed.
-   ~00:34:01  A critique of [[degrowth]] as an organising message.
-   ~00:37:16  Huber agrees with a lot of the actual programme of degrowth though.  But there's a lot of framing issues.
-   ~00:40:27  Degrowth often says global south should grow, global north should contract - but it misses the inequalities within countries.
-   ~00:41:49  Has a problem with degrowthers anarchist vibe of prefiguration, 'nowtopia', small pockets of non-capitalism that are never joined up.
-   ~00:46:20  Electrification is a place to intervene.
-   ~00:48:23  Renewable industry is problematic in terms of labour rights. There is an aspect of green capitalism there.

I feel like the criticisms of degrowth might be a bit straw man.  But strongly agree with the assertion of needing class consciousness and the working class involved in any eco-socialist environmental movement.

