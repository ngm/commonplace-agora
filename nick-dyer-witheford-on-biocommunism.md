# Nick Dyer-Witheford on Biocommunism

A
: [[podcast]]

URL
: https://futurehistories-international.com/episodes/s02/e27-nick-dyer-witheford-on-biocommunism/

Featuring
: Nick Dyer-Witheford

Series
: [[Future Histories]]

[[Biocommunism]].

-   ~00:00:00  Earlier ideas on biocommunism in relation to [[species-being]].  It was closer to left accelerationism, FALC, etc back then, but changed now.
-   ~00:07:51  Acknowledges lack of content on transition in his paper.
-   ~00:08:35 "A process of vast association"
-   ~00:06:48 Biocommunism: combines social and environmental system. Fits in doughnut style boundaries. Uses collective and [[democratic planning]].
-   ~00:10:23 Trying to bridge: between green and red; between centralists and decentralists; between state socialists and autonomists/anarchists.
-   ~00:13:25 We are currently in a multi-dimensional crisis / polycrisis.
-   ~00:20:24 Rhizomatic governance.

