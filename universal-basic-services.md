# Universal basic services

Kind of an extension and sanctification of [[public services]].

So the provision of things like health, education, housing, public transport, connectivity and personal care services to everyone, free at the point of service.

