# Why have a personal wiki?

> I find writing too hard to want to spend it on things that disappear
> 
> &#x2013; Martin Fowler ([What is a Bliki](https://martinfowler.com/bliki/WhatIsaBliki.html))

At first my reasons were very similar to [Nick's reasons](https://github.com/nicksellen/ponderings#why):

> -   writing my thoughts publically makes me write them a bit more carefully
> -   my thoughts are fuzzy and vague in my head, they become more real outside it (and may self desctruct in a cloud of stupidity)
> -   it provokes debate, which helps me refine the arguments: 1+n minds are better than 1
> -   I think I have something to say (we'll find out right?)
> -   I get sad at the state of debate in the world
>     -   news and media normally tell gossipy stories about anything but the underlying topic
>     -   in-person debate usually veers dramatically off-course and debates a well-worn set of less interesting topics instead
>     -   blogs or "point-in-time" publishing systems don't produce an always-up-to-date sof thoughts

I think my reasons have changed slightly over time - my wiki has become perhaps more a way for personal notetaking, for shaping my thoughts, with less emphasis on the debate straight away.  

The social part is definitely important though because I want to learn through discussion. Don't just want to be typing into the void, in dialectic only with myself.  The [[stream]] is perhaps more for that - [[networked learning]], [[connectivism]]. 

But is that its own thing? I guess so. You could technically have an entirely private stream and garden, if you wanted. So making it public is more for the networked learning aspect?

Combined, the [[blog/wiki combo]] helps me think more about what I learn (through wiki-ing it) **and** learn more about what I think (through conversations on the stream), I'm really digging it.

I think we need to better foster that though in our [[social software]] - currently it feels optimised for aggro.  If you say a wrong thing, make a mistake, you're more likely to be piled on rather than helpfully educated.

I have found it has aided massively in helping me form ideas. I've built up enough mass to have gravitational pull for other ideas now.  New articles I encounter, things people post, I feel like I'm in a good place to relate it to what I already have thought about.

