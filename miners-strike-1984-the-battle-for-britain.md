# Miners’ Strike 1984: The Battle For Britain

Documentary about the [[Miners' strike]].

Three different episodes.


## [[Shirebrook]]

First one is about the division between strikers and those that crossed the picket line.  Focused on the mining town of [[Shirebrook]].

Feelings are still raw, 40 years later.


## Orgreave

The [[Battle of Orgreave]].

[[Police brutality]]. [[State violence]].

Shocking, the violence that was meted out by the police on the miners.  Seemingly premeditated.

Then perjury.

All seemingly coming directly from the wishes of Margaret Thatcher.

The media apparently complicit, a piece of state apparatus.

