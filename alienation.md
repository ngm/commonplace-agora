# Alienation

Being disconnected from your labour, and feeling disconnected from other human beings.

> The central concept was alienation, but Marx saw the source of this alienation not as a problem of consciousness, as [[Hegel]] put it, but in the material conditions of capitalist society. **Under capitalism, human beings led a nonhuman existence, being alienated from their work, from the product of their labor, from one another, from nature, from their own true selves**. The solution was not in the realm of ideas, but in action to overturn these conditions.
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

