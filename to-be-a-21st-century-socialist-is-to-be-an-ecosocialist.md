# To be a 21st century socialist is to be an ecosocialist

To be a 21st century [[socialist]] is to be an [[ecosocialist]].


## Because

(presumably) 

-   Climate breakdown is the dominant issue of our times


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Source (for me)
: [[Breht O'Shea]] [[Socialist States &amp; the Environment w/ Salvatore Engel-Di Mauro]]

Probably without a doubt, just need to double-check I understand the intent here.  

