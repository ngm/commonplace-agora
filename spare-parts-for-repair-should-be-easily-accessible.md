# Spare parts for repair should be easily accessible

A
: [[claim]]

[[access to spare parts]]

Why? To make repair more accessible. Access to (affordable) spare parts is often a barrier to repair. If you cant get a part, often you cant do a repair.

