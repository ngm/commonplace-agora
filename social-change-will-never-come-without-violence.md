# Social change will never come without violence

[[Social change]] will never come without [[violence]].

I don't know.  I hope this is not true.  But I am not confident.

Some say that [[social change has never come without violence]] and that it never will.

If it **is** true, then you must at least believe that [[when bringing change we must minimise violence]].

