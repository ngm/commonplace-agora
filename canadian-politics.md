# Canadian politics



## tl;dr from Paris Marx

> Okay, the tl;dr: The Liberals are the centrist party and considered the “natural governing party” because they spent so much of the 20th century in government. The typically dangle social programs and progressive reforms at the election and rarely follow through. The Conservatives are the right-wing party, they aren’t as right-wing as like the US Republicans, but still have social conservative and libertarian elements. The NDP used to be explicitly anti-capitalist decades ago, but are vaguely even social democratic now. They’re slightly to the left of the Liberals, but are willing to sacrifice Palestinian solidarity and a lot of other left causes to look “electable.” They’ve never been in government on the federal level, but have in a number of provinces. There’s also the Bloc Quebecois, a Quebec nationalist party, kind of centre-left but also Islamophobic (think French laïcité), and the Greens which are a neoliberal, not ecosocialist, party.
> 
> &#x2013; Paris Marx, [[TWSU]] Discord

