# Some files keep reappearing in Syncthing

This happens from time to time.  Some files keep reappearing in [[syncthing]].  I'm syncing across about 4 devices.  Even when deleting from the device that I'm certain is the creator of the files, they keep on coming back.

So what's going on?

Here's a likely looking post on the Syncthing forum: [Syncthing Constantly Reinstating Old Deleted Files - Support - Syncthing Comm&#x2026;](https://forum.syncthing.net/t/syncthing-constantly-reinstating-old-deleted-files/18026) 

Maybe not then - looks like some other software was the culprit in that case.

Though they do mention a 'nuclear option':

> To be clear, I’ve taken the nuclear option to try and fix this:
> 
> -   Uninstalled Syncthing off all devices, including the config files (in AppData on the Windows machines and in root on Linux).
> -   Deleted as many of the reappearing old files as possible.
> -   Deleted all the ‘.’ folders associated with Syncthing (.stfolder, .sync, .stversions) from all of my synced folders.
> -   Reinstalled Syncthing fresh, re-established synced folders from scratch.

Don't particular want to do all that, so I'll keep searching for a bit.

Another one:
[Deleted files being recreated - #20 by gmhall - Support - Syncthing Community&#x2026;](https://forum.syncthing.net/t/deleted-files-being-recreated/17803/20)

where the end result sesms to be another piece of software causing the issue.  Hmm.

Pretty sure I don't have any other software doing any syncing.  I have Nextcloud syncing some other things, but not my folder of `org` files (where this is happening).

There's a mention of the version of syncthing being potentially an issue here [Deleted Folders Keep Re-appearing - #4 by tomasz86 - Support - Syncthing Comm&#x2026;](https://forum.syncthing.net/t/deleted-folders-keep-re-appearing/19290/4) 

I don't think that's it, as it's only just started happening.  But I'll make sure all the version are updated, as that'll be useful to do anyway.

OK:

-   laptop: 1.23.4
-   phone: v1.22.2-dev.1.g15c0afec
    -   Getting an error when trying to upgrade
-   server: 1.23.1~ynh1 -&gt; v1.23.3
    -   also it wasn't actually running for some reason, but back up and running again after the upgrade
-   tablet: not sure, it's currently turned off

Alright.  Only thing succeeded in changing this time was getting it running again on the server.

Deleted the files again, and they haven't come back in about 10 minutes or so. Let's see how we get on.  If they come back again, I'm guessing it'll be when I turn my tablet on, if so that'll point at that being the culprit.

I think maybe it's some oddity of orgzly's syncing feature.  When I opened up orgzly on my tablet, it synced those old files back to the filesystem.  I deleted them from within orgzly, and they've gone again.  Hopefully for good now!

