# Black Box: Episode one – The connectionists

A
: [[podcast]]

Part of
: [[Black Box]]

Potted history of AI. [[Perceptron]]. [[AI winter]]. [[Geoff Hinton]]. The connectionists. ImageNet victory by [[AlexNet]]. [[AlphaGo]].

