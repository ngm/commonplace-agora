# International Telecommunication Union

> The International Telecommunication Union (ITU) is the United Nations specialized agency for information and communication technologies – ICTs.
> 
> Founded in 1865 to facilitate international connectivity in communications networks, we allocate global radio spectrum and satellite orbits, develop the technical standards that ensure networks and technologies seamlessly interconnect, and strive to improve access to ICTs to underserved communities worldwide.  Every time you make a phonecall via the mobile, access the Internet or send an email, you are benefitting from the work of ITU. 
> 
> &#x2013; [About ITU](https://www.itu.int/en/about/Pages/default.aspx)

