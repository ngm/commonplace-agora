# socially useful production

> Ideas about labour process, skill, design and technology were at the heart of the movement for socially useful production. The movement sought a more democratic human relationship with technology that furnished tools for people to become architects in a deliberated societal vision, rather than perpetuating a situation where humans became scientifically managed bees tending machines in the service of capital
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> to present a vision of an alternative paradigm that prefigured a different role for technology in society … To do this it is necessary to produce both a critique of the current shape and aims of existing technologies together with examples of alternatives that could lead to social and technological change
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Aspirations for socially useful production permitted alliances between workers and the new social movements for peace, environment, community activism and women. 
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

Seems relevant for [[Just transition]] and [[climate justice]].

