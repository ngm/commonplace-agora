# Hyperconversations

> People seem very focused on technological solutions to online communication, but the hyperconversation approach is trying to prove that the problem is a human problem. 
> 
> &#x2013; [The Hyperchat Modality](https://www.kickscondor.com/comments/the-hyperchat-modality/) 

<!--quoteend-->

> If you read and listen to each other and try to respond thoughfully and carefully - and try to find your own style and wee innovations along the way - you start to feel like you don’t need anything more complicated than a TiddlyWiki!
> 
> &#x2013; [The Hyperchat Modality](https://www.kickscondor.com/comments/the-hyperchat-modality/) 

