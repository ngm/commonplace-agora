# Emacs: Dead and Loving It

Found at
: https://batsov.com/articles/2024/02/26/emacs-dead-and-loving-it/

[[Emacs]]

Reports of Emacs' death have been greatly exaggerated.

"What is dead cannot die"

