# This Machine Kills

A
: [[podcast series]]

URL
: https://soundcloud.com/thismachinekillspod

Featuring
: [[Jathan Sadowski]] / [[Edward Ongweso Jr.]].

Podcast series on digital technologies.  Sadowski describes it as being on [[technology and political economy]]. (I would say specifically [[digital technology]])

[[Socialist]], [[Communist]], [[anarchist]] in outlook.

