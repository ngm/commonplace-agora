# Climate Leviathan



## Book

A
: [[book]]


## Idea

An
: [[idea]]

Outlines four paths for climate politics:

-   Climate Leviathan
-   [[Climate Mao]]
-   [[Climate Behemoth]]
-   [[Climate X]]

Just going off what I remember from listening to [[Climate Leviathan with Geoff Mann]], my very very crude recollection at the moment:


#### Climate Leviathan

Ultimately ineffectual.  Not something we want.  Essentially the ineffectiveness of COP writ large.


#### Climate Behemoth

Something like Trump, who not only doesn't give a shit about the climate, but also wouldn't want to work with other countries on it anyway.


#### Climate Mao

If a response could be centrally planned by a non-capitalist nation state.


#### Climate X

Pockets of climate activism at various scales, e.g. city level, etc.

![[2022-03-26_18-49-49_screenshot.png]]

