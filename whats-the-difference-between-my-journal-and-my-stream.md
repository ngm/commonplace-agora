# What's the difference between my journal and my stream?

I'll first describe what is the current state of affairs.  This does not necessarily align with the way I desire things to be.


## Current state of affairs

I write my journal in org-roam.  It is a bulleted list of thoughts.  It is read-only - noone can interact with it directly.  (Though of course, people could annotate it with hypothesis, or something similar).  It is not structured - you could not subscribe to items within it in a feed reader, say.  It is public, and is thus filtered - despite the name, I don't put much personal or intimate things in this public journal.

I publish to my stream via micropub and WordPress, and syndicate it to Mastodon.  My stream allows for comments and interactions.

What goes in my stream is generally a subset of my journal.  But responses to comments in my stream are not necessarily included in my journal.  (Though likely pulled in to my garden in the relevant place.)

I guess my journal is [[narrative]], my stream is [[dialogic]].


## A desire for how things should be

Not sure.  It's fine at the moment.  Just a vague feeling that perhaps they should be combined.  But this might be mistaken.

