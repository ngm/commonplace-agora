# Looking Backward

A
: [[book]]

Author
: [[Edward Bellamy]]

Year
: [[1888]]

> [Bellamy's] vision of a harmonious futuristic socialism in the year [[2000]]
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> proto–science fiction novel [&#x2026;] anticipates much of the postwork and luxury communism writing of today
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Julian West falls asleep in the year 1887 and wakes up in a socialist United States in the year 2000. In this society, all industries are nationalised, humans retire at the age of 45, citizens receive an equal share of goods and a credit of equal size provided on a debit card that is used for shopping, those performing unpleasant or dangerous labour work fewer hours than others, there is an industrial army performing compulsory labour in an efficient manner, education up to the age of 21 and the level of college is free, all types of labour are seen as being of equal importance, poverty and starvation do not exist, there is a low level of crime and disease, and there is free entertainment.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]


## Criticism

[[William Morris]] not a fan.  He saw it as [[Prometheanism]].

> The development of man’s resources,’ Morris wrote in his review of Looking Backward, ‘which has given him greater power over nature, has driven him also into fresh desires and fresh demands on nature … and the multiplication of machinery will just – multiply machinery
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Morris recognized Bellamy’s simplistic, undialectical belief that socialism could simply take what it pleased of modernity, totally intact, while getting rid of the bad parts. It represented a “half-change” that revealed Bellamy’s perspective as rooted in “that of the industrious professional middle-class men of to-day purified from their crime of complicity with the monopolist class.”
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Or, as Morris said of Bellamy, “He conceives of the change to Socialism as taking place without any breakdown of [modern] life, or indeed disturbance of it

