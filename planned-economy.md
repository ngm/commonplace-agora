# planned economy

> He could see that this would not be possible under [[capitalism]], where all the factories had separate owners, locked in wasteful competition with one another. There, nobody was in a position to think systematically. The capitalists would not be willing to share information about their operations; what would be in it for them? That was why capitalism was blind, why it groped and blundered. It was like an organism without a brain.
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> it did grow. It was designed to. Market economies, so far as they were ‘designed’ at all, by their institutions and their laws, were designed to match buyers and sellers. They grew, but only because the sellers might decide, from the eagerness of the buyers, to make a little more of what they were selling, or because the buyers might decide to use what they’d bought to sell something else. Growth wasn’t intrinsic. It wasn’t in the essence of a market economy that it should always do a little more this year than it had last year. The planned economy, on the other hand, was created to accomplish exactly that
> 
> &#x2013; [[Red Plenty]]

^ Really?

> The planned economy measured its success in terms of the amount of physical things it produced. Money was treated as secondary, merely a tool for accounting
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Conscious control is a planned economy’s greatest strength, but it requires democracy to prevent authoritarian and inefficient control over the production and distribution of goods
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> [[Hayek]]’s argument that a planned economy requires ‘one single mind possessing all the information’ was either naive or cynical, but it is not a convincing rebuttal to Kantorovich’s vision of socialism
> 
> [[Half-Earth Socialism]]

