# Node Club Tarot

My [[Node Club Tarot Readings]].

Enjoying node club tarot muchly so far.

First, the simple nudge to use /random is great - you stumble on nodes/subnodes from others, hear about new things, even come across other Agoreans you might not have known existed. (To some degree it also facilitates a bit of communal wiki gardening - if you see an odd looking or broken node/subnode, maybe you log it and get it patched up.)

Second, the [[Tarot for thought]] aspect is fun.  It's very interesting to try and connect nodes, or see how your existing mental model relates to your random drawing.  It's a nice bit of [[dérive]].

