# Literate programming

Tried the paradigm a little bit on config files, with [[My Spacemacs User Config]].

It could be problematic on shared projects though, presumably - you'd be forcing everyone in to doing it that way, otherwise your literate file and the code would get out of sync.

-   [Thoughts after 6 months of literate programming · GitHub](https://gist.github.com/jpf/d71453f535065a0d9281672152541386)
-   [Ask HN: Why did literate programming not catch on? | Hacker News](https://news.ycombinator.com/item?id=10069748)

