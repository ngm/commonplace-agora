# For years, I suspected MI5 interfered in the miners’ strike. The truth was even more shocking than I thought

An
: [[article]]

Found at
: https://www.theguardian.com/commentisfree/2024/mar/07/mi5-miners-strike-national-archives-security-service-government

[[Miners' strike]].

> A document buried in the National Archives reveals how the security service abused its power to help the government win

