# syncthing

I use syncthing to sync files between my desktop and my mobile.  Pretty much just my [[org]] files to be honest.

Also recently set up [[syncthing]] on my [[YunoHost]] box.  The whole point of Syncthing is kind of that it's [[P2P]], without a central server, but I thought this might be handy as an extra node for those two to sync between.  I do occassionally get sync conflicts when I've edited stuff on both while they weren't in communication with each other.  It's not a **central** server, just a more available node, I guess.

-   [[Syncthing Android doesn't sync until I restart or rescan]]

