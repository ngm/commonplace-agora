# Movies

<div class="abstract">
  <div></div>

Some films I've liked (or at least watched) over the years, in no particular order.

(Probably best to assume there will be some spoiler alerts&#x2026;)

</div>


# Table of Contents

-   [Children of Men](#%5B%5Bid%3Aa422e92e-fb2d-4124-abeb-90ff3dea0c9e%5D%5BChildren%20of%20Men%5D%5D)
-   [Knives Out](#Knives%20Out)
-   [Brick](#Brick)
-   [Garden of Words](#Garden%20of%20Words)
-   [Tron Legacy](#%5B%5Bid%3A89ce6677-e7f1-42b1-bd6f-1188e8a3d688%5D%5BTron%20Legacy%5D%5D)
-   [Miller's Crossing](#Miller%27s%20Crossing)
-   [Tron](#Tron)
-   [Pan's Labyrinth](#%5B%5Bid%3A2cbac6fd-6345-4370-bc7f-a7fe2fc57a5d%5D%5BPan%27s%20Labyrinth%5D%5D)
-   [Monty Python and the Holy Grail](#Monty%20Python%20and%20the%20Holy%20Grail)
-   [Eternal Sunshine of the Spotless Mind](#Eternal%20Sunshine%20of%20the%20Spotless%20Mind)
-   [The Matrix](#The%20Matrix)
-   [Mad Max: Fury Road](#Mad%20Max%3A%20Fury%20Road)
-   [Back to the Future](#Back%20to%20the%20Future)
-   [Raiders of the Lost Ark](#Raiders%20of%20the%20Lost%20Ark)
-   [Gattaca](#Gattaca)
-   [Her](#Her)
-   [You Were Never Really Here](#You%20Were%20Never%20Really%20Here)
-   [Blade Runner](#%5B%5Bid%3Ae5c05fa6-8f8d-495d-b790-d1518ee651cc%5D%5BBlade%20Runner%5D%5D)
-   [Adam Curtis stuff](#%5B%5Bid%3A6437a525-85b3-4ae7-bf9e-beae5e72de85%5D%5BAdam%20Curtis%20stuff%5D%5D)
-   [The Speed Cubers](#%5B%5Bid%3Ac382321a-3992-47f9-8bcb-685401589d60%5D%5BThe%20Speed%20Cubers%5D%5D)
-   [To watch](#To%20watch)


<a id="%5B%5Bid%3Aa422e92e-fb2d-4124-abeb-90ff3dea0c9e%5D%5BChildren%20of%20Men%5D%5D"></a>

## [[Children of Men]]


<a id="Knives%20Out"></a>

## Knives Out

About 2 hours long, but you wouldn't notice - just non-stop enjoyment.  A modern spin on the murder mystery genre.  Great ensemble performance, Daniel Craig remarkably enjoyable as a Southern US sleuth.


<a id="Brick"></a>

## Brick

A kind of Shakespeare-cum-neo-noir-cum-high-school-drama. Rian Johnson's first film.  A long time since I watched it, must rewatch after seeing Knives Out (Johnson's latest), I remember loving it at the time.  


<a id="Garden%20of%20Words"></a>

## Garden of Words

A short and interesting anime about two strangers who meet in a park and forge a connection despite many barriers.  They have a profound effect on each others' lives as they get to know each other.

Gentle and philosophical on life.  It looks gorgeous, with some beautiful backdrop illustration.  Makes you think about the nature of human connection.


<a id="%5B%5Bid%3A89ce6677-e7f1-42b1-bd6f-1188e8a3d688%5D%5BTron%20Legacy%5D%5D"></a>

## [[Tron Legacy]]


<a id="Miller%27s%20Crossing"></a>

## Miller's Crossing


<a id="Tron"></a>

## Tron


<a id="%5B%5Bid%3A2cbac6fd-6345-4370-bc7f-a7fe2fc57a5d%5D%5BPan%27s%20Labyrinth%5D%5D"></a>

## [[Pan's Labyrinth]]


<a id="Monty%20Python%20and%20the%20Holy%20Grail"></a>

## Monty Python and the Holy Grail


<a id="Eternal%20Sunshine%20of%20the%20Spotless%20Mind"></a>

## Eternal Sunshine of the Spotless Mind


<a id="The%20Matrix"></a>

## The Matrix


<a id="Mad%20Max%3A%20Fury%20Road"></a>

## Mad Max: Fury Road


<a id="Back%20to%20the%20Future"></a>

## Back to the Future


<a id="Raiders%20of%20the%20Lost%20Ark"></a>

## Raiders of the Lost Ark


<a id="Gattaca"></a>

## Gattaca


<a id="Her"></a>

## Her


<a id="You%20Were%20Never%20Really%20Here"></a>

## You Were Never Really Here

Brutal film.  A bit of a modern day Taxi Driver.  Someone losing it in a bad world.

Joaquim Phoenix is superb.  

It's amazingly directed.  Really minimal but effective.  The soundtrack by Jonny Greenwood is perfect for the film.

The plot is very grim.  Maybe also a bit far fetched.  But based on a book by Jonathon Ames.  The film is impressive in the amount of stuff that it leaves out.  It alludes to a lot of stuff that is probably much more fleshed out in the book.


<a id="%5B%5Bid%3Ae5c05fa6-8f8d-495d-b790-d1518ee651cc%5D%5BBlade%20Runner%5D%5D"></a>

## [[Blade Runner]]


<a id="%5B%5Bid%3A6437a525-85b3-4ae7-bf9e-beae5e72de85%5D%5BAdam%20Curtis%20stuff%5D%5D"></a>

## [[Adam Curtis stuff]]


<a id="%5B%5Bid%3Ac382321a-3992-47f9-8bcb-685401589d60%5D%5BThe%20Speed%20Cubers%5D%5D"></a>

## [[The Speed Cubers]]


<a id="To%20watch"></a>

## To watch

-   Finally Got the News

