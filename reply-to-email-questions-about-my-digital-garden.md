# Reply to email questions about my digital garden



## Do you store your notes (second brain or otherwise) in one place, or across many apps/services/plain text files/napkin sketches/etc?

I have a both a public, outer garden, and a private, inner garden.  See Bill Seitz's page on this for more info: http://webseitz.fluxent.com/wiki/TendingYourInnerAndOuterDigitalGardenspublic

I use org-mode, org-roam and Emacs for all of my writing.  My private garden is where I do all my planning and todo management, too, with org-mode.

I also currently do freeform journalling occassionally in a paper notebook with pen.  I'd like it to be a daily habit, but not there yet.   I've been contemplating for about a year now getting one of the e-ink tablets for this, but haven't yet.  Haven't been able to justify the price/purchase of a new device just yet.

As well as to my own website at https://commonplace.doubleloop.net, I also publish my garden to https://anagora.org, a collective space that combines multiple gardens together.


## How often do you look up info in your notes, and how do you find what you’re looking for?

Interesting question!   I don't have any stats, but anecdotally I would say that it is fairly infrequent.  Let's say the order of magnitude is weekly rather than daily.  It's most often when I want to share a link publicly as part of a social media discussion I'm having, and I want to point to some previous thinking on it.  I always find what I'm looking for.  org-roam has good title search and org-mode quick full text search of all notes.  So it may take more or less time depending on how hazy my memory is of it, but it's rare that I can't find something that I know is there.

There's probably things that **I don't remember are even in there** though, which is another story.


## If you need to look something up while you’re working outside of your notes, how difficult is it for you to get back into a flow state?

Not too hard.  Just by its nature social media can be a distraction if I need to look something up there.  But I'm generally good at avoiding rabbit holes, and I tend to prefer reading in an e-reader or writing in Emacs to surfing the web anyway.   All that said, given that I have a child, cat and a partner usually not that far away, I tend not to often get into a full flow state anyway. I'm generally in some kind of half-flow, and I've had plenty of practice at recovering back to that half-flow from external distractions.

