# progressive summarisation

Summarising a note in multiple stages over time.


## What is it?

> Progressive Summarization is a technique that relies on summarizing a note in multiple stages over time. You save only the best excerpts from whatever you’re reading, and then create a summary of those excerpts, and then a summary of that summary, distilling the essence of the content at each stage. These “layers” are like a digital map that can be zoomed in or out to any level of detail you need. Progressive Summarization allows you to read the note in different ways for different purposes: in depth if you want to glean every detail, or at a high level if you just need the main takeaway. This allows you to review a note’s contents in seconds to decide if it’s useful for the task at hand
> 
> &#x2013;  [Building a Second Brain: An Overview - Forte Labs](https://fortelabs.co/blog/basboverview/)

<!--quoteend-->

> You can greatly facilitate and speed up this process by distilling your notes into actionable, bite-sized summaries. It would be near impossible to review your 10 pages of notes on a book you read last year in the midst of a chaotic workday., for example. But if you had just the main points of that book in a 3-point summary, you could quickly remind yourself of what it contains and potentially apply it to something you’re working on
> 
> &#x2013;  [Building a Second Brain: An Overview - Forte Labs](https://fortelabs.co/blog/basboverview/)

<!--quoteend-->

> The four top-level categories of P.A.R.A. — Projects, Areas, Resources, and Archives — are designed to facilitate this process of forwarding knowledge through time.  
> 
> -   By placing a note in a project folder, you are essentially scheduling it for review on the short time horizon of an individual project
> -   Notes in area folders are scheduled for less frequent review, whenever you evaluate that area of your work or life
> -   Notes in resource folders stand ready for review if and when you decide to take action on that topic
> -   And notes in archive folders are in “cold storage,” available if needed but not scheduled for review at any particular time

-   [[opportunistic compression]]

![[2020-03-21_11-17-23_screenshot.png]]


## Misc

> A common challenge for people who love to learn is that they constantly force feed themselves more and more information, but never actually put it to use.


## Layers

-   [[Layer 0]] is your bookmarks, feeds, etc, your info inbox. Your first lens of curation on to what is of use to you.
-   Layer 1: When you encounter something interesting, capture it
-   Layer 2: Bold the most interesting parts
-   Layer 3: Highlight the most interesting bolded sections
-   Layer 4: Summarize the bolded portions and the note in your own words
-   Layer 5: Turn your notes into something new: a tweet, a blog post, even a book


### Layer infinity

Updated thought: [[Layer Infinity]] is the Internet.  (Or just all information out there).

