# eco-nihilism

> Other progressives have rightly refused to join Bendell, Scranton, and Franzen in their embrace of [[eco-nihilism]].
> 
> &#x2013; [[Revolution or Ruin]]

<!--quoteend-->

> Less metaphysical, although equally resigned to planetary ruin, is Jonathan Franzen. For Franzen, any hope of avoiding civilizational catastrophe is misguided, even harmful, leading to misplaced efforts and broken dreams. To think that we might build new transportation and energy systems, much less replace capitalist competition with communist planning, is a pipe dream—futile and delusional.
> 
> &#x2013; [[Revolution or Ruin]]

