# Organisational tech/digital policies



## Incarcerated workers policy

-   https://incarceratedworkers.org/resources/iwoc-technology-policy
-   it's pragmatic about challenges, but striving to improve

> We must employ a divest/invest strategy to the technology we use if we are to combat surveillance capitalism and build radical infrastructure that reflects our vision of the world.

<!--quoteend-->

> When a technology does not meet one of these criteria we will organize to help raise a project to that standard. When we need to use corporate technology, we use it strategically and subversively – always on the lookout for alternatives.

