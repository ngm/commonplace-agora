# Complexity economics

[[Complexity]] [[economics]].

[[W. Brian Arthur]].

> From its beginnings as a discipline nearly 150 years ago, economics rested on assumptions that don’t hold up when studied in the present day. The notion that our economic systems are in equilibrium, that they’re made of actors making simple rational and self-interested decisions with perfect knowledge of society— these ideas prove about as useful in the Information Age as Newton’s laws of motion are to quantum physicists.
> 
> &#x2013; [[W. Brian Arthur (Part 1) on The History of Complexity Economics]]

<!--quoteend-->

> A novel paradigm for economics, borrowing insights from ecology and evolutionary biology, started to emerge at SFI in the late 1980s — one that treats our markets and technologies as systems out of balance, serving metabolic forces, made of agents with imperfect information and acting on fundamental uncertainty.
> 
> &#x2013; [[W. Brian Arthur (Part 1) on The History of Complexity Economics]]

<!--quoteend-->

> This new complexity economics uses new tools and data sets to shed light on puzzles standard economics couldn’t answer — like why the economy grows, how sudden and cascading crashes happen, why some companies and cities lock in permanent competitive advantages, and how technology evolves.
> 
> &#x2013; [[W. Brian Arthur (Part 1) on The History of Complexity Economics]]

<!--quoteend-->

> And complexity economics offers insights back to biology, providing a new lens through which to understand the vastly intricate exchanges on which human life depends.
> 
> &#x2013; [[W. Brian Arthur (Part 1) on The History of Complexity Economics]]

