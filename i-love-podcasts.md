# I love podcasts

[[I love]] [[podcasts]].


## Because

-   They are such a phenomenal source of information.
-   They are a knowledge commons.
-   I can listen to them while doing other things (walking, chores, etc).

