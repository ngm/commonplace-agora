# the potential of crossposting

This is really interesting from CJ Eller on [the potential of crossposting](https://blog.cjeller.site/from-blog-to-blocks).  Interesting to think about how it fits in with the IndieWeb ideas of owning your own content and POSSEing (publishing on your own site, syndicating elsewhere).

I've thought of POSSE before more as a means of transitioning away from the big platforms (the [[bit tyrants]]) while they still have the network effects.  But this is more about your stuff existing in various locations as a means to enable new creative uses of it.

