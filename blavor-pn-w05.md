# Blavor PN-W05

A solar charger and power bank.

10,000mAh capacity.

5V output.

Minimum sunlight intensity 25,000 Lux in order to charge, which gives a 180 mA charging current.  So it'll take 55 hours (10000/180) to fully charge the bank I think that means.

This here says that you'll perhaps get 25,000 lux from:

-   shade illuminated by entire clear blue sky, midday
-   and possibly in the light on a typical overcast day at midday

https://www.liquisearch.com/daylight/daylight_intensity_in_different_conditions

3700 mAh battery on my tablet. The USB outputs ouver at 2.1A.  That means it could fully charge my tablet in about an hour and a half.  But it would take about 20 hours on sunlight for the bank to have charged enough to get that.

My phone battery is 2800 mAh.  That's about 15.5 hours.

The phone needs charging once a day usually.  The tablet, maybe every 2 or 3 days.

The phone could use up the juice daily alone then.  But let's see how it pans out.

