# Scientific management

[[Taylorism]].

> Taylor and his team promised to rationalize manufacture, rendering it more efficient and productive, by determining the “one best way” for every aspect of the work process
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Scientific management, for all its pretensions, was less about determining ideal working methods and more about shattering this tremendous source of worker power.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Scientific management was, then, less a science of efficiency and more a political program for reshaping the worker as a pliant subject—what Taylor himself called “a complete mental revolution on the part of the workingmen … toward their work, toward their fellow men, and toward their employers.”
> 
> &#x2013; [[Breaking Things at Work]]

