# Google Scholar alternatives

[[Google Scholar]] alternatives.

> All the talk about Google losing ground &amp; cutting jobs made me nervous about what I'd do if Google Scholar got the chop&#x2026;
> 
> So I dug into the options – and came away relieved. Even a little optimistic!
> 
> New PLOS post: Could there be some viable challengers to Google Scholar on the horizon?
> 
> https://absolutelymaybe.plos.org/2023/02/16/could-there-be-some-viable-challengers-to-google-scholar-on-the-horizon/
> 
> \#OpenScience #AcademicMastodon #AcademicChatter
> 
> &#x2013; https://mastodon.online/@hildabast/109873723113968357

Not quite a Google Scholar alternative but interesting way of accessing papers: [[r/Scholar]]

