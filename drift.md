# drift



## In psychogeography

See [[dérive]].


## In politics of technology

> Winner calls it “drift”: the process whereby a technology imposes requirements of its own, pulling us ass-backwards along an unchosen course.
> 
> &#x2013; [[The technologies of all dead generations]]

<!--quoteend-->

> Drift is the [[entropy]], the quantum of chaos that haunts the politics of technology.
> 
> &#x2013; [[The technologies of all dead generations]]

