# distributed action

A type of [[political action]] conceptualised by [[Rodrigo Nunes]] in [[Neither Vertical Nor Horizontal]].

A combination of [[aggregate action]] (things spontaneously happening) and [[collective action]] (things being organised) - so a little something each of [[Horizontalism]] and [[Verticalism]].

Can't say I fully grok it yet, but I like what I understand thus far.  It makes some space for networked and spontaneous action while recognising the need for more concrete organisation structures to orchestrate some actions.

> Let us call it distributed action: the common space in which collective and aggregate action combine, communicate, relate and establish positive and negative feedback loops with one another
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

<!--quoteend-->

> It is distributed action, not organisations, that must therefore be the point of departure for a theory of political organisation

Nunes says distributed action should be the key focus of study for [[political organisation]].  

> The distributed bypasses the binary opposition between collective and individual as well as that between centralised and decentralised; it is all of those at once
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

<!--quoteend-->

> whereas aggregate action refers to the ways in which people act together outside of organisations, and collective action to how they do so with or without them, distributed action accounts for the ways in which these two modes of acting together relate to each other. 
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

