# repair and reuse in the IPCC Sixth Assessment Report

There's a good amount of mentions of [[repair]] and [[reuse]] in the [[IPCC Sixth Assessment Report]].

There's a whole chapter on 'demand-side' solutions, including consumption patterns.

> This implies that we could potentially save gigatonnes of CO2 through behavioural change (like community repair events) and policies (like the Right to Repair) that prioritise consuming less and (re)using what we already have for longer. And while small compared to changing what we eat, that’s still a massive impact in absolute terms.
> 
> [What does the IPCC report say about repair &amp; reuse? - Right to repair: policy&#x2026;](https://talk.restarters.net/t/what-does-the-ipcc-report-say-about-repair-reuse/8535)

There’s a few mentions of repair in Chapter 11 - Industry:

-   Designing for long life and repairability are mentioned in the context of [[material efficiency]] on page 11-25
-   [[Ecodesign directive]] and [[spare parts]] are mentioned on page 11-94
-   Repair services and job creation are mentioned on page 11-96

