# New Constellations

Found at
: https://newconstellations.co/

> We created New Constellations to hold new, experimental spaces for people to examine the moment of upheaval we are living through, and to imagine and begin shaping the better futures we believe can emerge from it; futures in which humanity and the planet flourish together.

I'm not entirely sure what they do.  They did something in [[Barrow]]: [Barrow's New Constellation](https://newconstellations.co/journey/barrow/).

