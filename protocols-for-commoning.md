# Protocols for commoning

Thinking about the [[Spheres of commoning]] (social, governance, and economic).

And how in a digital commons, stewarded by a distributed organisation e.g. a [[DisCO]], you could make use of different protocols for each of those spheres.

e.g.

-   [[IndieWeb]] or [[ActivityPub]] for social
-   [[ValueFlows]] for economic
-   [[Metagov]] based for governance?

This might be putting the cart before the horse a little bit&#x2026; i.e. don't think about the protocols until you're actually doing the thing in reality.  But interesting though&#x2026;

