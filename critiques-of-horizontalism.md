# Critiques of horizontalism

A lot of critiques of [[horizontalism]] seem to stem from 2011, Arab Spring, Occupy etc, and perceived failure for these to persist long term.

> the fitfulness of those uprisings and their incapacity to sustain themselves over time; their inability to move on from the tactics around which they had initially coalesced, typically square occupations, and the decline in their capacity for tactical innovation as circumstances changed; their inability to scale up in a viable way, and tendency to fall apart when they tried to do so; their propensity to demand large investments of time and energy from participants in return for little by way of clear strategy and decision-making; their relative lack of rootedness and strength to defend themselves when repression came bearing down. Many if not all of these have come to be associated with the tag that many people used to describe the spontaneous philosophy behind those mobilisations: ‘horizontalism
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

