# IPCC Sixth Assessment Report

[[IPCC]].

> It represents the world’s full knowledge to date of the physical basis of [[climate change]]
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

<!--quoteend-->

> the sixth such report from the IPCC since 1988, has been eight years in the making, marshalling the work of hundreds of experts and peer-review studies.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

Comes in three parts.

-   The Physical Science Basis (WGI)
-   Impacts, Adaptation and Vulnerability (WGII)
-   Mitigation of Climate Change (WGIII).


## WGI: The Physical Science Basis

Published in August 2021.  Looked mainly at temperature forecasts.

-   [[Global temperature is likely to rise by more than 1.5C above pre-industrial levels by 2040]]
-   This will bring widespread devastation and extreme weather
-   [[Human activity is unequivocally the cause of rapid changes to the climate]]

> The gravity of the situation laid out in the report blows away blustering over the supposed costs of climate action. In any case, not acting will cost far more.
> 
> &#x2013; [IPCC report’s verdict on climate crimes of humanity: guilty as hell | Climate&#x2026;](https://www.theguardian.com/environment/2021/aug/09/ipcc-reports-verdict-on-climate-crimes-of-humanity-guilty-as-hell) 

<!--quoteend-->

> with some of the changes now inevitable and “irreversible”, climate scientists have warned.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn)

![[images/temperature-since-1AD.png]]


## WGII: Impacts, Adaptation and Vulnerability

> The second part, published in February, described in detail the kind of catastrophic impacts we could expect if temperatures rose to 1.5C and beyond, with widespread and sometimes irreparable harm to the earth’s life-support systems – including damage to food production, water scarcity, coastal inundation, extreme weather and chaos for at least half of the world’s population, who are “highly vulnerable” to the impacts
> 
> &#x2013; Down to Earth newsletter


## WGIII: Mitigation of Climate Change

> how emissions reductions will need to be delivered. It will look at the level of decarbonisation required by specific nations and business sectors.
> 
> [IPCC: Half of global population 'highly vulnerable' to climate crisis impacts&#x2026;](https://www.edie.net/ipcc-half-of-global-population-highly-vulnerable-to-climate-crisis-impacts/)

-   [[We are currently on track for catastrophic 3C heating]]
-   [[North America and Europe have made the biggest contribution to the climate crisis]]
-   [[Half of the world's population is highly vulnerable to the climate crisis]]
-   [[The overarching solution to our energy needs is to electrify everything we can]]
-   Retiring big emitters, such as coal-fired power stations, is needed.
-   Plus, planned new oil fields and airports that lock in high emissions need cancelling.

<!--listend-->

-   This report is essentially a manifesto for ending the fossil fuel age.
    -   It will help the growing mass of people fighting against the fossil fuel industry on many fronts – but especially those taking official channels, such as MPs lobbying government not to open new oil fields, lawyers and citizens taking countries or companies to court, or those justifying direct action to stop high-carbon infrastructure being built.
-   The licence for climate action has never been stronger.

-   [[No amount of tree planting will be enough to cancel out the effects of continued fossil fuel emissions]]

-   These tools – renewable energy, electric vehicles, low-carbon technology – exist and are sufficient to the task.
-   The cost, by mid-century, will amount to just a few per cent of global GDP.
-   We have the knowledge and the means, the IPCC made clear. All that’s missing is the will.

> But if we want to limit global heating to 1.5C, we need to make drastic changes at once. There is no more time to lose. Fossil fuel infrastructure already in operation, planned or under construction is more than enough to bust the available carbon budget comprehensively, the IPCC found, so we must stop building more and retire what is already there. Renewable energy has plummeted in cost by 85% in a decade, so the alternatives to fossil fuels are readily available and cheap.
> 
> https://globalpossibilities.org/the-guardian-%E2%80%8C-%E2%80%8C-its-now-or-never-if-we-want-to-limit-global-warming-to-1-5c-%E2%80%8C-%E2%80%8C-%E2%80%8C-%E2%80%8C/

<!--quoteend-->

> Even if we act fast, there is still likely to be a need for some new technologies that suck carbon dioxide from the air, to return temperatures to 1.5C by the end of the century after the “almost inevitable” overshoot that is predicted. Yet the IPCC made clear these could not be used to compensate for continued high emissions. We will need them in addition to stopping burning coal, gas and oil.
> 
> https://globalpossibilities.org/the-guardian-%E2%80%8C-%E2%80%8C-its-now-or-never-if-we-want-to-limit-global-warming-to-1-5c-%E2%80%8C-%E2%80%8C-%E2%80%8C-%E2%80%8C/

<!--quoteend-->

> Current policies will not be enough to bring about the change needed – they would take us to 3C or more. We need new policies, fast.
> 
> https://globalpossibilities.org/the-guardian-%E2%80%8C-%E2%80%8C-its-now-or-never-if-we-want-to-limit-global-warming-to-1-5c-%E2%80%8C-%E2%80%8C-%E2%80%8C-%E2%80%8C/

