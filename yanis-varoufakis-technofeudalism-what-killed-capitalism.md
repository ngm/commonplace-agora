# Yanis Varoufakis, "Technofeudalism: What Killed Capitalism"

-   Great interview with [[Yanis Varoufakis]].
-   [[Techno-feudalism]].
-   Ultimate goal: socialisation of cloud capital (i.e. [[Big tech]])
-   Only America and China have cloud capital.
-   Glad they mention [[McKenzie Wark]] - Varoufakis says he agrees with all of it.

