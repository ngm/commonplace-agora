# Setting up Calibre-Web and koreader (via YunoHost and NextCloud)

[[Calibre-Web]] is a web interface for your Calibre library.

I have a [[Kobo]] and I use [[koreader]] on it.  koreader can download books from a Calibre-web install.

You could just copy your books directly between your Kobo and your computer via USB.  But I find often when I want to download a new book, I'm not near my computer.  So this is a nice way of being able to access your library anywhere you have wifi.  And it looks nice with the book covers and all.  If you feel bad about not having a physical bookshelf, you can show this to people so they can see how many clever books you've bought but not read.

![[images/calibre-web.png]]


## Installing Calibre-Web

You could manually self-install it somewhere, but I installed it on my [[YunoHost]] server, which I already have set up for various other things, including [[NextCloud]].

During the install there's an option to create a shared folder where it will look for your ebook.  The default is a directory that is also available in NextCloud:  `/home/yunohost.multimedia/share/eBook`.  I used this, as I already use NextCloud for syncing some files between devices.    You could sync other ways if you wanted - see the next section.


## Syncing your Calibre library to Calibre-Web

OK - so we'll assume you have [[Calibre]] on your local box already with your ebook library in it.

Now you want to sync your Calibre library to where your Calibre-Web install is expecting to find books.

Two main filesync options I would possibly use here - [[syncthing]] or [[NextCloud]].

I love syncthing and use it a lot, but haven't fiddled much with it on YunoHost yet.

I went with NextCloud for now, as it already includes the 'Shared multimedia' folder that the Calibre-Web default install used.

You need the NextCloud app installed on your desktop in order to sync.  By default it'll sync to and from a folder in your home directory called 'NextCloud'.

I had an existing Calibre library in a different place on my box, so I moved it to the eBook folder in my NextCloud folder (instructions on moving your library as per [How to Backup, Move and Export Your Library in Calibre - Calibre Blog](https://blog.calibre-ebook.com/how-to-backup-move-and-export-your-library-in-calibre/))


## Pulling the books down to koreader

Calibre-Web exposes your books as an OPDS catalog.  koreader has built-in support for OPDS catalogs so you can browse them straight from koreader. (https://github.com/koreader/koreader/wiki/OPDS-support).

In koreader:

-   Go to `OPDS catalog`
-   Choose `Add new catalog`
-   Put the address of your Calibre-Web install and your YunoHost user creds (LDAP working on all these apps is super handy)

I found a few bits of flakiness with the OPDS catalog.

-   It appears that only some catalogs work
    -   e.g. unread books works
    -   others show "catalog not found"

But, all being well, you should now be able to download your books from your library straight to your ebook reader.


## Misc Notes

Is it worth it?  I'm not convinced, if you're just using it on one device - maybe just easier to sync directly.

That said, syncing the library to NextCloud seems like a reasonable idea anyway.  It then gets picked up automatically in my server backups. 

I'm using [[Z-Library]] to download books that I've paid for already via the Kobo store, but want to read DRM-free on koreader.  I own them dammit, I can read them wherever I want.

