# climate modelling

> Climate models are planetary climate forecasts that span decades, or more. They use equations to represent the processes and interactions driving our climate across oceans, land masses, ice-covered regions and within the atmosphere. Crucially, they try to predict the outcomes that different types of human activity will have for global heating trends.
> 
> Today’s climate models are often run on massive super-computers, some capable of cumulatively generating up to 14,000 trillion calculations per second.
> 
> &#x2013; Down to Earth newsletter

