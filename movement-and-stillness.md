# Movement and Stillness

A
: [[podcast]]

Found at
: https://novaramedia.com/2023/11/05/acfm-trip-38-movement-and-stillness/

Part of
: [[ACFM]]

About social and political [[movement]]s. Their formation and their rhythms.

At one point they mention [[social movement theory]] and describe it as an almost cybernetic theory.  Interested to learn more about that then.

