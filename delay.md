# delay

> Delays such as this – between inflows and outflows – are common in [[systems]] and can have big effects. Sometimes they bring useful stability to a system, allowing stocks to build up and act as buffers or shock absorbers: think energy stored in a battery, food in the cupboard, or savings in the bank. But stock–flow delays can produce system stubbornness too: no matter how much effort gets put in, it takes time to, say, reforest a hillside, build trust in a community, or improve a school’s exam grades. And delay can generate big oscillations when systems are slow to respond – as anyone knows who has been scalded then frozen then scalded again while trying to master the taps on an unfamiliar shower
> 
> &#x2013; [[Doughnut Economics]]

