# Walney Island

![[photos/walney-coast.jpg]]

You can see the epic [[Walney Wind Farm]] and the [[Isle of Man]].

![[photos/black-combe-from-walney-island.jpg]]

[[Black Combe]] in the distance.

