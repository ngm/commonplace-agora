# Spatial software

&#x2013; [Spatial Software | Dark Blue Heaven](https://darkblueheaven.com/spatialsoftware/) 

Spatial software is the idea of bringing spatial interfaces into our software.  So far it's most commonly been in games, but you can see it spilling out into more things lately.

> We will see spatial interfaces move into all verticals, **starting with game-like interfaces for all kinds of social use-cases**.

<!--quoteend-->

> But this simplicity also strips away so much of the freedom we have during in-person interactions. **You can type any message, but typing a message is all you can do. You can call any person, but talking directly into your camera is all you can do once you're connected.**

<!--quoteend-->

> Designers and developers will begin using spatial interfaces in non-traditional ways, **taking design patterns from gaming and applying them to non-competitive, explicitly social use-cases**.

<!--quoteend-->

> Like a remake of AIM, where each user gets a house that can friends can stop by to visit. Or like a web browser with a topographical view, where users work together to build a spatial map of related websites. Or a video conferencing app where people can go on a walk together. In fact, you can put a world inside of almost any interface. Even a blog post.

**Like a remake of AIM, where each user gets a house that can friends can stop by to visit.**  This is kind of what I was thinking with wikis and [[collaborative memory palaces]]. 

**In fact, you can put a world inside of almost any interface. Even a blog post.**  Or a wiki perhaps.  What would that look like?  [[Antistatic gardens]] are maybe a hint at that.  A throwback to [[HyperCard]] perhaps.

-   http://nototo.app/

