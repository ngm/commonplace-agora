# The Society of the Spectacle

]]&#x2013; [[Guy Debord]]

Another work of political and cultural theory found on my brother's bookshelf.

Going off the back blurb, it's going to look at modernity, capitalism, and everyday life; and it's crucial "for understanding the contemporary effects of power, which are increasingly inseparable from the new virtual worlds of our rapidly changing image/information culture."

This overlap of power and new virtual worlds / information culture interests me.

> The book examines the "[[Spectacle]]," Debord’s term for the everyday manifestation of capitalist-driven phenomena; advertising, television, film, and celebrity.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> An earlier stage in the economy's domination of social life entailed an obvious downgrading of _being_ into _having_ that left its stamp on all human endeavour.  The present stage, in which social life is completely taken over by the accumulated products of the economy, entails a generalized shift from _having_ to appearing: all effective "having" must now derive its immediate prestige and its ultimate raison d'etre from appearances.
> 
> &#x2013; The Society of the Spectacle

<!--quoteend-->

> The spectacle is hence a technological version of the exiling of human powers in a "world beyond" - and the perfection of separation _within_ human beings.
> 
> &#x2013; The Society of the Spectacle

<!--quoteend-->

> By paraphrasing Marx, Debord immediately establishes a connection between the spectacle and the economy. The book essentially reworks the Marxist concepts of commodity fetishism and alienation for the film, advertising, and television age.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> First time readers of Debord’s work may prefer to read Comments first, since it is a brisker and more informal read than The Society of the Spectacle. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 


## Criticism

> The most significant criticism that can be leveled at The Society of the Spectacle is Debord’s failure to proffer any convincing solutions for countering the spectacle, other than describing an abstract need to put “practical force into action
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

