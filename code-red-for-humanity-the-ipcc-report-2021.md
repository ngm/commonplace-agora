# Code Red for Humanity: the IPCC Report 2021

A
: [[podcast]].

Series
: [[Red Menace]]

URL
: https://redmenace.libsyn.com/ipcc

They do a good recap on the [[IPCC Sixth Assessment Report]].

They talk about how [[climate anxiety]] should not become [[climate apathy]] and the need to organise even in the face of almost certainly pushing beyond 1.5 and probably 2 degrees and things falling apart.

Because capitalism will not simply collapse along with the climate.  It will likely accelerate and goose step towards its latent fascistic tendencies.

Also, interesting discussion on the merits of individual behaviour in the midst of an obvious need for large structural change.  [[Horizontalism vs verticalism]]. Given they tend towards [[vanguardism]] and the need for a vertical party, good to hear some constructive thoughts on the need for [[prefiguration]] too.

