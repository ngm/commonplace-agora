# The Week in Green Software: Disintegration vs Integration

A
: [[podcast]]

URL
: https://podcast.greensoftware.foundation/e/08jlzrq8-the-week-in-green-software

Series
: [[Environment Variables]]

A round up of various recent international reports on the severity of [[climate change]].

Finds some small cause for hope, in that while things are terrible, the trend is for things to become marginally less terrible.

Later focuses specifically on tech, and [[Big tech]] in particular.  Mentions some ways in which Microsoft, AWS, and Google, are adopting sustainability initiatives in their cloud offerings.

In my view though, while these are these are obviously positives, it does not change the fact that that they are capitalist through and through, and that [[capitalism is the cause of the climate crisis]], and that [[big tech is complicit in climate breakdown]].

