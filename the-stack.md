# The Stack

The big concept from [[Benjamin Bratton]]'s book [[The Stack: On Software and Sovereignty]].

See also [[Stack]].

> Bratton proposes treating the structure of [[planetary-scale computation]] through the vertical topological model of “the Stack”. He describes the Stack as a complex system, an “[[accidental megastructure]], a platform of platforms” comprised of six layers: Earth, Cloud, City, Address, Interface and User.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]


## What's in 'the stack'?

> The Stack is divided into six layers, moving from the global to the local, from geochemical up to the phenomenological: Earth, Cloud, City, Address, Interface, and User
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> At the top of any column, a User (animal, vegetable, or mineral) would occupy its own unique position and from there activate an Interface to manipulate things with particular Addresses, which are embedded in the land, sea, and air of urban surfaces on the City layer, all of which can process, store, and deliver data according to the computational capacity and legal dictates of a Cloud platform, which itself drinks from the Earth layer's energy reserves drawn into its data centers.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### Earth layer

> the Earth layer provides a physical foundation for The Stack.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> a foregrounding of the geological substrate of computational hardware and of the geopolitics of mineral and resource flows of extraction, consumption, and discarding.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> regarding the ultimate energy sourcing and routing necessary for planetary computational infrastructure and the paradoxes posed by the race to build smart grids capable of supporting its continuance and maturation.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> In important ways, it is possible for us to sense, quantify, and predict ecological precariousness through Stack technologies, and yet the production and feeding of those same systems are also key contributors to those same risks.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> the knotty problems of ecological governance and the issues posed by turning the ecology itself into a kind of final, ambient emergency.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### Cloud layer

> the vast server archipelagoes behind the scenes and behind the surface that provide ubiquitous computational services as well as the geopolitical intrigue that involves them.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> It includes in this the entire infrastructural complex of server farms, massive databases, energy sources, optical cables, wireless transmission media, and distributed applications.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> It focuses on the conflicts arising from the juxtaposition and superimposition of state geography and cloud platforms (i.e., the Google-China conflict) and on how the evolution of states into cloud platforms extends and complicates the locations of infrastructural and legal sovereignty. 
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### City layer

> The City layer of The Stack comprises the environment of discontinuous megacities and meganetworks that situate human settlement and mobility in the combination of physical and virtual envelopes.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> These partition and subdivide access to urban space, but in their generic comprehensiveness, they may also provide for forms of accidental cosmopolitanism, ones derived not from parliamentary certificates but from a shared physical relationship to pervasive infrastructure.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> We also examine the urban-scale imprints of major Cloud platforms and how their physical postures and positions disclose specific geopolitical imaginaries.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### Address layer

> Now closer to the scale of familiar objects and interactions, the Address layer examines massively granular universal addressing systems such as IPv6 (Internet Protocol version six) (including cryptographically generated hash addresses), which would allow for a truly abyssal volume of individual addressees. Such individuated addresses make any thing or event appear to the Cloud as a communicable entity, and for The Stack, computation then becomes a potential property of addressed objects, places, and events, and a medium through which any of these can directly interact with any other. 
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### Interface layer

> The Interface layer describes the projective, perceptual cinematic, semiotic layer on a given instrumental landscape, including the frames, subtitles, navigable maps, pixelated hallucinations, and augmented realities through which local signification and significance are programmed.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> Interfaces provide imagistic and linguistic mediation between Users and the Addressable computational capacities of their habitats, priming the pump for possible communication.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


### User layer

> At the top of The Stack is the most culturally complex layer, the User.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> This chapter describes how The Stack sees the humans and nonhumans that initiate columns up and down its layers, from Interface to Earth and back again.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]

<!--quoteend-->

> Human and nonhuman Users are positioned by The Stack (perhaps rudely) as comparable and even interchangeable through a wide-ranging and omnivorous quantification of their behaviors and effects.
> 
> &#x2013; [[The Stack: On Software and Sovereignty]]


## The Stack in action

A useful interpretation of something happening in the Stack from McKenzie Wark:

> Your desire has to be parsed into a form a machine can understand; that’s the job of this interface layer. The interface also positions you in relation to it, and to the rest of the stack, as a particular kind of subject: you are a user. Let’s say you are a user who wants a book. You say: “Alexa, order me a copy of Capital by Karl Marx.” Once you confirm that this is what you really want, this information will pass as if it were a vector, a particular kind of line, through a whole other series of layers of stack infrastructure, which will return this product to you, either immediately (if it is an e-book) or in a day or two (if it is a physical object). Each such expressed desire becomes a unique vector through a layered space that can fulfill an almost infinite number of desires, so long as they all take the form of a user asking an interface to satisfy a demand with a commodity. It does not really let you want or be much else.
> 
> &#x2013; [[Capital is Dead]]

<!--quoteend-->

> Your desire becomes a vector that will pass through many more layers of the stack. Bratton calls these the address, city, cloud, and earth layers. The address layer knows where you are, and it knows where the book that you want is, and it can calculate the optimal return vector to get one to the other to fulfill this desire. The city layer is where the physical part of the infrastructure resides. There is a warehouse, somewhere.30 There is a server farm, somewhere; there are Amazon offices that design and manage and sell all this stuff—somewhere.
> 
> &#x2013; [[Capital is Dead]]

<!--quoteend-->

> The cloud layer connects all these sites and many others together and performs the operations on the information gathered from all of them not only to fulfill orders and manage every vector, but also to learn from the aggregate of all of these actions and predict how else to extract information from them.31 The earth layer is that from which the resources and energy to make and run this whole vast edifice to the digitized commodity are extracted.32 Those resources are fed into sites of production that will make that book you ordered, or the t-shirt, or the sex toy, or whatever.
> 
> &#x2013; [[Capital is Dead]]


## The Stack and political economy

> With respect to political economy, the main novelty of the Stack is the introduction of platforms as autonomous forms of organisation. A platform is defined as “a standards-based technical-economic system that simultaneously distributes interfaces through their remote coordination and centralizes their integrated control through that same coordination”
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

<!--quoteend-->

> In our reading, the emergence of the Stack as an accidental megastructure reflects the tendency towards an integrated global capitalism. This form of capitalism means an intensification of labour and environmental exploitation on a planetary scale, coupled with the emergence of new forms of sovereignty (i.e. [[platform sovereignty]]).
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

<!--quoteend-->

> This insight supports the hypothesis that the Stack represents an intensification of the vertical integration of global capitalism, but neither through the emergence of a new form of capital, nor through a new mutation in its mode of production, nor from the sublation of the law of labour-value.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

contra [[Vectoralism]]?

> we note its complex dynamics with respect to global production networks and the sovereignty of state and non-state actors. We observe how the reproduction of infrastructure of and for planetary-scale computation depends on Fordist accumulation and exploitation in the Global South, as well as on the role of China as the Stack’s most central production and trading hub of semiconductor-based infrastructure and interfaces.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]


## Critcism

> While it identifies a generic structure of planetary-scale computation, it leaves the question of hegemony and therefore of any crucial political economic implications intentionally unresolved, since the digital infrastructure is subject to competing forms of sovereignty.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

They don't really present it as a criticism as such, they just note that it is description, not normative.

In [[Principles of Stacktivism]], the author seems to have a beef with [[The Stack]] and [[Benjamin Bratton]], which is interesting to read.  Though at the same time they also seem to find it a useful framework to situate yourself against.

