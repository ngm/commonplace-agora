# Consciously arranged society

> And while Marx didn’t say much about economics after the revolution, he did insistently name the state he promised was coming, at history’s happy end. He called it ‘consciously arranged society’.
> 
> &#x2013; [[Red Plenty]]

If you search for "consciously arranged society" you don't get many results.  Where did Marx call it that?

