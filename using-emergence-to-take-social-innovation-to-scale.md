# Using Emergence to Take Social Innovation to Scale

-   https://berkana.org/wp-content/uploads/2020/04/Emergence-Booklet-English.pdf

Related to the [[Two Loop Model]].

> Rather than worry about critical mass, our work is to foster critical connections. We don’t need to convince large numbers of people to change; instead, we need to connect with kindred spirits. Through these relationships, we will develop the new knowledge, practices, courage and  commitment  that  lead  to broad-based change.

<!--quoteend-->

> When separate, local efforts connect with each other as networks, then strengthen as communities of practice, suddenly and surprisingly a new system emerges at a greater level of scale.

[[Networks]] turn in to [[Community of practice]] turn in to [[systems of influence]].

