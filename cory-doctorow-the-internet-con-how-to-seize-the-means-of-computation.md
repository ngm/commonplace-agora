# Cory Doctorow, "The Internet Con: How to Seize the Means of Computation"

URL
: https://newbooksnetwork.com/the-internet-con

Featuring
: [[Cory Doctorow]]

Doctorow discussing his book

[[Seize the means of computation]] is pretty much the same sentiment as [[reclaim the stacks]].

[[Interoperability]] is Doctorow's main lever.  [[adversarial interoperability]] being the most radical type of interoperability.

[[Taxonomy of interoperability]].

