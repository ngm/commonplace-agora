# Digital Capitalism online course

Found at
: https://www.tni.org/en/event/digital-capitalism

A course on [[digital capitalism]] from the [[Transnational Institute]].

> 6 week essential course for activists to understand how [[digitalisation]] is shaping our world


## Course outline

-   1: What is digital capitalism?
-   2: Big Tech and the Digital Overlords
-   3: Digital colonialism - Geopolitics of data and development
-   4: The digital trade agenda
-   5: Digitalisation and the Security State
-   6: What's the alternative? The digital world we want to live in

I'm most looking forward to section 6.

![[digital-capitalism-course.png]]


## Week 1: What is digital capitalism?

> How is [[digitalisation]] shaped by capitalism and how is digitalisation shaping capitalism? Is data today’s most important commodity? What will be the impact of algorithms for social movements? How is digitalisation impacting on labour and the environment?


### Notes from reading

-   Difference conceptions of digital capitalism
    -   e.g. [[Surveillance capitalism]], [[platform capitalism]], [[Digital colonialism]], [[Techno-feudalism]]
-   [[Datafication]]
-   Problems of [[technological solutionism]]
-   Algorithms and bias ([[Algorithmic bias]])
-   From persuasion to prediction
-   Worker [[alienation]]
-   Impacts on [[rights]], [[development]] and the [[environment]]


### Resources

-   [[Cartografias da Internet]]
-   [The problems of digital capitalism - YouTube](https://www.youtube.com/watch?v=lFvX-wIlRTs)
-   [In a nutshell: Shoshana Zuboff: Shoshana Zuboff: Surveillance Capitalism and &#x2026;](https://www.youtube.com/watch?v=5AvtUrHxg8A)
-   [Evgeny Morozov: Competition and Cooperation in Digital Capitalism (#DigitalCa&#x2026;](https://www.youtube.com/watch?v=FKOKjDQ662A)
-   [The dangers of digital capitalism - YouTube](https://www.youtube.com/watch?v=iHGYUReqZTs)

-   [Seizing the means of computation – how popular movements can topple Big Tech &#x2026;](https://www.tni.org/en/article/seizing-the-means-of-computation-how-popular-movements-can-topple-big-tech-monopolies)
-   [Are We Transitioning From Capitalism to Silicon Serfdom?](https://jacobin.com/2024/02/yanis-varoufakis-techno-feudalism-capitalism-interview)
-   [Evgeny Morozov, Critique of Techno-Feudal Reason, NLR 133/134, January–April &#x2026;](https://newleftreview.org/issues/ii133/articles/evgeny-morozov-critique-of-techno-feudal-reason)

-   https://listen.datasociety.net/episodes/race-after-technology


### Questions


#### In 50-200 words, tell us about your experience with digital capitalism. When did you feel that the digital products you were enjoying were bringing problems to our society?

I work as a software developer, and I have loved free and open source software for a long time, getting hooked on Linux at university.  The antagonism towards proprietary software in the FLOSS world introduced me to an early notion of struggling against capitalism in a digital form. Back then the main protagonist was Microsoft.

Over time I came to understand libre software as an example of digital socialism. But I have also learned that there is more to digital capitalism than simply proprietary code.  I was enthusiastic about Google at first, for example, as they embraced open source.  Yet over time it became clear that you can still be at the commanding heights of digital capitalism while using and creating free software.

Similarly, I have been disappointed as the folksonomy of Web 2.0 has morphed into platform capitalism, and decentralised technologies and work have become enclosed (e.g. Git and Github).

In recent years I've been increasingly concerned about the environmental impact of digital technology - from the intense energy use of data centres, to the resource extraction and e-waste associated with digital products.


#### In 50-200 words, tell us what are your biggest concerns about digital capitalism.

My biggest concerns with digital capitalism is its destructive impact on the environment and its exploitation of human labour.

Digital capitalism makes a huge contribution to environmental degradation and climate change. For example, the constant manufacture and upgrade cycle of the devices that power digital capitalism (from phones to computers to servers) requires a vast amount of mining of resources.  The e-waste that comes out of the other end is the fastest growing waste stream.  The endless growth of digitalisation also requires immense computational resources, which requires huge amounts of energy and water.

Digital capitalism also exploits people around the world.  Labourers in mining and manufacturing are exploited mercilessly in the pursuit of profit.  End-users of digital products are pushed to become addicted to their devices, to become hostile and polarised online, and to disengage from the civic sphere.

I believe that digital technologies can be truly liberatory, but only when the means of production are socialised and taken out of the hands of the monopolistic cabal of Big Tech firms.


## Week 2: Big Tech and the Digital Overlords

> How did a few digital companies become so powerful? How do they differ to other non-digital corporations? What is the source of their power, finance and influence? How successful have regulatory attempts been to limit their power? How can we effectively rein them in?


### Notes from reading


### Further materials

-   [The Intelligent Corporation: data and the digital economy - Longreads](https://longreads.tni.org/stateofpower/the-intelligent-corporation-data-and-the-digital-economy)
-   [Digital Power | Transnational Institute](https://www.tni.org/en/publication/stateofpower2023)
-   [Tech States: Apple - Tortoise](https://www.tortoisemedia.com/file/tech-states-apple/)
-   [The financialisation of Big Tech - SOMO](https://www.somo.nl/the-financialisation-of-big-tech/)
-   [How to Destroy Surveillance Capitalism, a New Book by Cory Doctorow | OneZero](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59)
-   [State Of Big Tech &amp;#x2d; Dismantling Big Tech Enclosures](https://projects.itforchange.net/state-of-big-tech/)
-   [Who owns the internet? The true story behind the power of Big Tech - YouTube](https://www.youtube.com/watch?v=SsksFJjsMGs)
-   [Jeff Bezos' Business Empire is WAY Bigger Than You Think - YouTube](https://www.youtube.com/watch?v=vTrxfL4qHC4)
-   [Google, Facebook, Amazon - The rise of the mega-corporations | DW Documentary&#x2026;](https://www.youtube.com/watch?v=Dy8ogOaKk4Y)
-   [Intellectual Property &amp; Monopoly Capitalism — with Cecilia Rikap - YouTube](https://www.youtube.com/watch?v=qNS0q9c8_nA)
-   [Are Big Tech Companies Becoming Banks? - YouTube](https://www.youtube.com/watch?v=O_NLDP3sM9I)


### Questions


#### In 50-300 words, write your thoughts on what tactics and strategies might work best for confronting Big Tech and taking on their power?

I like the strategic framework suggested by James Muldoon in Platform Socialism. It has three main areas of action: Resist, Regulate, Recode.  (Which seems to map very closely to working "Within the system, against the system, and beyond the system" as mentioned by Ulises in week 3's masterclass!)

Resistance is directly confronting Big Tech to create cracks in its structures.  Tactically this might look like actions such as unionisation, strikes, protests, etc.

Regulation is seeking legal routes to rein in Big Tech.  This may take the form of pushing for anti-monopoly legislation and interoperability legislation, for example.

Recoding is actively building alternative structures that provide another option for people to use. This could be developing things such as alternative software (e.g. Linux, Mastodon, LibreOffice, NextCloud, etc); and alternative organisational structures such as tech worker coops, etc.

I think all of this should be done with the strategic end goal of digital ecosocialism: socially-owned, democratically-governed ICT infrastructure rooted in the commons that is used to facilitate a socially equitable society that stays within planetary boundaries.


## Week 3: Digital colonialism - Geopolitics of data and development

> How has digitalisation perpetuated global inequality? Who benefits and loses from global value chains? How is the internet and data management governed globally? What are the strategies of the major state digital powers - US, China and Europe – and where does that leave everyone else?


### Notes from reading

-   [[Data value chains]]
-   [[Datafication]] of social life / [[data colonialism]]
-   Individual data has little value - only when it is aggregated does it gain relative value
-   Assetization


### Further materials

-   [Digital colonialism: The evolution of US empire - Longreads](https://longreads.tni.org/digital-colonialism-the-evolution-of-us-empire)
-   [View of Undersea cables in Africa: The new frontiers of digital colonialism](https://firstmonday.org/ojs/index.php/fm/article/view/13637/11607)
-   https://unctad.org/system/files/official-document/der2021_en.pdf
-   [In a nutshell: Nick Couldry on Data colonialism - YouTube](https://www.youtube.com/watch?v=5tcK-XIMQqE)
-   [Digital feudalism: The future of data capitalism - YouTube](https://www.youtube.com/watch?v=fKgPQSa1_0o)


### Questions


#### Q1: Do you think GDPR and other data regulations around the globe are enough? What collective rights over data can you propose?

No, individual-focused regulations such as GDPR are not enough.  They are a step in the right direction, as they begin to move the needle away from corporate ownership of our data.  But they focus on individual data and individual consent, and do not cover aggregate data.  As stated in the reading materials: "the pitfalls of such a logic is that it renders data into private property and obscures the structural crisis of data capitalism by reducing it to a “choice” to share/not share one’s own data." (https://digitalcapitalismcourse.tni.org/mod/book/view.php?id=32&chapterid=58)

Data regulations should also be pushing for public, collective ownership of our data.  Initiatives such as data commons and data trusts should be supported.


## Week 4: The digital trade agenda


### Notes from reading


### Further materials

-   https://ourworldisnotforsale.net/2020/Digital_trade_rules.pdf
-   [Digital colonialism | Transnational Institute](https://www.tni.org/en/publication/digital-colonialism)
-   [EU Digital Trade Rules: Undermining attempts to rein in Big Tech – The Left](https://left.eu/issues/publications/eu-digital-trade-rules-undermining-attempts-to-rein-in-big-tech/)
-   https://www.southcentre.int/wp-content/uploads/2022/06/RP157_WTO-Moratorium-on-Customs-Duties-on-Electronic-Transmissions_EN.pdf
-   [Webinar Digital Trade Rules with Deborah James, Rashmi Banga, Caroline Khamat&#x2026;](https://www.youtube.com/watch?v=n0BdM2ddCJg)
-   [Webinar - Key Issues for Developing Countries in Negotiations on the Taxation&#x2026;](https://youtu.be/6Ny0NtszUfw)


### Questions


#### How do you think the digital trade rules might impact your area of work or activism or concern that you are involved in? Explain briefly in 50-300 words


## Week 5: Digitalisation and the Security State


### Questions


#### In 50-300 words, share either an experience of fighting against digitalised security in your own country/region or your thoughts about what should be the priorities for campaigning on this in your own country.


## Week 6: What are the alternatives?


### Questions


#### In 50-300 words, describe the strategy or some of the steps we need to take to a build a more just digital future. Click reply to add your response.


#### How do you plan or hope to take the learning from this module and the course overall forward into your work individually, within your organization, or within a movement? Click reply to share

