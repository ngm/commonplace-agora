# Hugo Blanco

> He joined with enthusiasm in the campaign against globalisation, and was a committed ecosocialist and climate justice activist.
> 
> &#x2013; [Hugo Blanco obituary | Peru | The Guardian](https://www.theguardian.com/world/2023/aug/21/hugo-blanco-obituary)

<!--quoteend-->

> Peruvian revolutionary Hugo Blanco. While Blanco retains fraternal links with the [[Fourth International]], his present politics is closer to that of the Mexican [[Zapatistas]]. He publishes the newspaper Lucha Indigena and is also an active support of the Rojava Revolution. Originally an agronomy student, he studied in Argentina, he led a peasant uprising in the early 1960s which successfully gained land reform. During his many decades of activism he has become increasingly engaged in ecological and indigenous struggles (Wall, 2018). As I write, he is in his 80s but remains a leading [[ecosocialist]] thinker and activist.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

