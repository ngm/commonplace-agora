# Immediatism: 690 Debord, Ressentiment, & Revolutionary Anarchism, by Aragorn!

URL
: https://immediatism.com/archives/podcast/690-debord-ressentiment-revolutionary-anarchism-by-aragorn

Start of a series on [[Situationism]].

Talks a bit about [[Guy Debord]]'s critique of [[anarchism]] in [[The Society of the Spectacle]].

