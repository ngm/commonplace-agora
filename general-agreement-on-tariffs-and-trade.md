# General Agreement on Tariffs and Trade

> An international trade organization was also proposed, but when the US Senate failed to ratify this part of the proposal, it was not founded. Later the GATT, the General Agreement on Tariffs and Trade, was established and took on the functions that the failed ITO would have fulfilled. Later the GATT was superseded by the [[World Trade Organization]].
> 
> &#x2013; [[The Ministry for the Future]]

