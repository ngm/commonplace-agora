# Beaker Browser

-   https://beakerbrowser.com/

> the web browser that replaces HTTP with Hypercore and Hyperdrive
> 
> &#x2013; [Redecentralize Digest — May 2020 — Redecentralize.org](https://redecentralize.org/redigest/2020/05)

<!--quoteend-->

> More than just a browser for websites that do not rely on any single server to host them, Beaker now puts extra focus on its visionary concept: every user is a participant on the web, owns their own data, can clone and edit any app, and publishes content and comments right from their browser without platforms in between.
> 
> &#x2013; [Redecentralize Digest — May 2020 — Redecentralize.org](https://redecentralize.org/redigest/2020/05)

