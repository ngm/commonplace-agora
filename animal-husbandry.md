# animal husbandry

> The food we eat masks so much cruelty. The fact that we can sit down and eat a piece of chicken without thinking about the horrendous conditions under which chickens are industrially bred in this country is a sign of the dangers of capitalism, of how capitalism has colonized our minds. We look no further than the commodity itself. We refuse to understand the relationships that underlie the commodities that we use on a daily basis
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Instead of worrying that animal husbandry could spark foreign wars, More lambasted its role in the internal colonization of England. In his day, wool merchants turned ‘meek and tame’ sheep into monsters that ‘consume, destroy, and devour whole fields, houses, and cities
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> husbandry and early capitalism, a relationship that would become more obvious by the end of the century
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Animal husbandry takes up 4 billion hectares – 40 per cent of Earth’s inhabitable land
> 
> [[Half-Earth Socialism]]

