# Gardens and Streams II

URL
: https://indieweb.org/2021/Pop-ups/Gardens_and_Streams_II

5 - 9pm UK time.

I was only able to attend the first session: Different camps of thought in the digital gardening world

This was proposed by Alasdair Ekpenyong and was to see what were individual motivations for digital gardening.  It was combined with a proposal from Bill Seitz on what was the purpose / theory of change of the (indie) gardening community.

It was interesting to hear the different perspectives.  Some come from PKM, some from writing, some from having an IndieWeb site that they realised they were using as a commonplace book anyway.  Others seeing digital gardens as a push back against the likes of Facebook and Twitter.

For me: I come to the community from both the garden and stream perspectives. I've had an indieweb site for about 4 years or so, seeing this as more of the streaming side of things. I've had a digital garden in it's roughly current form since about 2020 (although obviously taking notes from before then). This came from a desire for better PKM and getting my thoughts in order. I do it to bolster my own ideas on technology, politics, the environment, and culture. **Learning in public** to motivate and to get feedback.  Recently I've also been publishing to Anagora. I kind of view it as a garden aggregator.  I see this as part of building a knowledge commons, an  alternative way of doing this to a centralised wiki. Multiple personal wikis combining.

