# Architectural rights

> One of the most useful tools for analyzing a network is the “architectural right.” This is, broadly speaking, any capability which is built into the network by its protocols and standards.
> 
> &#x2013; [[Information Civics]] 

