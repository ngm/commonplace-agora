# 2022 Week 14 Summary

-   I was on holiday this week and went to visit E's family in [[North Norfolk]].  It has big skies and lovely beaches and [[salt marsh]]es.
-   Lots of binocular action.  Involved lots of sitings of [[birds]].  Starting to recognise a few more. We saw [[seals]] which were amazingly cute and comical from a distance.
-   Learned a little about [[using solar panels for the home]].
-   I read a fair bit about the [[IPCC Sixth Assessment Report]], part 3.  Laying out what needs to be done.  In essense: renewable energy, electric vehicles, low-carbon technology.  "This report is essentially a manifesto for ending the fossil fuel age".  We have the knowledge and the means, it just needs putting in to action.
-   In tandem, read about and learned that [[the UK energy strategy is terrible]].  Focusing on nuclear, ignoring [[onshore wind]], even pushing fossil fuels.
-   I didn't read much about Ukraine while away, but on return see that it is getting increasingly grim and on course to escalate further.
-   I've been reading a bit more of [[Cybernetic Revolutionaries]].  Interesting to contrast the Chilean version of a planned economy to the Soviet one.  Chilean seems much better.
-   Next week is still Easter school holidays, and also it's E's birthday, so spending time on that.

