# Degrowth communism

[[Kohei Saito]].

[[Degrowth]] [[Communism]].

Saito says something like: degrowth needs communism, communism needs degrowth.

> So when I talk about degrowth communism, this is actually coming from Karl Marx. So I even argue that Marx became a degrowth socialist, a degrowth communist in the end of his life.
> 
> &#x2013; [[Kohei Saito on Degrowth Communism]]

