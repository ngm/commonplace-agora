# information

> Information only exists when there’s a material substrate of matter and energy to store, transmit, and process it. Information is part of a material world. But it’s a strange part. The word information is hardly new, but the science of information is very new; it is a postwar creation
> 
> &#x2013; [[Capital is Dead]]

<!--quoteend-->

> Information is a rather strange thing. Contrary to the popular understanding, there’s nothing ideal or immaterial about it
> 
> &#x2013; [[Capital is Dead]]

