# knowledge graph

> a knowledge base that uses a graph-structured data model or topology to integrate data.
> 
> &#x2013; [Knowledge graph - Wikipedia](https://en.wikipedia.org/wiki/Knowledge_graph) 

