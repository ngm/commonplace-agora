# digital self-determination

> Digital self-determination will involve: 
> 
> -   making use of the technical tools available to communicate freely
> -   designing information infrastructure in ways that favour de-centralisation
> -   designing online spaces and devices that are welcoming
> 
> &#x2013; [[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind]]

<!--quoteend-->

> A better way to understand what we mean when we talk about privacy, then, is to see it as a right to self-determination. Self-determination is about self-governance, or determining one’s own destiny.
> 
> &#x2013; [[Future Histories]] 

<!--quoteend-->

> Self-determination is both a collective and individual right, an idea of privacy that is much more expansive and politically oriented. It is about allowing people to communicate, read, organize and come up with better ways of doing things, sharing experiences across borders, without scrutiny or engineering, a kind of cyberpunk internationalism.
> 
> &#x2013; [[Future Histories]] 

[[Agency]]. [[Politics of technology]].

