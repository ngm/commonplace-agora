# Peer discovery

> However, techniques for peer discovery, such as [[distributed hash table]]s (DHTs) and ‘[[gossip protocol]]s’ mean that we have a lot of flexibility about which server is used. We can have a dynamic ‘pool’ of cooperative servers, meaning the server we happen to use does not constitute a single point of failure. This minimises the degree of power the server operators have over the network.
> 
> &#x2013; [[Seeding the Wild]]

