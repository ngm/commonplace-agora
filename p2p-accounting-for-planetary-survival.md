# P2P Accounting for Planetary Survival

URL
: http://commonstransition.org/p2p-accounting-for-planetary-survival/

Really interesting.  A sketch of [[resource allocation]] that meets foundational life needs but stays within planetary boundaries.  Something of a synthesis of markets, planning, and commoning.  A pro-technology stab at it, leaning on [[distributed ledger]]s.

> Our inspiration comes from the great synthesis provided by Kate Raworth in her book, [[Doughnut Economics]], which graphically presents the great question of our age: can we produce for human needs, without exceeding planetary boundaries?

<!--quoteend-->

> This report will introduce topics such as [[contributory accounting]], which recognizes generative contributions to the wellbeing of the earth and its inhabitants; [[value flow accounting]], posed as an alternative to the narcissistic [[double-entry bookeeping]], upon which capitalism relies to remain blind to its social and ecological impact; and thermo-dynamic accounting, which provides direct access to non-financialized streams of matter and energy

References the [[socialist calculation debate]].  But adds in a third option beyond the usual market and state distinction: "that of the [[commons]] and their self-management through mutual coordination."

> Today, large capitalist firms certainly plan, but they do not plan for balance with humanity and natural beings, it is extractive planning. The market allocates resources, but without any knowledge of impact. 

For more on capitalist firms engaging in planning, see [[The People's Republic of Walmart]].

"The market allocates resources, but without any knowledge of impact" - yes.  I think that's getting at the nub of things for me.  I like decentralised, complex systems, and the market is one of those, for sure.  But we need a layer of planning and limits placed on top of it, that ensures it stays within boundaries.

> So what we are describing here is a 3-layered economy, and its infrastructure, that is able to coordinate production, by transcending and including the 3 great methods of allocating resources:
> 
> -   Mutual coordination through shared logistics and shared accounting
> -   Ethical, generative market mechanisms for the fair exchange of resources
> -   A planning framework, indicating the planetary resources available for human choice, so that we can produce while preserving the planet and its beings, and even regenerating them

Sounds pretty great.

> This report is based on the understanding that one of the main weaknesses of the current political economy is its inability to recognize and deal with ‘externalities’, in regards to costs and benefits received or caused by economic actors that are not accounted or paid for. 

<!--quoteend-->

> We believe that a significant number of these necessary ingredients for such a structural change are available through some of the emerging techno-social systems that are co-evolving with distributed networks. 

<!--quoteend-->

> [[Protocol cooperatives]] are global open source repositories of knowledge, code and design, that allow humanity to create infrastructures for the mutualization of the main provisioning systems (such as food, habitat, mobility), and that are governed by the various stakeholders involved, including the affected citizenry.

<!--quoteend-->

> [[REA accounting]], i.e., accounting for Resources, Events, Agents, allows actors to see their transactions as part of an ecosystem of collaboration, which is ‘flow accounting’ rather than a vision based on the accumulation of assets in a single firm. 

<!--quoteend-->

> So leave behind today’s widespread obsession with [[smart contracts]], [[platform capitalism]] and [[economies of scale]]: these only serve to reinforce last century’s dominant and extractive modes of production. Instead, dive into this report and discover the possibilities of [[Ostrom contracts]], [[platform cooperativism]] and [[economies of scope]]. These ideas are the seeds of a generative commons-based economy that is fit for the 21st century’s social and ecological challenges.

