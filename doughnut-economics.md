# Doughnut Economics

A
: [[book]]

Written by
: [[Kate Raworth]]

I like the book.  Combines alternative economics, systems thinking, care, feminism, environmentalism, some socialist ideas.

I very much like the idea of a regenerative and distributive economy.  Not sure if she'd describe it as such, but it comes across fairly [[ecosocialist]], with its dual concern for [[social equity]] and [[planetary boundaries]].  Though not explicitly anti-capitalist, it's anti neoliberal economics.


## The Doughnut

I like how easily graspable as a visual rubric it is - don't let anyone go into the hole of the doughnut, i.e. have a baseline of equity for everyone, and don't go outside the outer edge - i.e. stay within planetary boundaries.

The bit in the middle, the doughnut, is a regenerative and distributive economy.

![[doughnut-model.jpg]]

(CC-BY-SA 4.0 from ![[https://commons.wikimedia.org/wiki/File:Doughnut_(economic_model).jpg]])


## Ecological ceiling

-   [[Climate change]]
-   Ocean acidification
-   Chemical pollution
-   Nitrogen &amp; phosphorous loading
-   Freshwater withdrawals
-   Land conversion
-   [[Biodiversity loss]]
-   Air pollution
-   Ozone layer depletion

i.e. the [[planetary boundaries]].


## Social foundation

-   Water
-   Food
-   Health
-   Education
-   Income &amp; work
-   Peace &amp; justice
-   Political voice
-   Social equity
-   Gender equality
-   Housing
-   Networks
-   Energy

I like the seven ways to think like a 21st century economist:

-   Change the Goal: Shift from GDP growth to thriving within the Doughnut's ecological and social boundaries
-   See the Big Picture: Recognize that economies are embedded within society and the environment.
-   Nurture Human Nature: Promote cooperative and caring behaviors over competitive ones.
-   Get Savvy with Systems: Understand economies as complex, interdependent systems.
-   Design to Distribute: Create distributive economies that address inequality.
-   Create to Regenerate: Transition from degenerative to regenerative economic practices.
-   Be Agnostic about Growth: Focus on thriving rather than perpetual growth.

