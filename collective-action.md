# collective action

Probably an overloaded term.

Have seen it used most recently in [[Neither Vertical Nor Horizontal]].

> By contrast, collective action properly speaking would refer to those cases in which people not only perceive themselves as participating in a broader common identity – that is, as belonging to a collective subject – but also intentionally come together and engage in processes of deliberation, planning, assessing, intervening, and so forth. 
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

Seen it mentioned before in [[Theory of collective action]].

Don't know if any relation between the two.

