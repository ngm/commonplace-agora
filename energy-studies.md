# energy studies

> By refusing to countenance restrictions on energy use in the rich world, Northern environmentalists have fostered little solidarity with potential allies in the Global South
> 
> [[Half-Earth Socialism]]

