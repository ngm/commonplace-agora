# Figuring out org-roam-bibtex

So I'm reading academic papers more of late, and storing them in [[Zotero]].

However I'm taking notes on them (and books, online articles, etc) in [[org-roam]].

I **think** [[Org Roam BibTeX]] can gain me something here.  But I've never quite got my head around what it actually does and if I need it.  I'll try and get to the bottom of it.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-12-11 Sun]</span></span>

OK, so first foray in to this.

I have a bibtex file exported from Zotero (with Better Bibtex).

I set `org-cite-global-bibliography` to point to that .bib file.

I insert with `org-cite-insert`.

I get a little cite link that when clicked pulls up the details of the citation in the bib file.

So I think probably what org-roam-bibtex will add to the mix is that I will be able to cite **my notes files** on a particular resource.  And still have it exported as citations of the referenced resource, when exporting the file that has the citations in it.


## Resources

-   https://github.com/org-roam/org-roam-bibtex/issues/245

