# Turning the Earth into Money w/ John Bellamy Foster

A
: [[podcast]]

URL
: https://tribunemag.co.uk/2022/05/turning-the-earth-into-money-john-bellamy-foster

Featuring
: [[John Bellamy Foster]]

> They discuss Marx’s metabolic theory of nature and the ‘[[metabolic rift]]’ that shapes the relationship between humanity and nature under capitalism, as well as the ongoing relevance of the theory of [[monopoly capital]] put forward by Monthly Review founders Paul Baran and Paul Sweezy.

