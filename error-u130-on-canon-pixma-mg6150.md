# Error U130 on Canon Pixma MG6150

I had this Error U130 come up on a Canon Pixma MG6150.

I can't remember the exact wording, but something about it not being able to determine how much ink was in one of the cartridges.  The cartridge wasn't an official Canon one, so maybe that's related.

Anyway - from watching [this handy video](https://www.youtube.com/watch?v=nbPjSg4tZVU) on YouTube it turns out that  just holding the red 'Stop' button down for 5 seconds or so stops you being pestered by the error, and you can print again.

Now, whether there's any longer term repercussions to ignoring the error, I don't know, but it's useful to know if you need to print in a pinch and this error is blocking you.

