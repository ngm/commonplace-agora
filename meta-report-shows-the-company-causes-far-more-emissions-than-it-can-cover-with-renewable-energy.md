# Meta report shows the company causes far more emissions than it can cover with renewable energy

URL
: https://www.datacenterdynamics.com/en/news/meta-report-shows-the-company-causes-far-more-emissions-than-it-can-cover-with-renewable-energy/

> [[Facebook]] owner [[Meta]] has been growing too fast to achieve its goal of being net zero in 2030, according to a report it has issued.

<!--quoteend-->

> Only one percent of the company's emissions are in its direct control, leaving 8.5 million tonnes of CO2 equivalent unaccounted for.

<!--quoteend-->

> The vast majority of Meta's emissions are from its supply chain and partners, and the company appears to have little prospect of getting those under control.

[[Scope 3 emissions]].

> Since 2020, Facebook says, it has kept emissions from its own operations to net zero, largely by buying renewable energy. The company buys more than 10GW of renewable power globally.

<!--quoteend-->

> However, renewable energy only affects the company’s Scope 2 emissions (from electricity used). Bringing these to zero leaves its own emissions (Scope 1) which are around one percent of its total emissions, and the vast emissions from its supply chain and ecosystem (Scope 3), which make up virtually all its environmental footprint (99 percent).

<!--quoteend-->

> Meta admits its growth over the last five years has vastly exceeded its ability to cancel the emissions: “Our business growth accelerated at a faster pace than we can scale decarbonization."

