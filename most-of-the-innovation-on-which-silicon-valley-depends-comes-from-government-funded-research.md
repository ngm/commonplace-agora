# Most of the innovation on which Silicon Valley depends comes from government-funded research

A
: [[claim]]

> Most of the innovation on which Silicon Valley depends comes from government-funded research
> 
> &#x2013; [[Internet for the People]]

[[Mariana Mazzucato]] researched this I believe.


## Because

> for the simple reason that the public sector can afford to take risks that the private sector can’t
> 
> &#x2013; [[Internet for the People]]

