# Indymedia

An open publishing network of activist journalist collectives that report on political and social issues.

> Vanguard stacks already lurk in the archaeology of colonial systems through legacies like Indymedia, an activist social network whose participatory servers and software prefigured the corporate “Web 2.0”
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

<!--quoteend-->

> Some of the earliest online social media emerged through Indymedia’s coverage of anti-capitalist protests.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

