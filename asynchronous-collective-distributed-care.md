# Asynchronous Collective Distributed Care

Spread the load of [[free software project management]].  Down with the [[Benevolent Dictator For Life]].

> To resist this HOT mindset, let's redefine maintenance as ACDC: Asynchronous Collective Distributed Care:
> 
> -   **Asynchronous** because time management is a private matter and we are all volunteers.
> 
> -   **Collective** because, well, no man is an island.
> 
> -   **Distributed**: the more power to the "edges", the more resilient the project is.
> 
> -   **Care** because this is all about care: with each other as users or as contributors, with the project's infrastructure (servers, websites, bug trackers, etc.) and care about having a useful product.
> 
> &#x2013;  [[How to help GNU Emacs maintainers?]]

