# social Taylorism

The [[social industry]] has abstracted our human interactions into mindless, repetitive actions, from which they can more easily derive profit.  The disbenefits are many - alienation; surveillance; manipulation.

It is social [[Taylorism]].  The craft of social relationships has been reduced to unskilled labour.  

> The [[spectacle]] reduces reality to an endless supply of commodifiable fragments, while encouraging us to focus on appearances. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Debord would no doubt have been horrified by social media companies such as Facebook and Twitter, which monetize our friendships, opinions, and emotions. Our internal thoughts and experiences are now commodifiable assets. Did you tweet today? Why haven’t you posted to Instagram? Did you “like” your friend’s photos on Facebook yet?
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

-&#x2014;

Discovered that [[digital Taylorism]] is already a fairly written about thing!

