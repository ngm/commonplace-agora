# Steel boss dismisses claim that sector needs new Cumbrian coalmine

URL
: https://www.theguardian.com/business/2022/may/01/steel-boss-dismisses-claim-that-sector-needs-new-cumbrian-coalmine

> Claims that a new coalmine in Cumbria will help supply British-made steel and replace Russian imports do not “stack up”, a senior industry figure has warned, as the government prepares to make a final decision over the project.

[[Cumbrian coalmine]]. [[West Cumbria Mining]].

