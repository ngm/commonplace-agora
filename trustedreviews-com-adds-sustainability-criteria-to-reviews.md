# TrustedReviews.com adds sustainability criteria to reviews

URL
: https://www.trustedreviews.com/news/trustedreviews-com-adds-sustainability-criteria-to-reviews-4241141

Seems pretty good.

-   They will ask manufacturers questions about the sustainability of their products.
-   they will look at material usage; supply chains; repairability; and recyclability.

I don't actually know how big Trusted Reviews is.  But it's not small, at least.

> The move will see [[TrustedReviews.com]] request answers on key questions around a product’s [[sustainability]] whenever it is reviewed. The information will then be added to every review making it clear to readers and buyers what steps have been taken to reduce the product’s impact on the environment. 

[[right to repair]]

> It focuses on four key areas: the materials used, the [[supply chain]] making it, its [[repairability]] and recyclability.

<!--quoteend-->

> “Sustainability is a key area not factored into most consumer review sites’ coverage or testing criteria. Seeing it added on Trusted Reviews is a great start to get this key issue more widely acknowledged and inform consumers about the [[environmental impact]] their purchases have on the planet. Given Trusted Reviews’ authority in the product testing space and the conversations we have had about its long-term goals around sustainability, I’m confident the new initiative will help shine a light on this often ignored side of consumer technology,” said Murray.

