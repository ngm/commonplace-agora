# Cost of living crisis

> Without another Covid wave, it is the cost of living crisis and particularly its impact on families already struggling to meet their rent, put food on the table and pay their heating bills that will dominate people’s lives in the next two years.
> 
> &#x2013; [The Observer view on Britain’s growing cost of living crisis | Observer edito&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/06/observer-view-on-britain-cost-of-living-crisis) 

<!--quoteend-->

> The poorest households in the UK could see their cost of living jump by as much as 10% by this autumn if Russia’s invasion of Ukraine leads to a prolonged conflict, the Resolution Foundation thinktank has warned.
> 
> [Cost of living for UK’s poorest could be 10% higher by autumn, thinktank warn&#x2026;](https://www.theguardian.com/business/2022/mar/14/cost-of-living-rise-uk-poorest-households-resolution-foundation)

<!--quoteend-->

> The crisis impacting working people isn’t a result of blind economic forces — it is the result of a class war waged from above
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> The year 2022 will, in all likelihood, be the toughest one for working people since the immediate aftermath of the 2008 Financial Crash
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> In its response to rising inflation, The Bank of England was quick to lay the blame on workers, calling for wage restraint. But this crisis hasn’t been caused by an increase in demand. The proximate cause is a crisis in supply, chiefly of energy but also in the structure of the supply chain itself, against which the only protection that workers have is the fight for higher wages
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> In the end, prices are not set by workers or by consumers, but by firms. This is one of the entitlements afforded to capital by the private ownership of the economy. So, when those prices go up, a decision is being made. In the case of Tesco, for example, Britain’s largest retailer, their decision is to increase the cost of food while sitting on an annual operating profit of £2.6 billion
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Much the same is the case in the energy sector, where the so-called Big Six raked in over £1 billion in profits on the eve of a truly astronomical hike in bills for their customers. British Gas alone saw a 44 per cent jump in its profits
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> When a crisis like the one we’re experiencing emerges, there is always a question: who pays for it? If the cost of production rises, will workers and consumers bear the brunt or will profits be squeezed? In an economy dominated by private ownership, this is in reality not much of a question at all. Profits will be protected; everything else is a lower-order concern
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> The only answer to the cost-of-living crisis is fundamental economic change. A government that truly represented the public interest would today be intervening in any number of ways to prevent living standards from dramatically deteriorating
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> This situation puts a particular focus on extra-parliamentary politics, from trade unions to social movements and campaigns. These are now the only ways that pressure felt in working-class communities across the country can be expressed in an organised form. And, in turn, the only way that pressure can be directed against the business interests profiting from this crisis and the politicians doing their bidding
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> This means that government by consent is fading and, in its place, government by force is becoming more commonplace. As the cost-of-living crisis deepens this year, and millions of people who were already struggling realise that their living standards will decline even further, the anger will be difficult for the government to contain. And so, they are providing themselves with the tools necessary for a crackdown
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Like steam, even great quantities of anger can soon dissipate. It is only when that steam is gathered in a structure that the pistons move and the wheels of history can begin to turn
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> We must set out our alternative of wage rises, price caps, and [[public ownership]], and of a fundamentally different political and economic system which serves a different set of interests
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> A great wave is about to come crashing down on this country, and the only thing which can protect us from it is [[class struggle]]
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> Rising inflation is driving the cost of living crisis, but it isn’t an act of God. It’s the result of policy decisions that favour the rich — and socialists need to have an alternative
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> there is reason to believe that inflation is running at even higher rates for poorer households than it is for wealthier ones
> 
> &#x2013; [[Tribune Winter 2022]]

