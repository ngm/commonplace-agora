# Cheeseless pasta bake

Source: [Cheeseless Pasta Bake - Easy but delicious](https://studentrecipes.com/recipes/pasta/cheeseless-pasta-bake) 


## Ingredients

-   150g dried pasta- penne/fusilli (75g per person)
-   1 tin of chopped tomatoes
-   1 tbsp tomato puree
-   1 chopped deseeded chilli (green or red)
-   1/2 chopped onion (and 1 chopped garlic clove if desired)
-   4 chopped mushrooms
-   1 chopped courgette or carrot (or 1/2 chopped courgette and 1/2 chopped carrot)
-   100g frozen peas
-   1 tbsp mixed herbs
-   Seasoning
-   (1 tin of tuna for non veggies/ fish lovers)


## Method

Serves 2: prep time 10-15 mins.
Cooking time: 45 mins.

1.  Boil water in a pan and add pasta to cook for about 10-15 minutes

2.  Meanwhile preheat oven to 200 degrees and prepare vegetables

3.  Add onion (and garlic if desired) with chopped chilli to a seperate pan and add spray oil

4.  Cook for 3 minutes stirring well

5.  Add remaining ingredients to the onion+chilli- again stir well making sure all the vegetables are coated with the chopped tomatoes

6.  At this point people who like fish could add 1 tin of tuna to the mixture but this step is not necessary!

7.  Leave mixture on lowest heat, cover and simmer until the pasta is cooked

8.  Once pasta is cooked, drain it and add it to the mixture and stir

9.  Transfer pasta mixture into an oven proof dish

10. Cook in oven for 20 mins (if you want the pasta to have a crispy texture on top cook for a further 10 mins)

11. Divide into 2 bowls and season with pepper


## My Notes

This was super easy and tasted really nice.

Would be better with cheese, but fine for when haven't got any, or avoiding dairy.

20 minutes was fine.

I used crushed chilli rather than a fresh chilli.

I added some red pepper pesto to use it up, and it seemed alright.

