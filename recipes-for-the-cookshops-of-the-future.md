# Recipes for the cookshops of the future

-   [In the Cookshops of the Future | Novara Media](https://novaramedia.com/2021/03/05/in-the-cookshops-of-the-future/)

> Marx and Engels declined to write ‘recipes’ for the ‘cook-shops of the future’ and concentrated on a detailed analysis of the capitalist economy. They opposed their own [[scientific socialism]] to the ‘[[utopian socialism]]’ of those who imagined societies of the future, but who failed to base their theories on the movement of existing social forces
> 
> &#x2013; [[Platform socialism]]

