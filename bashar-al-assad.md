# Bashar al-Assad

> Bashar al-Assad succeeded his father as president of Syria in 2000. He was just 34, had studied in London, and some hoped he would open up the autocratic country. Instead, following the [[Arab spring]] uprising he has cracked down harder than ever on the Syrian people. In the decade-long civil war that followed, more than 500,000 people are thought to have died. 
> 
> &#x2013; [[Bashar al-Assad's decade of destruction in Syria]]

