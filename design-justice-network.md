# Design Justice Network

URL
: https://designjustice.org/

[[design justice]]

> The Design Justice Network is a home for people who are committed to embodying and practicing the Design Justice Network Principles.

<!--quoteend-->

> We wield our collective power and experiences to bring forth worlds that are safer, more just, more accessible, and more sustainable. We uplift liberatory experiences, practices, and tools, and critically question the role of design and designers.

<!--quoteend-->

> Rooted in a sense of abundance, possibility, and joy, we provide connection, care, and community for design justice practitioners.

