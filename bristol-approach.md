# Bristol Approach

> Between 2016 and 2017, KWMC and Ideas for Change tested the Bristol Approach, exploring the potential of a [[data commons]] as a tool for social change
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

