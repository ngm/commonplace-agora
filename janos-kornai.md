# János Kornai

> a Hungarian economist, had a hard-nosed but fair theory of actually existing socialism. Indeed, his work achieved the rare feat of garnering respect on both sides of the Iron Curtain. In short, he argued that socialism inevitably led towards a [[shortage economy]]
> 
> [[Half-Earth Socialism]]

