# Slow writing

> In other words, the foraging, summarizing, and linking parts of the process are often separated by weeks.
> 
> -   [Slow-Writing with Wikity | Hapgood](https://hapgood.us/2016/10/20/slow-writing-with-wikity/)

Kind of similar to [[progressive summarisation]].

