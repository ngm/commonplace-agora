# The police

Good bit in [[Future Histories]] about the Marine Police Office, the oldest police force in England.  Set up in cahoots with the merchants, to enforce wage labour paid by time and stamp out the labourers taking stock from the employers.

> “The origins and functions of the police are intimately tied to the management of inequalities of race and class.” 
> 
> — Alex Vitale


## [[Digital urban planning]]

