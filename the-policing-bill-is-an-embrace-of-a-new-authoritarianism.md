# The policing bill is an embrace of a new authoritarianism

The [[Policing bill]] is an embrace of a new [[authoritarianism]].

https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain


## Because

-   [[The policing bill is an attack on some of the most basic democratic rights of citizens]]
-   [[The policing bill increases the risk of peaceful demonstrators being criminalised]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

