# Agroecology is an example of systems thinking

A
: [[claim]]

[[Agroecology]] is an example of [[systems thinking]].

Source: Robert Biel in [[Entropy and the Capitalist System with Robert Biel]].


## Resources

-   [Agroecology: A Systems Approach - resilience](https://www.resilience.org/stories/2019-04-26/agroecology-a-systems-approach/)
-   [Agroecology &amp; system thinking presentation - YouTube](https://www.youtube.com/watch?v=uMJd0kp1V-0)

