# Digital weeding and watering

A bunch of jobs to keep my [[digital garden]] in good health.

Perhaps this kind of tidying up also acts as a kind of [[Spaced repetition]]?  In that it nudges me to come back to nodes I may have otherwise forgotten about.


## Orphans

Pages with no incoming or outgoing links.

I usually only create pages by linking from another page, so these are usually erroneous, historical mishaps, etc, and generally do need fixing in some way.

Note that we ignore incoming links from **this** page.

-   [[Unix philosophy]]
-   [[How I log the data from our Repair Cafe]]
-   [[Smoke tests]]
-   [[rsync.net]]
-   [[Carbon farming]]
-   [[Nowcasting]]
-   [[2020-09-05]]
-   [[mental wellbeing]]
-   [[Project ideas]]
-   [[doubleloop todo]]
-   [[2020-07-28]]
-   [[Extropians]]
-   [[Use web-mode for Laravel templates.]]
-   [[Use org-super-agenda to get a nicer looking agenda.]]
-   [[capture templates]]
-   [[Refiling]]
-   [[Prefer immediate DB update method.]]
-   [[org-roam capture templates]]
-   [[Graph settings]]
-   [[Themes]]
-   [[Tabs (centaur)]]
-   [[mu4e (mail)]]
-   [[IRC (erc)]]
-   [[Tidal]]
-   [[NFT]]
-   [[My biases]]
-   [[Public keys]]
-   [[Climate Vanguard]]
-   [[open blogchains]]
-   [[bolo'bolo]]
-   [[Changing the filename that org-roam gives new notes]]
-   [[Recent Changes]]
-   [[LinkedIn]]
-   [[The Word for World is Forest]]
-   [[Tracks that I have liked at some point in my life, in no particular order]]
-   [[Not the End of the World]]
-   [[Thick time]]
-   [[Updating to Emacs 29 on Linux Mint]]
-   [[My answers to Sophie's questions about digital gardens]]
-   [[Four Thousand Weeks]]
-   [[Dark forest]]
-   [[Cozyweb]]


## Introverts

Pages with no outgoing links.
[Does it matter if a page has no outgoing links?]

I have a lot of these, so I'll just put the top 42 here.

-   [[repair information]]
-   [[Rebecca Solnit]]
-   [[GreenHost]]
-   [[repair prices]]
-   [[Princess Mononoke]]
-   [[Concept map]]
-   [[digital technology]]
-   [[MermaidJS]]
-   [[Massachusetts Institute of Technology]]
-   [[Kurt Vonnegut]]
-   [[water shortage]]
-   [[Anti-austerity movement]]
-   [[rights]]
-   [[environment]]
-   [[Just Stop Oil]]
-   [[Bunny Fonts]]
-   [[NGI Forward]]
-   [[F-Droid]]
-   [[Planning permission]]
-   [[Colombia]]
-   [[Windermere]]
-   [[Consciousness]]
-   [[Viktor Orbán]]
-   [[repression]]
-   [[Fidel Castro]]
-   [[environmental politics]]
-   [[I dislike capitalism]]
-   [[Isabelle Stengers]]
-   [[waste disposal]]
-   [[theory of change]]
-   [[a pattern language for political organisation]]
-   [[income]]
-   [[Cumbria]]
-   [[Catalysts for revolution]]
-   [[Alibaba]]
-   [[Ecology of technology]]
-   [[isms and itys]]
-   [[mySociety]]
-   [[An Introduction to Cybernetics]]
-   [[Power]]
-   [[Pesticides]]
-   [[ufw]]


## Exiles

Pages with no incoming links.
[Does it matter if a page has no incoming links?]

Note that we ignore incoming links from **this** page.

-   [[Problem: Error running timer]]
-   [[2020-05-16]]
-   [[Peter Kropotkin]]
-   [[Ada Lovelace]]
-   [[2020-07-01]]
-   [[Visual thinking]]
-   [[What's wrong with WhatsApp]]
-   [[2020-06-01]]
-   [[digitalization]]
-   [[Unix philosophy]]
-   [[Tredegar Medical Aid Society]]
-   [[How I log the data from our Repair Cafe]]
-   [[The Machine Stops]]
-   [[DMCA 1201]]
-   [[The Means of Connection]]
-   [[Skills]]
-   [[The Ecology of Freedom]]
-   [[How to build a new world in the shell of the old]]
-   [[Climate Justice Charter]]
-   [[critical theory]]
-   [[Police licence]]
-   [[Tags]]
-   [[School]]
-   [[The Cost of Free Shipping]]
-   [[Airline bailouts]]
-   [[vector]]
-   [[cost of living]]
-   [[2020-08-30]]
-   [[Alternatives to GDP]]
-   [[Amazon Ring]]
-   [[Smoke tests]]
-   [[Commons-based Internet]]
-   [[Municipal online housing]]
-   [[Exploitation of workers during coronavirus]]
-   [[July 2020]]
-   [[Houses of Culture]]
-   [[Reclaim]]
-   [[Nationality and Borders Bill]]
-   [[Microblogging]]
-   [[2020-05-20]]
-   [[Why are co-ops relevant in a time of political and economic crisis?]]
-   [[Gross National Happiness]]


## Things without claims

Podcasts, articles, etc, where I haven't yet made a claim.

-   [[Books I've finished but have no claims]]


## Empty pages / stubs

I don't have a good way of querying for this just yet.


## Pages that haven't been edited for a long time

I don't have a good way of querying for this just yet.


## Review

This has been very useful already.

I've found:

-   pages that were using old file: links (e.g. Praxis)
-   duplicate nodes (e.g. Philsophize This).
-   pages that I'm interested in, but just had never had time to write anything about (e.g. climate action)
-   mistakenly created pages

And it has been fun to revisit various older pages in the process.

