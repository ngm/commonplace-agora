# Small web

> Most websites today are built like commercial products by professionals and marketers, optimised to draw the largest audience, generate engagement and 'convert'. But there is also a smaller, less-visible web designed by regular people to simply to share their interests and hobbies with the world. A web that is unpolished, often quirky but often also fun, creative and interesting.
> 
> &#x2013; [Rediscovering the Small Web - Neustadt.fr](https://neustadt.fr/essays/the-small-web/) 

Same as the [[IndieWeb]]?  If not the same, then very adjacent to it.

> The internet historian and artist Olia Lialina sums up this historical negligence aptly when she writes, “. . . we’ve studied the history of hypertext, but not the history of Metallica fan web rings or web rings in general.”
> 
> &#x2013; [[404 Page Not Found]]

