# psychogeography

> [[Psychogeography]], as the term suggests, is the intersection of psychology and geography. It focuses on our psychological experiences of the city, and reveals or illuminates forgotten, discarded, or marginalised aspects of the urban environment. 
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> Both the theory and practice of psychogeography have been around since 1955, when French theorist [[Guy Debord]] coined the term. While it emerged from the [[Situationist]] International movement in France, the practice has far-reaching implications. It’s relevant, for instance, in contemporary Sydney. 
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> Psychogeographers advocate the act of becoming lost in the city. This is done through the [[dérive]], or “drift”.
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> Psychogeography has other uses besides drifting or re-enchanting marginalised spaces. It has a historical use as well. In cases where the landscape has been affected by crime or suffering, psychogeographic readings are especially poignant.
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> Psychogeography thrives as an interrogation of space and history; it compels us to abandon – at least temporarily – our ordinary conceptions of the face value of a location, so that we may question its mercurial history.
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

