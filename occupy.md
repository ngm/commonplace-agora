# Occupy

> Occupy, in particular the original Occupy camp in the belly of the beast that is New York’s financial district, [[Occupy Wall Street]], has become synonymous with this manifestation of radical participatory democratic decision making. 
> 
> &#x2013; [[Anarchist Cybernetics]]

[[[participatory democracy]]]

> You may have heard folks say that Occupy Wall Street was a failure — and if you’re talking about how the movement failed to, say, overthrow capitalism and usher in a new era of eco-socialism devoid of subprime loans and hedge fund managers, then yes, sure, Occupy definitely didn’t accomplish that. But to say the movement was a failure is to overlook so, so much.
> 
> &#x2013; [[Occupy Wall Street: A Decade Later]]

<!--quoteend-->

> the things that Occupy gave us. The networks that were built, the ideas that were shaped around democracy — not just the electoral form of democracy that’s confined to the ballot box, but real, direct democracy — the space that was created to exercise the muscles of solidarity and cooperativism, mutual aid and political organizing, as well as the shifts in public discourse…in the next hour, we’ll look at how the chaotic, fervent explosion that was Occupy Wall Street manifested from the moments after the encampments were cleared to today — ten years later.

