# What Does Class Mean Now?

URL
: https://novaramedia.com/2023/09/15/novara-fm-what-does-class-mean-now/

> We’re going back to basics on Novara FM this autumn with a series about the big one: [[class]]. What does it mean to look at the world through the lens of class in the 2020s, an era of precarious work, rising inequality and elusive social mobility?
> 
> Three classy thinkers join FM to investigate: political researcher Nihal ElAasar, cultural critic Juliet Jacques and writer and Verso editor John Merrick. They begin the series with an introspective session on the meaning of class today, drawing on their very different upbringings in Egypt, the home counties and the north of England.

I didn't really come out of this knowing what class means now.  Other than: there's a difference between the strict Marxist definition of class (based primarily on where you stand regarding ownership of the means of production), and the general definition of social class.  That latter, noone really seems to agree on a definition.

