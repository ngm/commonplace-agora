# Educate, agitate, organise

"the holy trinity of [[socialism]]" ([None of Us Cracked the Corbyn Moment – but, Comrades, We Can All Build On It &#x2026;](https://www.weareplanc.org/blog/none-of-us-cracked-the-corbyn-moment-but-comrades-we-can-all-build-on-it/))


## Educate

Read and disseminate.

![[images/educate.jpg]]
CC BY-NC-SA Autonomous Design Group   https://www.weareadg.org/unions


## Agitate

![[images/agitate.jpg]]
CC BY-NC-SA Autonomous Design Group  https://www.weareadg.org/unions


## Organise

Participate in networks, out of this form communities of praxis.

![[images/organise.jpg]]
CC BY-NC-SA Autonomous Design Group   https://www.weareadg.org/unions

