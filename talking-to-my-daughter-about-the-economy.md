# Talking to My Daughter About the Economy

A
: [[book]]

Subtitle
: A Brief History of Capitalism.

Written by
: [[Yanis Varoufakis]]

A brief history of [[capitalism]].

Good stuff all in all.  A very readable introduction to some economics concepts, from a perspective critical of capitalism.

I like the referencing of Greek myths, literature, sci-fi TV and films as analogies for some of his points.  That and the conversational tone (written for his young daughter) makes it a pleasant read, not dry and dense like a lot of books on this topic.

Maybe not the most rigourous analysis given the style , but that's absolutely fine, it does the job. I think some of the broad sweep of history of agriculture and surplus and economics could possibly need a bit of fact-checking (e.g. see [[The Dawn of Everything]] for a thorough reevaluation of the history of things).

> So, instead of speaking about capitalism, I use the term ‘[[market society]]’. Instead of ‘[[capital]]’ you will find more normal words like ‘[[machinery]]’ and ‘produced means of production’. Why use jargon if we can avoid it?

Talks about the [[Enclosure Movement]].  How [[debt]] has played a big role in the genesis of capitalism and the industrial revolution. [[Money]]. [[Markets]].

Gives an overview of how growth and capitalism leads to [[boom and bust cycle]]s and the concentration of wealth with the few not the many.

Interesting to note he uses the term 'experiential value' seemingly in place of [[use value]].  (He uses [[Exchange-value]] as normal).  Wonder what the rationale is behind that.


## What is to be done?

His brief suggestion of a solution to capitalism is that we need more democracy rather than more markets. Democracy in ownership of the means of production, democratic control of money, and democratic control over how we treat the environment.

> a decent, rational society must democratize not only the management of money and technology, but the management of the planet’s resources and ecosystems as well

<!--quoteend-->

> I concluded that the only solution was to **democratize the process by which monetary decisions are made**. And at the end of the chapter before that, do you remember asking what can be done in the face of the opposition of the small but powerful minority who own all the machines if we are ever to escape becoming the slaves of our creations? **The answer was similar: democratize technology by making all humans the robots’ part owners.**

<!--quoteend-->

> In the final two chapters I hope to convince you that the answer to this most important question is the same whether we are examining **the rise of machines** – as we have in this chapter – or the **lifeblood of the economy, money** – as we shall in the next chapter – or the **lifeline of our species, the environment** – as we shall in the final chapter.

-   Democratize the process by which monetary decisions are made.
-   Democratize technology by making all humans the robots’ part owners. i.e. ownership of the means of production?

What actually is his proposition though? Simply to democratise them all? But how do we actually do that?


## Democratise technology

The chapter 'Haunted Machines'.

> Here is one idea for how to align humanity’s interests with the rise of the machines. Very briefly, this simple, practical measure would be for **a portion of the machines of every company to become the property of everyone** – with the percentage of profits corresponding to that portion flowing into a common fund to be shared equally by all

<!--quoteend-->

> To put it as clearly and plainly as possible: we urgently need as a species a way to make full use of our potential without periodically destroying the livelihoods of great swathes of humanity and ultimately enslaving ourselves to the few. To do this, we must first and foremost redistribute between us the riches that the machines we have created can produce through part-ownership of those machines. I can think of no other way of turning human society from the slave of its creations into their master.

Sounds good. But how?

> What stops us from doing this? The fierce opposition of the tiny but very powerful minority who own the existing machines, land, office blocks and of course the banks. What do we do in the face of their resistance?


## Democratise money

The chapter 'The Dangerous Fantasy of Apolitical Money'.

> To recap: **controlling the money supply** is our only faint hope of charting a course that **avoids the Scylla of bubbles, debt and unsustainable development** on the one hand, and the Charybdis of **deflation and stagnation** on the other.

Control the money supply to avoid:

-   bubbles
-   [[debt]]
-   unsustainable development
-   deflation
-   stagnation

Note that some would go further, and abolish money altogether, rather than simply democratise it. ([[Communism requires the abolition of money and the market system]]).

> But as any such intervention will affect different people – the rich and propertied on the one hand, the poor and powerless on the other – in different ways, it can never be impartial. Having accepted that **money is inescapably political**, there is only one thing we can do to civilize it: democratize it! Give the power to control it to the people on the basis of one person, one vote. It is the only defensible way we know. Of course, to democratize our money we will need to democratize our states first. And this is a tall, tall order. But it may not be impossible.

-   Money is inescapably political.
-   And, as socialists, we therefore want it to be democratised.
-   Which means democratising the state, first.


## Democratise management of the planet's resources and ecosystems

The chapter 'Stupid viruses?'.

> Market societies made their appearance as exchange values were triumphing over experiential values. As we have seen, it was a triumph that produced unimaginable wealth and untold misery and led to mass mechanization, exponentially increasing the quantity of products humanity could fabricate while turning workers and employers alike into the machines’ mechanized servants. It accomplished something else too: it put us, as a species, on a collision course with Earth’s capacity to maintain life.

<!--quoteend-->

> Yes, I know it sounds absurd, but it’s true: according to a variety of measures, the economy benefits from our biosphere’s suffering.

<!--quoteend-->

> Now, in this chapter, I am taking the same line further by arguing that a decent, rational society must democratize not only the management of money and technology, but the management of the planet’s resources and ecosystems as well. 

How do you do that though?  He doesn't really say.

