# Is there an overlap between municipal socialism and libertarian municipalism?

They are both based on a focus on the municipal level.

What's the difference?  Where are the overlaps?

[[Municipal socialism]]
[[Libertarian municipalism]]

I don't know much about either at present.

LM feels more like it is focused on the civic engagement of citizens of a municipality.

MS feels more focused on traditional councils, but these councils having more power, with the state having less power.

