# Spycops

> Donna McLean, who was deceived into a relationship as part of the Spycops scandal, speaks to Tribune about the deliberate nature of abuse by undercover police, the wider web of corruption exposed by recent revelations — and why she feels the public inquiry process offers little justice
> 
> &#x2013; [[Tribune Winter 2022]]

-   [Secrets and lies: untangling the UK 'spy cops' scandal | Undercover police an&#x2026;](https://www.theguardian.com/uk-news/2020/oct/28/secrets-and-lies-untangling-the-uk-spy-cops-scandal)

