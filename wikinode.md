# WikiNode

Hi - this is the WikiNode page for Neil's Noodlemaps.

Here you can find a bit of info about this site, and links to neighbouring nodes.


## Summary

-   [[About this site]]


## Neighbour Nodes

-   [PhilJones:WikiNode](http://thoughtstorms.info/view/WikiNode)
-   [BillSeitz:WikiNode](http://webseitz.fluxent.com/wiki/WikiNode)

