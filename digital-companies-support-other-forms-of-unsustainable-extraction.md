# Digital companies support other forms of unsustainable extraction

> And then there is the pivotal role of digital companies in supporting other forms of unsustainable extraction. Tech giants help corporations explore and exploit new sources of fossil fuels and digitize industrial agriculture. Digital capitalism’s business model revolves around pushing ads to promote mass-consumption, a key driver of the environmental crisis. Meanwhile many of its billionaire executives have a carbon footprint thousands of times higher than average consumers in the Global North.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

Big tech has both direct involvement in emissions, as well as a supporting role of providing technology to other extractors.

