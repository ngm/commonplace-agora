# Inside the Real Repair Shop 7

URL
: https://fixitworkshop.co.uk/2022/04/18/inside-the-real-repair-shop-7/

"So, this month, I thought I’d consider the case against repair. Have I gone quite mad?"

Outlining some reasons as to why repair isn't always feasible - i.e. [[barriers to repair]].

> It’s no coincidence that popular TV series The Repair Shop majors on pre-1980 cherished items. Older items, made from quality materials, conceived before manufacturers built-in precise planned mechanical and electronic obsolescence, usually stand a chance of being repaired by crafts people, as that’s how they might have been made in the first place.
> 
> You see, a child’s toy or home printer manufactured today, on a sophisticated plastic moulding machine with sealed-in electronics will be almost impossible to repair without destroying the outer casing first.

[[No way to open the product]].

> And even if you could get to the faulty battery or printed circuit board, it would be virtually impossible to repair at a reasonable cost, assuming the spares were available.

<!--quoteend-->

> But take a modern washing machine, and parts prices are often not economic to buy, especially when you factor-in an engineer’s time for the repair Its often easier and cheaper to replace the whole thing anyway with next day delivery, just a click away on your phone with interest free credit. And that’s a mighty tempting prospect when the family’s washing is piling up on the floor. 

[[Spare parts too expensive]].

And [[artificial cheapness of new products]], due to 'externalities', labour exploitation, etc.

> [&#x2026;]repair is now the reserve of the reasonably well-healed. [&#x2026;] To bring it back to life, it will need care, parts, experience and (usually lots of) time. All things that must cost money. 

<!--quoteend-->

> Repair is definitely a discretionary purchase now and if you’re a bit brassic, and your microwave oven goes kaput, are you going to spend £100 getting your old machine repaired?  No, you’ll do the sensible thing and buy a new one from Amazon for £40 delivered next day, as you need to feed your family.

Artificial cheapness again.  

> New appliances are sometimes more efficient and perform better.

<!--quoteend-->

> Take a domestic fridge.  Modern ones could use as much as half the energy than those made 30 years ago, and offer more features as standard.

<!--quoteend-->

> Technology never stops marching on, and we can all benefit from replacing some creaking appliances with something up to date sometimes.

<!--quoteend-->

> It’s usually more environmentally beneficial to keep something running as long as possible by spreading the manufacturing and shipping impact over a long functional life. And I don’t know about you, but I dislike the thought that someone else has already booked the death day for something I own!

<!--quoteend-->

> I mean, just by looking at carbon-offsetting alone, consider this:  Replacing a domestic kettle every three years, which has been shipped from China, made with complicated materials and electronics with no hope of repair, is not as environmentally kind as a simpler one that lasts for 9 years.  Sadly, those kettles just don’t exist any more and if they did, I suspect that they would be far too expensive to be a mass-market product

<!--quoteend-->

> But I can leave you with a simple piece of advice; look after your things, buy quality items if you can and consider second-hand at all times to make the Pound (or insert your chosen currency here) in your pocket go further, all very sensible in these uncertain times.

Care.

