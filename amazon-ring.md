# Amazon Ring

> In 2019, Caroline Haskins did a great investigation into what Ring cameras do to communities, and effectively identified how they rely on fear: people need to be scared of their surroundings, and the cameras show them things they didn’t know was happening before, making them even more suspicious.
> 
> She linked Ring to the politics of suburbia, noting it’s “not only a destination of white flight, but a refuge.” People of color are seen as the least trustworthy, fueling racism in neighborhood watch apps. Beyond that, Amazon’s Neighbors is advertised as “[[digital neighborhood watch]]” — eerily similar to Labour’s slogan — but Haskins says it really creates “a [[digital gated community]].”
> 
> &#x2013;  [Amazon is always watching](https://mailchi.mp/techwontsave.us/amazon-is-always-watching) 

