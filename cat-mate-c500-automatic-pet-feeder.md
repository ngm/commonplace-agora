# Cat Mate C500 automatic pet feeder

URL
: [Cat Mate Five-meal Automatic Pet Feeder with Digital Timer (C500)](https://closerpets.co.uk/products/five-meal-automatic-pet-feeder-with-digital-timer-c500-petmate)

It's not the nicest looking thing out there, but it is a bit of a godsend for our cat [[Enid]].  We're able to go out on day trips much more easily without worrying about her.  It's robust enough for her to not destroy it, which we have had in the past with other automatic feeders.

