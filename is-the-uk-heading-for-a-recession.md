# Is the UK heading for a recession?

URL
: https://www.theguardian.com/news/audio/2022/jun/01/is-the-uk-heading-for-a-recession-podcast

[[UK recession 2022]].

> Despite the chancellor’s announcement of a £15bn package for people struggling with the cost of living crisis and energy bills, experts worry that there may still be danger for the economy ahead. Inflation is currently at 9% in the UK, and could peak at 10% later in the year.
> 
> The Guardian’s economics correspondent, Richard Partington, tells Nosheen Iqbal about the impact of high [[inflation]] and low [[growth]] on the economy. Is the government doing enough to stop the country from going into a [[recession]]?

Related to the [[cost of living crisis]] driving up prices which then is reducing presumably the rate at which people spend and then that's having a knock on effect on [[GDP]].

They defined briefly recession: GDP decreasing for two quarters in a row.

It's interesting, I mean, seems kind of taken as read that this is a bad thing. But it would be interesting to explore more, given we kind of know that endless growth and the pursuit of GDP is one of the reasons why we are in climate crisis and we are experiencing so many problems around the world.  Through relentless pursuit of growth.

Is GDP and growth slowing down or shrinking such a bad thing? I'm sure there's plenty of reasons why in real terms it does have many repercussions on people's lives. But it would be interesting to unpick that

Talked about some of the measures from the government, this 400 pounds which every household is getting to help with the cost of energy. (I think it's specifically the cost of energy).  And then 650 pounds for those who need it the most. From everything that I've read, that's not enough.

Even weak as it is, doesn't even have the support of many in the Conservative party. Who see it as some kind of socialist policy.

Some of the money for this is coming from a windfall tax. It was a temporary windfall tax. I don't quite know is temporary about it. I guess they're increasing the rate of tax for a short period of time, decreasing it again afterwards.

Given that energy companies have been experiencing record profits (I think it's record profits, huge profits anyway) then yeah, it seems pretty justifiable to do, right.   It said it was originally a Labour policy which is popular with the public.  The Tories have done it based on that public popularity. 

It's all about the lack of growth in the economy. But is it such a bad thing? I mean, yes, again, probably in real terms in the present days there will be some repercussions.  But perhaps they can be handled in a different way than just saying, oh, we need to get back to growth. There was a bit at the end that said the government has been accused of being out of ideas as to how to reinvigorate the economy. Maybe the left's response should not be 'let's just reinvigorate the economy via growth in a lefty way', but let's change what it means to have a successful economy. Perhaps there's some opportunity for that.

