# Trip 35: The Internet

URL
: https://novaramedia.com/2023/08/06/trip-35-the-internet/

The [[Internet]].

Lots of interesting chat.

Touches a bit on [[Attention economy]], [[The Californian Ideology]], [[Free software]], [[Commons-based peer production]], [[Capitalism]], [[Commons]], [[Linux]].

[[Jeremy Gilbert]] says he suggested a policy for the Corbyn Labour Party that the government would sponsor turning Ubuntu into a proper rival for Windows or MacOS.

