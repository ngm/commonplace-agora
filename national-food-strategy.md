# National Food Strategy

> The independent National Food Strategy, drawn up by Dimbleby, was commissioned in 2019 by the then environment secretary, Michael Gove, and has produced two reports.
> 
> &#x2013; [[Food strategy for England likely to be watered down]]

<!--quoteend-->

> Any government food strategy that fails to deal adequately with both the cost of living and the climate and nature crisis will be woefully inadequate
> 
> &#x2013; [[Food strategy for England likely to be watered down]]

<!--quoteend-->

> Boris Johnson’s new food strategy for England contains virtually no new measures to tackle the soaring cost of food, childhood hunger, obesity or the climate emergency, a leaked version of the white paper shows
> 
> &#x2013; [‘Worse than half-baked’: Johnson’s food strategy fails to tackle cost or clim&#x2026;](https://www.theguardian.com/politics/2022/jun/10/worse-than-half-baked-johnsons-food-strategy-fails-to-tackle-cost-or-climate) 

<!--quoteend-->

> Dimbleby made a number of high-profile suggestions, including the expansion of free school meals, increasing environment and welfare standards in farming, and a 30% reduction in meat and dairy consumption
> 
> &#x2013; [‘Worse than half-baked’: Johnson’s food strategy fails to tackle cost or clim&#x2026;](https://www.theguardian.com/politics/2022/jun/10/worse-than-half-baked-johnsons-food-strategy-fails-to-tackle-cost-or-climate) 

<!--quoteend-->

> the few specific policies chosen by the government include an increase in domestic tomato production, and making it easier for deer stalkers to sell wild venison
> 
> &#x2013; [‘Worse than half-baked’: Johnson’s food strategy fails to tackle cost or clim&#x2026;](https://www.theguardian.com/politics/2022/jun/10/worse-than-half-baked-johnsons-food-strategy-fails-to-tackle-cost-or-climate) 

<!--quoteend-->

> Dimbleby’s report noting that 85% of farmed UK land is used to either grow food for livestock, or to rear meat.  “They have said we need alternative proteins but they have not mentioned the unavoidable truth that the meat consumption in this country is not compatible with a farming system that protects agriculture and sequesters carbon,” he said
> 
> &#x2013; [‘Worse than half-baked’: Johnson’s food strategy fails to tackle cost or clim&#x2026;](https://www.theguardian.com/politics/2022/jun/10/worse-than-half-baked-johnsons-food-strategy-fails-to-tackle-cost-or-climate) 

