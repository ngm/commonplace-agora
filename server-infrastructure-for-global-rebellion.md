# Server Infrastructure for Global Rebellion

URL
: https://media.ccc.de/v/36c3-11008-server_infrastructure_for_global_rebellion

A talk by Julian Oliver on the infrastructure set up used by [[Extinction Rebellion]].  

In this talk Julian will outline his work as sysadmin, systems and security architect for the climate and environmental defense movement Extinction Rebellion. Responsible for 30 server deployments in 11 months, including a community hub spanning dozens of national teams (some of which operate in extremely hostile conditions), he will show why community-owned free and open source infrastructure is mission-critical for the growth, success and safety of global civil disobedience movements.

> Julian will give an overview of his own discoveries, platform choices, successes and mistakes meeting the needs of 5-figure at-risk server memberships, from geo-political and legal challenges, to arrest opsec and uptime resilience in the face of powerful adversaries driving attacks on infrastructure and seized activist devices spanning many countries before and during periods of mass civil disobedience. In particular the talk is a call for all sysadmins, opsec and infosec professionals and enthusiasts to rise up and join the fight for current and future generations of all life.

