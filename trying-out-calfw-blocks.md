# trying out calfw-blocks

Trying out [[calfw-blocks]] as a means to [[Using org-mode for timeblocking]].

On my desktop, added a private spacemacs layer setting up calfw (based on https://github.com/dudelson/spacemacs-calendar-la) then extended it with some calfw-blocks setup.

On mobile:

-   added the calendar app to init.el - this sets up [[calfw]]
-   added [[calfw-blocks]] to packages.el
-   and then some setup in config.el.

<!--listend-->

```emacs-lisp
(package! calfw-blocks :recipe (:host github :repo "ml729/calfw-blocks"))
```

```emacs-lisp
(use-package calfw-blocks
  :config
  (defun ngm/org-day-blocks ()
    (interactive)
    (cfw:open-calendar-buffer
   :contents-sources
   (list
    (cfw:org-create-source "medium purple")
    (cfw:org-create-file-source "Day" "/storage/emulated/0/org/day-2024-03-27.org" "green")
    )
   :view 'block-day)))

```

Should check my version of the spacemacs layer in somewhere. And my doom init and config files.


## Issues

-   does the agenda source not include overdue items?
-   if items A ends at xx:00 and item B starts at xx:00 then they're drawn with an overlap rather than contiguous

