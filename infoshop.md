# infoshop

> Infoshops are places in which people can access anarchist or autonomist ideas. 
> 
> &#x2013; [Infoshop - Wikipedia](https://en.wikipedia.org/wiki/Infoshop) 

I went to 56a Infoshop a couple of times when I lived in London; wish I'd gone more, in retrospect.

