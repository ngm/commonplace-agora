# open access

> Open access (OA) is a set of principles and a range of practices through which research outputs are distributed online, free of cost or other access barriers.
> 
> &#x2013; [Open access - Wikipedia](https://en.wikipedia.org/wiki/Open_access) 

