# Maintek

> MTK and FOX refer to two giant contract manufacturers, Maintek and Foxconn. Remember the top lid that Durrell popped off so easily? That was made by Casetek, which, alongside Maintek, is part of Pegatron, a giant corporation that creates consumer electronics, including the iPhone. You can find Maintek’s factory at the same address as Casetek in Suzhou, China. No. 233, Jinfeng Road is a monumental site employing up to 80,000 workers in peak season. In 2014, Students and Scholars Against Corporate Misbehaviour, a non-government organization based in Hong Kong, detailed abhorrent conditions there, including punishing work spells that can last for 10 weeks at a time, illegal charges for health checks, little protective equipment, and excessive performance-related fines. 
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

