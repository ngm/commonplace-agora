# The movements of the squares

> What these have in common are both their origins –​ in various ways, responses to the financial crisis of 2008, the political crisis of (the absence of) democracy and the connections between these – and many of their methods –​ the occupation of public space, experimenting with forms of direct, participatory democracy and the creation of networks of mutual aid.
> 
> &#x2013; [[Anarchist Cybernetics]]

