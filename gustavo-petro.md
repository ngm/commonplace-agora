# Gustavo Petro

> Last week that changed – and when Gustavo Petro formally takes over in August, [[Colombia]] will have its first leftist leader.

<!--quoteend-->

> the new president is promising to end the decades-long war on drugs, change Colombia’s relationship with the US, and shift the country’s economy away from gas and oil. It’s a tall order, especially as Colombia’s presidents are limited to a single term of four years
> 
> &#x2013; [Can Colombia’s first leftwing president deliver change? – podcast | News | Th&#x2026;](https://www.theguardian.com/news/audio/2022/jun/27/colombia-first-leftwing-president-gustavo-petro-podcast)


## Articles

-   [Colombia’s First Leftist President Won Despite Everything the Establishment T&#x2026;](https://novaramedia.com/2022/06/20/colombias-first-leftist-president-won-despite-everything-the-establishment-threw-at-him/?utm_source=Novara+Media+Contacts&utm_campaign=967cc81fad-EMAIL_CAMPAIGN_2022_06_21_04_49&utm_medium=email&utm_term=0_c9054fa80c-967cc81fad-133696841&mc_cid=967cc81fad&mc_eid=f14e7af0fd)

