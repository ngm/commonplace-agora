# English councils are battling financial ruin

[[English councils]] are battling financial ruin.

> BBC analysis in 2020 showed nine out of 10 major local authorities in England did not have enough cash to cover their spending plans this year, and coronavirus could lead to them going £1.7bn over budget.
> 
> &#x2013; [English councils battling financial ruin - BBC News](https://www.bbc.co.uk/news/uk-politics-55754882)

