# Open Repair Alliance

URL
: https://openrepair.org

Defining a standard for community repair data, i.e. the kind of stuff you log at a [[Repair Cafe]], coordinating the pooling of data collected by lots of different partners, wrangling it all together and releasing it as one big [[Open data]] set of [[open repair data]].

