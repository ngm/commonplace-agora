# Grid balancing

Keeping electricity supply as closely matched to demand as possible.

> National Grid needs to keep generation closely matched to consumer demand across the country at all times, down to the nearest few seconds, to keep the grid stable.
> 
> Too little electricity means people will suffer power shortages or blackouts.
> 
> Too much electricity destabilises the grid because the system frequency starts to rise - generators start to trip if it deviates too far from the standard 50Hz that we’re all used to.
> 
> &#x2013; [The National Grid Balancing Market: What is it and why does it matter?](https://www.ecotricity.co.uk/our-news/2021/the-national-grid-balancing-market-what-is-it-and-why-does-it-matter)

