# graphic novel

I used to read comics (mostly 2000AD I think) when I was a kid.  My brother was into them.

But I never got in to graphic novels back then. I spent more time with prose for whatever reason.

But I am really enjoying graphic novels now. The visual aspect, the thought around framing and visual language.

I would like to enhance my visual thinking and this feels like a pretty decent way of doing it&#x2026;

