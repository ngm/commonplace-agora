# Outlining vs bullet-pointing in org-mode

A great overview from rooster of a distinction you can make between outlining and bullet-pointing in [[org-mode]].  You can see something like workflowy or [[Roam]] as a bullet-pointer.

-   [Plain-text vs Outliner Focused Features Discussion - Development - Org-roam](https://org-roam.discourse.group/t/plain-text-vs-outliner-focused-features-discussion/110/8)

See also [[Outlining vs writing]].

