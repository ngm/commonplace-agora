# Vandana Shiva

> Vandana Shiva was an important leading public intellectual, combining defense of local land rights, indigenous knowledge, feminism, post-caste Hinduism, and other progressive programs characteristic of New India and the Renewal
> 
> &#x2013; [[The Ministry for the Future]]

