# dulongderosnay2020: Digital commons

URL
: https://policyreview.info/concepts/digital-commons

> This article presents the history of the movement of the digital commons, from free software, free culture, and public domain works, to open data and open access to science. It then analyses its foundational dimensions (licensing, authorship, peer production, governance) and finally studies newer forms of the digital commons, urban democratic participation and data commons.

<!--quoteend-->

> They are shared in ways that avoid their enclosure and allow everyone to access and build upon them.

<!--quoteend-->

> The notion of the digital commons lies at the heart of digital rights, the political fight to expand, rather than restrict, access to information, culture and knowledge (Kapczynski &amp; Krikorian, 2010).

<!--quoteend-->

> Unlike tangible commons (such as urban gardens, forests or meadows), the digital commons (such as free software or Wikipedia) are not affected by overuse or material exclusivity. However, their existence can still be threatened by undersupply, inadequate legal frameworks, pollution, lack of quality or findability.

^ I think this misses though that the time, energy and health of the community can be overused/drained.  Does the product of the commons == the commons itself?

> We highlight how fundamental an alternative the commons can be, particularly in relation with current issues of capitalism with data-driven surveillance, platform monopolies and the increasingly authoritarian orientation of even many democracies.

<!--quoteend-->

> Digital commons rely on open licensing rules and we study **legal** models preserving sharing and access, which constitute the originality of the digital commons compared to standard copyright used by firms focusing on exclusivity. We then study **cultural** models, which have an impact on authorship and creativity, leading to original **economic** peer production models, the third pil- lar of commons studied holistically. Last, these three holistic dimensions depend on **governance** by communities, presented as a fourth overarching dimension.

So B&amp;H talk of social life, governance and provisioning of commons.  The authors here are also adding in legal - not sure why that doesn't fit under governance, we shall see.

> These are all necessary to rebalance the relation between individual and collective rights, whereas both singular and collective need to be understood, as [[Donna Haraway]](2016) calls it, “entities-in-assemblages”.

<!--quoteend-->

> The new paradigm of producing informational goods as commons emerged first in the field of software development.

<!--quoteend-->

> As FLOSS (Free, Libre and Open Source Software) projects grew and proliferated, it established the practical example that complex, knowledge-intensive informational resources can be managed as commons in Ostrom’s sense (Schweik &amp; English, 2012) and that these are stable and reliable over long periods of time capable of competing directly with market-based commodity production (Weber, 2004). 

<!--quoteend-->

> While there are many commonalities between digital and the tangible commons, one of the fundamental differences between them is that in the former, the resource is by and large, non-rival. There is no danger of overuse. 

^ I'd probably contest that a little again - there's maybe no danger of overuse, but there's definitely danger of burnout.

> Everyone who adheres to the relevant governing rules, for example, the conditions of use prescribed in a licence, is allowed to use the resource and thus can be regarded as part of the community at large. In other words, **producers and users are not separated**.

interesting, so producers and users are all part of the same commons.

> Conventionally, liberal theory conceived creativity as the capacity of the individual exercised in isolation by an unusually gifted person, the (white male) genius (Woodmansee, 1984). Many cultural tropes, from the writer struggling with the empty page, to the artist secluded in her atelier, and the inventor with his personal “eureka” moment, reflect and popularise this notion. This model of the creative process underlies copyright and justifies to attribute a creative work to a single person and afford him (and only much later, her) sole ownership of the work, which is seen as an “original”, that this, as something new, a beginning without precedence. While this notion has long dominated the cultural field and the public imagination, for complex knowledge-intensive goods this was never seen as adequate. In 1942, Robert K. Merton (1973, p. 273), defined “communism”, understood as “common ownership of goods [as] a[n] … integral element of the scientific ethos”, because “the substantive findings of science are a product of social collaboration and are assigned to the community”.
> 
> Since the late 1960s, postmodern literary theories, using notions such as [[intertextuality]], started to question ideas of individual authorship and reveal the collective dimension of literary work (Woodmansee, 1992). While these theories remained confined to relatively specialised audiences for a long time, they started to resonate with the experience within digital networks (Turkle, 1995) where collaboration and transformation of third party works were technically supported and culturally accepted. The free software movement started out as a cultural revolt in which the encroachment of intellectual property was seen as threatening long-held values of community and cooperation (Stallman, 1985). Within networked culture more implicitly and the commons more explicitly, creativity is understood less as the faculty of an individual genius, and more as a balance between individual contribution and collective enablement (Stalder, 2018). This points to a more comprehensive transformation of subjectivity, away from standard liberal notions starting from, and centering around, the individual—separate from his or her environment—to different configurations that some started to call “networked individualism” through which the collective (the network) and the singular (the individual) are co-constituted (Nyiri, 2005; Rainie &amp; Wellman, 2012). All of this rubs against notions of individual authorship which are deeply rooted in Western countries, both legally and culturally. It indicates the depths of the challenge that the commons poses to the framework of Western modernity.

^ this whole section is a huge 'yes' moment.  should probably break it up but it works as one chunk really&#x2026;

> However, without strong approaches to govern the appropriation from the digital commons, it is not certain that large companies benefiting from it will contribute back. The sharing economy, while initially also working with notions of non-market exchange (for example, couch-surfing used to be a non-commercial community platform (Schöpf, 2015)), has been overtaken by capitalist approaches redefined “sharing” as short-term rental of granular resources (such as a room in an apartment, a taxi ride and so on) and has lost all relation to the commons (Slee, 2015).

