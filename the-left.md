# The Left

> If the Left and the environmentalist movement had undergone a theoretical and organizational revision of Mont Pèlerin proportions following their defeats in the 1980s, then things might not be so bleak now
> 
> [[Half-Earth Socialism]]

[[Inventing the Future]] makes a similar point.

> For too long the Left has been better at critique than creating its own positive proposals
> 
> [[Half-Earth Socialism]]

