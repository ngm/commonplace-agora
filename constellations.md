# constellations

A nice word for [[connecting the dots]] / [[pattern recognition]] I think.  I first saw it [used](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/) by [[Ton]].

> Starting from small crumbs doesn’t work for me as most thoughts are not crumbs but rather like Gestalts. Not that stuff is born from my mind as a fully grown and armed Athena, but **notes, ideas and thoughts are mostly not a single thing but a constellation of notions, examples, existing connections and intuited connections**.
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

<!--quoteend-->

> In those constellations, the connections and relations are a key piece for me to express. In wiki those connections are links, but while still key, they are less tangible, not treated as actual content and not annotated. **Teasing out the crumbs of such a constellation routinely constitutes a lot of overhead I feel, and to me the primary interest is in those small or big constellations, not the crumbs.** 
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

<!--quoteend-->

> The only exception to this is having a way of visualising links between crumbs, based on how wiki pages link to each other, because such **visualisations may point to novel constellations** for me, emerging from the collection and jumble of stuff in the wiki. That I think is powerful.
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

