# xrandr: Configure crtc 0 failed problems

So I was setting up a second machine with an old monitor.  (Old?  From 2011.)

The monitor is a Dell P2412H.  The machine has an Intel HD Graphics 4600 (HSW GT2).  They're connected with a VGA cable.

I had Debian loaded on the box. The problem was that the resolution was 1024 by 768 maximum, despite the monitor going up to 1920 x 1080.

I was sure it used to work, but maybe I imagined it.  Maybe it was a different monitor.

I tried the common stuff you see around, run `cvt` to figure out a modeline, then use xrandr to create and add the mode.

I did:

```shell
cvt 1920 1080
# 1920x1080 59.96 Hz (CVT 2.07M9) hsync: 67.16 kHz; pclk: 173.00 MHz
Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
```

Then

```shell
xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
xrandr --addmode XWAYLAND0 "1920x1080_60.00"
xrandr --output XWAYLAND0 --mode "1920x1080_60.00"
```

But got the error:

> Configure crtc 0 failed

Great.

I had an Ubuntu 21.04 installer lying around, so I installed that and tried again.  Same problem.

Reading around various places I convinced myself it was Wayland.

So I installed [[MX Linux]], which has resisted Wayland so far and I wanted to try out for other reasons anyway.

I tried it all again, except with VGA-1 as the output rather than XWAYLAND0 this time.

```shell
xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
xrandr --addmode VGA-1 "1920x1080_60.00"
xrandr --output VGA-1 --mode "1920x1080_60.00"
```

Same shit, different distro - argh!

So then I search for the manual for the monitor.

https://downloads.dell.com/manuals/all-products/esuprt_electronics/esuprt_display/dell-p2412h_user's%20guide_en-us.pdf

And came across this table of display modes:

![[2022-02-18_21-57-51_screenshot.png]]

The pixel clock is different from what cvt pumps out!  148.5 rather than 173.0.

So I tried

```shell
xrandr --newmode "1920x1080_60.00"  148.5  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
xrandr --addmode VGA-1 "1920x1080_60.00"
xrandr --output VGA-1 --mode "1920x1080_60.00"
```

And it worked, boom!

I wonder if it would have worked on Wayland, too.  Probably.  But that's OK, I like the look of MX Linux for now.

Just need to put it in a script to load that modeline automatically.

Huge faff though - I thought this stuff just worked these days.  I guess it's something specific to this graphics chip.  It's an Intel though, I thought they were pretty well supported.  And the monitor is Dell.  Not some weird generic brand.  Hey ho.


## Resources

-   [xorg - Intel® HD Graphics 4600 (HSW GT2) 1920x1080 resolution not work - Ask &#x2026;](https://askubuntu.com/questions/1356226/intel-hd-graphics-4600-hsw-gt2-1920x1080-resolution-not-work)
-   [multiple monitors - Can't set 2560x1440 resolution on Ubuntu 20.04 with Intel&#x2026;](https://askubuntu.com/questions/1294846/cant-set-2560x1440-resolution-on-ubuntu-20-04-with-intel-graphics-card)
-   [display resolution - Ubuntu 21.04 - How to fix xrandr "X Error of failed requ&#x2026;](https://askubuntu.com/questions/1365626/ubuntu-21-04-how-to-fix-xrandr-x-error-of-failed-request-badvalue-integer-p)

