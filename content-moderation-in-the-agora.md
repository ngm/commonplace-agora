# Content moderation in the Agora



## <span class="timestamp-wrapper"><span class="timestamp">[2021-12-01 Wed]  </span></span>  [chat](https://matrix.to/#/!WhilafaLxfJNoigHCj:matrix.org/$4zQFd30BPeX-E5M9b2JA5AeBzdMNKw1ecPFtUagwJLI)

> doubleloop
> 
> This is interesting, kind of a 'liquid moderation' https://toot.cat/@zkat/107368437167225213
> Kat Marchán 🐈 - toot.cat
> 
> Have we thought about content moderation on anagora?
> 
> e.g. what happens if someone posts something racist, pornographic etc on a node.
> 
> flancian
> 
> I like liquid moderation as an approach I think.
> 
> hpec by leah houston, who I met in a call back in October, is using a similar approach IIRC.
> 
> within the Agora, this brings me back to the 'browse as' idea a bit &#x2013; where we could make our Agora preferences public essentially, and let people use our defaults if they want.
> 
> ranking/filtering seems like one of the personal settings that could be shared this way.
> 
> also potentially this handles one of the possible objections to this approach (the filter bubbles). if I can easily 'put on' someone else's filter, perhaps that's not as big a concern (unsure).
> 
> perhaps essentially what we'd end up is something like an inheritance/composition graph of ranking and filtering settings.
> 
> implementation wise, you could imagine everybody inheriting by default from the policies of the system account (@agora in anagora.org) &#x2013; all of those explicit 
> 
> and the community voting to make particular policies the default

