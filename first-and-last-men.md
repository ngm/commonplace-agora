# First and Last Men

> opposite approach to dystopia can be found in the Liverpudlian socialist [[Olaf Stapledon]]’s still incredible 1930 First and Last Men, a boundlessly optimistic, Hegelian account of human evolution
> 
> &#x2013; [[Tribune Winter 2022]]

