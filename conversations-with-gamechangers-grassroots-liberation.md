# Conversations with Gamechangers: Grassroots Liberation

(part of the [[Conversations with Gamechangers]] webinar series.)


## Organised by SEA ([[Solidarity Economy Association]])

-   small coop based in UK
-   multiple small projects for solidarity economy from below
-   SE
    -   being done all over the world (although maybe not called SE)
-   SEA: sharing tools tips and solidarity from around the world


## [[Grassroots Liberation]]


### Speakers

-   Waringa Wahome
    -   social justice and human rights lawyer
-   Brayan Mathenge  - writer on politics
-   Gacheke Gachihi
-   Kinuthia Ndung'u
-   all members of differnt social justice centres, involved in grassroots liberation, part of young communist league kenya

-   music played at the beginning, tribute to the leader of the Mozambique people ([[Eduardo Mondlane]]?)


### [[Mathare Social Justice Centre]]

-   economic crisis in east africa
-   mathare social justice centre started in 2015
    -   collective political power and economy from below
    -   documenting human rights violations
    -   fight for the right to organise
    -   in the face of extrajudicial killing and torture
-   inspired by [[democratic confederalism]] of [[Rojava]]
-   the centres have around 30 membres
-   campaigns: rights to water, extrajudicial killing, ecological justice


### Genesis

-   how are the conditions historically changing
    -   colonialism to neoliberalism
    -   spread of hopelessness in [[Kenya's informal settlements]]
    -   broken healthcare, education systems
    -   high levels of unemployment
    -   police brutality, enforced disappearances, extrajudicial killings
-   activities engaged in by the SJCs:
    -   monitoring these things, stepping up to be a vanguard of the community
    -   organised both legally and when legal means fail taken to mass actions in the streets
    -   organising ecological justice hubs, parks in local neighbourhoods
    -   building a popular power within local neighbourhoods
-   Ecological Justice hubs
    -   people's park
    -   clean up garbage and plant trees
        -   not just for the park
        -   but in memory of those affected by state violence, police brutality and killings
    -   Kenya has a history of police state and violence
    -   former british colony that has remained violent
    -   sustainable economy
    -   in urban areas Ecological Justice is pressing
        -   growth
        -   people trying to make profits from housing
        -   open sewers, causing myriad of chronic conditions
        -   in shanties (informal settlements) lots of people suffering from cancer and similar
        -   also a lot of land grabbing, minimal green spaces
            -   air is polluted
            -   can't drink the water that is in the rivers
        -   social justice centres
            -   organised in different ways in different areas
            -   own means of sustenance
            -   home where people can communicate
        -   two struggles
            -   first, building a civic space for community organising
            -   second, building a sustainable basis for these
            -   these spaces are sites of struggle


### Informal settlements

-   [[Kenya's informal settlements]] - what is the life like there?
    -   history, problems, typical lifes
    -   poverty is violence, poverty is crime, drugs
    -   70% of people live in settlements
    -   no water, housing
    -   crises of capitalism
    -   police stop people from organising against poverty and hopelessness
        -   SJC are to give a space to start organising

-   protest marches every year


### Political education

-   international solidarity is critical to send message to gov to stop cleansing and criminalisation of the youth
-   hard to do community organising where there is poverty and hopelessness
-   political education is important
-   ecological justice campaign has helped challenging for a democratic space to organise

-   political education
    -   important to understand our history
    -   to know where we are going
    -   looking at history, all struggles are political questions
    -   politicisation of human rights
    -   in kenya, HR has been suspended
    -   borrow from history, such as Che Guevara
        -   first duty of revolutionary is to be educated
    
    -   very important in understanding neoliberalism
    -   students of walter rodney: revolutionary must understand the system
    -   different ideological factions with doing opinions
        -   standardised political programme
    
    -   hopelessness in informal settlements (because of poverty)
    -   wretched of the earth are detached
    -   PE reawakes the wretched of the earth
    -   to expose systemic forms of oppression
        -   taking up sites of struggle
    -   e.g. to help connecting water shortages to political organisation
    -   create a syllabus of political thinkers and political writers
        -   network of political thinkers
        -   to organise around the same issues, and conduct education

-   jeff miley
    -   lives in england, political sociology
        -   kurdish freedom movement in 2014
        -   very involved in that since then, 2018 involved in dialogue with people in mathare social justice centre
        -   affinity between kurdish movement and movement in nairobi
        -   beyond NGOs and the nation state
    -   grassroots liberation was born out of internationalist spirit
    -   trying to do things at grassroots
    -   organised a series of seminars done in the communities themselves
        -   rasta resistance
        -   women's lib in the 21st century (from kurdish freedom movement)
        -   social ecology as a revolutionary paradigm
        -   legacy of walter rodney - he saw need for grassroots education
            -   not political speeches, but go to grassroots and engage
    -   arusha declaration for the 21st century


### Intergenerational movement?

-   focus on youth, intergenerational?  how is it structured?
    -   bring together different movements

-   emancipation from NGOism
-   majority of african population is young people in general
-   borrowed heavily from democratic confedarlism
    -   three line
        -   ecological justice (youth, young people)
        -   women's rights
        -   democratic confederalism
-   rasta population has a lot of similarities with the kurdish people


### What was the existing way of organising?

-   organising cooperatives around food and rural organising
-   organising around water
-   people were doing self-help but began organising around struggle
-   borrowing from south africa that organises around the housing question
-   existing resilience of the people
-   preexisting conditions
    -   used to have individuals taking charge in the community
    -   any time the community involved itself it was as a spontaneous reaction
        -   that was not sustained organising
        -   political education to help sustain the organising
    -   individuals who defended the community might disappear
        -   but if it's as a whole community, then it's harder to take down
-   the main road that runs through mathare
    -   mau mau road
    -   preexisting tradition of land and freedom movement
    -   political consciousness in the ghettos
        -   political history that was betrayed by post colonial activities
        -   referenced [[Dedan Kimathi]]


### NGOism

-   recent form of colonialism is NGOism
    -   international development, NGOism
    -   how have they challenged NGOism?
    -   last 30 years in neoliberal economy
        -   politiclisation of social movements by NGOs
        -   has affected many people
        -   similar to as in latin america
    -   organic intellectual networks is putting together a book on ngo discourse, role of ngos in east africa
        -   community organisation has to challenge NGOism every day
    -   NGOs don't ask fundamental question of why people are poor!
        -   never look at political economy
-   NGO activities limit their struggle
-   neoliberalism is not necessariliy a tactic that people understand, this is part of the education work
-   NGOs fragment different political struggles in communities
-   the communities become depolitised
    -   political education helps fight back against this
-   fundmental question within GLs circles
-   NGOism is very deeply rooted
-   NGOs were born from the belly of neoliberalism and free market economy
    -   very tied to neoliberal system
    -   move from dependency to dignity
    -   a self-determined structure
    -   some progressive NGOs have had a positive influence or positive contribution (but they are still a result of neoliberal economy)
    -   jeff: from an internationalist perspective
        -   even going in with the grassroots, it's hard to avoid a neocolonialist perspective
        -   distinguish between NGOisation and leveraging our privilege
        -   check our privelege and leverage our privilege
        -   e.g. speak out about police violence, can be very dangerous for those threatened by it, leverage privilege to speak out about it
        -   distinguish internationalism from neocolonialism


### Economic aspect


### Differences between rural struggles and urban struggles

-   [missed a bit]
-   as capitalism reinvents itself, the rural workers are experiencing similar struggles urban struggles as well, so there are overlaps


### international solidarity

-   sharing of their documents
-   online fundraising that can be shared with us by jeff
-   sharing about the crisis of capitalism
-   creating awareness across the globe
-   connect struggles - collective international campaigns


### power relations between those with abilities and those with needs

-   philosophical question
-   it is inherent in the nature of capitalism that ther eis an imbalance
-   there has long been  a class difference
-   those who are privilege must leverage that
-   jeff:
    -   fanon says that political education is different from coming in with speeches.  help to be the midwife in the birth of critical thinking.
    -   paulo freire - pedagogy of the oppressed
    -   these webinars are one way of attempting to organise political education different


### what is the situation for women?

-   primary question is class struggle
-   particular issues that address the women question
-   patriarchy is prominent in capitalism
    -   something to organise around
    -   advocacy from women in SJC
    -   women in SJCs are organising wmen in informal settings to politicise them
-   NGOs and red carpet feminism
-   glass ceiling and rock ceilings

Do you have any interactions with local government?  Are they supportive or antagonistic?

(my computer is also struggling with zoom so I can't ask this personally, sorry!)


### social justice centres

-   even if people don't necessarily belief in the political theory
-   people can engage with questions of housing, poverty, police violence, etc
-   once reasoning about immediate needs
-   history of kenya: dictatorship, crisis of economy
-   social justice was a proposition from below
-   why do we have an economy that is inherently violent?
-   21 SJCs in Nairobi
    -   meet every 2 weeks (or twice a week?)
    -   building a mass urban movement
    -   organise forces against state violence
    -   SJCs are found in informal settlements, where it is the less privileged and majority of poor people


### closing remarks

-   learn from each other and inspire international solidarity as the crisis of capitalism continues

