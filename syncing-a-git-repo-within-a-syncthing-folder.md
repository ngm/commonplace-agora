# Syncing a git repo within a syncthing folder

I want to:

-   have my source files synced between my laptop and phone
-   be able to commit to the remote git repo from my phone (as there is a remote build process triggered from there)


## How I could do it?


### (1) sync files between devices with syncthing

-   (a) with git folder not synced - push/pull done with git
    -   no sync issues in git folder
    -   when you push from one device, then pull from another, files have already been synced by syncthing so git sees this as overwrite of local changes
-   (b) with git folder synced
    -   git status is then synced
    -   git commit can be made from phone when needed
    -   possible sync conflicts in git folder - bad news
    -   because this is an org-roam git repo, git folder is huge and with many files - slow initial sync


### (2) just sync files with git

-   maybe this is just simplest to be honest?
-   have to always remember to git commit, otherwise will have to resolve conflicts in git merge at a later date


## Thoughts

Probably either 1(b) or 2 is best.

2 is safest, with some minor inconvenience.
1(b) is most convenient, with possible corruption of git repo (not good).

I'll go with 2.  I'm pretty good with git commit push pull habit.  So that means actually pausing the syncthing sync for now then&#x2026;

See how I get on with it.  Then maybe try 1(b).

This was useful regardless: https://github.com/orgs/community/discussions/54617

