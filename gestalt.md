# Gestalt

"Gestalt psychologists emphasized that organisms perceive entire patterns or configurations, not merely individual components."  

[[Ton]] has [[mentioned this]] as something perhaps more conducive to building new ideas that the smallest crumbs approach of TiddlyWiki / Zettelkasten.

