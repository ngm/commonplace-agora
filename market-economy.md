# Market economy

[[Markets]].

> Market Economies are systems of exchange based on buying and selling things with money. A market exchange is specific, quantifiable, direct, immediate, founded upon an explicit agreement. As in, this croissant costs £1 right now.
> 
> &#x2013; [The Gift Economy](https://maggieappleton.com/gift-economy) 

