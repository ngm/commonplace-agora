# carbon coin

> carbon coin. This to be a digital currency, disbursed on proof of carbon sequestration to provide carrot as well as stick, thus enticing loose global capital into virtuous actions on carbon burn reduction. Making an effective carrot of this sort would work best if the central banks backed it, or created it. A new influx of fiat money, paid into the world to reward biosphere-sustaining actions
> 
> &#x2013; [[The Ministry for the Future]]

<!--quoteend-->

> carbon coin. Noted that some environmental economists now discussing the Chen plan and its ramifications, as an aspect of commons theory and sustainability theory
> 
> &#x2013; [[The Ministry for the Future]]

<!--quoteend-->

> The Chen papers sometimes call it CQE, carbon quantitative easing.
> 
> &#x2013; [[The Ministry for the Future]]

