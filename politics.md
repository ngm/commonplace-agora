# Politics

![[2020-07-19_13-26-49_screenshot.png]]

I would describe myself as an [[anti-capitalist]], sitting somewhere on the spectrum of [[revolutionary socialism]].  I read plenty about [[anarchism]] and [[Marxism]], but not too sure yet where I'd place myself.  Somewhere in between the [[horizontal and the vertical]].  Interested in but not very knowledgeable about [[autonomism]] of late. [[I like solarpunk]].

I like the [[social and solidarity economy]], [[cooperatives]] and [[I like commons]].

Also into the [[politics of technology]].


## [[Political theory]]


## [[Political history]]


## [[Praxis]]


## [[Global politics]]


## [[UK politics]]


## [[European Union]]

