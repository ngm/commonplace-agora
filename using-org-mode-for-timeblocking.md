# Using org-mode for timeblocking

I used to use [[Goalist]] for [[timeblocking]] / [[hyperscheduling]].

There's lot of unfortunate '10x your productivity' articles around the practice, but I just found it very useful to keep on top of things and for my peace of mind.

Goalist was really great in terms of functionality, but I got annoyed partly that it was proprietary, but mostly that I couldn't sync it and make use of it anywhere else other than on my phone.

So - I'm now trying to use org-mode for timeblocking.  I've had one attempt at this before, half-building my own Quasar app that works off an org-mode file (https://gitlab.com/ngm/org-day-plan).  Never finished it though.

Recently I'm [[trying out org-timeblock]] and [[trying out calfw-blocks]].  There's also org-hyperscheduler which I might look at if the others don't pan out.

To be continued.

