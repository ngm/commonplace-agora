# types of systems thinking

Types of [[systems thinking]].

Hard, soft and critical.

Hard: 'think about systems' as real entities.
Soft and critical: systems are used as epistemological constructs.

Check out Systems Approaches to Making Change: A Practical Guide for more info.

