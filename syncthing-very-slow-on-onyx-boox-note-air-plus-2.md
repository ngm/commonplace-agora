# syncthing very slow on Onyx Boox Note Air Plus 2

I've installed [[syncthing]] on my [[Boox]] Note Air Plus 2, which is fantastic.  A massive perk of it being built on top of Android.

However, syncthing is horrendously slow.  Like bytes per second, if anything.

Give that under the hood it's a reasonably well-specced Android tablet, I'm not sure what the issue might be.

Need to have a bit of a read around.

On my syncthing network I have a phone, a tablet, my laptop, and then a remote server.

So, one odd thing was that syncthing on the boox was only connecting to syncthing on the android phone via the relay connection type. This is not good, because a relay connection can be much slower.  It goes via a remote server somewhere, not device to device.

A couple of posts as to why this might be happening on Android:

-   https://forum.syncthing.net/t/syncthing-on-android-seems-to-only-work-for-me-via-relay-no-lan-on-devices-with-newer-android/18054
-   https://github.com/syncthing/syncthing-android/issues/1628

Interestingly, they say: "But this should only affect the Android-to-Android usecase." which is true for me.  So that is probably the issue there.

I was able to change the connection to TCP LAN by amending the address on the Boox device on syncthing on the phone to it's direct IP address.  This is not currently static though, so I might have to reserve it via the router.

However, what was in fact the super slow sync for me, was from my laptop to the Boox, on a folder that doesn't exist on the Android phone at all (I don't sync it to the phone).  So that must be a different issue&#x2026;

