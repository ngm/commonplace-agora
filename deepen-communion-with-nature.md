# Deepen Communion with Nature

A pattern of commoning.  Part of the sphere [[The Social Life of Commoning]].

> People involved in [[agroecology]], [[permaculture]], [[community forests]], and [[traditional irrigation]] become closely attuned to the rhythms of natural systems and the subtle indicators of their health or endangerment.
> 
> &#x2013; [[Free, Fair and Alive]]

Wondering how this might apply to [[digital commons]].

Some links of interest:

-   [COMPOST Issue 02: Growth Through Replication by Liaizon Wakest](https://two.compost.digital/growth-through-replication/)

