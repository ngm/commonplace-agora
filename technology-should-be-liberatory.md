# Technology should be liberatory

I believe that technology should only be used for socially beneficial aims. [[Technology does not guarantee progress]].  To be liberatory, technology should save time and labour without having damaging 'externalities'.  

What is [[Liberatory technology]]?

Some uses of technology that I think are liberatory in goal:

-   [[guerrilla technology]]
-   [[Project Cybersyn]]
-   [[Digital public assets]]
-   Humanitarian tech
    -   Glia
        -   [Tarek Loubani on 3D-Printing in Gaza](https://logicmag.io/bodies/tarek-loubani-on-3d-printing-in-gaza/)
        -   [Restart Radio: 3D printing medical devices in Gaza](https://therestartproject.org/podcast/3d-printed-medical-devices-gaza/)
    -   [[Coronavirus and 3D printing]]
-   [[Distributed Cooperative Organisations]]
-   [[IndieWeb]]

How is it different from 'tech for good'?  I think 'liberatory technology' has a more defined political agenda.  Tech for good is a bit vague.  This specific phrase comes from Murray Bookchin, I think.  As a general concept it probably goes back further.

Could [[3rd and 4th industrial revolutions]] be liberatory?


## [[Radical technology must be accompanied by radical politics]]


## Useful links

-   [Communalism Pamplet](https://www.communalismpamphlet.net/index.html#liberatory-technology)
-   [Towards a Liberatory Technology](https://theanarchistlibrary.org/library/lewis-herber-murray-bookchin-towards-a-liberatory-technology), Murray Bookchin


## Related

-   copyfarleft
-   [[appropriate technology]]
-   small tech


#### [[Value flows]]


#### Should behavioural analytics be used in the fediverse?

-   https://social.coop/@mike_hales/102954515608206585
-   Emergent patterns could be interesting. Although needs more unpacking. Instinctive push back to mass behavioural analysis, so need to be sure of the benefits. It would have to be some kind of opt-in, hoovering of data exhaust is one reason people shun the big networks.
-   Perhaps an element of temporality can be introduced. Avoid real time, but see patterns over the last month, year.
-   There at emergent phenomena, like protocol updates etc. These bubble up from somewhere and coalesce without quantitative analysis.
-   Who would collect and analyse the data?  Squinting a bit Feels a bit like anarchist assemblies vs a party structure. Could be a data commons.

