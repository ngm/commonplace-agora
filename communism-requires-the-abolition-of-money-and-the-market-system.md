# Communism requires the abolition of money and the market system

A
: [[claim]]

[[Communism]] requires the abolition of [[money]] and the [[market system]].

First saw this explicitly stated in [[Forest and Factory]].

They also discuss how this is a point of contention.

> Since money and markets both predate capitalism, the question of whether they can serve any purpose within a communist society is often a contentious one.
> 
> &#x2013; [[Forest and Factory]]


## Because

-   [[Any society in which people must rely on money and markets for the essentials of life is not a communist society]].
-   [[Attempting to limit markets to just "non-essential" sectors is risky]]


## Counterclaims

> so long as money and markets are confined to "non-essentials" (or, more strictly "frivolous" or "luxury" goods), then they can be allowed to play some role.

