# Guardians of the Galaxy

A
: [[film]]

A lot of fun.

If I was to look for some themes:

-   friendship and (/versus?) family
-   how you can do good deeds even if you're flawed or have a troubled history.

