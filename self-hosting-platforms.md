# Self-hosting platforms

-   Sandstorm
-   [[Cloudron]]
-   [[Yunohost]]
-   dplatform
-   freedombone
-   eclips.is

Main apps I'm interested in for personal hosting:

-   radicale
-   [[syncthing]]
-   something for git repos

