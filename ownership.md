# Ownership

> It is common for infrastructure to be treated under one legal regime while the fruits produced via that infra- structure are treated under another one. Ownership rights for a building can be separated from ownership of land, for example, and ownership of website content is considered separate from ownership of the Wi-Fi net- work and telephone lines upon which the content flows.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

<!--quoteend-->

> To put it bluntly: there is no logical or compelling reason that those who extract oil or minerals should necessarily have the right to own it. That right is simply a political construct that has been sanctified by law.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

<!--quoteend-->

> The [[state]] approaches the [[ocean]] as a nonliving resource. As such it can be divided up into quantified, bounded units and exploited with an abstract market logic. Oil extraction is perfectly logical to the New Zealand state, whose legal system is constructed to privilege such activity. By contrast, the [[Maori]] see the ocean as a living being that has intense, intergenerational bonds with the Maori people. The ocean is imbued with mana, ancestral power, that must be honored with spiritual rituals and customary practices. (If this sounds irrational to you, consider this: such a worldview has worked remarkably well to protect both the oceans and human societies.)
> 
> &#x2013; [[Free, Fair and Alive]]

