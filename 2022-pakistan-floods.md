# 2022 Pakistan floods

2022 [[Pakistan]] floods.

> Pakistan’s government has appealed for international help to tackle a flooding emergency that has killed more than 1,000 people and threatens to leave a third of the country – an area roughly the size of Britain – underwater.
> 
> &#x2013; [Pakistan floods: plea for help amid fears monsoon could put a third of countr&#x2026;](https://www.theguardian.com/world/2022/aug/29/pakistan-floods-plea-for-help-amid-fears-monsoon-could-put-a-third-of-country-underwater) 

<!--quoteend-->

> More than 1,100 people have been killed, one-third of the country is under water, millions of acres of crops have been wiped out, and nearly 33 million people in one of the world’s most populous nations have so far been affected. The rains continue, and the numbers are sure to rise.
> 
> &#x2013; Down to Earth newsletter

<!--quoteend-->

> Yet this year’s flooding, the worst in the county’s history, was entirely predictable and foreseen. Pakistan’s central and provincial governments, its scientists and diplomats, non-government groups and others have all been shouting for years in summits and forums that the country is peculiarly vulnerable to global heating and likely to be hit hard again and again.
> 
> &#x2013; Down to Earth newsletter

