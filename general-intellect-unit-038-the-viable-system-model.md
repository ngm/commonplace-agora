# General Intellect Unit 038 - The Viable System Model

-   http://generalintellectunit.net/e/038-the-viable-system-model/

Brilliant podcast from [[General Intellect Unit]] on the [[Viable system model]].

Some notes I took as I was listening:

-   Stafford Beer got to some of his ideas via influence from his time in India

-   You could probably get to the ideas via multiple routes though, e.g. via [[Deleuze &amp; Guattari]].

-   VSM can gives you both autonomy AND cohesion
    
    -   And help avoid the tyranny of structurelessnes
    
    <!--listend-->
    
    -   Jon Walker who writes [The VSM Guide](https://www.esrad.org.uk/resources/vsmg_3/screen.php?page=home) was applying VSM to coops in the UK in the 80s (including [[Suma]])

-   VSM provides a framework for how organisation in communism might work, once the state has withered away

-   VSM can help identity what is the ACTUAL purpose of an organisation, often different to what the STATED purpose of the organisation might be.  (Example: e.g. a group states their purpose is to overthrow capitalism, but empirically their actual purpose is to sit around drinking coffee.)

-   In VSM management becomes a function of the overall system, not an individual role attainable only by those with a class privelege.

-   We should think of VSM as an explicitly socialist technology.

