# Socialist economy

> What would a socialist economy look like? The answers to this question vary, but most of them involve [[planning]]
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> A socialist economy, by contrast, would be “consciously regulated… in accordance with a settled plan,” to borrow a line from Marx.
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> One camp has placed particular emphasis on computers. These “digital socialists” see computers as the key to running a [[planned economy]]. Their focus is on algorithms: they want to design software that can take in information on consumer preferences and industrial production capacities—like a gigantic sieve feeding into a data grinder—and output the optimal allocations of resources
> 
> &#x2013; [[How to Make a Pencil]]

