# formal subsumption

> In formal subsumption, independent craft laborers, such as the weavers who made up the Luddite rebellions, work for capitalists, who own the means of production. However, control over the labor process is delegated to the workers, who carry on as they had when they themselves owned their tools. “Technologically speaking,” he writes, “the labour process goes on as before, with the proviso that it is now subordinated to capital
> 
> &#x2013; [[Breaking Things at Work]]

