# The next UK election will be decided in the north of England

> The next election, like the last, will be won in the north of England. While the south is steadily becoming less Conservative over time, there is no blue wall waiting to fall across the home counties in two years’ time.  "But there are dozens of traditional Labour seats in the north that could yet switch
> 
> &#x2013; [Next general election will be decided in north of England, says thinktank | P&#x2026;](https://www.theguardian.com/politics/2022/feb/27/next-general-election-will-be-decided-in-north-of-england-says-thinktank)


## Because

-   there are dozens of traditional Labour seats in the north that could switch
-   Labour could regain 31 seats in the north, Midlands and north Wales if 2019 Conservative voters switched back to their preferred party
-   only three southern seats would change hands


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

