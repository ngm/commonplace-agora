# Climate & Revolution: How do we transition from catastrophe?

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/climate-revolution-how-do-we-transition-from-catastrophe

Featuring
: [[Kai Heron]] / [[Jodi Dean]]

Nice interview.  I like Heron and Dean.  On [[revolutionary transition]] and the ideas from [[Leninism]] (though not necessarily the [[Bolsheviks]] / [[Russian Revolution]]) as a means to work towards that.

-   ~00:05:39  Some kind of revolution/transition is going to happen regardless - is happening already to some degree.  So we should be on top of the direction that it takes.  (Sounds similar to [[Climate Leviathan]] here?)
-   Need for party, criticisms of Occupy, etc.
-   ~00:16:25  Positives of COP -  they're talking about transition, and at least theiyre working at the global scale.
-   Rupture vs incrementalism - why we don't necessarily need to work within the bounds of existing structures (e.g. the existing Labour Party).
-   ~00:27:25  Leninism contributes towards the idea of revolutionary non-linearity.
-   ~00:29:45  Leninism is thinking at the right scale. And it thinks about transition.
-   ~00:43:34  Direct action is a tactic not a strategy - e.g. XR use it well as a tactic - but to support a flawed strategy.

Advice: join a union.  Support [[Don't Pay UK]].  Get involved in organising.

