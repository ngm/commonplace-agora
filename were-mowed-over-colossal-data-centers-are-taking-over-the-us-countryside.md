# 'We're mowed over': colossal data centers are taking over the US countryside

URL
: https://www.theguardian.com/us-news/2023/jun/05/virginia-historic-preservation-data-center-development

Lots of [[data centres]] being built in Virginia, US.  In the article, two sides are pitched as those wanting to preserve the history of the are, and those who welcome the stimulus to the local economy.  There's no mention though of why so many new data centres are being built.

