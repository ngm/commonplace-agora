# Viktor Glushkov

> the founding father of information technology in the Soviet Union and one of the founding fathers of [[Soviet cybernetics]].
> 
> &#x2013; [Victor Glushkov - Wikipedia](https://en.wikipedia.org/wiki/Victor_Glushkov)

