# Right to Repair in the UK

> **the UK produces more electronic waste per person than anywhere in the world apart from Norway**. At 23.9kg a year per person, that’s more than the weight of a suitcase you’d typically be allowed to take with you onto a plane.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

<!--quoteend-->

> People really care about reducing waste. Witness the outrage at the recent revelations of Amazon’s policy of destroying unsold or returned goods in the UK.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

<!--quoteend-->

> [[Green Alliance]]’s research with Cardiff University in 2018 showed 75 per cent of people in the UK want the government to require manufacturers to make products more repairable.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]


## 2021

-   The main barrier to repair has not been addressed
-   Only a few products are included in the legislation
-   It doesn’t extend to a consumer right to repair
    
    > Last week, the UK adopted what it described as “tough new rules” meaning that manufacturers will have to make repair information and spare parts available for repairs for up to ten years for some new white goods and televisions. The move aligns Great Britain with the EU and Northern Ireland, where the same legislation came into effect in March.
    > 
    > &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]
    
    <!--quoteend-->
    
    > In announcing the new standards, the government boasted it was giving us all a new “legal right for repair”. However, this is not true. In reality we are still far from having such a legal right in the UK.
    > 
    > &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]
    
    <!--quoteend-->
    
    > This new regulation doesn’t put any caps on the prices that manufacturers can charge for spare parts. And it does nothing to make repair more financially attractive to consumers, for instance by removing VAT, which could reduce the cost of all professional repairs. 
    > 
    > &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

> The legislation also allows the practice of ‘[[bundling]]’ multiple smaller components together with some common spare parts.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

<!--quoteend-->

> What’s more, the new regulations only tackle a few product categories: washing machines, dishwashers, fridges and electronic screens, including TVs. We know that work is ongoing at the EU level to bring repairability requirements to products such as smartphones, tablets and laptops, but it’s not yet clear whether the UK will follow suit.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]


## 2020

> If the UK would like to exceed the EU measures, the way forward is clear: the UK could easily adopt European Right to Repair measures and extend [[access to spare parts]] and [[access to repair information]] to all consumers, not just professional repairers.
> 
> Ultimately, if we do not adopt European standards as a minimum, what mechanisms are in place to ensure we do not become a dumping ground of sub-standard products?
> 
> &#x2013; [Great Right to Repair news from Brussels - we must benefit in the UK - The Re&#x2026;](https://therestartproject.org/news/right-to-repair-news-uk/) 

<!--quoteend-->

> We have some new doubts about the UK’s sincerity about “doing better” than Europe, as this week after failing to meet electronic waste collection targets, Defra announced it would simply lower them. According to trusted industry insiders, this “appears to signal a decision not to be bound by EU targets as we go through the Brexit transition phase”.
> 
> If we cannot be serious about the collection of [[electronic waste]], what hope is there to prevent waste upstream?
> 
> &#x2013; [Great Right to Repair news from Brussels - we must benefit in the UK - The Re&#x2026;](https://therestartproject.org/news/right-to-repair-news-uk/) 

<!--quoteend-->

> A 2020 study by the European Environment Agency showed that smartphones last on average 2 years which is 3 years less than what people would like them to last.
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

<!--quoteend-->

> Extending the lifetime of all smartphones in the EU by 1 year would save 2.1 Mt CO2 per year by 2030, the equivalent of taking over a million cars off the roads. This is why one of our first campaign demands was measures to make longer-lasting and more repairable smartphones the norm! 
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

<!--quoteend-->

> 86kg of “invisible waste” is generated on average to produce a smartphone (weighing around 200g) from scratch according to a study by Avfall Sverige on the waste footprint of products. This can be mining waste, slag and other type of waste produced during the extraction of minerals and the manufacturing of the product. 
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

<!--quoteend-->

> Europe is the largest generator of electronic waste per person in the world. How much do Europeans generate on average per year?
> 
> 16 kg. Europe ranked first worldwide in the 2019 UN e-waste monitor in terms of e-waste generation per capita, with 16.2 kg per capita. Oceania was second (16.1 kg per capita), followed by the Americas (13.3 kg per capita), while Asia and Africa generated just 5.6 and 2.5 kg per capita, respectively.
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

<!--quoteend-->

> Our electronics are filled with gold, copper and aluminium, as well as “critical raw materials” that need to be mined from the Earth. What is the recycling rate of these critical raw materials?
> 
> For the vast majority, it’s less than 6%: Rare earth elements like neodymium have nearly insignificant rates of recycling. Recyclers are constantly playing catch-up to an ever-faster cycle of new products, new materials and new technologies - having to invent new techniques and business models for processing dead devices. What this means in practice is that demand for virgin critical raw materials continues to increase with every new product we buy. This is why we need the Right to Repair to extend the lifespan of our devices.
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

[[Repairability standard]]

