# European Commission

The European Union's executive branch.

Responsible for proposing legislation, implementing decisions, upholding the EU treaties and managing the day-to-day business of the EU.

