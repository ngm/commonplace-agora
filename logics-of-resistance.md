# Logics of resistance

[[Anti-capitalism]].

SCHEDULED: <span class="timestamp-wrapper"><span class="timestamp">&lt;2020-08-03 Mon&gt;</span></span>

> Wright observes that there have been a number of strategies embraced by the Left. But they can be broadly amalgamated into two — a revolutionary strategy, which seeks to replace capitalism in a decisive break, and a more gradualist one
> 
> &#x2013; A Blueprint for Socialism in the Twenty-First Century


## [[Four types of anti-capitalism]]


## Wright's suggestion: mixture of taming and eroding

> We need a way of linking the bottom-up, society-centered strategic vision of anarchism with the top-down, state-centered strategic logic of social democracy
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

He describes some 'real utopias'


## [[Horizontalism vs verticalism]]


### Some possible arguments for the party form

> While the people who went into the streets were united in their indignation over injustice and their opposition to the old order, they had very different ideas about the future of their countries. When the regimes collapsed, the only parties established enough to take advantage were those aligned with the long-suppressed Muslim Brotherhood.
> 
> &#x2026;
> 
> &#x2026;the victory was the natural outcome of the inevitable schism between the nature of the revolution and the readiness of the Islamists for power.
> 
> When there is no single leader to focus a political movement — Khomeini, Mandela, Lenin — there may be more and faster revolutions than previously, but there are fewer revolutionary outcomes and scenarios,” Ullah writes. “So when a dictatorship – by definition and decree the sole and strongest institution in a country — is deposed by insurrections like the Arab Spring, what comes into the place of the power vacuum is not dictated by those who have created it.
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

<!--quoteend-->

> Online organizing and propaganda can be legitimately useful for destabilizing regimes, especially rigidly authoritarian ones that need to strictly control the flow of information. But because of the speed with which it can precipitate change, it is less useful for building up the networks and organizations needed to fill the gap created when old governments actually fall.
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)


## Continuity or rupture?

Crisis will have to erupt, with some popular upsurge alongside it. 

But you need need reform leading up to the point of rupture, otherwise you will be unprepared for the rupture.  You have to steadily build and organise while preparing for it.


## Insurrection or electoralism

> this debate really shakes down into a confrontation between those arguing for a (pro-Kautsky) strategic orientation that seeks to combine electoral and parliamentary activity on the one hand, with extra-parliamentary mobilisation on the other, versus a (pro-Lenin) strategy that hinges on the need for the insurrectionary overthrow of the existing parliamentary state and to place all power into the hands of soviets (workers’ councils)
> 
> &#x2013; [The Bolsheviks did not 'smash' the old state // New Socialist](https://newsocialist.org.uk/bolsheviks-did-not-smash-old-state/)  

