# distributed ledgers and commoning

> [[Distributed ledger]]s are significant for the commons because they have the potential for providing a powerful software architecture to support commoning on open networks,
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Why are distributed ledger technologies important for commoning? Because they potentially offer a way for commoners to wrest control of the “master switch” in digital technologies away from capital, and instead empower and protect collective action.
> 
> &#x2013; [[Free, Fair and Alive]]

