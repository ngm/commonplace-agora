# 2020-04-11



## Watching Aphex Twin's Manchester set

This is an epic set from Aphex Twin from Manchester last year. Great visuals too with the embarrassing crowd interaction.  Thanks to Sophy for the tip!

[Aphex Twin – Manchester 20/09/19 - YouTube](https://youtube.com/watch?v=961uG4Ixg_Y)

