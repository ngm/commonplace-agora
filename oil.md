# Oil

> Most oil deposits are thought to have been laid down between 10 and 180 million years ago, when dead zooplankton and algae sank to the seafloor and were buried in layers of sediment, before being slowly transformed into hydrocarbons by heat and pressure and time
> 
> &#x2013; [[Wasteland]]

