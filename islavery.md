# iSlavery

Exploitation of workers for the production of technology in pursuit of profit.  As described in [[Goodbye iSlave]] by Jack Qiu.

> “iSlavery” is “a planetary system of domination, exploitation, and alienation…epitomized by the material and immaterial structures of capital accumulation”
> 
> &#x2013; [[Shackles of Digital Freedom]] 

Material being the physical devices, and immaterial being the things we do on them?

> This in turn underscores the “world system of gadgets” that Qiu refers to as “Appconn” (13); a system that encompasses those who “designed” the devices, those who “assembled” them, as well as those who use them.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

What is the story behind "assembled in China" that we might see on our devices?

> As Qiu demonstrates in discomforting detail this is a story that involves exploitative labor practices, enforced overtime, abusive managers, insufficient living quarters, and wage theft, in a system that he argues is similar to slavery.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

<!--quoteend-->

> “Technology does not guarantee progress. It is, instead, often abused to cause regress”

Referencing slavery is contentious, but it sounds like Qiu lays out his reasoning for using the terms.  

> In considering which elements from the history of slavery are particularly relevant for the story of “iSlavery,” Qiu emphasizes: how the slave trade made use of advanced technologies of its time (guns, magnetic compasses, slave ships); how the slave trade was linked to creating and satisfying consumer desires (sugar); and how the narrative of resistance and revolt is a key aspect of the history of slavery. For Qiu,  “iSlavery” is manifested in two forms: “manufacturing iSlaves” and “manufactured iSlaves.”
> 
> &#x2013; [[Shackles of Digital Freedom]] 

The [[manufacturing iSlaves]] are those who working in conditions similar to slavery to produce high-tech goods.

> In the process of creating high-tech gadgets there are many types of “manufacturing iSlaves,” in conditions similar to slavery “in its classic forms” including “Congolese mine workers” and “Indonesian child labor
> 
> &#x2013; [[Shackles of Digital Freedom]] 

<!--quoteend-->

> Qiu investigates many ways in which “institutions and practices similar to slavery” shape the lives of Foxconn workers.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

"[[Manufactured iSlaves]]" are those addicted and constantly attached to their devices.

> “manufactured iSlave” entails “a conceptual leap” (91) that moves away from the “practices similar to slavery” that define the “manufacturing iSlave” to instead signify “those who are constantly attached to their gadgets”
> 
> &#x2013; [[Shackles of Digital Freedom]] 

<!--quoteend-->

> many companies that have made their fortunes off the immaterial labor of legions of “manufactured iSlaves” dutifully clicking “like,” uploading photos, and hitting “tweet” all without any expectation that they will be paid for their labor. Indeed, in Qiu’s analysis, what keeps many “manufactured iSlaves” unaware of their shackles is that they don’t see what they are doing on their devices as labor.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

<!--quoteend-->

> While smartphones may be cast as the symbol of the exploitation of Foxconn workers, Qiu also notes that these devices allow for acts of resistance by these same workers “whose voices are increasingly heard online” 
> 
> &#x2013; [[Shackles of Digital Freedom]] 

[[Phone Story]].

> “the underbellies of the digital industries have been obscured and tucked away; too often, new media is assumed to represent modernity, and modernity assumed to represent freedom”
> 
> &#x2013; [[Shackles of Digital Freedom]] (from Goodbye iSlave)

<!--quoteend-->

> Qiu highlights the coercion and misery that are lurking below the surface of every silly cat picture uploaded on Instagram, and he questions whether the person doing the picture taking and uploading is also being exploited.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

There are lots of good arguments within the book, but the use of slavery as a frame is problematic, despite Qiu's nuanced approach to it. 

> The matter of “slavery” only gets thornier as Qiu shifts his attention from “manufacturing iSlaves” to “manufactured iSlaves.” [&#x2026;] When Qiu discusses “manufactured iSlaves” he notes that it represents a “conceptual leap,” but by continuing to use the term “slave” this “conceptual leap” unfortunately hampers his broader points about Foxconn workers. The danger is that a sort of false equivalency risks being created in which smartphone users shrug off their complicity in the exploitation of assembly workers by saying, “hey, I’m exploited too.”

