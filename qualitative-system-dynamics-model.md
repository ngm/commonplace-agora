# qualitative system dynamics model

Qualitative [[system dynamics]] models.

> Qualitative system dynamics models (QSDMs) focus on the structure of a system and the qualitative relation between system components (Luna-Reyes and Andersen, 2003).
> 
> &#x2013; [[A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]]


## Why?

> describing a system is, in itself, a useful thing to do which might lead to better understanding of the problem in question
> 
> &#x2013; [[Qualitative and quantitative modelling in system dynamics]]

Apparently there's some debate between the relative merits of qualitative system dynamics models vs [[quantitative system dynamics model]]s.

> Using SD qualitatively involves focusing on discovering the feedback loops to describe a dynamically complex system. In this case, the use of tools such as [[causal loop diagrams]] (CLD) (see table 1 for an explanation), and in some occasions stocks and flow networks [[[stock and flow diagram]]s], without quantitative modeling is enough to provide insights for decision makers (Lane and Husemann 2008; Kunc and Morecroft 2009)
> 
> &#x2013; [[System dynamics: a soft and hard approach to modelling]]

