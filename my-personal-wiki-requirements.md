# My personal wiki requirements

Without getting into the weeds of  specific [[tooling]], what do I actually want to do with a personal wiki?


## Higher level stuff

[NOTE: this is pretty [[wiki/garden]] focused - I would like to take this up a level to also include the [[stream]] stuff I want, too. That's currently kind of separate in [[blog tooling]].]

-   something that makes writing easy to do and easy to maintain. Whatever works best for you for getting your ideas down, is the most important.
-   Next up is some way of linking those ideas.
-   A way to help you see the [[constellations]] of your thoughts
-   If you want it to feed into publish discussion (which I do), you probably want some way of publishing it.
-   [[Libre software]].  I don't want to use a commercial VC-funded silo that will monetise me and my thoughts, then close down.

Lilia Efimova writes about her [dream wiki/weblog tool](http://blog.mathemagenic.com/2004/06/08.html#a1233) in 2004(!).


## More technical stuff

Some more personal/techy bits of it that I want:

-   **plain-text**: I just want text files, man.  Mainly so I can edit them with spacemacs, but for all the other benefits of plain text over a DB.
-   **backed by version control**: I just like VC for things that incrementally change over time.  Also acts as a kind of backup if you push to a remote somewhere.
-   **publishable as HTML**: so I can point other people to it if it's ever handy to do so
-   **easy to write and maintain**: otherwise I know my enthusiasm will fizzle out.  For me that means key bindings and navigation that's intuitive to me.
-   **local first**: I want to be able to work on this without needing an Internet connection available.  To be fair, I usually do have one available, but very occasionally not.  And those times when I don't are quite often those when I have the most time and headspace for writing.  I also like local-first as a general principle.

Right now, I'm not so bothered about web access / mobile support.  I have [[ways of taking fleeting notes]] while I'm out and about if I need to, and I see longer-form writing as more of a desk-based activity.

In short I basically want a mash-up of TiddlyWiki and emacs, so I can do all the text editing in emacs, but with all the work that has gone into knowledge management in TiddlyWiki.

