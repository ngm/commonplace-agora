# green politics

> However, specifically Green Party politics, in some states, has seen a movement of former Marxist-Leninists towards a revisionist understanding of politics, with revolutionary objectives being discarded.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> movements are reflected in the work of a single individual. André Gorz, the French ecological theorist, acted paradoxically to promote a movement from red to green, and conversely from environmentalism to anti-capitalist commitment. Best known for his book Farewell to the Working Class, the former Marxist argued that class conflict was largely redundant and new social movements, including environmentalists, represented a force for potential change
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> The present Fourth International, from Ernest Mandel’s line, is explicitly ecosocialist in nature.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> Roberts’ argument was that a lack of democratic involvement including an absence of workers’ control, led to a frustrated demand for consumer goods. The less we participate and have the ability to shape our life experience, the more we compensate by consuming wasteful goods.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> This was based on an understanding that capitalism drives environmental destruction and thus green political economy inevitably demanded an articulation with anti-capitalism, if it was to provide a realistic chance of overcoming ecological problems.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> Kovel noted the distinction between 'use values' and 'exchange values', discussed by Marx in the first chapter of the first volume of Capital, was essential to creating an ecologically sustainable society. Thus by making goods to last longer and providing communal products for use, human prosperity could grow without the waste of capitalism.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

● right to repair

