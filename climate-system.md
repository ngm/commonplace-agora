# climate system

The Earth's climate system.  A [[complex system]].

Five interacting components:

-   the [[atmosphere]] (air)
-   the [[hydrosphere]] (water)
-   the [[cryosphere]] (ice and permafrost)
-   the [[lithosphere]] (earth's upper rocky layer)
-   the [[biosphere]] (living things)

