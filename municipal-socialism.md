# Municipal socialism

Things like:

-   [[municipal ownership]]
-   [[mutual aid]]
-   [[community organising]]
-   [[community wealth building]]
-   [[participatory budgeting]]
-   local [[Green New Deal]]s

to improve local economies and communities and the lives of residents.

[[The Preston Model]] being an example, I think.

But also wider society as a whole?:

> the spending of local authority money in order to benefit the public as a whole.
> 
> &#x2013; [Municipal socialism - Why the Liberals introduced social welfare reforms - Hi&#x2026;](https://www.bbc.co.uk/bitesize/guides/z83ggk7/revision/5)

Not a new thing:

> In the UK, interest in the economic and political possibilities of municipal socialism came and went with the rising and ebbing of the tides of economic reform and mass politics.
> 
> &#x2013; [The ‘Preston Model’ and the modern politics of municipal socialism - New thin&#x2026;](https://neweconomics.opendemocracy.net/preston-model-modern-politics-municipal-socialism/) 

In tension with top-down governance:

> Tensions soon arose, however, between local and national aspirations. With the rise of Labour as an electorally successful national party committed to a top-down reorganisation of the British economy, municipal socialism began to wither. 
> 
> &#x2013; [The ‘Preston Model’ and the modern politics of municipal socialism - New thin&#x2026;](https://neweconomics.opendemocracy.net/preston-model-modern-politics-municipal-socialism/) 

<!--quoteend-->

> Only with the sunset of the top-down Keynesian economic management of the postwar Golden Age did municipal socialism begin to re-emerge as a political force. In the dark days of Thatcherism, radical local experiments re-appeared in the shape of the Greater London Council (GLC) and other metropolitan councils
> 
> &#x2013; [The ‘Preston Model’ and the modern politics of municipal socialism - New thin&#x2026;](https://neweconomics.opendemocracy.net/preston-model-modern-politics-municipal-socialism/) 


## Challenges

> Though cooperatives may offer an additional line of defence they don’t prevent the systematic problems these regions face due to their location within the national economy. These models may help retain money spent in the economy but compared to national investment strategies they will do little to economic realities such as the fact almost half of new jobs in the UK are in the South East and London.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Articles

-   [The ‘Preston Model’ and the modern politics of municipal socialism](https://neweconomics.opendemocracy.net/preston-model-modern-politics-municipal-socialism/)
-   [Let’s see the Covid recovery bring in a new era of municipal socialism](https://labourlist.org/2021/02/lets-see-the-covid-recovery-bring-in-a-new-era-of-municipal-socialism/)
-   [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Misc

> Supportive municipal policies for provision or support for physical infrastructures for DisCOs, including hackerspaces, hackerlabs, maker and co-working spaces. Unused municipal facilities could serve as short- or long-term incubators for knowledge work, skill sharing and technology transfer, as well as for the development of new, federated DisCOs
> 
> &#x2013; [[The DisCO Elements]]

