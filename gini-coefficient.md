# Gini coefficient

> Gini coefficient, devised by the Italian sociologist Corrado Gini in 1912, is a measure of income or wealth disparity in a population
> 
> &#x2013; [[The Ministry for the Future]]

Has problems.

> The Gini figures for Bangladesh and for Holland are nearly the same, for instance, at 0.31; but the average annual income in Bangladesh is about $2,000, while in Holland it’s $50,000.
> 
> &#x2013; [[The Ministry for the Future]]

<!--quoteend-->

> While discussing inequality, it should be noted that the Gini coefficient for the whole world’s population is higher than for any individual country’s, basically because there are so many more poor people in the world than there are rich ones, so that cumulatively, globally, the number rises to around 0.7.
> 
> &#x2013; [[The Ministry for the Future]]

