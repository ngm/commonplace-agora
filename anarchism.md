# Anarchism



## What

Anarchy literally means 'without rulers'.  Anarchism I think of as a political philosophy meaning 'avoiding all unnecessary hierarchy'.

> anarchism represents a theorisation of **how society can be structured to enable [[liberty]] and [[solidarity]]**, **a society based on the principle of [[mutual aid]]**, where the needs of all are met through [[cooperation]] and the [[sharing of resources]].
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Real political change comes from below and from many points, not from above and from a center. "The anarchist alternative is that of fragmentation, fission rather than fusion, diversity rather than unity, a mass of societies rather than a mass society."
> 
> &#x2013; [[Is Post-Structuralist Political Theory Anarchist?]] 

<!--quoteend-->

> It is also not a form of amoralism. By refusing to submit to an ideal of “the good,” anarchism does not reject morality.
> 
> &#x2013; [[Is Post-Structuralist Political Theory Anarchist?]] 

<!--quoteend-->

> Nonetheless, anarchism has always been centrally concerned with self-organisation, with how groups of people can collectively govern themselves and make decisions about how they want to exist as a community.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Anarchism is the opposition to all hierarchical power structures, the framework for locating and understanding them, and the method by which we might dismantle and replace those hierarchical power structures with a horizontal society of free association, controlled together by the people, which we call anarchy.  This definition then references three distinct aspects of anarchism: **a mode of analysis**, **a method of struggle**, and **a socio-political goal**
> 
> &#x2013; [[A Modern Anarchism (Part 1): Anarchist Analysis]]

-   a mode of analysis
-   a method of struggle
-   a socio-political goal


## History

Anarchism emerged in the 19th century.

> Anarchism emerged, as Ruth Kinna and Alex Prichard argue, in the 19th century, from a critique of [[slavery]] and [[private property]], and how both were made possible by the [[state]].
> 
> &#x2013; [[Anarchist Cybernetics]]


## Cricicism

> Anarchism is often dismissed in the same terms as [[post-structuralism]] for being an ethical relativism or a voluntarist chaos.
> 
> &#x2013; [[Is Post-Structuralist Political Theory Anarchist?]] 


## Links

-   [Demanding the Impossible](https://www.akpress.org/demandingtheimpossible.html)
-   Port Nasau pirates - [Republic of Pirates - Wikipedia](https://en.wikipedia.org/wiki/Republic_of_Pirates#Pirates_of_Nassau)

