# digital ecosocialism

Digital ecosocialism is the construction and application of digital technologies to support the ideals of [[ecosocialism]] and the transition to an ecosocialist society.

That is, initiatives in the ICT sector that enable some combination of [[planetary stability]], [[social equity]] and [[agency]].

They usually take some form of [[resist, regulate, recode]].

They  embody the general principles of open knowledge; cooperativism; commoning; degrowth ; socially useful / ecologically safe production; adversarial interoperability; municipal support; innovation from below; and socialisation / deprivatisation (of physical and digital ICT infrastructure).

They should be transformative - i.e. actively enabling a transition to ecosocialism. They should not be simply maintaining the status quo or making mild reforms.

Examples of digital ecosocialist technologies and initiatives are: [[libre software]]; [[data commons]]; [[open hardware]] and the [[right to repair]]; [[platform cooperatives]].

