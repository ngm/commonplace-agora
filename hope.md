# Hope

On the podcast [[Building Lifeboats to the Emerging Futures with Sophia Parker of the Joseph Rowntree Foundation]] they mention some different types of hope: pre-tragic hope; tragic hope; and post-tragic (emergent?) hope.  They recommend some writings from [[Rebecca Solnit]] on the idea.

I liked the definition of they give in the podcast, something like: Hope is not optimism. Hope leads to action.

> It's the space of existence where you don't know that the actions you take are going to make the difference, but you believe that they might.
> 
> &#x2013; [[Building Lifeboats to the Emerging Futures with Sophia Parker of the Joseph Rowntree Foundation]]

