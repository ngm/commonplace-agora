# Convert a folder of org files to markdown

I wanted to see how my [[org-roam]] files look like in [[Obsidian]]'s graph view.  Commence massive yak shave to recursively convert a folder of org files to markdown.  Result below, pretty much a copy paste from here: [Bulk Org-Mode to Github Flavored Markdown](https://blog.mikecordell.com/2019/04/14/bulk-org-mode-to-github-flavored-markdown.html).  (I didn't bother with github flavoured markdown though).  

In the process I discovered that pandoc doesn't convert extensions of cross-file links, which was massively annoying.

```elisp
;;;###autoload
(require 'org)

(defun dired-org-to-markdown ()
    (let ((files
		     (append
				 (let ((default-directory "."))
					       (mapcar #'expand-file-name
									   (file-expand-wildcards "**/*.org")))
					   (let ((default-directory "."))
							 (mapcar #'expand-file-name
										     (file-expand-wildcards "*.org")))
						    )
			      ))
	  (mapc
		 (lambda (f)
			  (with-current-buffer
				       (find-file-noselect f)
						(org-md-export-to-markdown)))
		      files))
      )

(dired-org-to-markdown)
```

Pop the above in a file called something like `export.el`, then run it with:

```sh
emacs --batch --load=export.el
```


## Ninja

Jethro Kuan uses Ninja for this: https://github.com/jethrokuan/braindump/blob/master/build.py


## Converting links to wikilink syntax

I'd like to do this so I could export to a repo that could be picked up by [[Agora]].

I haven't done it, but it would probably be done by using a filter on the Markdown org-export backend:

```elisp
(defun commonplace/md-link-to-wikilink (text backend info)
  (when (org-export-derived-backend-p backend 'md)
    (replace-regexp-in-string "..." "..." text)))

(add-to-list 'org-export-filter-link-functions
	     'commonplace/md-link-to-wikilink)
```

