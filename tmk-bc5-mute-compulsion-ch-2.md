# TMK BC5: Mute Compulsion, Ch. 2

A
: [[podcast]]

Found at
: https://www.patreon.com/posts/premium-282-tmk-89182323

Part of
: [[This Machine Kills]]

Discussing chapter 2 of [[Mute Compulsion]].

> We discuss chapter two – Power and Marxism – which offers a survey of how Marxist theories of power have largely been caught between the two poles of violence and ideology. With power-as-violence, the focus has been on the changing roles of the state, its relationship with capital, and the ways it physically controls people. With power-as-ideology, the focus has been on the role of institutions and culture in shaping how people think, thus exercising forms of mental control over people. While both these traditions are valuable and necessary, as Mau argues, they offer a duality of power, rather than a trinity. That missing link? Mute compulsion

[[Power]] and [[Marxism]].

They also talk about [[Historical materialism]], violence and the state, [[Hegemony]], [[ideology]].

In Marxist theory for a while there was too much focus on violence and the state. Then, a pendulum swing to focus on ideology and hegemony.

Both are necessary, but misses out economic power.

At the end of this chapter Mau 'pulls in his team' - Marxist feminists, Marxist ecologists, and labour theorists.

