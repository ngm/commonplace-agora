# Don't buy from Amazon

Don't buy from [[Amazon]], if you can avoid it.


## Because

-   [[Amazon exploits workers and is anti-union]]
-   [[Amazon is a chronic tax dodger]]


## See also

-   [Boycott Amazon | Ethical Consumer](https://www.ethicalconsumer.org/ethicalcampaigns/boycott-amazon)

