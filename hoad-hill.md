# Hoad Hill

Ulverston's friendly local hill.

The view towards the [[Lake District]] from the top of Hoad Hill, in December 2020.

![[photos/lakes-from-hoad.jpg]]

