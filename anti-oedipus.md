# Anti-Oedipus

> Anti-Oedipus (1972). Here, Deleuze and Guattari set out to explain the relationship between desire and reality, connecting the pitfalls of psychoanalysis and philosophy with the current state of political affairs. They wanted to show how desire interacts with the material world, and to examine how it was entwined with politics
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

