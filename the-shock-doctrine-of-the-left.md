# The Shock Doctrine of the Left

A
: [[book]]

Found at
: https://www.politybooks.com/bookdetail?book_slug=the-shock-doctrine-of-the-left--9781509528547

Written by
: [[Graham Jones]]

Very good. Has a primary focus on [[movement building]], organising. The combination of [[leftist politics]] and [[complex adaptive systems]] is right up my street.

And also touches on topics such as [[organisational ecology]], [[care work]].

I would like to apply some of the concepts to [[reclaim the stacks]]. Particularly the description of using (and creating) shocks as points of leverage and transition is useful.

> [[Shocks]], from natural disasters to military catastrophes, have long been exploited by the state to impose privatization, cuts and rampant free markets. This book argues that the left can use such moments of [[chaos]] to achieve [[emancipation]].
> 
> Graham Jones illustrates how everyone can help to exploit these shocks and bring about a new world of [[compassion]] and [[care]]. He examines how combining mutually reinforcing strategies of ‘smashing, building, healing and taming’ can become the basis of a unified left. His vivid personal experience underpins a compelling, practical vision for [[activism]], from the scale of the individual body to the global [[social movement]].
> 
> This bold book is a toolkit for [[revolution]] for activists and radical millennials everywhere.

The idea of shocks obviously from Naomi Klein's [[The Shock Doctrine]].


## Use of complex adaptive systems

With regards to complex systems, I like his usage of 'body' rather than 'system'.  As a way of making the topic more approachable.  He talks about '**parts-relations-wholes, pasts-presents-futures**' which is a pretty good memory aid for thinking about system structure and system dynamics.

> This model provides a simple but widely applicable tool for understanding complex bodies. Parts in relation creating emergent wholes. Past, present and future synthesized in every moment to produce change.

He references [[The Systems View of Life]] as a good overview of complex systems.  He also mentions [[The Entropy of Capitalism]] as a modern application to leftist politics.  (Aside: Jones' book is much easier to read than Biel's!)

I'm not sure he fully details why complex adaptive systems are useful. But I'm willing to believe.

> The body model of ‘parts–relations–wholes, pasts–presents–futures’ can be used as a tool for engaging in social struggle. When planning action, mapping the parts and relations within our opponents can highlight their weaknesses and our potential allies. When building organizations, a focus on how parts are interacting can inform how we design and structure bodies to create the powers we want to emerge. In mediating interpersonal disputes, and unlearning and healing from oppressions, it can help to understand how bodies on divergent paths have created conflict, and to ensure that the needs of all bodies affected are taken into account, in all their differences of experience and knowledge. And in navigating the corridors of power, it can help us understand how and why our paths can be corrupted, and what we can do to prevent this


## Strategies

He repurposes [[Erik Olin Wright]]'s [[four types of anti-capitalism]] into Smashing, Building, Healing and Taming.

Not that dissimilar to [[resist, regulate, recode]].  Resist = Smash.  Recode = Build.  Regulate = Tame.

Smashing:

> Taken as a whole, this chapter suggests a starting point for countering the neoliberal shock doctrine: using disruptive actions to create our own chaos, as the initial spark of an accelerator of movement growth, to feed larger and larger shocks. In order to grasp these moments fully, we will also need organizations ready to create and push for something new, and not merely destroy the old. This is therefore the focus of the next chapter.

Building:

> Acts of smashing, while vital for disruption, do not create the kind of resilient, large-scale, long-term bodies needed to replace dominant powers. As we have seen, the direction our world takes in moments of chaos will be defined by the ideas and institutions that are already available. If we want a world of workplaces owned and run cooperatively, of political decision-making power in local community hands, we stand a much better chance if this is already being built in time for social shocks.

Healing:

> The new world we build must not only create powerful collective bodies, but must ensure that its parts – us – are autonomous, empowered and cared for also

Taming:

> The previous chapters have focused on action from outside the state: attacking enemy bodies, creating new alternatives, repairing the relations in our existing communities. Action within the state – such as through elections – is a more contentious topic, as it represents a point at which parts of the left sharply diverge. Either way, given the wideranging impact of electoral politics, as well as the state’s ability to hinder our other interventions, we stand to benefit from investigating its dynamics more closely.

The Meta-Strategy:

> Different strategies of the left tend to privilege some of the four logics over the others. Where these are not coordinated, tensions can arise. Smashing actions can unintentionally harm bodies we wish to preserve, whether through creating power vacuums, trauma, or retaliation. Building of a social movement body may reproduce internal power relations that others are struggling against, such as how leftist organizations can nonetheless be racist, sexist, ableist and so on. Communities focused on Healing may develop processes to protect people from oppressed and traumatized backgrounds that are ethically laudable, but become increasingly impenetrable to outsiders, and so hamper growth. And Taming dominant powers through the state comes at the risk of empowering the very bodies which seek to repress and demobilize the movement. Only by coordinating these different parts can we overcome these tensions, and so create an emergent whole strategy.

