# Imperialism is the Arsonist of our Forests and Savannas

URL
: https://anti-imperialism.org/2018/02/26/thomas-sankara-imperialism-is-the-arsonist-of-our-forests-and-savannas/

A speech by [[Thomas Sankara]].

[[Anti-imperialism]].

> In his speech Imperialism is the Arsonist of our Fires and Savannas, Sankara shows how anti-imperialist struggle and ecological struggle are one and the same.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> This speech was first given at the first International Silva Conference for the Protection of the Trees and Forests in Paris. It has been since been disseminated by revolutionaries through many means as a way to underscore **the necessity of social revolution and an environmental focus**. Now more than ever this speech is necessary to internalize, as we face profound and previously unseen danger regarding environmental crises in capitalism.
> 
> &#x2013; [Thomas Sankara: Imperialism is the Arsonist of our Forests and Savannas | Ant&#x2026;](https://anti-imperialism.org/2018/02/26/thomas-sankara-imperialism-is-the-arsonist-of-our-forests-and-savannas/) 

The necessity of [[social revolution]] with an environmental focus.

> Colonial plunder has decimated our forests without the slightest thought of replenishing them for our tomorrows.

<!--quoteend-->

> We therefore wish to affirm that the battle against the encroachment of the desert is a battle to establish a balance between man, nature, and society. As such it is a political battle above all, and not an act of fate.

<!--quoteend-->

> This is the place to denounce the one-sided contracts and draconian conditions imposed by banks and other financial institutions that doom our projects in this field. It is these prohibitive conditions that lead to our countries’ traumatizing debt and eliminate any meaningful maneuvering room.

