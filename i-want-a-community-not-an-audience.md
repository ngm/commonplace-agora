# I want a community, not an audience

Bad [[social media]] gives you an audience: stuff like reach, personality/celebrity, [[spectacle]], anxiety, [[alienation]], competition.

Good social media gives you community: it’s more like voice, agency, discussion, comradery.

I want a community, not an audience. 

