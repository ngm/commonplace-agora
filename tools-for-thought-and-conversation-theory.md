# Tools for thought and conversation theory

[[Tools for thought]] + [[Conversation theory]].

> We often talk about knowledge as if it is a storable commodity. We gain, gather, and transfer knowledge, share knowledge artifacts, build knowledge graphs.
> 
> Conversation Theory takes another view. It sees knowledge as conversational. Knowledge exists subjectively in our minds, and is constructed through conversation with others
> 
> &#x2013; [[Notes are conversations across time]]

<!--quoteend-->

> A conversation can happen between yourself and yourself, across time, through the notes your past self took for your future self. An autopoietic system where information time travels between your future and past self in a meaningful cybernetic loop.
> 
> &#x2013; [[Notes are conversations across time]]

[[autopoesis]].

