# Technology of the Oppressed

URL
: https://mitpress.mit.edu/books/technology-oppressed

Author
: [[David Nemer]]

Subtitle
: Inequity and the Digital Mundane in Favelas of Brazil

> Brazilian favelas are impoverished settlements usually located on hillsides or the outskirts of a city. In Technology of the Oppressed, David Nemer draws on extensive ethnographic fieldwork to provide a rich account of how favela residents engage with technology in community technology centers and in their everyday lives. Their stories reveal [[the structural violence of the information age]]. But they also show how those oppressed by technology don't just reject it, but consciously resist and appropriate it, and how their experiences with digital technologies enable them to navigate both digital and nondigital sources of oppression—and even, at times, to flourish.

<!--quoteend-->

> Nemer uses a decolonial and intersectional framework called [[Mundane Technology]] as an analytical tool to understand how digital technologies can simultaneously be sites of oppression and tools in the fight for freedom. Building on the work of the Brazilian educator and philosopher [[Paulo Freire]], he shows how the favela residents appropriate everyday technologies—technological artifacts (cell phones, [[Facebook]]), operations ([[repair]]), and spaces (Telecenters and Lan Houses)—and use them to alleviate the oppression in their everyday lives. He also addresses the relationship of misinformation to radicalization and the rise of the new far right.

-   [[Even with access to technology, marginalized people face numerous sources of oppression]].

> Contrary to the simplistic techno-optimistic belief that technology will save the poor, even with access to technology these marginalized people face numerous sources of oppression, including technological biases, racism, classism, sexism, and censorship. Yet the spirit, love, community, resilience, and resistance of favela residents make possible their pursuit of freedom. 
> 
> &#x2013; [[Technology of the Oppressed]]

