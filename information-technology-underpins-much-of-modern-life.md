# Information technology underpins much of modern life

> networked digital information technology has become the dominant mode through which we experience the everyday. **In some important sense this class of technology now mediates just about everything we do.**
> 
> &#x2013; [[Radical Technologies]]

