# Centralisation

> Another way to see this is that a centralized bureaucracy does not have requisite variety to govern organic growth. The edge is more expressive than the center, and changes more quickly. This forces the center to limit the variety at the edge in order to maintain control of the system.
> 
> &#x2013; [[Network intersubjectives]] 

^ [[Law of Requisite Variety]]

> While centralization first evolves for reasons of efficiency, the purpose of the resulting hierarchy soon shifts toward its own perpetuation.
> 
> &#x2013; [[Network intersubjectives]] 

<!--quoteend-->

> The original purpose of a hierarchy is always to help its originating subsystems do their jobs better. This is something, unfortunately, that both the higher and the lower levels of a greatly articulated hierarchy easily can forget. Therefore, many systems are not meeting our goals because of malfunctioning hierarchies.
> 
> -   [[Thinking in Systems]]

