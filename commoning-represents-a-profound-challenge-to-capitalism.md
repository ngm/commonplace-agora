# Commoning represents a profound challenge to capitalism

[[Commoning]] represents a profound challenge to [[capitalism]].

A claim I first came across in [[Free, Fair and Alive]].


## Because&#x2026;

-   [[Commoning is based on a very different ontology from capitalism]]. OK&#x2026;  Need a bit more than that though.
-   Commoning is a means of provisioning with minimal reliance on markets or states


## Epistemic status

Type
: [[claim]]

Gut feeling
: 6

Confidence
: 2

I believe the claim, but need a bit more evidence.

