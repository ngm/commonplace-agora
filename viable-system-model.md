# Viable system model

> a general model that he believed balanced centralized and decentralized forms of control in organizations.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

A mixture of horizontal autonomy with channels for vertical communication and stabilisation.

> It offered a balance between centralized and decentralized control that prevented both the tyranny of authoritarianism and the chaos of total freedom.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

At first blush there feels like some overlap between the Viable System Model and Elinor Ostrom's Institutional Analysis and Development framework.

In that they both approach structures from a multi-level conceptual map, with units acting autonomously at each level but communicating between them. The polycentrism thing.

Would be interesting to compare and contrast them. 

[[Stafford Beer]].

> He explains that System Three is concerned with the ‘inside and now’ of the organisation and System Four with the ‘outside and then’ ([1979] 1994). This mirrors how strategy is defined by scholars of organisation theory (for example, Carter et al., 2008),
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> System Five, where overarching goals and political strategy is developed, is something that ought to be rooted in everyone involved in the system.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> The fourth level is where [[Gosplant]] would live, as it is where medium- and long-term plans are devised
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> As Medina notes, because daily management is affected by plans with a longer horizon, ‘Beer did not see System Four as the boss of System Three but rather as its partner in an ongoing conversation
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Perhaps most importantly for us, Beer’s model exemplifies the promise of using control theory to organize an economy at multiple levels, without relying solely on optimization, and avoiding Soviet cybernetics’ tendency towards rigidity
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Although Beer was not a Neurathian, much of his viable system model can be reconciled with in natura economics. This is because Beer’s model was designed to manage any complex system, whether the internal operations of a company or the co-ordination of an entire economy, and therefore could be based on natural units rather than money
> 
> [[Half-Earth Socialism]]

