# Fraidycat

> Fraidycat is a very small attempt to move toward tools that give us some power. It really only adds the ability to assign "importance" to someone you are following - allowing you to track them without needing to be aware of them every second. 
> 
> &#x2013; [Show HN: Fraidycat | Hacker News](https://news.ycombinator.com/item?id=22545878) 

