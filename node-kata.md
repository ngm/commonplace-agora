# Node kata

Like [[code kata]] but for nodes.

In a code kata you code up a simple program, and the idea is more on thinking about and improving the tools and process you do to do it, rather than the final output (in fact you usually ditch the final output).

How would that apply to a node/page in a digital garden?

Maybe it doesn't.  Maybe doing some [[wiki gardening]]  (general cleanup tasks, weeding and watering) would be better.

I do like the idea of doing some small task though that makes you think about how you're using your tools though, rather than getting lost in the content.

(Of course without fetishising the tools, and remembering that ultimately the whole point is the content&#x2026;)

