# Peter Kropotkin

> Peter Kropotkin (1842-1921) was a Russian anarchist, scientist, writer, and activist. He is widely seen as the most influential theorist of [[anarcho-communism]]. Kropotkin’s [[The Conquest of Bread]] outlines what a future anarcho-communist society could look like.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

