# How to Blow Up a Pipeline

A
: [[book]]

Author
: [[Andreas Malm]]

[[climate activism]], [[pacifism]], [[climate fatalism]].

> Malm argues that sabotage is a logical form of climate activism, and criticizes both pacifism within the climate movement and "climate fatalism" outside it.
> 
> -   [How to Blow Up a Pipeline - Wikipedia](https://en.wikipedia.org/wiki/How_to_Blow_Up_a_Pipeline)

