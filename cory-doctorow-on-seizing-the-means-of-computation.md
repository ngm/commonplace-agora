# Cory Doctorow on Seizing the Means of Computation

URL
: https://jimruttshow.blubrry.net/cory-doctorow-2/

Another interview with [[Cory Doctorow]] for [[The Internet Con]].

The presenter Jim Rutt is a bit of a character. But does have some insights on old technology and interoperability.

Doctorow says that anti-trust and tech cabals had led us to where we are.

Should check out the EFF thing on a thought experiment for Facebook interoperability.

