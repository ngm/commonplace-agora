# tiddlers

> The purpose of recording and organising information is so that it can be used again. The value of recorded information is directly proportional to the ease with which it can be re-used.
> 
> &#x2013; [Philosophy of tiddlers - TiddlyWiki](https://tiddlywiki.com/#Philosophy%20of%20Tiddlers) 

<!--quoteend-->

> What would be ideal, I think, is if all information could be represented as “cards”, and all cards could be easily threaded. Every book, every blogpost, every video, even songs, etc – all could be represented as “threaded cards”. Some cards more valuable than others.
> 
> &#x2013; [twitter threads solve the fragmentation problem - @visakanv's blog](http://www.visakanv.com/blog/threading/) 

I like this.  It's a bit like what TiddlyWiki goes for I think.  Or FedWiki.  Little cards joined together.  (As Panda pointed out to me - this is very much like what [[HyperCard]] was).

I'm not getting that so much with my current wiki setup - the finest grain is a page, which I'm thinking is a bit too big.  I have quotes, but they're kind of stuck in a page - not really reference-able outside of that.  My thoughts themselves are just a mash.

To some extent however I would like to push back against everything becoming bite-sized.  Is every paragraph a self-contained digestible thought?  Let's not lose sight of the long-form idea, the slow-burner.  The album is not just 12 singles.  But, if cards are a means to forming threads, then that's OK I think.

(h/t to Kicks for sharing that link)

> The philosophy of tiddlers is that we maximise the possibilities for re-use by slicing information up into the smallest semantically meaningful units with rich modelling of relationships between them. Then we use aggregation and composition to weave the fragments together to present narrative stories.
> 
> TiddlyWiki aspires to provide an algebra for tiddlers, a concise way of expressing and exploring the relationships between items of information. 
> 
> &#x2013; [Philosophy of tiddlers - TiddlyWiki](https://tiddlywiki.com/#Philosophy%20of%20Tiddlers) 

[[Ton]] has [[referred]] to tiddlers as crumbs, and is somewhat skeptical of how useful they are in contributing to new ideas.

