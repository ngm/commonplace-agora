# Repair and degrowth

[[Repair]] could be seen as a part of [[degrowth]] because:

-   by repairing broken things we can extend their useful life, avoiding the production and consumption of new items
    -   this reduces waste and pollution
    -   this reduces the demand for natural resources and energy
-   repair promotes both self-reliance and community
-   repair fosters commons

