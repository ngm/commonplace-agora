# Rent

> Rent, in other words, is not just an economic phenomenon: it’s a reflection of the prevailing political order, and the struggles that define it
> 
> &#x2013; [[The Rent Is Too Damn High]]

<!--quoteend-->

> This ‘30% rule’ has its roots in American president Lyndon B. Johnson’s ‘Great Society’ programmes, when, following 1968’s Housing and Urban Development Act, legislation was passed capping rents in public housing at 25 per cent of residents’ income. This in turn was raised to 30 per cent in the 1980s
> 
> &#x2013; [[The Rent Is Too Damn High]]

