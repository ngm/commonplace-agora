# data commons

"data of the people, by the people, for the people" (https://datacommons.barcelona/en/)


## What is a data commons?

> A data commons is a software platform along with a governance framework that together allow a community to manage, analyze and share its data.
> 
> &#x2013; [3 Key Steps to a Successful Data Commons - data.org](https://data.org/guides/3-key-steps-to-a-successful-data-commons/)

<!--quoteend-->

> in an early definition of “data commons,” human-computer-interaction researchers Dana Cuff and colleagues described them as “repositories generated through decentralized collection, shared freely, and amenable to distributed sense-making” and noted that these repositories “have been proposed as transforming the pursuit of science but also advocacy, art, play, and politics.”
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]


## Don't forget the commoning part of it

https://www.go-fair.org/fair-principles/

> In Bristol, data commons, rather than being only stocks or stores of citizen-generated data, became sites to develop relationships of solidarity through the tolerance of friction, tension, and dispute about how data connects with things that matter to people. 
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

For example, Google's datacommons.org, doesn't really feel like a commons.  Where's the community governance?  A data repo managed by a big tech corp is not a commons.


## Why?  What are they for?

A counter to big tech data collection.

> We need a new way of thinking about data not as a commodity to be further protected by stronger intellectual property laws but as a collective resource that could be used to empower citizens and provide them with tools to tackle their own problems.
> 
> &#x2013; [[Platform socialism]]


### What's the problem?

> A reliance on private companies to provide the infrastructure of projects has led to a lack of control over how data is collected and handled. Companies currently use data collected through digital platforms as a commodity to generate economic value for shareholders. Data ends up being siloed and controlled by a few individuals with little benefit flowing to citizens. Due to the way it has been abused by these companies, the very idea of data collection has developed sinister connotations of surveillance and manipulation. When platforms do produce reports they often involve the highly questionable and selective use of data to promote the company and shine a positive light on its activities.
> 
> &#x2013; [[Platform socialism]]


### What are the benefits?

> A data commons has a huge potential to create a launching pad for further innovation and development. It provides enormous public benefit while at the same time putting citizens in control of their digital future.
> 
> &#x2013; [[Platform socialism]]


## How?

> There is no single model of how a data commons should be organised. Each institution would need to be designed based on the needs of the community, the type of data and how it could be collected and used. These would range from large-scale public [[data trusts]] (for health, science and mobility) to commercial and financial data firms and local [[data co-operatives]].
> 
> &#x2013; [[Platform socialism]]


## Examples

-   [[DECODE]]?
-   [[Open Street Map]]  ([[Is Open Street Map a data commons?]])
-   [[Open Repair Alliance]]?


## Limitations / criticisms

> One tension of a data commons is the balance between the potential benefits of large shared troves of data versus the harm that could be caused from their misuse. Data collection and use must be consensual, fair and transparent. In some cases, certain types of data simply shouldn’t be collected because the risk posed to individuals is too high. No security system is fail-safe and we should think carefully before collecting any type of data and ask first whether it is necessary and potentially of public value. There are other forms of discriminatory data gathering which should be eliminated outright. This includes carceral technology to assist predictive policing, which Ruha Benjamin in Race after Technology has called to be abolished
> 
> &#x2013; [[Platform socialism]]


## Resources

-   [Unlocking the value in data as a commons | Nesta](https://www.nesta.org.uk/feature/four-future-scenarios-personal-data-economy-2035/unlocking-the-value-in-data-as-a-commons/)
-   [Data Commons &amp; Data Trusts. What they are and how they relate | by Anouk Ruha&#x2026;](https://medium.com/@anoukruhaak/data-commons-data-trust-63ac64c1c0c2)

