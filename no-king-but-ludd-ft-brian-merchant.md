# No King But Ludd (ft. Brian Merchant)

A
: [[podcast]]

URL
: https://soundcloud.com/thismachinekillspod/286-no-king-but-ludd-ft-brian-merchant

Brian Merchant discussing [[Luddites]], [[Luddism]] and his book [[Blood in the Machine]].

-   ~01:01:28: Anti Luddite reaction was one of first examples of state siding with capital over people.
-   ~01:17:09: The writers strike is a good example of some modern day Luddism.

