# Repairing a Cat Mate C500 automatic pet feeder

We have one of these: [[Cat Mate C500 automatic pet feeder]]

It started going into an infinite spin.  (Which drove [[Enid]] a bit mad!)

Thanks to this very helpful video [Fixing Cat Mate / Closer Pets Automatic Feeder (Infinite Spinning)](https://www.youtube.com/watch?v=EYNqHgT_C-U), I learned that usually all you need to do is clean the sensor.

I did, and it worked fine again.

Handy tip also about holding ADJ + UP to make it spin to test it, and to realign it if it's gotten misaligned.

