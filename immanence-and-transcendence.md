# Immanence and transcendence

I half listened to the philosophisethis podcast on [[Deleuze &amp; Guattari]]'s [thoughts on immanence](https://www.philosophizethis.org/podcast/gilles-deleuze-pt-2-immanence) while doing other things.

One thing I latched on to was transcendence inevitably introducing hierarchy into thought, and [[immanence]] being more horizontal and equalising.  So I liked that.

Searching for an image to go along with immanence and transcendence, I found this [blog post about immanence and transcendence in Buddhism](https://jayarava.blogspot.com/2007/12/immanence-vs-transcendence.html).  

![[Immanence_and_transendence/2020-06-15_20-39-53_300px-A_thaliana_metabolic_network.png]]

I have no idea why they chose that image for the post, but I love it.  It seems very related to the kind of images you see about [[rhizomes]].

Also, you find excellent images like this:

![[2020-06-15_21-17-48_tabula1.gif]]

Also from a [Buddhist blog post](http://inebnetwork.org/transcendence-or-immanence/)!

