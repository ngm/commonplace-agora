# Marxist humanism

Some say Marx's values changed over time. Younger Marx cared more about humanity, later Marx cared more about the science and politics of his theories.

On this [podcast](https://revolutionaryleftradio.libsyn.com/humanism), Maximillian Alvarez says that if you're anti-capitalist, you're pretty much by definition believe in Marxist humanism.  Because if you didn't care about what was happening to people under capitalism, why would you be bothering?

Contrast Marxist humanism with liberal humanism.

