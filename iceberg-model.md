# Iceberg Model

In a nutshell, the idea is that you look below the surface of any problem/event, to figure out the underlying patterns, structures and mental models that led to the event.

You see it referenced a lot in relation to [[systems thinking]], but the results that come up in a search feel a little bit sketchy.  Where does it come from originally? It's mentioned obliquely in [[Thinking in Systems]], so perhaps that's the genesis.

> Like the tip of an iceberg rising above the water, events are the most visible aspect of a larger complex—but not always the most important.
> 
> &#x2013; [[Thinking in Systems]]


## Some links

-   [How The Iceberg Model of Systems Thinking Can Help You Solve Problems?](https://durmonski.com/self-improvement/iceberg-model-systems-thinking/)
-   [Iceberg Model - Ecochallenge.org](https://ecochallenge.org/iceberg-model/)

