# Actor-network theory

> ANT sees agency as a distributed achievement, emerging  from  associations  between human and non-human entities (the actor-network).
> 
> &#x2013; [Assemblage thinking and actor‐network theory: conjunctions, disjunctions, cross‐fertilisations](https://rgs-ibg.onlinelibrary.wiley.com/doi/full/10.1111/tran.12117) 

[[Post-structuralism]]?

