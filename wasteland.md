# Wasteland

A
: [[book]]

URL
: https://www.simonandschuster.co.uk/books/Wasteland/Oliver-Franklin-Wallis/9781398505452

Author
: [[Oliver Franklin-Wallis]]

Very enjoyable.

Blurb:

> When we throw things ‘away’, what does that actually mean? Where does it go, and who deals with it when it gets there? In Wasteland, award-winning journalist Oliver Franklin-Wallis takes us on an eye-opening journey through the global waste industry. From the mountainous landfills of New Delhi to Britain’s overflowing sewers, from hollowed-out mining towns in the USA to Ghana’s flooded second-hand markets, we meet the people on the frontline of our waste crisis – both those being exploited, and those determined to make a difference. On the way, we discover the corporate greenwashing that started the recycling movement; the dark truth behind our second-hand donations; and come face to face with the 10,000-year legacy of our nuclear waste.

[[Waste]].  [[waste streams]].

