# Cooperatives in Rojava

> In the case of NES, cooperatives also directly cover the needs of the local population, without commercializing their produce, and support civil organizations.
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> This economic legacy allowed the development of agricultural cooperatives, which today make up the bulk of NES’ cooperatives. But cooperatives have also developed in other economic spheres: mostly bakeries but also textiles, dairy production, small manufacturing, and even a salt mine.
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> Cooperatives in NES vary in terms of size and field of activity, but also by type. Some are worker cooperatives, which produce a good or provide a service and gain a profit to be distributed among members. Others are service cooperatives, which are a special type of consumption cooperative: financial capital is gathered by members in order to set up a service for the community, which is directly provided without being commercialized. This is most commonly done in order to purchase a diesel-powered electricity generator for a neighborhood or village, which can provide electricity when the general electricity is out.
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> Cooperatives are developed on the most local level, the communes, with the support of different bodies: the Cooperatives Bureau of each canton or region, the economy committees of local Women’s Councils, and Aboriya Jin. 
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> To get cooperatives started, the Cooperatives Bureau and [[Aboriya Jin]] get in touch with local communes, identify the needs of the commune and introduce them to the principle of cooperatives through training sessions. 
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

