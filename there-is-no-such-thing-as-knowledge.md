# There is no such thing as knowledge

So sayeth [[Mike Hales]] https://social.coop/@mike_hales/107430510590782176


## Because

Dunno

> It's hard to pitch and sustain this kind of perspective, and I can't easily point to other places where it's up front in the same way. Culture is so saturated with idealist perceptions of 'knowledge' as 'a body' or as accumulatable 'stuff'.
> 
> A historical, materialist framing - centred on **practices** of people-in-culture - is difficult to arrive at in the face of such hegemonic stuff


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Concentrate and ask again]]

I'm not really sure what Mike meant by this, and Mike himself doesn't seem to have a clear description..  But it's interesting and provocative and I [[Trust Situated Knowing]] from Mike.  I'd guess that Mike's gist is around knowledge being a relationship rather than an abstract thing.

