# We need to be thinking in terms of systems rather than technological quick fixes

> We need to be thinking in terms of systems rather than technological quick fixes. Discussions about smart cities, for example, regularly focus on better network infrastructures and the use of information and communication technologies such as integrated sensors, mobile phone apps, and online services. [&#x2026;] But this [[technological determinism]] doesn’t offer a holistic understanding of how such technologies might negatively impact critical aspects of city life. 
> 
> &#x2013; [[The Cybersyn Revolution]]

<!--quoteend-->

> Throughout the Cybersyn Project, Beer repeatedly expressed frustration that Cybersyn was viewed as a suite of technological fixes — an operations room, a network of telex machines, an economic simulator, software to track production data — rather than a way to restructure Chilean economic management.
> 
> &#x2013; [[The Cybersyn Revolution]]

<!--quoteend-->

> We must resist the kind of apolitical “innovation determinism” that sees the creation of the next app, online service, or networked device as the best way to move society forward. Instead, we should push ourselves to think creatively of ways to change the structure of our organizations, political processes, and societies for the better and about how new technologies might contribute to such efforts.
> 
> &#x2013; [[The Cybersyn Revolution]]

