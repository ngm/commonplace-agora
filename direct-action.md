# direct action

> Direct action is a tactic, not a strategy.
> 
> &#x2013; Kai Heron, [[Climate &amp; Revolution: How do we transition from catastrophe?]]

<!--quoteend-->

> Direct action, as a method of anarchism, involves those who are exploited and dominated acting for themselves, to change their situations in ways that do not depend on the goodwill or charity of others.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> direct action in the anarchist tradition as action that ‘refers to practical prefigurative activity carried out by subjugated groups in order to lessen or vanquish their own oppression’
> 
> &#x2013; [[Anarchist Cybernetics]]

