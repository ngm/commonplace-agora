# Aligning the look of my garden and stream

My wiki started quite a long while back, just taking notes in org-mode.  It got a bit of a reboot in October 2019 when I started publishing the notes with org-publish, and then a turbo boost in March 2020 when I started using org-roam.

(Side note: I should dig out some of my much much older blog posts, and wikify them.)

I'm totally into my wiki now.  I'm fairly certain that it's more than just a spike, an experiment, and will be something I continue for a long time.   It's the permanent record of my stream.

> My personal site is a repository for my memories, experiences, feelings, recipes, tips, photos, and more. [&#x2026;] it is an ever-growing extension of myself that I have total control over, my mirror and memory aid. I want to be able to look back at this when I’m eighty and thank my past self for surfacing things that I otherwise would have forgotten.
> 
> &#x2013;  [On personal sites, and adios analytics — Piper Haywood](https://piperhaywood.com/on-personal-sites-adios-analytics/)

So - given that, I've given it a bit of a makeover and tried to align the garden and the stream a bit.  I see them both now as major parts of my home on the web.

I've been doing that today - adding a bit of a shared top menu between the two, and pulling in some of the styles from my WordPress theme in to my org-publish styles.  The garden and stream are slightly different in layout, but nothing major.  The main thing I did was get rid of the left margin on the stream that was there before, to match the garden, which wouldn't look sensible with a huge left margin.

I've been using tailwind on my blog theme.  I know there's arguments against it, but it's been pretty handy.  So I've reused it on my garden part too.  That involved the obligatory yak-shave with node, npm, package.json, etc, but all good now.

