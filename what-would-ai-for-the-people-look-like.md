# What would AI for the people look like?

My biggest problems with [[AI]] are its environmental impact and the fact that it is in the hands of a cabal of big tech firms using it to turn a profit.

([[Generative AI has a ferocious environmental impact]]; [[Generative AI is further concentrating power with Big Tech]])

Is there a way it could be retained and reclaimed? Publicly owned, democratically governed, socially useful, and existing within planetary boundaries?

