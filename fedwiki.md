# FedWiki

&#x2013; [about federated wiki](http://fed.wiki.org/view/welcome-visitors/view/about-federated-wiki) 

> Smallest Federated Wiki Goals
> 
> -   Demonstrate that wiki would have been better had it been effectively federated from the beginning.
> -   Explore federation policies necessary to sustain an open creative community.

I very much like FedWiki and [[federated wikis]] as a principle.

However, a downside of FedWiki (as far as I understand it) is that it is one platform, one tool, that facilitates the federation.  A [monoculture](https://indieweb.org/monoculture).

I would prefer a more distributed, protcol-based approach.  [[Agora]] works in this sense as each individual can write their garden whichever way they see fit, and just needs to fit in to some accepted formatting protocols to be aggregated.

See also the [[IndieWeb]] approach, where the onus is less on aggregation and more on a certain protocol of communication.  

See [[Interlinking wikis]].

FedWiki does seem to offer much simpler actions for copying content from one wiki to another.  Agora doesn't have this built in as such.  IndieWeb maybe more so (retweets, quotes, likes, etc, are catered to through webmentions)


## Functionality

> All content posted on Fedwiki sites is automatically licensed under a Creative Commons Attribution-ShareAlike 4.0 license upon publi- cation — meaning that it is “born shareable” the moment someone publishes it, making it available to the federation of sites.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

<!--quoteend-->

> The Fedwiki recordkeeping “journal” tracks who has posted what, so authorship can be chronicled even if people make mashups of someone else’s content.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

<!--quoteend-->

> The Fedwiki commons does have one vulnerability to outside control that it has not, as yet, been able to evade: the authentication of digital identity. Because of the complexities of providing a commons-friendly alternative, Cunningham and his colleagues have relied on the identity systems developed by Google and Facebook that function as a default for many sites on the internet.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]


## Criticism

-   http://thoughtstorms.info/view/LeavingTheSFW

