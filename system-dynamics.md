# system dynamics

A way of looking at the behaviour of [[complex systems]].

> SD has a rich history on both uses: soft (qualitative or descriptive) and hard (quantitative or predictive/prescriptive) . Soft SD modelling can be easily used as a meta-modelling tool where the use of CLD helps the modeler to understand the system. Then, the modeler uses any quantitative method, e.g. statistical analysis, linear programming, discrete event simulation, SD or hybrid, to improve the performance of the system. Soft SD can also help modeler to engage with their clients to discuss the assumptions about the problem and what it is the best approach to model the problem when it is used in facilitated modeling workshops.
> 
> &#x2013; [[System dynamics: a soft and hard approach to modelling]]

<!--quoteend-->

> System dynamics models are particularly suitable to visualize and analyze systems that change over time in a nonlinear manner, and to communicate complex issues to non-experts (Forrester, 1958).
> 
> &#x2013; [[A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]]

