# Inventing the Future

A
: [[book]]

URL
: https://www.versobooks.com/books/2315-inventing-the-future

Subtitle
: [[Postcapitalism]] and a World Without Work

Authors
: [[Nick Srnicek]] / [[Alex Williams]]

First read it in 2017.

https://doubleloop.net/2017/04/08/inventing-the-future/

Inventing the Future is a political book that provides a great breakdown of why [[neoliberalism]] succeeded, and why the left has failed so far to reclaim any ground back.  It offers insights into how we might reclaim modernity and invent the future.

Despite its political density I found ItF to be thoroughly readable.  It’s also an exciting and motivating read.  On the left, you might feel a bit downtrodden of late, and lamenting why the world the way it is.  The ItF analysis of this is that the hegemony of neoliberalism was very cleverly instituted over several decades, weedling its premises into the background consciousness through thinktanks, placements in political institutions, etc, such that when the Keynesian economic era began to flounder in the 1970s, it simply seemed like there was only one answer to the question of what to do next – neoliberalism.

Where the left has failed in recent history is to do the same.  Despite chronic recessions since the turn of the century, highlighting the paucity of neoliberalism as a valid system, the left simply hasn’t been in a position to offer an alternative.  We should have been building up a body of evidence and opinion and mindshare over the last decades, such that our most progressive ideas were already pitched as the only alternative.

Srnicek and Williams make some bold demands in the book (and have elsewhere indicated that these are deliberately provocative, and perhaps not ever entirely attainable.)  These are: full automation; [[reduction of the working week]]; and a [[universal basic income]].  Concomitant to that is a general diminishment of work ethic.

They talk a lot about [[Horizontalism vs verticalism]], calling horizontalism [[folk politics]]. Kevin Carson gives a [lengthy critique](https://c4ss.org/content/50849) of some of their critique of [[horizontalism]].

