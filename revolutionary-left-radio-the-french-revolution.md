# Revolutionary Left Radio: The French Revolution

A
: [[podcast]]

URL
: https://revolutionaryleftradio.libsyn.com/the-french-revolution

Series
: [[Revolutionary Left Radio]]

[[French Revolution]].

> Together, they discuss the conditions that led to the revolution, the major events and figures of the Revolution, the importance of mass mobilization for any authentic revolution, the so-called Reign of Terror, Maximilien Robespierre, Jean-Paul Marat, the Guillotine, the role of women, Babeuf and the Conspiracy of Equals, the Thermidorian Reaction and the end of the Revolution, the response to the Rev by the rest of monarchical Europe, and why communists today should value this bourgeois revolution and even see it as a part of our egalitarian and revolutionary tradition!

Very enjoyable and informative.

-   ~00:05:00 [[The French Revolution has historical significance to revolutionary socialism today]].
-   ~00:10:26  [[Catalysts for the French Revolution]].
-   ~00:15:19  Enlightenment figures were some inspiration for Haitian, French revolutions.
-   ~00:23:02  [[Jean-Paul Marat]].
-   ~00:24:31  [[Political club]]s and [[Jacobin]]s.
-   ~00:26:58 [[Maximilien Robespierre]]. Inspired by [[Jean-Jacques Rousseau]].

