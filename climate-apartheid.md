# climate apartheid

> According to a UN report, “We risk a ‘climate apartheid’ scenario where the wealthy pay to escape overheating, hunger and conflict while the rest of the world is left to suffer.”
> 
> &#x2013; [[Revolution or Ruin]]

see [[Eco-apartheid]]

