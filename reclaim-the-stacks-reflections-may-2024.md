# Reclaim the Stacks: reflections, May 2024

[[Digital ecosocialism]] is a counter to [[digital capitalism]], and reclaiming the stacks is focused on the transition from the latter to the former.

![[reclaim-the-stacks.png]]

While I'm more interested in building the new world than theorising the old, it's true that you need to have an understanding of the current conditions in order to change them.

So, it's important to understand - just what is [[digital capitalism]]?  We probably all have a rough notion of what we mean by it - essentially, whatever it is that the [[Big Tech]] firms do ([[The purpose of a system is what it does]]).  But there's obviously a lot of fine-grained detail within that.

Two great resources have just dropped that will be of interest to anyone who wants a more thorough understanding:

-   [[Transnational Institute]]'s [[Digital Capitalism online course]]
-   [[tripleC]]'s special issue [[Critical Perspectives on Digital Capitalism: Theories and Praxis]]

I'm slowly working my way through the [[Digital Capitalism online course]].  It's a trove of well-presented learnings on digital capitalism - what it is, why it's bad, and what we could do instead.

I'm also dipping into [[Critical Perspectives on Digital Capitalism: Theories and Praxis]] here and there - it's much more dense and academic, but people like [[Christian Fuchs]] and [[Jodi Dean]] have been theorising on this for a long time, so you know it's going to be good.

