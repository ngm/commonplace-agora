# dual power

I like ryan's simple description:

"Dual power is the practice of setting up alternative institutions to state power."

Note: usually alongside state power (temporarily, i.e. [[Build the new world in the shell of the old]]).  Via [[mutualism]], [[municipalism]] and [[syndicalism]]. 


## [[Leninist]] take

Where the term originated I think.

> Two powers coexisted with each other and competed for legitimacy: the Soviets (workers' councils), particularly the Petrograd Soviet, and the continuing official state apparatus of the Russian Provisional Government of social democrats. 
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)


## [[Libertarian socialist]] take:

This one chimes more with me.

> [[Libertarian socialist]]s have more recently appropriated the term to refer to the nonviolent strategy of achieving a libertarian socialist economy and polity by means of incrementally establishing and then networking institutions of direct [[participatory democracy]] to contest the existing power structures of state and capitalism.
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

<!--quoteend-->

> This does not necessarily mean disengagement with existing institutions; for example, Yates McKee describes a dual-power approach as "forging alliances and supporting demands on existing institutions – elected officials, public agencies, universities, workplaces, banks, corporations, museums – while at the same time developing self-organized counter-institutions."
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

<!--quoteend-->

> In this context, the strategy itself is sometimes also referred to as "[[counterpower]]" to differentiate it from the term's Leninist origins. 
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

<!--quoteend-->

> Strategies used by libertarian socialists to build dual power include:
> 
> -   [[Mutualism]] – building alternative economies through co-operatives, credit unions and local purchasing.
> -   [[Municipalism]] – building popular assemblies to make decisions at the community level.
> -   [[Syndicalism]] – building revolutionary trade unions to confront management in the workplace.
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)


## As per Cooperation Jackson

[[Cooperation Jackson]] consider dual power part of their strategy.  Their usage of the term seems kind of in line with the libertarian socialist definition, i.e. developing counterpower while still engaging with state instituations where it is strategic to do so. 

> [[MXGM]] firmly believes that at this stage in the struggle for Black Liberation that the movement must be firmly committed to building and exercising what we have come to regard as “dual power”—building autonomous power outside of the realm of the state (i.e. the government) in the form of People’s Assemblies and engaging electoral politics on a limited scale with the expressed intent of building radical voting blocs and electing candidates drawn from the ranks of the Assemblies themselves.
> 
> &#x2013; [[Jackson Rising]]

<!--quoteend-->

> As we have learned through our own experiences and our extensive study of the experiences of others that we cannot afford to ignore the power of the state.
> 
> &#x2013; [[Jackson Rising]]

<!--quoteend-->

> It is this combination of building and exercising dual power—building autonomous People’s Assemblies and critical engagement with the state via independent party politics—that are the two fundamental political pillars of the Jackson Plan.
> 
> &#x2013; [[Jackson Rising]]


## As per communalism

See [[Communalism]].

