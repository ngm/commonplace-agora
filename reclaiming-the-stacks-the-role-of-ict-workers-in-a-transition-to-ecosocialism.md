# Reclaiming the stacks: the role of ICT workers in a transition to ecosocialism

[TODO: this is half finished and was probably superceded by [[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]]. Perhaps delete this?]

Multiple impending environmental crises and escalating global social inequalities are the pressing concerns of our time. Environmental politics aims to address the former, while socialism aims to address the latter.  Ecosocialism, a merging of socialist and environmental politics, aims to address both.  It strives for equity of resources and a fair standard of living for all, all while staying safely within planetary boundaries.

ICT underpins much of the infrastructure and operation of modern society.  As a sector of industry, it has a significant environmental impact of its own; it also supports other industries; and it has an ever-increasing influence on how society communicates and is organised.

The way in which society uses ICT will undoubtedly play a role in how we transition to a more sustainable and equal world.  I will explore the broad theme of how technology and ecosocialism relate in general, and will narrow down on specifically how ICT and ecosocialism relate, and the ways in which ICT workers could contribute to a transition to an ecosocialist society.


## Capitalism and environmental breakdown

Our present conjuncture is the outcome of technology used in a particular way in the capitalist system. ([[Twenty-First Century Socialism]])

The IPCC reports show that the multiple environmental crises are the results of human activity.  This has been referred to as the Anthropocene.  Others go a step further and refer to this as the Capitalocene.  The environmental crises are as a result of capitalism and growth ([[Less is More]]).

We need alternatives which move away from the growth imperative of capitalism.

> twenty-first-century socialism is the only reasonable solution to the various crises and problems that the world faces today – from social inequality to climate breakdown.
> 
> &#x2013; [[Twenty-First Century Socialism]]


## Socialism

Socialism is the alternative to capitalism.  Cooperation, decommodification, democratic participation, and equality. ([[Twenty-First Century Socialism]])


## ICT and technology

The cybernetic revolution, digital revolution, 3IR, whatever you want to call it.  The big tech firms have built their empires on top of this.  They're now trying to extend it with 4IR technologies.


## Big tech, the stacks, platform capitalism

Big tech are the current apex of capitalism.  They control vast tracts of society, through software, hardware, networks, supply chains and manufacturing.  These are sometimes referred to as the Stacks, sometimes as platform capitalism.  Also vectoralism.

To move from capitalism to socialism, a part of this must be to reclaim the stacks for socialist purposes.


## Digital socialism

How to reclaim them?  One multi-pronged approach is to resist, regulate and recode. ([[Platform socialism]])

Resiting is things like unionisation and strikes.  Regulation is things like anti-trust, anti-monopolisation, enforcing interoperability, etc.  Recoding is building alternatives that can be used instead of the big tech offerings.

Democracy, self-governance and decentralisation are parts of this.  Commoning are parts of this.  FLOSS.


### Resist


### Regulate


### Recode

Design justice and Technology Networks are recommended as parts of this in order to be inclusive ([[Internet for the People]]).  See [[British Digital Cooperative]] as well.


## Ecosocialism

Ecosocialism is the marrying together of socialist and environmental politics.


## Other approaches to sustainable development

In a mapping of approaches to sustainable development, (, ) outline three main areas: 'Status Quo', 'Reform' and 'Transformation'.  Ecosocialism is placed within the Transformation area.


## Digital ecosocialism

Kwet's stuff.  He takes some of it a step further?  It's essentially decommodification and decentralisation though, so not that different.  He identifies big tech as major environmental criminals, and says 'stay within planetary boundaries'.  Not too many specifics.

