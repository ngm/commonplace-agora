# subsidiarity

> local, regional, and global decisions made at the levels where their effects are felt most
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

<!--quoteend-->

> services should be delivered by the most local and proximate level that would be able to undertake the task efficiently, sustainably and in a manner that would maximise its benefit for users.
> 
> &#x2013; [[Platform socialism]]

