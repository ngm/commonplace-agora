# Is the IndieWeb too hard to use?

> Diversity is absolutely a problem in tech, but IndieWeb folks are, from my experience, absolutely doing what they can to rectify that; bringing in people from all sorts of backgrounds, trying to boost the minority voices, and being supportive of everyone who is trying to make the world, or at least the Internet, a better place. 

This is a really good article by Fluffy on the state of the [[IndieWeb]] and making it more accessible for wider adoption.  Just because we're not there yet, doesn't mean that we're not trying.

https://beesbuzz.biz/blog/3876-Incremental-progress

