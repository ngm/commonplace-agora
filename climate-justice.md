# climate justice

A type of [[climate action]].  Specifically focused on avoiding inequitable outcomes of climate change.

e.g. mitigating climate change is no good if you only mitigate it for rich Westerners.

