# Choose Commons-Friendly Financing

A pattern for [[commoning]].  In the [[Peer Governance]] sphere of commoning.

In [[Free, Fair and Alive]] it's referred to as [[Finance Commons Provisioning]], but 'Choose Commons-Friendly Financing' is used in the Pattern Language wiki: http://patternlanguage.commoning.wiki/view/-choose-commons-friendly-financing

I prefer this name, I think it places more emphasis on only looking at the finance options if you need them.


## Reduce the need for finance

You want to try and reduce the necessity of money in the first place.  

> The pursuit of money and market success, usually seen as a necessary path to community well-being, too often ends up subordinating it to outside markets and capital.
> 
> &#x2013; [[Free, Fair and Alive]]

Don't let it dominate the dynamics of the commons.

> the health of any commons depends upon preventing money from dominating its social dynamics. If debt or capital compromise people’s independence or sow social divisions, a commons will likely fall apart. It is therefore helpful for commoners (or anybody) to become less dependent on money and markets. 
> 
> &#x2013; [[Free, Fair and Alive]]

And you want to favour peer-driven approaches over outside investment to keep the value in the commons.

> Peer-driven approaches enable commoners to recirculate the value that they create for mutual benefit, rather than letting creditors or outside equity holders siphon value away in the form of interest or dividend payments.
> 
> &#x2013; [[Free, Fair and Alive]]

In the book they look at:

-   [[Money-lite commoning]] to reduce the need for money and markets
-   [[Collaborative financing]] that lets commoners create and circulate money or credit for themselves, wholly from within their commons
-   [[Public-commons circuits of finance]] which allow taxpayer funds administered by government to be used to support commons

