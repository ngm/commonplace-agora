# Changes to councils in Cumbria

There's going to be two unitary authorities.

> The east of the county will have one authority for Barrow, Eden and South Lakeland, while the west will encompass Allerdale, Carlisle and Copeland.
> 
> &#x2013; [[Cumbria councils to be replaced by two authorities]]

The two unitary authorities are going to be called [[Westmorland and Furness]] and [[Cumberland]].  South Lakeland will be in Westmorland and Furness.

[[Cumbria County Council]] will go, too.

> Opponents of the reorganisation have claimed that the proposal is being pursued to benefit the electoral prospects of the Conservative Party. 
> 
> -   [Westmorland and Furness - Wikipedia](https://en.wikipedia.org/wiki/Westmorland_and_Furness)

<!--quoteend-->

> Opponents believe the government's decision to create two authorities is aimed at consolidating Conservative power in the north west of the county.
> 
> -   [Cumbria County Council launches legal action over shake-up - BBC News](https://www.bbc.co.uk/news/uk-england-cumbria-59007448)

I would have liked the mooted Morecambe Bay Council.

> In much of England, a two-tier system involves county councils providing education, social services and waste disposal with district councils responsible for rubbish collection, housing and planning.
> 
> In areas governed by unitary authorities there is a single body which provide all local government services.
> 
> &#x2013; [[Cumbria councils to be replaced by two authorities]]

