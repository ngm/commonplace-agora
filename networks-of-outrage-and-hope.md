# Networks of Outrage and Hope

A
: [[book]]

URL
: https://www.wiley.com/en-us/Networks+of+Outrage+and+Hope%3A+Social+Movements+in+the+Internet+Age%2C+2nd+Edition-p-9780745695761

Author
: [[Manuel Castells]]

[[Networks]] of [[outrage]] and [[hope]].

I got a copy from the Oxfam charity shop in Preston.

