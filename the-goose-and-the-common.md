# The Goose and the Common

A protest rhyme from the 17th century, decrying the [[enclosure of the commons]].

<div class="verse">

They hang the man and flog the woman<br />
Who steals the goose from off the common<br />
Yet let the greater villain loose<br />
That steals the common from the goose<br />

</div>

http://unionsong.com/u765.html

