# Problems with an old mouse

Mouse keeps disconnecting on Linux.

It's a Trust mi-2510t wired USB mouse.

/var/log/syslog shows stuff like:

```nil
Feb 20 11:38:54 anarres kernel: [ 5257.705919] usb 2-1: Device not responding to setup address.
Feb 20 11:38:54 anarres kernel: [ 5257.941808] usb 2-1: device descriptor read/all, error -71
```

