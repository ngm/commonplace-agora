# Productivity

> Productivity is not about optimizing every aspect of your life or being well-versed on the latest and most flashy new app. The point of productivity is to do what brings you pleasure and to have more freedom.
> 
> &#x2013; [Feminine Energy: What Productivity is Missing - Forte Labs](https://fortelabs.co/blog/feminine-energy-what-productivity-is-missing/)

