# tech exceptionalism

> “Tech exceptionalism” is the sin of thinking that the normal rules don’t apply to technology.
> 
> &#x2013; [[The Internet Con]]

