# Hunger

> The [[global food system]] is complex and fragile – and the impacts when it fails are deadly. Despite more than enough food being produced to feed the world, there has been a six-fold increase in the number of people living in famine-like conditions since 2020.
> 
> &#x2013; [Tuesday briefing: Why hunger is as big a threat as Covid](https://www.theguardian.com/world/2022/aug/02/tuesday-briefing-hunger-food-security-global-food-crisis-ukraine) 

<!--quoteend-->

> Vidal rejects the framing of this as a crisis of food, of which there is plenty. In fact, food production has been rising steadily for decades. “It’s really a crisis of poverty and distribution – people cannot afford to buy food that is there,” he says.
> 
> &#x2013; [Tuesday briefing: Why hunger is as big a threat as Covid](https://www.theguardian.com/world/2022/aug/02/tuesday-briefing-hunger-food-security-global-food-crisis-ukraine) 

<!--quoteend-->

> To properly tackle the crisis of hunger, the global food industry has to be diversified so poorer countries aren’t held hostage by a few massive companies and can rely on their own food supplies to feed their people
> 
> &#x2013; [Tuesday briefing: Why hunger is as big a threat as Covid](https://www.theguardian.com/world/2022/aug/02/tuesday-briefing-hunger-food-security-global-food-crisis-ukraine) 

