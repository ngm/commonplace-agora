# Any society in which people must rely on money and markets for the essentials of life is not a communist society

A
: [[claim]]

Essentials being things like food, housing, clothing, education, healthcare, etc.

> There is generally consensus that any society in which people must rely on money and markets for the essentials of life (things like food, housing, clothing, education, healthcare, etc.) would not in any way be communist.
> 
> &#x2013; [[Forest and Factory]]


## Because

-   [[Markets don't give you what you need, they give you what you can afford]]


## Relatedly

> Beyond this, there are (very loosely) two schools of thought.
> 
> &#x2013; [[Forest and Factory]]

