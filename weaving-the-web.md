# Weaving the Web

> Berners-Lee's creation was fueled by a highly personal vision of the Web as a powerful force for social change and individual creativity. 
> 
> &#x2013; [Berners-Lee: Weaving the Web](https://www.w3.org/People/Berners-Lee/Weaving/)

[[World Wide Web]].

