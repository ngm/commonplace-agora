# digital capitalism

[[Digital]] [[Capitalism]].


## What is it?

What is digital? What is capitalism?

Digital can span a lot of things. But in this context I take it to be a shorthand for [[ICT]], so for starters let's say it's computers, software, data, platforms and the internet and networking infrastructure.

Capitalism is many things too, but for starters let's say it is private ownership of the means of production, with the intent of capital accumulation.

Digital capitalism then might be the private ownership of the means of production via digital technologies.

So really the 'digital' modifier is focusing the analysis on a particular set of forces of production and relations of production.

And then clearly the [[Big Tech]] firms are the arch digital capitalists, as they are the ones who currently are at the commanding heights of the economy, by virtue of their control of computers, software, data, platforms, the internet, and networking infrastructure.


## What does it do?

Different conceptions of it, e.g.  [[Surveillance capitalism]]; [[Digital colonialism]]; [[Techno-feudalism]], but all essentially describing the actions of Big Tech.

-   Destruction of the environment
-   [[Digital colonialism]]
-   Labour exploitation and alienation
-   Monopoly power
-   [[Algorithmic bias]]
-   Surveillance advertising
-   Persuasion
-   [[Technological solutionism]]


## Why does it matter?

Big Tech are the biggest corporations in the world.  They are at the commanding heights of the economy.  If we want to deconstruct capitalism and transition to something else, then we need to understand and deconstruct Big Tech / digital capitalism.

> Digital capitalism is a reality nowadays. Understanding its architecture, its core tools and how it affects us is essential to construct a different world which is more equal and just and less exploitative and alienating and one that fits into planetary environmental boundaries.
> 
> &#x2013; [[Digital Capitalism online course]]


## My experience with digital capitalism

I work as a software developer, and I have loved free and open source software for a long time, getting hooked on Linux at university.  The antagonism towards proprietary software in the FLOSS world introduced me to an early notion of struggling against capitalism in a digital form. Back then the main protagonist was Microsoft.

Over time I came to understand libre software as an example of digital socialism. But I have also learned that there is more to digital capitalism than simply proprietary code.  I was enthusiastic about Google at first, for example, as they embraced open source.  Yet over time it became clear that you can still be at the commanding heights of digital capitalism while using and creating free software.

Similarly, I have been disappointed as the folksonomy of Web 2.0 has morphed into platform capitalism, and decentralised technologies and work have become enclosed (e.g. Git and Github).

In recent years I've been increasingly concerned about the environmental impact of digital technology - from the intense energy use of data centres, to the resource extraction and e-waste associated with digital products.


## My biggest concerns with digital capitalism

My biggest concerns with digital capitalism are its destructive impact on the environment and its exploitation of human labour.

Digital capitalism makes a huge contribution to environmental degradation and climate change. For example, the constant manufacture and upgrade cycle of the devices that power digital capitalism (from phones to computers to servers) requires a vast amount of mining of resources.  The e-waste that comes out of the other end is the fastest growing waste stream.  The endless growth of digitalisation also requires immense computational resources, which requires huge amounts of energy and water.

Digital capitalism also exploits people around the world.  Labourers in mining and manufacturing are exploited mercilessly in the pursuit of profit.  End-users of digital products are pushed to become addicted to their devices, to become hostile and polarised online, and to disengage from the civic sphere.

I believe that digital technologies can be truly liberatory, but only when the means of production are socialised and taken out of the hands of the monopolistic cabal of Big Tech firms.

