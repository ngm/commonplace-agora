# Half-Earth

> The concept of Half-Earth comes from [[E. O. Wilson]], an entomologist whose research has shown the need to rewild half of the planet to staunch the haemorrhaging of biodiversity
> 
> [[Half-Earth Socialism]]

