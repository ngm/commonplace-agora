# ChatGPT Will Command More Than 30,000 Nvidia GPUs: Report

URL
: https://www.tomshardware.com/news/chatgpt-nvidia-30000-gpus

[[ChatGPT]] will command more than 30,000 [[Nvidia]] GPUs.

> Artificial Intelligence (AI) will be one of Nvidia's biggest income generators, according to the latest TrendForce projection.

<!--quoteend-->

> The research firm estimates that OpenAI's ChatGPT will eventually need over 30,000 Nvidia graphics cards.

<!--quoteend-->

> Nvidia has always had a knack for sniffing out gold rushes. The chipmaker was at the forefront of the cryptocurrency boom, pulling in record-breaking revenues from miners.

<!--quoteend-->

> The A100 costs between $10,000 and $15,000, depending upon the configuration and form factor. Therefore, at the very least, Nvidia is looking at $300 million in revenue.

<!--quoteend-->

> During the cryptocurrency bonanza, miners bought every graphics card in sight, helping to contribute to the graphics card shortage.

