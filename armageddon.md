# Armageddon

Big-budget, big cast, completely stupid disaster movie.

A bunch of Americans of dubious moral character are the only hope to save the planet from a stray asteroid.

They don't like environmentalists, and they don't like paying taxes.  They've no time for all these scientists' horseshit.  They bend the rules, break stuff, get the job done.  They are American heroes!

Directed by Michael Bay, produced by Jerry Bruckheimer, enough said.

