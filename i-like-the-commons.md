# I like the commons

[[I like]] the [[Commons]].

Because&#x2026;

-   [[Commoning represents a profound challenge to capitalism]]
-   [[A commons is a politics of belonging]]
-   [[The commons enables people to enjoy freedom without repressing others]]
-   [[The commons enables people to enact fairness without bureaucratic control]]
-   [[The commons enables people to foster togetherness without compulsion]]
-   [[The commons enables people to assert sovereignty without nationalism]]


## Epistemic status

Type
: [[feeling]]

Strength
: 9

How well I could explain why
: 6

