# Google won’t repair cracked Pixel Watch screens

URL
: https://www.theverge.com/23874281/google-pixel-watch-cracks-no-repairs-warranty
    
    > Just as California passes a new right-to-repair act, Google has confirmed it currently offers no repair options if your Pixel Watch screen cracks.

