# technofeudalism

A phrase from Yanis Varoufakis.  (originally?)

The book on the topic: [[Technofeudalism: What Killed Capitalism?]]

> Capitalism is dead. The #1 bestselling economist shows how the owners of big tech have become the world's feudal overlords.

I really hope he references [[McKenzie Wark]] ([[Capital is Dead]]).

> It might look like a market, but Varoufakis says it’s anything but. Jeff (Bezos, the owner of Amazon) doesn’t produce capital, he argues. He charges rent. Which isn’t capitalism, it’s feudalism. And us? We’re the serfs. “Cloud serfs”, so lacking in class consciousness that we don’t even realise that the tweeting and posting that we’re doing is actually building value in these companies.
> 
> &#x2013; [['Capitalism is dead. Now we have something much worse': interview with Yanis Varoufakis]]

