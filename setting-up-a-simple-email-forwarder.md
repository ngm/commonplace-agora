# setting up a simple email forwarder

I wanted a simple way to get emails sent to one account but forwarding to a couple of different email accounts.  Also using a decent, ethical service.

I kind of thought it was a standard feature, but turns out not.

I looked at [[Tutanota]], [[ProtonMail]] and [[FastMail]].

-   Tutanota you can a free account, but it doesn't do mail forwarding - https://www.reddit.com/r/tutanota/comments/acaush/forwarding_emails/
-   ProtonMail you can get a free acount, but it doesn't do mail forwarding - https://www.reddit.com/r/ProtonMail/comments/iv3and/a_way_to_forward_emails_from_protonmail_to/
-   FastMail does mail forwarding, but you have to pay for an account.

But then I realised, why not see if [[YunoHost]] does it?  I already have YunoHost set up, and I know it does email, I just don't use it for anything as I've already got an email provider.

And lo and behold, it does!

It's unbelievably easy to set up.  The mail stuff just works out of the box (I think, anyway - maybe I did something when I first set up YunoHost, but nothing too fancy.)

Then all I had to do was create a new user account in YunoHost, then edit that user and add a couple of email forwarding addresses to that user account.

And it all worked, c'est super!

