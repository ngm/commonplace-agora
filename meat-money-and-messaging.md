# Meat, money and messaging

[[Meat industry]]

https://www.sciencedirect.com/science/article/abs/pii/S0306919222000173?dgcid=author

first peer-reviewed and systematic analysis of how the meat industry frames the issue.

> The meat industry fosters uncertainty about scientific consensus and casts doubt over the reliability of both researchers and the evidence, a technique that has been employed by the tobacco, fossil fuel and alcohol industries

<!--quoteend-->

> cherry-picking and misrepresentation of evidence was seen

