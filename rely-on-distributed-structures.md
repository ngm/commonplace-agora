# Rely on Distributed Structures

> This means that the (infra)structures should enable peers, teams, and local nodes to interconnect and form semi-autonomous spheres of self-provisioning and -governance. Each part of the whole can then operate semi-autonomously, according to its own distinct rules and situational needs, while also coordinating with their other semi-autonomous peers.
> 
> &#x2013; [[Free, Fair and Alive]]

