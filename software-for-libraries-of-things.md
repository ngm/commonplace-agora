# Software for libraries of things

Software to help you run your [[library of things]].

The two main ones seem to be [[Lend Engine]] and [[MyTurn]].

Snipe IT is an open source asset management tool that might work&#x2026; but it's not its main focus.

