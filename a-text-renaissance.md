# A Text Renaissance

&#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

> The text renaissance is an actual renaissance. It’s a story of history-inspired renewal in a very fundamental way: exciting recent developments are due in part to a new generation of young product visionaries circling back to the early history of digital text, rediscovering old, abandoned ideas, and reimagining the bleeding edge in terms of the unexplored adjacent possible of the 80s and 90s.


## roam

> Roam attempts to implement a near-full conception of hypertext as originally conceived by visionaries like Vannevar Bush and Ted Nelson.

<!--quoteend-->

> It implements a few key features of 1980s vintage hypertext visions — block-level addressability, transclusion (changes in referenced blocks being “transfer-included” wherever they are cited), and bidirectional linking — that utterly transform the writing experience at the finger-tips level. You end up organizing high-level structure as you work at fleshing out low-level chunks of information, because the UX collapses high and low-level thinking into a single behavior. 

<!--quoteend-->

> It can also do note-taking, workflows (like kinda-sorta competitor Notion), and wiki-like knowledge management, but those use cases are not as interesting to me. Conspiracy theories and extended universes, in the best senses of those terms — escaped reality construction might be the general category — is what Roam wants to be about. 


## substack/email

> Email today is now less a communications medium than a communications compile target. It’s a clearinghouse technology. It’s where conversations-of-record go, where identity verification happens, where service alerts accumulate, and perhaps most importantly for publishers, where push-delivered longform content goes by default. It is distributed and federated, near universal, and is not monopolized by a single provider. Now that a constellation of other product categories have hived off around it in the last two decades, it’s finally finding its own true nature.

Don't agree that RSS is dead.  (At least, I don't _want_ it to be).  I would prefer to subscribe than to give away my email and be pushed to.

Not a huge fan of the idea of Substack - charging people for a regularly email newsletter.  In general, I'd prefer ideas shared freely, and the means of living provided some other way.


## Static websites

> I see several promising young writers already moving away from the blog as the main vehicle for online written expression, and building bespoke sites with weirder, more experimental structures, using Gatsby and its kin as the foundation. 

<!--quoteend-->

> But it’s going to get easier. Much easier. And then Interesting Times will ensue. The WordPress monoculture will start to shift and many more weird structures will begin to flourish.

Makes me think of Kicks and href hunt, looking for weird and interesting perosnal sites.


## Threaded Twitter

