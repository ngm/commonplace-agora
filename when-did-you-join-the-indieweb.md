# When did you join the IndieWeb?

I think I first **heard** of the [[IndieWeb]] movement in 2016 sometime.  It's a bit hazy now, but I've a memory that I stumbled on it through a trail of links starting on [[Wikity]].

I've tinkered with web sites in some form or another for a long time, and I have happy memories of various weird Geocities experiments, sadly lost to the sands of time it seems.

As far as independently hosting goes, I appear to have had a self-hosted blog since 2006 at my old domain noodlemaps.net - thanks Wayback Machine for that!  My first self-hosted post is seemingly on the topic of [data sonification and playing non-audio files through /dev/dsp on Linux](https://web.archive.org/web/20060822085346/http://blog.noodlemaps.net/index.php?/archives/1-Art-in-Linux.html).  Excellent.

I've had a self-hosted blog up at doubleloop.net since around [2013](https://web.archive.org/web/20160823223823/http://doubleloop.net/blog/archives) (thanks again Internet Archive).  I went through a through static site generators.  I actually really like how my old Hexo-based site looked.  Better than my current site&#x2026;

My first documented attendance of HWC London looks to be [December 2016](https://indieweb.org/events/2016-12-14-homebrew-website-club), hosted by [Calum](https://calumryan.com) and [Barry](https://barryfrost.com) who were really welcoming.  Around then, I fiddled about with various platforms before switching over to WordPress.

I've really enjoyed being part of the IndieWeb community.  My active involvement (at events, on IRC, etc) has peaked and waned over the years, but I've still always felt part of the bigger whole.  That's another post, though.

How did you find out about the IndieWeb community?

