# Walter Rodney

> The postwar order thus remained extractive, premised on grabbing resources from nature and the global South.  Such an understanding is central to the 1970s writings of Walter Rodney, which I would see as a key component in a truly [[anti-racist systems theory]]
> 
> &#x2013; [[For an anti-colonial, anti-racist environmentalism]]

