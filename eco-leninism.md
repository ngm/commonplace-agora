# Eco-Leninism

> attempts to read the [[climate crisis]] through the insights of [[Lenin]]
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

See also [[Climate Leninism]].

Proposed by [[Andreas Malm]]?  Associated with it recently at least.

> Andreas Malm the Swedish academic has argued that to overcome the climate crisis we need to return to Lenin. He has argued that the urgency of the climate crisis might mean embracing an approach similar to the war communism of the early years of the Soviet Union
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> Leninism is, out of a number of important Marxist contributions to ecological debates, to my mind potentially the most important. Lenin’s contribution was to investigate how in a specific context we make revolution. There is a growing awareness that capitalism is the key driver of climate change and other ecological ills. Transforming society and transcending capitalism can be seen as essential to human survival, the critical investigation of how we do so can be advanced by an open reading of Lenin’s words and work.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> Given the necessity of systemic change in the face of climate breakdown, it was only a matter of time before thinkers would look to the Russian revolution for insights.
> 
> &#x2013; [[Moving towards an ecological Leninism]]

<!--quoteend-->

> Over the last year, ecological Leninism has burst onto the scene in the works of several authors. Andreas Malm’s latest book [[Corona, Climate, Chronic Emergency]] focuses on the concept, Jodi Dean and Kai Heron have written of a ‘[[climate Leninism]]’, and Derek Wall has written of Lenin’s importance to environmental movements in his latest book, [[Climate Strike]].
> 
> &#x2013; [[Moving towards an ecological Leninism]]

<!--quoteend-->

> Ecological Leninism must be more than a theoretical extension of ecological Marxism, it must indicate the practices which a revolutionary eco-socialist movement are to adopt. Crucially, it also requires getting involved in actually existing movements, as there is no such thing as armchair Leninism.
> 
> &#x2013; [[Moving towards an ecological Leninism]]

