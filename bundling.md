# bundling

> This means that if, for example, the bearings in your washing machine fail, you’ll still have no choice but to replace the entire drum, with costs comparable to replacing the machine. Similarly, when the heater in a dishwasher fails, you’ll have to replace the entire heat pump, doubling the cost of repair, even though the pump may still be working.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

