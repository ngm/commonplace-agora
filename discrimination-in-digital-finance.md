# Discrimination in digital finance

I don't know much about [[digital finance]].  My first thoughts are that it refers to [[cryptocurrency]] but it's probably more than that.

In general what I've seen of fintech (admittedly not that much), it comes across as brotech and not [[Liberatory technology]].  I wonder if there is [[liberatory fintech]]. 

I'll read [[vera]]'s notes on this node to get started&#x2026;

They mention cashapp and venmo too - I don't know too much about these.

Does online banking count as digital finance?  I can't remember the last time I went in to an actual branch of my nominally physical bank to be honest.

Added some definitions: [[digital finance]].

But yeah in general seems to have a wide range of description, from online banking to distributed ledgers.

How does it discriminate? One obvious act of discrimination I guess is the exclusion of those who aren't comfortable in the use of digital technology.  People may prefer to interact in person in a real branch, not fuck around with shitty bank websites.

On the other end, stuff like [[bitcoin]] say, you need to be pretty tech savvy to get started with it.  And to mine it in the first place, you have to have capital for hardware.

