# Importmagic and/or epc not found

[[Using Python in org]], I was getting the following message when entering a code block:

> Importmagic and/or epc not found. importmagic.el will not be working.

Some answers to the same issue here: https://stackoverflow.com/questions/49065606/how-to-fix-spacemacs-importmagic-and-or-epc-not-found

For me it specifically turned out to be that Emacs was looking at python2 rather than Python 3.

Check the value of `python-shell-interpreter`.  It was `python`.  I customised it to `python3` and that removed the issue.

