# Servers should provide a supporting role, not a central role

> Servers thus have a role to play in the [[local-first]] world — not as central authorities, but as “cloud peers” that support client applications without being on the critical path. For example, a cloud peer that stores a copy of the document, and forwards it to other peers when they come online, could solve the closed-laptop problem above.
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html) 

<!--quoteend-->

> The key difference between traditional systems and local-first systems is not an absence of servers, but a change in their responsibilities: they are in a supporting role, not the source of truth.
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html) 

