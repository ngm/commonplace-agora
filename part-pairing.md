# Part pairing



## What?

When parts of a device are tied to the device serial number, and what spare parts you can use with your device are then restricted via this mechanism.

Also known as [[serialisation]].

> Here is how it works: some **parts have a unique serial number, which is paired to an individual unit of a device using software.** If any of these parts need replacing during a repair, they will not be accepted unless remotely paired to the device again via software by the manufacturer. This approach could create major barriers to independent and self repair.
> 
> &#x2013; repair.eu newsletter July 2021

<!--quoteend-->

> The main challenge has been a system of software restrictions called parts pairing, which rely on **parts being paired with a device serial number**.
> 
> &#x2013; [[Oregon Just Struck a Blow to Parts Pairing and Won a Decade of Repair Support]]

<!--quoteend-->

> Then, when the device does not recognize the part, it may not work at all (for instance, the iPhone selfie camera) or will work with limitations (screens missing True Tone and Auto-Brightness; batteries missing battery health) or will work but have vague and misleading warnings (“unidentified battery” even when the part is an original Apple battery, taken from an identical same-model phone).
> 
> &#x2013; [[Oregon Just Struck a Blow to Parts Pairing and Won a Decade of Repair Support]]


## Why?

> an increasingly common practice used by manufacturers of smartphones and other electronic products to control who can and can't perform certain types of repairs.
> 
> &#x2013; repair.eu newsletter July 2021

Apple claims it is for safety, security and privacy reasons.

> They complained that doing so would result in risks to customer safety, security, and privacy, but they presented no explanation for how every other major smartphone manufacturer has been able to allow selfie camera replacements, screen replacements, and battery replacements without artificially limiting features or throwing up aggressive warnings
> 
> &#x2013; [[Oregon Just Struck a Blow to Parts Pairing and Won a Decade of Repair Support]]

