# Children of Men

One of my favourite films.  Just so well done.  So much detail.

I wrote this in January 2016 about it:

> A bit exaggerated, but not **entirely** unimaginable picture of the not-so-distant future, if the present took a darker path. In the midst of a massive global crisis, the world turns insular and closes its borders. England has become a fascist state, with anyone foreign treated as subhuman and deported. Underground groups fight against the system, but also amongst themselves. Bexhill is a squalid refugee camp policed by gun toting thugs (e.g. if EDL ran the UKBA). People in dire circumstances put their faith in uncertain ships to take them to a better place.
> 
> Children of Men. Great film. Hopefully not too prescient. The premise for the global crisis is far fetched (although, extrapolate the current Zika virus out a bit&#x2026;) but swap that idea out for some other vehicle of global instability (climate change anyone?), and you can watch Children of Men as a cautionary tale. To not just sit idly by and let the pernicious shit-stirring of the media get a toe hold in the nation's consciousness. You can laugh at the Daily Mail all day, but people actually read and believe that bullshit, and it's one of the most widely circulated propaganda devices in the UK. Disseminate information at every possibility to inform and debunk. Stick up for what's right, and never lose hope in humanity.

Also this article said it a lot better in December 2019: [Alfonso Cuarón’s Children of Men Is a Dystopian Masterpiece](https://www.vulture.com/2016/12/children-of-men-alfonso-cuaron-c-v-r.html?mid=fb-share-vulture)

