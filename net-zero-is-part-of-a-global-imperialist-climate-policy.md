# Net zero is part of a global imperialist climate policy

First encountered in [[Revolutionary Strategies on a Heated Earth]].

> Yet governments talk about “net zero emissions”. Behind this rhetoric lies a great red herring to which, unfortunately, the climate movement and left-wing parties sometimes succumb when they adopt the term unquestioned. “Net-zero” strategies involve appropriating and using vast areas of land in dependent, poor countries to absorb carbon emissions so that the large emitters in imperialist countries can avoid significant reductions of their own emissions.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

<!--quoteend-->

> “Green” capitalist modernization thereby intensifies neocolonial plunder and intra-imperialist rivalry which must be stopped. “Net zero” is part of a global imperialist climate policy. The climate movement needs to clarify and oppose this diversionary tactic.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

