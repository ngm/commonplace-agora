# Information Technology and Socialist Construction

Author
: Daniel E. Soros

[[information technology]]

> As Paul Sweezy ( 1971 : 108) explains, “Transitions from one social order to another involve the most difficult and profound problems of [[historical materialism]].”

-   [[Modern information technology is one of the main conditions for socialist revolution]].

> At this stage, it may be mentioned that some changes in the forces of production assist the proletarian movement, such as modern information technology, whereas other changes in the productive forces encourage collective class ruin, such as the development of nuclear weapons and ecologically destabilizing branches of production.

<!--quoteend-->

> If modern information technology is to play a central role in the establishment of socialism, then Adamovsky’s recognition of the fact that new possibilities are opening up that we were never able to imagine before lends great support to the reason provided in chapter 2 for Marx and Engels’s silence on the question of socialism. Realizing that the social forces of production had not yet matured, Marx and Engels could do little else but study the existing mode of production to lay the foundation for a future socialist revolution. Finally, Adamovsky ( 2008 : 356) includes a warning that Google-like sites help us fi nd each other, but they put a great power in corporate hands that can be used against us. The socialist state must, therefore, play a central role in ensuring that capitalists in this sector do not sabotage efforts to alter the mode of production

<!--quoteend-->

> As has been shown, modern information technology makes possible the communication of individual scales of value in a manner that is far more effective and thoughtful than the market mechanism.

<!--quoteend-->

> Information technology must be used to create a system of decentralized planning that serves as a substitute for the market mechanism as discussed in the previous chapter.

