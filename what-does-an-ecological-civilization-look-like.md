# What Does An Ecological Civilization Look Like?

URL
: https://www.yesmagazine.org/issue/ecological-civilization/2021/02/16/what-does-ecological-civilization-look-like

Author
: [[Jeremy Lent]]

A lovely article.  The [[ecological civilization]] is a great vision for society.  It incorporates many things that I am interested in.  The article could do more to envision how we actually get to it.  It does list some organisations working in the present system in ways that might get us closer to it.  I wonder if something more revolutionary might be needed though.

> We are rapidly approaching—if we haven’t already passed—climate tipping points with reinforcing feedback loops that would lead to an unrecognizable and terrifying world.

<!--quoteend-->

> As long as government policies emphasize growth in gross domestic product and transnational corporations relentlessly pursue shareholder returns, we will continue accelerating toward global catastrophe.

<!--quoteend-->

> We’re rapidly decimating the Earth’s forests, animals, insects, fish, fresh water—even the topsoil we need to grow our crops. We’ve already transgressed four of the nine planetary boundaries that define humanity’s safe operating space, and yet global GDP is expected to triple by 2060, with potentially calamitous consequences.

<!--quoteend-->

> When Lakota communities, on the land that is now the U.S., invoke Mitakuye Oyasin (“We are all related”) in ceremony, they are referring not just to themselves but to all sentient beings. Buddhist, Taoist, and other philosophical and religious traditions have based much of their spiritual wisdom on the recognition of the deep interconnectedness of all things.

<!--quoteend-->

> There is a secret formula hidden deep in nature’s intelligence, which catalyzed each of life’s great evolutionary leaps over billions of years and forms the basis of all ecosystems. It’s captured in the simple but profound concept of mutually beneficial symbiosis: a relationship between two parties to which each contributes something the other lacks, and both gain as a result.

<!--quoteend-->

> Nature uses a fractal design with similar patterns repeating themselves at different scales.
> ● fractal has parsllels to multi scsle?

<!--quoteend-->

> Currently, the success of political leaders is assessed largely by how much they increase their nation’s GDP, which merely measures the rate at which society transforms nature and human activities into the monetary economy, regardless of the ensuing quality of life.

<!--quoteend-->

> Contrary to the widespread view that an entrepreneur who becomes a billionaire deserves his wealth, the reality is that whatever value he created is a pittance compared to the immense bank of prior knowledge and social practices—the commonwealth—that he took from.

<!--quoteend-->

> Dare to make it possible by the actions you take, both individually and collectively—and it might just happen sooner than you expect.

● we need something bolder than this i feel.

> Daring to Make It Possible

● link all of the different orgs mentioned here.

