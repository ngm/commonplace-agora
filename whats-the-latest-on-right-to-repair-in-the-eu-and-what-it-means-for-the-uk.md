# What's the latest on Right to Repair in the EU? And what it means for the UK

An
: [[article]]

Found at
: https://therestartproject.org/news/new-eu-legislation/

> by asking that [[repair prices]] are “reasonable”, it helps to not deter people from repairing. But we’ll have to see how this plays out in practice, as reasonable is not easy to quantify.

<!--quoteend-->

> A second massive barrier is the use of [[parts-pairing]] to limit the use of second-hand or compatible spare parts. The directive should tackle this aspect, but we know the devil is in the details and we suspect manufacturers will try to find loopholes

