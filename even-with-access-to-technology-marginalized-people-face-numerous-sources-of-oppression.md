# Even with access to technology, marginalized people face numerous sources of oppression

from [[Technology of the Oppressed]]

Digital access does not remove technological biases, racism, classism, sexism, censorship, etc.

