# An Ecological Technology: An Interview with James Bridle

URL
: https://emergencemagazine.org/interview/an-ecological-technology/

Transcript of an interview with [[James Bridle]].

Mainly around topics covered in [[Ways of Being]].  But a little bit from [[New Dark Age]] too.

Although I am surprisingly uninterested in [[artificial intelligence]] as a topic these days, maybe it's just the general discourse around it.  Ways of Being does sound it'll be more up my street - comes across much more in line with an [[Evolutionary and adaptive systems]] vibe - i.e. importance of [[embodiment]], environment, [[relationality]].

Interested to learn more what Bridle means by [[Ecology of technology]].

