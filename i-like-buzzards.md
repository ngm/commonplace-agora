# I like buzzards

[[I like]] [[buzzards]].

They are probably my favourite bird, along with [[kingfishers]].


## Because

-   I've got lots of fond memories seeing them in the [[Lake District]] over the years.
    -   e.g. [[Buzzard from above]].

