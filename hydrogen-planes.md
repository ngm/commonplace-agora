# Hydrogen planes

> How do hydrogen planes work?
> 
> They look the same but inside they’ll be very different. Currently, planes use hydrocarbon fuel. When it burns, carbon dioxide is produced, which increases global warming. But with hydrogen, you are only producing water. 
> 
> [Do we need to stop flying to save the planet? We ask an expert | Air transpor&#x2026;](https://www.theguardian.com/lifeandstyle/2022/apr/15/do-we-need-to-stop-flying-to-save-the-planet-we-ask-an-expert)

