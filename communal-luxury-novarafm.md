# Communal Luxury - NovaraFM

URL
: https://novaramedia.com/2015/05/22/communal-luxury/

Publisher
: [[Novara Media]]

Interview with
: [[Kristin Ross]]
    
    > On this week’s show James Butler and Aaron Bastani are joined by Kristin Ross as they discuss her new book “Communal Luxury: The Political Imaginary of the Paris Commune”.

[[Paris Commune]].

It didn't just spontaneously appear - it had history in debating clubs etc.

Article excerpt from the book - [[Birth of the Commune]].

