# Capitalism is the root cause of the overshoot of planetary boundaries

[[Capitalism]] is the root cause of the [[overshoot of planetary boundaries]].


## Because

-   The endless accumulation dynamics of capital are in contradiction with planetary boundaries.

> The comprehensive ecological crisis is an expression of the contradiction between the planetary boundaries of growth and the endless accumulation dynamics of capital.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

-   The capitalist mode of production practices a social metabolism with nature that pushes it to disrespect planetary boundaries.

> capitalist mode of production practices a [[social metabolism]] with nature that pushes it to disrespect [[planetary boundaries]]
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

-   The treadmill of accumulation leads to a planetary overload.

> The ‘treadmill’ of accumulation leads to a planetary overload and to an “overall break in the human relation to nature arising from an alienated system of capital without end.”
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]


## Also

> This ecological rift is the result of a social rift: the domination of humans over humans.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

Sounds very similar to [[Bookchin]]. Interesting to hear that Marx said this also.  But yes capitalism could be described as the domination of humans over humans.

