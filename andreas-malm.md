# Andreas Malm

> In Corona, Climate, Chronic Emergency, Andreas Malm suggests that neither anarchist horizontalism nor social democracy is capable of decarbonizing society fast enough to avoid the dire consequences of ecological collapse. Rehearsing a familiar Marxist critique of anarchism, Malm finds the tradition to be too decentralized, too opposed to programs, discipline, and the state’s potential as an instrument of revolutionary transition.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

See discussion in [[Neither Vertical Nor Horizontal]].

> If neither anarchism nor social democracy are up to the task, then what are we left with? Malm’s answer intends to provoke: eco-Leninism and [[war communism]]
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

