# Neofeudalism

> Over the past decade, “neofeudalism” has emerged to name tendencies associated with extreme inequality, generalized precarity, monopoly power, and changes at the level of the state.
> 
> &#x2013; [[Neofeudalism: The End of Capitalism?]]

-   Extreme inequality
-   Monopoly power
-   Generalized precarity
-   Changes at the level of the state

