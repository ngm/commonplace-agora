# What a waste: our study shows almost half of electricals sent for recycling could be reused

An
: [[article]]

Found at
: https://therestartproject.org/news/recycling-reusable-products/

[[The Restart Project]] set up in a Household Waste Recycling Centre (in Brent, London).

Assessed the reusability of products being brought in for recycling.

Shocking results: almost 50% could have an easy second life (either fully functional as is, or in need of only minor repair).

There needs to be less focus on recycling, and much more on reuse and repair.

Filtering and diverting initiatives should be mandatory.

