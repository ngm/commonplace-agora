# Coops and commoning

> [&#x2026;] [[co-ops]] formalized the ancestral practice of [[commoning]] into applicable legal structures offering islands of resistance in capitalist markets.
> 
> &#x2013; [[DisCO Manifesto]] 

Coops as a way to make money is a positive way under capitalism?  Commoning as way to change the system long-term?

