# Open Green Map

URL
: https://new.opengreenmap.org/about

> Designed for community mapping, this platform is ready for all kinds of mapmakers - from students to professionals - who want to draw attention to sustainability in their own city or town. 

