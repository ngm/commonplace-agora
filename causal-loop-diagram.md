# causal loop diagram

> Causal loop diagrams map the causal relationships between pairs of elements within a system and identify feedback loops. These loops can either be reinforcing (vicious cycle) or balancing (goal-seeking) and complex interactions between loops can lead to unintended consequences.
> 
> &#x2013; [Causal Loop Diagram](https://systemsthinkinglab.wordpress.com/causal-loop-diagram/) 

Causal loop diagrams are good for exploring a qualitative understanding of a system. Helping you understand the system and its dynamics.

You might take a CLD and build a stock and flow diagram from it to begin to get a quantitative understanding.


## Tools

-   https://kumu.io
-   Miro has a template
-   https://ncase.me/loopy/


## Guidelines

-   [The Systems Thinker – Guidelines for Drawing Causal Loop Diagrams - The Syste&#x2026;](https://thesystemsthinker.com/guidelines-for-drawing-causal-loop-diagrams-2/)

