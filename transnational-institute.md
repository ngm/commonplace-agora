# Transnational Institute

Found at
: https://tni.org

> The Transnational Institute (TNI) is an international research and advocacy institute committed to building a just, democratic and sustainable planet. For 50 years, TNI has served as a unique nexus between social movements, engaged scholars and policy makers.

