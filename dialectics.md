# Dialectics

> Dialectics was taken as the opposite of this, the comprehension of things in their connections, motions, histories and trajectories
> 
> &#x2013; [[Red Enlightenment: On Socialism, Science and Spirituality]]

<!--quoteend-->

> In fact, there was always, bubbling away as an undercurrent, a yearning to transcend the limitations of mechanistic thought. Dialectics is a term which I find really useful to encapsulate this, because it somehow brings together three important currents: (1) [[systems theory]], (2) [[traditional knowledge]]/belief, and (3) a non-mechanistic science premised on [[complexity]]
> 
> &#x2013; [[When nature and society are seen through the lens of dialectics and systems thinking]]

<!--quoteend-->

> the notion of contradiction, or the unity of opposites, defines both the essence of something, and its intrinsic propensity to change.
> 
> &#x2013; [[When nature and society are seen through the lens of dialectics and systems thinking]]

<!--quoteend-->

> In a sense, dialectics is about training the mind to operate like the natural system it in fact is: neurons form self-organising networks, the brain has no command centre, its structures are emergent from its own fabric; according to some arguments, the brain maintains itself at a point of ‘criticality’, namely the creative zone where phase transition is very close.  Thus, by embracing the concept of phase transition, dialectics is tapping-into the way mind itself physically works.
> 
> &#x2013; [[When nature and society are seen through the lens of dialectics and systems thinking]]

<!--quoteend-->

> “Dialectics is the self-consciousness of the objective context of delusion; it does not mean to have escaped from that context,” writes Adorno in Negative Dialectics. “Its objective goal is to break out of the context from within.”
> 
> &#x2013; [[The technologies of all dead generations]]


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, &amp; Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)

