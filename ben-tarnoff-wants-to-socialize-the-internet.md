# Ben Tarnoff Wants to Socialize the Internet

URL
: https://jacobin.com/2022/07/ben-tarnoff-wants-to-socialize-the-internet/

[[Socialize the internet]]. A review of [[Internet for the People]] by [[Ben Tarnoff]].

> When it comes to internet infrastructure, the for-profit model is presented as inevitable — but political decisions built today's internet, and political movements could build something different.

<!--quoteend-->

> Tarnoff offers hopeful green shoots of what a truly social internet could be, such as [[community broadband cooperative]]s — initiatives that put accessibility, cost, and performance before the needs of shareholders or unaccountable company boards.

<!--quoteend-->

> There is a call for the Left to engage in broad cooperation, drawing ideas from precedents like [[Community Wealth Building]] and the [[Technology Networks]] established in the ’80s by the left-wing Greater London Council.

