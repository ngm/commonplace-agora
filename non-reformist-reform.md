# non-reformist reform

An idea from [[André Gorz]].

AKA revolutionary reform, transformative reform, structural reform.

"cracks created in the system by attacks on its weak points"

Seems like a sensible approach to me.

> Gorz's formulation centers on struggling for demands and reforms that improve conditions in people's immediate lives while subverting the logic of the capitalist system, upending its social relations, and diluting its strength.
> 
> &#x2013; [[Jackson Rising Redux]]

<!--quoteend-->

> Gorz posed the formulation to bridge short-term engagements for social justice in everyday life with the longer-term vision for an anti-capitalist world.
> 
> &#x2013; [[Jackson Rising Redux]]

<!--quoteend-->

> Nonreformist reforms seek to create new logic, new relations, and new imperatives that create a new equilibreium and balance of forces to weaken capitalism and enable the development of an anti-capitalist alternative.
> 
> &#x2013; [[Jackson Rising Redux]]

<!--quoteend-->

> With non-reformist reforms, popular movements can win immediate gains that shift power away from elites — and clear the way for more radical transformations.
> 
> &#x2013; [[André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today]]

<!--quoteend-->

> Gorz proposed that through the use of “non-reformist reforms,” social movements could both make immediate gains and build strength for a wider struggle, eventually culminating in revolutionary change.
> 
> &#x2013; [[André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today]]

<!--quoteend-->

> “A non-reformist reform is determined not in terms of what can be, but what should be.”
> 
> &#x2013; [[André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today]]

<!--quoteend-->

> For Gorz, non-reformist reforms seek to undermine the established order. “Structural reforms should not be conceived as measures granted by the bourgeois State at the end of a compromise negotiated with it, measures which leave its power intact. They should rather be considered as cracks created in the system by attacks on its weak points,”
> 
> &#x2013; [[André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today]]

<!--quoteend-->

> The great strength of Gorz’s theory is not that it offers easy answers but that it provides a framework through which we can weigh the costs and benefits of pursuing any given demand or accepting any given compromise. It creates an orientation toward action that forces us to balance revolutionary vision with a hardheaded assessment of present conditions
> 
> &#x2013; [[André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today]]

Seems important.  I've heard [[Green New Deal]] mentioned in the context of non-reformist reforms.


## See also

-   [[Protopia]]?

