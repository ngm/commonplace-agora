# Digital garden

Recently-ish popular term for a kind of public personal PKM / wiki.  [[A Brief History &amp; Ethos of the Digital Garden]] is a great read to learn more about digital gardens.

Also see [[the Garden]] metaphor for some history.


## What

> **an online space at the intersection of a notebook and a blog**, where digital gardeners share seeds of thoughts to be cultivated in public. 
> 
> &#x2013; [How to set up your own digital garden - Ness Labs](https://nesslabs.com/digital-garden-set-up) 

-   a concept that describes the practice of maintaining and growing a collection of digital content, such as notes, ideas, and thoughts, in an organic and unstructured way.
-   often used as a creative tool for exploring, sharing, and developing ideas, and can be viewed as a more flexible and fluid alternative to traditional personal blogs or portfolios.
-   a way to document learning and growth over time, and a way to share thoughts and experiences with others.


### You mean a wiki, right?

> I think "wiki" is a term that focuses on a particular tool, whereas "digital garden" is a more user-intention high level phrase
> 
> &#x2013; https://chat.indieweb.org/dev/2021-11-26#t1637964070215800 


### You mean blogging, right?

Sounds a bit like blogging, no?

> I prefer to think of digital gardening as a new variation of blogging. Blogging that is:
> 
> -   Constantly evolving
> -   Less performative
> -   Community-focused
> 
> &#x2013; [🪴 Planting Your Digital Garden](https://www.conordewey.com/blog/on-digital-gardening/) 

<!--quoteend-->

> Contrary to a blog, where articles and essays have a publication date and start decaying as soon as they are published, a digital garden is evergreen: digital gardeners keep on editing and refining their notes.
> 
> &#x2013; [How to set up your own digital garden - Ness Labs](https://nesslabs.com/digital-garden-set-up) 


### You mean personal websites, right?

I tend to think of it more as that intersection of notebook/blog/wiki, but it is sometimes also framed as 'old school [[personal website]]'.

> A growing movement of people are tooling with back-end code to create sites that are more collage-like and artsy, in the vein of Myspace and Tumblr—less predictable and formatted than Facebook and Twitter. 
> 
> &#x2013; [Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/) 

<!--quoteend-->

> Digital gardens explore a wide variety of topics and are frequently adjusted and changed to show growth and learning, particularly among people with niche interests. 
> &#x2013; [Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/) 


## Why

> “With [[blogging]], you’re talking to a large audience,” he says. “With digital gardening, you’re talking to yourself. You focus on what you want to cultivate over time.”
> 
> &#x2013; [Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/) 

<!--quoteend-->

> Through them, people are creating an internet that is less about connections and feedback, and more about quiet spaces they can call their own.
> 
> &#x2013; [Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/) 

<!--quoteend-->

> “Gardens … lie between farmland and wilderness,” he wrote. “The garden is farmland that delights the senses, designed for delight rather than commodity.”
> 
> &#x2013; [Digital gardens let you cultivate your own little bit of the internet](https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/) 


## Why not

Should you really publish your half-baked notes-to-self to the Internet?

> To me that is unthinkable: my notes are an extension of my thinking and a personal tool. They are part of my inner space. Publishing is a very different thing, meant for a different audience (you, not me), more product than internal process. At most I can imagine having separate public versions of internal notes, but really anything I publish in a public digital garden is an output of my internal digital garden.
> 
> &#x2013; [100 Days in Obsidian Pt 6: Final Observations – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-6-final-observations/)  

<!--quoteend-->

> To be honest, I don’t see much appeal in publishing your entire unfiltered notes to the web. Synthesize interesting portions of them occasionally into coherent blog posts that other people can consume without digging through a forest of links, backlinks, and footnotes. 
> 
> &#x2013; [hpfr](https://news.ycombinator.com/item?id=26493416)


## You're probably already doing it

> Believe it or not, you've probably already started planting the seeds of your digital garden. **You don't necessarily need an organized wiki on your self-hosted personal site.** Posting on social media is still the most common form of digital gardening.
> 
> &#x2013; [🪴 Planting Your Digital Garden](https://www.conordewey.com/blog/on-digital-gardening/) 

Agree with that wholeheartedly.  Although the [[indiewebber]] in me says that if you're doing it on a big social media platform, it won't work out in the long run.


## Misc

-   The garden is more about the [[use-value]] of information, the stream more about [[exchange-value]] of information.


## Twin Pages

-   https://www.mentalnodes.com/a-gardening-guide-for-your-mind

