# The Weather Underground

A
: [[documentary]] [[film]]

About the [[Weather Underground]].

-   Radicalised by [[Vietnam War]].
-   Marches on Washington not working in their eyes.
-   They felt they were part of wider worldwide [[revolution]] at the time.
-   Wanted to overthrow [[capitalist]] system and put in place something more humane.
-   Grew out of [[Students for a Democratic Society]]. The Weathermen took it over.
-   Took their name from Bob Dylan song.
-   Aimed to establish themselves in [[working class]] neighbourhoods.
-   Smash monogamy.
-   Riots in Chicago - "Days of Rage" Black Panthers distanced from them.
-   Murder of [[Fred Hampton]]. Altamont.  Charles Bronson.  My Lai. The sixties was over.
    
    40:41.

