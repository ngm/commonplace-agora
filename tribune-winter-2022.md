# Tribune Winter 2022

Got some good stuff in there.  Editorial on [[Cost of living crisis]].  Article about [[New Towns]].  Article about socialist science-fiction - some good recommendations in there.  Article on successful [[trade unionism]], specifically in refuse workers sector - some organising to drive worker conditions back up.  Also something about [[Partygate]] being hopefully the end for Boris Johnson.  I think this article is a few months old, doesn't seem to have happened yet though.

> As the politically bankable public joy over the success of the Covid vaccine programme began to wane, the latest scandal was a reminder of who had been steering the country through this generational crisis: a prime minister who fundamentally did not believe he had any responsibility to the rules he set, and a man whose entire political career has been built on impunity

<!--quoteend-->

> North. There’s no cast-iron cohort of loyal MPs because there’s no such thing as a Johnsonite — only fair-weather fans

<!--quoteend-->

> Like many industries, the answer lies in the advent and development of neoliberalism throughout the 1980s. The dogma pushed by Margaret Thatcher and her successors in government (including New Labour) was one of ‘value for money’, with private sector ‘entrepreneurship’ being the main guarantor of efficiency in public services

<!--quoteend-->

> Those that worked through a pandemic and were lauded as key workers are now acutely aware of their importance to society and have no intention of letting the politicians and bureaucrats who decanted them into the hands of private companies off the hook

