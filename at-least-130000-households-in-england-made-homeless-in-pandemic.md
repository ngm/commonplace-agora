# At least 130,000 households in England made homeless in pandemic

URL
: https://www.theguardian.com/society/2021/jun/13/at-least-130000-households-in-england-made-homeless-in-pandemic

Publisher
: [[The Guardian]]

Summary
: 

> While ban on evictions protected some people, domestic abuse and loss of temporary accommodation were common triggers for [[homelessness]]

<!--quoteend-->

> “During the pandemic, the most common triggers for homelessness were no longer being able to stay with friends or family, losing a private tenancy, and domestic abuse.”

<!--quoteend-->

> The end of the [[eviction ban]] in England will undoubtedly have an impact on the number of people turning to the council for help, but we are yet to see the end result of this change in policy.

The eviction ban ended 1st June 2021 in the UK.

