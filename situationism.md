# Situationism

> &#x2026;the Situationist International (1957–1972), a group of avant-garde artists and political theorists united by their opposition to advanced capitalism. At varying points the group’s members included the writers Raoul Vaneigem and Michèle Bernstein, the artist Asger Jorn, and the art historian T.J. Clark. Inspired primarily by [[Dadaism]], [[Surrealism]], and [[Marxist]] philosophy, the SI rose to public prominence during the [[May 1968]] demonstrations during which members of the group participated in student-led occupations and protests. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> last gasp of alienation theory was the Situationists
> 
> &#x2013; [A Lot of People Don't Want to Win | James Butler Meets David Graeber - YouTube](https://www.youtube.com/watch?v=v7s-dn9P33k&feature=youtu.be) 

