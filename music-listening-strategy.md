# Music listening strategy

How to find and listen to music in a way that supports artists and gives the finger to the big corps.  

I guess three parts of it - discovery, support, and sharing.


## Why

I cancelled my Deezer subscription a long while back.  With the intention of going back to indie music discovery and support.

> Over the last 7 years, Spotify has earned me 4.3% of what Bandcamp has. Over the last 3 months it's 1.6%.
> Getting playlisted by the Spotify lords and heard by 50,000 people is worth as much as ~20 people deciding to pay a fiver for a release on Bandcamp.
> 
> [@datassette](https://twitter.com/datassette/status/1280417867679313920)


## Discovery

Can't beat curation in my opinion.  So either DJs, or friends recommendations.


### Streaming radio


#### BBC 6 Music


#### soma.fm

> listener-supported, commercial-free, underground/alternative radio broadcasting to the world

I subscribe to [soma.fm](https://soma.fm) and mainly listen to the cliq hop, DEF CON, and Drone Zone channels.

Question: does this support the artists?  Does soma pay it forwards - maybe there's some licensing fee?


## Support


### Bandcamp

Occassionally buy tracks from somewhere where artists get a good proportion of it.  e.g.  [[Bandcamp]].   Can't afford to buy every single track I like, so need to think of a way 


### Resonate

Stream from [[Resonate]] where the artists get much better pay than other platforms.


## Sharing

Post links to music that you like, where the link goes to somewhere like [[Bandcamp]] (not a random upload of the track on youtube).

