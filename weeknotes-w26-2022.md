# Weeknotes W26 2022



## Sunday

-   Started skim reading (ironically?) [[How To Take Smart Notes]].
    -   I've encountered most of the ideas before via the numerous articles and posts written about it.
    -   I need to spend more time turning literature notes / quotes from books in to my own understanding.

-   [[Social media is important because everything is social]].

-   Joined the [[Bonfire]] playground but haven't got time to do much on there at the mo.  But I really love the mission they have around social media so following with great interest.

-   Reading a bit on [[Antonio Gramsci]] apropos [[site of struggle]].

-   [[Patrice Lumumba]]
    -   Read: [Patrice Lumumba: Why Belgium is returning a Congolese hero's golden tooth](https://www.bbc.co.uk/news/world-africa-61838781)

-   [[There is no such thing as an ungoverned space]].


## Monday

-   [[Calculating effort estimates and scheduled times in org-mode]]
-   [[Emacs note taking systems]]


## Tuesday

-   Watching: [[Don't Look Up]]

-   I like Iain M. Banks and I'm currently re-reading [[Look to Windward]] because in [[Red Plenty]] it was described it as an example of a 20th century [[Marxian idyll]].  It's good and all but I'm reading the [[Culture]] now as kind of all premised on [[Prometheanism]] / [[fully automated luxury communism]].  I'm gonna reread [[A Wizard of Earthsea]] (last read as a young teen!) by [[Ursula K. Le Guin]] next as in [[Half-Earth Socialism]] they describe that as [[Jennerite ecological scepticism]] and that is more my bag lately.

-   I feel happy discovering more and more as I get older that sci-fi books I took off my Mum's bookshelf as a kid are in fact often allegorical for some kind of radical politics that I had no idea of at the time.

-   [[Scorched Earth: Beyond the Digital Age to a Post-Capitalist World]]


## Friday

-   Listened: [[Max Liboiron and Josh Lepawsky, "Discard Studies: Wasting, Systems, and Power"]]

-   Listened: [[Richard Seymour, "The Disenchanted Earth: Reflections on Ecosocialism and Barbarism"]]

-   [[Adding more storage space to YunoHost / NextCloud on Hetzner]]


## Saturday

-   Read: [[For a Red Zoopolis]]
    -   Very good.  On [[eco-socialism]].  I like the description of a what [[abundance]] could actually mean.
    -   Also introduced me to the idea of a [[Zoopolis]]&#x2026;

-   I have oscillatory waves of activity on my garden.
    -   Sometimes I have blocks of focus on the mechanics of how the garden works.
        -   All the PKM, zettelkasten, smart notes, kind of stuff.
    -   Then periods of time spent on the actual content.
        -   Reading, taking notes, writing about politics, technology, the environment, etc.
    -   Occasionally there is a moment of harmony between the waves where the two almost intersect.
        -   e.g. critical theory of social media, how digital gardens, the Agora, fit in to that, etc.

-   Chat with Flancian
    -   FlanciaCam
    -   Flancian tour of America
        -   Portland
        -   homelessness
        -   'illegal' encampment
            -   30,000 in San Francisco
        -   Housing First
        -   is homelessness a complicated or complex system
            -   if there are X people without homes, can we just give X homes to solve the problem?
                -   [[David Peter Stroh, "Systems Thinking For Social Change"]]
                    -   [[Adam Day, States of Disorder, Ecosystems of Governance]]
                    -   similar to [[Seeing Like a State]]
                -   too top down?
            -   c.f. [[universal basic income]]
                -   perhaps a simplistic approach, yet in present society money gives agency
    -   walking and meditation
        -   otter.ai
            -   what about libre alternatives?
    -   [[counterantidisintermediation]]

-   Listened: [[Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"]]
    -   More [[eco-socialism]].
    -   I listened while doing household chores so have less notes.  But it's a good podcast.
    -   Marxist approach - emphasis on class struggle, movement building, trade unionism.

