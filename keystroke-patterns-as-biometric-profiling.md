# Keystroke patterns as biometric profiling

I had not heard of this…

> The industry is also adopting various forms of [[biometric profiling]], including using keystroke patterns.  How we type is marked by minute differences, which can create a biometric profile of individuals…
> 
> &#x2013; [[Future Histories]]

I guess I’m lucky that for me it can be filed under ‘disturbing curiosity’ rather than ‘legitimate concern’.  But.  Honestly.  What a mess we’re in that this is actually a thing.

