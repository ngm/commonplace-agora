# social movement theory

[[Social movement]] theory.

First heard mention of this in [[Movement and Stillness]].  There [[Keir Milburn]] discusses:

-   David Easton and [[systems theory in political science]].
-   Black box model.
-   Robert Dahl and [[pluralism]] as an elaboration of this.
-   The social movement is part of the feedback within the social system.
-   [[Resource mobilisation theory]].
-   [[Repertoires of contention]].

