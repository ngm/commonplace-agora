# Places I like

-   Langdale Valley and the Langdale Pikes
-   View over Morecambe Bay from Heysham
-   Loughrigg Fell
-   [[The Cragg]]
-   the salt marsh on the way to [[Sunderland Point]]


## And some nice things&#x2026;

-   Murmuration of birds (which kind?) on Morecambe Bay

