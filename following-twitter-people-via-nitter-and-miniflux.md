# Following Twitter people via Nitter and Miniflux

So I use a self-hosted [[Nitter]] and self-hosted [[Miniflux]] combo to follow these feeds.

Nitter is an alternative front-end to Twitter, and very helpfully gives you an RSS feed of any account.  Miniflux is a minimal RSS reader.  So I just plop the Nitter RSS feeds in to Miniflux and read the local stuff there.

Both of them are installed via [[YunoHost]], which makes it a doddle to set up.


## Issues

Feed fetching from Nitter fails periodically.  But I just have to force refresh it a few times and it sorts itself out.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-21 Mon]</span></span>

Get 404s and 429s returned from Nitter via Miniflux fairly frequently.  I wonder if that's Nitter actually returning that, or Twitter?


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-19 Sat]</span></span>

Since the [[RSS-Bridge]] thing was never much of a success, I've installed [[Nitter]] on my [[YunoHost]] to get RSS feeds of people I want to follow on [[Twitter]].

I could add the RSS feeds to [[Aperture]], but I would feel kind of bad adding a feed for every single Twitter user I want, it's more load on the Aperture server.  

So I've also installed [[Miniflux]] via YunoHost too, interested to give that a pop as an RSS reader.

