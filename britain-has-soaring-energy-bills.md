# Britain has soaring energy bills

[[Britain]] has soaring [[energy bills]] in [[2022]].

> Households will face a record energy bill increase of 54% from April after the regulator lifted the cap on default tariffs to £1,971
> 
> &#x2013; [British households face record 54% energy bill rise as price cap is raised | &#x2026;](https://www.theguardian.com/money/2022/feb/03/uk-households-face-record-54-energy-bill-rise-as-price-cap-is-lifted) 

-   [Energy bills: what help is Sunak offering on the cost of living crisis? | Ene&#x2026;](https://www.theguardian.com/money/2022/feb/03/what-help-is-rishi-sunak-offering-cost-of-living-crisis-energy-price-cap)


## August 2022

As of August, this is estimated to go up to over £4,200 next year when the price cap increases again.  

> The monthly take-home pay for the average worker will be £2,054 next year, based on Bank of England forecasts, while the annual cost of energy is predicted to be £4,200.
> 
> &#x2013; [UK energy bills ‘set to cost two months’ wages’, ministers warned | Energy in&#x2026;](https://www.theguardian.com/business/2022/aug/12/uk-energy-bill-cost-two-months-wages-ministers-warned) 

