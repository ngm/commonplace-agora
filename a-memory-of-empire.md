# A Memory of Empire

-   tags: [[books]]

&#x2013; Arkady Martine

Enjoyed this.  It's written well, and it's fun.  At first I didn't find so much to sink my teeth into, ideas-wise, but I think maybe it's just ideas I'm not so familiar with.  [[Empire]], [[colonisation]], displacement, culture clash and culture shock all feature heavily.  

Plot-wise, it's a little bit of murder mystery, but mainly kind of like court politics and the machinations of power. 

Couple of interesting sci-fi-y ideas.  The [[imago machines]], where current generations incorporate the personalities of individuals from previous generations directly via neural implants.  The AI city in Teixcalaan - theme there of how a neutral algorithm is not as neutral as you may be led to believe, which is very relevant to present day.

At first it felt maybe a little slow to get going, but I definitely was gripped the more I went along with it.

Finished January 2020.

