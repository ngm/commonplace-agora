# Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers

URL
: https://doi.org/10.1080/10455752.2022.2106578

Author
: Michael J. Albert

[[Ecosocialism]] for realists.

Interesting.  Appreciate the call to action for more focus the now, the how, the strategy for [[transition]].  That said, not 100% convinced there's **that** much strategy in this paper.  It's speculation on the direction in which two versions of the [[Green New Deal]] could go.  Not a huge amount about how to transition **to** a Green New Deal in the day-to-day.

Regardless - handy initial overview of different [[types of ecosocialism]], and the common thread between them.

Useful notion of 'realist utopianism' and some critiques of existing ecosocialist thought.  This is a nod to [[Envisioning Real Utopias]]  I believe.

Also handy outline of various types of Green New Deals out there.

Then, interesting thought experiment of the way different approaches to GND could pan out, and the dangers we need to be prepared for as a result.

Yet - nothing really in terms of praxis of what I can do today.  Other than a CTA asking us to think more about what to do.

Lots of useful stuff though.  Some takeaways:

-   need more theory of transition in ES
-   different types of GNDs could pan out different ways
-   [[solidarity economy]] etc in the here and now, plus some thought on the hard questions of mid- and long-term is a decent dual prong approach

I liked the combination of a bit of horizontal, a bit of vertical.  Or at least a bit of prefiguration plus thinking about the harder strategic problems.


## Highlights

> An ecosocialist economy would prioritize and invest in forms of labor, enterprises, and infrastructure projects that are socially useful and ecologically regenerative (rather than profitable for capitalists)

<!--quoteend-->

> Some, like Ian Angus and John Bellamy Foster, describe transitional steps in the struggle for ecosocialism, but these usually involve a wish list of things we want and hope to achieve in the nearterm (e.g. “immediately” eliminating military spending, or an immediate “moratorium on economic growth in rich countries”), rather than how we might do so and the conditions that would make this possibl

<!--quoteend-->

> Yet developing plausible (r)evolutionary scenarios that can guide concrete praxis is one of the key challenges for ecosocialists that requires more careful reflection, lest we be guided by little more than leaps of faith with minimal grounding in current tendencies. A useful step forward is taken by ecosocialists who emphasize the strategic

<!--quoteend-->

> might the GND actually form a stabilizing mechanism for global capitalism by responding to red-green demands for green jobs, redistribution, and accelerated climate action?

<!--quoteend-->

> realist” or “realist utopian” approach to ecosocialism that combines rigorous social and ecological analysis with speculative imagination, working through the likely constraints on utopian potentials, how they might be surmounted, and the likely challenges, tensions, trade-offs, and dilemmas we would face even in the best-case scenarios.

<!--quoteend-->

> While engagement with the GND by ecosocialists has often been critical, others rightly view it as a promising transitional program that can begin rapidly reducing emissions while building the longer-term foundations for a post-capitalist transformation

<!--quoteend-->

> the argument made by degrowth critics like Matt Huber and Robert Pollin—that we must promise more to win the working class—is understandable, it not only chooses to ignore politically inconvenient ecological limits but is itself a strategically risky proposition, given the likely (but not inevitable) failure of growth in an era of converging climate, political-economic, energy, and food crises

<!--quoteend-->

> The risk, in other words, is that ecosocialist regimes that are democratically elected in a context of unprecedented climate-energy-economic crises may be forced down an authoritarian path in order to enforce carbon rationing, enact rapid and far-reaching transformations in land-use, break through the gridlock of dysfunctional and polarized legislatures, and defend themselves
> against violence and sabotage from capitalist elites and the far-right

<!--quoteend-->

> ecosocialists should reflect more systematically on the domestic and international threats that ecosocialisms-in-transition would confront – including capital flight, of course, but also militant far-right resistance, cyberattacks, social media-enhanced disinformation operations from capitalists and counterrevolutionary states, and other forms of sabotage – and how they might respond.

<!--quoteend-->

> we can speculate that there at least two different ecosocialist “equilibria” in the future possibility space: first would be the ecosocialist degrowth trajectory that emerges from a relatively near-term GND crisis (e.g. between 2030 and 2050), and second would be a more long-term ecosocialist transition (e.g. between 2050 and 2080) that forces a rapid scaleup of NETs, collectively managed solar geoengineering, and an intensification of extractivist conflicts.


## Handwritten notes

![[files/handwritten-notes/ecosocialism-for-realists/page-1.jpg]]

![[files/handwritten-notes/ecosocialism-for-realists/page-2.jpg]]

![[files/handwritten-notes/ecosocialism-for-realists/page-3.jpg]]

![[files/handwritten-notes/ecosocialism-for-realists/page-4.jpg]]

![[files/handwritten-notes/ecosocialism-for-realists/page-5.jpg]]

![[files/handwritten-notes/ecosocialism-for-realists/page-6.jpg]]

