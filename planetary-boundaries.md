# planetary boundaries

Often discussed in terms of the [[overshoot of planetary boundaries]] and what we can do to stay within them.

Nine boundaries:

1.  [[climate change]]
2.  [[ocean acidification]]
3.  stratospheric ozone depletion
4.  biogeochemical flows in the nitrogen (N) cycle
5.  global freshwater use
6.  land system change
7.  the erosion of biosphere integrity
8.  chemical pollution
9.  atmospheric aerosol loading

They each have associated limits.

> On top of this territorial restriction, scientists have provided global figures on myriad other ecological limits: from the maximum amount of nitrogen and phosphorus that can be used as fertilizer (62 and 6.2 megatonnes per year respectively) without causing mass eutrophication, to the freshwater available for consumption (4 petalitres per year), to the carbon that can be emitted (1.61 tonnes per person per year for 2°C of warming, even less for the more ambitious 1.5°C target).39 Further restrictions on acceptable levels of pollution can be taken from the public health literature – for example, fine particulate matter suspended in the atmosphere should have an annual mean of 10 micrograms per cubic metre or less
> 
> [[Half-Earth Socialism]]

