# Beware of the collector's fallacy

> Because ‘to know about something’ isn’t the same as ‘knowing something’.
> 
> &#x2013; [The Collector’s Fallacy • Zettelkasten Method](https://zettelkasten.de/posts/collectors-fallacy/)

