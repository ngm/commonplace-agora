# The policing bill increases the risk of peaceful demonstrators being criminalised

The [[Policing bill]] increases the risk of peaceful demonstrators being criminalised.

> Restrictions on protests in the controversial new policing bill breach human rights laws and will increase the risk of peaceful demonstrators in England and Wales being criminalised, MPs and peers have warned.  
> 
> -   [Curbs on protests in policing bill breach human rights laws, MPs and peers sa&#x2026;](https://www.theguardian.com/uk-news/2021/jun/22/curbs-on-protests-in-policing-bill-breach-human-rights-laws-mps-and-peers-say)

<!--quoteend-->

> with protesters suffering prison or sky-high fines for not observing prohibitive new rules, about which they could not pretend they did not know if tried in court
> 
> &#x2013; [Attempting to ban protest is usually the mark of a repressive state. That’s n&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain) 

<!--quoteend-->

> The police and home secretary would become the arbiters of whether a protest or assembly is allowable, when it can start and finish, and they could outlaw it if it is too noisy.
> 
> &#x2013; [Attempting to ban protest is usually the mark of a repressive state. That’s n&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain) 

<!--quoteend-->

> The police would be empowered to issue “serious disruption prevention orders” at will, arrest individuals without any cause except that they look suspicious, and imprison, for up to 51 weeks, those who “lock on” to railings.
> 
> &#x2013; [Attempting to ban protest is usually the mark of a repressive state. That’s n&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain) 


## Because

> The bill includes powers for the home secretary to ban marches and demonstrations that might be “seriously disruptive” or too noisy; a criminal offence of obstructing infrastructure such as roads, railways, airports, oil refineries and printing presses; jail sentences for attaching or locking on to someone or something; bans on named individuals from demonstrating or going on the internet to encourage others; police stop-and-search powers to look for protest-related items; and up to 10 years’ prison for damaging memorials or statues
> 
> &#x2013; [Thursday briefing: ‘A man without shame’ | | The Guardian](https://www.theguardian.com/world/2022/jan/13/thursday-briefing-a-man-without-shame)


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Signs point to yes]]

