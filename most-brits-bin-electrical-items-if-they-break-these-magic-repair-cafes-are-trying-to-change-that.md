# Most Brits bin electrical items if they break. These 'magic' Repair Cafés are trying to change that

URL
: https://www.bigissue.com/news/environment/repair-cafe-uk-fix-broken-electrical-items/

Lovely article in the Big Issue on [[repair cafes]].

> As the cost of living crisis squeezes budgets up and down the country, repair groups are increasingly popular. As our ecological footprint grows, they’re also an environmental necessity.

