# An Ecosocialist Strategy to Win the Future

URL
: https://www.rosalux.de/en/news/id/49515/an-ecosocialist-strategy-to-win-the-future

[[Ecosocialism]].

> Developing an effective strategy for radical political change demands a clear vision of antagonisms, alternatives, and pathways of execution.

^ yes

> The problem with the question of “how” is that it has often been perceived as a simple matter of mechanisms and tools that can be chosen from an existing arsenal. If we need to get from Mexico City to Guadalajara, we can choose between driving, taking the bus, flying, or even walking. A purely instrumental view of the “how” depoliticizes the conditions and consequences of the methods employed and prevents us from continuously evaluating the compatibility between a chosen tactic and the overall strategy.

<!--quoteend-->

> When we propose socialism as a system that will save us from capitalism, it is not enough to simply assert that socialist revolution is necessary because society will not survive without it. To those already familiar with the dire need to overthrow capitalism, these statements are nothing more than truisms used to assert our own radical positions.

^ yes

> In that sense, green capitalism poses more of a threat than standard climate denialism, as it appears to acknowledge the scientific consensus around climate change, but conceals capitalism’s role in the crisis.

<!--quoteend-->

> We face the immediate threats of the reorganization of far-right and fascist forces — including ecofascists – and the growing dominance of green capitalism. As we organize to fight such threats, our job is to also identify and engage with possible courses of action that can tackle many things at once.

<!--quoteend-->

> Indeed, desirable outcomes are at the core of a successful strategy. Life needs to improve early on in the implementation of an ecosocialist programme to ensure long-term support for the socialist horizon and the possibility for rupture, especially when under external threats of repression, sanctions, and war.

<!--quoteend-->

> In sum, our strategy is for an ecological transition that will make the socialist transition possible. It transitions from a deeply unsustainable society to one where the risk of collapse will have been delayed by at least a few centuries.

<!--quoteend-->

> Our strategy will indeed prepare for war, but will seek to avoid it by laying the foundations for peace.

<!--quoteend-->

> Since the reforms promoted by the many plans and deals of the ecological transition are not enough to actually overcome capitalism, our strategy requires strong movement building that will guarantee these reforms but also create conditions for rupture.

<!--quoteend-->

> One tide carries a faster transition from point A to point B, where we buy ourselves ecological time and offer glimpses into a better life while still under capitalism.

<!--quoteend-->

> The other tide consists of movement building, whereby we strengthen class consciousness and democratic socialist standards that build collective power for a more radical rupture targeting all the pillars of private property, profit, and accumulation, in what will be the transition from capitalism to socialism.

<!--quoteend-->

> Generally, a GND is a bundle of reforms, investments, and adjustments tied to mitigation and adaptation to climate change, but also to other aspects of the ecological crisis, which must be implemented within a short timeframe. GNDs must be part ofour strategy, but are not ourstrategy as such, as they aim at more direct public policy and are vulnerable to changes in government.

<!--quoteend-->

> Different versions of the Green New Deal have been introduced since the debate re-emerged in the US after 2018, some more capitalist and some more radical.

<!--quoteend-->

> Irrespective of the labels used, the advantage of integrating GND-like programmes into an ecosocialist strategy is two-fold: they contain changes that can be implemented today, and they can be tools for mobilization.

<!--quoteend-->

> Reducing working hours at stable productivity levels alters the rate of exploitation of workers, making it a radical anti-capitalist demand.

<!--quoteend-->

> An ecosocialist strategy creates convenience of a different kind by providing green public infrastructure that makes life easier and cheaper for workers, reconciling the needs of people and nature in the ecological transition.

