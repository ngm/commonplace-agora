# Associational socialism

I heard of it first from [[Aaron Benanav on Associational Socialism and Democratic Planning]].

There he says it has similarities to [[Guild socialism]] and [[council communism]].  So, I think, something of an avoidance of centralised party control, with power more in the hands of 'associations'.  Which might be guilds or councils, but might be other types of associations.

