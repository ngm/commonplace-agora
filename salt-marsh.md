# Salt marsh

I'm really liking salt marshes.  They're pretty new to me, and there's lots in the vicinity of [[Lancaster]].

> A salt marsh or saltmarsh, also known as a coastal salt marsh or a tidal marsh, is a coastal ecosystem in the upper coastal intertidal zone between land and open saltwater or brackish water that is regularly flooded by the tides.
> 
> &#x2013; [Salt marsh - Wikipedia](https://en.wikipedia.org/wiki/Salt_marsh)


## Morecambe Bay salt marsh from the Furness line train

![[photos/salt-marsh.jpg]]

