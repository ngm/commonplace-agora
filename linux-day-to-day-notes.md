# Linux day to day notes



## Disk usage


### Chromium disk usage

I just noticed that Chromium (which I very rarely use) was taking up 1.3GB of space in my .config folder.

Here's how to clean that up: [disk usage - Why does Chromium take up 1 GB in my .config and can I reduce th&#x2026;](https://askubuntu.com/questions/1038150/why-does-chromium-take-up-1-gb-in-my-config-and-can-i-reduce-that-size)

Odd that it was taking up so much space, given that my daily driver is Firefox.


### Composer disk usage

```nil
composer clearcache
```


### apt

```nil
sudo apt-get clean
```

```nil
sudo apt-get autoremove && sudo apt-get autoclean
```


## Sorting by file size

I keep on running low on space.  Disk Usage Analyzer is OK, but for the command line:

```shell
du -hs * | sort -h
```

The -h in both commands is for 'human readable'.

`dust` is better though.


## Trimming a video from my phone so I can upload it up to Signal

Signal has a 100M upload limit.  Plus it'd take ages to upload.

This does the job pretty simply, still good enough quality for sending as a message and reduces the filesize massively: 

```shell
ffmpeg -i input.mkv -vf "scale=iw/2:ih/2" half_the_frame_size.mkv
```

https://unix.stackexchange.com/a/447521


## Shrinking images off my phone for web upload

Quick command to shrink images from my camera for uploading to the web.

```shell
mogrify -auto-orient -resize 25% *.jpg
```


## Finding out which security updates need installing on a server

```shell
sudo unattended-upgrade -v --dry-run
```

https://askubuntu.com/a/832648/10271


## Killing a process tree

I seem to need to be doing this more lately on `php artisan serve`.

Appears to be lots of ways of doing it: https://stackoverflow.com/questions/392022/whats-the-best-way-to-send-a-signal-to-all-members-of-a-process-group

All falling in to the category of https://xkcd.com/1168/

pkill one works OK for me:

```shell
ps x | grep 8000
# gives me some process ids, pick the one at the top
pkill -TERM -P <pid>
```


## ls love

Sort by modified time, in reverse:

```shell
ls -ltr
```


## git


### find first time a bit of text was added in a repo

```nil
git log -S <search string> --source --all
```

https://stackoverflow.com/a/5816177/206297

