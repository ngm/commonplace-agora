# Universal credit



## Articles

-   [The universal credit cut is the end point of years of ‘welfare’ cruelty](https://www.theguardian.com/commentisfree/2021/oct/03/after-years-of-cruelty-to-people-on-welfare-is-the-uk-starting-to-think-differently)
-   [‘Choice between using shower or oven’: harsh realities of universal credit cut](https://www.theguardian.com/society/2021/oct/06/choice-between-using-shower-or-oven-harsh-realities-of-universal-credit-cut)

