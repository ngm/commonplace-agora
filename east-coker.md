# East Coker

A
: [[poem]]

Author
: [[T.S. Eliot]]

East Coker is part of Four Quartets. 

This excerpt was read at my Dad's funeral.

<div class="verse">

**East Coker (excerpt)**<br />
<br />
Home is where one starts from. As we [[grow older]]<br />
The world becomes stranger, the pattern more complicated<br />
Of dead and living. Not the intense moment<br />
Isolated, with no before and after,<br />
But a lifetime burning in every moment<br />
And not the lifetime of one man only<br />
But of old stones that cannot be deciphered.<br />
There is a time for the evening under starlight,<br />
A [[time]] for the evening under lamplight<br />
(The evening with the photograph album).<br />
Love is most nearly itself<br />
When here and now cease to matter.<br />
Old men ought to be explorers<br />
Here or there does not matter<br />
We must be still and still moving<br />
Into another intensity<br />
For a further union, a deeper communion<br />
Through the dark cold and the empty desolation,<br />
The wave cry, the wind cry, the vast waters<br />
Of the petrel and the porpoise. In my end is my beginning.<br />

</div>

