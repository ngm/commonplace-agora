# Synergia MOOC

Synergia [[MOOC]].

> ntnsndr - Looking to up your systems-change game? Consider joining @synergia's upcoming online course on "Transition in a Perilous Century." These are top-notch guides to the cooperative commonwealth we need: https://synergiainstitute.org/mooc-overview/
> 
> &#x2013; https://social.coop/@ntnsndr/109543785060375709

