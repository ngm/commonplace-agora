# Tour Bus Stop

Welcome!  Thanks for alighting the tour bus here.


## This stop

This is [[Neil's Digital Garden]].  Visit that page and you'll find much more info on some good entry points in to my garden.

In case you're interested: [[My personal wiki setup]].


## Other stops

This bus stop is on the route Bus #200 - the Personal Wiki Tour.

Next stop - [Melanocarpa](https://garden.bouncepaw.com).

Check out the [full bus map](http://meatballwiki.org/wiki/TourBusMap).

Ding ding.


## Bye

Bon voyage! - stop by again soon.

(if you need to get in touch - email me at neil at doubleloop dot net, or find me on Mastodon - @neil@social.coop.)

