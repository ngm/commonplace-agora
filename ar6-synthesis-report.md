# AR6 synthesis report

URL
: https://www.ipcc.ch/report/sixth-assessment-report-cycle/

[[IPCC Sixth Assessment Report]] synthesis report.

> The “synthesis report” contains no new science, but draws together key messages from all of the preceding work to form a guide for governments. The next IPCC report is not due to be published before 2030, making this report effectively the scientific gold standard for advice to governments in this crucial decade.
> 
> &#x2013; [Scientists deliver ‘final warning’ on climate crisis: act now or it’s too lat&#x2026;](https://www.theguardian.com/environment/2023/mar/20/ipcc-climate-crisis-report-delivers-final-warning-on-15c?ref=upstract.com) 

<!--quoteend-->

> Monday’s final instalment, called the synthesis report, is almost certain to be the last such assessment while the world still has a chance of limiting global temperature rises to 1.5C above pre-industrial levels, the threshold beyond which our damage to the climate will rapidly become irreversible.
> 
> &#x2013; [Scientists deliver ‘final warning’ on climate crisis: act now or it’s too lat&#x2026;](https://www.theguardian.com/environment/2023/mar/20/ipcc-climate-crisis-report-delivers-final-warning-on-15c?ref=upstract.com) 

<!--quoteend-->

> Richard Allan, a professor of climate science at the University of Reading, said: “Every bit of warming avoided due to the collective actions pulled from our growing, increasingly effective toolkit of options is less worse news for societies and the ecosystems on which we all depend.”
> 
> &#x2013; [Scientists deliver ‘final warning’ on climate crisis: act now or it’s too lat&#x2026;](https://www.theguardian.com/environment/2023/mar/20/ipcc-climate-crisis-report-delivers-final-warning-on-15c?ref=upstract.com) 

<!--quoteend-->

> Hoesung Lee, chair of the body, which is made up of the world’s leading climate scientists, made clear that – despite the widespread damage already being caused by extreme weather, and the looming threat of potentially catastrophic changes – the future was still humanity’s to shape.
> 
> &#x2013; [Scientists deliver ‘final warning’ on climate crisis: act now or it’s too lat&#x2026;](https://www.theguardian.com/environment/2023/mar/20/ipcc-climate-crisis-report-delivers-final-warning-on-15c?ref=upstract.com) 

<!--quoteend-->

> This image from today's IPCC synthesis report is brutal.  The existing policy trajectory represents a profound failure of our governments, and of our international political system. We need much more aggressive mitigation and much stronger international cooperation.
> 
> &#x2013; https://twitter.com/jasonhickel/status/1637809317758873604

[‘It can be done. It must be done’: what the latest IPCC report tells us - Pos&#x2026;](https://www.positive.news/environment/a-summary-of-the-ipcc-report/)

