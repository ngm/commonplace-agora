# miniflux issue: dial tcp 127.0.0.1:5432: connect: connection refused

I was getting the error:

> dial tcp 127.0.0.1:5432: connect: connection refused

When trying to load up my install of [[miniflux]] on [[YunoHost]].

Turns out it was because postgresql had stopped running - don't know why.  Starting the postgresql service from Tools -&gt; Services sorted it out.

