# TMK BC5: Mute Compulsion, Introduction

A
: [[podcast]]

Found at
: https://soundcloud.com/thismachinekillspod/unlocked-275-tmk-bc5-mute-compulsion-introduction

Part of
: [[This Machine Kills]]

> We kick off the TMK book club with the excellent new book – [[Mute Compulsion]] by [[Søren Mau]] – which offers a foundational analysis of power, value, capital, and social reproduction. Mau’s book is written with a real analytical clarity that advances our critical, theoretical understanding of the relations and operations of those things in society and our lives. We set the context for the book and our approach before discussing the Introduction chapter, which established the book’s motivating questions and overarching argument about the need to pay special attention to the economic power of capital.

Recognising [[economic power]] means we can better articulate what needs to be done. Otherwise if we only see coercion we think we need only challenge the state, if we only see ideology we think we need only change our minds. But if we see economic power we challenge it and it's role in [[social reproduction]].

