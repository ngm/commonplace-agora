# Forest and Factory

An
: [[article]]

Found at
: https://endnotes.org.uk/posts/forest-and-factory

Subtitle
: The Science and the Fiction of Communism

The science and the fiction of [[communism]].

Heard about it from the [[This Machine Kills]] podcast.

Provocative.  Kind of a modern day update on the topic of [[Socialism: Utopian and Scientific]].

Critiques a bunch of things I've read recently as utopian, in the sense of lacking any practical route from the here and now to there. Fair comment - though I've appreciated them, I've thought similar.

Find it a bit unnecessarily disdainful in tone to some of the other projects that it is critiquing. We're all on the same side here!

Bit dubious of their mentions of carbon capture and storage / direct air capture is a bit of a red flag.

They speak highly of [[Mute Compulsion]], though critique Mau's recent article on what the future might look like as utopian.


## Problems with [[utopian socialism]]

> The problem with the utopia, then, is not that it is science fiction. Its fictive power is precisely why utopia is able to wield such a disproportionate force in the political imagination and therefore why the artful production of attractive aesthetics and imaginative worlds will be essential to the practical construction of any political project. The problem is instead that most utopias are not actually science fiction—or, at least, not "hard" science fiction, distinguishable from fantasy for its efforts to take the physical world seriously

<!--quoteend-->

> they all share the same glaring absences etched onto the same fictive flatness: first and foremost, we find the absence of "politics" itself, in the sense of some strategic sequence of struggle stretched between the immediate world and the envisioned utopia—

<!--quoteend-->

> These utopias therefore act as what Lenin called a "wish that can never come true," or, more pointedly, "a wish that is not based on social forces and is not supported by the growth and development of political, class forces.

<!--quoteend-->

> Another fundamental failure of most utopian visions is the fact that they treat his process of revolutionary transition and communist construction as largely incidental to the character of the communist society that is its ultimate result. Instead, we would argue that it is precisely the messiness of this process of revolution and reconfiguration that provides the real raw material (in both a technical and social sense) from which a communist world will be constructed

<!--quoteend-->

> In other words, the utopian strategy was to exit capitalist society or retreat to its edges and either peaceably build a new world in these settler colonies or use them as launch pads to combat capitalist decay from the outside. But Marx argued that modern industry and the associated build-up of state power doomed such a strategy from the outset.

I would say that their notion of 'hard' political imaginary isn't that different from what I've heard called [[scientific utopianism]].


## Money and markets

> Communism would therefore require the abolition of money and the market system that it represents, since these are the material underpinnings of the specifically capitalist form of social domination that has wedged itself in the metabolic gap between the human species and its means of subsistence.

[[Communism requires the abolition of money and the market system]].

