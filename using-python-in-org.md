# Using Python in org



## Setup

To avoid [[Importmagic and/or epc not found]], [[Wrong version of Python in org code blocks]], I've got the following config options set:

```emacs-lisp
(setq python-shell-interpreter "python3")
(setq org-babel-python-command "python3")
```

