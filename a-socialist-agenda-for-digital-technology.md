# A socialist agenda for digital technology

A [[socialist agenda]] for [[digital technology]].

In contrast to [[The digital sector under capitalism]]. 

> Citizens must be able to access and share publicly relevant information, publish their responses, and have their responses assessed in turn, confident that, to the extent that they are vulnerable to manipulation, they have the means to combat it. In practical terms this means **developing interoperable social media and messaging resources**, as well as **secure data storage for individuals and groups**. 
> 
> &#x2013; [[The British Digital Cooperative: A New Model Public Sector Institution]]

-   would include interoperable social media and messaging platforms
-   would include secure data storage for individuals and groups
-   citizens would collaborate in efforts to understand and change the world
-   citizens would share information consciously
-   citizens would access and analyse collectively generated data as equally

> Instead of relying on an environment designed to deliver advertising content to targeted demographics, we will **be able to shape our online experience and collaborate in efforts to understand and change the world**. We will **share information consciously and be able to access and analyse collectively generated data as equal citizens**. Designers will be able to concentrate on **promoting sociability and productive exchange**, without the need to extract and analyse data for the purposes of manipulation.
> 
> &#x2013; [[The British Digital Cooperative: A New Model Public Sector Institution]]

<!--quoteend-->

> Resources we control, on the other hand, will enable us to find one another in diverse ways, and to delight in the full potential of our sociable natures.
> 
> &#x2013; [[The British Digital Cooperative: A New Model Public Sector Institution]]

-   [[online platforms should return value to the citizen body]]

