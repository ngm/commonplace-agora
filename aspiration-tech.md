# Aspiration Tech

URL
: https://aspirationtech.org

> Aspiration's mission is to connect nonprofit organizations, free and open source projects, philanthropic funders and activists with strategic frameworks, technology solutions, digital practices and data skills that help them more fully realize their missions.

