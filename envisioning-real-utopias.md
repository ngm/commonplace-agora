# Envisioning Real Utopias

[[Erik Olin Wright]]

> The greatest achievement of Envisioning Real Utopias is, as I understand it, its invaluable role in demonstrating that despite the reigning cynicism about the possibility for change, one need not seek refuge in a utopia in the clouds
> 
> &#x2013; [[Envisioning real utopias from within the capitalist present]]

<!--quoteend-->

> It is worth remembering here that capitalism began its journey within the margins of the feudal economy – and what Wright provided us with are the analytical resources to understand how socialism could potentially emerge from within the margins of the capitalist economy.
> 
> &#x2013; [[Envisioning real utopias from within the capitalist present]]

<!--quoteend-->

> Its realisation, Wright thought, would depend on the formation of institutions that can guarantee “social empowerment” or, to put it differently, individuals’ capacity to exert control, collectively, over the ownership and use of economic resources and activities. In a workers’ cooperative, for example, workers are effectively empowered by owning an equal share of the organisation and having an equal voice in decision making.
> 
> &#x2013; [[Envisioning real utopias from within the capitalist present]]

