# ERC

This is an [[IRC]] client in [[Emacs]].  I'm using [[ZNC]] as an IRC bouncer, so some of this might only apply if you're using ZNC too.


## Hiding alerts

```emacs-lisp
(setq erc-hide-list '("JOIN" "PART" "QUIT"))
```

&#x2013; https://emacs.stackexchange.com/questions/26997/erc-mode-how-to-hide-alerts-when-users-join-leave-a-channel


## Setting default server

In spacemacs, they are set at the point of including the erc layer in your layers config.

```emacs-lisp
(erc :variables
	erc-server-list
	'(("irc.example.com"
		:port "6770"
		:ssl t
		:nick "nick"
		:password "password")))
```

-   https://emacs.stackexchange.com/a/36035
-   https://www.spacemacs.org/layers/+chat/erc/README.html#default-servers

**IMPORTANT NOTE**: don't actually put your password in the config - https://www.spacemacs.org/layers/+chat/erc/README.html#security-note

```nil
machine <irc.server.url> login <yournick> password <yourpassword> port <portnumber>
```

