# Community wealth building

> The “community wealth building” strategy for promoting community networks down the stack can be helpful here. Public and quasi-public anchor institutions like government agencies, hospitals, and universities could be mined for procurement contracts that funnel revenue to the deprivatized sector and help it grow. Cooperatives could form federations to collaborate with one another in various ways. Governments could encourage them with loans and grants and tax breaks.
> 
> &#x2013; [[Internet for the People]]


## Five principles

-   Plural ownership of the economy.
-   Making financial power work for local places.
-   Fair employment and just labour markets.
-   Progressive procurement of goods and services.
-   Socially productive use of land and property.


## Challenges

> The problem is that although this offers a solution emphasising growth, and the potential for a more democratic model of ownership to emerge, there can be strong tendencies towards simple local protectionism. Leadership candidates speaking about [[localism]] must ensure it’s more than a simply a listening exercise and they should be looking forward for their solutions, not into the past.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]

<!--quoteend-->

> Blaenau isn’t so much an alternative to the Preston model as it is a bottom-up application of the same principles. In Preston, the impetus came from the council, so the focus has been on procurement, regeneration, and building new institutions. In Blaenau, it came from local people, so the focus has been on social enterprise, co-operatives, and community energy. For community wealth-building to work—for it to be applicable across the country—you need both elements
> 
> &#x2013; [[‘It’s Not Rocket Science – It’s Just Community’: Radical Ffestiniog]]


## Articles

-   [The principles of community wealth building | CLES](https://cles.org.uk/what-is-community-wealth-building/the-principles-of-community-wealth-building/)

