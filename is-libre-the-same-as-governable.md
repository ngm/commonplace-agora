# Is libre the same as governable?

Is [[libre]] the same as [[governable]]?

I'm thinking from the perspective of [[libre software]] and [[governable stacks]].

Is governable a layer above libre?  Does libre necessary mean that you can have some input in to the direction of the thing?  I'm thinking probably not.

> Technologists seeking alternative visions have often gravitated to the Free Software and Open Source movements, which employ creative licensing to enable the sharing of accessible and modifiable code. These movements have been successful in terms of the sheer volume of widely used software in their commons. But their emphasis on the freedoms of individual users, as well as of corporations, has privileged those with the technical know-how to take advantage.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> Governable stacks should prioritise community accountability alongside individual freedom.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

