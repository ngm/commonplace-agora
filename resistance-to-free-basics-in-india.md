# Resistance to Free Basics in India

> However, successful resistance to developments like Facebook’s [[Free Basics]] in India and the construction of Amazon’s headquarters on sacred Indigenous land in Cape Town, South Africa show the possibility and potential of civic opposition.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

^ https://indianexpress.com/article/technology/tech-news-technology/facebook-free-basics-india-shut-down

