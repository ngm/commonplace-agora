# Economic Calculation in the Socialist Commonwealth

Author
: [[Ludwig von Mises]]

> Mises argued that socialists were wrong on both counts. Instead, people in a socialist society would work more hours and get less for it. That’s because, in his view, the efficiency of modern economies was inextricably connected to their organization via the market, with its associated institutions of money and private property. Get rid of these institutions, and the technologies developed over the course of the capitalist era would become fundamentally worthless, forcing societies to regress to a less advanced technological state
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Ultimately, prices tell producers which production possibilities have any chance of turning a profit. Without prices, Mises argued, the rational allocation of assets becomes impossible
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Each producer can make rational decisions about what and how to produce, only because a struggle for market supremacy forces producers to maximize their revenues and minimize their costs. All of these market-dependent producers absorb information to the best of their abilities, make decisions, and take risks in search of new production possibilities and the corresponding monetary rewards. Socialist planners couldn’t possibly reproduce such a complex system, Mises believed, because they would never have more information than market participants mediated through the price mechanism
> 
> &#x2013; [[How to Make a Pencil]]

