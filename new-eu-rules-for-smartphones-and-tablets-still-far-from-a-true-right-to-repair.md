# New EU rules for smartphones and tablets: still far from a true Right to repair

URL
: https://repair.eu/news/new-eu-rules-for-smartphones-and-tablets/

Date
: 2022-11-25


Re: [[ecodesign]] requirements proposed by the [[European Commission]] that will apply to phones ([[smartphones]], mobile phones, cordless phones) and tablets

while having initial [[repairability]] rules for these products is a first important step

missed opportunity to introduce rules granting people a true [[Right to repair]].


New rules are **meant to** reduce the [[environmental impact]] of phones and tablets.

They will include [[durability]] requirements, and aim to improve the repairability and reliability of these devices.

This will be the first time that such rules apply to this category of products, setting the tone for future regulation on other ICT products such as computers and printers.


In December, the Member State expert group will decide on the introduction of a label on smartphones and tablets.
    -   Such a label is expected to include a repair score and comparative information on the reliability of these devices.


## The good bits in the new rules

-   In terms of repairability, rules will:
    -   force manufacturers to give access to:
        -   repair and maintenance information
        -   and spare parts
        -   to professional repairers and end-users
        -   for at least 7 years after retiring a product from the market.
    -   software updates will also have to be made available
        -   for at least 5 years after retiring a product from the market.
-   In terms of reliability, smartphones will:
    -   have to survive at least 45 accidental drops before losing functionality
    -   retain at least 80% of a battery’s capacity after 800 charging cycles


## Not good enough though

-   However, the near final version of the agreed text obtained by the Right to Repair Coalition lacks the ambition needed to grant people a [[universal right to repair]] and reach the objectives of the [[Green Deal]].

-   Right to Repair campaigners are concerned that:
    -   the final rules will not prevent manufacturers from using software practices to limit independent repair.
    -   the high price of spare parts will not be tackled either.
    -   the range of spare parts available to consumers and community repair initiatives will be seriously limited.

> manufacturers and retailers will still keep control of who repairs their devices through part pairing.

<!--quoteend-->

> Especially disappointing is the removal of manufacturers’ obligations to display and adhere to a maximum price of spare parts.

<!--quoteend-->

> For the first time spare parts, repair information and software updates will have to be made available long-term.

<!--quoteend-->

> Yet, the rules fail to address repair’s affordability, the need for end-users and community repair initiatives to access all spare parts, and the use of software by manufacturers to limit the use of reused and third-party parts.

