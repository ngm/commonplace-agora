# flâneur

> the flâneur’s raison d’etre—to participate fully through observation
> 
> &#x2013; [In Praise of the Flâneur](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) 

<!--quoteend-->

> [[Psychogeographers]] idolise the flâneur, a figure conceived in 19th-century France by [[Charles Baudelaire]] and popularised in academia by [[Walter Benjamin]] in the 20th century. A romantic stroller, the flâneur wandered about the streets, with no clear purpose other than to wander. 
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> The figure of the [[flâneur]]—the stroller, the passionate wanderer emblematic of nineteenth-century French literary culture—has always been essentially timeless; he removes himself from the world while he stands astride its heart.
> 
> &#x2013; [In Praise of the Flâneur](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) 

<!--quoteend-->

> “[the flâneur] was a figure of the modern artist-poet, a figure keenly aware of the bustle of modern life, an amateur detective and investigator of the city, but also a sign of the alienation of the city and of capitalism,” 
> 
> &#x2013; [In Praise of the Flâneur](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) 

