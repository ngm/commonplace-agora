# system

> A system is an interconnected set of elements that is coherently organized in a way that achieves something. 
> 
> &#x2013; [[Thinking in Systems]]

<!--quoteend-->

> If you look at that definition closely for a minute, you can see that a system must consist of three kinds of things: elements, interconnections, and a function or purpose.
> 
> &#x2013; [[Thinking in Systems]]

<!--quoteend-->

> So what is a system? Simply a set of things that are interconnected in ways that produce distinct patterns of behaviour – be they cells in an organism, protestors in a crowd, birds in a flock, members of a family, or banks in a financial network. And it is the relationships between the individual parts – shaped by their [[stocks and flows]], [[feedbacks]], and delay – that give rise to their emergent behaviour
> 
> &#x2013; [[Doughnut Economics]]

<!--quoteend-->

> -   A system is more than the sum of its parts.
> -   Many of the interconnections in systems operate through the flow of information.
> -   The least obvious part of the system, its function or purpose, is often the most crucial determinant of the system’s behavior.
> -   System structure is the source of system behavior. System behavior reveals itself as a series of events over time.
> 
> &#x2013; [[Thinking in Systems]]

<!--quoteend-->

> most of the challenges that really matter involve systems, for example, energy and global warming; water, food, and population; and health and social justice.
> 
> &#x2013; [[A Systems Literacy Manifesto]]

