# Open Data Commons Open Database License

https://opendatacommons.org/licenses/odbl/

Feels similar to [[Creative Commons]], but for data.

Though it seems to me that it's primarily about keeping the data open - with not much in there really regarding governance of the data.

