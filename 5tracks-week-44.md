# 5tracks - week 44

[[5tracks]] from Bandcamp  for you to and enjoy and support the artists.

-   [Deaf Center - Social Lucy Waltz](https://deafcenter.bandcamp.com/track/social-lucy-waltz)
-   [Angel Olsen - (We Are All Mirrors)](https://angelolsen.bandcamp.com/track/we-are-all-mirrors)
-   [Deru - Cottonmouth Lothario](https://musicstore.deru.la/track/cottonmouth-lothario)
-   [Karmahacker - Happens Anyway](https://cosmicleaf.bandcamp.com/track/happens-anyway)
-   [Vektordrum - Rhubarb](https://vektroid.bandcamp.com/track/rhubarb)

