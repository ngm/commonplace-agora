# Climate Leninism

Climate [[Leninism]].  (Is that specifically Leninism rather than Marxism-Leninism?) [[Kai Heron]] and [[Jodi Dean]].  See also [[Eco-Leninism]].

> We thus use Climate Leninism as the name for the politics needed at this conjuncture of imperialism and climate emergency. The [[revolutionary party]] is its basic premise.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

So I think [[vanguard party]] aspect.

![[images/climate-lenin.jpg]]
&#x2013; from [[Revolution or Ruin]] article

> It contains a Climate Leninist theory of revolutionary transition: party-building, anti-imperialism, and a global coalition of the oppressed. A COP26 for anti-imperialists. The same form — planetary transitions, planetary aspirations — with a different, revolutionary, content.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

And [[anti-imperialism]].

> This recognition lets us formulate Climate Leninism more precisely as preparedness plus nonlinearity within the given material conditions, in other words, the organization of a collectivity with the capacity to respond to the climate emergency.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> But Climate Leninism cannot mechanically apply Lenin’s political prescriptions. Climate Leninism must mean something more expansive. It must be situated within and draw from the entire tradition of revolutionary thought and struggle that has positioned itself as a continuation of the Russian Revolution. This includes anti-colonial revolutionaries who, in Fanon’s words, found that they must “stretch” Lenin and the lessons of the revolution, reshaping them for their own times and context: intellectuals and organizers like Walter Rodney, Amilcar Cabral, Samir Amin, Jose Carlos Mariategui, Antonio Gramsci, A.M Babu, Harry Haywood, Sam Moyo, and Rossana Rossanda. It includes struggles in China, Vietnam, Guinea Bissau, Angola, the island of Ireland, Burkina Faso, Cuba and more. What unites these thinkers and movements across their differences is a knowledge of the necessity of revolution, the seizure of the state, and the role of peasants, workers, women, and national minorities. The Russian Revolution itself would have been impossible without the development of just such a “coalition of the oppressed,” as Lenin put it.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> These coalitions cannot be assumed. They must be composed in and through shared struggles, acts of solidarity, and party-building. Climate Leninism requires building coalitions between Indigenous peoples, workers in the Global North, smallholder farmers and pastoralists, women, racialized communities, and other oppressed and exploited groups on issues of ecological, economic, and political significance.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> Climate Leninism today should draw inspiration from these struggles. It should listen to the signatories of the Cochabamba People’s agreement and stand in solidarity with ongoing calls for economic and food sovereignty from peasant movements like La Via Campesina and Brazil’s Landless Workers’ Movement as well as calls for national self-determination and Land Back from Indigenous and colonized peoples the world over. Such struggles and their demands to delink from capital’s global divisions of labor must be the point of departure for a radical anti-capitalist climate politics in the Global North and South. Following thinkers such as Max Ajl and Keston Perry, Climate Leninism should put climate reparations and technology transfers at the center of its internationalism.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

