# Consul

https://consulproject.org/en/

> Free software for citizen participation.

Grew out of Decide Madrid, I think.

-   Debates
-   Proposals
-   [[Participatory budgeting]]
-   Voting
-   [[Collaborative legislation]]
    
    > Decide Madrid is an open source civic tech platform developed by the Madrid city council in 2015 and licensed under AFFERO GPL 3. It received the UN's prestigious public service award in 2018 and has been implemented in more than 100 cities, including Paris, New York, and Porto Alegre, Brazil. The platform was designed to be as easy to use as possible.
    > 
    > &#x2013;  [[Building an open infrastructure for civic participation]]
    
    <!--quoteend-->
    
    > Madrid's implementation is unique because it gives executive powers to its citizens: any proposal that receives 1% of citizens' votes is automatically placed on the government's agenda. Because of this, Decide Madrid remains one of the most consequential open source projects to date and an important experiment in combining representative and direct democracy in the context of entrenched disenchantment with democracy.
    > 
    > &#x2013; [[Building an open infrastructure for civic participation]]

> A better indicator of a civic platform's success than participation numbers might be its direct effects. In this context, that is how many citizen proposals received enough votes to be sent to the city council. Here, the picture is less rosy: only two citizen proposals were voted onto the agenda. The conversion rate between visits on the site and voting for proposals was too low.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

