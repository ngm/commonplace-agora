# Working Class Voices: Season Two with Emma River Roberts

URL
: https://www.gndmedia.co.uk/podcast-episodes/working-class-voices-season-two-with-emma-river-roberts

> This week Ads is joined by Emma River Roberts. They discuss why [[Just Stop Oil]]'s actions miss the point when it comes to accelerating [[climate action]], Emma's experience of [[being working class in the climate movement]], Emma's view of the [[Labour Party]] right now and what needs to be done to properly link the working class and climate movements together.

Interesting discussion. About what needs to be done to make the environmental movement resonate better with the working class.

Ads says when doorknocking, Green Industrial Revolution did resonate. Keir Starmer has nothing that resonates.

Working class generally sceptical of promises of a just transition - been burned by that before.

