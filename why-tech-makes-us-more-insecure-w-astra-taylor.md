# Why Tech Makes Us More Insecure w/ Astra Taylor

A
: [[podcast]]

Part of
: [[Tech Won't Save Us]]

Featuring
: [[Astra Taylor]]

[[Insecurity]] and [[security]] - in more of social and psychological meanings of the word than the technical sense.

Claim: [[Capitalism requires insecurity]].

Claim: [[Social insurance is a bulwark against insecurity]].

Also by Astra: [[The Dads of Tech]] and [[The People's Platform]].

