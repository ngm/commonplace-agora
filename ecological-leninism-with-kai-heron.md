# Ecological Leninism with Kai Heron

URL
: https://cosmopod.libsyn.com/ecological-leninism-with-kai-heron

Series
: [[Cosmopod]]

Featuring
: [[Kai Heron]]

Great discussion.  Really like [[Kai Heron]].

Touches on lots of things, including [[Ecosocialism]], [[Degrowth]], [[land rights]], [[Green New Deal]], [[Eco-Leninism]], [[trade unionism]], [[Extinction Rebellion]], [[ecomodernism]].

[[Ecological Marxism]].

