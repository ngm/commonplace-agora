# The Future is Degrowth

A
: [[book]]

URL
: https://www.versobooks.com/books/3989-the-future-is-degrowth

> The Future of Degrowth argues that the global economy must be scaled down to align with its natural limits.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> The book offers a broad overview of the [[degrowth]] movement and its critique of the postwar Keynesian paradigm – along with the colonialist, capitalist and patriarchal ideologies that underpinned it.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> Though the authors acknowledge ‘overlaps and similarities’ between their framework and the [[Green New Deal]], they argue that the latter is fundamentally flawed. Not only is it hitched to a fantasy of ‘progressive productivism’, it would also require a neo-colonial mining regime for its build-out of renewable-energy infrastructure.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> Against this trend in climate policymaking, The Future is Degrowth articulates a form of utopianism rooted in the here and now, based on what Engels once called ‘model experiments’. Engaging with the work of [[Erik Olin Wright]], the authors describe a series of ‘nowtopias’ – community-supported agriculture, communing, cooperative economies – which they see as an antidote to climate ‘megaprojects’ (on which they propose a blanket ‘moratoria’).
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> Schmelzer et al. also lay out a concrete agenda: the ‘Global North’ must lower consumption while switching to renewable energy and more localized production. How will this ‘fundamental political and economic reorganization of society’ be brought about? The authors admit it may require ‘confrontations with private ownership structures’. Historically, they write, such transformations ‘have always been marked by fierce controversies, public disputes and, up to now, (violent) conflict.’ **Yet their main strategy for enacting this green transition is borrowed from the well-worn post-1968 playbook of turning the Leninist Antonio Gramsci into an ecumenical pluralist.**
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> They predict that degrowth alternatives will add up, one by one, into a powerful ‘counter-hegemony’ able to simultaneously offer alternative lifestyles, pass ‘non-reformist reforms’ via the state machinery, and build revolutionary ‘dual power’ ready for ruptural crises.
> 
> &#x2013; [[Mish-Mash Ecologism]]

<!--quoteend-->

> The Future of Degrowth envisions a ‘pluriverse’ of diverse and localized alternatives, letting a thousand degrowth flowers bloom
> 
> &#x2013; [[Mish-Mash Ecologism]]

