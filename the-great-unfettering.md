# The Great Unfettering

URL
: https://newleftreview.org/sidecar/posts/the-great-unfettering

Author
: [[Kai Heron]]

Response to [[Mish-Mash Ecologism]].

From a first quick skim:

-   agrees with need for widening of climate politics to appear to wider base (i.e. working class)
-   seems less critical of degrowth
-   finds issue with the US focus of Huber
-   brings in anti-imperialism to the debate

> What matters here is that, like all eco-modernists, Huber assumes capitalist industrialization is the pinnacle of technological advancement. Technology progresses, they suppose, in a linear fashion from inefficient and labour-intensive systems to efficient, energy-intensive, labour-saving ones. Hence, for Huber as for Phillips, the aim should be ‘to take over the machine, not turn it off!’

<!--quoteend-->

> Instead of seeing capital’s abolition as the unfettering of productive forces, it is better to view it as freeing the world’s producers to choose from a richer and more diverse array of technologies and socio-ecological relations than capitalist industrialization can offer.

<!--quoteend-->

> The question, then, is not about whether one is for or against technology – as if this were possible. It is about adopting appropriate technologies and collectively managing energy and food systems at relevant scales.

