# Red Plenty

A
: [[book]]

Author
: [[Francis Spufford]]

A fictionalised but reasonably historical account of the attempts by the [[Soviet Union]] at a [[planned economy]].  With many contrasts and comparisons to the contemporaneous [[free market economy]] in the [[United States]].  Kind of a living, breathing account of the [[socialist calculation debate]].

Gorgeously written.  I must say that it feels like a cousin of sorts to [[The Dispossessed]].  I thoroughly enjoyed it.

Set in the 50s and 60s so [[Khrushchev]] figures.  And mathematicians such as [[Leonid Kantorovich]].


## Quotes

Also see backlinks - quoted heavily elsewhere.

> Some comrades seemed to think that fine words and fine ideas were all the world would ever require, that pure enthusiasm would carry humanity forward to happiness: well excuse me, comrades, but aren’t we supposed to be materialists? Aren’t we supposed to be the ones who get along without fairytales? If communism couldn’t give people a better life than capitalism, he personally couldn’t see the point

<!--quoteend-->

> No more patches, no more darns

Hey, I like patches and darns.  I think it hints how the Soviet Union and the United States were competing on **[[growth]]**.

> Since the great quarrel between capitalism and socialism was really an economic one, why not conduct it that way, instead of as a war

<!--quoteend-->

> Let’s compete on the merits of our washing machines, not the strength of our rockets

<!--quoteend-->

> Some of the cars had open tops, and you could see the drivers inside, all alone, sitting on plump benches as wide as beds. One of the cars was pink.

Extravagance.

> The Americans seemed to want to take the order of the city into the country with them: having dreamed of the forest, they woke, and tidily organised their dream

<!--quoteend-->

> But the Americans got it. Of all the capitalist countries, it was America that was most nearly trying to do the same thing as the Soviet Union. They shared the Soviet insight. They understood that whittling and hand-stitching belonged to the past

<!--quoteend-->

> That was Marx’s description, anyway. And what would be the alternative? The consciously arranged alternative? A dance of another nature, Emil presumed. A dance to the music of use, where every step fulfilled some real need, did some tangible good, and no matter how fast the dancers spun, they moved easily, because they moved to a human measure, intelligible to all, chosen by all. Emil gave a hop and shuffle in the dust.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-26 Sat]</span></span>

Still excellent.  Gorgeous writing.  It presents the contrasts fairly I think?  It's obviously set from a Soviet perspective, but you see their inner doubts and struggles.  Not sure where [[Francis Spufford]] sits politically but he seems to present things without too much personal spin.  Has obviously thoroughly done his research, based on the footnotes.

Gives some nice broad, sweeping pieces of prose outlining the history of the Soviet Union, and its failings, and of some Marxist theory.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-03-16 Wed]</span></span>

Finally started reading Red Plenty.  It's really good so far.  Really nicely written meditations on [[planned economy]] versus [[free market economy]].  Take away so far is that both, insofar as encapsulated by [[Soviet Union]] and [[United States]], are just different ways of willy-waving and chasing growth.  I feel like there must be a middle-way that's about [[prosperity without growth]], somewhere between planned and free market, [[neither vertical nor horizontal]].

