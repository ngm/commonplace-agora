# Problems with org-publish cache

At the step of loading the cache file&#x2026;

> Loading _home/neil_.org-timestamps/commonplace-notes.cache&#x2026;

Sometimes get errors.

e.g.

> ‘org-publish-cache-file-needs-publishing’ called, but no cache present

In this case, the file in question is there, but it's empty - is that is what is meant by not present?  I guess so, as the cache file seems to build the cache.

Wonder why it's empty?

Another time got the error:

> End of file during parsing: _home/neil_.org-timestamps/commonplace-notes.cache

which is true, seems to have stopped halfway through the file.

The cache file is 340K in size, don't know if that's of relevance.

