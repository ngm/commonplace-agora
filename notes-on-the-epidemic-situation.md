# Notes: On the epidemic situation

&#x2013; [On the Epidemic Situation](https://www.versobooks.com/blogs/4608-on-the-epidemic-situation)
:: [[post-covid]] 

Summary: Alain Badiou saying that despite the current necessary state interventions, the pandemic will not by itself change anything politically.  But he doesn't really say what actually is required to do so, other than the left needing to work on 'new figures of politics'.  Not much to go on, really.

> From the start, I thought that the current situation, characterised by a viral pandemic, was not particularly exceptional. 

There have been a number of terrible viruses in recent memory.

> It is clear then that the current epidemic is **by no means the emergence of something radically new or unprecedented**. It is the second of its kind this century and can be situated as the first’s descendant. 

This one is an evolution of the original SARS.

> So, I didn’t think there was anything to be done other than try, like everyone else, to isolate myself at home, and nothing to be said other than to encourage everyone else to do the same. 

I think his point is that this does not appear to be anything new or a portent of something bigger?

> But I am reading and hearing too many things, including in my immediate circles, that disconcert me both by the confusion they manifest and by their utter inadequacy to the – ultimately simple – situation in which we find ourselves.

<!--quoteend-->

> These peremptory declarations, pathetic appeals and emphatic accusations take different forms, but they all share **a curious contempt for the formidable simplicity, and the absence of novelty, of the current epidemic situation**. 

But others are decrying it as such, and ignoring the fact that it could have been predicted?

> Some are unnecessarily servile in the face of the powers that be, who are in fact simply doing what they are compelled to by the nature of the phenomenon. Others invoke the Planet and its mystique, which doesn’t do any good. Some blame everything on the unfortunate Macron, who is simply doing, and no worse than another, his job as head of state in times of war or epidemic. Others make a hue and cry about the founding event of an unprecedented revolution, whose relation to the extermination of a virus remains opaque – **something for which our ‘revolutionaries’ are not proposing any new means whatsoever**. Some sink into apocalyptic pessimism.

<!--quoteend-->

> An epidemic is rendered complex by the fact that it is always a point of articulation between natural and social determinations.

Natural:

> The natural trajectory of the virus from one species to another thereby transits towards the human species.

Social:

> After which there simply operates a fundamental datum of the contemporary world: the rise of Chinese state capitalism to imperial rank, in other words an intense and universal presence on the world market. 

<!--quoteend-->

> China is thus a site in which one can observe the link – first for an archaic reason, then a modern one – between a nature-society intersection in ill-kept markets that followed older customs, on the one hand, and **a planetary diffusion of this point of origin borne by the capitalist world market and its reliance on rapid and incessant mobility**, on the other.

<!--quoteend-->

> Despite the existence of some trans-national authorities, it is clear that it is local bourgeois states that are on the frontline.

<!--quoteend-->

> The economy, including the process of mass production of manufactured objects, comes under the aegis of the world market – we know that the simple assembly of a mobile phone mobilises work and resources, including mineral ones, in at least seven different states

<!--quoteend-->

> the rivalry between imperialisms, old (Europe and US) and new (China, Japan…) excludes any process leading to a capitalist world state

<!--quoteend-->

> **national states attempt to confront the epidemic situation by respecting as much as possible the mechanisms of Capital, even though the nature of the risk compels them to modify the style and the actions of power**.

<!--quoteend-->

> in the event of a war between countries, the state must impose, not only on the popular masses, as is to be expected, but on the bourgeoisie itself, considerable constraints, all in order to save local capitalism.

<!--quoteend-->

> by weakening, decade after decade, the national health system, along with all the sectors of the state serving the general interest, it acted instead as though nothing akin to a devastating pandemic could affect our country. To this extent the state is very culpable

<!--quoteend-->

> And it is surely not leftists – or gilets jaunes or even trade-unionists – who enjoy a particular right to hold forth on this point, and to continue to make a fuss about Macron, their derisory target for the last while. They too had absolutely not envisaged this. On the contrary, as the epidemic was already on its way from China, they multiplied, until very recently, uncontrolled assemblies and noisy demonstrations, which should disqualify them today, whoever they may be, from loudly condemning the delays taken by the powers that be in taking the full measure of what was happening.

<!--quoteend-->

> The ongoing epidemic will not have, qua epidemic, any noteworthy political consequences in a country like France

Not going to change anything politically.

> As for those of us who desire a real change in the political conditions of this country, we must take advantage of this epidemic interlude, and even of the – entirely necessary – isolation, to work on new figures of politics

But the left should work on alternatives during this period?

> a political charge will only be carried by new affirmations and convictions concerning hospitals and public health, schools and egalitarian education, the care of the elderly, and other questions of this kind. Only these might possibly be **articulated with a balance-sheet of the dangerous weaknesses on which the current situation has shed light**

<!--quoteend-->

> In passing, one will need to **show publicly and dauntlessly that so-called ‘social media’ have once again demonstrated that they are above all – besides their role in fattening the pockets of billionaires – a place for the propagation of the mental paralysis of braggarts, uncontrolled rumours, the discovery of antediluvian ‘novelties’, or even  fascistic obscurantism**.

