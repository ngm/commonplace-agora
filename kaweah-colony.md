# Kaweah Colony

> [the world's largest tree] was originally named after Karl Marx by a utopian socialist community called the Kaweah Colony, who controlled the land between 1886 and 1892. The community was disbanded when Sequoia national park was created and the tree was renamed for American civil war general William Tecumseh Sherman. The tree is believed to be between 2,300 and 2,700 years old.
> 
> &#x2013; [From feral pigs to windfarms: how good is your green knowledge? – quiz | Envi&#x2026;](https://www.theguardian.com/environment/2021/oct/30/from-feral-pigs-to-windfarms-how-good-is-your-green-knowledge-quiz) 

