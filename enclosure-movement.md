# Enclosure Movement

[[Enclosure]]

> The forcible expropriation of peasants alienated humanity from the natural world, and led to a “metabolic rift,” as Marx described it, beginning a process that has driven us today to the brink of planetary destruction
> 
> &#x2013; [[A People's Guide to Capitalism]]

