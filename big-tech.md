# Big Tech

Generally refers to the digital technology firms with the most money and power, i.e. the US firms of Alphabet ([[Google]]), Meta ([[Facebook]]), [[Amazon]], [[Apple]], [[Microsoft]].

'Digital' technology being a shorthand for [[ICT]].

> Big Tech is a shorthand term used to refer to the leading companies in the [[information and communications technology]] industry (ICT) sector
> 
> &#x2013; [[Digital Capitalism online course]]

Sometimes also includes the big Chinese firms - [[Tencent]], [[Alibaba]], [[Baidu]].

Might loosely stretch to others, e.g. to [[platform capitalism]] in general.  So your Twitters, Airbnbs, Netflix, etc, as well.  Even if not quite so big. Let's not forget general shits like [[Palantir]], too.

> The idea of ‘Big Tech’– referring to a handful of digital companies at the apex of the digital economy — signifies the inordinate power that a few tech corporations have accrued
> 
> &#x2013; [[Digital Capitalism online course]]

[[I dislike big tech]].


## [[Money]]

Big Tech has a **lot** of money.  They currently represent the [[commanding heights of the economy]].

> Big Tech also stands out in the stock market. In 2023, Apple, is the most valuable company in the world, worth an outstanding $2.716 trillion dollars45. Microsoft comes close, as the second most valuable firm at $2,289 trillion, followed by Google (Alphabet) in 4th with $1.425 trillion and Amazon at $1.137 trillion in 5th. Only the fossil fuel firm Saudi Aramco reaches the heights of US Big Tech firms when it comes to its market valuation
> 
> &#x2013; [[Digital Capitalism online course]]


## [[Power]]

Big Tech has a **lot** of power.  Economic power, yes, but also domination over key infrastructure of the modern world.

> These firms have come to dominate specific niche markets
> 
> &#x2013; [[Digital Capitalism online course]]


## Misc

> Needless to say, web services are a powerful force that can shape politics and economics around the globe. Even the slightest updates to regulatory law in the US, where many of these platforms are based, can result in profound global reverberations that are difficult to predict.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

<!--quoteend-->

> Similarly, the large platforms are all renting Amazon cloud services for exorbitant monthly amounts, like Twitch ($15M/month), Linkedin ($13M/month), and Twitter ($7M/month on top of $10M/month to Google Cloud). This doesn’t take into account many other operating costs, like personnel salaries and software maintenance. 
> 
> &#x2013; [Toward a Digital Economy That’s Truly Collaborative, Not Exploitative](https://thereboot.com/toward-a-digital-economy-thats-truly-collaborative-not-exploitative/) 

<!--quoteend-->

> In the realm of digital technology, it is commonly known that today’s tech giants have built much of their empires on a code commons. The open meadows of collaboratively written code and generously shared repositories, published under permissive licences, are treated as fair game — and fenced off into proprietary products. 
> 
> &#x2013; [[Seeding the Wild]]

<!--quoteend-->

> The dominance of tech monopolies resulting from current conditions gives us reason to doubt that the free market alternative fully delivers on its commitment to promoting pluralism and individual freedoms.
> 
> &#x2013; [[Platform socialism]]


## Resources

-   Some great infographics: [Big Tech - The rise of GAFAAMT | Transnational Institute](https://www.tni.org/en/big-tech-the-rise-of-gafaamt)

