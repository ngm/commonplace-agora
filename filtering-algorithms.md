# Filtering (algorithms)

Isolating what's important.


## examples

-   speech recognition filtering out background noise to hear voice
-   Facebook and Twitter filtering out the stories they think will interest you

