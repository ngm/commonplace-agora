# A Traversal Network of Feminist Servers

URL
: https://varia.zone/en/atnofs-publication.html

> A Traversal Network of Feminist Servers (ATNOFS) is a collaborative project formed around intersectional, feminist, ecological servers whose communities travelled between each other in 2022 to share and extend their knowledges through live gatherings. ATNOFS argues that such platforms and tools are necessary to navigate our communications and cultural growth beyond the current media oligopolies, and democratise cultural and political expression outside obscure, bureaucratic algorithms and advertising monetisation.

