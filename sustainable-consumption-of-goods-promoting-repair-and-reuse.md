# Sustainable consumption of goods – promoting repair and reuse

[[Sustainable consumption]] of goods - promoting [[repair]] and [[reuse]].  [[Right to Repair in Europe]].

URL
: 

https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13150-Sustainable-consumption-of-goods-promoting-repair-and-reuse_en

> This initiative promotes a more sustainable use of goods throughout their useful life. It will encourage consumers to make more sustainable choices by providing incentives and tools to use goods for a longer time, including by repairing defective goods.

<!--quoteend-->

> It will also:
> 
> -   encourage producers to design goods that last longer and are easily reparable
> -   help reduce unsustainable consumption and its negative impact on the global environment and climate
> -   help build a [[circular economy]].

A document with some thoughts on what to submit here: https://docs.google.com/document/d/1Ug6g9IC3h1ImpbPQr3EkUtHQPPkUTyNScA2G2SHPprk/edit

> So far, EU policy related to the Right to repair has been limited to regulating product per product and it’s becoming urgent to secure mandatory rules that could apply to multiple product categories at once (such as [[non-destructive disassembly]], [[access to spare parts]] and [[access to repair information]] for instance). 

<!--quoteend-->

> We ask for policies that: 
> 
> -   Ensure [[access to spare parts]] at a reasonable cost and free [[access to repair information]] to everyone including consumers, non profit repair initiatives and independent repairers
> -   Ensure [[independent repair]] is possible outside of manufacturers or distributor’ networks and not limited through software, hardware or contractual elements
> -   Ensure products are designed in a way that allows the use of “non genuine” and second hand spare parts by consumers and independent repairers
> -   Develop a [[repair index]] that includes pricing as a criteria and covers a wide range of products to allow consumers to be informed about the repairability of products at the time of purchase
> -   Promote safe self repair by requiring manufacturers to provide consumers with adequate information on product disassembly and reassembly
> -   [[Ensure repair can be performed at a fair price]] by developing fiscal support, tax breaks and ensuring affordable cost of spare parts
> -   Ensure these measures apply to ALL product categories, and not only for those that the Commission is already partly regulating with ecodesign regulations - which are very limited in reach

<!--quoteend-->

> Products can be used for a long time only if they are designed in a durable way and if they are used for as long as possible. The first aspect is addressed by the Commission’s ‘sustainable product initiative’ (SPI), which aims at setting **binding requirements for producers** to ensure that products are designed to be durable and repairable. The second aspect relies on the **consumer’s willingness** to use the product for a long time and to avoid early replacement or disposal. 
> 
> &#x2013; https://ec.europa.eu/eusurvey/runner/Right_to_Repair?surveylanguage=en

<!--quoteend-->

> Studies show that one of the **main causes for premature disposal of goods lies in the difficulties to repair broken products**. The ‘right to repair’ initiative aims at addressing this problem by providing consumers with incentives to encourage sustainable use of products, and increase their ability to repair defective goods.
> 
> &#x2013; https://ec.europa.eu/eusurvey/runner/Right_to_Repair?surveylanguage=en

<!--quoteend-->

> The Commission is considering a number of measures focused on promoting sustainable use of goods, e.g. through incentivising repair under the legal guarantee period and promoting the use of second-hand goods
> 
> &#x2013; https://ec.europa.eu/eusurvey/runner/Right_to_Repair?surveylanguage=en

I believe it is vital that products are made to last longer, for environmental and social reasons.  The climate cost of constant, short upgrade cycles is one of more carbon in the atmosphere, as much of the carbon impact of products is in their manufacture; and of the Earth's finite resources being unnecessarily depleted.  There is also a social impact - the exploitation of human labour in mining the raw materials, and in producing the goods, is increased as the demand for products increases.

Thus I desire policies that enable products to last for a long time by strongly encouraging durable design practices and that facilitate repair.

I would like policies that:

-   Ensure access to reasonably priced spare parts, and free access to repair information, to everyone including citizens, community repair initiatives, and independent repair businesses
-   Ensure that independent repair is not limited through software, hardware or contractual elements
-   Ensure products are designed in a way that allows the use of 3rd party party and second hand spare parts
-   Develop a repair index allows consumers to be informed about the repairability of products at the time of purchase
-   Promote safe self repair by requiring manufacturers to provide consumers with adequate information on product disassembly and reassembly
-   Ensure repair can be performed at a fair price (for example by developing fiscal support and tax breaks)
-   Ensure these measures apply to all types of product

