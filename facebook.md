# Facebook

-   [Imagine the End of Facebook | LibrarianShipwreck](https://librarianshipwreck.wordpress.com/2021/10/07/imagine-the-end-of-facebook/)
-   [Can The Public Sector Displace Facebook’s Unprecedented Corporate Power?](https://www.readthemaple.com/can-the-public-sector-displace-facebooks-unprecedented-corporate-power/)

> Meta makes 97% of its revenue from advertising, by building up profiles of its users that can then be matched to advertisers’ needs
> 
> &#x2013; [Facebook’s first ever drop in daily users prompts Meta shares to tumble | Fac&#x2026;](https://www.theguardian.com/technology/2022/feb/02/facebook-shares-slump-growth-fourth-quarter)

