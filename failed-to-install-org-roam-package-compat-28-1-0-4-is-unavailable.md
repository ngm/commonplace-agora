# Failed to install org-roam: Package `compat-28.1.0.4' is unavailable

My gitlab pipeline to build my org-roam based digital garden is failing with:

> Failed to install org-roam: Package \`compat-28.1.0.4' is unavailable 

Similar problem here:
https://emacs-china.org/t/compat-28-1-0-4-is-unavailable/20724

If you look at https://elpa.gnu.org/packages/compat.html, then 28.1.0.4 is indeed not available.

Actually though, it's a few days later and now it's saying that 28.1.1.0 is unavailable.  And that one **does** appear to be on elpa.  Hmm.

Oh OK, but if I look at my publish.el file, I'm not actually included elpa, I'm including melpa and org-elpa.  Maybe compat moved from one to the other recently?

OK, so I added elpa to the list of available package archives:

```emacs-lisp
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("elpa" . "https://elpa.gnu.org/packages/")
			 ("org" . "http://orgmode.org/elpa/")))
```

And that seems to have sorted it out.  Not sure if compat moved, or org-roam started needed compat.

