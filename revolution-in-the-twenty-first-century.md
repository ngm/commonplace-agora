# Revolution in the twenty-first century

[[Revolution]] in the [[21st century]].

You might take it as a factual list of revolutions that have already happened: [[21st-century revolutions]].  There have been plenty of [[revolutionary waves]] already.

You might look forwards and take it as the intent to inspire a world revolution beyond capitalism.

You may find articles related to [[Occupy]], the [[Arab spring]], etc.  Also the aftermath e.g. [[Arab Winter]].


## What kind of revolution?

First off.  What do we mean by revolution.  There's various types.  I'm a socialist so [[Revolutionary socialism]] is my thing.  End of capitalism, onset of socialism is the end goal, and revolution is the means, as opposed to e.g. gradual reform.

That said, a revolution doesn't happen over night, so you have to plant the seeds.


## How?

I have anarchist tendencies so favour [[socialism from below]] rather than socialism from above.  Though I recognise the need for organisation for any lasting revolution so wish for a [[synthesis of horizontalism and verticalism]].

In the 21st century it seems inevitable that [[the revolution will be networked]] one way or another.

[[Educate, agitate, organise]]. Educate - read and disseminate. Agitate - ?
Organise - participate in networks, out of this form [[communities of praxis]].

I believe in [[prefiguration]], so enact the parts of the future you want to see today. Also works for network building. 

Join and support [[cooperatives]]. Participate in [[Mutual aid]] and [[Self-governance]]. Form [[affinity groups]]. Become a [[commoner]]. Do all of these both locally and globally.  Be part of the [[catalyst group]] for the [[emergent revolution]]. 


## When?

My starting bet for socialism to be solidly the prevalent structure worldwide is: 2099 

However "[[the revolution is continuous]]" &#x2013; [[vera]]

so continuous revolution and revolutionary waves along the way to that point.  [[intensive revolution]]s leading to [[extensive revolution]].


## Some links that might be worth reading

-   [Revolution in the 21st century - Counterfire](https://www.counterfire.org/articles/opinion/16783-revolution-in-the-21st-century)
-   https://www.marxists.org/ebooks/harman/Revolution_in_the_21st_Century_-_Chris_Harman.pdf
-   [[Common: On Revolution in the 21st Century]] This seems to be about commoning as a mode of revolution.  Though what's the 'common' as opposed to the commons?

