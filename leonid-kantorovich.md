# Leonid Kantorovich

> Leonid Kantorovich (1912–86), mathematician and economist, nearest Soviet equivalent to [[John von Neumann]]
> 
> &#x2013; [[Red Plenty]]

He features in Red Plenty a lot.

[[Linear programming]].

> This ‘[[Road of Life]]’ was the only way to keep the millions of trapped civilians and soldiers in Leningrad alive and fighting. There was much death on this road too – some forty trucks fell through the ice during the first week of the winter convoy.  Kantorovich’s job was to minimize these losses. If he failed, the city would not hold out for long.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> As Soviet science emphasized applied work for ideological and practical reasons, it was natural enough that the plywood factory’s engineers presented Kantorovich with the problem that would define his career: optimize production at a factory with eight different lathes, each with different speeds and outputs depending on the type of material processed, and five plywood types to be produced in a specific ratio. The problem was much more difficult than it appeared. Kantorovich soon realized that if he were to use traditional methods of optimization, he would need to solve about 1 million equations. He raised the matter with his colleagues in the department, but they had no solutions – apparently the engineers had visited the mathematicians before, to no avail
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> As Kantorovich understood it, the goal is not to micromanage every kilogram of coffee or piece of steel rebar around the globe, but to ‘construct a system of information, accounting, economic indices, and stimuli which permit local decision-making organs to evaluate the advantage of their decisions from the point of view of the whole economy
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Instead, Kantorovich realized that optimization had to occur on multiple scales, both in space and time, with a vast number of models loosely connected to one another. Regional governments would have leeway over how the local economy operated, so long as it met the broad conditions set by the national plan
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> This multilayered planning is perfect for working within a natural and social world with enormous variation in ecology, climate, and needs in different areas
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Kantorovich, on the other hand, envisioned factories and farms independently using the parameters from the linear programming algorithm to plan their production, not unlike a price system but without a universal equivalent guiding economic decisions
> 
> [[Half-Earth Socialism]]

