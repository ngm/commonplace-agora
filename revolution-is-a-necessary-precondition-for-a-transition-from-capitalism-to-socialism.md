# revolution is a necessary precondition for a transition from capitalism to socialism

[[Revolution]] is a necessary precondition for a transition from [[Capitalism]] to [[Socialism]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

