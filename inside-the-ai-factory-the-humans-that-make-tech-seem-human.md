# Inside the AI Factory: the humans that make tech seem human

URL
: https://www.theverge.com/features/23764584/ai-artificial-intelligence-data-notation-labor-scale-surge-remotasks-openai-chatbots

The exploitation of people to train AI to make other people lots of money.

A problem.

Domain: [[Social equity]]. [[Exploitation]].
Stack layer: [[software]]. [[artificial intelligence]]

