# Markets for goods and services are compatible with socialism

A
: [[claim]]

> The first sort [[[markets for goods and services]]] was compatible with [[feudalism]], and it can be compatible with [[socialism]]: with solid regulation, price discovery through market clearance is a useful tool in signaling demand and avoiding the shortages in goods and services that may result from clumsy, or capricious, [[central planning]].
> 
> &#x2013; [[Markets in the Next System]]


## Because

-   [[price discovery]] through market clearance is a useful tool in signaling demand
-   price discovery through market clearance can avoid [[shortages]] in goods and services that may otherwise result from clusmy, or capricious, central planning


## If

-   you have solid regulation


## But

-   This is specifically for markets for goods and services.  The other [[type of markets]] are not nearly so capable in the resource-allotment division.


## Counterclaim

-   [[Communism requires the abolition of money and the market system]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

The article seems good.  I don't know enough about markets to make this claim solidly though.  

