# positive feedback loop



## Positive feedback loops in the environment

> When rising temperatures alter the environment in a way that intensifies the rate of overheating, a positive feedback loop is created. For instance, the melting of permafrost in Arctic tundra may trigger a release of methane from frozen hydrates. Methane is a highly potent greenhouse gas and its increased concentration in the atmosphere will likely speed the rate of global heating – so releasing more methane. 
> 
> &#x2013; Down to Earth newsletter

^ Positive as in reinforcing.  **Not** positive in general.

> Positive feedback is any process where the result magnifies the cause: an example is the screech which comes from a sound system when the signal from the speaker is itself picked up by the mic, which feeds it through to the speaker, forming a continuous loop.
> 
> &#x2013; [[The Entropy of Capitalism]]

