# On the Waterfront

> Before long we have recapitulated the final scene of the 1954 [[McCarthyist]] blockbuster On the Waterfront, in which the dockworkers flee from their union’s problems into the arms of the boss, newly able to experience their collective exploitation as individual liberation.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

