# Zebras Fix What Unicorns Break

URL
: https://medium.com/zebras-unite/zebrasfix-c467e55f9d96

> Why zebras?
> 
> -   To state the obvious: unlike unicorns, zebras are real.
> 
> -   Zebra companies are both black and white: they are profitable and improve society. They won’t sacrifice one for the other.
> 
> -   Zebras are also mutualistic: by banding together in groups, they protect and preserve one another. Their individual input results in stronger collective output.
> 
> -   Zebra companies are built with peerless stamina and capital efficiency, as long as conditions allow them to survive.
> 
> &#x2013; [[Zebras Fix What Unicorns Break]]

