# The cloud

> If the internet is a collection of machines that talk to one another, the cloud is the subset of machines that do most of the talking. More concretely, the cloud is a globally distributed set of climate-controlled buildings—“[[data centers]]”—filled with servers. These servers supply the storage and perform the computation for the software running on the internet.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> When selling computation and associated services as a business, “cloud” companies need to make everything we do more computationally intensive to create a justification for the growth of that business, which means the demands for computation must increase and along with it the number of data centers to facilitate it
> 
> &#x2013; [[AI is fueling a data center boom. It must be stopped.]]

