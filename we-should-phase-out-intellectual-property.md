# We should phase out intellectual property

A
: [[claim]]


## Because

Kwet ([[Digital Ecosocialism: Breaking the power of Big Tech]]) says that phasing out intellectual property in favour of a commons-based model of sharing knowledge would:

-   reduce prices
-   widen access to and enhance education for all
-   function as a form of wealth redistribution and reparations to the Global South

> Phasing out intellectual property in favor of a commons-based model of sharing knowledge would reduce prices, widen access to and enhance education for all and function as a form of wealth redistribution and reparations to the Global South.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]


## How?

-   [[Intellectual property markets must be socialized]]

