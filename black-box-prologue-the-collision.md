# Black Box: Prologue: The collision

A
: [[podcast]]

Part of
: [[Black Box]]

Woman begins dating an [[AI]], finds genuine positivity from it. She suffers from CFS/ME and has to shield from COVID.

Presenter talks about '[[AI vertigo]]' - dizziness when trying to comprehend what is coming with AI. The 'collision' of the title refers to the collision between artificial intelligence and us humans. We being the first generations to truly experience it.

