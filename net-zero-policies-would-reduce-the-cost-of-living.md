# Net zero policies would reduce the cost of living

Net zero policies would reduce the [[cost of living]].

> He said net zero policies were also the best way to reduce the soaring cost of living. [&#x2026;] “If you want to deal with the cost of living crisis, this is exactly what you need to do,” he said.
> 
> &#x2013; [Government policies will not get UK to net zero, warns damning report | Clima&#x2026;](https://www.theguardian.com/uk-news/2022/jun/29/government-policy-failures-are-obstacle-to-uk-net-zero-target-advisers-warn) 

That's the chair of the [[Climate Change Committee]].

> Average household bills would be about £125 lower today if previous plans on green energy and energy efficiency had been followed through.
> 
> &#x2013; [Government policies will not get UK to net zero, warns damning report | Clima&#x2026;](https://www.theguardian.com/uk-news/2022/jun/29/government-policy-failures-are-obstacle-to-uk-net-zero-target-advisers-warn) 

<!--quoteend-->

> Energy efficiency measures have already saved the average British household about £1,000 a year in energy bills, and further insulation and home improvements could halve future bills, analysis has shown.
> 
> [Green energy measures saving households £1,000 a year – analysis | Energy | T&#x2026;](https://www.theguardian.com/environment/2022/jan/28/green-energy-measures-saving-households-money-analysis-shows)

