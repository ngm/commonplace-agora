# V for Vendetta

A
: [[graphic novel]]

A
: [[film]]

[[totalitarianism]]. The Voice of Fate. Norsefire. [[Cultural genocide]]. 


## Graphic novel

Reading it in 2021 and enjoying it.


## Film

It has its flaws, but lots of fun and plenty of [[anti-fascist]], [[anti-authoritarian]] sentiment.

I've still never read the graphic novel - really should.

