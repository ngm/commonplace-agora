# Free software

> Free Software is any software that grants any user the [[four freedoms]] to use, study, share and improve the software. These “four freedoms” are given by a [[software licence]]. Software licences define the conditions under which a programme can be used and reused. For it to be Free Software, the licence text must contain at least the full exertions of the aforementioned four freedoms to any user without limitations. 
> 
> &#x2013; [[On the Sustainability of Free Software]]

-   [[free software that I use]]

-   [[Free software licensing]]

-   [[Free software economics]]


## If open source takes longer, so be it - degrowth is fine


### 'slow tech'?


### 'the maintainers' idea is kind of a slow tech movement


### not 'degrowth' - post-growth


## [[Municipal FOSS]]

