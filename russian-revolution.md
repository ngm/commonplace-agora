# Russian Revolution

Of [[1917]].

> The problem was that Marx had predicted the wrong revolution He had said that socialism would come, not in backward agricultural Russia, but in the most developed and advanced industrial countries: in England, or Germany, or the United States.
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> None of this was of the slightest use to the Marxists trying to run the economy of Russia after 1917. The Soviet Union inherited very few whirring production lines. Marxists elsewhere, in the countries where the revolution was supposed to have happened, had settled down over the years since Marx’s death as ‘Social Democrats’, running parliamentary political parties which used the votes of industrial workers to get exactly the kind of social improvements that Marx had said were impossible under capitalism. Social Democrats still dreamed of the socialist future; but here and now they were in the business of securing old-age pensions, unemployment insurance, free medical clinics, and kindergartens equipped with miniature pinewood chairs. Except in Russia, obscure despotic Russia, which had the oddest Social Democrats in the world. 
> 
> &#x2013; [[Red Plenty]]

