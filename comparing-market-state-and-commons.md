# Comparing market, state and commons

Comparing [[market]], [[state]] and [[commons]].

They are all different ways in which you can produce and allocate goods and services, so it's interesting to compare and contrast.

|            | Market               | State                    | Commons                     |
|------------|----------------------|--------------------------|-----------------------------|
| Ownership  | Private              | Public                   | Community                   |
| Control    | Individual/corporate | Centralised/bureaucratic | Participatory/collaborative |
| Allocation | Price/competition    | Regulation/planning      | Use/stewardship             |

> The market is based on private ownership, and it is controlled by individuals and corporations. The allocation of resources in the market is determined by prices and competition.
> 
> The state is based on public ownership, and it is controlled by a central authority, such as a government or a bureaucracy. The allocation of resources in the state is determined by regulation and planning.
> 
> The commons are based on community ownership, and they are controlled by the people who use and manage them. The allocation of resources in the commons is determined by use and stewardship.
> 
> Overall, the market, the state, and the commons are different forms of ownership, control, and allocation. The market is based on private ownership and competition, the state is based on public ownership and regulation, and the commons are based on community ownership and collaboration.
> 
> &#x2013; ChatGPT, 2022, response to "compare market, state, and commons in a table please"

