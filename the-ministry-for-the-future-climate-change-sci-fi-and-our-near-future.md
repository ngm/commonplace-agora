# The Ministry for the Future: Climate Change, Sci Fi, and our Near Future

A
: [[podcast]]

URL
: https://revolutionaryleftradio.libsyn.com/ministry-for-the-future

[[Kim Stanley Robinson]] and [[The Ministry for the Future]].

> We are joined by Kim Stanley Robinson, the renowned science fiction author, to discuss his NYT bestselling book "The Ministry for the Future". We discuss [[climate change]], [[geoengineering]], [[Fredric Jameson]], anti-dystopian fiction, political terrorism, [[Capitalist Realism]], [[democratic socialism]], the boomer/millennial divide, and so much more!

-   ~00:07:15  Nice quick political bio of Robinson.
-   ~00:09:18  Any mid to near future science fiction has to take climate change in to account.
-   ~00:11:59  Capitalist realism and science fiction.

