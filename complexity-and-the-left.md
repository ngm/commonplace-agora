# Complexity and the left

I've come across quite a few books recently that make a connection between [[leftist politics]], [[climate change]] and one or multiple of [[complexity science]], [[complex systems]], [[complex adaptive systems]], [[cybernetics]], and [[systems thinking]].

For example:

-   [[Doughnut Economics]]
-   [[Free, Fair and Alive]]
-   [[Anarchist Cybernetics]]
-   [[Emergent Strategy]]
-   [[Cybernetic Revolutionaries]]
-   [[The Shock Doctrine of the Left]]

What is the common thread of all of these?

[@lollonero](https://antinetzwerk.de/@lollonero) shared [[Komplexität «Chaostheorie» und die Linke]]. Not translated yet but according to lollonero it is about how the left should catch up to complexity science.


## Complex systems and economics

Doughnut Economics suggests that the economy is a complex system, and therefore can't be understood with the linear models that are dominant in present economics.  It has a chapter entitled "Get savvy with systems", which has a great intro - asking how things might have been different if Newton had been more interested in how the apple grew rather than how it fell.

> So much for that fantasy. It was the apple as it fell that grabbed Isaac’s attention and led to his groundbreaking discoveries. Craving the authority of science, economists then mimicked Newton’s laws of motion in their theories, describing the economy as if it were a stable, mechanical system. But we now know it is far better understood as a complex adaptive system, made up of interdependent humans in a dynamic living world. So if we are to have half a chance of bringing ourselves into the Doughnut, then it is essential to shift the economist’s attention from the apple as it falls to the apple as it grows, from linear mechanics to complex dynamics. Bid farewell to the market as mechanism and discard the engineer’s hard hat: it’s time to don a pair of gardening gloves instead.


## Complex systems and political organisation

[[Anarchist Cybernetics]] draws on organisational cybernetics and the viable system model in particular to discuss political organisation and how it can be supported through social media.  In a way that bridges the divide between horizontal and vertical modes of organisation.  I would say that this is for less a concern around complexity, and more a concern around decentralisation and non-hiearchy.  But given the focus of decentralisation and interconnected in complex systems, there is that link.

[[Cybernetic Revolutionaries]] is about the use of the VSM in socialist organisation in Chile.  So again more organisation cybernetics than complex systems, as such.  This is also history, not just theory.  It also has relevant critiques of when the theory didn't translate in to practice.

[[Emergent Strategy]] is explicit in drawing the connection to complex systems.  It describes most of the properties of complex systems and how they then apply to political organisation.

[[The Shock Doctrine of the Left]] explicitly uses [[complex adaptive systems]] theory to discuss a strategy for left organisation.


## Complex systems and the commons

[[Free, Fair and Alive]] has a section called [[Complexity Science and Commoning]].  And they very much like to shift to the idea of what they call a [[Relational ontology]].

> Complexity science has important things to say about the commons, too, because it sees the world as a dynamic, evolving set of living, integrated systems.
> 
> &#x2013; [[Free, Fair and Alive]]

In a nutshell, I think it's about the aspect of commoners and commoning being about relationships between individuals and the collective outcomes they are working towards together.  So linked to the actions of individuals being guided towards emergent behaviour.

FFA sees commoning as a tactic of economic and political organisation I would say, so it is also linking complex systems to both of those spheres.


## Complex systems and dialectics

A couple of sources make the connection between systems thinking and [[dialectics]]. Dialectics being a cornerstone of Marxist thought.

[[Red Enlightenment: On Socialism, Science and Spirituality]], [[When nature and society are seen through the lens of dialectics and systems thinking]].

