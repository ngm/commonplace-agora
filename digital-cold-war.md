# digital cold war

> ‘Everything seems to indicate that we are entering a digital cold war. If this is so, it is time for the peripheries to start giving shape to a [[digital non-aligned movement]]. Such a movement could operate as a buffer between the PRC and the U.S. – striving to protect the value of an open Internet, helping us adapt the Internet to become the knowledge-sharing tool that our times demand, and offering the necessary cover so that no nation feels coerced into joining an intranet that does not work in the interests of its people.’
> 
> &#x2013; [TikTok, Trump and the need for a digital non-aligned movement | openDemocracy](https://www.opendemocracy.net/en/oureconomy/tiktok-trump-and-need-digital-non-aligned-movement/)

