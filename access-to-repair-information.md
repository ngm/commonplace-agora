# access to repair information

[[Access]] to [[repair information]].

"the information necessary to repair"

To everyone, including consumers, non profit repair initiatives and independent repairers.

Promote safe self repair by requiring manufacturers to provide consumers with adequate information on product disassembly and reassembly.

