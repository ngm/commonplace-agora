# Capitalism is historically contingent

> The [[Manifesto]] demolished the idea that capitalism was a natural and eternal condition. It was a stage in history, which came out of [[feudalism]] and would give way to a more humane society
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> [[Capitalism]] has always been an improbable social formation, full of conflicts and contradictions, therefore permanently unstable and in flux, and highly conditional on historically contingent and precarious supportive as well as constraining events and institutions. 
> 
> &#x2013; [Capitalism: Its Death and Afterlife](https://www.versobooks.com/blogs/2998-capitalism-its-death-and-afterlife) 

