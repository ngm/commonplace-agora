# Connected Northern regions

-   tags: [[Politics and the North-West of England]]

Idea of connecting Northern cities like Manchester, Liverpool, Sheffield, Leeds with great public transport. (Frequent, electrical, large size for plenty of passengers).

Turning them into a megalopolis like Randstad in Holland (Rotterdam, Utretch, Amsterdam, The Hague, and smaller places).

Heard the comparison to Randstad first in: [Analysis - Unequal England - BBC Sounds](https://www.bbc.co.uk/sounds/play/m000g503).

