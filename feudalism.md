# Feudalism

> a way of structuring society around relationships that were derived from the holding of land in exchange for service or labor
> 
> &#x2013; [Feudalism - Wikipedia](https://en.wikipedia.org/wiki/Feudalism) 

<!--quoteend-->

> social hierarchy was established based on local administrative control and the distribution of land into units (fiefs). A landowner (lord) gave a fief, along with a promise of military and legal protection, in return for a payment of some kind from the person who received it (vassal). 
> 
> &#x2013; [Feudalism - World History Encyclopedia](https://www.worldhistory.org/Feudalism/) 

<!--quoteend-->

> Feudalism essentially divided monarchic lands among local lords. These lords then ruled over the inhabitants of their respective estates. Tied to the lord’s estates, serfs (unfree peasants) were put to work. They had to pay their lords with money, or with a portion of their harvest, or by tilling the lord’s fields for a certain number of days per week.
> 
> &#x2013; [[A People's Guide to Capitalism]]

[[Political theory]].

