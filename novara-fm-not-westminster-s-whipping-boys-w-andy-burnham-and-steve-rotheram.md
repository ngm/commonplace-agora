# Novara FM: Not Westminster’s Whipping Boys w/ Andy Burnham and Steve Rotheram

A
: [[podcast]]

Found at
: https://podcast.novaramedia.com/2024/03/novara-fm-not-westminsters-whipping-boys-w-andy-burnham-and-steve-rotheram/

Chat with [[Andy Burnham]] and [[Steve Rotheram]] (mayors of Manchester and Liverpool).

Constitutional reform. Abolition of House of Lords / having an alternative upper chamber. Basic Law of equality across the regions. Transport quality in different regions.

