# GND Summer Book club

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/gnd-media-summer-book-club

Very enjoyable.  Talk about a bunch of [[climate fiction]].

Mentions:

-   [[News from Nowhere]]
-   [[The Ministry for the Future]]
-   [[Oryx and Crake]]
-   [[The Monkey Wrench Gang]]
-   The Overstory

A bunch of others that sounds worth checking out.

Some good reflections of the role of fiction, climate fiction, and fiction on the left, too.

