# Governable stacks

[[Governable]] [[stack]]s.

A term coined by [[Nathan Schneider]] in the article [[Governable Stacks against Digital Colonialism]].

As far as I read it, in a nutshell, a governable stack is a set of tools/system that affords self-governance to its members across the multiple layers that build up the system.

> Stacks: collections of tools and social practices that become the infrastructure of our lives.
> 
> Governable: the quality of being accountable to the people affected by something and whose labor produces it
> 
> https://twitter.com/ntnsndr/status/1481497350321283075

-   https://twitter.com/ntnsndr/status/1481493320060657664

It makes a lot of sense to me - the governable part sounds very similar to [[commoning]].  The stack part makes perfect sense - for example, what would be the point of a democratically-owned social media site, if the server it runs on is owned by Amazon, and the device you access it on is locked down by Apple or Google.

> Governable stacks are cyborg assemblages of inter-operating technology, in symbiosis with human relationships (Haraway 1991; Puar 2012)
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> governable stacks invite the people who use them to change their relationship with technologies, to imagine different sorts of technologies, and to be changed themselves
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> The lifeblood of the governable stack is not any claim to innovation but the [[self-governance]] that flows through it. What emerges from there is the point.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> There can be no one governable stack – only many, whose archipelagos of commoning enable each other and give rise to more.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]


## Libre software

> Technologists seeking alternative visions have often gravitated to the Free Software and Open Source movements, which employ creative licensing to enable the sharing of accessible and modifiable code. These movements have been successful in terms of the sheer volume of widely used software in their commons. But their emphasis on the freedoms of individual users, as well as of corporations, has privileged those with the technical know-how to take advantage.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> Governable stacks should prioritise community accountability alongside individual freedom.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]


## Strategy


### Sovereignty

> What makes technology sovereign is when its stewards are the people who depend on it, protected from outside control by any legal or extra-legal means available. The data, the algorithms, and the interfaces are for their users, rather than acting surreptitiously against them.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]


### Democracy

[[Participatory democracy]] is the guarantor and the everyday practice of sovereignty.

> Organisational designs that work well could become part of a [[governance commons]], enabling other groups to adopt, adapt, and share them back into the common pool (Schneider et al. 2021). 
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> The more we demand and practice the arts of self-governing, the harder we are for someone else to govern.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]


### Insurgency


## Examples

-   [[May First Movement Technology]]
-   [[Community broadband]]
-   koreader

Basically reading information. 

Koreader, epub, etc the software is fairly governable

Hardware a little bit (at least I can install my own software). Not sure how repairable it is.

Network stack - no control at all. Not my router, not my connectivity.

