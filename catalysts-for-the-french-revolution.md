# Catalysts for the French Revolution

[[Catalysts for revolution]].

France's financial situation.
Debt due to monarchy's mismanagement of money.
7 years war. And investment in American revolutionary war.
Completely unequal distribution of wealth and power.

Couple of years and months of bad harvests.  Destitution, starvation, infant mortality.

Stifling taxation and feudal privileges.  Inflicted on peasants by nobility.
Everything in a peasant's life cost them a ransom.
Overall poor standard of living and political disenfranchisement.

Reluctance of nobility, church etc to relinquish any power or wealth to solve problems.

To some degree influence of Enlightenment philosophy and American revolution.

