# Payback

> Margaret Atwood’s Payback, which I recommend unreservedly as perhaps the best, and most entertaining, book ever written on [[debt]]
> 
> &#x2013; [[Talking to My Daughter About the Economy]]

