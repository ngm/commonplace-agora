# Globalization

> Globalization was many things— including a reality, in that they all lived on one shared planet in which borders were historical fantasies— but it was also a form of Americanization, of soft power imperialism combined with economic dominance, in that the US still had seventy percent of the capital assets of the world secured in its banks and companies, even though it had only five percent of the world’s population.
> 
> &#x2013; [[The Ministry for the Future]]

