# Moloch



## a Biblical thing

> is a name or a term which appears in the Hebrew Bible several times, primarily in the book of Leviticus. The Bible strongly condemns practices which are associated with Moloch, practices which appear to have included child sacrifice.
> 
> &#x2013; [Moloch - Wikipedia](https://en.wikipedia.org/wiki/Moloch) 


## a poem by [[Allen Ginsberg]] (part of [[Howl]])

> 'Moloch' has been figuratively used in reference to a person or a thing which demands or requires a very costly sacrifice.
> 
> &#x2013; [Moloch - Wikipedia](https://en.wikipedia.org/wiki/Moloch) 

<!--quoteend-->

> sphinx of cement and aluminum

Something has been sacrified to Moloch.  Modern materials but an old god.

![[images/moloch.png]]

Moloch in [[Metropolis]].

> whose mind is pure machinery!

Anti-technology?

> whose blood is running money!

[[Anti-capitalist]]?

> whose fingers are ten armies!

Anti-war?

There's a lot of exclamation marks and angry energy.

> Moloch who entered my soul early!

[[Hegemony]].

> Moloch who frightened me out of my natural ecstasy!

Yearning for nature?

> Moloch whom I abandon! Wake up in Moloch! Light streaming out of the sky!

^ [[The Matrix]] vibes.

After a first read through, I'm thinking Ginsberg was seriously angry about over-consumption and technology in the pursuit of money, at the expense of nature (or something more 'natural').  I don't know who he thinks has sold out and made this sacrifice.  People in general?  He himself maybe.  I should read the other parts.


### Words to describe Moloch

-   Solitude!
-   Filth!
-   Ugliness!
-   Nightmare
-   Loveless
-   Heavy judger
-   Incomprehensible prison
-   Crossbone soulless jailhouse
-   Congress of sorrows
-   etc

