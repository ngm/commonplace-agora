# Sisters With Transistors

A
: [[film]]

URL
: https://sisterswithtransistors.com/

It's a documentary on some of the early electronic music pioneers - I knew [[Delia Derbyshire]] and [[Wendy Carlos]] already but got introduced to some amazing others.

For example [[Daphne Oram]], [[Suzanne Ciani]] and [[Laurie Spiegel]].

