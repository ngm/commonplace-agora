# Digital Revolutions: Activism in the Internet Age

I got it from the Oxfam charity shop in Kendal.

A discussion of the role of technology in the 2011 uprisings.

From the foreword and intro, seems like it'll be pretty balanced and reflective.

-   p.7 Technology for activism throughout history.
-   The internet is a tool not a cause of revolution.

