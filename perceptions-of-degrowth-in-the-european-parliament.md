# Perceptions of degrowth in the European Parliament

URL
: https://www.nature.com/articles/s41893-023-01246-x

Pre-print available here:  https://www.researchgate.net/publication/375697646_Perceptions_of_degrowth_in_the_European_Parliament

I haven't read it yet, but will do.  They mention in the abstract that [[ecosocialism]] is one of the positions held by MEPs in the European Parliament, which is exciting and I want to learn more about.  They compare it alongside [[green growth]] and [[post-growth]] as positions held on [[growth]] and the environment within the EP.

Not sure I would concur that ecosocialism is "agnostic towards growth", as they say.  Plenty of ecosocialists are committed to [[degrowth]].

