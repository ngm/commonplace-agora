# Ethical issues in AI



## panel at MozFest

-   governance is law, education, culture
-   many industries started off unregulated and got better over time
    -   e.g. aviation, automative
    -   but don't want to wait 80 years for this to improve!
    -   (c.f. Future Histories on this)
-   explainability is really important factor
-   does the market value responsible AI?  in which ways can it be incentivised to do?
-   UN report on the [digital welfare state](https://www.theguardian.com/technology/2019/oct/16/digital-welfare-state-big-tech-allowed-to-target-and-surveil-the-poor-un-warns)

