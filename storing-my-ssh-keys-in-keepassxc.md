# Storing my SSH keys in KeePassXC



## Why?

-   to keep them safe and backed up: my keepass DBs are always backed up
-   to keep them easily synced between machines: my keepass DBs are always synced


## Why not?

-   with regards to syncing: you should probably have a different ssh key per device anyway, right?


## Resources

-   https://www.padok.fr/en/blog/ssh-keys-keepassxc

