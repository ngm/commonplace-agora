# Peter Jones and Kristel van Ael, "Design Journeys Through Complex Systems Practice Tools for Systemic Design"

URL
: https://newbooksnetwork.com/design-journeys-through-complex-systems

Really enjoyed this.  About the book [[Design Journeys through Complex Systems]] and the [[Systemic Design Toolkit]].

Loads of good stuff in here.

Seven stages.  Multiple useful tools within each stage.

