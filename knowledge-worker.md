# Knowledge worker

I don't really like the term.  [[Knowledge commoner]] might be better.

> If you had to set one metric to use as a leading indicator for yourself as a knowledge worker, the best I know might be the number of [[Evergreen notes]] written per day.
> 
> &#x2013; [Evergreen note-writing as fundamental unit of knowledge work](https://notes.andymatuschak.org/z3SjnvsB5aR2ddsycyXofbYR7fCxo7RmKW2be) 


## Hackers

I think that in McKenzie Wark's terms from [[A Hacker Manifesto]], a knowledge worker would be a [[Hacker]]?

> What does it mean to be a digital worker as opposed to, let's say, an analogue worker?  It's not like I can shove you on an assembly line.  When a digital worker's object is to come up with a new idea, it doesn't quite work that way.
> 
> &#x2013; McKenzie Wark, Stir to Action interview

