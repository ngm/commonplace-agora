# Elysium Fire

A
: [[book]]

Written by
: Alistair Reynolds

I liked this.  More of a detective story set in the future, than a deep thinking hard sci-fi.  A very well-written page turner.

Finished: September 2019.

