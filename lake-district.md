# Lake District

[[I love the Lake District]].

A view of the Langdale Pikes from Skelwith Fold.

![[2020-07-25_15-55-34_1539194616471.jpg]]


## 2020

I cycled up to the Lakes from Ulverston in August.

I went along the eastern shore of Lake Coniston.

![[photos/lakes-2020/next-to-coniston.jpg]]

I cycled (/pushed) up the Wrynose Pass&#x2026;

![[photos/lakes-2020/wrynose-pass.jpg]]

Then walked up to the top of Cold Pike, around the back of Pike O'Blisco.

![[photos/lakes-2020/behind-pike-o-blisco.jpg]]

![[photos/lakes-2020/behind-pike-o-blisco-2.jpg]]

Another day I went up [[Loughrigg]].

[[Loughrigg Tarn]].

Grasmere from Loughrigg.

![[photos/lakes-2020/grasmere-from-loughrigg.jpg]]

Windermere from Loughrigg.

![[photos/lakes-2020/windermere-from-loughrigg.jpg]]

