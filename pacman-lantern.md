# Pacman lantern

For [[Ulverston Lantern Festival]] 2022.

A paper lantern, structure made from willow withys.  He'll have a real candle in his main body, and has 3 red LEDs connected to a [[Micro Bit]] to the pills.

![[photos/pacman-lantern.jpg]]

The circuit is simple - it is 3 parallel circuits from pins 0 to 2, each one has a resistor and an LED, and they all go back to GND.

Here's the code:

```python
from microbit import *

presses = 0
mouth = pin0
middle = pin1
end = pin2

def set_lights(mouth_value, middle_value, end_value):
    mouth.write_digital(mouth_value) 
    middle.write_digital(middle_value) 
    end.write_digital(end_value) 
    sleep(pause) 

while True: 
    # can speed up the lights by pressing button a.
    presses = presses + button_a.get_presses()
    if presses > 10:
	presses = 0

    pause = (10 - presses) * 100

    # the pattern that we cycle through.
    set_lights(0, 0, 1)
    set_lights(0, 1, 0)
    set_lights(1, 0, 0)
    set_lights(0, 0, 0)
```

