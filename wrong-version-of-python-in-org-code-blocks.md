# Wrong version of Python in org code blocks

I was trying to use the `statistics` module.

But it was telling me it wasn't found.

It's a new thing in Python3.

I thought I had told org to use Python3 with `python-shell-interpreter`.
But no.

I checked with

```python
import platform
return platform.python_version()
```

and it was still version 2.

Turns out you also need: `org-babel-python-command` set to Python3 too.

See: [python - org-mode: how can i point to python3.5 in my org-mode doc? - Emacs S&#x2026;](https://emacs.stackexchange.com/questions/28184/org-mode-how-can-i-point-to-python3-5-in-my-org-mode-doc)

