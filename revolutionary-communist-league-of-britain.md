# Revolutionary Communist League of Britain

> In the RCLB, it was really important that we had this commitment to developing a programme; it’s that which really pushed us to struggle and resolve issues of line, and bring to fruition the rethink which opened up all these issues around gender, racism and the national question
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

