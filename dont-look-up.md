# Don't Look Up

A
: [[film]]

A comedic allegory for general societal indifference in the face of [[climate change]].

Reviews for it seem to be fairly mixed but I'm finding it laugh-out funny.

Also kind of sad.  It does a good job of bringing home the absurdity of much of modern activity in the face of our incipient self-inflicted extinction.

The "Don't look up" types in the film have a hint of [[climate sadism]] to them.

Then you have the [[ultra-rich nerd tycoon]] with some high-tech [[techno-solutionism]] that will save everyone, oh with a side bonus of bring vast wealth - to them, but with some trickle down, right?  And then showing true colours as a billionaire doomsday prepper when it goes wrong.   As well as having undue control over state policy.

