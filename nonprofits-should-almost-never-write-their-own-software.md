# Nonprofits should (almost) never write their own software

A
: [[claim]]

From the [[Aspiration Manifesto]].

Being the tech lead for a non-profit that develops our own software - I fully agree. We are currently going through a process of trying to divest as much bespoke code as possible to pre-existing (FLOSS) software.

One alternative I see, where no other software exists for the desired purpose, is for non-profits to perhaps be incubators for the software, but always with an intention to [[exit to community]] / exit the software to cooperative.

