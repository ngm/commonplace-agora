# Ant-Man

A
: [[film]]

Part of
: [[Marvel Cinematic Universe]]

[[Recidivism]]. Difficult for Scott Lang to reintegrate into society after being in prison. He ends up reoffending.

[[Precautionary principle]]. Hank Pym hides his work due to how dangerous it is. (After using it for himself for many years though?)

[[Redemption]]. Lang redeems himself in the eyes of others. By helping bring someone else to justice.

