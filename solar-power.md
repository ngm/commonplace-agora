# solar power



## Opposition

> The new environment secretary, Ranil Jayawardena, is understood to oppose solar panels being placed on agricultural land, arguing that it impedes his programme of growth and boosting food production
> 
> &#x2013; [Ministers hope to ban solar projects from most English farms | Solar power | &#x2026;](https://www.theguardian.com/environment/2022/oct/10/ministers-hope-to-ban-solar-projects-from-most-english-farms)

