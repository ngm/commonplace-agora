# ProtonMail Android app takes a long time to load messages

[[ProtonMail]] Android app takes ages to load up new emails.  Makes it kind of unusable. Frustrating, as this is an important service for me and one that I pay for.

Others seem to be having the same problem - https://github.com/ProtonMail/proton-mail-android/issues/165 - with no response from ProtonMail in over a month.

In the comments, someone suggests downgrading to an earlier version of the app.  https://github.com/ProtonMail/proton-mail-android/issues/165#issuecomment-1179731854

I'll try that when I get a minute.

OK, a few days later someone posted a fix: 
https://github.com/ProtonMail/proton-mail-android/issues/165#issuecomment-1186924180

> proton team got back to me suggesting to turn off alternative routing, which resolved the issue for me.

Works for me!

