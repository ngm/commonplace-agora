# Informatics of the Oppressed

URL
: https://logicmag.io/care/informatics-of-the-oppressed/

Author
: Rodrigo Ochigame

Looks at historic experiments in informatics from Cuban socialism and liberation theology for redistribution of informational wealth.

> Recent scholarship has documented the disastrous effects of “surveillance capitalism,” and in particular how commercial search engines deploy “algorithms of oppression” that reinforce racist and sexist patterns of exposure, invisibility, and marginalization.

Algorithms are currently tools of oppression against minorities.

> from the very beginnings of [[informatics]]—the science of information—as an institutionalized field in the 1960s, anti-capitalists have tried to imagine less oppressive, perhaps even liberatory, ways of indexing and searching information

However, have been attempts before to reverse this oppresion, and even liberate.

> Two Latin American social movements in particular—Cuban socialism and [[liberation theology]]—inspired experiments with different approaches to informatics from the 1960s to the 1980s.

<!--quoteend-->

> Taken together, these two historical moments can help us imagine new ways to organize information that threaten the capitalist status quo— **above all, by facilitating the wide circulation of the ideas of the oppressed**.


## The Redistributing of Informational Wealth

The redistribution of information wealth.  I very much like this phrase.

> on the distribution of publication counts: “They follow the same type of distribution as that of millionaires and peasants in a highly capitalistic society. A large share of wealth is in the hands of a very small number of extremely wealthy individuals, and a small residual share in the hands of the large number of minimal producers.”

Information not evenly distributed.  Feels quite related to [[Capital is Dead]].  Is [[Independent researcher]] a way to distribute information?

> Traditional informatics was incompatible with revolutionary librarianship because, by treating historically contingent regularities as immutable laws, it tended to perpetuate existing social inequalities.

Early example of biased [[algorithmic decision-making]] in a way.

> These claims are never innocent descriptions of how things simply are. Rather, these are interpretive, normative, politically consequential prescriptions of what information should be considered relevant or irrelevant. 

"prescriptions, disguised as descriptions" &lt;- good phrase.

> The innovative experiments by Cuban information scientists remind us that we can design alternative models and algorithms in order to disrupt, rather than perpetuate, patterns of inequality and oppression.


## A Network Theory of Liberation Theology

> The Medellín experience inspired a group of liberation theologians, largely from Brazil, to try to envision new forms of communication among poor and oppressed peoples across the world. Their objective was conscientização, or “conscientization”: the development of a critical consciousness involving reflection and action to transform social structures—a term associated with their colleague [[Paulo Freire]], who had developed a theory and practice of critical pedagogy.

<!--quoteend-->

> To address this problem, the liberation theologians and allied activists envisioned a system of information diffusion and circulation that they called an “intercommunication network.” This network would make available “information that was not manipulated and without intermediaries,” break down “sectoral, geographic, and hierarchical barriers,” and make possible “the discovery of situations deliberately not made public by controlled information systems.” 

<!--quoteend-->

> Since the modern internet was not yet available in the 1970s, the operation of the “intercommunication network” relied on print media and the postal service.

<!--quoteend-->

> For Whitaker, the concept Intercommunication sought to produce radical equality: “All must be able to speak and be listened to regardless of the hierarchical position, level of education or experience, social function or position, moral, intellectual, or political authority of each.” 


## The Freedom to be Heard

The compendium of compiled texts sounds fascinating, along with the indexing system.  A way of combining individual texts in to a greater piece, and already it to be searched in various ways.

> By building a distributed worldwide network via regional conferences, the liberation theologians had bypassed the central authority of the Vatican. 

<!--quoteend-->

> Techno-utopian conceptions of “information freedom,” whether in the Californian libertarian-capitalist version or in the Brazilian liberation-theological one, are never quite right.

^ Important point.

> The remarkable innovation of the Brazilian liberation theologians is that they moved beyond a narrow focus on free speech and toward a politics of audibility. The theologians understood that the problem is not just whether one is free to speak, but whose voices one can hear and which listeners one’s voice can reach.


## The Retrieval of History

> The reasons why some technologies live and others die are not strictly technical, but political.


## \* Critical search

[[Critical search]] would actively strive to increase the visibility of counterhegemonic intellectual traditions and of historically marginalized perspectives. We must build systems of information diffusion and circulation that seek to amplify critical voices and to cut across linguistic, national, racial, gender, and class barriers. 

