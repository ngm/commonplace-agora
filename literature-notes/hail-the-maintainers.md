# Hail the maintainers

[Innovation is overvalued. Maintenance often matters more | Aeon Essays](https://aeon.co/essays/innovation-is-overvalued-maintenance-often-matters-more).

> Capitalism excels at innovation but is failing at maintenance, and for most lives it is maintenance that matters more.

