# Can municipal socialism be Labour’s defence against ongoing austerity?

URL
: https://www.mutualinterest.coop/2020/03/can-municipal-socialism-be-labours-defence-against-ongoing-austerity

> Key to these models is the belief that community and worker, not state, ownership is the key to building a better economy. 

<!--quoteend-->

> the focus is the growth of the co-operative economy as a way to fight off the worst effects of austerity by helping improve the economy and increased local engagement through democratisation of the economy.

<!--quoteend-->

> A more exclusive focus on building the co-operative is being started in Greater Manchester where Metro Mayor Andy Burnham has launched the [[Greater Manchester co-operative agency]].

<!--quoteend-->

> in [[Emilia Romagna]] in Italy co-operative enterprises generate close to 40% of GDP.

<!--quoteend-->

> Emilia Romagna is helped by a much better legal framework for the establishment and development of co-operatives. The [[Marcora Law]] gives workers the right to buy and convert a company should it plan to close, and has been credited to have saved around 14,500 jobs in the country. 

