# A Cooperative Vision for Technological Innovation w/ Dan Hind

URL
: https://www.buzzsprout.com/1004689/3943718-a-cooperative-vision-for-technological-innovation-w-dan-hind

Publisher
: [[Tech Won't Save Us]]
    
    Really like this interview with [[Dan Hind]].
    
    Discusses his ideas around the [[British Digital Cooperative]].

Great interview, chimes a lot with my vision for municipalist technology.

