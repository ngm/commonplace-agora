# structural violence

> a form of violence wherein some social structure or social institution may harm people by preventing them from meeting their basic needs. 
> 
> &#x2013; [Structural violence - Wikipedia](https://en.wikipedia.org/wiki/Structural_violence)  

<!--quoteend-->

> Some examples of structural violence as proposed by Galtung include institutionalized racism, sexism, and classism, among others
> 
> &#x2013; [Structural violence - Wikipedia](https://en.wikipedia.org/wiki/Structural_violence)  

