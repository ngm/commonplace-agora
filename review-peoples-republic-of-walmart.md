# Review: People's Republic of Walmart

An
: [[article]]

URL
: http://socialistplanning.org/posts/review-peoples-republic

Author
: Bjorn Westergard

[[The People's Republic of Walmart]].
[[socialist calculation debate]].  [[Friedrich Hayek]]. [[Ludwig von Mises]].

Informative article.

It seems to disagree with most of the propositions of the book.  I **think** the author is socialist (to help place where the disagreement is coming from).

Gives some handy background to the key points of the socialist calculation debate.

