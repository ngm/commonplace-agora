# Adding more storage space to YunoHost / NextCloud on Hetzner

Tags
: [[Sysadmin]]

I installed PhotoPrism.  Now I want to point it at the photos that are uploaded via InstantUpload to [[NextCloud]] from my phone.

But then I realise I've only 2Gb left on my server.  I think PhotoPrism will take up extra space with thumbnails and resizes and whatnot.

The easy way to increase storage of my server is to rescale it.  But that's the expensive way also.  The sensible way to do it is to add a volume.  It's much cheaper and makes logical sense too.

But I'll have to dig in to it as to how you a) make that volume available to yunohost; b) make that something that NextCloud can see; c) make that somewhere that InstantUpload can upload to; d) make it so photoprism can work with it too.


## Add volume to Hetzner

-   [Overview - Hetzner Docs](https://docs.hetzner.com/cloud/volumes/overview/)

This was easy.

Huh, but it turns out that volumes are not backup up like you can get your server backed up.  That's a shame.  https://github.com/hetznercloud/csi-driver/issues/79


## Get YunoHost aware of external storage

-   [Adding an external storage to your server | Yunohost Documentation](https://yunohost.org/en/external_storage)

Didn't need to do any of this - adding the volume via Hetzner sets up all the mounting that you need.


## Make Nextcloud aware of it

-[Configuring External Storage (GUI) — Nextcloud latest Administration Manual l&#x2026;](https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/external_storage_configuration_gui.html)  

This is easy too.  Just go to Settings / Administration / External Storage and add the folder.

HOWEVER: Nextcloud doesn't have perms to do anything with it.  So I logged in to the server and created a subfolder in the mount with user and group as `nextcloud`.  Dunno if this is the best way to do it, but I can write to that subfolder from the Nextcloud app now. 


## Change Instant Upload location

-   [nextcloud/android#405 Change default InstantUpload location](https://github.com/nextcloud/android/issues/405)

Not looked at this yet - for now I'll just periodically move files from InstantUpload to the new mounted folder.


## PhotoPrism

-   

https://www.reddit.com/r/photoprism/comments/q5oq33/how_do_i_change_my_storage_folder/

It seems like this is an **installation** option&#x2026; that's annoying.

I did find a settings.yml file, but change the values didn't seem to do much.  Maybe I could set up symlinks.

