# Is there a connection between prefiguration and vanguardism?

Reading [[Vanguard Stacks: Self-Governing against Digital Colonialism]] make me think about this.

They both involve some kind of revolutionary acts in advance of the actual full revolution, right.  [[Vanguardism]] is your Marxist approach, [[Prefiguration]] your more anarchist approach.

From a bit of reading around, there is certainly a connection.  It is mostly proclamations of prefiguration as a better alternative to vanguardism.

The counterpart of the vanguard party is the catalyst group.

I think that [[vanguardism]] is more about having some professional group of revolutionaries who lead the charge.  And [[Prefiguration]] is more about living the way now that you want to see the future be.

> The politics of prefiguration rejected the centrism and vanguardism of many of the groups and political parties of the 1960s. 
> 
> &#x2013; [Prefigurative politics - Wikipedia](https://en.wikipedia.org/wiki/Prefigurative_politics) 


## One view

Clearly biased towards prefiguration, seeing vanguardism as authoritarian.

> This sort of organizing council, composed of ideological anarchists and libertarian socialists that has the goal of spurring on further radicalization and prefiguration, is called a [[catalyst group]]. It stands in contrast to the vanguard model of the authoritarians.
> 
> &#x2013; [Constructing the Revolution](https://www.thecommoner.org.uk/constructing-the-revolution/) 

<!--quoteend-->

> And if such a group does not view itself as accountable to and consisting of the community it is embedded in, it will often have a tendency to become a sort of violent vanguard party, conscripting the masses into an insurrection under their hierarchical control.
> 
> &#x2013; [Constructing the Revolution](https://www.thecommoner.org.uk/constructing-the-revolution/) 


## Another view

Also biased towards prefiguration and against vanguardism it would seem.

> In the times depleted of futurity in which we live, the role of prefigurative practices stands to become a vital ingredient to any ambitious political project that wants to stay clear of vanguardism.
> 
> &#x2013;  https://mycourses.aalto.fi/pluginfile.php/565674/mod_resource/content/1/Prefigurative_Practices.pdf 

