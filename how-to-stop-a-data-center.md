# How to stop a data center

Found at
: https://disconnect.blog/how-to-stop-a-data-center/?ref=disconnect-newsletter

How organisers in Santiago stopped the development of a data centre in their locality.

Seemed to be primarily information campaigns around water usage.

Non-violent - compare to [[How to Blow Up a Data Centre]].

