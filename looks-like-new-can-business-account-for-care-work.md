# Looks Like New: Can Business Account for Care Work?

URL
: https://news.kgnu.org/2019/11/looks-like-new-can-business-account-for-care-work/

Featuring
: [[Stacco Troncoso]] / [[Nathan Schneider]]

Summary
: > This month, Spanish activist Stacco Troncoso and his collaborators released the [[DisCO Manifesto]] (disco.coop), which proposes a new model for organizations that account for the care work that supports workers and their communities. In this conversation, he explains how the model works and how it challenges norms, both in familiar workplaces and in techno-utopian visions. Troncoso is a founding member of the [[Guerrilla Media Collective]], which is pioneering the [[DisCO]] concept.
    
    I liked this a lot, quoted by Stacco:
    
    > Large-scale problems do not require large-scale solutions.
    > They require small-scale solutions within a large-scale framework.
    > 
    > &#x2013; [[David Fleming]]

