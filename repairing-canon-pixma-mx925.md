# Repairing Canon Pixma MX925

-   may loose connection on the cable?
-   one of the LEDs comes on
-   but screen not coming
-   just stopped working
-   left on in sleep mode in between prints
    
    > To try and resolve this issue, please unplug the power from the unit for about 25-30 minutes.  Once this time frame has passed, plug the unit in directly to a wall outlet to see if the display will power up properly.
    
    It's been off for more than 30 minutes&#x2026;


## Resources

-   [SOLVED: Why is my mx922 not powering up? - Canon Printer - iFixit](https://www.ifixit.com/Answers/View/348152/Why+is+my+mx922+not+powering+up)
-   [MY PRINTER JUST STOPPED. COPY LIGHT IS ON BUT SCRE&#x2026; - Canon Community](https://community.usa.canon.com/t5/Desktop-Inkjet-Printers/MY-PRINTER-JUST-STOPPED-COPY-LIGHT-IS-ON-BUT-SCREEN-WON-T-LIGHT/m-p/183584)
-   [Pixma mx922 not powering on - Canon Community](https://community.usa.canon.com/t5/Desktop-Inkjet-Printers/Pixma-mx922-not-powering-on/m-p/312810)

