# universal care

> But we also want to take this theory of care further, to promote the idea of ‘universal care’: the ideal of a society in which care is front and centre at every scale of life and in which we are all jointly responsible, for hands-on care work as well as the care work necessary for the maintenance of communities and the world itself
> 
> &#x2013; [[The Care Manifesto]]

