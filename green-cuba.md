# Green Cuba

> While Socialist states have been criticised on their ecological policies during the 20th century, [[Cuba]] has proved a sharp exception. Since the early 1990s, Cuba has pursued policies to drastically reduced climate change emissions and to protect the environment in a variety of ways.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> The reason for Cuba’s overt and strong turn towards environmental protection is twofold. The collapse of the Soviet Union meant that Cuba was no longer supplied with cheap oil after 1990. This led to a severe crisis, in the context of a continuing US blockade, resulting in what has been termed the ‘[[Special Period]]’. Thus a sharp reduction in the consumption of oil was vital so as to ensure the survival of Cuban society
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> In turn, and apparently irrespective of this necessity, Fidel Castro became deeply engaged in ecological concerns and debates. At the 1992 UN Rio conference on the international environment he made the case for green policies,
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

