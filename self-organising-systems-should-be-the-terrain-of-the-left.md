# self-organising systems should be the terrain of the Left

A
: [[claim]]

> The fundamental point is that self-organising systems should be the terrain of the Left, but somehow (mostly due to the stance of competing with capitalism at being better at central planning), capitalism has been allowed to appropriate this sphere.
> 
> &#x2013; [[The Entropy of Capitalism]]


## Because

&#x2026;

