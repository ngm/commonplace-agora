# New Economics Podcast: Should we be going for growth?

A
: [[podcast]]

Good discussion.

[[Growth]]. [[GDP]]. [[Degrowth]]. [[Green growth]]. Decoupling. [[Doughnut economics]]. Policies for alternatives to growth. [[Growthism]]. Growth is structurally baked in - how to change that?

