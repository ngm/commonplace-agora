# Exobrain

An externalisation of thoughts.  Less sci-fi: a [[personal wiki]].

![[2020-07-18_22-02-10_ArtificialFictionBrain.png "Artificial Fiction Brain [CC-BY-SA](https://commons.wikimedia.org/wiki/File:ArtificialFictionBrain.png)"]]

> I aim for my wiki to be a kind of exobraindump (uploading my mind into heavily linked text), a model of myself (PSM). The wiki is a story-telling device that reuses older stories to reconstruct new ones; if it is a memory palace or garden, it's one that is evolving toward externalizing the relationship between our sensibilities and cognition (for the sake of something else).
> 
> &#x2013; [⦗ℍ𝕪𝕡𝕖𝕣𝔱𝔢𝔵𝔱: h0p3⦘ — ‍ ‍ ‍‍ ‍‍‍ ‍‍‍ ‍ ‍‍ ‍‍ ‍ ‍‍ ‍𖡶 1.2.20200719](https://philosopher.life/#2020.04.25%20-%20Indieweb%3A%20Wiki%20Event%20Notes)

-   [[Building a second brain]]
-   [[Be Right Back]]
-   [beepb00p's exobrain](https://beepb00p.xyz/exobrain/index.html)

