# community gardens

> The communal and social benefits of gardening, as well as the nascent political potential of gardens and other common urban spaces, are often celebrated. Researchers looking at Glasgow’s community gardens saw the gardens as spaces of social transformation that held the potential for a new political practice. “Enabled by an interlocking process of community and spatial production, this form of citizen participation encourages us to reconsider our relationships with one another, our environment, and what constitutes effective political practice.”
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

