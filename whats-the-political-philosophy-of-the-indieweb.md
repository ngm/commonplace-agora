# What's the political philosophy of the IndieWeb?

This is a really fun listen from Doug Belshaw.  He discusses [[IndieWeb]] and the issues he sees with it.

[Microcast #081 - Anarchy, Federation, and the IndieWeb - Doug Belshaw's Thoug&#x2026;](https://thoughtshrapnel.com/2020/01/01/microcast-81/)

Doug has a preference for the Fediverse as an approach to an open web, and says the [[political philosophy]]of the IndieWeb is a type of right-libertarianism, because it lacks social equality, and without that it is just a focus on individual freedom.

My gut response is that I disagree and that it's all a bit more nuanced than that.  But it's a good jumping off point for discussion!

IndieWeb and federation.  Geeky, white and male.

Bit of a superficial summary of IndieWeb&#x2026;

Likes the practical side of it.

Doug doesn't think it will scale.  More enthusiastic about federation.

Definition in opposition to a thing is not a good idea.

Political philosophy&#x2026;

Anarchism.

Left-libertarianism.  Individual freedom and social equality.  Anti-authoritarianism, social anarchism.

Social equality aspect is missing.  Collective ownership is missing.  And therefore compatible with neoliberalism.  

People have to be equipped for the journey.  "Bring your own infrastructure" (mantra of Indieweb?) - individualist rather than collectivist.

Doug doesn't like being a sysadmin.

He doesn't mind it, but doesn't think it is socially focussed.

