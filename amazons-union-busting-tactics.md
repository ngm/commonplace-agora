# Amazon's union-busting tactics

[[Amazon]]. [[Union-busting]].

> Many of the detailed proposals set out in this week’s consultation are designed to prevent a repeat of the union-busting tactics Amazon deployed to fight off a recognition bid by the GMB union at its Coventry warehouse earlier this year.
> 
> They include provisions to prevent employers going on a hiring spree purely in order to dilute union membership and thwart a recognition bid.
> 
> &#x2013; https://www.ft.com/content/bb85e4f1-523c-4868-8fd0-06fbe83ddd21 (via [[Foxglove]] newsletter)

