# The Received Wisdom

-   A [[podcast]].
-   https://shobitap.org/the-received-wisdom
-   The Received Wisdom is a podcast about how to realize the potential of science and technology by challenging the received wisdom
-   [[Shobita Parthasarathy]] / [[Jack Stilgoe]]
-   [[Science, technology and society]]

