# State power

> going beyond” the nation-state doesn’t mean “without the nation-state.” It means that we must seriously alter state power by introducing new operational logics and institutional players.
> 
> &#x2013; [[Free, Fair and Alive]]

