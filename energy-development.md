# energy development

> Energy development is the field of activities focused on obtaining sources of energy from natural resources. These activities include production of [[renewable]], nuclear, and fossil fuel derived sources of energy, and for the recovery and reuse of energy that would otherwise be wasted.
> 
> &#x2013; Wikipedia

