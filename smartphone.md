# Smartphone

AKA 'pocket computer' (I heard this name used first by the technology writer Bill Thompson)

I generally find smartphones remarkably useful.  But their environmental and social impact is huge.  I try to keep them lasting as long as possible, and only buy second-hand/refurbished.


## What it is made up of

> Beneath the screen, nestled within a snug enclosure, are the components that permit the smartphone to receive, transmit, process and store information. Chief among these are a multi-core central processing unit; a few gigabits of nonvolatile storage (and how soon that “giga-” will sound quaint); and one or more ancillary chips dedicated to specialized functions. Among the latter are the baseband processor, which manages communication via the phone’s multiple antennae; light and proximity sensors; perhaps a graphics processing unit; and, of increasing importance, a dedicated machine-learning coprocessor, to aid in tasks like speech recognition.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Thanks to its Assisted GPS chip—and, of course, the quarter-trillion-dollar constellation of GPS satellites in their orbits twenty million meters above the Earth—the smartphone knows where it is at all times.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> This machinic sense of place is further refined by the operation of a magnetometer and a three-axis microelectromechanical accelerometer: a compass and gyroscope that together allow the device to register the bearer’s location, orientation and inclination to a very high degree of precision.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> A small motor allows the phone to produce vibrating alerts when set in silent mode; it may, as well, be able to provide so-called “haptics,” or brief and delicately calibrated buzzes that simulate the sensation of pressing a physical button.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Wound around these modules, or molded into the chassis itself, are the radio antennae critical to the smartphone’s basic functionality: separate ones for transmission and reception via cellular and WiFi networks, an additional Bluetooth antenna to accommodate short-range communication and coupling to accessories, and perhaps a near-field communication (NFC) antenna for payments and other ultra-short-range interactions. This last item is what accounts for the smartphone’s increasing ability to mediate everyday urban interactions; it’s what lets you tap your way onto a bus or use the phone to pay for a cup of coffee.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> Finally, all of these components are arrayed on a high-density interconnect circuit board, and powered by a rechargeable lithium-ion or lithium-polymer battery capable of sustaining roughly 1,500 charging cycles. This will yield just about four years of use, given the need to charge the phone daily, though experience suggests that few of us will retain a given handset that long.
> 
> &#x2013; [[Radical Technologies]]


## What it represents

> this light, we can see the handset for what it truly is: an aperture onto the interlocking mesh of technical, financial, legal and operational arrangements that constitutes a contemporary device and service ecosystem
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> And given that it is, in principle, able to connect billions of human beings with one another and the species’ entire stock of collective knowledge, it is in some sense even a utopian one.
> 
> &#x2013; [[Radical Technologies]]


## What is behind its manufacture

> Behind every handset is another story: that of the labor arrangement, [[supply chains]] and flows of capital that we implicate ourselves in from the moment we purchase one, even before switching it on for the first time
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> To function at all, the smartphone—like all electronic devices—requires raw materials that have been wrested from the Earth by ruthlessly extractive industries.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> The damage caused by the processes of extraction fans out across most of a hemisphere, mutilating lives, human communities and natural ecosystems beyond ready numbering
> 
> &#x2013; [[Radical Technologies]]

