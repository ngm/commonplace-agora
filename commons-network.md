# Commons Network

https://www.commonsnetwork.org/

> Commons Network is a collaboratory for the social and ecological transition. We bring together people and ideas and we provide tools and insights for social movements, governments and community groups. We explore new models for economy and society in order to collectively transform the system and shape a caring and just future.

