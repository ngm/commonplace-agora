# Late Devonian extinction

> The trigger for that ancient apocalypse was the plant kingdom’s conquest of land
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Apart from fungi and cyanobacteria, life had been confined to the seas, but newly evolved vascular plants could withstand the elemental rigours of earth and air
> 
> [[Half-Earth Socialism]]

