# Evolutionary and adaptive systems

I did a Masters degree in Evolutionary and Adaptive Systems (AKA EASy) at the University of Sussex in 2007/2008.

I got into the area through an interest in [[agent-based modelling]] and [[artificial life]] (through things like [[Lindenmeyer systems]] and [[cellular automata]].  I did a project on [[agent-based modelling of predator-prey dynamics]] in the final year of my undergraduate degree.)

The science of [[evolution]] and [[adaptation]] was less my strong point, and I focused a little more on the [[biologically-inspired computing]] side of things. I really enjoyed the coding of [[agent-based models]].

The degree covered a lot of things, in a nutshell I would say the main part that appealed to me was the idea of there being no Gods, no central planners, just things evolving over time, and individuals interacting with each other causing patterns to emerge that noone really 'built in' or planned for.  This is a thread that lives me still, in an interest in [[Decentralisation]], both technological and political.

A really lovely example of complexity and [[emergence]] is [[murmurations]] of starlings, when you get a huge flock of them in the sky, they manage to stay together as a coherent whole, and these beautiful shapes and patterns form as they fly around with each other, but there's no 'head bird' so to speak, noone choreographing everything.  And so that kind of swarming behaviour has been modelled and used in computer graphics in films for example, to make birds or bats or shoals of fish look realistic.

My final project on the Masters was working on some software ([[NeurAnim]]) to visualise artificial neural networks.

One of my projects (not a great one to be honest, but I still quite like the idea) was based around some kind of algorithm based on fish swimming that worked as an information retrieval mechanism for you.  I still like this idea - [[info daemons]].

I enjoyed a side module in [[generative art]] while there too.

As far as I understand, evolutionary and adapative systems are a subset of [[complex systems]], and more specifically probably a subset of [[complex adaptive systems]].

