# Red Enlightenment (podcast)

Found at
: https://repeater-radio.com/shows/red-enlightenment/

Featuring
: [[Graham Jones]]

> Red Enlightenment is an 8-part exploration of [[socialism]], [[science]], and [[spirituality]].

In the first episode, he talks about the [[Enlightenment]] a bit, and gives it a good critique.

