# The government's measures to address the rising cost of energy bills go nowhere near far enough

[[Britain]]. [[2022]]. [[Cost of living crisis]]. [[Britain has soaring energy bills]].

[The Observer view on Britain’s growing cost of living crisis | Observer edito&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/06/observer-view-on-britain-cost-of-living-crisis)

-   [[Rishi Sunak]], has chosen to prioritise Conservative voters – and hence his own leadership prospects
-   The measures are a flat-rate, poorly targeted council tax rebate that 80% of households will benefit from
-   More than 40% of its value will go to households in the top half of the income distribution
-   While more than 600,000 low-income households will miss out
-   The £200 rebate for all households will be taxed back through a £40-a-year surcharge for the next five years, which will push today’s cost pressures into the future.


## And

-   Low-income families have been at the sharp end of tax credit cuts and benefit freezes over the last decade, with many low-paid parents losing thousands of pounds a year as a result.
-   The savings paid for income tax cuts that disproportionately benefited better-off households;
    -   they were a political choice made by successive Conservative chancellors, supported between 2010 and 2015 by the Liberal Democrats.
-   The cuts have left less affluent households particularly vulnerable to this cost-of-living squeeze
    -   and are a large part of why [[child poverty]] rates have risen and growing numbers of people are using [[food banks]].


## So

-   Government support to help with rising energy bills should be targeted at those who need it most, through the tax credit and benefit system

