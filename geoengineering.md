# Geoengineering

Deliberate attempts to modify and control the Earth's [[climate system]].  


## Geoengineering and the left

Some on the eco-left are for it; some not.  Generally seems like a bad idea to me, but some argue it is necessary to mitigate climate breakdown now that we've let it get so far.


### For

> In 2013, ‘accelerationists’ [[Alex Williams]] and [[Nick Srnicek]] called for a ‘Promethean politics of maximal mastery over society and its environment’.
> 
> &#x2013; [[Half-Earth Socialism]]

I think Srnicek and Williams moved on quite a bit from their 2013 take.  Would be interesting to see if they mentioned geoengineering in [[Inventing the Future]].

> Four years later, [[Jacobin]] published a special issue on the environment which included essays praising geoengineering and nuclear power to undergird cornucopian communism.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> [[Holly Jean Buck]], a socialist and self-described ‘geoengineer’, warns the Left that entrepreneurs who are ‘taking action, being creative, or disrupting’ are ‘the wrong focus of critique’. She speculates that one day there might be carbon-credit ‘gift cards’ and an AI-controlled SRM (a true Skynet).
> 
> &#x2013; [[Half-Earth Socialism]]

I haven't read it yet, but I would guess that [[Fully Automated Luxury Communism]] by Aaron Bastani would be pro-geoengineering to some degree.


### Against

> geoengineering, it seems, had always been a form of planetary class war.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Fossil-fuel companies, conservative think tanks, and economics departments, after all, had been among the earliest supporters of geoengineering.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> In an era of climate change, this logic leads to geoengineering despite its manifest threat to the essential and extremely complex Earth system. Neoliberals willingly gamble with something as risky as SRM rather than countenance restrictions on their revered market.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> To get a sense of how foolish it is to think that privatized geoengineering will produce an optimal climate, it is worth remembering the shock of the [[ozone hole]].
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> The ozone crisis is not just a useful allegory to warn against an overweening geoengineering programme but is directly relevant because SRM might itself damage the ozone layer.
> 
> &#x2013; [[Half-Earth Socialism]]

