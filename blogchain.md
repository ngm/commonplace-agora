# Blogchain

A string of short, ad-hoc posts in a blog that together build on a theme.

I came across the idea of blogchains on [Tom Critchlow's blog](https://tomcritchlow.com/2019/07/17/blogchains/) I believe.  The word is from [Venkatesh Rao](https://www.ribbonfarm.com/2019/03/21/constructions-in-magical-thinking/), and the very tl;dr is that it's a string of short, ad-hoc blog posts that build on a theme.  That's cool, and tied in with a wiki is kind of how I see me builing up ideas over time.  

But where the idea gets really interesting (for me) is when it extends to [cross-site blogchains](https://tomcritchlow.com/blogchains/networked-communities/) and [open blogchains](https://tomcritchlow.com/2019/10/31/new-blogging-2/).  These are more open-ended, involving two or more people conversing and building on a theme, simply by posting to their blog about it and linking the posts together.  Kind of a webring, but for posts rather than sites.

There's definitely something to be said for the long-form, turn-based conversation.  One of the best conversations I have had recently was a long email chain.  And some of the thoughts that have stuck with me the most are ones I've written as a long reply to someone else's open question or musings on a topic.

> 1.  Slouching towards mediocrity and allowing myself the freedom of short-ish posts. Venkatesh aims for 300 words. Seems like a good number.
> 
> 2.  Infinite, open-ended world building.
> 
> https://tomcritchlow.com/2019/07/17/blogchains/

<!--quoteend-->

> A blogchain is longform by other means. Containerized longform if you like. A themed blog-within-a-blog, built as a series of short, ideally fixed-length posts (we’re trying to standardize on 300 words as one container size). 
> 
> https://www.ribbonfarm.com/2019/03/21/constructions-in-magical-thinking/

