# iPhone 13

> In fact, it's quite disturbing the lengths that Apple has gone to in order to retain control over your iPhone.
> 
> It's become so bad that you can't swap genuine parts from one iPhone with parts out of another iPhone. 
> 
> &#x2013; [Your new iPhone 13 doesn't belong to you | ZDNet](https://www.zdnet.com/article/your-new-iphone-13-doesnt-belong-to-you/) 

<!--quoteend-->

> The iPhone 13 is about as close to being unrepairable by third-party repairers as it could be. In fact, given that the display and the battery are the two parts that are most likely to need attention, the iPhone 13 is functionally irreparable by third-party repair shops. 
> 
> &#x2013; [Your new iPhone 13 doesn't belong to you | ZDNet](https://www.zdnet.com/article/your-new-iphone-13-doesnt-belong-to-you/) 

