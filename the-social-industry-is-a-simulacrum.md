# The social industry is a simulacrum

In a bit towards the end of [this podcast](https://play.acast.com/s/intelligencesquared/037b5e3c-6692-4241-a900-3cf88f60a09e), [[Richard Seymour]] describes the [[social industry]] as a [[simulacrum]], and recommends reading [[Simulacra and Simulation]] by Baudrillard if you're interested in this sort of stuff.  

![[The_social_industry_is_a_simulacrum/2020-05-24_12-14-44_Simulacres_et_Simulation.jpg]]

Okey doke, it was already half on the reading list, bumped up to full member. Coincidentally, that's another one for sure on my brother's old treasure trove of a bookshelf back home.  

