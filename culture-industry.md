# Culture industry

![[images/culture-industry.png]]

[[Theodor Adorno]].

The culture industry contributes to [[alienation]].


## Work and consume

Work and consume, work and consume.

40 hour work week makes you a commodity. Being a consumer is the flipside of being a worker. The cultural products are designed to make you into a better worker.  They are for people who are tired of work and need some break before going back to work again.  It makes no demands on you mentally. Consumption is just the logic of work imprinted onto leisure time. ([The Partially Examind Life:Episode 136: Adorno on the Culture Industry](https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/))


## Industry

Adorno very explicitly wanted it referred to as a culture **industry**, to make clear it was driven from above and imposed.  Not a natural representation of the desires of the masses. ([The Partially Examind Life:Episode 136: Adorno on the Culture Industry](https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/)  [00:08:14])


## Fantasy of rebellion

You're consuming a fantasy of rebellion, a substitute gratification, you're not actually rebelling. ([The Partially Examind Life:Episode 136: Adorno on the Culture Industry](https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/)  [00:40:00])

Within the culture industry you can totally have a film that inveighs against capitalism.  (e.g. [[Jurassic World]]!)  ([The Partially Examind Life:Episode 136: Adorno on the Culture Industry](https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/)  [00:37:20])


## Resources

-   [[The Pervert's Guide to Cinema]].

-[The Partially Examind Life:Episode 136: Adorno on the Culture Industry](https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/)

-   [Episode 110 – The Frankfurt School pt. 3 – The Culture Industry – Philosophiz&#x2026;](http://philosophizethis.org/the-culture-industry/)

