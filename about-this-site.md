# About this site

This is my [[hyper commonplace garden wiki]].  I started it (in this format) in [[October 2019]].

It is a companion to my [blog](https://doubleloop.net).  They are [[the Garden and the Stream]].

Please feel free to click around here and explore.  Don't expect too much in the way coherence or permanence&#x2026; it is a lot of half-baked ideas, badly organised.  The very purpose is for snippets to percolate and  morph and evolve over time, and it's possible (quite likely) that pages will move around.  

That said, I make it public in the interest of info-sharing, and occassionally it is quite useful to have a public place to refer someone to an idea-in-progress of mine.

Some more info on the [[whats and the whys]].

