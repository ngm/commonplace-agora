# Solarpunk

[[I like solarpunk]].


## Overview

-   an [[eco-futurist]] movement
-   imagining a future most people would actually like to live in
    -   Instead of ones we should be trying to avoid.
-   the future as bright, green and sustainable
-   "sustainable" at a not-just-for-rich-people level
-   "[[renewable energy]], reusable infrastructure, an end to throwaway culture, room for human dignity, and the possibility for continued flourishing"
-   "rebellion against the structural pessimism in our late visions of how the future will be. "
-   powered by [[green energy]]
-   "prefiguring the world to be created, through science-fiction and fantasy literature, arts, fashion, filmmaking, music, games, and a set of ideas which inform political, economic, and ecological activism"
-   "the Positive Articulation of a Better World "
-   "Not content to accept the dictates of a tomorrow ruled by authoritarian states, rapacious corporations, and a despoiled biosphere"


## Lineage/context

> If cyberpunk was ‘here is this future that we see coming and we don’t like it’, and steampunk is ‘here’s yesterday’s future that we wish we had’, then solarpunk might be ‘here’s a future that we can want and we might actually be able to get.’


## Participants

-   "writers, artists, environmentalists, engineers, scientists, and others"


## Politics


### Oppositional


#### Hence the 'punk'

> Punk is more of an ethos than a specific set of signifiers, implying rebellion against, and negation of, the dominant paradigm and everything repressive about it


### Combo of localism and globalism


#### [[Localism]]

> one of the healthy things about local resilience is that it puts you in a much better bargaining position against the people who might want to shut you off


#### Globalism

> it’s an opposition that begins with infrastructure as a form of resistance


### Beyond war, domination, and artificial scarcity


### Practical present examples

-   [[Worker cooperatives]]

-   Self-sufficient eco-communities

-   directly-democratic popular assemblies

-   voluntary federations of small polities

-   mutual aid networks

-   [[community land trusts]]


## Culture

-   "driven by an overriding ethos of compassionate rationalism, where science and reason are not seen as antithetical to imagination and spirituality, but as concepts which bring out the best in each other"

-   Culture founded on radical inclusiveness, unity-in-diversity, free cooperation, participatory democracy, and personal self-realisation

> "societies of polycultural ethnic diversity and gender liberation, where each person is able to actualise themselves in societal environment of free experimentation and communal caring"


## Technology

-   'Solar' is a proxy for clean abundant energy
-   [[Post-scarcity]]
-   [[Jugaad]] "an improvised solution born from ingenuity and cleverness"
    -   Creative re-use of existing infrastructure
-   High tech back ends with simple outputs
-   "decentralised eco-cities, 3D printing, vertical farms, solar glass windows"
-   Automation of human labour
-   Technology that is restorative
    -   Examples
        -   "solar and wind and wave energy, 3D printing, vertical farming, micro-manufacturing, free software, open-source hardware, and robotic machinery which can automate away human"


## Aesthetics

-   Not too sure about these
-   Art nouveau
-   It should make space for multiple aesthetics
-   "wild or inventive forms of dress and design"
-   "vibrant cosmopolitan aesthetic"


## Notes/criticism


### Where does science fit in?


#### Ingenuity should not be in adversity to institutionalised science

-   I think it fits quite well with Inventing the Future
-   "sometimes “positive futures” are framed in terms of technological megaprojects, which has a whiff of the Promethean about it"
-   "crafting positive or utopian futures is really hard and valuable work at this particular moment"
-   Critique solarpunk from [[Inventing the Future]] perspective
-   Which country is closest to solarpunk already?


## Random thoughts


### Solar punk combines well with [[afro-futurism]]

-   in a world of solar, sun-rich countries will prosper


## Writing

-   Solar punk with a gods kind of slant?
-   Tempus is a good character name


## Resources

-   [[Solarpunk books]]
-   [[Solarpunk movies]]
-   [[A Solarpunk Manifesto]]

