# The Internet as a Super-Commons

URL
: http://www.bollier.org/week-9-internet-super-commons

Featuring
: [[David Bollier]]

Podcast about the internet, the web, and [[Libre software]] from the perspective of the [[Commons]].

From 2010 so some thinking might have moved on.  But still a good history.

There's certainly more to be added now to ideas from e.g. [[The Cathedral and the Bazaar]].  For example [[Post-Open Source]].

[[Architecture is politics]].  [[Anarchism Triumphant: Free Software and the Death of Copyright]].

