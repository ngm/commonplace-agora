# Upgrading Linux Mint 20 to 20.3

Upgrading [[Linux Mint]] from 20 to 20.3.

I've just done [[Upgrading Linux Mint 19.3 to Linux Mint 20]]. Might as well keep going to 20.3 straight away.  Could let stuff bed in, but to be honest prefer just to iron out any issues all in one go.

This one should be a lot simpler than 19.3 to 20.  Just let the updater do it's thang in the background.

-&#x2014;


## <span class="timestamp-wrapper"><span class="timestamp">[2022-12-18 Sun 12:25]</span></span>

Yep, that all seemed to work absolutely fine.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-12-18 Sun 12:30]</span></span>

I note that `keepassxc` is still a really old version in the Mint repos.  So I'll upgrade it from a PPA again I guess.


## <span class="timestamp-wrapper"><span class="timestamp">[2022-12-21 Wed]</span></span>

When I start `KoHighlights`, I get lots of fontconfig errors and warnings like:

> Fontconfig error: "/etc/fonts/conf.d/10-hinting-slight.conf", line 5: invalid attribute 'translate'

And it starts up with an odd looking font.  I guess I'm missing some fonts.

Plenty of reports on t'interwebs about similar errors, but no quick fixtures.

