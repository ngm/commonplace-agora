# Taking fleeting notes with koreader

Looking at how to add and sync basic [[fleeting notes]] with [[koreader]].


## Why?

I don't mean notes related to a specific book.  I mean general fleeting notes.  Things that pop into my head that I want to action at some point.  Part of the reason being, I'm avoiding having my mobile phone in my bedroom.  But at night I occassionaly have random ideas pop into my head, and I'd like to jot them down in a way that doesn't involve turning the light on.


## How?

Options:

-   Just use a pen and paper.  Disadvantage: I don't want to turn the light on and disturb my partner.
-   Use Boox.  Advantage: I already have orgzly and syncthing set up and running nicely on there.  Disadvantage: bit bulky, have to wait a short time for it to turn on, I don't always have it in my bedroom.
-   Use Kobo. Advantage: I usually am reading a book at night anyway, it's nice and lightweight.  Disadvantage: syncing always a bit of a pain.


### Taking the notes

koreader has a simple text editor that I can use.  It's not amazing, but does the job well enough.  Go to the 'spanner and screwdriver' menu item in the the top menu, go to `Text Editor`,  create a `notes.txt` file, open it up.  Now it lives in a list of recent files for the text editor, so it's only three taps to get to it wherever I am - nice and quick.  Could possible set up some gesture or corner tap zone if I wanted it even easier, but this seems fine.


### Syncing the notes

To be honest, the question is more about file sync.  I'm sure I looked at getting syncthing on there before, but it wasn't really possible as I recall.

How best to then sync the notes to somewhere that I can then process them later?  Ideally, I'd like that sync to be automatic, not a manual thing.

There's lots of stuff around progress sync, but that's not what I want.

Oh nice, actually it looks like installing syncthing is possible - https://anarc.at/hardware/tablet/kobo-clara-hd/#install-syncthing.  Maybe I'll try that.  It looks slightly fiddly though, so perhaps I'll save it for a rainy day and just copy the file manually periodically for now.

To be honest, I could also just hand copy the notes from the file.  There will next be that much in there.

