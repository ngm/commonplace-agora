# Antonio Gramsci

[[Cultural hegemony]].
War of position vs war of movement.
Long march through the institutions.
Basically the importance of culture in revolution.


## Articles

-   [Meet the Godfather of Cultural Marxism - Foundation for Economic Education](https://fee.org/articles/antonio-gramsci-the-godfather-of-cultural-marxism/) (Note; This article seems to be by some right wing libertarian)
-   [Antonio Gramsci’s Long Struggle - Public Discourse](https://www.thepublicdiscourse.com/2022/06/82558/) (More good background on Gramsci, again from another seemingly right wing place.)
-   [A Critical Evaluation of Gramsci](https://struggle-sessions.com/2019/07/16/a-critical-evaluation-of-gramsci/) (An attack on gramsci from the left. here he is presented as a revisionist, polluter of MLM ideas.)

