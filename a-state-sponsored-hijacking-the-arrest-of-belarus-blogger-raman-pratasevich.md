# A state-sponsored ‘hijacking’ – the arrest of Belarus blogger Raman Pratasevich

URL
: https://www.theguardian.com/news/audio/2021/may/28/a-state-sponsored-hijacking-the-arrest-of-belarus-blogger-raman-pratasevich

Publisher
: [[The Guardian]]

[[Belarus]]. [[Raman Pratasevich]]. [[Alexander Lukashenko]].

