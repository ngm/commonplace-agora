# Worker coops and consulting

> joelleichty - I’ve been in the #workercooperative space for literally ones of months. On the Dunning Krueger scale I’ve reached the pinnacle of confidence.
> 
> That’s why there is no doubt in my mind that more consulting firms should consider organizing as #coops.
> 
> Consulting is a business where the link between workers’ labor and revenue is obvious. It also requires very little capital for startup and has low overhead, making it simpler to get rolling.
> 🧵 1.
> 
> &#x2013; https://social.coop/@joelleichty/109564026599168905

