# Big tech is complicit in climate breakdown



## Evidence

-   [How Google, Microsoft, and Big Tech Are Automating the Climate Crisis](https://gizmodo.com/how-google-microsoft-and-big-tech-are-automating-the-1832790799)
-   [[Mining for minerals used in electronics is often ecologically destructive]]
-   [[Digital companies support other forms of unsustainable extraction]]
-   [[AI is fuelling a data centre boom]]

