# ValueFlows

Like [[ActivityPub]] but for economic activities rather than social activities.

Primarily for [[value flows]] other than your standard market-based ones. Aimed more at [[solidarity economy]], [[cooperatives]], etc.

> VF in particular embraces this experimentation towards next-economy / solidarity-economy / commons-based-economy / P2P-economy/etc. in this transitional time.
> 
> &#x2013; [Principles - Valueflows](https://www.valueflo.ws/introduction/principles/)

I like the idea and the people working on it, but I don't **get** it yet - because I've not used it or seen an example in action.  I also like the fact there's a big part of it revolves around [[agents]].

I'd kind of like [[Flancia Collective]] to become a [[DisCO]] somehow - and then we might use it there.


## What

> [[Money]] is an abstraction to represent value. It's not a good abstraction, but it persists because the global economy is in a local minimum because the disadvantages of sharing information outweigh the advantages. An economy with sufficient incentives for economic agents to communicate their values directly without the abstraction layer would be more efficient, assuming the existence of an agreeable language for that communication  **[&#x2026;] valueflows [&#x2026;] is a grammar for that language**
> 
> &#x2013; https://banana.dog/@yaaps/102928925219253732


## Why

> The important thing to know is that the benefits of collectives aren't just for adversarial situations where we interface with capitalism. We have a basis for building economic networks that are competitive with capitalist business units comparable in size while being just to labor, community, and the environment  The issue is bringing trust-based networks up to scale with multinational corporations. This can happen, but folks are still working on the software support
> 
> &#x2013; https://banana.dog/@yaaps/102928925219253732


## How


### key concepts

REA (Resources, Events, Agents) - see [DisCO and Valueflows](https://mothership.disco.coop/DisCO_and_Valueflows#/).


#### Agents

perform Economic Events affecting Economic Resources.

Agents can be individual persons or organizations of various types. They can be related in user-defined ways, like “member” or “trading partner”.


#### Economic events

An Economic Event can take actions like create, change, consume, use, or destroy Economic Resources, or transfer them from one Agent to another, or transport them from one place to another.


#### Economic Resources

Anything of value, however you define that - examples are

-   Useful goods and services
-   Money, tokens, credits
-   Labor power, skills
-   Documented knowledge
-   Air, water, soil
-   Etc.


## Resources

-   [DisCO and Valueflows - DisCO MOTHERSHIP Wiki](https://mothership.disco.coop/DisCO_and_Valueflows)
-   https://write.as/economic-networks/about
-   [A ValueFlows story about pie](http://mikorizal.org/ValueFlows-Story.pdf)

