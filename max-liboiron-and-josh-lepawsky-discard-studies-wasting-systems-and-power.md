# Max Liboiron and Josh Lepawsky, "Discard Studies: Wasting, Systems, and Power"

A
: [[podcast]]

URL
: https://newbooksnetwork.com/discard-studies

Featuring
: [[Josh Lepawsky]]

I found this a very interesting interview - on ‘[[Discard studies]]’:

> Rather than focusing on waste and trash as the primary objects of study, discard studies looks at wider systems of waste and wasting to explore how some materials, practices, regions, and people are valued or devalued, becoming dominant or disposable.

They talk about how the vast majority of waste is beyond the prevailing idea of ‘municipal solid waste’, with most waste occurring before things ever reach individual citizens. They say discard studies is about understanding whole systems - and also who has the power, who gets to decide what is discarded.

~00:13:01  [[Behaviour change or system change?]]  Even if we all recycled 100% it still wouldn't be enough, as majority of waste is before it even gets to the consumer. 

~00:37:51  Dealing with symptoms vs dealing with systems.

Some good thoughts on [[Recycling]].

