# TMK BC5: Mute Compulsion, Ch. 1

A
: [[podcast]]

Discussing chapter 1 of [[Mute Compulsion]].

[[Power]]. [[Capital]].

> We discuss chapter one – Conceptualising Power and Capital – which lays the groundwork for analyzing these two fundamental concepts and their relation. Mau goes through mainstream social and political theories of power, showing how they are all deficient in various ways and what features a theory of power must possess. Then he lays out what it means to understand capital as a social logic and how this logic can exercise power in ways that are meaningful and material.

Capital is an emergent property of social relations.

Recognising economic power means we can better articulate what needs you be done. Otherwise if we only see coercion we think we need only challenge the state, if we only see ideology we think we need only change our minds. But if we see economic power we challenge it and it's role in [[social reproduction]].

