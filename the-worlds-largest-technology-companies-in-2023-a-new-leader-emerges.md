# The World's Largest Technology Companies In 2023: A New Leader Emerges

An
: [[article]]

URL
: https://www.forbes.com/sites/jonathanponciano/2023/06/08/the-worlds-largest-technology-companies-in-2023-a-new-leader-emerges/?sh=52e780c55d1d

The problem of [[domination]], counter to [[democracy]] and [[agency]], at all layers of the stack.

