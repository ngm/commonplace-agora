# Net zero

> It is not just environmentalists who champion net zero. Last week, Alistair Phillips-Davies, the chief executive of one of the big six energy companies, SSE, said: “Net zero is not only an environmental decision, it’s a rational economic one. Investing now will not only reduce our future exposure to gas markets but it will also support jobs and growth.”
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 


## Criticism from the left

-   [[Net zero is part of a global imperialist climate policy]]

