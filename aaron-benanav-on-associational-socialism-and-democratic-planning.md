# Aaron Benanav on Associational Socialism and Democratic Planning

A
: [[podcast]]

URL
: https://www.futurehistories.today/episoden-blog/s02/e10-aaron-benanav-on-associational-socialism-and-democratic-planning/

Series
: [[Future Histories]]

Featuring
: [[Aaron Benanav]]

> A world without scarcity is possible for everybody! Aaron Benanav shares his idea of how we could achieve this associational utopia, today.

[[post-scarcity]].

References [[How to Make a Pencil]].

~00:08:59  Problems with e.g. [[market socialism]] and the inability to deal with [[capital strike]].

~00:14:28  Critique of [[universal basic income]].  Mentions the positives of it - benefits should be universal, noone should live in poverty, etc.  I think the gist of the critique being that UBI deals with distribution only, it doesn't touch the engine of growth, the organisation of production.  It is a transformation of welfare that won't change things dramatically.

~00:27:34  Static vs [[dynamic planning]].

~00:32:47  On incorporating non-financial / economic concerns in to planning.  When you do, it becomes a political question that requires debate and consensus, not one that is just treated as having some technical optimum.

~00:56:30  [[Democratic planning]] is an alternative to the polar opposites of markets and central planning. Also - it's only on the big things, on the world changing decisions that we have to negotiate.  This is similar to Half-Earth Socialism I think.

~00:59:50  [[Associational socialism]].

~01:19:50  A bit of a mention of [[universal basic services]].

~01:32:58  Abolition of money as a general equivalent. A different set of quantitative indicators.

~01:34:49  [[Mechanism design]].

