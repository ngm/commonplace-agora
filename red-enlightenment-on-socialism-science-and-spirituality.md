# Red Enlightenment: On Socialism, Science and Spirituality

A
: [[book]]

Found at
: https://repeaterbooks.com/product/red-enlightenment/

Written by
: [[Graham Jones]]

[[Red Enlightenment]]: On [[Socialism]], [[Science]] and [[Spirituality]].

> Why we need a materialist spirituality for the secular left, and how to build one.
> 
> The left commonly rejects [[religion]] and spirituality as counter-revolutionary forces, citing Marx’s famous dictum that “[[religion is the opium of the people]].” Yet forms of spirituality have motivated struggles throughout history, ranging from medieval peasant uprisings and colonial slave revolts, to South American [[liberation theology]] and the US [[civil rights movement]]. And in a world where religion is growing, and political movements are ridden with conflict, burnout, and failure, what can the left learn from religion?
> 
> Red Enlightenment argues not only for a deepened understanding of religious matters, but calls for the secular left to develop its own spiritual perspectives. It proposes a materialist spirituality built from socialist and scientific sources, finding points of contact with the global history of philosophy and religion.
> 
> From [[cybernetics]] to liberation theology, from ancient Indian and Chinese philosophy to Marxist [[dialectical materialism]], from traditional religious practices to contemporary art, music, and film, Red Enlightenment sets out a plausible secular spirituality, a new socialist praxis, and a utopian vision.

