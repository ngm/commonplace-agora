# Community-oriented life at human scale

URL
: https://jornbettin.com/2021/03/16/community-oriented-life-at-human-scale/

Author
: [[Jorn Bettin]]

> Like bees and ants, humans are eusocial animals. Through the lenses of evolutionary biology and cultural evolution, local communities – and especially small groups of 20 to 100 people – are the primary organisms within human society, in contrast to individuals, corporations, and nation states. The implications for our civilisation are profound.

-   [[Humans are eusocial animals]]
-   [[Local communities are the primary organisms within human society]]
-   The implications for our civilisation are profound

