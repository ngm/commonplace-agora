# Metaverse

-   [The Metaverse Has Always Been a Dystopian Idea](https://www.vice.com/en/article/v7eqbb/the-metaverse-has-always-been-a-dystopia)
-   [The Metaverse Is Bad](https://www.theatlantic.com/technology/archive/2021/10/facebook-metaverse-name-change/620449/)

> Thus, to get people into the metaverse — a digital environment where we visit virtual worlds, interact with other people’s avatars, and (most importantly) spend money on digital goods — “it must be made more appealing than reality.” That’s done by making the real world worse, or convincing people it’s far better to be someone else.
> 
> &#x2013; [Jeff Bezos is THE UNION-BUSTER](https://mailchi.mp/techwontsave.us/jeff-bezos-is-the-union-buster)

<!--quoteend-->

> Technically, land is more productive, because you can grow stuff on it. And hunt game etc.
> 
> But yeah, the people moving into NFTs in "metaverses" today are basically hoping that they'll become the new landlord class of a neofeudal future.
> 
> &#x2013; https://twitter.com/interstar/status/1454229759240585216

