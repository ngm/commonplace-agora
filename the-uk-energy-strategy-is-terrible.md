# The UK energy strategy is terrible

The [[UK energy strategy]] (as of April 2022) is terrible.


## Because

-   It does bugger all to tackle soaring [[energy bills]] ([[Britain has soaring energy bills]]).
-   It ignores quick, cheap and clean solutions like [[onshore wind]] and home [[insulation]].
-   It pushes for construction of [[nuclear power]] stations.
-   It even includes ramping up on oil and gas and fracking.
    
    > The cabinet eventually agreed that atomic energy would form the backbone of the strategy, and up to eight new reactors are planned.
    > 
    > &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy) 
    
    <!--quoteend-->
    
    > The British Geological Survey will conduct an “impartial” review of whether fracking for shale gas can proceed safely, a move likely to spark fury among environmental campaigners about the controversial technology, particularly after a moratorium was imposed on the process in 2019.
    > 
    > &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy) 
    
    <!--quoteend-->
    
    > New North Sea oil and gas projects are also likely to be accelerated, although the strategy sets out proposals to limit emissions as much as possible.
    > 
    > &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy) 
    
    <!--quoteend-->
    
    > Miliband said this would do little to unblock planning restrictions that he said place a “unique burden” on onshore wind, which he claimed could have replaced Russian gas imports within 24 months.
    > 
    > &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy)

> Four out of five members of the public support the use of onshore wind farms, which could be built quickly and cheaply if planning rules were eased. This was rumoured to be on the cards. But Mr Johnson has instead bowed to the nimby instincts of Tory MPs and ministers, whose views are at odds with the mood of the country, but who have the power to make life difficult in parliament. Limited consultations with some “supportive” communities will have next to no impact and a game-changing possibility has been lost.
> 
> [The Guardian view on Boris Johnson’s energy strategy: missed opportunities | &#x2026;](https://www.theguardian.com/commentisfree/2022/apr/07/the-guardian-view-on-boris-johnsons-energy-strategy-missed-opportunities)


## Resources

-   [‘Major misjudgment’: how the Tories got their energy strategy so wrong | Ener&#x2026;](https://www.theguardian.com/environment/2022/apr/06/major-misjudgement-how-the-tories-got-their-energy-strategy-so-wrong)
-   [Sign the petition: we need energy security now!](https://act.38degrees.org.uk/act/energy-security-now-petition)

