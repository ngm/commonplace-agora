# Peer Production License

A type of [[copyfarleft]] license devised by [[Dmytri Kleiner]].  [[Peer production]].

More for content that software as per [[Creative Commons]].

I definitely don't fully understand the nuances, but AFAICT, the purpose is to adapt the Non-Commercial (NC) part of Creative Commons to freely allow commons-based commercial use, but not corporate commercial use unless you remunerate the original provisioner.  ShareAlike (SA) allows anyone to profit as long as they share.

I use it on my wiki.

-   [Peer Production Licence _ Human-readable summary](https://civicwise.org/peer-production-licence-_-human-readable-summary/)


## Critiques

Plenty here: http://wiki.p2pfoundation.net/Copyfarleft

> I'm not very impressed by many of the licenses produced in recent years. Often they're seeking to produce a closed ingroup of beneficiaries. It's this sort of clubbishness which was one of the main hallmarks of proprietary licenses. If you're in the corpus or the chosen few in the magic pentangle then maybe you get access/use rights, but otherwise you're just plain out of luck.
> 
> &#x2013; https://epicyon.freedombone.net/@bob/106402944442892676 

