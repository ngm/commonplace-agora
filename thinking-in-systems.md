# Thinking in Systems

A
: [[book]]

Author
: [[Donella Meadows]]

> reading Meadows is a good start. But Meadows represents only one lens, the systems dynamics lens of “resource stocks and their flows.” Meadows only briefly touches on regulation and feedback; she does not fully address systems as “information flows;” and she ignores second-order systems and related topics, such as learning and conversation.
> 
> &#x2013; [[A Systems Literacy Manifesto]]

