# Moving towards an ecological Leninism

Author
: [[Gus Woody]]

URL
: https://www.rs21.org.uk/2020/12/18/revolutionary-reflections-moving-towards-an-ecological-leninism/

> The urgency of the climate crisis has led some on the left to turn what’s been called ‘Ecological Leninism’. Gus Woody assesses recent writings of some key left thinkers on the environment and asks whether this turn to Lenin offers a way forward for organising to stop the climate catastrophe.

[[Climate crisis]]. [[Eco-Leninism]].

I think I saw this article first referenced from [[Climate Leninism and Revolutionary Transition]].

Good article I thought.  Primarily on [[revolutionary transition]].  Gives a bit of an overview of some of the recent positions on eco-Leninism (Heron and Dean, Malm, Wall).  Also namechecks social ecology, Bookchin and Rojava, which is good.  And outlines some of the key aspects of Leninism that might have relevance to revolutionary transiation.

> this article aims to reflect on several concerns which may become the bedrock of an ecological Leninism: the state, the party and movements, imperialism, and the philosophical underpinnings of Leninist materialism.


## Three shades of Ecological Leninism

Gives an overview of the positions of [[Kai Heron]] and [[Jodi Dean]], [[Andreas Malm]], and [[Derek Wall]].

> Each of these authors reads Lenin in different ways. Dean and Heron emphasise the party and state, Malm for speed and struggle under conditions of hardship, Wall for constant strategic organising. Each has shortcomings, but all are contributing to a useful development of—or rupture in—ecological theory and practice.


### Dean and Heron

Party building and seizure of the state.

> For Dean and Heron then, ecological Leninism centres on party building and the seizure of the state.

<!--quoteend-->

> the contradictions present within the variety of contemporary Green New Deal (GND) proposals. In general terms, these aim through state-led investment to repurpose national economies towards decarbonisation and redistributive policies.  Surveying plans from either side of the Atlantic, Dean and Heron point out that many GNDs still refuse to nationalise the industries necessary for large-scale decarbonisation.

<!--quoteend-->

> Instead they argue we need to organise for revolutionary socialism whilst recognising the necessity of seizing control of the state for decarbonisation implicit within many left-wing GNDs.

<!--quoteend-->

> By thinking through these issues around the GND, Dean and Heron affirm the need for environmentalists to abandon ‘state phobia’ and seriously engage with the possibility of a ‘state-led, centrally planned, and global response’ to the climate crisis—the ‘old’ Leninist revolutionary seizure of state power.

Woody doesn't buy all of it.

> There is much to disagree with in their interpretation.

<!--quoteend-->

> Herein we encounter a conundrum of ecological Leninism – how do we square the need for rapid changes to tackle climate change with the simultaneous need to smash the state and replace it with organs of workers’ control?


### Andreas Malm

War communism is Malm's main take from Leninism.

> This leads us to Malm, whose ecological Leninism is focused predominantly on the period of War Communism and the struggles of the early Bolshevik regime to establish itself. Rather than Roosevelt’s New Deal period in the 1930s, to which GND organisers look for historical analogy, Malm argues decarbonisation would look more akin to the War Communism of the 1920s.

<!--quoteend-->

> In simple terms, just as the Bolsheviks sought to turn the imperialist World War One into a crisis for capitalism more generally, today ecological Leninism requires turning the chronic emergencies of climate disaster and zoonotic diseases into a general crisis for the capitalist system. In the face of escalating symptoms, there is a need to rapidly build for a revolutionary preservation of life.

<!--quoteend-->

> consists of leaping ‘at any opportunity to wrest the state in this direction’ and turn society away from catastrophe towards direct public control.

<!--quoteend-->

> Crucially, his vision of this is not in line with the more optimistic vision of progressive GND proposals, or ‘luxury Communisms’. Instead, the idea of War Communism, of the Bolshevik state attempting to lead a transformation of Russian society whilst facing war, famine, and fuel shortage, reflects the sort of dire circumstances that any ecological Leninist regime would face.

<!--quoteend-->

> The earth the wretched would – will – inherit, will be in need of an assiduous programme of restoration. While we may yearn for luxury, what will be necessary first is Salvage Communism.

<!--quoteend-->

> War Communism here shifts to a violent pessimism, where all we can do is mobilise a variety of strategies to cut these tethers. Malm seems deeply sceptical about the possibility of the formation of alternative institutions of dual power.

<!--quoteend-->

> Malm attempts to have his ‘Lenin cake’ and eat it: he argues ecological Leninism should raise the consciousness of spontaneous movements and route them towards the drivers of breakdown, whilst he avoids really discussing how to approach Leninist organisation and the formation of working class power to rival the state.

<!--quoteend-->

> Malm falls into the trap of creating a dichotomy between waiting for revolution and acting within existing social movements to pressure the bourgeoise state.

<!--quoteend-->

> This points to an absence at the heart of Malm’s ecological Leninism. It is a Leninism without a revolution of 1917, focused instead on the difficulties of the Bolshevik government during the Civil War and on Lenin’s wagers during the First World War.

<!--quoteend-->

> It has little to say about the act of building movements capable of intervening in revolutionary situations, tipping them towards revolutionary outcomes.

<!--quoteend-->

> the same time, as Tugal has eloquently pointed out, Malm’s ecological Leninism is without a revolutionary subject

<!--quoteend-->

> Rarely in his book does Malm speak of the role of working class struggle within any ecological Leninist project, and as a result he seems pessimistic about the formation of working class power which could rival the state, laying the bedrock for a Leninist project.


### Derek Wall

> Building the base – Wall

<!--quoteend-->

> his recent Climate Strike and in other writings, Leninist strategic thinking is central

<!--quoteend-->

> Wall’s is a practical account of Lenin; it looks to the ways in which Leninist thinking suggest revolutionaries should organise in the here and now.

<!--quoteend-->

> Contra Dean and Heron’s need for a party, contra Malm’s need for speed, Wall argues for the slow construction of spaces of working class power. There are no shortcuts to revolution, and Wall argues this to its limit.

<!--quoteend-->

> Whilst Wall has a slower conception of ecological Leninism, focused on the hard slog of organising, it doesn’t entirely repudiate the need for urgent action, which requires more specificity around the practicalities of building for dual power over the coming years. A strategy geared to base-building risks becoming a new form of economism, as Lenin might have put it. There is a balance that must be struck, but Wall’s intervention is crucial in pointing to the dearth of institutions here in the UK at least which could be considered bases of working class power.


## Areas of eco-Leninism

Looks at a few different parts of Leninist theory particular from an eco-socialist lens.

-   the state
-   the relationship between movement, workers, and party
-   the centrality of imperialism
-   wider concerns for the philosophy of nature and science


### The State

> the rise of global supply chains and new international bodies for capitalist states to interact has created a world where states are increasingly entangled and co-dependent, creating challenges for any revolutionary theory which aims for global impact. [13] There is therefore an urgent need to analyse the

<!--quoteend-->

> Marxist theorising around the state has not stayed still since Lenin’s day. There has been Althusser, [14] Gramsci, [15] and the Miliband-Poulantzas debate about the class character of the state, [16] as well as the emergence of Open Marxist approaches. [17] As a result, in the words of Khachaturian, ‘Marxist state theory is largely an open-ended and intellectually pluralistic research framework.’

<!--quoteend-->

> the point pushed by both GND proponents and the ecological Leninist authors already discussed – the state apparatus has significant potential to be used as an instrument for rapid decarbonisation.

<!--quoteend-->

> In the period of rapid decarbonisation which ecological Leninism aims to deliver, there is the twofold problem of creating worker control over society as well as the difficulty of ensuring the forces of [[fossil capital]] cannot exert influence.

<!--quoteend-->

> This is a valuable endeavour if used to move environmentalism away from the liberal conception of ‘climate assemblies’ towards climate councils and soviets of workers.

<!--quoteend-->

> On the second issue of suppression, socialists must recognise that if a revolutionary movement seized state power, without continued international mobilisations, any decarbonisation effort will face new enemies of global capitalism – the IMF, the World Bank, etc. Studying how these institutions have been mobilised against socialist and social democratic states is crucial, as in addition to imperialist armies and governments, these bodies will attack any ecological Leninist regime.

<!--quoteend-->

> International eco-socialist solidarity is crucial,

<!--quoteend-->

> In short, how to prevent violent autarky whilst regimes build for global eco-socialist revolution is a pressing issue.


### The Party, the movement, and the agent

> This points to a further urgent issue when moving towards an ecological Leninism, one that the preceding authors vary on– the organisation of revolutionary socialists and its relationships with mass movements.

<!--quoteend-->

> Dean and Heron are open in their desire for an ecological Leninist party, Malm reads ecological Leninism as a collection of principles not implying an actually existing party, Wall is focused on the need for grassroots organising rather than any formal party.

<!--quoteend-->

> Whilst both are certain to be crucial contributions, just as Lenin railed against the economist and terrorist strategies of simply organising around workplace conflicts or conducting isolated violent actions, there is still the tricky problem of creating an organisational form with the strategic acumen to transcend the limits of both.

<!--quoteend-->

> Just as Lenin in What is to be Done? points to the need for Marxists to organise to ensure that economic struggle is not seen as subservient for the need to organise and agitate for revolutionary socialism, today simply hoping for rising radicalism from ecological struggle is insufficient.

<!--quoteend-->

> But if we reject the reading of Lenin as party builder, as many contemporary eco-socialists do, where does this leave us?


#### Bookchin and social ecology

Bookchin not a fan of the state and the party.

> It would be remiss of any ecological socialist to ignore the pioneering work of early anti-capitalist environmentalists like [[Bookchin]], whose [[social ecology]] was deeply critical of Leninist, Trotskyist and other socialist party formations as they emerged in the US.

<!--quoteend-->

> Just as the Paris Commune was instructive to Marx and Lenin, modern ecological Leninists should consider the organisation of the revolution and state in [[Rojava]], inspired by Bookchin and Ocalan.

<!--quoteend-->

> This is just one of many ‘ecological regimes’ which are attempting to grow today, and they should be used to inspire and develop contemporary Leninism.

<!--quoteend-->

> Transforming this opposition to particular pieces of fossil fuel infrastructure into a total and international opposition to fossil capitalism is crucial.

<!--quoteend-->

> Perhaps the best summation of this project has been made by Mohandesi:
> I suggest we think of the “party” as an organization among others, one defined by its articulating function, as that which unites disparate social forces, links struggles over time, and facilitates the collective project of building socialism beyond the state.


### [[Imperialism]]

> Perhaps the largest parallel between Lenin’s thinking and the needs of modern ecological Marxism left under-analysed by existing accounts is the theory of imperialism.

<!--quoteend-->

> Again, just as the form of the capitalist state has changed since 1917, so too has the emergence and form of imperialism. Imperialism as national acts of military expansion to secure resources has been joined by an array of techniques by which global capitalism secures its resource frontiers. In particular, the mechanisms by which international bodies like the World Bank and IMF reinforce and intensify core-periphery extraction have been studied extensively by Marxist geographers and ecologists. These insights need to be brought into the analyses of environmentalists and socialists alike.

<!--quoteend-->

> account of how global fossil capital and climate breakdown depends on resource imperialism

<!--quoteend-->

> we cannot account for the climate crisis without accounting for the colonial project of frontier expansion and extraction

<!--quoteend-->

> European armies into the Amazon to prevent its burning, or in the increasingly complex geopolitics of Lithium mining for batteries and other renewable tech

<!--quoteend-->

> Taking inspiration from Lenin and ensuring an account of modern imperialism is central to explaining environmental breakdown prevents socialists, particularly those in the Global North, falling for an imperialist environmentalism.

<!--quoteend-->

> Such is the case with many accounts of the Green New Deal, which may talk of the exciting potential of batteries and electric transitions, but talk little of global empire’s continued extraction of rare earth minerals through violence and exploitation in the Global South.

<!--quoteend-->

> Certainly, within the varied literature on degrowth there is some utopian hot air, but there is also attention to its links to decolonisation and anti-imperialism.

<!--quoteend-->

> Global eco-socialist solidarity will require massive programs of repair and restoration to the damage caused by core nations to the periphery.

<!--quoteend-->

> There is a space open to develop an ecological Leninist degrowth, which looks like a fundamental reorganisation of the production and extraction which fuels destruction in the core, towards the reversal of core-periphery metabolic flows.

<!--quoteend-->

> Such a politics opens up a space to link up revolutionaries in the core with those in the periphery, united in their commitment to ending the politics of national growth and its often-inevitable extractivism.

<!--quoteend-->

> Martin Arboleda’s work on the political economy of mining, Planetary Mine


### Science, Nature and the philosophical foundations of ecological Leninism

> Histories of the social construction of nature and science, and how these categories are subsumed in processes of domination such as patriarchy, racism, and colonialism have exploded on the scene.

<!--quoteend-->

> [[Donna Haraway]]’s theories of cyborgs and situated perspectives, [39] with its own approach to theory and the partiality of viewpoints is a critical challenge to those constructing totalising theory today.

<!--quoteend-->

> At the same time, the critical realism of Bhaskhar also emerged as an approach to the gordian knot of knowledge and science.

<!--quoteend-->

> Quantum mechanics, geo-engineering, biotechnology – all these disrupt interpretations of science and nature, which poses a problem to an ecological scientific socialism.

<!--quoteend-->

> In questions of ecology, a binary has emerged between techno-optimist and techno-critical schools with regards to mitigation and adaptation. Let us paint crude pictures. One side focuses on a vision of eco-modernism, where technologies bring the potential for luxury and a restructuring of production towards liberatory and greener horizons. The other, adopts a wider rejection of technologies’ potentials, arguing instead for the necessity of a crude degrowth or in some case primitivism.

<!--quoteend-->

> Technology is neither inherently liberatory or oppressive, instead it is the social relations of its production and implementation which determine its social effect. In short, contemporary ecological Leninism should, whilst adopting some form of critical realism in line with the broad thrust of Empirio-Monism, focus on how capitalism develops and mobilises forms of science and nature to the detriment of humanity.

<!--quoteend-->

> Ultimately the response must be designing a historical and dialectical materialism which incorporates the insights of thinkers who have pointed to the role of a racialised and gendered capitalism in constructing certain sciences, technologies, and natures.


## Conclusion

> Metabolic theory may point to nodes, points, and flows in the metabolism where the proletariat can intervene, but it tells us little about how to get there. To put it crudely, there remains the absent question of ‘What is to be Done?’

^ I'd like to know more about that first bit, too.  (the nodes, points and flows where the proletariat can intervene - sounds like it must reference [[Leverage Points: Places to Intervene in a System]])

> The introduction to Bellamy Foster and Burkett’s Marx and the Earth highlights what may be termed the ‘three stages’ of ecosocialist thought. [43] Put simply, the first consisted of the rejection of aspects of Marxism, to be supplemented by Green theory. The second saw the return of Marx: the excavation of his ecological thought in the metabolic theory championed by [[John Bellamy Foster]] and others. Finally, the third wave points to those who apply metabolic theory and its insights to specific situations and developments, the unleashing of the metabolism.

The fourth wave of ecological Marxism.

> would suggest an ecological Leninism is the shift here. The fourth wave of ecological Marxism

<!--quoteend-->

> How does ecological Leninism approach the gordian knots of ‘nature’ and ‘science’ in ways that promote the transition to an ecological society?

<!--quoteend-->

> There is a chance that any ecological Leninism, if it does answer the above questions and build on the insights of the 100 years since War Communism, would be unrecognisable to the Bolsheviks. Perhaps this is for the better.

