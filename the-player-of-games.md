# The Player of Games

A
: [[book]]

Author
: [[Iain M. Banks]]

Just finished The Player of Games (Iain M Banks) – also really enjoyable.  Less cinematic than [[Consider Phlebas]] (though still nicely paced), and a bit more to chew on philosophically.  The contrast of the [[hierarchical]] Azadian empire and the egalitarian Culture is interesting.  Gurgeh, the main character, from the Culture, finds some appeal in the way things work in Azad.  Azad feels like a caricature of Western society as it is today – superficially advanced and urbane, but with some real darkness hidden away, out of sight.

