# Reclaim the stacks

We live under capitalism, powered by [[digital capitalism]]. [[Digital ecosocialism]] is an alternative.  Reclaiming the stacks is focused on the transition from the one to the other.

![[reclaim-the-stacks.png]]

[[Seize the means of computation]]. Reclaim the stacks.


## Monthly roundups

-   [[Reclaim the Stacks: reflections, May 2024]]
-   [interlude, [[Life happens]]]
-   [[Reclaim the Stacks: October 2023 roundup]]
-   [[Reclaiming the stacks: September 2023 roundup]]
-   [[Reclaiming the stacks: August 2023 roundup]]
-   [[Reclaim Roundup: July 2023]]
-   [[Newsletter: June 2023]]


## Longer posts

-   [[Ways to reclaim the stacks]]
-   [[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]]
-   [[Amazon doesn't care how you heat swimming pools: ICT and ecosocialism]]
-   [[Reclaiming the stacks: the role of ICT workers in a transition to ecosocialism]]

