# Wikis as hypertexts

Kicks calls wikis a type of [[hypertexting]]. I don't feel I am actively building a hypertext, like I'm not intentionally focusing on that kind of non-linear hypertextual navigation.  But it is definitely full of hyperlinks and they're a big part of the utility of it. I guess a personal wiki **is-a** hypertext, with its own special focus.  

