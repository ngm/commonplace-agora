# gene-culture coevolution

> And here we have a new feedback loop to consider: in a process known as gene-culture coevolution, [[culture]] has shaped the human niche so profoundly that it's caused changes within the human genome, affecting the very direction of [[human evolution]]
> 
> &#x2013; [[The Patterning Instinct]]

