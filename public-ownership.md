# public ownership

An alternative of [[privatisation]].

Often equated with state ownership and [[nationalisation]].

That's one option - also municipal and community-based ownership is a type of public ownership.  The commons.

