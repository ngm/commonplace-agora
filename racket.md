# Racket

[14: Digital Humanities Workshops &#x2013; FOSS and Crafts](https://fossandcrafts.org/episodes/14-digital-humanities-workshops.html) &lt;- discusses Racket as a teaching tool for digital humanites.

-   [Racket Programming the Fun Way | No Starch Press](https://nostarch.com/racket-programming-fun-way)

