# English water industry

> More than 70% of the English water industry is owned by foreign investment vehicles, the super-rich, businesses lodged in tax havens and pension funds, Guardian research has found
> 
> &#x2013; [Revealed: more than 70% of English water industry is in foreign ownership | W&#x2026;](https://www.theguardian.com/environment/2022/nov/30/more-than-70-per-cent-english-water-industry-foreign-ownership) 

