# The Ministry for the Future

A
: [[book]]

Author
: [[Kim Stanley Robinson]]

Paints an incredibly grim picture of a world experiencing [[climate breakdown]] and the political ramifications.  The opening chapter of a deadly heatwave in India is absolutely brutal.  This is what climate crisis means.

Lots of didactic bits along the way.  Some reviews say these are a bit tedious, but I've found them fairly positive so far - you learn from them.  It does have a pretty stilted narrative flow as a result though.

It is hard science-fiction.  So much that you might call it [[scientific utopianism]].  Though I don't know how much Robinson actively wants things to play out this way.  (e.g. is he down with the acts of [[eco-terrorism]] or just thinks it will likely happen regardless).


## Timeline

![[ministry-future-timeline.png]]

