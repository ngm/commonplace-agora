# care work

> Jobs that are often done by women, so-called ‘pink-collar’ jobs in health and education, not only represent some of the strongest segments of the labour movement today but also foreshadow the shift to a zero-carbon economy that prioritizes care work over extractive labour.
> 
> [[Half-Earth Socialism]]

