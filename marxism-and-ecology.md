# Marxism and ecology

[[Marxism]] and [[ecology]].

> A variety of academics and green political writers argued bluntly that Marxism had little to contribute to ecological struggles. Marx and Engels were defined as [[Prometheans]] concerned to use nature as an instrument to promote human progress.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> would argue that we have seen a sharp break from such perspectives, since the publication of US sociologist [[John Bellamy Foster]]’s book [[Marx's Ecology]]. Foster argues, convincingly to my mind, that ecology is core to Marx and Engels’ project (Foster, 2000). Indeed an examination of Marx and Engels’ texts suggests an overwhelming concern with environmental issues. In turn their philosophy based on relationships derived from Hegel and perhaps Spinoza, is akin to ecology defined as a science of relationships
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> In turn, Marx’s notion of a [[metabolic rift]] between humanity and the rest of nature, has been used by Foster to conceptualise ecological crisis. Healing the rift is the answer to problems such as climate change, to the extent that humans master nature, we are mastering an element of ourselves rather than something alien.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

