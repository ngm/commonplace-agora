# intellectual property

A ridiculous concept: The enclosure of ideas built upon the commons of all prior thought.

-   Intellectual provenance, not intellectual property

> We have a hard time thinking what the writer and the scientist and artist and the engineer have in common. Well, the vectoral class does not have that problem. What all of us make is intellectual property, which from its point of view is as equivalent and as tradable as pink goo.
> 
> &#x2013; [[Capital is Dead]]

-   [[We should phase out intellectual property]]
-   [[Intellectual property markets must be socialized]]

