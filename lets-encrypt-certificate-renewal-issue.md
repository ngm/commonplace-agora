# Let's Encrypt certificate renewal issue

I got an email that my certificates were going to expire.

Given they are set to auto-renew, this seemed odd.

I logged in to the server and some errors indeed in ~/var/log/letsencrypt/letsencrypt.log

Along the lines of

> Detail: 37.218.246.201: Fetching http://commonplace.doubleloop.net/.well-known/acme-challenge/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx: Timeout during connect 
> (likely firewall problem)

And indeed it was a firewall problem.  I did not have port 80 open, just 443 and some others.

```nil
ufw allow http
certbot -q renew
```

sorted it.

Weird that this just manifested though.  What changed?

