# information overload

Whenever I get that stab of feeling like there's too many ideas to explore, too many books to read, too little time to do it in, I remind myself not to turn to productivity systems, efficiency improvements and the like, but to [[knowledge sharing]] and [[communities of practice]], and remind myself that you don't need to know it all, knowing things is not just an individual pursuit but a collective endeavour.

> If we look from a hierarchical perspective there is a need for having all available information at your disposal. It is what keeps you on top of it all. The usage of the term information overload implies a hierarchical situation. Taking the emergence perspective, information overload dissolves into nothingness: it is not about the individual information items, it’s about the overall shapes and patterns they in combination convey, which you should be alert to. 
> 
> &#x2013; [[The Emergence of Blogging]]

