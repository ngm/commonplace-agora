# The Uber files: the unicorn

URL
: https://www.theguardian.com/news/audio/2022/jul/11/the-uber-files-the-unicorn-part-1

[[Uber]].

This makes you realise how really any focus on Uber being a tech company is kind of missing the story.

There's nothing particularly amazing about the tech.  They got as big as they are through obscene amounts of capital investment and underhand tactics that fuelled growth in new markets.  Force your way in, muscle out the competition, until the point that you're entrenched and it's incredibly difficult to get you out again.

The rise of Uber is all about the valorisation of growth as a measure of value above all else.

The fact that it's a shitty bro-culture tech firm is an unpleasant side detail, but not the reason that it made it so big.

