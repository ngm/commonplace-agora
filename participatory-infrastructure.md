# participatory infrastructure

> It entails **building an infrastructure that continuously supports citizens in participating**.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

I guess it could be partly [[citizen participation platform]]s, but needs to be much more than just that.

> Cities can look to open source and its decades of experience in creating this type of infrastructure to find valuable lessons.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

^ what are the lessons?

> Even for well-established open source projects, encouraging effective contributions remains a time-consuming activity that involves mentorship. This is true even in projects with well-developed infrastructures and professional participation standards that include procedures to follow with pull requests encoded in a program, coding standards, expectations for commenting code appropriately, informative variable names, automated tools, etc. There is very little such infrastructure for participatory platforms, and what exists supports the technical side (e.g., software development and deployment).
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

