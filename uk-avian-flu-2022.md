# UK avian flu 2022

> The Food Standards Agency says it poses a very low safety risk for people, but as a precaution farmers temporarily have to keep all free-range hens in barns. This is to protect their welfare and stop the spread by keeping farmed hens away from wild birds.

