# Capitalism and migration

> Increasing percentages of the refugees, if they survive their typically harrowing treks or dangerous sea voyages, come up against vast numbers of agents “trained, armed, and paid to stop them.” This drive to “stop them” is promoted by a ruling class which at the same time relentlessly stokes the economic engines of capital that gave rise to the climate crisis in the first place.
> 
> &#x2013; [[Red-Green Revolution]]

