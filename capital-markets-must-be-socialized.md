# Capital markets must be socialized

> [[Co-ops]] also help [[de-marketing capital]], relocating ownership from stock market gamblers who are physically and sentimentally remote from the firm’s operations to workers who inherently give a damn about the fate of the firm and the labor they deploy for it. In addition to this, it is crucial to expand our public wealth funds in number and scope. The Alaska Permanent Fund, CalPERS, and even the Social Security trust funds are pools of capital whose income streams (dividends and interest) flow to the public. A national sovereign wealth fund and/or many regional ones would shrink capital markets. (It might make sense to emulate Alaska, using the fund to outlay the aforementioned basic income.) For best results, this should be held together by a [[public banking system]], most easily run through the postal service, featuring a publicly-controlled mobile payments system.
> 
> &#x2013; [[Markets in the Next System]]

