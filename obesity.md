# Obesity

> Almost two-thirds (63%) of adults in England are overweight or obese.
> 
> &#x2013; [Coronavirus: Public Health England calls for action on obesity in Covid-19 fi&#x2026;](https://www.theguardian.com/society/2020/jul/25/public-health-england-calls-for-action-on-obesity-in-covid-19-fight) 

<!--quoteend-->

> Captains of the food industry would have you believe that this a result of poor personal choice alone. This is a libertarian falsehood. Obesity is a population health problem connected to social, economic and environmental factors. In higher-income countries, it is associated with lower socio-economic status, poverty and inequality. It does not exist in a vacuum.
> 
> &#x2013; [To Tackle Obesity, Fight Inequality](https://tribunemag.co.uk/2020/07/to-tackle-obesity-fight-inequality)

