# Half Nelson

> In the movie Half Nelson (directed by Ryan Fleck), Ryan Gosling stars as a teacher in a tough inner-city school who teaches [[dialectics]], and engages with students in a way I find very believable.
> 
> &#x2013; [[When nature and society are seen through the lens of dialectics and systems thinking]]

<!--quoteend-->

> And in fact, in Half Nelson, it’s the teacher who eventually finds himself mentored by one of his pupils - confirming the words of Brazilian educationist (and dialectician) [[Paulo Freire]], that “education must begin with the solution of the teacher-student contradiction, by reconciling the poles of the contradiction so that both are simultaneously teachers and students.”
> 
> &#x2013; [[When nature and society are seen through the lens of dialectics and systems thinking]]

