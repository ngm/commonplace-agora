# Kill the Bill

civil liberties campaign

> A response to far-right culture war hostility to both racial justice and climate protection activism, this partnership of [[Black Lives Matter]], [[Extinction Rebellion]], and a number of grassroots and civil society activist coalitions may be the most prominent social movement of recent times. t
> 
> &#x2013; [[Tribune Winter 2022]]

