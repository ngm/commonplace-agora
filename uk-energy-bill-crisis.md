# UK energy bill crisis

Part of the [[Cost of living crisis]].


## Timeline


### <span class="timestamp-wrapper"><span class="timestamp">[2022-09-11 Sun]</span></span>

[[Energy price freeze]].

> Britain will be plunged into an even worse energy crisis in a year’s time without an immediate plan to improve leaky homes and dramatically reduce demand for gas, ministers have been warned.
> 
> &#x2013; [UK must insulate homes or face a worse energy crisis in 2023, say experts | E&#x2026;](https://www.theguardian.com/money/2022/sep/11/britain-insulate-homes-energy-crisis-2023-heat-loss-houses-subsidising-bills)

