# Mutualism

> building alternative economies through [[cooperatives]], [[credit union]]s and local purchasing
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

<!--quoteend-->

> Mutualism is an anarchist school of thought and economic theory that advocates a socialist society based on free markets and usufructs, i.e. occupation and use property norms
> 
> &#x2013; [Mutualism (economic theory) - Wikipedia](https://en.wikipedia.org/wiki/Mutualism_(economic_theory)) 


## References

-   https://zinelibrary.c4ss.org/media/What%20is%20Mutualism.pdf

