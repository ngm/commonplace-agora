# asset manager

> Three asset managers – [[Vanguard]], [[BlackRock]] and [[State Street Global Advisors]] – now control more than US$15 trillion, making them the largest shareholders of 88 per cent of firms in the S&amp;P 500.
> 
> &#x2013; [[Platform socialism]]

