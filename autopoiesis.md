# autopoiesis

[[Humberto Maturana]] and [[Francisco Varela]].

> They describe an autopoietic system as one which continuously reproduces its own internal structure, as well as its external boundary with its environment
> 
> &#x2013; [[Red Enlightenment: On Socialism, Science and Spirituality]]

