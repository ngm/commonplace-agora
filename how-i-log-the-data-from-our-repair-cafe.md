# How I log the data from our Repair Cafe

Some context:

-   we're a small Repair Cafe
-   we run for two hours once a month (with an occasional popup)

We have paper forms that are filled in during the event.

I receive those at some point.

I scan all of those in using the Genius Scan app on Android. This is really quick to do.

I export from Genius Scan to a PDF in Google Drive. File named 'URC data YYYY-MM-DD'.

I have a Google Sheet where I log the data first. I do this because there's some pieces of data that we record that there aren't fields for in Restarters.net.

I load up the PDF and the spreadsheet in split screen mode on my phone.

