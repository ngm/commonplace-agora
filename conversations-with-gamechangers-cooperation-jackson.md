# Conversations with Gamechangers: Cooperation Jackson

Speaker
: [[Kali Akuno]] of [[Cooperation Jackson]]

Part of [[Conversations with Gamechangers]] series.  It was an intro to coops, and CJ in particular, followed by questions.

```plantuml
@startmindmap
+ CJ
++ community organising
+++ unions
+++ coops
-- Jackson water crisis
---_ broken treatment facility
---_ no availability of drinking water
++ Jackson Kush-Plan
+++ dual power
++++ people's assemblies
+++ democratising the economy
++++_ coops
+++ running for elected office
@endmindmap
```

-   currently a water crisis in Jackson
-   currently in the middle of distribution event
-   types of community organising
    -   organising by trades (union style)
    -   cooperatives
    -   originally coops serviced social and reproductive needs
        -   child care coops, etc
    -   after: a compromise during 1930s compromise
    -   capital struck a deal with some labour leaders
        -   national labour board etc
        -   set it up so that labour could only work for wages, hours, and healthcare
            -   but removed ability for political strikes and political action
                -   one union can not work in concert with other set of workers
                    -   same as UK
        -   social security and social welfare as well
        -   removed the need for social organising in a sense - divorced the coops from the trade unions
            -   limiting what both could do in terms of power and struggle, became special interest groups within bourgeious order
        -   coop jackson are intentional in trying to break that
    -   history exists in a context of colonization, white power and patriarchy
    -   black workers did not work in industries that were allowed to be unionzed
        -   e.g. farming or maids
        -   because of race
        -   black workers were excluded from trade unions
    -   lost the ability to own the means of production
    -   CJ developing model that gets out of these relations of production
    -   Jackson, rouhgly 220,000 in Jackson
        -   it has shrunk to about 160-180 thousand in recent years
        -   80% of city is black
    -   brain drain: black doctors from Mississipi move elsewhere in the US
    -   85% population is potential leverage of political power
        -   black political power
    -   Jackson-Kush Plan
        -   automous independent power, dual power
            -   self-governance through people's assemblies
        -   democratising the economy
            -   CJ was explicitly for this part of it
        -   **political power without economic power is symbol without substance**
        -   additionally running for elected office
            -   two mayors have been elected
    -   a young project with a lot of experimentation
        -   mistake have been made, CJ aim to learn from them
    -   CJ about 8 years old
    -   accomplishments:
        -   [[community land trust]]
            -   44 properties
            -   collectively-owned
                -   saved up money to own outright
                    -   (CJ don't trust banks, racist institutions)
        -   network of federated [[cooperatives]]
            -   Green Team (landscaping cooperative)
            -   Freedom Farms (hit hard by water crisis)
            -   [[Community Production Coop]] - manufacturing coop
                -   digital fabrication coop, 3d printing, electrical design and craft, print shop
            -   in the pipeline
                -   Zero Waste Jackson (recycling and composting coop)
                -   People's Groceries
                -   Cannabis coop - medicinal cannabis coop
                    -   hemp and bamboo as sustainable materials for the CPC
    -   withstand pressures of capitalist market
        -   largest employer in Mississipi is Wal-Mart
            -   a vicious corporation - they want no form of competition
    -   some coops have failed, but there's space to do that
        -   owning gives this time and space for learning
    -   one coop failed (previous recycling and composting coop)
        -   too tightly coupled to city system
        -   city shut everything down, then the coop failed
    -   walmart investor in recreational cannabis in mississipi
-   to democratise economy
    -   create supply and value chains
-   most coops in US
    -   constructed on friends and favours orientation
        -   10 people who like the same thing, turn it in to an income
    -   in CJ, given poverty etc
        -   what will given the most independence from market structure and political leverage?
-   e.g. food was weaponised during hurricane katrina, so ownership of food supply is strategic
-   a lot of trial and error in CJ
    -   that's something to get used to in solidarity economy


## questions


### how are the people's assemblies organised and used?


### what does the solidarity economy mean to CJ?

-   what are CJ striving against?
    -   entrepreneuriship has been promoted in black community as answer to all problems
    -   i.e. individual ownership
        -   but this is a continuation of unequal relationships
-   prefer individuality over individualism
-   **solidarity is the antidote to individualism**
    -   being in alignment and in practice with people who are not your blood or kin or clan, but your community
    -   productive relationships where we consciously and deliberately depend upon each other


### how to get started in Solidarity economy

-   start with what you have
-   avoid thinking in terms of deficits
-   e.g. if you don't have a job, maybe you have a lot of time
-   who can I work with and how can I work with them


### water crisis in Jackson

-   there's no affordable drinking water currently in Jackson
-   water pressure has been restored (since about a week ago)
    -   sanitary system was not functioning
-   need to avoid airborne diseases
    -   it's a hot and humid region
    -   malaria and denge fever and sleeping sickness are coming back
-   to get bottled water you would have had to go 20 miles out of the city (last week)
    -   now if you get to a grocery store early in the morning you can buy water
    -   most people don't have enough money to go and buy water
-   CJ doing water distribution
-   all the faith and community organisations are doing water distribution
    -   but still not enough people are getting water
-   if you don't have a car, you can't get enough water
-   CJ going to members in the community and doing water drop offs
-   working age people are off working elsewhere sending money back
    -   reverse bell curve for the population (lots of young and elderly)
-   no indication as to when the water will be restored
-   problems
    -   pipes are old, antiquated and lead, they break a lot
    -   treatment facility is broken - it's not cleaning the water
        -   billions of dollars to build a new treatment facility
            -   state government is refusing to give money for this
-   racial component to how it is playing out
    -   state government redirecting money from federal government to white neighbourhoods outside of jackson


### how did jackson get going?

-   it was a long time in gestation
-   going back generations
-   then in 2001&#x2026;
    -   group decided to get together and pool resources
    -   $50 a month in to collective pool
    -   to buy land in mississipi
    -   a lot of patience, a lot of discipline
    -   first purchase in 2011
-   additionally some athletes and entertainers from the area
    -   they've given back
-   property values are low in mississipi and in jackson
    -   might not have been able to do the same thing in atlanta for example


### how to avoid cooptation?

-   foundations come and offer grants with a lot of conditions
-   have to be patient and disciplined and don't be tempted by these
-   a long-term intergenerational vision and being able to stick to it


### accountability processes?

-   open communication and transparent communication


### threats to CJ from the state etc

-   the most concerning of late
    -   mississipi has a supermajority that controls state legislature now
    -   passing preventative laws
    -   e.g. climate justice
        -   after helping with committment to reducing emissions by about 10% in a couple of years&#x2026;
        -   state legislature
            -   said mississipi must not adhere to any EU standards around climate
-   so CJ have had to censor themselves to some degree
-   FBI sniffing around, trying to intimidate
    -   they've been infiltrated, disruption


### how to get those on low income involved?

-   don't just assume that people can participate
    -   that is classist and gendered
-   low dues
-   but if can't afford it, then give us some time
-   building up systems that allow people to participate
    -   e.g. single mothers - provide childcare

