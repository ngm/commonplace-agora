# platform capitalism

> By creating the digital infrastructure that facilitates online communities, platform companies have inserted value capture mechanisms between people seeking to interact and exchange online
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Platform Capitalism, referring to the market power that these technology companies have by controlling key platforms we use for our basic internet services.
> 
> &#x2013; [[Digital Capitalism online course]]

