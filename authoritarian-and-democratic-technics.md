# Authoritarian and Democratic Technics

An
: [[article]]

URL
: https://theanarchistlibrary.org/library/lewis-mumford-authoritarian-and-democratic-technics

Author
: [[Lewis Mumford]]

> While 'democratic' technics, available to small communities, may be neutral, 'authoritarian' technics, available only to large-scale, hierarchical, authoritarian, societies, are not. Such technics are not only unsustainable, but 'are driving planetary murder'.
> 
> &#x2013; [Lewis Mumford - Wikipedia](https://en.wikipedia.org/wiki/Lewis_Mumford) 

