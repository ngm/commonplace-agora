# Revisionism

Attempts to massively revise Marxist theory.  I'm not sure what the boundary is, because obviously it's open to critique and re-evaluation when context changes.  You shouldn't be doctrinaire. Who says what is revisionist?

I guess it's when the revision is seen to be particular egregious, e.g. Bernstein, to the extent that the theory is neutered and aligning itself with the bourgoise.

But anyone can easily throw the term around against someone they disagree with.

> Revisionism essentially means accepting the norms of imperialist society and working within them, which would inevitably shade off into living off crumbs from the imperialist banquet.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

