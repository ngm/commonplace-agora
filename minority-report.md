# Minority Report

A
: [[film]]

Very enjoyable and well done. Good visuals and action scenes.  Directed by Steven Spielberg, surprisingly dark.

Themes?  [[Policing]], more specifically [[predictive policing]].  Though not exactly in the sense of [[algorithmic decision-making]], as the Precogs are not machines.

[[Free will]] and [[determinism]].

A bit of power and corruption I guess.

