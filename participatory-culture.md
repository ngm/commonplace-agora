# participatory culture

> Understanding participatory culture as just a numbers game can be part of the problem. Participation is not only about quantity but, perhaps more importantly, about how it materializes—who speaks and in what way—and technology can hide understanding of these processes.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

<!--quoteend-->

> Creating a participatory culture requires thinking beyond technology and participation numbers. It entails building an infrastructure that continuously supports citizens in participating. Cities can look to open source and its decades of experience in creating this type of infrastructure to find valuable lessons.
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

<!--quoteend-->

> Open sourcing a city requires helping citizens participate in the best possible conditions and paying attention to their own contexts and requirements. This is important since the modern understanding of "citizens" is articulated around the idea that they renounce their own power in exchange for security. This is called the [[social contract]], and it is a foundational concept in constructing the contemporary idea of "nation." It is a weak form of democracy, relying on regular yet infrequent participation (and sometimes even discouraging active participation).
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

