# stocks and flows

> Stocks and flows are the basic elements of any system: things that can get built up or run down – just like water in a bath, fish in the sea, people on the planet, trust in a community, or money in the bank. A stock’s levels change over time due to the balance between its inflows and outflows. A bathtub fills or empties depending on how fast water pours in from the tap versus how fast it drains out of the plughole. A flock of chickens grows or shrinks depending on the rate of chicks born versus chickens dying. A piggy bank fills up if more coins are added than are taken awa
> 
> &#x2013; [[Doughnut Economics]]

