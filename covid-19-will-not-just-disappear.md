# Covid-19 will not just disappear

[[Coronavirus]].


## Because

-   Only one human infectious disease has been eradicated from the planet – smallpox – and that took nearly 200 years
-   Polio is near to extinction, but it has taken a 70-year campaign
-   Covid-19 may be even more troublesome
-   Unlike those viruses, Covid-19 has been able to easily adapt to find its way around human immunity (whether from infection or vaccination) so that it can survive.
-   Omicron, or its progeny, will probably be with us for decades to come.


## Related

-   Vaccine protection is not universal


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Source (for me)
: [In future, living with Covid could mean boosters only for our most vulnerable&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/25/vaccines-best-weapon-covid-boosters)

