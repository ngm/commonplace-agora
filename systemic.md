# systemic

> Within language (i.e. in the foreground) it refers to the understanding of a phenomenon within the context of a larger whole. To understand things systemically literally means to put them into a context, to establish the nature of their relationships
> 
> &#x2013; [[Systems Practice: How to Act]]

