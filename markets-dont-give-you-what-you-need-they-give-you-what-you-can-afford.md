# Markets don't give you what you need, they give you what you can afford

A
: [[claim]]

[[Markets]] don’t give you what you need—they give you what you can afford.

> Markets don’t give you what you need—they give you what you can afford.
> 
> &#x2013; [[Internet for the People]]

