# Prague Spring

> The first and more proximate cause was the Prague Spring in 1968, when a reformist ‘socialism with a human face’ spooked party elites of other Warsaw Pact states, who then invaded Czechoslovakia. 
> 
> [[Half-Earth Socialism]]

