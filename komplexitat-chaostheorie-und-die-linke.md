# Komplexität «Chaostheorie» und die Linke

URL
: https://www.anarchia-versand.net/Buecher-und-Broschueren/Gesellschaftskritik/Politik/Komplexitaet-Chaostheorie-und-die-Linke::1697.html
    
    A translation of the blurb by DeepL:
    
    > The goal of every left movement is to change existing conditions for the better. However, in order to better understand the possibilities, but also the limits of change, whether in evolutionary or revolutionary form, it is indispensable to understand the results and conclusions of complexity research.
    > 
    > In this context, there are few terms that are used so misleadingly today as "complexity" and "chaos theory". Which anarchist actually knows that chaos theory does not study chaos (in the sense of randomness or stochasticity) at all, but the highly diverse outcomes of a purely deterministic system whose initial conditions are changed? Which socialist is concerned about the fact that in think tanks in the USA (and other countries) results of network theory have long been used to trigger revolutionary movements or to suppress them? On the one hand, many results of these schools of thought hold opportunities (and dangers) or contain similarly far-reaching conclusions for ideologies. On the other hand, conclusions can also be drawn through ignorance that are in no way covered by real results.
    > 
    > That systems are not static but dynamic, i.e. in motion, is known to every Marxist from their description of human history. Anarchists have long thought about assumed and observed self-organisation phenomena. Leftists are basically systems theorists and dynamists from the beginning.
    > 
    > In this book of the "theorie.org" series, some central ideas such as those of "chaos theory", dynamic systems or networks are therefore first developed. As far as possible, examples from social theory are used. Then the current state of the discussion in various scientific disciplines will be presented. A separate chapter will present part of the left-wing discussion to date, the results of which will be critically discussed.

