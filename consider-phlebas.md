# Consider Phlebas

A
: [[book]]

Author
: [[Iain M. Banks]]

The title comes from [[The Waste Land]] by [[T.S. Eliot]].

-&#x2014;

My book at the moment is Consider Phlebas by Iain M. Banks. I’m fairly certain I’ve read it before, many years ago (my Mum had most of his books when I was growing up) – long enough ago to remember only brief flashes of it.

Enjoying it greatly so far. It’s a lot lighter than I remember Banks’ books being (maybe I’m just thinking of The Wasp Factory…). I came back to it after a recommendation from a friend, of science fiction with some thought experiments around a socialist society. The Culture, a race within the novel, are I believe what one podcast I listened to recently called ‘space communists’, warts and all, so it should be interesting to see how they’re explored.

-&#x2014;
About halfway through, and I’m enjoying Consider Phlebas so far.  It’s my book I’m reading at night at the moment, and it’s ticking the boxes for that (nicely written and not too taxing).  It’s not a really strong plot, but kind of a series of gripping set pieces and cinematic imagery.  Slightly overly masculine in viewpoint I think.

-&#x2014;

Really enjoyed Consider Phlebas (Iain M. Banks).  It was a rip-roaring read.  Very visual and cinematic, I can still conjure up a picture of a lot of the scenes in my head.  Enjoyed the brief intro it gives to the Culture, too – the [[post-scarcity]] socialist society that are featured in a lot more books in the Culture series.  Interested to see how that’s explored further.  I’ve just started reading the next in the series, The Player of Games.

After this I think I’ll try some Kim Stanley Robinson – more speculative socialist futures as far as I understand.

-&#x2014;

Finished January 2019.

