# Cartografias da Internet

Found at
: https://www.cartografiasdainternet.org/en

> Great interactive map that explores the materiality and power behind the internet: the cables, satellites, antennas, servers, computers, cell phones, extractivism, programmed obsolescence, electronic waste, running coding, content moderation.
> 
> &#x2013; https://digitalcapitalismcourse.tni.org/course/view.php?id=2&section=1

