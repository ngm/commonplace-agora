# Improving my Emacs writing mode

Work in progress.


## Installing the ETBembo font

-   Need to also install the EtBembo font from here: https://github.com/edwardtufte/et-book
-   The way I did it on Linux Mint:
    -   download a zip file of the repo
    -   extract
    -   open File Browser, and find and double click on each of the fonts .tff files
        -   et-book-bold-line-figures.ttf
        -   et-book-display-italic-old-style-figures.ttf
        -   et-book-roman-line-figures.ttf
        -   et-book-roman-old-style-figures.ttf
        -   et-book-semi-bold-old-style-figures.ttf
    -   Each one should open up Font Viewer with the font loaded
        -   click on 'Install' to install the font


## Amendments

I've just been trialling this - I should probably add it to my ngm/writing-mode function.

> what font do quotes get?

```elisp
(setq org-startup-indented t
      fringe-indicator-alist '((truncation nil nil)
			       (continuation nil nil)
			       (overlay-arrow . right-triangle)
			       (up . up-arrow)
			       (down . down-arrow)
			       (top top-left-angle top-right-angle)
			       (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle)
			       (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
			       (empty-line . empty-line)
			       (unknown . question-mark))
      org-superstar-remove-leading-stars t
      org-superstar-headline-bullets-list '(" ")
      org-ellipsis "  " ;; folding symbol
      org-pretty-entities t
      org-hide-emphasis-markers t
      ;; show actually italicized text instead of /italicized text/
      org-agenda-block-separator ""
      org-fontify-whole-heading-line t
      org-fontify-done-headline t
      org-fontify-quote-and-verse-blocks t)
```


### And this in user-init

```elisp
(setq-default theming-modifications
      '((spacemacs-light
	 (variable-pitch
	  :family "EtBembo"
	  :background nil
	  :foreground "#1c1e1f"
	  :height 1.7)
	 (org-document-title
	  :inherit nil
	  :family "EtBembo"
	  :height 1.8
	  :foreground "#1c1e1f"
	  :underline nil)
	 (org-document-info
	  :height 1.2
	  :slant italic)
	 (org-level-1
	  :inherit nil
	  :family "EtBembo"
	  :height 1.6
	  :weight normal
	  :slant normal
	  :foreground "#1c1e1f")
	 (org-level-2
	  :inherit nil
	  :family "EtBembo"
	  :weight normal
	  :height 1.3
	  :slant italic
	  :foreground "#1c1e1f")
	 (org-level-3
	  :inherit nil
	  :family et-font
	  :weight normal
	  :slant italic
	  :height 1.2
	  :foreground "#1c1e1f")
	 (org-level-4
	  :inherit nil
	  :family "EtBembo"
	  :weight normal
	  :slant italic
	  :height 1.1
	  :foreground "#1c1e1f")
	 (org-headline-done
	  :family "EtBembo"
	  :strike-through t)
	 (org-quote
	  :inherit variable-pitch)
	 (org-block
	  :inherit fixed-pitch
	  :height 0.7
	  :background "#eee"
	  :foreground "#1c1e1f")
	 (org-block-begin-line
	   :background nil
	   :height 0.8
	   :family "Source Code Pro"
	   :foreground "#8FA1B3")
	 (org-block-end-line
	   :background nil
	   :height 0.8
	   :family "Source Code Pro"
	   :foreground "#8FA1B3")
	 (org-document-info-keyword
	   :height 0.8
	   :foreground "#bbb")
	 (org-link
	  :foreground "#1c1e1f")
	 (org-special-keyword
	  :family "Source Code Pro"
	  :height 0.8)
	 (org-hide
	  :foreground "#fbf8ef")
	 (org-indent
	   :inherit (org-hide fixed-pitch))
	 (org-date
	   :family "Source Code Pro"
	   :height 0.8)
	 (org-ellipsis
	   :underline nil
	   :foreground "#525254")
	 (org-tag
	  :foreground "#727280")
	 (org-table
	   :family "Source Code Pro"
	   :height 0.9
	   :background "#fbf8ef")
	 (org-code
	   :inherit nil
	   :family "Source Code Pro"
	   :foreground "#525254"
	   :height 0.9))))))
```


## TODO

-   Follow the font settings from https://github.com/lepisma/rogue/blob/75ab1c3422b409f41daa4c003b931e869eed0914/config.el#L205
-   Don't use the ETBembo font in src blocks, tables, etc


## Resources

-   Inspiration for general look: [Ricing up Org Mode](https://lepisma.xyz/2017/10/28/ricing-org-mode/)
-   Fringe indicator stuff: [Changes to fringe-indicator-alist not taking effect - Emacs Stack Exchange](https://emacs.stackexchange.com/questions/70028/changes-to-fringe-indicator-alist-not-taking-effect)
-   Worth checking: [Emacs proof-of-concept: tweaked focused writing for Org | Protesilaos Stavrou](https://protesilaos.com/codelog/2020-07-18-emacs-concept-org-tweaked-focus/)

