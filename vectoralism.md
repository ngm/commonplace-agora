# Vectoralism

See [[Vectoralist class]], [[Capital is Dead]].

![[images/vectoralism.jpg "Photo by @thanospal on unsplash (https://unsplash.com/photos/bakq5bepwbQ)"]]

> The older class antagonisms have not gone away. It’s just that there’s a new layer on top, trying to control them.
> 
> &#x2013; [[Capital is Dead]]

Seems related to [[Communicative capitalism]].

