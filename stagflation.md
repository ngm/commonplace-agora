# stagflation

> The most noticeable effect on the rich world was the novel phenomenon of ‘stagflation’. According to the Keynesian consensus of the post-war period, a stable relationship was supposed to exist between inflation and employment. When the economy was operating at full capacity, prices would increase because all available resources (including labour) were being used in production, pushing up prices (and wages). The inverse was thought to be true when production and growth slowed.
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> But during the 1970s, unemployment and inflation were both high at the same time. Looking back, the reason for this — the formation of OPEC — is quite obvious. But the chaos and confusion of the time provided neoliberal economists with an opportunity to push a new theory of inflation: [[monetarism]]
> 
> &#x2013; [[Tribune Winter 2022]]

