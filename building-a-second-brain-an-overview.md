# Building a Second Brain: An Overview

-   source: [Building a Second Brain: An Overview](https://fortelabs.co/blog/basboverview/)
-   tags:[[Building a second brain]]

Quick intro into BASB.  A lot of hyperbole about how it'll change your life, interspersed with some decent (common sense) tips.

> Building A Second Brain is a methodology for saving and systematically reminding us of the ideas, inspirations, insights, and connections we’ve gained through our experience.

<!--quoteend-->

> By offloading our thinking onto a “second brain,” we free our biological brain to imagine, create, and simply be present. We can move through life confident that we will remember everything that matters, instead of floundering through our 
> days struggling to keep track of every detail.

<!--quoteend-->

> By consolidating ideas from these sources, you’ll develop a valuable body of work to advance your projects and goals. You’ll have an ongoing record of personal discoveries, lessons learned, and actionable insights for any situation.

Advancing my projects and goals&#x2026; actionable insights&#x2026; I guess that's what I want. But not jiving with this language so much.

> Your second brain will serve as an extension of your mind, not only protecting you from the ravages of forgetfulness but also amplifying your efforts as you take on creative challenges.

The&#x2026; ravages of forgetfulness?

> -   Consistently move your projects and goals to completion by organizing and accessing your knowledge in a results-oriented way
> -   Transform your personal knowledge into income, taking advantage of a rapidly growing knowledge economy
> -   Uncover unexpected patterns and connections between ideas
> -   Reduce stress and “information overload” by expertly curating and managing your personal information stream
> -   Develop valuable expertise, specialized knowledge, and the skills to deploy it in a new job, career, or business
> -   Cultivate a collection of valuable knowledge and insights over time without having to follow rigid, time-consuming rules
> -   Unlock the full value of the wealth of learning resources all around you, such as online courses, webinars, books, articles, forums, and podcasts

Aw shit man.  This all feels a bit Hacker News/Y Combinator-y.  I want a tool for thought and learning, not something to help me get My First Acquisition.

Anyway.  Could still be useful.

-   What are the recurring themes and questions that I always seem to return to in my work and life?
-   What insightful, high-value, impactful information do I already have access to that could be valuable?
-   Which knowledge do I want to interconnect, mix and match, and periodically resurface to stimulate future thinking on these subjects?

The main gist seems to be: don't capture information haphazardly.  Fair enough.  Not a revolutionary idea, but if you've got some good tips I'm all ears&#x2026;

> But unless we make conscious, strategic decisions about what we consume, we’ll always be at the mercy of what others want us to see
> 
> (tag:[[Information strategies]]) 

Organising by projects.  Keep only what resonates.  This is fluff really.

> You can greatly facilitate and speed up this process by distilling your notes into actionable, bite-sized summaries. It would be near impossible to review your 10 pages of notes on a book you read last year in the midst of a chaotic workday., for example. But if you had just the main points of that book in a 3-point summary, you could quickly remind yourself of what it contains and potentially apply it to something you’re working on

^ this sounds useful

-   design notes for your future self
-   [[summarise progressively]]

"add value to a note every time you touch it"
I like that.  kind of like the boy scout rule.

"A common challenge for people who love to learn is that they constantly force feed themselves more and more information, but never actually put it to use."
very true

"But information only becomes knowledge – something personal, embodied, grounded – when we put it to use. That’s why we should shift as much of our effort as possible from consuming information, to creating new things. "
yeap, thought this for a while

-   don't just consume information passively - put it to use
-   create smaller, reusable units of work
-   share your work with the world

This article is a lot of sales pitch, not so much substance.  But some useful reiterations of good techniques for organising information.

