# Envisioning Platform Socialism w/ James Muldoon

A
: [[podcast]]

Series
: [[Tech Won't Save Us]]

Featuring
: [[James Muldoon]]

Good chat with James Muldoon on platform socialism.

> Paris Marx is joined by James Muldoon to discuss his vision for [[platform socialism]] and the different ways we could reorganize platforms to serve the public good over corporate profit.

