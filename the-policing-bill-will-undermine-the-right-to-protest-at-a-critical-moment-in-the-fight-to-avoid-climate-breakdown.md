# The policing bill will undermine the right to protest at a critical moment in the fight to avoid climate breakdown

The [[Policing bill]] will undermine the [[right to protest]] at a critical moment in the fight to avoid [[climate breakdown]].

(I guess really here the actual claim is that the policing bill will undermine the right to protest.  The followup about it being a bad time to do that is an addendum? Maybe I should split that out?)


## Because

-   [[The policing bill increases the risk of peaceful demonstrators being criminalised]]


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Signs point to yes]]

Haven't looked at the policing bill a huge amount, but it does sound like what is contained within is designed to have a chilling effect on protest.

