# COP 27

27th [[COP]], held in Sharm el-Sheikh in [[2022]].


## 1.5C

> When the history of the climate crisis is written, in whatever world awaits us, Cop27 will be seen as the moment when the dream of keeping global heating below [[1.5C]] died.
> 
> &#x2013; [The 1.5C climate goal died at Cop27 – but hope must not | Cop27 | The Guardian](https://www.theguardian.com/environment/2022/nov/20/cop27-summit-climate-crisis-global-heating-fossil-fuel-industry)

<!--quoteend-->

> Can the UN climate talks deliver this at speed? It does not look that way. It is too easy for the fossil fuel states to hold the consensus-based negotiations to ransom, threatening to blow up the whole thing if their black gold is so much as mentioned by name. There were more fossil fuel lobbyists at Cop27 than delegates from the Pacific islands, which their industry is pushing below the waves.
> 
> &#x2013; [The 1.5C climate goal died at Cop27 – but hope must not | Cop27 | The Guardian](https://www.theguardian.com/environment/2022/nov/20/cop27-summit-climate-crisis-global-heating-fossil-fuel-industry)


## Loss and damage

> Cop27 did achieve something. The new [[loss and damage]] fund promises to finance the rebuilding of poorer, vulnerable countries hit by increasingly severe climate impacts that they have done little to cause. It is a long overdue acknowledgment of the moral responsibility the big polluters have for the climate emergency. It is all the more important given that Cop27’s failure to meaningfully drive emissions cuts means even worse disasters are to come.
> 
> &#x2013; [The 1.5C climate goal died at Cop27 – but hope must not | Cop27 | The Guardian](https://www.theguardian.com/environment/2022/nov/20/cop27-summit-climate-crisis-global-heating-fossil-fuel-industry)

<!--quoteend-->

> The agreement to establish a loss and damage fund is a historic breakthrough, demanded for three decades by developing countries.
> 
> &#x2013; [The Guardian view on Cop27’s outcome: a real achievement, but too far to go |&#x2026;](https://www.theguardian.com/commentisfree/2022/nov/20/the-guardian-view-on-cop27s-outcome-a-real-achievement-but-too-far-to-go)

<!--quoteend-->

> The loss and damage fund is necessary, but amounts to mitigation, instead of prevention; equivalent to a whipround to buy a neighbour new clothes after watching as their house burnt down – because you dropped a lit match.
> 
> &#x2013; [The Guardian view on Cop27’s outcome: a real achievement, but too far to go |&#x2026;](https://www.theguardian.com/commentisfree/2022/nov/20/the-guardian-view-on-cop27s-outcome-a-real-achievement-but-too-far-to-go)

