# Everyday Utopia and Radical Imagination with Kristen Ghodsee

URL
: https://www.upstreampodcast.org/conversations

Series
: [[Upstream]]

Featuring
: [[Kristen Ghodsee]]

> It’s perhaps more important than ever in these especially tumultuous, lonely, and oppressive times that we continue to believe that another world is possible. Simply reimagining the way we raise our children, the homes that we dwell in, the property we horde or share, and the form of the families we choose — can have profound and long-term impacts on the quality of our lives and on the world we’re living in more broadly. By challenging these seemingly ordinary structures of everyday life we can spark and re-spark our collective and individual desire to live in a more just and equitable world.

<!--quoteend-->

> This is the premise of new book [[Everyday Utopia]]: What 2,000 Years of Wild Experiments Can Teach Us About the Good Life, written by Kristen Ghodsee. In this conversation, we take a journey around the world and through time, exploring some of the most fascinating, inspiring, and sometimes quirky, experiments in alternative ways of living. From Plato to the Buddha, from the Bible to the Communist Manifesto, from ancient Athens to the Soviet Union, we’ll explore what utopian thinking and practice has achieved, not just materially, but also in igniting our capacity for hope, radical imagination, and militant optimism.

