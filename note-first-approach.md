# Note-first approach

> I propose we make the design of individual notes the primary factor, instead of tags or notebooks. This has many advantages:
> 
>     It works well with any other organizational system, without depending on them (including but not limited to tags and notebooks, if you want to use those)
>     It makes all work you do on your notes value-added, because you’re spending close to 100% of the time engaging directly with the content itself
>     It can more easily survive migrations to other devices, storage locations, and even programs, because note content is much more likely to be preserved than overarching structure
>     It cultivates skills (succinct communication, finding the core of an idea, visual thinking, etc.) that are inherently valuable and highly transferrable to other activities
>     It makes your notes more legible and useful to others (unlike your internal notebook structure, which is only for 
> your use), promoting collaboration and sharing
> 
> With a note-first approach, your notes become like individual atoms — each with its own unique properties, but ready to be assembled into elements, molecules, and compounds that are far more powerful.
> &#x2013;  [Progressive Summarization: A Practical Technique for Designing Discoverable N&#x2026;](https://fortelabs.co/blog/progressive-summarization-a-practical-technique-for-designing-discoverable-notes) 

