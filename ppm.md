# ppm

> The ratio of carbon dioxide molecule concentrations in the atmosphere is counted in parts per million – or ppm. Before the industrial revolution, CO2 levels were around 280 ppm. By February 2022 they stood at 419 ppm, the highest level since the Pliocene era four million years ago, when temperatures were between 3-4 degrees hotter, and sea levels were 5-40 metres higher
> 
> &#x2013; Down to Earth newsletter

