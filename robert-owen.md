# Robert Owen

> Owen organized utopian settlements in New Lanark in Scotland and New Harmony in Indiana, where he fostered innovative childhood education programmes and workers’ co-operatives
> 
> &#x2013; [[Half-Earth Socialism]]

