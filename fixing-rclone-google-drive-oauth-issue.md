# Fixing rclone Google Drive oauth issue

[[rclone]]

https://forum.rclone.org/t/google-oauth-migration-for-rclone/30545/

I though this was supposed to be happening in October, but I'm already getting errors about it.

-   First off, I install to latest version of rclone, was a bit out of date
-   I need to reauthenticate with Google remotely, as I'm using it on a server
    -   Here's the relevant bit https://forum.rclone.org/t/google-oauth-migration-for-rclone/30545/6
-   `rclone listremotes` to remember the name of my connection
-   `rclone config reconnect gsuite:` to then do the config
    -   y to refresh
    -   N to auto config
-   Turns out you need rclone available on a machine with a web browser then.
    -   install it on my local box
    -   run the authorize command it asked me to run
    -   it gives me a code at the command line on my local box, i copy that code to the command line on remote box
-   leave the other config alone

That seems to have sorted it!  Great project, is rclone.


## <span class="timestamp-wrapper"><span class="timestamp">[2023-03-20 Mon]</span></span>

I had to do this again just now, but on my local desktop this time, not on a headless server.

So at the `rclone config reconnect gsuite:` stage, this time I needed to say 'N' to auto config.  Otherwise I got the error:

> Access blocked: Rclone’s request is invalid

