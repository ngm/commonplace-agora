# Money-lite commoning

A way to reduce the need for money and markets.

> Commoning means relying on sharing, dividing up, and [[mutualization]] to meet needs as much as possible, and on collaborative financing. This lets people minimize their reliance on money and markets. We call this “money-lite” — the ethic and social practice of reducing one’s need for money as much as possible through sharing, co-using, DIT (do-it-together), and other practices that minimize reliance on market exchange. This is fundamental to all commons. Commoners can improve their long-term independence by withdrawing as much as possible from dealings with the market/state system.

<!--quoteend-->

> Money-lite commoning is how hackers helped neutralize Microsoft’s proprietary abuses of its monopolies over Windows and Office in the early 2000s (see pp. 128 and 167–168). They developed GNU/Linux, Open Office, and scores of other high-quality open source programs as practical, low-cost or no-cost alternatives to the standard programs offered by the market giants. Through shareable, peer-produced programs, people can create digital commons of their own and escape
> 
> &#x2013; [[Free, Fair and Alive]]

