# Nested-I

> It is an expression that helps us describe the practices and identity of a [[commoner]]. It overcomes the deeply rooted assumptions about individual identity and agency being opposed to collective goals. The Nested-I is an attempt to make visible the subtle, contextual social relationships that integrate “me” and “we.”
> 
> &#x2013; [[The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons]]

