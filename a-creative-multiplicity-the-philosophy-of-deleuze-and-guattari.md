# A creative multiplicity: the philosophy of Deleuze and Guattari

&#x2013; [A creative multiplicity: the philosophy of Deleuze and Guattari](https://aeon.co/essays/a-creative-multiplicity-the-philosophy-of-deleuze-and-guattari) 

I really enjoyed this article.

It gives a bit of back story to [[Deleuze &amp; Guattari]].  I find that helps give me a grounding, much like with A Short History of Nearly Everything.  

They met during [[May 68]].  Sounds like Guattari was the more political of the two.  I am fully on-board with a description of their work as "a progressive, Marxist-inspired, anti-capitalist politics of joy".

It's quite interesting though.  There seems to be some leaning towards a more anarchist than Marxist approach, though.  Very much anti-hierarchy, at least.  

Yet, at the same time, anti-individual.

> Deleuze and Guattari were both resolutely anti-individualist: whether in the realm of politics, psychotherapy or philosophy, they strived to show that the individual was a deception, summoned up to obscure the nature of reality.

I like how D&amp;G seem to sit somewhere between the horizontal and the vertical.

