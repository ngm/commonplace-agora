# Public-civic collaboration, coop incubators and stewarding digital infrastructure as a commons

https://forum.meet.coop/t/session-8-discussion/1003

Sophie Bloemen from [[Commons Network]].

[[Coop incubators]].
[[NGI Forward]].
[[Generative interoperability]].

Individual -&gt; community
Extractive -&gt; regenerative
Competition -&gt; cooperation
Linear -&gt; ecosystem

Commons / cooperative economy.

Need governmental support at all levels.

[[Public-collective partnerships]] 

