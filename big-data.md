# Big data

> Understood as the basic resource of communicative capitalism, big data has the characteristic of being self-renewing. It is inexhaustible and co-extensive with the reproduction of social life. It reaches through and beyond work, even beyond the reproduction of workers, into the social substance itself.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> UPS installed sensors as well as GPS in its trucks in an effort to increase efficiency and control costs. Data on more than 200 elements is collected, including truck speed, number of times the truck is put in reverse, driver seat belt use, the length of time a truck is idling.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Many such enterprises “track the time employees arrive, what they do at work, when they leave for breaks, the times they call in sick, schedule details, personal information and much more”, writes the author, Bill Barlow. Workforce analytics lets a company use this information “to optimize its labor force by scheduling the right mix of full-time, part-time and temporary labor on a variety of schedules”.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Approached in terms of [[class struggle]], big data looks like further escalation of capital’s war against labor.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> If earlier waves of automation displaced industrial workers, big data portends the displacement of post-industrial or knowledge workers
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> The value in and of big data is for capital, not for the people from whom it is expropriated.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

