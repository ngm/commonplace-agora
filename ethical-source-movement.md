# Ethical Source Movement

URL
: https://ethicalsource.dev/

> We are creating ways to empower developers, giving us the freedom and agency to ensure that our work is being used for social good and in service of human rights.

