# green capitalism

> Unless there is a revolution, the next couple of decades will be defined by a struggle between competing capitals — fossil capital on the one side, “green” capital on the other, with finance capital taking its cut from both  — vying for a greater share of the world’s ever increasing and unsustainable use of energy
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> Such thinking underestimates how much money there is to be made in a warming world. Mining companies buy land in Greenland with the knowledge that melting ice will reveal new mineral and oil reserves.16 Private security firms prepare to defend wealthy clients from civil unrest caused by droughts, floods, and famines.17 Dutch engineering companies sell flood-management expertise and plans for floating cities.18 Wealthy investors buy vast swathes of farmland in the Global South in hope of cashing in when droughts make arable land scarce.19 Many millions will die from the effects of global warming and capitalists are counting on it.
> 
> &#x2013; [[Revolution or Ruin]]

