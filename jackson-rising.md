# Jackson Rising

A
: [[book]]

Subtitle
: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi

Authors
: [[Ajamu Nangwaya]], [[Kali Akuno]]

Documenting the work of [[Cooperation Jackson]].

I didn't actually read every chapter yet.  But the ones I did were really inspiring.  A handbook of activist tactics in pretty adversarial circumstances.

It's available to read online as of 2021: https://jacksonrising.pressbooks.com/


## Notes from when social.coop read it

[[social.coop]] had a reading group on it April 22, 2018.

Here's my notes on each chapter plus some notes from the discussion.


### my notes


#### Main points

-   awesome comprehensive (and ambitious) plan
-   amazing what they achieved given the backdrop
-   eco-socialism (renewables, recycling, etc)
-   both economics and politics important
-   dual power - inside/outside - struggle and reform, build and fight
-   based on mondragon
-   education is vital
-   some excellent detail from people that have actually done it
    -   e.g. chapter on people's assemblies


#### chapter 1: build and fight: the program and strategy of cooperation jackson

-   very inspiring
-   also very practical
    -   comprehensive outline for [[solidarity economy]]
-   like the fact there's reuse/recycling
    -   and [[tool libraries]] explicity mentioned!
-   [[eco-socialism]]
-   interesting that they're working in the gaps of capitalism
    -   makes me wonder if London is a bit of a lost cause
-   The Program and Strategy of Cooperation Jackson
-   very comprehensive and ambitious strategy
-   sustainable communities initiative is cool
    -   green worker coops
    -   building an eco village
    -   developing food sovereignty
    -   just transition policy reform
-   ecovillage idea is very awesome
    -   building green affordable housing
    -   using solar &amp; renewables
-   avoiding gentrification is a big part of the movement
-   3IR &amp; 4IR
    -   [[third industrial revolution]]
        -   digital revolution
        -   e.g. internet, personal computer, mobile phones
    -   [[fourth industrial revolution]]
        -   cyber physical revolution
        -   e.g. robotics, 3D printing, CNC, AI, big data, etc
    -   really exciting that they plan to harness these
-   community production initiative
    -   using 3IR and 4IR for manufacturing
    -   centre for community production
        -   fablab/makerspace/coding/digital innovation
    -   really like this
        -   appears to my techy side I guess
        -   it seems quite utopian in scope (referencing star trek!)
        -   c.f. Adam Greenfield's cautions around 3D printing
        -   still, exciting stuff I think
-   education division is cool
    -   teaching the community to code
-   polycentric approach
    -   they recognise they must go beyond the municipal and work also within the wider region
-   multiracial and multiclass alliance
-   recycling cooperatives
-   love the fact they have presented their plans even though it could be to their detriment
    -   (by enabling enemies to keep a step ahead)
    -   but it's presented, in a sense, as an open source plan that others can use and refine
    -   also to elicit solidarity &amp; [[mutual aid]]
-   how to provide technical assistance to cooperation jackson?
    -   they ask for it in p.36


#### chapter 2: toward economic democracy, labor self-management and self-determination

-   "politics without economics is symbol without substance"
-   cooperatives &amp; solidarity economy are CJs economics of choice
    -   and labour self-management
-   "build a counterhegemonic practice that mirrors the embryonic values and institutions of the future socialist society, while living within the existing capitalist, patriarchal and racist social order" p.49
    -   coop economics and labour self-management help to attain this
-   solidarity economy work has to be accompanied by political struggle
    -   Jackson strongly influenced by [[Mondragon]] and Basque struggle for autonomy
-   "cultural revolutions typically precede political revolutions, as the former creates the social conditions for a critical mass of the people to embrace new social values that orient them toward the possibility of another world" p.53
    -   absolutely critical point
    -   this chimes with comments from Inventing the Future
-   "the constant disemintation of critical information&#x2026;" p.53
-   this requires educational structures
-   coop movement also requires financial institutions
-   "In order for cooperatives to be utilized as a tool of revolutionary social transformation their members must constantly struggle against being coopted by the institutions and other instruments of the bourgouis state" p.59
-   summary
    -   gives maybe a borader brush on the movement
    -   really enjoyed this one also
    -   [[counterhegemony]], [[prefiguration]], cultural revolution, education and financial institutions
    -   politics &amp; economics need to go together
    -   inspiration from Mondragon is very interesting, must read more on that
        -   (perhaps that could be in the next book club)


#### chapter 4: the jackson-kush plans

-   [[dual power]]: building autonomous power outside of the realm of the state and engaging electoral politics on a limited scale p.75
-   "platform for a restoration of the [[commons]]" p.75
-   important of alliance building
    -   including quite bespoke trading blocs
-   page 82 lists ways in which to help support the JK plan
-   summary
    -   this chapter gives a broader brush than the CJ outline.  I guess.  Talks to some degree about the wider political engagement and geographic engagement I suppose.
    -   the first chapter was more detailed I guess, more grassroots level.


#### chapter 5: people's assembly overview

-   1/5th of population needs to be involved in a [[People's Assembly]]
-   interesting discussion on different types of assemblies
    -   appreciate the recognition that mass assemblies demand a lot of people's time
-   People's Assemblies have the dual functions of dual power approach
    -   grassroots organising
    -   governmental pressure (including some political engagement where required)


#### chapter 6: building the city of the future today

-   cooperatives are a huge part of the plan
    -   "Jackson in many respects is poised to become the Mondragon of the United States"
    -   not possible to find all of the Jackson infrastrucuture plans through state/federal funding.
        -   need strategy partnerships with philanthropies and other NGOs


#### chapter 7: toward solidarity economy in Jackson

-   obviously heavily inspired by Mondragon
-   Ajamu Nangwaya is an anarchist
-   "as an anarchist, I am not a person who is hopeful or excited by initiatives coming out of the state or elected political actors" p110
-   imperatives (p.111)
    -   build capacity of civil society
        -   (transfer resource from the state)
    -   intersectionality
        -   not just some structureless context
    -   develop assembly governance system
    -   displace economic predators
        -   replace with coops/solidarity economy
-   From this chapter I'm picking up a definite feeling of
    -   get someone in power
    -   but only to the transfer resources to civic/solidarity structures wh


#### Critical eye

-   is it too ambitious?
-   how much has actually been implemented?
-   tensions in the dual power approach?
-   how will they deal with similar issues to Mondragon?
    -   i.e. existing in a wider global system


#### Questions for CJ

-   What tech do CJ use/need to organise?
    -   can we help?  I don't think we're in a great position to do hosting right now
    -   but we could give advice at least


### social.coop chat

caitlin - didn't get through whole book. main thing - they are doing it.
lots of socialists talk about the need for collective liberation. 
like how real it is. existing institutions. people who have a certain way of being.
designing unlikely futures. culture is usually exploiting.
not seen anything in print where they've put in all in one place like this.
actual means of working towards solidarity economy.
also liked how in one place they name all of the social ills. land disposession. 
talk about climate change.

jeff - relates to what caitlin said.
keenedy - we wanna go to the moon. very aspirational.
found curious - not clear how they are progressing?
not any reference to cooperation jackson in local newspaper.
they recognise people will be opposing the work they are doing.

matt
same reaction as caitlin. incredible to see everything in one place.
strategic vision that covers everything.
not seen this level of detail before.
they leave it open for change. flexible framework. leave it open for change.
another thing - integration of climate and the environment. first solidarity group that takes it so seriously.
use of technology is exciting.

neil
encouraged by level of detail
interested in how heavily based/inspired by mondragon
climate change and environmental (eco-socialist) emphasis was inspiring
tool lending, eco-housing type of organization named focus on technology was cool (third and fourth industrial revolution mentioned)
how much of this has been actually implemented?  aspirational, so how much has been done?
dual power: tension between being outside of politics and trying to get elected officials in office
article (http://blackrosefed.org/electoral-pursuits-have-veered-us-away-kali-akuno-on-movement-lessons-from-jackson/) about disagreement within group re: focus on getting elected, some members are anarchists and are weary of elected officials

emi
encouraged by chapter on people's assemblies.
in relation to how do you create governance structures, this was clearly articulated, a blueprint that other's can use.
want to translate it into japanese!
with tech - they talk about there relationship with labour.
explicit about things that have previously mostly only been written about in an abstract sense.

michele

quote: "we came in peace, but we came prepared"
links to kropotkin.
all revolutions failed because people were not prepared.
interlocking cooperatives.
crop growing cooperative to serve the cafe.
incredible organisation.  details and amount of practicality is epowering.
recent elections in italy - populist catastrophe.
sixth months prior to election - power to the people came about.
managed to get 1% of the vote.
people organising from the bottom up is the future.
this is the handbook to implement this type of policy from around the world.
 people of jackson have a strong identity because politically speaking they are a minority.
could this same policy work in europe?  there is little ethnic variation in europe.

Discussion. 

Comparison with Mondragon
reference: Values at Work (George Cheney)
many similaries, many differences
nobody in mondragon wrote a book about the work they did
others have, but even then, nowhere near as systematic
jackson rising conference 
the sources they quote are not just the easy sources
show CJ have a sophisticated take
including questions of growth
CJ very ideological, mondragon thought purely in terms of business development
CJ thinking about how much they have on the ground
contrast between buil.ding on the ground vs fleshing out
what can be acknoweldged as being successful

home healthcare coops in the US
why not have the workers in those fields capture the value of their labour

Replicability of this model in different contexts?
ex: London is so capitalistic that this might not be a good fit
Europe: minority/majority dynamic
neoliberalistic policies/ influence &#x2013;&gt; impact on introducing these types of concepts
Could have cooperation queens, cooperation brooklyn
could they join up?
Bullshit takes more effort to dismantle than produce
i.e. capitalism.  takes low energy to happen. requireds higher order of energy to happen.
Urban masses are not exposed daily to problems that are occuring
you don't see the problem with energy, with topsoil depletion
how do you 'market' the problem to people who don't see that it exists?
CJ addressing what people can do to help?
spreading the ideas helps

Catalyst for action?
lack of desperation, comfort in inaction
lack of action may also be due to having to acknowledge impoverished/disempowered position
does a level of desperation need to exist, to give something to rally around?
one of their advantages, is they bypass a lot of attention, gives them opportunity to develop


#### Gentrification

-   affordable housing is not a priority
-   book interesting look at gentrification
-   understanding that need political support to oppose gentrification
-   concept of development needs to be taken back
-   think of ourselves as developers


#### CJ hosted an ecosocialist convergence recently

CJ want international solidarity actions
relationship between Jackson and state of missisippi
city of jackson is majority black, 
what kind of support are they seeking internationally
people's climate assembly held at same time as UN convention
CJ was there
standing rock 

Passing of Chokwe
what effect did that have?

the groups that were key in starting CJ
politically important, but very marginal
they say if they get 5% active in assemblies then this is good
a lot of altnerative african american structures
CJ have managed to break out of the margins

Solidarity:
technical assistance
what are they currently using to coordination their organisation?
what online platforms? can we / floss community help ?
political support
how do you co-ordinate collaboration
FOSS?
Affiliate membership
outside influence/ assistance
online portal?
Nuts and bolts of getting people together
keeping notes, meeting process etc.
formal Roberts Rules approach
informal tech approach (real time minutes and access to notes)
how do you get everyone to participate? inclusion?
process fatigue: all ideas need to get involved
how to come to a decision about how to make decisions (without getting bogged down by the process/ discussion)
Digital divide
platform co-op world vs. capitalist platform world
what is CJ using? (same/similar as Neil's question)
area for supporting CJ from social.coop
how do they see coop model working in other parts of the world?
without the same strong social bonds that are present in jackson?
(also raised about mondragon too, simlar repression in the basque country)
CJ is not only coop solution in black community in the south
what is the relationship with other groups?
doesn't come in the news
Where is the funding coming from for CJ?
Grassroots organizing vs. elected officials
getting elected officials (getting people in power) to use state resources = long term strategy

