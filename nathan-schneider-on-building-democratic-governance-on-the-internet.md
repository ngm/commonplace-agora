# Nathan Schneider on Building Democratic Governance on the Internet

https://david-bollier.simplecast.com/episodes/nathan-schneider-on-building-democratic-governance-on-the-internet-pRpyDBjL

-   Love this interview of [[Nathan Schneider]] by [[David Bollier]]. New book out, [[Governable Spaces]]. Sounds like a synthesis of a bunch of recent papers (e.g. [[Governable Stacks]]).
-   Main thesis is about the need for inclusion of democracy in our everyday online tools. To avoid '[[Implicit feudalism]]'.
-   [[Decidim]] referenced favourably.

Democracy in everyday life is important for engendering democracy in politics. ( something like that? referenced de toqueville )

