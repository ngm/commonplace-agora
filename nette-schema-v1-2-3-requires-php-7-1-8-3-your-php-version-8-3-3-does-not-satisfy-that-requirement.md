# nette/schema v1.2.3 requires php >=7.1 <8.3 -> your php version (8.3.3) does not satisfy that requirement.

I updated packages on Mint like a good boy, and now I'm getting complaints from composer when building a project, because PHP version is too new.

composer says:

> Your lock file does not contain a compatible set of packages. Please run composer update.

And:

> Problem 1
> 
> -   nette/schema is locked to version v1.2.3 and an update of this package was not requested.
> -   nette/schema v1.2.3 requires php &gt;=7.1 &lt;8.3 -&gt; your php version (8.3.3) does not satisfy that requirement.
> 
> Problem 2
> 
> -   nette/utils is locked to version v3.2.9 and an update of this package was not requested.
> -   nette/utils v3.2.9 requires php &gt;=7.2 &lt;8.3 -&gt; your php version (8.3.3) does not satisfy that requirement.
> 
> Problem 3
> 
> -   nette/schema v1.2.3 requires php &gt;=7.1 &lt;8.3 -&gt; your php version (8.3.3) does not satisfy that requirement.
> -   league/config v1.2.0 requires nette/schema ^1.2 -&gt; satisfiable by nette/schema[v1.2.3].
> -   league/config is locked to version v1.2.0 and an update of this package was not requested.

Running composer update usually causes a world of pain.

