# Tools for thought

I remember this phrase from the book by [[Howard Rheingold]] that I read a long while ago. It's coming back in to fashion with PKM tools like [[Roam]] and [[org-roam]].

> It's easy to forget, but the web began as a tool for thought, and it had many of these same properties — personal, multiplayer, distributed, evolvable. It lost most of the personal and distributed aspects as it grew into an app platform. Perhaps we might carve out a bit of space for those again?   
> 
> &#x2013; [[Building a Second Subconscious]]

