# Cost of Living Campaigns Should Fight for a Green Transition

URL
: https://novaramedia.com/2022/08/16/cost-of-living-campaigns-should-fight-for-a-green-transition/

Author
: [[Kai Heron]]

Nice article on from a climate/social justice 'same fight' perspective.  Useful set of policy demands.

[[Cost of living crisis]] and [[green transition]].

> This all adds up to what Marxists call a crisis in the ability of the working class to reproduce itself within capital’s circuits of accumulation.


## Demands


### Build free and expanded public transportation networks.

[[Public transport]].


### Expand no-car zones in cities and better cycling infrastructure.

> reduce car usage, free public transport must be combined with prohibitions on car travel in selected areas

<!--quoteend-->

> The bike is a cost of living and climate crisis fighting machine. Maintaining a bike is easier and cheaper than maintaining a car, produces no emissions and creates far less particle pollution.

<!--quoteend-->

> There’s also poor infrastructure for those wanting to make journeys between cities and suburban or rural areas: cyclists are frequently forced to take dangerous A-roads or B-roads where close passes at high speed are common. The UK’s cycling network is just 12,739 miles long, less than half of which is off road. The Netherlands, meanwhile – which is a sixth of the UK – has a network that’s 21,748 miles long. It’s no surprise, then, that while just 2% of journeys in the UK are taken by bike, 27% are in the Netherlands.


### Rollout publicly-funded housing insulation and heat pumps.

[[Insulation]]


### Transform our food systems.

> It’s not just energy bills that have been hit by inflation. The average food bill has risen by £454 a year as prices of staples hit a 14-year high.

<!--quoteend-->

> Industrialised farming has reduced flying insect numbers by 60% in 20 years, and bird numbers have also declined dramatically.

<!--quoteend-->

> monocultural fossil fuel dependent systems should be replaced by agroecological farming practices. By implementing region-specific methods that integrate livestock and cropping and incorporate crop rotations featuring nitrogen-fixing legumes, Europe could feed its projected population in 2050 without feed imports and with reduced synthetic fertiliser use


### Nationalise energy companies.

[[We should have public ownership of energy]]


### Introduce a four-day work week.

[[Four day week]]

