# veganism

> in the US, for example, vegans are disproportionately working-class people of colour
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The easiest – and perhaps only – way to achieve large-scale reforestation and feed the world at the same time is through widespread veganism
> 
> [[Half-Earth Socialism]]

