# Immanuel Wallerstein

> a highly influential sociologist and radical intellectual
> 
> &#x2013; [[The Brilliant Immanuel Wallerstein Was an Anticapitalist Until the End]]

<!--quoteend-->

> Wallerstein was the last surviving member of the affectionately named “Gang of Four” — an ensemble of scholars dedicated to the study (and abolition) of global capitalism that also included [[Samir Amin]], Andre Gunder Frank, and my doctoral advisor, Giovanni Arrighi
> 
> &#x2013; [[The Brilliant Immanuel Wallerstein Was an Anticapitalist Until the End]]

