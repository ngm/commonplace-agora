# Putting the space in cyberspace

> Today's web browsers want to be invisible, merging with the visual environment of the desktop in an effort to convince users to treat "the cloud" as just an extension of their hard drive. In the 1990s, browser design took nearly the opposite approach, using iconography associated with travel to convey the feeling of going on a journey.
> 
> &#x2013; [Cyberspace, the old-fashioned way | Rhizome](https://rhizome.org/editorial/2015/nov/30/oldweb-today/) 

This feels related to [[spatial software]].  A return to the notion of journey, space and travel.  A desire that has been expedited by being stuck at home.  What was the reason for the original flattening?

> Navigation. Exploration. Browsing. Surfing. The web was akin to a virtual manifestation of physical space. We even had a word for it: webspace. In Geocities, this was expressed with the notion of neighborhoods, creatively-named categories like Area51 for sci-fi, Heartland for families and pets, RainForest for the environment, Vienna for classical music, CapeCanaveral for science and mathematics.
> 
> &#x2013; [Rediscovering the Small Web - Neustadt.fr](https://neustadt.fr/essays/the-small-web/) 

