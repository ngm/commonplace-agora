# What Is To Be Done? with Breht O'Shea and Alyson Escalante

A
: [[podcast]]

URL
: https://www.upstreampodcast.org/conversations

Series
: [[Upstream]]

Featuring
: [[Breht O'Shea]] / Alyson Escalante

Discussion of [[revolutionary strategy]].  With [[What is to be done?]] being the jumping off point for the discussion.

[[Economism]] and [[vanguardism]]. Economism thinking that with enough economic reforms you'll get rid of capitalism. E.g. coop movement, social democratic minimum wage reforms, empowering unionisation, etc.  These things good and necessary of course, but economism would say they're all you need for change.

Spontaneity vs organisation. Particularly with reference to economism here.

Mentions Richard Wolff as temporaneous example of economism mindset.

-   01:02:33 Reforms are often rolled back by reactionary politics counter revolution.
-   01:09:05 Spontaneity vs conscious action. [[Occupy]], [[Black Lives Matter]], etc.

They obviously favour party organisation, but I like that they say spontaneous activity is important. it just needs organising to go with it. a la [[Neither Vertical Nor Horizontal]].

Alyson says Occupy didn't really fizzle out, it was crushed by the state.

-   01:23:06.  Spontaneity easily devolves/coopted into incoherent violent protest, making call for law and order easy for the state. France as current example of spontaneous protest not challenging the state.
-   01:27:22. Structure of the party is very centralised and hierarchical&#x2026;
-   01:37:41. Good description of [[vanguard party]]. I find it a bit intuitively problematic.  But they have good points.
-   01:41:13. [[Mondragon]] as example of politics less coop movement losing its way.
-   01:43:29. Do we need an external rupture? No - capital is very good at adapting to rupture. See eg [[Climate Leviathan]]. Also, if you're building the party in the middle of the crisis then you're already too late.
-   01:52:08. Sometimes political education is prerequisite of organisation.

