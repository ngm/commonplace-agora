# water cycle

> The water cycle shows the continuous movement of water within the Earth and atmosphere. It is a [[complex system]] that includes many different processes. 
> 
> &#x2013; [Water cycle | National Oceanic and Atmospheric Administration](https://www.noaa.gov/education/resource-collections/freshwater/water-cycle) 

The water cycle is the process by which water moves from the Earth's surface to the atmosphere and back again.

It is driven by the sun's energy, and it is essential for sustaining life on Earth.

The water cycle has four main stages: evaporation, condensation, precipitation, and collection

