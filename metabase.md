# Metabase

-   https://www.metabase.com/

We use Metabase for ad-hoc business analytics.

It's open-source and very handy.

Always worth evaluating alternatives though:

-   [Metabase Limitations and Top 4 Alternatives (BI)](https://www.holistics.io/blog/metabase-limitations-and-top-alternatives-bi/)

