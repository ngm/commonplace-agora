# Slow computing

> slow computing (Schneider 2015), its pace measured not by band- width or processing speed but by the attention to the social dimensions of everyday practice.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

