# Ansible

[[Infrastructure as code]].

Configuration management.

neil: Getting started with Ansible. Definitely enjoying it as a reproducible way of documenting all the steps involved in a server’s setup.

But… am I going to forget how to do all the actual underlying commands if everything is just an Ansible task in a yaml file? What if I need them in an emergency??

brennen: yes.

brennen: i don't care for ansible very much in general, but apart from implementation details i've forgotten (it's been a few years since i used it intensively), i think most of my discontent is with the entire class of tool it occupies.  

as far as i can tell, configuration management is an unsolved problem and there a lot of disconnects between the ways things like puppet, chef, ansible, helm, k8s, et al. claim to model system state and how that state is actually achieved.

neil: My journey so far is: a) I have a bunch of servers I've configured manually over time, this is not good for disaster recovery; b) maybe I'll just write a shell script with all the steps; c) may as well use something someone else has done for this; d) this is cool but&#x2026;is it going to be more trouble than it's worth?

brennen: i wish i had a good answer to that, but i really really don't.  at work, i swim in a sea of ad hoc tooling (shell, python, php, golang), puppet manifests, jenkins jobs, templated dockerfiles, and deeply nested yaml.  on a very good day i can probably claim to understand about 1% of it.

all that stuff is what it is for Reasons, but in my personal life it's all kind of driven me back in the direction of least-common-denominator shell scripts and text files with some notes about what to run.

brennen: i think a lot of people get good value out of something like ansible (or even, say, something like https://www.fabfile.org/), but i think it's good to know it can be a real tarpit going in and consciously try to keep things simple.

neil: I'm doing pretty small-fry stuff so if I can get by with something not too much far above shell scripts and docs, that'd be my ideal.

