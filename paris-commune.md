# Paris Commune

Paris, 1871.

> The city was transformed into an autonomously organized society, a [[commune]] that experimented with alternative ways of structuring social and political life based on collaboration and cooperation
> 
> -   [The Judgment of Paris | Lizzie O’Shea](https://thebaffler.com/salvos/the-judgment-of-paris-oshea)

An example of the [[dictatorship of the proletariat]].

Didn't last long (72 days) - it was crushed by the state.

It was not completely [[Marxist]], not completely [[anarchist]].  It had its own thing going on.  (And existed before these distinctions were that clear, anyway).

The Paris Commune was instructive to both Marx and Lenin.

> Marx’s observation in the context of his analysis of the lessons of the Paris Commune, that 'the working class cannot simply lay hold of the ready-made state machinery, and wield it for its own purposes' - is hard to miss.
> 
> &#x2013; [The Bolsheviks did not 'smash' the old state // New Socialist](https://newsocialist.org.uk/bolsheviks-did-not-smash-old-state/) 

<!--quoteend-->

> when the armed people of the French capital raised barricades not only to defend the city council of Paris and its administrative substructures but also to create a nationwide confederation of cities and towns to replace the republican nation-state.
> 
> &#x2013; [[The Communalist Project]]


## Resources

-   [ Revolutionary Left Radio: The Paris Commune: A Brief Blossoming of Proletarian Power](https://revolutionaryleftradio.libsyn.com/the-paris-commune-a-brief-blossoming-of-proletarian-power)
-   [[After Storming Heaven]]
-   [[Communal Luxury - NovaraFM]]
-   [[The Radical Imagination of the Paris Commune]]

