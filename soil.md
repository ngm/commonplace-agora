# soil

> “Ninety-nine percent of our calories come from soil. Everything we are, everything we have built, everything in our lives comes from the soil. Without it, we’re finished,” says the author and environmentalist [[George Monbiot]]. “And yet, we treat it with extreme disrespect and disregard.”
> 
> &#x2013; [[How to feed the world without destroying it]] 

