# rhizome

> Deleuze and Guattari use the terms “rhizome” and “rhizomatic” to describe theory and research that allows for multiple, non-hierarchical entry and exit points in data representation and interpretation.
> 
> &#x2013; [The Rhizome - Cryppix - Medium](https://medium.com/@cryppix/rhizome-cf443e9e10b6)

![[images/rhizome.png]]

> [[Deleuze and Guattari]] introduce A Thousand Plateaus by outlining the concept of the rhizome (quoted from A Thousand Plateaus):
> 
> \\1 and 2. Principles of connection and heterogeneity: "&#x2026;any point of a rhizome can be connected to any other, and must be";
> \\3. Principle of multiplicity: it is only when the multiple is effectively treated as a substantive, "[[multiplicity]]", that it ceases to have any relation to the One;
> \\4. Principle of asignifying rupture: a rhizome may be broken, but it will start up again on one of its old lines, or on new lines;
> \\5 and 6. Principle of cartography and decalcomania: a rhizome is not amenable to any structural or generative model; it is a "map and not a tracing".
> 
> https://en.wikipedia.org/wiki/Rhizome_(philosophy)

![[2020-06-07_16-12-50_rhizome.png]]

