# digital degrowth

-   [Digitalization and degrowth | degrowth.info](https://degrowth.info/blog/digitalization-and-degrowth)
-   [Patterns for digital degrowth](https://zagreb.degrowth.net/en/9_int_dg_conf/public/events/184)
-   [Digital degrowth: toward radically sustainable education technology](https://www.tandfonline.com/doi/abs/10.1080/17439884.2022.2159978)

(edtech specific?)

-   [Unlocking wise digital techno-futures: Contributions from the Degrowth commun&#x2026;](https://www.sciencedirect.com/science/article/pii/S0016328719303362)
-   [What might degrowth computing look like? – Critical Studies of EDUCATION &amp; TE&#x2026;](https://criticaledtech.com/2022/04/08/what-might-degrowth-computing-look-like/)

