# Updating to org-roam 1.2.2

[org-roam 1.2.2](https://github.com/org-roam/org-roam/releases/tag/v1.2.2) was released 19 days ago.  I'm updating to it.


## Get the new package

First off I do a blanket update of packages in spacemacs with `SPC f U`.  (Is there a way to update just one package?)  Then I restart Emacs to do the installs proper.

As part of that, [[org-roam]] does a db update.

> (org-roam) Processed 1/1019 modified files&#x2026;
> &#x2026;.
> (org-roam) Processed 1017/1019 modified files&#x2026;
> (org-roam) Processed 1018/1019 modified files&#x2026;
> (org-roam) total: Δ1019, files-modified: Δ1019, ids: Δ0, links: Δ4221, tags: Δ4, titles: Δ1056, refs: Δ6, deleted: Δ0

On restart, I'm sure my theme looks different&#x2026; yeah, there's a message "error: (error Unable to find theme file for ‘doom-one’)"  Hmm.  Dunno.  I seem to be able to load it regardless though.  Will come back to that.


## Fuzzy links

So anyway, org-roam.  The main new feature seems to be the addition of 'fuzzy links'.

My notes on setting it up here: [[Using fuzzy links AKA wikilinks in org-roam]].


### Completion algorithm

I find the completion algorithm a little weird though.  It seems to favour fuzzy matches over partial words.  Hey ho, just another thing to configure I guess&#x2026;

So it turns out that's from a previous config change I needed to make for when spacemacs helm-completion that wasn't working.

I had 

```emacs-lisp
(setq completion-styles '(helm-flex))
```

as per this issue.  So the `completion-styles` setting also affects company.  Hence my weird completions.  But that issue has now been resolved upstream, so I can remove it, and the company completions work again.

First I guess I need to update spacemacs though, so I get that update to the helm layer.

OK.  When I do that, nothing in org-roam works.  There's a message at startup:

> Warning: More than one init function found for package org-roam. Previous owner was org, replacing it with layer org-roam

Some info on that here: [Spacemacs: More than one init function found for package org-roam - Troublesh&#x2026;](https://org-roam.discourse.group/t/spacemacs-more-than-one-init-function-found-for-package-org-roam/870/2).

I fixed that by changing my include of org-roam layer like so:

```emacs-lisp
  ; before
(org-roam)
  ; after
    (org :variables
	 org-enable-roam-support t)
```

OK.  The helm completion thing works now, without needing that old config workaround.  And company-at-point matches nicely on my roam note titles.


### Setting the company-mode backend

So back to my problem that I had before of turning off company-mode for my writing mode.  I guess rather than turning it off, I should remove the particular backend.

Actually for now, I'm copping out and just setting the specific backend. Might need to come back to this I guess.

```emacs-lisp
(defun ngm-journal-mode ()
  "Set up journalling mode the way that I like it"
  ; ...
  (setq company-backends '(company-capf)) ; for org-roam completion
  )
```


### Company overlay

My company overlay looks a mess, probably because of something I do in `ngm-journal-mode`.  Oh well - going to leave that one for now.


## org-roam.db

The other thing I need to check is something about the default location of org-roam.db moving to your user dir.  I have a private wiki and a public one, so I wonder if this will cause a clash somehow.  I think I'm setting it in one of them with `.dir-locals.el`, so should be OK.

