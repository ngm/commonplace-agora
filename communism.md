# Communism

[[Communist Manifesto]].

> Communism is a democratic society that is based on equality, justice, fairness, solidarity, wealth and luxury for all, and participatory democracy.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

