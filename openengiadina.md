# openEngiadina

URL
: https://openengiadina.net/

.

> is developing a platform for open local knowledge.

<!--quoteend-->

> openEngiadina is an acknowledgment that data creation and curation is a social activity. We are developing a mashup between a knowledge base (like Wikipedia) and a social network using the XMPP protocol and the Semantic Web.

<!--quoteend-->

> The network protocol we have been using is ActivityPub. This seemed to be a perfect fit. **We believe that managing knowledge is a social activity** and ActivityPub is primarily a protocol for social interactions. Furthermore, ActivityPub can be extended to allow arbitrary RDF content via the JSON-LD serialization.
> 
> &#x2013; [openEngiadina: From ActivityPub to XMPP — inqlab](https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html) 

^ note they go on to say why ActivityPub wasn't the right fit.  But "We believe that managing knowledge is a social activity" is very interesting.

