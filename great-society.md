# Great Society

> For Hayek, capitalist society does not impose a collective purpose on its denizens, and is therefore a “Great Society” unprecedented in world history. The principal merit of this order is that it allows participants to coexist without having to discuss their aims, much less agree on them
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> That we assist in the realization of other people’s aims without sharing them or even knowing them, and solely in order to achieve our own aims, is the source of strength of the Great Society
> 
> &#x2013; [[Review: People's Republic of Walmart]]


## Criticism

> In fact, capitalist society does impose a collective project on its members: the endless accumulation of capital
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> As the philosopher John O’Neill notes, Hayek’s argument depends for its force on the assertion that “beliefs about values do not answer to rational argument
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> For more on the “liberal mirage” of a capitalism as a society without a collective purpose, see Murray
> 
> &#x2013; [[Review: People's Republic of Walmart]]

