# Acorn

URL
: https://acorn.software

> Peer-to-Peer Agile Project Management For Software Teams

<!--quoteend-->

> Acorn is an open-source, [[peer-to-peer]] [[project management]] application. It is designed and built as a scrum-alternative, Agile Development Pattern for distributed software development teams.


## Testing Acorn

-   join a project, put in the five word code
    
    -   
    
    ![[Testing_Acorn/2022-12-18_22-36-10_screenshot.png]]
-   Luckily, Flancian was online so I didn't have to wait :)
-   I can see the issues that Flancian has created.  I've commented on one - wonder if he sees it.
-   Immediate initial thought is that there is a need for async chat, that comments probably won't cut it for.  So you'd still need to discuss matters in e.g. Element.
    -   looks like you can't @ people.
-   Also I don't think you can edit comments.
-   I wonder where is the data stored on my machine?
-   You don't seem to be able to remove children.  A bit annoying as I want to amend the structure.

