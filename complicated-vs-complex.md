# Complicated vs complex

> These reciprocal feedback loops are not just [[complicated]]—they're also [[complex]]. In everyday language, we tend to use these two words interchangeably, but, in the world of [[systems theory]], they're very different. A system can be complicated but not complex, no matter how large, if each of its components and the way they relate to each other can be completely analyzed and given an exact description. A jumbo jet, an offshore oil rig, and a snowflake are all examples of complicated systems. A complex system, on the other hand, arises from a large number of nonlinear relationships between its components with feedback loops that can never be precisely described. Any living thing, or system comprising living things, is complex: a bacterium, a brain, an ecosystem, a financial market, a language, or a social system.
> 
> &#x2013; [[The Patterning Instinct]]

