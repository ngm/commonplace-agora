# Mutual aid

Caring for each other.  Changing the world to make it more caring.

> Mutual aid is the radical act of caring for each other while working to change the world.
> 
> &#x2013;  https://www.versobooks.com/books/3713-mutual-aid

<!--quoteend-->

> Mutual aid projects are a form of political participation in which people take responsibility for caring for one another and changing political conditions. 
> 
> &#x2013; [Mutual aid (organization theory) - Wikipedia](https://en.wikipedia.org/wiki/Mutual_aid_(organization_theory)) 

<!--quoteend-->

> 🚨 🚨DRUMROLL PLEASE!!!🚨🚨
> 
> \#FromBelow now online for FREE! 
> 
> A #documentary film about #mutualaid and community action in England during #Covid and beyond.
> 
> It speaks to the hard times we faced, and continue to face during the cost of living crisis, and what can potentially be done. Those interested in #ukpolitics and getting the #toriesout will no doubt find it affirming. 
> 
> Please spread far &amp; wide!
> 
> https://mutualaid.uk/feature-film/
> 
> &#x2013; https://mas.to/@olimould/109998551285154312

