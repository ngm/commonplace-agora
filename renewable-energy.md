# Renewable energy

> What would a completely renewable energy system look like in terms of land use? Energy expert Vaclav Smil estimates that such a system would take up 25 to 50 per cent of the US land-mass, while rich and densely populated countries like the UK would have a ratio approaching 100 per cent
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> renewable energies are extremely resource-intensive, and building an infrastructure for renewable energies will itself continue to consume huge amounts of fossil energy.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

<!--quoteend-->

> order to keep the prices of raw materials low enough so that the prices of renewables do not exceed those of fossil fuels, an imperialist race is already underway to control and develop raw material deposits. A “green” capitalism is therefore, necessarily an imperialist one
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

[[green capitalism]]

