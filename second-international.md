# Second International

> After the collapse of the Second International, there was an institutional distinction between Socialists and Communists. Whereas reformism dominated the Socialist International, Stalinism became dominant in the Communist International. The notion of ‘socialism’ became associated with social democratic parties and the notion of ‘communism’ with communist parties.
> 
> &#x2013; [[Communicative Socialism/Digital Socialism]]

<!--quoteend-->

> “Leading theorists like Karl Kautsky specifically rejected workers’ control, arguing that the complexities of the advanced industrial economy and the modern enterprise precluded bringing democratic procedures directly into the economy itself.” 17 Questions over the organization of production were scientific, not political, affairs. Instead, the SPD maintained, politics should train its sights on seizing the reins of the economy by contesting for leadership of the national parliament.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Luxemburg’s 1899 polemic against Bernstein’s “revisionism,” Social Reform or Revolution , takes up this very question. In it, she argues that the contradictions of capitalism had not been overcome, but had been intensified, even as the final crisis was temporarily postponed. In doing so, Luxemburg reveals her continued debt to Kautskyian determinism: she counters Bernstein’s “ethical socialism” with the “objective necessity of socialism, the explanation of socialism as the result of the material development of society”—in other words, a socialism proceeding mechanically from a terminal crisis stemming from internal contradictions of capitalist production.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> This determinist belief ultimately formed the basis of Kautsky’s critique of the 1917 Bolshevik Revolution, which, in organizing a seizure of power in a peasant-majority country where capitalist social relations had only begun to take hold, had not followed the orderly progress of developmental stages.
> 
> &#x2013; [[Breaking Things at Work]]

