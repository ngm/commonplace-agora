# 'Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives

URL
: https://www.ebb-magazine.com/essays/imperialism-runs-deep-interview-with-robert-biel-on-british-maoism-and-its-afterlives

Interview with [[Robert Biel]].  Lots of interesting stuff.  Good snippets of history of international relationships from a left perspective.  Regardless of what you might think about Maoism, Biel seems like a good egg.

[[Imperialism]], [[Anti-imperialism]]. [[Maoism]].  [[British Maoism]].  [[Western Maoism]].

Biel was part of the [[Revolutionary Communist League of Britain]] in the 1970s/80s.

> understand the world in the process of striving to change it

<!--quoteend-->

> [[Chartists]], and sometimes signed our statements ‘[[William Cuffay]]’, adopting the name of the Black former slave who was one of their key leaders

<!--quoteend-->

> The economic foundation of this argument was totally unconvincing. The reason given for the alleged triumph of the Soviet system is that it was rational and centrally planned. But central planning, even though it may have a certain role, has never been the main criterion of socialism. And in fact, the quest for complete predictability is conceptually the antithesis of a socialist project which should on the contrary be edgy, thrilling, open to the unexpected. We need a decentralised society which can develop many nodes and zones of creativity, which defy rigid categories.

<!--quoteend-->

> If we are striving to build a movement for the long term, we would have to look at issues of [[care]]. Marxist-Leninists used to speak a lot about ideological ‘remoulding’, without realising this is a sensitive issue with a lot of dimensions of psychology. We are whole human beings, and it’s impossible to isolate some political or ideological faculty and treat it as separate from the rest of our existence. The binary opposition between politics and ourselves, as humans, is unhealthy and antithetical to wellbeing. It’s only by revealing, and correcting, these errors, that we can really open ourselves to absorbing the lessons from indigenous perspectives, feminism, queer reflection on the body, and of course nature/ecology as the indispensable context for our existence

<!--quoteend-->

> I think the imperialist project is partly about forging a stunted humanity (cyborg soldiers and administrators) in which any caring faculty has been neutered. In contrast, the liberation movements made culture central, because it’s a way of liberating the whole human being, in the framework of our relation to the natural world

