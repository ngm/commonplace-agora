# Pakistan Floods and COP 27 with Asad Rehman

URL
: https://www.gndmedia.co.uk/podcast-episodes/pakistan-floods-and-cop-27-with-asad-rehman

Featuring
: [[Asad Rehman]]

[[2022 Pakistan floods]] and [[COP 27]]

-   ~00:04:02  Relative vulberabity of global north and global south
-   ~00:08:45  Majority of Pakistan's taxes go on debt repayment ([[Global debt peonage]])
-   ~00:20:27  Why are countries poor? [[Walter Rodney]].
-   ~00:23:49  Global south develops global north.
-   ~00:27:28  Humanitarian aid but no [[loss and damage]]
-   ~00:33:21  Climate denialism is not anymore of climate change but of mitigation

