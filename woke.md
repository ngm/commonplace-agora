# Woke

> I don't know who needs to hear this, but the term "woke" didn't start with kids on Twitter.
> 
> The was first recorded by Lead Belly in the 1930s in a song called "The Scottsboro Boys," about the dangers Black Americans faced traveling through states in the Deep South. The original line was "best stay woke," as in, "remain aware of the dangers of racism."
> 
> Black communities shortened the term to "stay woke," over time, and eventually added "woke" as a state of being&#x2013;or constant awareness.
> 
> &#x2013; https://robot.rodeo/@mike/110782319068894541

