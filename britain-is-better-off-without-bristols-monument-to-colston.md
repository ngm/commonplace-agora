# Britain is better off without Bristol's monument to Colston

Articulated here: [The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down) 

Fuck yes.

Not in a public celebratory view, anyway.  It's still in a museum to reckon with the past.

[[Colston]].  [[Slavery]].


## Because

-   Colston was a slave trader, one of Britain's wealthiest
-   [[Figures such as Colston are not simply remote characters from history]]
-   [[Britain's history contains astonishing levels of greed and cruelty]]
-   [[Acknowledging historic injustices is part of building a more equal society today]]
-   [[The built environment reinforces cultural messages]]


## But

-   Tackling racism requires a lot more than removing statues


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Without a doubt]]

