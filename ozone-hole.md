# ozone hole

> Perhaps the strangest aspect of this story was that NASA satellites had long taken measurements of Antarctic ozone every day and recorded the same decline as Farman, but the results were so anomalous that the data-processing programme junked them
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The gravity of the situation was quickly recognized, and by 1987 governments from around the world had agreed to the Montreal Protocol, which banned the use of CFCs everywhere. It took time for this impressive diplomatic feat to have some effect, and the ozone layer continued to thin until the late 1990s, when it was a mere 33 per cent of its normal thickness.  Even now it is nowhere close to full recovery
> 
> [[Half-Earth Socialism]]

