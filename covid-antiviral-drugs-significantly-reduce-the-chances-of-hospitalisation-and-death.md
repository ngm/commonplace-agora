# Covid antiviral drugs significantly reduce the chances of hospitalisation and death

[[Coronavirus]] [[Covid antiviral drugs]]

[‘More people will die’: fears for clinically vulnerable as England axes plan &#x2026;](https://www.theguardian.com/world/2022/jan/22/more-people-will-die-fears-clinically-vulnerable-england-axes-plan-b)


## If

-   taken during the first few days of illness.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

I believe it.

