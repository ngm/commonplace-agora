# How to follow me



## All my posts

You can subscribe and get notified of the things that I post in a few ways.


### Old skool

You can just remember that you like me and come back to my blog now and then to see what I'm up to :)


### With an RSS reader

You can subscribe to my full RSS feed here: https://doubleloop.net/feed


### With a social reader

You can follow me in a [[social reader]].  It's better than RSS, you can interact with posts right in your reader, a bit like what you'd do in Twitter.  But you'll need your own [[IndieWeb]] site to do it.  


### On the Fediverse

If you use Mastodon or similar, you can get subscribe to me as @neil@doubleloop.net.  Or you can also follow me @neil@social.coop which I sometimes cross-post coop related stuff to.


## NAQ (Never Asked Questions)


### Can't I just follow you on Twitter?

I don't like to use Twitter.  I have a few notes on why [[here]].

