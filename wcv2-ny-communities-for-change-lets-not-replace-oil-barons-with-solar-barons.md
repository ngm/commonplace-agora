# WCV2 NY Communities for change "Let's not replace Oil Barons' with Solar Barons'"

A
: [[podcast]]

Lives at
: https://www.gndmedia.co.uk/podcast-episodes/wcv2-ny-communities-for-change-lets-not-replace-oil-barons-with-solar-barons

Part of
: [[Working Class Voices]]

Features
: Alice Hu

Discussion around [[working class and green politics]].

Alice Hu one of main organisers of [[March to End Fossil Fuels]].

Interesting stuff at the end about tactic of arrests and what works from working class perspective.

Arrests for arrests sake seems inauthentic Ads says. Arrest for an actual action against some kind of fossil infrastructure seems more authentic.

Ads mentions that things like soup on a painting doesn't move the needle at all for working class engagement. "Two posh people messing up a posh painting in a posh place" he says something like.

Alice mentions that that particular action got huge coverage worldwide, and more attention for an action she was involved in organising a couple of weeks later.

