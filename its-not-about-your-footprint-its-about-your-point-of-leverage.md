# It's not about your footprint, it's about your point of leverage

URL
: https://fivetimesfaster.org/blog/its-not-about-your-footprint-its-about-your-point-of-leverage/

I like this idea.  I've been aware for a while that [[carbon footprint]] isn't a great measure.  This article reiterates that, and suggests that its better to think about your [[leverage points]] instead.  Which obviously resonates with idea of leverage points in [[systems thinking]].

