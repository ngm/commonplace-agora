# Google joins Samsung in working with iFixit on a self-repair program

URL
: https://www.theverge.com/2022/4/8/23016233/google-pixel-smartphones-ifixit-repair-program

[[Google]] joins Samsung in working with [[iFixit]] on a [[self-repair]] program.

[[right to repair]].

> As of the Pixel 6, Google is promising three years of Android updates and five years of security updates, which could see the phones being used into late 2026. At that point, it’s all-but-guaranteed that a phone will need a battery replacement or some kind of repair at least once over its lifetime, which makes easy access to spare parts vital

