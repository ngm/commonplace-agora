# Debian

[[GNU]]/[[Linux]].


## Values

-   [Debian Constitution](https://www.debian.org/devel/constitution)
-   [Diversity Statement](https://www.debian.org/intro/diversity)
-   [Debian Social Contract](https://www.debian.org/social_contract)
-   [Debian Code of Conduct](https://www.debian.org/code_of_conduct)

