# Xanadu

[[Ted Nelson]]'s [[hypertext]] project.


## Attempts at implementation

Interesting thread from John Ohno on attempts at implement Xanadu concepts - https://threadreaderapp.com/thread/1232381856294875137.html

[[Roam]] and [[wikis]] get namechecked as examples of regularly occurring attempts to rebuild Xanadu concepts, badly..
I suppose you could argue, at least they actually exist :)

I like [[Maggie Appleton]]'s interpretation in [[The Pattern Language of Project Xanadu]] that we can should perhaps see Xanadu as an increasingly more utilised [[pattern language]], and stop just thinking of it as a failed software project.


## Twin Pages

-   http://thoughtstorms.info/view/Xanadu
-   http://webseitz.fluxent.com/view/Xanadu
-   https://en.wikipedia.org/wiki/Project_Xanadu

