# The Case Against the Professional Managerial Class with Catherine Liu

A
: [[podcast]]

URL
: https://www.upstreampodcast.org/conversations

[[Professional-managerial class]].

~00:02:29  PMC is technically a stratum of the working class but kind of aligned with capitalist class.

~00:21:07  Idea that PMC tend to think that they are more virtuous than other strata of working class.

~00:53:28  [[Taylorism]] and PMC.

~00:58:46  [[New Left]] was dominated by PMC.

~01:06:19  PMC always favours [[incrementalism]] over [[revolution]].

