# Notes on "In Praise of the Flâneur"

&#x2013; [In Praise of the Flâneur](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) 

Morozov said the internet has been changed that flanerie is dead.  This article says its still happening, under the surface of the strict urban geography laid out by the silos.

> In the ensuing decades, however, the idea of flânerie as a desirable lifetsyle has fallen out of favor, due to some arcane combination of increasing productivity—hello, fruits of the Industrial Revolution!—and the modern horror at the thought of doing absolutely nothing. (See: Michael Jordan’s “retirements.”) But as we grow inexorably busier—due in large part to the influence of technology—might flânerie be due for a revival?

Cyberflânerie.

> he waxes nostalgic about the early days of the Web, comparing the evolution of the Internet to Baron Haussmann’s violent reconfiguration of Paris. “Transcending its original playful identity,” Morozov writes, “[the Internet is] no longer a place for strolling—it’s a place for getting things done.”

<!--quoteend-->

> corporations like Facebook divide the Web into increasingly well-defined, dedicated avenues, and, on the surface, there does appear to be a lack of diversity, idiosyncrasy, or whatever essence it is that drives flâneurs to flânerie.

<!--quoteend-->

> Morozov mourns the death of the old Internet communities, but he misses the essential point: new arenas, new arcades have replaced them, and they’re no less valid than the old. 

Maybe, but it's more effort to maintain them.

