# holarchy

> This form of organization is known as a holarchy, where each element—from cells on up—is a coherent entity in its own right, while also an integral component of something larger. In a holarchy, the health of the system as a whole requires the flourishing of each part. Each living system is interdependent on the vitality of all the other systems.
> 
> &#x2013; [[What Does An Ecological Civilization Look Like?]]

Any different from [[Heterarchy]]?

