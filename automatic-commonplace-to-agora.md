# Automatic commonplace to Agora

To allow me just to commit to commonplace repo, and let the publish to Agora format and Agora repo happen automatically on my server.


## setup

-   install emacs-nox on server (128Mb :/)
-   mkdir commonplace-to-agora
-   git clone https://gitlab.com/ngm/commonplace
-   git clone https://gitlab.com/ngm/commonplace-agora
-   try make agora to see what happens
    -   error in process filter: Could not create connection to orgmode.org:443
    -   huh.  just use a more up to date server, don't waste time on this.
-   git clone https://gitlab.com/ngm/ox-agora ~/.emacs.d/private/
-   git clone https://gitlab.com/ngm/commonplace-lib ~/.emacs.d/private/
-   make agora
    -   paths are hardcoded&#x2026;
    -   fix that up
-   make agora<sub>remote</sub>
-   for some reason it's outputting &amp; as &amp;amp; now.  is that because of change in org version used?  I think it's more correct to do so in markdown anyway, so fine.
-   add ssh key


## cron job

```nil
cd /mnt/HC_Volume_21098789/commonplace-to-agora/commonplace
git pull
make agora_remote
cd /mnt/HC_Volume_21098789/commonplace-to-agora/commonplace-agora
git add . && git commit -m "export" && git push
```

runs every hour.


## tweaks

on pages where I have a table of contents, org gives each heading a completely random anchor id each time on export.  these get recommited as updates so new up as recently edited nodes in agora each time, which is not what I want.

problem is here: https://emacs.stackexchange.com/questions/35339/random-href-ids-in-org-9-exported-html

