# Ecosocialism

[[I am an ecosocialist]].


## What is ecosocialism?

A combo of [[socialism]] with [[green politics]], [[ecology]] and [[alter-globalisation]].
A baseline of fulfillment of human needs while staying within [[planetary boundaries]]. Ecosocialism sees [[social and environmental issues as interconnected and inseparable]]. Ecosocialism is [[anti-capitalist]], seeing capitalism as the root cause of both social inequity and the overshoot of planetary boundaries.

> Loosely defined, ecosocialism describes a classless socioeconomic system in which humans live in balance with nature.
> 
> &#x2013; [[Jackson Rising Redux]]

<!--quoteend-->

> an ideology merging aspects of socialism with that of green politics, ecology and alter-globalization or anti-globalization.
> 
> &#x2013; [Eco-socialism - Wikipedia](https://en.wikipedia.org/wiki/Eco-socialism) 

-   [[Capitalism is the root cause of social inequity]].
-   [[Capitalism is the root cause of the overshoot of planetary boundaries]].


## What is the political economy of ecosocialism?

> market-based solutions to ecological crises (ecological economics, environmental economics, green economy) are rejected as technical tweaks that do not confront capitalism's structural failures.
> 
> &#x2013; [Eco-socialism - Wikipedia](https://en.wikipedia.org/wiki/Eco-socialism) 

What is its [[political economy]] then?

> Eco-socialists advocate dismantling capitalism, focusing on common ownership of the means of production by [[freely associated producers]], and [[restoring the commons]].
> 
> &#x2013; [Eco-socialism - Wikipedia](https://en.wikipedia.org/wiki/Eco-socialism) 

[[Commons]].

What's the **specifically** eco bit?

> Exchange value would be subordinated to use value by organiszing production primarily to meet social needs and the requirements of enviornmental protection and ecological regeneration.
> 
> &#x2013; [[Jackson Rising Redux]]

<!--quoteend-->

> To build an ecologically rational society along these lines requires the collective ownership of the means of production, democratic planning to enable society to define the goals of production and invemestment, and a new technological production structure that meshes with society's plans and stays within the Earth's ecological carrying capacity.
> 
> &#x2013; [[Jackson Rising Redux]]


## Various flavours of ecosocialism

There are various [[types of ecosocialism]].

> The discursive terrain of ecosocialist thinking is vast, and there is no single way to define ecosocialism. However, we can identify three broad principles that would likely be shared across these varieties: 1) the priority of use-value over exchange-value; 2) collective ownership and planning to shape and constrain markets; and 3) “contraction and convergence” in consumption levels between the global north and south
> 
> &#x2013; [[Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]

<!--quoteend-->

> But at core these movements share a commitment to struggling for a socioecological transition beyond capitalism by democratizing the means of production, subjecting markets to more ecologically rational planning, and subordinating private profit to social use-value and ecocentric production.
> 
> &#x2013; [[Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]


## Why not just eco?

> One might argue that the climate situation is so serious, we should prioritise it over everything; if we don’t get this right there’s no room for any human society, equitable or otherwise.  But then, I would say, the changeover to sustainability is an enormous undertaking, requiring the energies of society as a whole: hence we must therefore address the disempowerment of the vast majority.  This is why I proceed from a ‘red-green’ perspective, which is socially radical as well as environmentally conscious.
> 
> &#x2013; [[How systems theory can help us reflect on the world]]


## Governance

-   participatory planning
-   democratic control of resources
-   collective ownership and management of the means of production
-   decentralisation and regional self-sufficiency


## History

> [[William Morris]], the English novelist, poet and designer, is largely credited with developing key principles of what was later called eco-socialism
> 
> &#x2013; [Eco-socialism - Wikipedia](https://en.wikipedia.org/wiki/Eco-socialism) 

<!--quoteend-->

> The "pre-revolutionary environmental movement", encouraged by the revolutionary scientist Aleksandr Bogdanov and the Proletkul't organisation, made efforts to "integrate production with natural laws and limits" in the first decade of Soviet rule, before Joseph Stalin attacked ecologists and the science of ecology and the Soviet Union fell into the pseudo-science of the state biologist Trofim Lysenko, who "set about to rearrange the Russian map" in ignorance of environmental limits
> 
> &#x2013; [Eco-socialism - Wikipedia](https://en.wikipedia.org/wiki/Eco-socialism) 


## Future

> The eco-socialist future is female
> 
> [[Half-Earth Socialism]]

