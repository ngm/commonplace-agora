# Passive repetition

Reading or listening to things without really engaging with it. Can lead to an [[illusion of knowing]].

As opposed to [[active recall]] / [[active repetition]].

[[Learning science]].

