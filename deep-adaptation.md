# deep adaptation

> For example, Jem Bendell’s 2018 “Deep Adaptation Agenda” takes the inevitability of societal collapse to be a matter not of physical infrastructure and energy sources but of human values and psychology.
> 
> &#x2013; [[Revolution or Ruin]]

<!--quoteend-->

> Scranton and Bendell write in terms of a civilizational us, a “we” of shared values, metaphysics, and investment in the privileges of the carbon economy. There’s no class struggle, no inequality of responsibility for or capacity to respond to the fires, droughts, floods, and storms of a rapidly changing planet. Politics disappears, replaced by the individual’s psychological capacity to acknowledge the worst and respond ethically, that is, reflectively.
> 
> &#x2013; [[Revolution or Ruin]]

