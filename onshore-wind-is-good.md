# Onshore wind is good

[[onshore wind]] is good.


## Because

-   It is cheap
-   It is quick to set up
-   It is clean

> Once an onshore windfarm has secured a contract to supply energy, it can be fully operational within about two years, compared with four or five for offshore projects, according to the trade body RenewableUK.
> 
> Electricity from onshore wind is also about 20% cheaper than offshore, which is in turn cheaper than nuclear or gas.
> 
> &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy) 


## And

-   [[People like wind power]]

