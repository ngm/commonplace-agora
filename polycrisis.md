# polycrisis

> a tangled knot of crises spanning global systems.
> 
> &#x2013; ['Polycrisis' may be a buzzword, but it could help us tackle the world's woes](https://theconversation.com/polycrisis-may-be-a-buzzword-but-it-could-help-us-tackle-the-worlds-woes-195280)

