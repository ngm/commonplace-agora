# The Limits to Growth

[[Donella Meadows]] et al

> Although one of its authors, Donella Meadows, was to later to become an important systems thinker, the original document was in some respects weak on understanding systemic features, notably the bad climate feedbacks, as well as the benign feedbacks which counteract exponential growth (for example, the rate of population growth tends to fall with rising development).
> 
> &#x2013; [[For an anti-colonial, anti-racist environmentalism]]

<!--quoteend-->

> Even so the original Limits had great merits.  It explicitly calls for halting [[accumulation]] (thus correctly hinting that [[capitalism]] is to blame), and makes a wonderful point, quoting ecologial economist [[Herman Daly]], that “The problem of relative shares can no longer be avoided by appeals to growth”.  In other words, in a finite world, we’ll have share! 
> 
> &#x2013; [[For an anti-colonial, anti-racist environmentalism]]

<!--quoteend-->

> The Limits to Growth, despite its many strengths and good intentions, was basically oblivious to this context.  That’s why the South was understandably suspicious: the Limits discourse seemed to say, OK let’s freeze things where they are, with the North developed and the rest of the world in perpetual servitude.
> 
> &#x2013; [[For an anti-colonial, anti-racist environmentalism]]

