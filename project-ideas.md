# Project ideas



## Project Idea: personal microtasking

-   using e.g. pybossa to define little tasks that you need to do
-   in my case, categorising todos or whatever, little stuff like that
-   kind of a way of learning pybossa
-   examples: tagging my notes on doubleloop.net
-   examples: tagging my todo items in org-mode
-   in a way, it's kind of a digital personal assistant, bringing things to your attention that need doing


## Project Idea: Featurepedia


### For each feature, we can recommend software that might fulfil that feature

-   going for the unix philosophy approach
-   could even go wild and try and have a system that automatically suggest heath robinson setup based on your feature selection


### My indieware idea could even use feature models/selections     :featurepedia:

-   for a way for users to select the things they actually care about


### In a similar way to how RMS built alternatives to Unix software, same for modern web software. Also coincidentally following principle of Unix philosophy     :featurepedia:


### Indiewarify mendeley     :featurepedia:

Good be a good case study of the process. Repeat of org mode. But based on the principles I outline for orgmode.


### Indieware could tie into bdd side of things, like a distributed attempt to document festures of popular systems then used as a framework to create alternatives     :featurepedia:


### Jugaad soft wiki


#### A decentralised one?


### Text is ultimate backwards and forwards compatible format (with some structure)


### Principles over concrete apps


## Project Idea:Ideas bank thing, share ideas, see if others will implement


### How to guarantee work on open source projects,  i mean guarantee dedicated hours for agile sprints, maybe ora.network could help somehow

-   perhaps cabbage tree method could help with this also

