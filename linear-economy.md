# linear economy

> A circular economy contrasts with a one-way, linear economy that depletes natural resources, transforms them into waste, and pollutes residual natural capital through resource extraction, production, and waste disposal practices
> 
> &#x2013; [[Degrowth, green energy, social equity, and circular economy]]

