# postmodernism

Bit of an overloaded and slippery term.  Seems to be used in lots of different ways.  In some uses it seems like a decent riposte to modernity and rationalism.  Othertimes it's [[The cultural logic of late capitalism]]. Another conception of it seems to be that its about irony and a lack of sincerity.

Maybe it's all of these, depending on context (it would probably quite like that).

> A series of critiques by a school of French philosophers coalesced into a movement known as postmodernism, which attacked the notion that objective truths could be applied universally under the rubric of such capitalized abstractions as Truth, Science, Reason, and Man. Included in this attack was the tradition of “cultural essentialism,” by which Northrop and those who followed him had sought to ascribe a particular set of universal characteristics to the Orient, the West, or, for that matter, any racial or cultural stereotype.
> 
> &#x2013; [[The Patterning Instinct]]

<!--quoteend-->

> In contrast to the modernist view of the world, which had emerged with the Scientific Revolution, the postmodernists proposed that reality is constructed by the mind and can never therefore be described objectively. Each culture, they argued, develops its own version of reality that arises from its specific physical and environmental context. 
> 
> &#x2013; [[The Patterning Instinct]]

<!--quoteend-->

> The postmodernists accused Westerners who had attempted to do so of engaging in a form of [[cultural imperialism]], seeking to appropriate what seemed valuable in other cultures for their own use while ignoring its historical context.
> 
> &#x2013; [[The Patterning Instinct]]

<!--quoteend-->

> A more useful investigation, according to the postmodernist critique, would be to recognize the multiplicity of discourses created by various cultures and, rather than try to distill some essential meaning from them, trace how certain social and political groups used these discourses to maintain or enhance their own power relative to others.
> 
> &#x2013; [[The Patterning Instinct]]

![[2020-06-07_18-23-54_diamond-dust-shoes.jpg]]

> rejection of the "universal validity" of binary oppositions, stable identity, hierarchy, and categorization.
> 
> &#x2013; [Postmodernism - Wikipedia](https://en.wikipedia.org/wiki/Postmodernism#Manifestations) 

<!--quoteend-->

> [[Jane Jacobs]]' 1961 book The Death and Life of Great American Cities was a sustained critique of urban planning as it had developed within Modernism and marked a transition from modernity to postmodernity in thinking about urban planning
> 
> &#x2013; [Postmodernism - Wikipedia](https://en.wikipedia.org/wiki/Postmodernism#Manifestations) 

