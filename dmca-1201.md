# DMCA 1201

[[DMCA]].

> Section 1201 of the DMCA deals with [[DRM]]. It says that “trafficking” in a tool or even information that helps someone bypass an “access control” for a copyrighted work is a felony that can be punishable by up to five years in prison and up to $500,000 in fines. 
> 
> &#x2013; [When DRM Comes For Your Wheelchair | Electronic Frontier Foundation](https://www.eff.org/deeplinks/2022/06/when-drm-comes-your-wheelchair)

<!--quoteend-->

> Most importantly, DMCA 1201 doesn’t limit itself to banning bypassing DRM in order to infringe copyright (for example, in order to make thousands of copies of a DVD and sell them on the black market). That has allowed companies to use copyright law to criminalize businesses that have nothing to do with copyright. A company that designs a product that has some DRM that prevents repair or maintenance or improvement can use Section 1201 to attack anyone who engages in those activities, because removing the DRM is itself against the law. To be clear, the  DMCA’s ban on bypassing DRM is unconstitutional, and gets in the way of many activities beyond repair. 
> 
> &#x2013; [When DRM Comes For Your Wheelchair | Electronic Frontier Foundation](https://www.eff.org/deeplinks/2022/06/when-drm-comes-your-wheelchair)

