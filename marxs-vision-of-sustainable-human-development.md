# Marx's Vision of Sustainable Human Development

URL
: https://monthlyreview.org/2005/10/01/marxs-vision-of-sustainable-human-development/

> In developed capitalist countries, debates over the economics of socialism have mostly concentrated on questions of information, incentives, and efficiency in resource allocation. This focus on “socialist calculation” reflects the mainly academic context of these discussions. By contrast, for anti-capitalist movements and post-revolutionary regimes on the capitalist periphery, socialism as a form of human development has been a prime concern

<!--quoteend-->

> Marx’s vision has been deemed ecologically unsustainable and undesirable due to its purported treatment of natural conditions as effectively limitless, and its supposed embrace, both practically and ethically, of technological optimism and human domination over nature

<!--quoteend-->

> argues that for Marx, the “materialistic determinist, economic growth is crucial in order to provide the overwhelming material abundance that is the objective condition for the emergence of the new socialist man. Environmental limits on growth would contradict ‘historical necessity’…

<!--quoteend-->

> Robyn Eckersley, is that “Marx fully endorsed the ‘civilizing’ and technical accomplishments of the capitalist forces of production and thoroughly absorbed the Victorian faith in scientific and technological progress as the means by which humans could outsmart and conquer nature.

<!--quoteend-->

> Environmental philosopher Val Routley describes Marx’s vision of communism as an anti-ecological “automated paradise” of energy-intensive and “environmentally damaging” production and consumption, one which “appears to derive from [Marx’s] nature-domination assumption.”3

<!--quoteend-->

> There is a conventional wisdom that Marx and Engels, eschewing all “speculation about…socialist utopias,” thought very little about the system to follow capitalism, and that their entire body of writing on this subject is represented by “the Critique of the Gotha Program, a few pages long, and not much else.”5

<!--quoteend-->

> In reality, post-capitalist economic and political relationships are a recurring thematic in all the major, and many of the minor, works of the founders of Marxism, and despite the scattered nature of these discussions, one can easily glean from them a coherent vision based on a clear set of organizing principles. The

