# Rhizomatica

URL
: https://www.rhizomatica.org/

> Through efforts around the world, we use innovative applications of technology to facilitate community organization and personal and collective autonomy. Our approach combines [[regulatory activism]] and reform, the development of decentralized telecommunications and energy infrastructure, direct community involvement and participation, and critical engagement with new technologies

-   [[decentralised telecommunications infrastructure]]
-   [[decentralised energy infrastructure]]

