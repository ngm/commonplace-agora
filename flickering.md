# flickering

In a [[complex system]], an aberrant behaviour that you potentially see before a collapse or a phase transition. Basically hinting that something big is about to happen.

