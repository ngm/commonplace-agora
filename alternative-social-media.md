# Alternative social media

[[Alternative media]]. [[social media]].

> Perhaps, rather than producing entirely new alternative platforms, the task for radical developers is to identify suites of existing platforms that are most useful to selforganisation and to then find ways of integrating these in ways that allow for coordination (for example, by being able to send and receive WhatsApp messages from a platform like Loomio).
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> More expansive than [[Crabgrass]], [[Lorea]] networks allowed for customisable profile pages and dashboards, wikis, collaborative writing pads, blogs, task managers, status updates and private messaging, affiliations with and between groups and a federal structure for groups and networks.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> any alternative digital platform ought to have a direct link between whatever decisionmaking functions it houses and the facetoface decisionmaking procedures of the organisation using it.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> An alternative platform would need to ensure that filtering for noise and overload was, at the very least, open to democratic oversight, if not the product of direct participatory engagement, for instance through popular content floating to the surface, as it were, through a voting procedure.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> When we explore alternative social media platforms that are designed specifically for selforganisation, we need to remember that these ought to be seen as aids for offline organising, and as such, as infrastructures that support facetoface discussion and action rather than as an entirely separate sphere that radical politics
> 
> &#x2013; [[Anarchist Cybernetics]] 


## What might it look like?

-   https://twitter.com/normative/status/1454074111450308609


## Examples

> Some of the earliest online social media emerged through [[Indymedia]]’s coverage of anti-capitalist protests.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> [[Twitter]] has its roots in technology for coordinating street protests
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> In the area of the Internet, a socialist strategy can try to resist the commodification of the Internet and the exploitation of users by trying to claim the common and participatory character of the Internet with the help of protests, legal measures, alternative projects based on the ideas of free access/ content/software and creative commons, wage campaigns, unionization of social media prosumers, boycotts, hacktivism, the creation of public service- and commons-based social media, etc.
> 
> &#x2013; [[Social Media: A Critical Introduction]]

<!--quoteend-->

> our reasoning is that everything is social. Political activities are social. Economic activities are social. Artistic activities are social. The way we work and coordinate activities&#x2026; is social.
> 
> &#x2013; [Not Just 'Yet Another Microblog'](http://bonfirenetworks.org/posts/not_just_yet_another_microblog/)

