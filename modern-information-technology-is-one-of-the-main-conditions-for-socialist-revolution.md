# Modern information technology is one of the main conditions for socialist revolution

Modern [[information technology]] is one of the main conditions for [[socialist revolution]].

> the main conditions for socialist revolution are the ability to produce abundantly, **modern information technology**, socialist vision, and proletarian consciousness.
> 
> &#x2013; [[Information Technology and Socialist Construction]]

<!--quoteend-->

> For a successful socialist revolution to occur, the aim and power of modern information technology must shift from providing support to the capitalist mode of production to providing support for the socialist mode of production.
> 
> &#x2013; [[Information Technology and Socialist Construction]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

Don't know.  It's quite bold, maybe technocratic, but maybe also true.

