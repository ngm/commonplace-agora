# Biogas

> An anaerobic digestion process can create methane and carbon dioxide when microorganisms are allowed to break down organic matter such as food remains, plant residues or animal waste in an air-tight container. The renewable biogas that results can be compressed and used to power small machines, heat stoves or lamps and power electricity, or upgraded to natural gas standards, after which it is known as biomethane. Residues of biogas production can also be used as a low-grade organic fertiliser. 
> 
> &#x2013; Down to Earth newsletter

