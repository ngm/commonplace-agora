# Olga Burmatova

> In the 1980s, Olga Burmatova, a cybernetician in [[Novosibirsk]], sought to reconcile economic planning with protection of [[Lake Baikal]], more than 1,400 kilometres to the east. By following Burmatova, we discover an experiment in planning that combined decentralized control within a total plan – something that Beer achieved with his [[viable system model]] – with explicit protection of the environment. 
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Burmatova was concerned about the environmental threat posed by the Baikal-Amur Mainline, a new railway that would cut through the permafrost to link Siberia’s rich natural resources to Moscow. Even worse was the Northern River Reversal Project, another mega-project that would have diverted Siberian rivers to irrigate fields in Kazakhstan – much like the disastrous ‘Virgin Lands’ campaign killed the Aral Sea. Burmatova realized that incorporating environmental data into economic planning was necessary to protect this unique ecosystem.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Burmatova worked within the planning framework of ‘territorial production complexes’, which were as close as the Soviets got to something like the viable system model.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> territorial production complexes were based on the insight that ‘local resources, production, goods, and population had to be considered as existing simultaneously within various networks, on many scales, embedded in several economies, and therefore in a host of complex relationships to each other
> 
> [[Half-Earth Socialism]]

