# News from Nowhere

A
: [[book]]

Author
: [[William Morris]]

> Morris’s News from Nowhere is a utopian-communist novel that is set in a future communist society in the year 2102.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Morris wrote his novel as a rebuttal to Edward Bellamy’s [[Looking Backward]], allowing the two to debate the future of socialism as social engineers
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Like [[Neurath]], Morris recognized that utopia was hardly an impractical romance to be discarded by tough-minded revolutionaries, but rather a necessary part of the practical work to realize socialism
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> The importance of rest is why Morris so vehemently opposed Bellamy’s hyper-mechanized and consumerist ‘cockney paradise
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> to his writing News from Nowhere” (Thompson 2011, 542). [[Fredric Jameson]] (2005, 144) argues that “Bellamy’s industrial state (modeled on the army) is refuted by the anarchistic ‘withering away’ of the state in Morris, while the account of labor in Looking Backward (something like Marx’s ‘realm of necessity’ opposed to the ‘realm of freedom’ of non-work and leisure time) is challenged by Morris’s notion of a non-alienated labor which has become a form of aesthetic production”. News from Nowhere is “not merely a contrasting utopia to Bellamy, it is a campaign against the whole mechanization of existence”
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> of the means of production, no exchange, no wage-labour, and no money. It is a classless society. There is no dull compulsion of the market forcing humans into wage-labour and class relations. They work voluntarily with the “freedom for every man to do what he can do best”
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Use-value shapes the economy, and humans produce for society’s real needs, not for the accumulation of profit. The social environment and goods are beautifully designed so that beauty is a general principle of society. There is no poverty and all individuals are generally happy and beautifully dressed. There is gender equality. The overcoming of alienation and exploitation has enabled longevity and has drastically reduced crime, which has allowed the abolishment of prisons. A participatory democracy (“the whole people is our parliament” [107]) has replaced government, parliament, and the state. In News from Nowhere, there are separate houses for individuals and families, indicating that Morris considers privacy and individuality in a communist society to be important. 2.2. Technology and Production in Communism In News from Nowhere, there is no compulsory labour. Everyone is very industrious, conducts self-chosen work that results in beautiful results,
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> In News from Nowhere, there is a certain idealisation of hard and mundane physical labour such as road-mending, street-paving, house-building, haymaking, or housework.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Although it is not ruled out that machines are used for replacing unpleasant human labour, overall the society that Morris describes in News from Nowhere is a pre-industrial, agricultural socialism, where humans enjoy hard physical labour and beauty is the abundant result of handicraft.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> The political system that Morris depicts in News from Nowhere is a communicative [[participatory democracy]] (in Chapter IV): Citizens meet in neighbourhood assemblies, where they discuss matters of concern for the community. Suggestions for certain changes, such as building a new bridge or town hall, are made. If such proposals are supported by others, then pro- and counter-arguments are collected and published. In a later meeting, a “vote by show of hands” (119) is taken. If the minority is of significant size, then discussions and further votes continue until consensus is reached or the minority is happy to accept the decision.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

