# Free, Fair and Alive Highlights

***

Bollier, David; Helfrich, Silke;  - Free, Fair, and Alive

***

Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 00:20]</span></span>:
The commons is a social form that enables people to enjoy freedom without repressing others, enact fairness without bureaucratic control, foster togetherness without compulsion, and assert sovereignty without nationalism.

Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 00:21]</span></span>:
A commons &#x2026; gives community life a clear focus. It depends on democracy in its truest form. It destroys inequality. It provides an incentive to protect the living world. It creates, in sum, a politics of belonging.”

Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 00:24]</span></span>:
The commons is a robust class of self-organized social practices for meeting needs in fair, inclusive ways.

Page 14 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:28]</span></span>:
What we really need today is creative experimentation and the courage to initiate new patterns of action. We need to learn how to identify patterns of cultural life that can bring about change, notwithstanding the immense power of capital.

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:33]</span></span>:
going beyond” the nation-state doesn’t mean “without the nation-state.” It means that we must seriously alter state power by introducing new operational logics and institutional players.

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:35]</span></span>:
this book is not just to illuminate new patterns of thought and feeling, but to offer a guide to action. But

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:50]</span></span>:
Of course, complications arise and multiply as young children grow up. They learn that some people are not trustworthy and that others don’t reciprocate acts of kindness. Children learn to internalize social norms and ethical expectations, especially from societal institutions. As they mature, children associate schooling with economic success, learn to package personal reputation into a marketable brand, and find satisfaction in buying and selling.

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:52]</span></span>:
The elemental human impulse that we are born with — to help others, to improve existing practices — ripens into a stable social form with countless variations: a commons.

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:56]</span></span>:
The commons is not simply about “sharing,” as it happens in countless areas of life. It is about sharing and bringing into being durable social systems for producing shareable things and activities.

Page 25 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 05:59]</span></span>:
Hardin’s “tragedy” thesis ought to be renamed “The Tragedy of Unmanaged, Laissez-Faire, Commons-Pool Resources with Easy Access for Non-Communicating, Self-Interested Individuals.”

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 06:00]</span></span>:
Commons are living social systems through which people address their shared problems in self-organized ways.

Page 31 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 14:18]</span></span>:
Tools should not attempt to control humans by prescribing narrow ways of doing things. Software should not be burdened with encryption and barriers to repair. Convivial tools are designed to unleash personal creativity and autonomy.13

Page 35 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-14 Fri 14:39]</span></span>:
Each commons depends on social processes, the sharing of knowledge, and physical resources. Each shares challenges in bringing together the social, the political (governance), and the economic (provisioning) into an integrated whole.

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 07:32]</span></span>:
ponder the general belief system developed during the Renaissance and expanded in the eighteenth and nineteenth centuries by the capitalist societies that arose from it. We moderns live within a grand narrative about individual freedom, property, and the state developed by philosophers such as RenéDescartes, Thomas Hobbes, and John Locke. The OntoStory that we tell ourselves sees individuals as the primary agents of a world filled with inert objects that have fixed, essential qualities. (Most notably, we have a habit of referring to “nature” and “humanity,” as if each were an entity separate from the other.) This Western, secular narrative claims that we humans are born with boundless freedom in a prepolitical “state of nature.” But our imagined ancestors (who exactly? when? where?) were allegedly worried about protecting our individual property and liberty, and so they supposedly came together (despite their radical individualism) to forge a “social contract” with each other.9 As the story goes, everyone authorized the establishment of a state to become the guarantor of everyone’s individual liberty and property.10

Page 45 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 07:38]</span></span>:
The “prevailing life-motif” of modern capitalism and the liberal state, writes Greek social critic Andreas Karitzis: promotes the idea that a good life is essentially an individual achievement. Society and nature are just backdrops, a wallpaper for our egos, the contingent context in which our solitary selves will evolve pursuing individual goals. The individual owes nothing to no one, lacks a sense of respect for the previous generations or responsibility to future ones — and indifference is the proper attitude regarding present social problems and conditions.14

Page 46 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 07:40]</span></span>:
to see that everything is interdependent, and that our individual wellbeing depends upon collective well-being. Our polity must be “attuned to the relational dimension of life,” as Arturo Escobar puts it.15

Page 49 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 07:52]</span></span>:
fundamentally different onto-logics about human relations with oceans.”20 The state approaches the ocean as a nonliving resource. As such it can be divided up into quantified, bounded units and exploited with an abstract market logic. Oil extraction is perfectly logical to the New Zealand state, whose legal system is constructed to privilege such activity. By contrast, the Maori see the ocean as a living being that has intense, intergenerational bonds with the Maori people. The ocean is imbued with mana, ancestral power, that must be honored with spiritual rituals and customary practices. (If this sounds irrational to you, consider this: such a worldview has worked remarkably well to protect both the oceans and human societies.)

Page 50 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 07:55]</span></span>:
The world of commoning represents a profound challenge to capitalism because it is based on a very different ontology.

Page 53 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 08:08]</span></span>:
In a relational ontology, the idea is that relations between entities are more fundamental than the entities themselves. Let this idea sink in. It means that living organisms develop and thrive through their interactions with each other.

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 09:28]</span></span>:
Complexity science has important things to say about the commons, too, because it sees the world as a dynamic, evolving set of living, integrated systems.38

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 09:30]</span></span>:
While individual organisms may have important degrees of agency, they can only be understood in the context of their myriad relationships and constraints by larger structures. The
● While individual organisms may have important degrees of agency, they can only be understood in the context of their myriad relationships and constraints by larger structures. The sounds a bit like vsm?

Page 56 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-15 Sat 09:34]</span></span>:
Complexity science offers a more coherent way of explaining how functional design can emerge without a designer. Design happens as adaptive agents (such as commoners) interact with each other. The self-organization of agents — what we call “peer organization” in a commons — gives rise incrementally to complex organizational systems.40 There is no master blueprint or top-down, expert-driven knowledge behind the process. It emerges from agents responding to their own local, bounded circumstances.41
● Complexity science offers a more coherent way of explaining how functional design can emerge without a designer. Design happens as adaptive agents (such as commoners) interact with each other. The self-organization of agents — what we call “peer organization” in a commons — gives rise incrementally to complex organizational systems.40 There is no master blueprint or top-down, expert-driven knowledge behind the process. It emerges from agents responding to their own local, bounded circumstances.41 very easy like!

Page 57 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 07:52]</span></span>:
Kate Raworth, in her brilliant book Doughnut Economics, has proposed a real-world economic framework that recognizes a new ontology — that people are social and relational (not rational and individualistic); that the world is dynamically complex (not mechanical

Page 59 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:00]</span></span>:
The new structure can help us envision different sorts of community, social practices, and economic institutions — and above all, a new culture that honors cooperation and sharing.

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:05]</span></span>:
We came to realize that if we aspire to social and political transformation but try to do so using the language of market economics, state power, and political

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:05]</span></span>:
liberalism, we will fail.

Page 64 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:08]</span></span>:
When a conception permeates a thought collective strongly enough, so that it penetrates as far as everyday life and idiom and has become a viewpoint in the literal sense of the word, any contradiction appears unthinkable and unimaginable.”
● sounds ike culturl hegemony When a conception permeates a thought collective strongly enough, so that it penetrates as far as everyday life and idiom and has become a viewpoint in the literal sense of the word, any contradiction appears unthinkable and unimaginable.”

Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:27]</span></span>:
Participants in a Federated Wiki can decide to interconnect their wiki pages into a neutral, shared neighborhood of content, through which a consensus of viewpoints becomes visible.

Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:28]</span></span>:
This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration.9 But in fact the effect is quite the opposite: giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis

Page 256 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:29]</span></span>:
results in a richer, more robust commons.

Page 256 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:39]</span></span>:
from conventional ownership? Imagine a huge continent of diverse residences.10 Some have only a few rooms. Others exist within skyscrapers and provide space for hundreds of rental homes with multiple rooms. Some are clustered together as neighborhoods. Others are smaller and more isolated from other flats and houses. These residences are dispersed all over the continent, but there are irregular corridors, pathways, and roads that can potentially interconnect them all.

Page 257 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun 08:41]</span></span>:
draw from their sites — or anyone else’s! In other words, the Fedwiki software creates protected individual spaces for content generation while facilitating diverse permutations of collaborative authoring on a massive scale. It opens countless paths for individuals to organize their own knowledge while easily sharing it with others, and, what’s more, enabling a commons of knowledge to arise, without the intervention of an editor.

Page 68 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-18 Tue 00:10]</span></span>:
reverent guests of nature”

Page 70 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-18 Tue 20:00]</span></span>:
Citizen, also called “a national,” identifies a person in relation to the nation-state and implies that this is a person’s primary political role. The term “citizen” is often used to imply that noncitizens are somehow less than equal peers or perhaps even “illegal.”22 A more universal term is Commoner.

Page 70 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-18 Tue 23:29]</span></span>:
Development is a term of political economy used by the US and European nations to prod “undeveloped” countries to embrace global commerce, resource extractivism, and consumerism along with improvements in infrastructure, education, and healthcare.

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-18 Tue 23:32]</span></span>:
The alternative to “innovation” is not this binary opposite, however, but creative adaptation to ever-changing needs in ways that are shared and convivial. Leadership is a term that

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-18 Tue 23:35]</span></span>:
The term nonprofit is somewhat misleading because it suggests that there is a way to participate in a capitalist economy in socially minded ways without making a profit; it is more accurate to say that nonprofits are reinvesting profits into social purposes.

Page 73 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-19 Wed 00:09]</span></span>:
Today the term is usually invoked in a positive way to imply that citizen participation (in hearings, decision-making, or participatory budgeting programs) fulfills democratic ideals and confers popular legitimacy on the outcomes. This is precisely the deficiency of the term “participation,” however: it is often confined within a predetermined, top-down set of policy options and implementation strategies. The public does not really initiate and show sovereign political agency in a fuller sense. It merely “participates” in public debates and processes on terms that politicians, regulators, and other state officials have already found acceptable, giving the ultimate decisions a veneer of legitimacy. By contrast, Commoning is a more robust, independent act of political agency. (To) scale: “How do we

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-19 Wed 02:33]</span></span>:
The “scarcity” of oil, land, and water may seem self-evident, but, in fact, the term does not reflect any inherent property of a resource. Oil, land, or water are merely finite.

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-19 Wed 06:46]</span></span>:
Commoners have learned to bypass this problem by choosing to ShaRe Knowledge geneRouSly — and then developing other ways of paying for any associated costs (e.g., Pool, CaP &amp; divide uP, in-kind support, selective market sales, cross-subsidies, etc.).

Page 84 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-20 Thu 04:01]</span></span>:
Commoner is an identity and social role that people acquire as they practice Commoning. It is associated with actual deeds, not an assigned legal or social title. Anyone is potentially a commoner.

Page 85 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-20 Thu 04:09]</span></span>:
Insights from complexity sciences help move beyond a Newtonian worldview of cause and effect to one that is holistic, nonlinear, and interactive.

Page 86 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-20 Thu 04:37]</span></span>:
A tool is convivial if people have access to the design and knowledge needed to create it; if it allows creative adaptation to one’s own circumstances; and if it is appropriate in the specific local context. (Are suitable materials and skills available? Is it compatible with the local landscape and culture?)

Page 90 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-21 Fri 00:01]</span></span>:
In a heterarchy, different types of rules and organizational structures are combined.

Page 91 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-21 Fri 00:01]</span></span>:
They may include, for example, top-down hierarchies and bottom-up participation (both of which are vertical), and peer-to-peer dynamics (which are horizontal). In a heterarchy, people can achieve socially mindful autonomy by combining multiple types of governance in the same system.

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 00:29]</span></span>:
Relational Ontology holds that the relations between entities are more fundamental than the entities themselves.

Page 102 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 00:38]</span></span>:
commoning is primarily about creating and maintaining relationships — among people in small and big communities and networks, between humans and the nonhuman world, and between us and past and future generations.

Page 103 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 00:45]</span></span>:
Despite vivid differences among commons focused on natural resources, digital systems, and social mutuality, they all share structural and social similarities.

Page 103 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 07:39]</span></span>:
Our idea was to make visible that which connects commons experiences in medieval times and today, in digital and analogue spheres, in cities and the countryside, in communities dedicated to water and to software code.

Page 104 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 07:42]</span></span>:
In this respect, patterns resemble DNA, a set of instructions that are underspecified so that they can be adapted to local circumstances. “Does the DNA contain a full description of the organism to which it will give rise?” asks Christopher Alexander in his

Page 105 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 07:48]</span></span>:
Principles tend to make universal claims. This is problematic because it is virtually impossible to find the same institutional structures, cultural beliefs, and social norms in different places and contexts. By contrast, universal patterns of human interaction already exist.

Page 106 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 07:54]</span></span>:
commoners are engaged in “world-making in a pluriverse”

Page 106 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 08:04]</span></span>:
the core purpose of commoning: the creation of peer-governed, context-specific

Page 107 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-22 Sat 08:04]</span></span>:
systems for free, fair, and sustainable lives.

Page 109 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-23 Sun 23:58]</span></span>:
— a fractal federation of countless unique and yet connected worlds

Page 110 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-23 Sun 23:59]</span></span>:
Commoning is what common people decide for themselves in their specific circumstances if they want to get along with each other and produce as much wealth for everyone as possible.

Page 116 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-26 Wed 23:33]</span></span>:
But if the goal is to contribute to a resilient commons, gift-givers need to make sure that their contributions are voluntary and commonly agreed upon, and not induced by pressure or sanctions from the outside.

Page 116 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-26 Wed 23:34]</span></span>:
While an accounting may work in larger and less personal contexts, a sharp focus on precise contributions and entitlements can undermine what makes a commons special: it’s a space where money doesn’t rule everything.

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-26 Wed 23:37]</span></span>:
A self-confident, gracious commons is thus one that is content with social equals enjoying a roughly balanced (but not absolutely equal) exchange over time.

Page 122 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 07:27]</span></span>:
Like any institution, a commons must have rules and norms that apply to everyone. But what also matters a great deal is how those rules and norms are upheld.

Page 123 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 07:27]</span></span>:
The idea is not to secure consensus through threats of punishment, but to prevent misaligned relationships in the first place.

Page 123 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 07:29]</span></span>:
What always matters, though, is striving to maintain collective morale while being unflinchingly honest.

Page 124 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 23:41]</span></span>:
just as there is no commons without commoning, so there is no commoning without peer governance.

Page 124 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 23:42]</span></span>:
commoning is not just a state of enhanced awareness and being, like Zen practice or mindfulness. It is an enactment of peer provisioning and peer governance.

Page 125 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-27 Thu 23:44]</span></span>:
own tastes and points of view, while inscribing such wikis within larger federations (known as “neighborhoods”) to allow the easy sharing of wiki pages

Page 129 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:15]</span></span>:
The supposed dualism between the collective and individual is largely overcome by sharing authority among everyone directly affected by decisions.

Page 129 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:16]</span></span>:
As we thought about how coordination works in a commons, we hesitated to use the term “governance” because it is so closely associated with the idea of collective interests overriding individual freedom.

Page 130 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:35]</span></span>:
peer governance rather than just governance. It points to an ongoing process of dialogue, coordination, and self-organization.

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:41]</span></span>:
Our analysis of Peer Governance therefore moves beyond Ostrom’s landmark design principles in several ways. First, we look at all sorts of contemporary commons — social, digital, and urban, among others — not just at natural resource-based commons. We also attempt to go beyond resource management and allocation as primarily economic matters, and instead emphasize commoning as a social system. Any assessment of governance in commons must deal squarely with the systemic threats posed by markets and state power, so we look to Peer Governance as a form of moral and political sovereignty that works in counterpoint to the market/state.

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:42]</span></span>:
by Elinor Ostrom is helpful,3 but ultimately not enough. The principles do not provide sufficient guidance for people to respond flexibly to feedback

Page 135 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-28 Fri 07:58]</span></span>:
Unitierra — Universidad de la Tierra en Oaxaca — in Oaxaca, Mexico. It is a “de-institutionalized university” founded by commoners for commoners that rejects formal roles and hierarchy.8

Page 140 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-29 Sat 00:56]</span></span>:
So instead of conceiving of commons as closed systems of common property managed by a “club,” it is more productive to see them as social organisms who, thanks to their semi-permeable membranes, can interact with larger forces of life — communities, ecosystems, other commons.

Page 142 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-29 Sat 08:24]</span></span>:
we cannot depend upon structures to do the work of culture. Transparency is not just about legal arrangements and

Page 143 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-29 Sat 08:35]</span></span>:
Early projects to develop shared bodies of code — such as the UNIX operating system that eventually became Linux

Page 143 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-29 Sat 08:35]</span></span>:
The fact that geeks are wont to speak of ‘the UNIX philosophy’ means that UNIX is not just an operating system but a way of organizing the complex relations of life and work through technical means …”20

Page 145 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-29 Sat 08:49]</span></span>:
It is fundamental that commoners have a meaningful say in developing the rules by which they shall be governed.

Page 149 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-02 Wed 03:24]</span></span>:
Consent — as opposed to agreement — is defined by the absence of reasonable objections.

Page 148 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-02 Wed 03:25]</span></span>:
To give my consent to a proposal does not necessarily mean that the proposal is my first choice.

Page 149 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-02 Wed 03:28]</span></span>:
Small teams using Sociocracy can be nested within a larger “parent circle” that has broad oversight and decision making responsibilities.

Page 153 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-02 Wed 03:43]</span></span>:
Heterarchy brings together top-down and bottom-up (both hierarchical), and peer-to-peer dynamics. One can think of it as reconciling distributed networks and hierarchies.36

Page 159 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:36]</span></span>:
The essence of relationalized property is the blending of individual and collective interests into a new paradigm.

Page 160 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:40]</span></span>:
You might wonder why cooperatives are often cited as examples of the commons when in fact many of them seem to produce for and sustain themselves entirely from the market.

Page 160 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:41]</span></span>:
The reason is that many cooperatives have not found cultural means to Keep Commons &amp; Commerce Distinct.

Page 161 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:43]</span></span>:
Enclosure is the opposite of commoning in that it separates what commoning otherwise connects — people and land, you and me, present and future generations, technical infrastructures and their governance, conservation areas and the people who have stewarded them for generations.

Page 161 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:43]</span></span>:
The process of enclosure is generally driven by investors and corporations, often in collusion with the nation-state, to commodify shared land, water, forests, genes, creative works, and much else.

Page 161 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:43]</span></span>:
Enclosure is thus a profound act of dispossession and cultural disruption that forces people into both market dependency and market frames of thought.

Page 162 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:50]</span></span>:
Commoning provided what we would now call a basic income — access to resources that ensure one’s basic survival.

Page 162 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:52]</span></span>:
how can I organize my life in such a way that I become less dependent on money? How do I decommodify daily life? Similar questions should be asked at the level of a project, initiative, infrastructure, or platform.

Page 163 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:57]</span></span>:
French city council offered to pay EnCommuns, a commons-based network of database programmers, to do some work, it suddenly introduced external performance pressures, trumping the project’s original goals and its self-determined work rhythms.

Page 163 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 07:58]</span></span>:
In response, EnCommuns created a semi-permeable membrane, an ingenious way to preserve the spirit of its commoning while engaging with commercial actors.

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 08:14]</span></span>:
Commoners can improve their long-term independence by withdrawing as much as possible from dealings with the market/state system.

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-05 Sat 08:16]</span></span>:
Money-lite commoning is how hackers helped neutralize Microsoft’s proprietary abuses of its monopolies over Windows and Office in the early 2000s (see pp. 128 and 167–168). They developed GNU/Linux, Open Office, and scores of other high-quality open source programs as practical, low-cost or no-cost alternatives to the standard programs offered by the market giants. Through shareable, peer-produced programs, people can create digital commons of their own and escape

Page 172 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:42]</span></span>:
Users have firsthand knowledge that is invaluable in design and production, even if economists regard the separation of production and consumption as an inexorable fact of modern life.

Page 173 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:43]</span></span>:
Trained to see the dismemberment of complex production processes as efficient and natural, and its segregation from consumption as a core premise of “the economy,” economists tend to overlook a more elegant, practical approach to provisioning — commoning.

Page 173 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:43]</span></span>:
Commoning is at heart an act of social self-organization and constant learning whose central purpose is to help people meet needs by producing things or services together.

Page 173 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:52]</span></span>:
In conventional economic terms, a commons helps reduce the need for administration, lawyering, “human resource” management, and marketing by instead relying on a community of trust and individual commitment. Who needs advertising when the goal is to meet needs, not promote consumption?

Page 174 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:58]</span></span>:
With the rise of capitalism, caring, childrearing, and education have been seen as activities external to the working of the economy.

Page 174 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:58]</span></span>:
commons does not externalize care, and so is more able to take account of a person’s fuller life, not just their need to earn money.

Page 174 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 07:59]</span></span>:
The things produced are not designed to be sold, or sold in high volumes at the highest prices, or to pander to our consumer fantasies and then fall apart through planned obsolescence so that the cycle can be repeated.

Page 175 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:13]</span></span>:
The goal of Provisioning through Commons is not maximum efficiency, profit, or higher Gross Domestic Product. It aims simply to meet needs and provide a stable, fair, satisfying, and ecologically minded way of life.

Page 175 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:14]</span></span>:
the commons economy invites people to reorient their perspectives and aspire to a different set of outcomes than those of market capitalism — the satisfaction of real, not contrived, needs.

Page 175 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:14]</span></span>:
greater freedom, fairness, and sustainability for all. The biggest shift that the commons economy brings is a move from the economy as an autonomous, globalized supermachine to an economy that nurtures life on its own terms, at appropriate scales.

Page 177 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:19]</span></span>:
According to a Study by Cedifa (Center for Digital Fabrication), a Fab Lab (fabrication laboratory) can be opened within seven days and a basic investment of only US$5,000 if it relies on commons-oriented approaches, including the use of open source software.5

Page 178 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:25]</span></span>:
The open workshops and repair cafes are places for community building, collective thinking, and learning. They Creatively Adapt &amp; Renew (see below) countless objects that are considered waste, giving them a second life cycle.

Page 179 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:30]</span></span>:
Care occurs when people bring their full humanity to a task instead of having impersonal, money-mediated relationships with market resources.

Page 182 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 08:41]</span></span>:
In the capitalist economy, companies are said to shoulder the risks of creating and marketing a product, even though their research and development budget is often subsidized by taxpayers and even though they often displace risks and expenses on to consumers, the environment, and future generations. This

Page 181 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-06 Sun 16:13]</span></span>:
In this respect, the commons challenges the very heart of market economics by asserting different standards of valuation.

Page 188 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-08 Tue 00:07]</span></span>:
This is the essence of a market: a transaction-based encounter (exchanging money for a commodity) rather than an enduring social relationship.

Page 192 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 00:02]</span></span>:
tragedy of the market.

Page 193 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 00:05]</span></span>:
When you’ve got 400 quarts of greens and gumbo soup for the winter, nobody can push you around or tell you what to say or do,”

Page 197 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 07:30]</span></span>:
Price sovereignty is about counting provisioning costs accurately and transparently in the first place, based on actual need and context.

Page 198 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 07:49]</span></span>:
Conviviality, which described a vision of a world in which a community of users develop and maintain their own tools.

Page 200 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 23:40]</span></span>:
To the degree that he masters his tools, he can invest the world with his meaning; to the degree that he is mastered by his tools, the shape of the tool determines his own self-image.”

Page 200 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-11 Fri 23:41]</span></span>:
In our times, open source tools and technologies are convivial tools with great potential for Provisioning through Commons because users can determine how they will be used.

Page 202 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 00:25]</span></span>:
requires some infrastructure to enable participants to reap the advantages of both distributed self-determination and larger-scale cooperation.

Page 202 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 00:27]</span></span>:
The goal in each case is not to consolidate management through a central body, but to enter into a process of Emulate &amp; Then Federate using digital networks.

Page 202 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 00:27]</span></span>:
Power and creativity can then be dispersed locally or regionally while retaining important elements of large-scale coordination.

Page 204 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:22]</span></span>:
jugaad — the Indian practice of slapdash innovation from whatever is at hand.29

Page 204 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:22]</span></span>:
People share “light” knowledge and design via peer-to-peer learning and the internet, but build “heavy” physical things such as machinery, cars, housing, furniture, and electronics locally. In the peer production community there is a saying, “If it’s light, share it globally — if it’s heavy, produce it locally.”

Page 205 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:25]</span></span>:
the Open Building Institute, an offshoot of Open Source Ecology, builds low-cost, modular houses that are ecological and energy-efficient using techniques that are open

Page 210 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:28]</span></span>:
how can the Commonsverse grow larger and transform the political economy and culture? How can we achieve changes in state power, law, and policy based on a commons approach? These questions are the focus of Part III.

Page 211 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:30]</span></span>:
Beating the bounds, you may recall, is the practice used by many English villages of walking the perimeter of their land to identify any fences or hedges that had encroached upon their shared wealth.

Page 211 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:30]</span></span>:
In our times, beating the bounds may initially involve direct action resistance and civil disobedience against enclosures, and attempts to “de-enclose” them.

Page 212 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:37]</span></span>:
collaborative ventures such as shared infrastructure, finance, and political advocacy. Beyond the activities of Emulate &amp; Federate, it is important for commons projects and networks to pursue strategies of intercommoning. This is the process of active collaboration and mutual support to assist and inspire individual projects, make sense of unfolding events, and develop proactive strategies.

Page 214 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:51]</span></span>:
We are not trying to smash capitalism in a traditional revolutionary sense, although of course any advance of the commons diminishes its power and represents an incremental triumph. The

Page 215 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-12 Sat 06:52]</span></span>:
The most natural opening for cooperation between state power and commoners is the local level. In smaller-scale political contexts, government tends to be less driven by ideology or party politics than by sheer practicality — what works?

Page 219 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-13 Sun 23:10]</span></span>:
Faced with existing frameworks of property law, commoners who wish to legalize their Peer Governance may have little choice but to attempt to creatively modify the law or turn to political pressure, social organizing, or civil disobedience.1

Page 222 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-14 Mon 00:01]</span></span>:
encourages the treatment of human beings as commodified labor and the internalization of such norms as people learn to sell themselves on the labor market.

Page 223 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-14 Mon 00:06]</span></span>:
At the very moment we recognize our condition to be that of human beings in relatedness, it becomes clear that the default premise of property law — that everyone is absolutely autonomous and separate from each other and the Earth — is highly problematic if not silly.

Page 224 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-14 Mon 23:26]</span></span>:
In other words, the legal relationship (property ownership) profoundly shapes and determines social relationships.

Page 225 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-14 Mon 23:32]</span></span>:
Property rights must be defended. Legality is thus used to eclipse the Vernacular Law of the commons — the informal, unofficial norms, practices, and customs used by peer communities to manage their affairs.16

Page 229 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-15 Tue 23:53]</span></span>:
Relational Ways of Having help us realize that there are many ways to steward and deepen the multiple relationships affected by property. This conceptualization helps us see how individual use rights and collective property regimes are not mutually exclusive. Indeed, they need each other!

Page 229 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-15 Tue 23:53]</span></span>:
Individual use rights are a key condition for a flourishing collective property regime.

Page 231 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 07:46]</span></span>:
In traditional commons, use rights were not enforced through formal, written law, but through social memory and lively traditions.

Page 233 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 07:52]</span></span>:
Precisely because custom defies Lockean notions of property (fixed, based on individual rights, market-oriented), it honors a richer set of relationships among people and the environment.

Page 233 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 07:54]</span></span>:
The Common Law”: “The first requirement of a sound body of law is that it should correspond with the actual feelings and demands of the community, whether right or wrong.”

Page 234 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 07:55]</span></span>:
The annual beating of the bounds — the village procession around the perimeter of a commons described in the Introduction to Part III — was both a festive event with cakes and beer and a serious assertion of commoners’ entitlements.

Page 235 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 07:58]</span></span>:
The basic idea of inalienability is a prohibition of market exchange. What is inalienable cannot be bought and sold on the market. An inalienable thing cannot be inherited, mortgaged, seized, indemnified, or taxed.

Page 236 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-16 Wed 08:02]</span></span>:
Tradeability, enacted through property rights, is.

Page 246 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-17 Thu 07:02]</span></span>:
shape the kind of human beings we become. They also shape the kinds of society we allow ourselves to build. We are encouraged to work hard, become owners, and become rich. This may not be especially meaningful or enlivening, but it is certainly a sensible and functional approach in a world that celebrates the competitive, acquisitive, self-serving mindset. To be

Page 246 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-17 Thu 07:04]</span></span>:
Relationalize Property, as we call it. We use this term to point to sociolegal systems that elevate concrete use rights and social relationships over absolute property ownership.

Page 250 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 04:08]</span></span>:
Property ownership is turned into a social vehicle for meaning-making and building a commons culture.

Page 250 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 04:09]</span></span>:
In one stroke, the work requirement helps solve the problems of altruistic burnout, high labor costs, and weak community culture.

Page 254 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 04:24]</span></span>:
Rather than simply “balance” the benefits from individual versus collective property rights, the goal is to integrate the two by design so that both are more organically aligned, minimizing potential conflict.

Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 04:26]</span></span>:
the universe is “not a collection of objects, but a communion of subjects.”

Page 255 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 05:30]</span></span>:
Federated Wiki is a stunning server program that allows individual works of creativity to seamlessly combine into collectively available content without

Page 261 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 05:45]</span></span>:
tries. Mietshäuser Syndikat is a federation of housing commons that has been operating since 1987.

Page 262 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 05:46]</span></span>:
Over the past thirty years, Mietshäuser Syndikat has removed more than 130 rental buildings from the real estate market. This has made permanently affordable, collectively owned housing available to more than 2,900 ordinary people.

Page 264 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:23]</span></span>:
If all housing projects are peer governed and legally independent (and should remain so), but all participating projects share a larger purpose (to decommodify real estate), how can this latter goal be assured when there might be disagreements among the dozens of projects? Mietshäuser Syndikat provides an effective vehicle for
● grand strstgy If all housing projects are peer governed and legally independent (and should remain so), but all participating projects share a larger purpose (to decommodify real estate), how can this latter goal be assured when there might be disagreements among the dozens of projects? Mietshäuser Syndikat provides an effective vehicle for

Page 266 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:30]</span></span>:
Hacking Property to Help Build

Page 267 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:32]</span></span>:
It is especially important to commoners, who frequently find that they must attempt a hack on legal forms in order to decriminalize or advance commoning.

Page 269 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:35]</span></span>:
In the early 2000s, Lessig famously wrote, “code is law.” By that, he meant that the design of software code so profoundly shapes what users can do on their computers and online that it has the effect of law.

Page 269 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:36]</span></span>:
They call the result the “sharing economy” and “gig economy,” but in fact it is simply a new species of markets designed for microrentals, piecemeal labor, data mining, and consumerism.

Page 271 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:54]</span></span>:
Open Source Seeds

Page 271 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 19:57]</span></span>:
globally more than sixty percent of commercial seeds is now controlled by four agrochemical/seed companies33

Page 273 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 20:13]</span></span>:
Inspired by the success of the GPL and free and open source software over the past thirty years, some leading players in the seed movement decided to align behind the banner of “open source seed.”

Page 275 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 22:44]</span></span>:
Commoning Mushrooms: The Iriaiken Philosophy Once a community escapes the conventional ideas of property (or never embraces

Page 275 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 22:44]</span></span>:
Iriaiken usually refers to the collective ownership of nonarable areas such as mountains, forests, marshes, bamboo groves, riverbeds, and offshore fisheries.

Page 282 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 22:56]</span></span>:
Governance. Building Stronger Commons Through Relationalized Property The idea of

Page 286 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:02]</span></span>:
Blending individual use rights with collective possession helps to generate more stable, fair outcomes, which itself is an affordance for minimizing conflict.

Page 288 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:07]</span></span>:
known as elegance. Re-Introducing Meaning Making into Modern Law

Page 289 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:10]</span></span>:
Or as French novelist Anatole France put it more tartly: “The law, in its majestic equality, forbids the rich and poor alike to sleep under bridges, to beg in the streets, and to steal loaves of bread.”

Page 292 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:15]</span></span>:
Power and Commoning W e have seen how ingenious sociolegal forms and ancient legal doctrines have the potential of neutralizing conventional property claims, thereby limiting the

Page 292 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:33]</span></span>:
How might states support commons-based governance and provisioning? Could state law establish a more muscular

Page 294 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:36]</span></span>:
There is a very simple explanation: the market/state system is structurally biased against protecting shared wealth as commons. It tends to interfere with private investment and market returns.5 The grab for

Page 294 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:38]</span></span>:
Market players need the political legitimacy and predictable rule of law that states provide, and states, in turn, need the tax revenues, geopolitical influence, and infrastructures that flow from an economy committed to relentless growth.

Page 295 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:40]</span></span>:
If we wish to take the commons idea seriously, then, we have to fundamentally rethink our ideas about how state power might be used strategically to advance the interests of commoners.

Page 295 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:40]</span></span>:
The State” and “The People”

Page 296 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:42]</span></span>:
To be a commoner is to understand social reality in a different way. One sees that the I as an individual is always connected to others, and in a pre-political sense.

Page 297 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-20 Fri 23:46]</span></span>:
In this sense, commoning serves as something of a staging area to co-create transnational, post-state identities that can get beyond the abuses of patriotism and nationalism.

Page 300 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 07:48]</span></span>:
There is a structural mismatch between state power and living systems.

Page 300 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 07:50]</span></span>:
Some Working Notes on State Power

Page 300 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:01]</span></span>:
But what if the modern state in its intimate alliance with capital represents an evolutionary dead end?

Page 300 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:03]</span></span>:
Has this centralized, hierarchical system of power become too brittle and inefficient to govern the riotous complexity of local realities and human diversity, notwithstanding its adaptations to the realities of networked society and hybrid governance institutions? Has it become too alienated from the more-than-human world and oblivious to its imperatives?

Page 301 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:04]</span></span>:
While the Leviathan18 purports to guarantee many rights and liberties for its citizens, the rise of the market/state is at least as important as a force for controlling people. The Great Wall of China was built as much to keep Chinese citizens in as it was to keep “barbarian” invaders out. In our

Page 302 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:07]</span></span>:
Over time, states prod their citizens to internalize values and goals, to bring about a unified, regimented order from what they regard as the chaos and barbarism of the pre-state.

Page 302 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:07]</span></span>:
Thus, while modern, liberal states may manage to enlarge the scope of freedom that ordinary people have, such gains come at a price: special privileges for the political authority of the state and the market power of capital.

Page 303 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:09]</span></span>:
Anything which requires difference, contingency and essential unpredictability is not going to be a skill of the state.21

Page 303 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 08:59]</span></span>:
This is one reason why cities and towns are likely to play an outsized role in transforming state power. Their smaller scale offers more opportunities for change.

Page 304 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:00]</span></span>:
An obscure Occupy Wall Street protest in Manhattan’s Zuccotti Park in 2011 sparked scores of Occupy protests around the world and made wealth inequality an urgent public issue.

Page 304 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:01]</span></span>:
When activists gain radical democratic experience — simple things such as the “procedural forms of 15-M, assemblies with direct democracy, facilitation methods, working groups, shows of hands, or consensus-oriented decision-making,”29 — they are able to contribute at the municipal level.

Page 305 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:02]</span></span>:
Revolution The

Page 305 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:02]</span></span>:
primary goal of commoning is not to seize state power through revolution or elections.

Page 305 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:02]</span></span>:
Since 2015, the Greek political coalition led by Syriza discovered that its stunning electoral victory, nominally giving it control of a sovereign state, was not enough. The Greek state was in fact still subordinated to the power of international capital and the geopolitical interests of other states.

Page 305 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:02]</span></span>:
elections. It is to develop stable independent spaces that have relative freedom to establish their own systems

Page 305 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:03]</span></span>:
The rise of Indigenous politician Evo Morales to the presidency of Bolivia revealed a similar lesson: even smart, well-intentioned electoral movements have trouble transcending the deep imperatives of state power because the state remains tightly yoked to an international system of capitalist finance and resource extraction.

Page 306 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:04]</span></span>:
Reform and revolution turn out to be children of traditional Marxism: They can imagine how to seize political power and redesign the state, but not how to enact a free society.”

Page 306 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:05]</span></span>:
radical reformism
● nkt sure about this. radical reformism

Page 307 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:07]</span></span>:
Can commoning as we’ve described it potentially enact a more humane social order at scale, notwithstanding state power?

Page 307 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:07]</span></span>:
with learning how to be the revolution rather than only doing it — often known as “prefigurative politics.”
● in general this is quite an anarchist ook with learning how to be the revolution rather than only doing it — often known as “prefigurative politics.”

Page 307 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:08]</span></span>:
That means trying things out. Living with them for a while. Reflecting on them. Making corrections and adjustments.
● and this is quite cybernetic That means trying things out. Living with them for a while. Reflecting on them. Making corrections and adjustments.

Page 308 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:11]</span></span>:
The Power of Commoning

Page 309 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 09:27]</span></span>:
This is a darkly hilarious conceit because in this time of climate breakdown, ecosystem collapse, desertification, etc., even state power cannot defy planetary systems that are becoming political agents in their own right, as Bruno Latour has noted.39

Page 310 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 13:59]</span></span>:
At the same time, it is imperative to engage with state power through elections and traditional advocacy, if only because that field of action can change the conditions for widening spaces of commonality. It is too consequential to be ignored.

Page 310 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 13:59]</span></span>:
So commoners need a two-track mindset in dealing with state power: a primary focus on building the new — keeping the conceptual insights above in mind — while also attempting to neutralize the old.

Page 310 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 14:00]</span></span>:
Revamping State Power to Support Commoning

Page 311 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 14:02]</span></span>:
In instances where state institutions regard sharing as a crime — e.g., seed sharing, software collaborations, information sharing — commoning must be decriminalized.

Page 313 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 14:09]</span></span>:
providing space and support for timebanks, repair cafés, hackerspaces, and much more.

Page 313 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 17:49]</span></span>:
The cultural dimensions of commons-based initiatives such as agroecology, community land trusts, platform cooperatives, and cosmo-local production, are generally ignored or seen as too small and inconsequential to be taken seriously. Businesses, for their part, generally see them as threats to their market share and profits.

Page 315 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 17:53]</span></span>:
The state at all levels is providing infrastructures, technical advice, and funding that let people launch their own makerspaces, CSA farms, energy cooperatives, tool-sharing commons, repair cafés, and timebanking exchanges.47

Page 318 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:00]</span></span>:
Provide Infrastructures for

Page 316 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:01]</span></span>:
Establish Commons at the

Page 319 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:02]</span></span>:
recognizing open technical standards that limit proprietary control, while taking steps to ensure that the largest, wealthiest businesses do not simply use open standards to capture the innovation space.

Page 319 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:06]</span></span>:
Open technical standards for government procurement could be issued to make free and open source software a default infrastructure in government agencies, especially in schools. Instead of being familiar with proprietary software, students would graduate from school with extensive skills in working GNU/Linux and other open source software. Schools would not be degraded by becoming quasi-captive extensions of large software corporations’ marketing departments. This would have spinoff benefits for general education, higher education, municipal governments, and the general public.

Page 320 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:06]</span></span>:
word processing, database, and email software and APIs (application protocol interfaces, which are the technical linkages between software programs and operating systems), could foster greater open source innovation, avoiding the walled gardens owned by Apple and Google.

Page 320 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:06]</span></span>:
Cities might more easily develop platform cooperatives for housing, ride-hailing services, and information services to benefit local residents instead of Silicon Valley investors. Open design protocols for energy grids could replicate the success of the internet by using open standards to encourage bottom-up innovation by smaller, creative players and preventing proprietary lock-ins by larger companies.

Page 320 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:07]</span></span>:
For infrastructures used by society in general, it is critical that they be discrimination-free, so that no class of users can be arbitrarily excluded from access.53

Page 320 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:07]</span></span>:
This is what the city of Linz, Austria, has done with its Open Commons Linz initiative.55

Page 320 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:07]</span></span>:

1.  Create New Types of Finance for the Commons

Page 322 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 18:11]</span></span>:
Commons and Subsidiarity

Page 323 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 22:43]</span></span>:
What about Fundamental Rights Guaranteed by the State?

Page 323 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 22:44]</span></span>:
This virtually requires that commoners first organize themselves, then federate at the mesolevel (the spaces among individual commons) to support each other in building commons despite the looming presence and meddling of state power.

Page 324 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 22:46]</span></span>:
They imply an alien political and social order, one of isolated-I’s petitioning a remote, powerful Leviathan.

Page 326 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:31]</span></span>:
Take Commoning to Scale

Page 328 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:34]</span></span>:
Charters for Commoning

Page 330 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:40]</span></span>:
Community Chartering Network

Page 332 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:43]</span></span>:
Distributed Ledgers as a Platform for Commoning

Page 332 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:45]</span></span>:
Distributed ledgers are significant for the commons because they have the potential for providing a powerful software architecture to support commoning on open networks,

Page 334 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:48]</span></span>:
The general point of ledger systems is to use peer-to-peer networks to verify the authenticity of a unique digital object.

Page 334 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:48]</span></span>:
Why are distributed ledger technologies important for commoning? Because they potentially offer a way for commoners to wrest control of the “master switch” in digital technologies away from capital, and instead empower and protect collective action.

Page 334 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:49]</span></span>:
Unlike Bitcoin and Ethereum (the two most prominent digital ledger technologies), Holochain is far more energy efficient and flexible in the way that it authenticates digital objects on networks.

Page 335 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:50]</span></span>:
It is a lighter, more versatile set of software protocols than the blockchain.

Page 336 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:54]</span></span>:
In this sense, Holochain is a tool based on a differentiated relational ontology (each user can express value in different, particular ways when interacting with people) while blockchain as a tool ultimately reflects an undifferentiated ontology (each user must accept the prevailing standard of the system).

Page 337 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-21 Sat 23:55]</span></span>:
Arthur Brock, one of the cofounders of the MetaCurrency Project, which has developed Holochain, explains that the real purpose of a currency should be to make the flow of value visible, as in a “current-see.”

Page 337 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 00:01]</span></span>:
Conventional money, as designed and used, cannot express important types of value, which Brock and his associates regard as a profound problem of the modern age. Dollars, euros, and other state currencies don’t let us see the flows of value that matter most — ecological flows, the social relationships of gift economies, people’s contributions to commons.

Page 339 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:50]</span></span>:
at scale.” In other words, the Holochain protocols would function as a grammar for the system, or language for building apps that name flows of value within a community, such as social contributions, reputation, work performed, care work, even community sentiment.

Page 339 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:52]</span></span>:
Harris-Braun claims that Holo Fuel will not simply be a substitute form of money that will end up replicating capitalism, but will instead propagate a “different grammatics of value.” Just as a different grammatical structure in a human language helps us to articulate different ideas and realities, the Holochain grammar is intended as a tool to express social forms of value — flows

Page 340 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:53]</span></span>:
Instead of market-exchange being the dominant form of value, it is envisioned that Holochain-based apps will enable other forms of value to be expressed and circulated within networked communities — in other words, not just the money values represented by prices.

Page 340 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:54]</span></span>:
As a society, we have a pretty good understanding of objects and how to manipulate them, but we’re not as good with flows,” says MetaCurrency project founder Arthur Brock.16

Page 341 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:57]</span></span>:
What matters about distributed ledger technology is the new and different affordances that it enables. It’s an open question who will be first and most influential in leveraging those affordances.

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:58]</span></span>:
Holochain is an attempt to secure an important beachhead for commoners, even if it will also be used, inevitably, for less elevated purposes. The appeal of ledger technologies, whether Holochain or others still in the works, is their potential to create durable new affordances for commoning.

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:58]</span></span>:
Partnerships Given the state’s close alliance with the corporate sector, it should not be

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:59]</span></span>:
PPPs are typically vehicles for developing infrastructure for water supply or sewage management, or building roads, bridges, schools, hospitals, prisons, or public facilities such as swimming pools and playing fields.

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 06:59]</span></span>:
PPPs. These are often good-faith attempts to address pressing social problems through contract-based collaborations between businesses and government.

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 07:00]</span></span>:
PPPs are based on fundamentally incompatible objectives — the state’s obligation to protect the public good and private businesses’ desire to maximize profits. In practice, many public-private collaborations function less as partnerships than as disguised giveaways.

Page 342 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 07:00]</span></span>:
PPP can let a company acquire equity ownership of public infrastructure such as roads, bridges, and public facilities for a long period — fifteen, thirty, even ninety-nine years — and then manage them as a private market asset.

Page 343 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 07:02]</span></span>:
The state and the corporate sector both pretend that PPPs are a healthy, wholesome arrangement that benefits everyone and solves the lack of public funds. In truth, a great many PPPs amount to a marketization of the public sector that extracts more money from citizens, surrenders taxpayer assets to businesses, and neutralizes public accountability and control.

Page 344 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 07:05]</span></span>:
But volunteering is different from commoning. While both involve individuals choosing to participate, volunteers work under terms set by a sponsoring organization whereas commoners initiate and manage a project themselves, on their own terms.

Page 347 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 07:12]</span></span>:
A commons-public partnership (CPP) is an agreement of long-term cooperation between commoners and state institutions around specific functions.

Page 353 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-22 Sun 14:39]</span></span>:
Commoning at Scale

