# Anarchist Cybernetics - raw highlights

Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 07:59]</span></span>:
2011 was supposed to be the year when everything changed. Protests erupted across the planet, largely as a response to the worsening economic situation that followed the 2007– 08 financial crash.

Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 08:04]</span></span>:
All of the major movements that rose up in 2011 – Occupy, the 15M movement and the Arab Spring

Page 14 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 08:05]</span></span>:
Occupy, in particular the original Occupy camp in the belly of the beast that is New Y ork’s financial district, Occupy Wall Street, has become synonymous with this manifestation of radical participatory democratic decision making.

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 22:41]</span></span>:
2011, it is important to situate them within a

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 22:44]</span></span>:
various ways, responses to the financial crisis of 2008, the political crisis of (the absence of) democracy and the connections between these – and many of their methods – the occupation of public space, experimenting with forms of direct,

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 22:44]</span></span>:
a number of shared tactics, such as the use of social media as an organising platform, of protest camps, and of popular assemblies.

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 22:57]</span></span>:
different ways, similar organisational forms were witnessed across the movement of the squares, with participatory democracy a defining feature of these mobilisations.

Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 22:59]</span></span>:
For Occupy in particular, the evolution that involved moving strategic decision making from the general assembly – marred by timewasting and the involvement of those not active in the camps, including tourists attracted by the media – to the spokescouncil marked a clear attempt to develop organisational processes in response to emerging challenges.

Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:00]</span></span>:
aim of this book is to illustrate in detail how a perspective taken from cybernetics and brought to anarchist organising can help show how these lessons from experience can be constructively heeded to inform future political organising.

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:09]</span></span>:
Nonetheless, anarchism has always been centrally concerned with selforganisation, with how groups of people can collectively govern themselves and make decisions about how they want to exist as a community.

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:10]</span></span>:
Anarchism emerged, as Ruth Kinna and Alex Prichard argue (2019), in the 19th century, from a critique of slavery and private property, and how both were made possible by the state.

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:25]</span></span>:
inception concerned how any kind of system, be that an engineering or electronic system or a biological or social system, was organised; what the different parts of the system

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:25]</span></span>:
were, what function they played; and, importantly, how they operated together to make up the whole.

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:28]</span></span>:
while these two traditions are both centrally concerned with selforganisation, they approach it in quite different ways: cybernetics from an interest in effectiveness, even efficiency; anarchism from the standpoint of social and political freedom.

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:33]</span></span>:
cybernetics, the steering that systems undergo is not conducted by an outside agency but by the system itself and its constituent parts.

Page 21 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:35]</span></span>:
In the mid1960s, Ward wrote a short article called ‘Anarchism as a Theory of Organization’ (Ward,

Page 21 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:38]</span></span>:
In society there exist basic units (individuals, associations, communes, etc.) which have to possess autonomy, and which can cooperate and federate on a voluntary basis with the other units.

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-24 Thu 23:41]</span></span>:
his work was taken up in the context of workers’ cooperatives

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 05:55]</span></span>:
The freedom that parts of an organisation have to make tactical choices, it will be suggested, is limited in important ways by both overarching strategies and grand strategies, that respectively set out the goals and worldviews of the organisation.

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:09]</span></span>:
anarchism represents a theorisation of how society can be structured to enable liberty and solidarity, a society based on the principle of mutual aid, where the needs of all are met

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:09]</span></span>:
through cooperation and the sharing of resources.

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:13]</span></span>:
Direct action, as a method of anarchism, involves those who are exploited and dominated acting for themselves, to change their situations in ways that do not depend on the goodwill or charity of others.

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:15]</span></span>:
direct action in the anarchist tradition as action that ‘refers to practical prefigurative activity carried out by subjugated groups in order to lessen or vanquish their own oppression’

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:16]</span></span>:
prefiguration refers to the means or methods of political action mirroring its ends or goals and thus reflecting the values that underpin them.

Page 29 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:18]</span></span>:
positive force that could bring about an end to economic exploitation, the anarchists challenged this, arguing that the actions of the state, whether controlled by capitalists or by socialist revolutionaries, would inevitably create forms of authoritarian domination.

Page 29 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:21]</span></span>:
While those who followed Marx in the First International argued for the development of political parties who would, either through revolution or election, seize state power, the anarchists championed the building of organisations that were prefigurative, insofar as they aimed at creating the conditions of the future society in the present.

Page 30 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:24]</span></span>:
the ‘vanguard’ anarchism of platformist tendencies

Page 31 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-25 Fri 06:29]</span></span>:
Federalism, therefore, is seen by anarchists and others as a means of providing for decision making at scales beyond the immediate communities people live in, that maintains a level of autonomy for those communities.

Page 32 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 00:10]</span></span>:
For Proudhon, Bakunin and many others, federalism is a way of reconciling the respect for autonomy with the need for cohesive political organisation from the local to the global.

Page 32 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 00:16]</span></span>:
Alexander Galloway writes that in selforganisation, ‘each agent is endowed with the power of local decision according to the variables and functions within its own local scope’

Page 33 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 06:38]</span></span>:
participatory democracy rather than representative forms of government that involve elections. In doing so I take inspiration from the Zapatistas and the Kurdish liberation movement, in Chiapas and Rojava respectively, where, Gordon recognises, the inheritance of democracy is not that of settler colonialism.

Page 34 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 06:41]</span></span>:
to describe the way multiple different lines of oppression ‘intersect’ with one another to produce specific configurations of oppression acting on different individual people and groups. For Crenshaw and other early theorists of intersectionality, the aim was to develop an understanding of how women of colour faced different conditions to white women. This was a response to and evolution of the dominant strands of feminist thought, at the time, which tended to view women as a single category.

Page 34 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 06:41]</span></span>:
Intersectionality illustrated that as well as oppression along gender lines, women of colour also faced oppression along racial lines, meaning that a homogeneous feminism would not be able to respond to the problems they faced.

Page 35 <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat 06:41]</span></span>:
workingclass women would face different forms of oppression from middle or upperclass women – and later to a wider range of intersecting axes of oppression, through what has come to be called the ‘matrix of domination’

Page 35 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 05:53]</span></span>:
intersectional anarchists reject the liberal tendency to focus on oppression as an issue of prejudice or bias, and instead focus on the historical structures that shape people’s lives and that serve to produce and reinforce oppressive conditions in ways that go far beyond the oppressive behaviour of particular individuals

Page 36 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 05:55]</span></span>:
Seeing some workingclass people join the ranks of the employers by being promoted to sit on corporate boards does nothing, so the argument goes, to address the structural exploitation that is capitalism.

Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:02]</span></span>:
State domination, he writes, operates at three levels: (1) the macro or systematic level of ‘a system that imposes its dynamic on a population and a territory over which it claims to exercise “sovereignty”

Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:03]</span></span>:
on a population and a territory over which it claims to exercise “sovereignty” ’; (2)

Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:03]</span></span>:
departments, and the courts […] but also state companies (often monopolistic) and public services, such as public schools’

Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:03]</span></span>:
(3) the micro or individual level of a dominant class of people that ‘includes a variety of members with different statuses, degrees of power, and material, psychological, and symbolic privileges’

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:07]</span></span>:
the alterglobalisation movement and the uprisings of 2011, the use of the internet, latterly alongside social media, has often been picked out

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:07]</span></span>:
commented of the Arab Spring revolutions that they were ‘planned on Facebook, organized on T witter and broadcast to the world via Y ouTube’

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:08]</span></span>:
[b] y significantly enhancing the speed, flexibility and global reach of information flows, allowing for communication at a distance in real time, digital networks provide the technological infrastructure for the emergence of contemporary networkbased social movement forms.’

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:12]</span></span>:
networked communication technologies have allowed movements to shift from relatively centralized, hierarchical organizational structures to highly decentralized, loosely affiliated contingent networks that link a wide variety of groups, actors, and

Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:14]</span></span>:
The riots that took place in London in August 2011 articulate very well the potential that social media holds for radical forms of selforganisation.

Page 39 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:19]</span></span>:
Manuel Castells, one of the authors most closely associated with network theory, defines networks in terms of flows of information and resources between interconnected nodes.

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:20]</span></span>:
Slime mould, a cluster of singlecelled organisms, functions much in the same way a network does.

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:24]</span></span>:
Individual cells receive and transmit information about their environment through the network, allowing it to function almost as a single body. If one cell in the network recognises a good source of food, this information will be passed from cell to cell and the network as a whole will extend itself towards that source of food. In search of food, then, the slime mould network, despite lacking both a nervous system and a brain, operates as a single, unified whole.

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:28]</span></span>:
Interestingly, slime mould has been shown to effectively replicate human transport networks. In experiments where food sources have been placed at locations on a map and in quantities designed to represent the population density of towns and cities, slime mould networks extend across the map in ways that often mirror train or road networks

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:28]</span></span>:
Each bird in the flock follows a set of simple rules, reacting to the movements of the birds nearby it. Orderly flock patterns arise from these simple, local interactions. None of the birds has a sense of the overall flock pattern. The bird in front is not a leader in any meaningful sense – it just happens to end up
● bird flocks boids at least remind me of smart cntracts. it may be decentralised but its very dendent nheules you choose. Each bird in the flock follows a set of simple rules, reacting to the movements of the birds nearby it. Orderly flock patterns arise from these simple, local interactions. None of the birds has a sense of the overall flock pattern. The bird in front is not a leader in any meaningful sense – it just happens to end up

Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:31]</span></span>:
This concept – manytomany communication – is one that perhaps best sums up how social media and networked communications operate.

Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:36]</span></span>:
tomany communication, in contrast, involves the type of communication commonly found in decentralised or distributed networks, where multiple nodes can send messages to one another without a central point of convergence.

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:37]</span></span>:
Many-to-many communication refers to the communication that takes place in networks where everyone participating is able to send and receive information to and from everyone else in the network. The technological mediation of manytomany communication would include wikis, blogs, social media platforms, online chatrooms and, potentially, microblogging sites.

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:37]</span></span>:
Social media, in so far as it creates an environment in which decentralised and dispersed communication networks can function, is one of the key architectures that allow for manytomany communication; and to the extent that these networked organisation forms mirror the kind of organisation anarchism is concerned with, social media can be explored as providing potential sites for anarchist politics.

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:38]</span></span>:
The agorae and assemblies of Ancient Greece could similarly be understood as facilitating forms of manytomany communication, and doing so in ways that, albeit imperfectly, embody some of the democratic processes common to anarchist and radical politics

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:39]</span></span>:
Traditional marketplaces, sports stadiums, graffiti and community notice boards can all be seen as instances of manytomany communication

Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:40]</span></span>:
tomany communication, social media platforms – which have been described as ‘architectures of participation’ (O’Reilly, 2005) – will be the focus of this book,

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:48]</span></span>:
A central aspect, that we need to appreciate in understanding the roles of social media in the context of manytomany communication and networked organisation, is its participatory nature, with the ‘social’ component of the term providing reference to a participatory and collaborative account of sociality; social media is participatory and collaborative in terms of both its communicative function, and its production.

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:49]</span></span>:
Castells, Mason, Juris – have written of the benefits that social media and manytomany communication can provide for the kind of horizontal organising common in recent social movements.

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:53]</span></span>:
several other authors – including Terranova (2000), Jarrett (2008), Morozov (2011) and Fuchs (2014) – have emphasised some of the negative aspects of social media platforms, that mitigate against this networked, nonhierarchical organisation.

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:54]</span></span>:
highlighted as key areas of concern. This has led to calls for social media platforms to either be nationalised and run by the state or brought under some other form of democratic control, such as ownership by users. Further, concrete responses to these critiques by

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:55]</span></span>:
One of the aims of this book is to outline how social media platforms

Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:55]</span></span>:
how social media platforms can

Page 44 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:55]</span></span>:
support effective anarchist organisation.

Page 44 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:55]</span></span>:
NETWORKS OF COMMUNICATION 33 support effective anarchist organisation. As

Page 44 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:56]</span></span>:
organisation and communication. For the anarchist tradition in general, as
● vs control and communication organisation and communication. For the anarchist tradition in general, as

Page 44 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:57]</span></span>:
organisation primarily concerns efforts to enshrine the autonomy of both individuals and groups within structures, in ways that allow for coherent political decision making.

Page 44 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 06:58]</span></span>:
capitalism, as well as attempts to address intersecting oppressions such as patriarchy, white supremacy and colonialism. Communication practices, in relation to these anarchist approaches to organisation, can be seen as functional aspects of how participatory and democratic decision making is realised. Networked, manytomany forms of communication

Page 46 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:14]</span></span>:
At the heart of this conversation between anarchism and cybernetics is the idea of the Viable Systems Model as articulated by Stafford Beer,

Page 47 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:17]</span></span>:
It is also worth noting that cybernetics is less a single coherent field and more a collection of different strands of academic and scientific development, that at some points bear little more than a rough family resemblance.

Page 48 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:22]</span></span>:
systems. These ranged from simple and deterministic systems (the dynamics of which we can fairly easily understand and that are largely predictable, such as how billiard balls move around a table) to exceedingly complex and probabilistic systems (that are difficult to understand and potentially impossible to predict, such as the brain, the economy, or forms of social organisation). Beer described this latter type of system as ‘so complex and so probabilistic that it does not seem reasonable to imagine that it will ever be fully described’

Page 49 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:26]</span></span>:
Rather than a single controller trying to understand and respond to the complexity of the entire system, the different parts of the system each operate in relation to the smaller amount of complexity in their own area and are thus able to respond to complexity effectively and in doing so, together allow the wider system to achieve its goals.

Page 49 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:28]</span></span>:
Project Cybersyn involved designing and implementing a networked communications system that allowed the units of production (factories) to respond to information about the overall state of the system (the economy).

Page 49 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat 10:29]</span></span>:
like this kind of project on this scale; indeed, it is commonly identified as one of the predecessors of the internet.

Page 53 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:28]</span></span>:
For Beer, however, the VSM is not in fact intended as a blueprint in this sense. Rather, he describes it as a ‘diagnostic tool’ ([1981] 1994: 155), a conception that is echoed in cybernetician Roger Harnden’s framing of it as a ‘hermeneutic enabler’ (1989).

Page 53 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:29]</span></span>:
Rather than being a strict plan as to how a system or organisation must be designed, the VSM works as a map of sorts, one that is aimed at helping people navigate the organisations they are working with(in). It highlights

Page 54 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:31]</span></span>:
This is often called the Law of Requisite Variety or, after its founder Ross Ashby, Ashby’s Law; this states that for any system to remain stable and to operate effectively in its environment, the system itself must display the same level of variety and flexibility as that of the environment that it operates

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:36]</span></span>:
As Beer puts it, control means ‘capable of adapting smoothly to change’ (1974: 88). Any viable system will be able to both attenuate variety in its environment (reduce variety so that

Page 56 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:40]</span></span>:
While, as with control, it has a technical definition in cybernetics, selforganisation can also be understood in very simple terms as the ability of systems and organisations to regulate or control themselves without the need of a centralised or external controller.

Page 56 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:42]</span></span>:
entails control as domination; governance, on the other hand, can operate in the form of control as selforganisation. Towards the end of his career, Beer conceived of a process, which he called ‘syntegration’ that was aimed at allowing

Page 57 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 01:45]</span></span>:
Syntegration follows a series of steps that are aimed at moving the group from their initially disparate opinions on a topic to a shared and informed position

Page 58 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 02:01]</span></span>:
this secondorder cybernetics approach to understanding systems is important for the argument of this book.

Page 59 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 02:03]</span></span>:
The ontology of secondorder cybernetics – in other words, the way cybernetics understands what reality is – is described by Pickering as an ‘ontology of becoming’

Page 61 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 02:07]</span></span>:
physiology that ‘we find no boss in the brain, no oligarchic ganglion or glandular Big Brother’

Page 62 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 02:11]</span></span>:
if we use ‘control’ in the sense of ‘maintain a large number of critical variables within limits of tolerance’. […] The error of the governmentalist is to think that ‘incorporate some mechanism for control’ is always
● doughnut if we use ‘control’ in the sense of ‘maintain a large number of critical variables within limits of tolerance’. […] The error of the governmentalist is to think that ‘incorporate some mechanism for control’ is always

Page 64 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:08]</span></span>:
According to McEwan, the concept of anatomical hierarchy refers to what we normally understand by the term ‘hierarchy’: different levels in an organisation with a chain of command running between then and with lower levels subordinate to higher ones. This is hierarchy within the structure or anatomy of an organisation, hence McEwan calling it anatomical. Functional hierarchy, on the other hand, concerns a situation where ‘there are two or more levels of information structure operating in the system’

Page 65 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:11]</span></span>:
Each member must have the possibility, however small, of inverting the structure without leaving his [sic] niche to do so. I do not mean ‘the office boy can rise to be manager’. I mean, ‘in some unspecified conditions the office boy can take the managerial decisions’

Page 66 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:11]</span></span>:
While there are functions that are ordered hierarchically on top of one another, this does not mean that these functions must be assigned to hierarchically ordered parts of the organisation.

Page 66 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:15]</span></span>:
and the VSM and cybernetics more generally. An

Page 69 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:25]</span></span>:
System Five, where overarching goals and political strategy is developed, is something that ought to be rooted in everyone involved in the system.

Page 71 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 07:28]</span></span>:
Occupy Wall Street illustrates one way in which a functional hierarchy can be maintained in an organisation which aims to have a nonhierarchical (in terms of structural or anatomical hierarchy) structure.

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:29]</span></span>:
Control (Part I): Tactics, Strategy and Grand Strategy If Stafford Beer’s Viable Systems Model (VSM) is at the heart of how we might understand

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:30]</span></span>:
A sticking point in anarchist theory and practice, at least since the alterglobalisation movement’s prominence around the turn of the millennium, has been whether the concept of strategy can be applied to anarchism or whether anarchism is, or ought to be in principle, purely tactical.

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:31]</span></span>:
I want to argue here, is that the relationship between strategy and tactics can be framed and articulated in such a way as to be wholly consistent with the ideals of selforganisation and participatory democracy that animate anarchism.

Page 73 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:32]</span></span>:
System and metasystem in the VSM

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:40]</span></span>:
He explains that System Three is concerned with the ‘inside and now’ of the organisation and System Four with the ‘outside and then’ ([1979] 1994). This mirrors how strategy is defined by scholars of organisation theory (for example, Carter et al., 2008),

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:41]</span></span>:
strategy is concerned with both what is happening in the moment inside the organisation and how best to regulate it to achieve set goals as well as what is happening outside in the external environment and with respect to the possible futures of the organisation.

Page 74 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:41]</span></span>:
tactics as that which focuses on specific engagements and strategy as that which brings those engagements together in working towards a common goal.

Page 75 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:42]</span></span>:
Tactics and strategy in anarchist politics

Page 75 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:45]</span></span>:
For anarchism, then, the kind of repertoires of action that are of interest are those that aim at changing reality in the here and now, at creating alternatives to what presently exists and, where judged to be necessary, directly challenging the dominance or even existence of what exists.

Page 76 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:47]</span></span>:
the tactics of anarchist organising cover this range of collective actions that, in one way or another, enact the ideals of anarchist politics – a concept called ‘prefiguration’
● tactics is prefiguration? the tactics of anarchist organising cover this range of collective actions that, in one way or another, enact the ideals of anarchist politics – a concept called ‘prefiguration’

Page 76 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 09:50]</span></span>:
First, strategy should operate to frame tactical action within the overall goals of the organisation. Second, strategy should be informed by the anarchist politics of selforganisation and participatory democracy discussed throughout this book so far. Third, strategy should be flexible and responsive to change.

Page 76 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-10 Sat 11:27]</span></span>:
in the language of Beer’s cybernetics and the VSM, the strategic function in an organisation is concerned with regulating the overall behaviour of the organisation in line with defined goals and in response to change both inside and outside the organisation.

Page 78 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-11 Sun 00:23]</span></span>:
strategy in terms of prefiguration. Prefiguration The discussion of the relationship between tactics and strategy covered thus far has centred on what I highlighted as the first dynamic of strategy

Page 79 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 07:57]</span></span>:
practising prefiguration has meant always trying to make the process we use to achieve our immediate goals an embodiment of our ultimate goals, so that there is no distinction between how we fight and what we fight for, at least not where the ultimate goals of a radically different society is concerned.

Page 79 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 07:58]</span></span>:
It is often summed up by reference to the Industrial Workers of the World slogan ‘building a new world in the shell of the old’ and its origins are to be found in the anarchist tradition from the late 19th century onwards.

Page 79 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 07:59]</span></span>:
The First International, the attempt in the mid1860s to bring together a range of leftwing groups across the world, fell apart because of a conflict between those who followed Karl Marx and argued that taking over government and seizing state power are central to building communism and the anarchists who sided with Mikhail Bakunin and his belief that revolution could only come about by building the desired future society in the present.

Page 80 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 08:01]</span></span>:
Emma Goldman (1924), another influential early anarchist theorist and activist, similarly argued that ‘methods and means cannot be separated from the ultimate aim. The means deployed become, through individual habit and social practice, part and parcel of the final purpose; they influence it, modify it, and presently the aims and means become identical.’

Page 80 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 08:02]</span></span>:
could we expect an egalitarian and free society to emerge out of an authoritarian organisation! It is impossible.

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 08:04]</span></span>:
The definition of prefiguration as focused on means (action) embodying or reflecting ends (goals) is shared by a range of authors

Page 81 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 08:05]</span></span>:
prefiguration is a practice, something that people do, and not just an abstract political concept.

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 13:24]</span></span>:
The key difference between prefigurative and consequentialist politics is that the tactics of prefiguration are intended to enact these desired goals in the present rather than trying to make their realisation more likely in the future.

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 13:26]</span></span>:
Prefigurative strategy concerns how strategic goals in anarchist and radical left organising are developed and agreed on and the way in which these processes of decision making themselves prefigure these goals, at least to the extent that they deal with participatory and democratic forms of governance.

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 13:28]</span></span>:
These prefigurative practices for decision making must be capable of subjecting decisions regarding goals (including the goal of realising these practices) to the same kind of reflection and deliberation that any other form of action would be subject to.

Page 83 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 16:48]</span></span>:
Both tactics and strategy prefigure the goals of the organisation; the former through embodying those goals in actions taken, the latter through subjecting the organisation’s goals to participatory and democratic reflection and deliberation.

Page 83 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 16:48]</span></span>:
In keeping with anarchist cybernetics’ resistance to mirroring this type of functional hierarchy in a structurally hierarchical organisational form, the higherlevel strategic functions are performed by the same people and groups that take functionally lower tactical action.

Page 83 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 17:01]</span></span>:
consistently with democratic control: ‘[b]ut the vital distinction comes here. The precise form of variety attenuation is a matter for local decision’ (1974: 79). Experimentation, flexibility and adaptation

Page 84 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 17:02]</span></span>:
realise, is also an ongoing process whereby the tactical repertoires adopted by collective actors are in a continual state of flux and are open to renegotiation over time and across changing circumstances and contexts.

Page 84 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 17:28]</span></span>:
This experimental side of prefiguration, where actions and ideals are, in Van de Sande’s words, ‘formulated, realized, tested, improvised, and discussed’ (2015: 190), can be seen as another point of overlap with Beer’s cybernetics, specifically with how he describes the planning function in an organisation.

Page 85 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 17:32]</span></span>:
Andrew Pickering describes this line of thinking in Beer’s work as ‘adaptive’ (2010: 224), because the planning process is about strategies that adapt to change through assessing a situation, introducing goals and relevant tactics, reassessing as things change, and abandoning those goals and tactics for new ones in an ongoing cycle.

Page 85 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun 17:42]</span></span>:
anarchist selforganisation differs from that described by Walter is that it is both goalseeking and goalmaking through processes of democratic participation.

Page 87 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-21 Wed 21:27]</span></span>:
cybernetic terminology, while Systems Three and Four perform a strategic function that is open to change through participatory and democratic decision making, System Five performs a far more fundamental function – in that it provides the initial goals or worldview that originate the prefigurative strategic cycle, goals that are considerably less open to change.

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 09:58]</span></span>:
What is important when it comes to grand strategy as the function of System Five in the VSM is that they provide the overarching principles and goals that make strategic decision making and, downstream from that, tactical action, possible within a single coherent organisational agenda.

Page 89 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:00]</span></span>:
council ‘would deal with logistical and financial decisions, whereas the [general assembly] would deal with larger political questions about OWS and the greater movement’. This provides a clear statement of the distinction

Page 90 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:00]</span></span>:
The spokescouncil, which included delegates from working groups and caucuses mandated by their groups to support certain agreements, dealt with general (rather than grand) strategic matters such as finance and logistics, which would then have an impact on the tactical choices that were open to the working groups.

Page 90 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:01]</span></span>:
The general assembly, where all participants could be part of the decisionmaking process, would maintain the grand strategic function of debating the movements aims and identity, its overarching goals and fundamental principles.

Page 90 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:03]</span></span>:
The VSM provides us with three distinct but related functional levels of decision making and acting. Systems One and Two help explain how organisations, or rather the separate parts of organisations such as the working groups of Occupy Wall Street, take collective tactical action. Systems Three and Four articulate the strategic decision making that frames such tactical action within the overall goals of the organisation, that in Occupy can be seen in the function played initially by the general assembly and later by the spokes council. System Five, in turn, highlights the grand strategic function, played by the general assembly

Page 91 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:03]</span></span>:
Occupy, which operates on a functionally different (higher) level to that making the decisions relevant to general strategic functions (Systems Three and Four).

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:05]</span></span>:
Control (Part II): Effective Freedom and Collective Autonomy Autonomy

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:13]</span></span>:
fact two distinct but connected definitions of autonomy at play – Functional Autonomy, drawn from cybernetics, and Collective Autonomy, developed through a reading of anarchism and related social movement traditions.

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:13]</span></span>:
These reflect two different sides of anarchist cybernetics: the ethical or political side, that champions anarchy and autonomy because they entail the freedoms individuals and groups are entitled to; and the practical side, that proclaims anarchy as the most effective way to structure a complex society.

Page 93 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:17]</span></span>:
While autonomy is essential for an organisation to be able to flexibly react and respond to complexity and change, at the same time, some degree of coherence between autonomous parts is just as important, because otherwise there would be no organisation, with overarching goals, to speak of.

Page 93 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:19]</span></span>:

1.  autonomous parts must operate in coordination with other autonomous parts; 2. autonomous parts must operate within the intentions of the whole organisation; and 3. autonomous parts must face the possibility of being excluded from the organisation as a whole.

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:40]</span></span>:
Fanfare for Effective Freedom. Cybernetics Praxis in Government’

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:40]</span></span>:
the polarity between centralisation and decentralisation – one masquerading as oppression and the other as freedom – is a myth’

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 10:41]</span></span>:
No viable organism is either centralised or decentralised. It is both things at once, in different dimensions.’

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 15:49]</span></span>:
effective freedom’

Page 95 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 15:50]</span></span>:
(1) that freedom was something that could be calculated and (2) that freedom should be quantitatively circumscribed to ensure the stability of the overall system’ (2011: 181). As an aside, the quantitative nature of freedom, as

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 15:55]</span></span>:
Autonomy, then, is a purely descriptive concept in so far as it identifies a functional condition in the parts of a system or organisation and is only of value in so far as it can be said to contribute to that system or organisation being viable.

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 15:55]</span></span>:
the use of autonomy is unrelated to a normative conceptualisation that values it for its own sake (that is, that organisations should be structured to enhance autonomy because autonomy is a quality we consider as good).

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 15:59]</span></span>:
this account of autonomy is ‘distinct from the ethical, political, or psychological arguments’ on the topic

Page 98 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:02]</span></span>:
On the one hand, there is a strong individualist strain running through the anarchist tradition, with anarchists who are commonly associated with collectivist or communist anarchisms espousing what, on the face of it, may seem like highly individualist statements.

Page 99 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:14]</span></span>:
Rather, autonomy is understood as dependent on collective organisation for its development and is thus constrained by this collective setting, its relation to the autonomy of others and the demands of maintaining the collective.

Page 100 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:15]</span></span>:
One of the political and activist traditions that has best characterised this form of autonomy is the Autonome movement in Germany, which sits adjacent to anarchism. In a way that chimes well with how anarchist cybernetics has been framed thus far in this book, the Autonomen articulate autonomy as a property of collectives rather than of individuals.

Page 100 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:17]</span></span>:
be identified and spoken of at all. Far from being a point of origin for political organisation it is in fact an outcome, one that is impossible without such organisation first being in place.

Page 101 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:22]</span></span>:
In her work on the German Autonomen, Darcy Leach writes that autonomy is about ‘balancing collective responsibility and solidarity against the right of selfdetermination’ (2009: 1057). Drawing on how autonomy is understood by the German Autonome movement and by theorists such

Page 102 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-23 Fri 16:28]</span></span>:
In contrast to liberal individualism, then, collective autonomy can be defined as follows: Collective Autonomy is the collectively determined capacity and scope an individual or group has to decide and act within the constraints set by collective organisation.

Page 104 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:27]</span></span>:
Consensus decision making is a creative and dynamic way of reaching agreement between all members of a group. Instead of simply voting for an item and having the majority of the group getting their way, a group using consensus is committed to finding solutions that everyone actively supports, or at least can live with. (Seeds for Change, 2013)

Page 104 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:28]</span></span>:
there are similarities between processes of CDM and the Team Syntegrity or Syntegration protocols developed by Beer and others that were discussed briefly in Chapter 3, and that can also form the basis for reaching consensus in organisations.

Page 105 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:31]</span></span>:
Given the aim of CDM of attempting to reach consensus, when compared to forms of decision making based on majorities measured through voting, CDM serves to shift the focus from competition to cooperation and common agreement.

Page 105 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:32]</span></span>:
Whatever form CDM takes, a key part of the process is that individuals are encouraged to share ideas, experiences and opinions in an open and accessible discussion that then aims to find a position everyone can agree on.

Page 106 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:33]</span></span>:
While designed to prevent the domination of a small minority by a large majority, blocking also paves the way for the smallest of minorities to dominate the majority.

Page 109 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:38]</span></span>:
Many strands of anarchism are built around communities being radically inclusive of difference, and to raise the prospect of exclusion on the grounds of effectiveness does stand at odds with how anarchism is often conceptualised and practised.

Page 110 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:40]</span></span>:
The freedom we embrace’, Beer writes, ‘must yet be “in control”. That means that people must endorse the regulatory model at the heart of the viable system in which they partake’

Page 112 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:45]</span></span>:
the Age of Social Media The discussion,

Page 112 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:45]</span></span>:
of understanding how selforganisation facilitates effective responses to complexity.

Page 112 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:47]</span></span>:
tomany communication. This refers to the type of networks in which anyone can share information with anyone else. Instead of communication being about two actors speaking to each other in relative isolation (onetoone communication) or a single actor or small select group broadcasting a message to a larger audience (onetomany communication), the communication processes of interest in this discussion of anarchist cybernetics are ones that maintain a level of horizontality and equality

Page 113 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:49]</span></span>:
manytomany communication. Cybernetics, information theory and communication T o start with, let’s take a look at how communication is understood in cybernetics. In his book Cybernetics and Management, Beer describes a system in terms that

Page 113 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:53]</span></span>:
Viable System Model, introduced in Chapter 3, lines of communication were again identified as being equally important for effective organisation as the different parts of the system are.

Page 113 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:54]</span></span>:
Indeed, for cybernetics, the organisation of a system is considered to be, in many ways, identical to its communication network.

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:54]</span></span>:
one of the key ways in which communication functions is articulated through the concept of ‘feedback’

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:55]</span></span>:
This notion of feedback, as an opinion or report that we can present to someone and that they can use in one way or another, is a slight corruption of how the concept was initially developed.

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:55]</span></span>:
Beer emphasised feedback as ‘the most important concept of all’ when it comes to understanding selforganisation and control in systems and organisations

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:55]</span></span>:
cybernetics, feedback refers not to a piece of information transmitted between two actors but to a causal loop that helps regulate behaviour in a system.

Page 114 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 07:57]</span></span>:
Feedback can be understood as operating in two ways, positively or negatively, both of which need to be divorced from any normative connotations. Positive feedback refers to a causal relationship where a certain behaviour in one part of a system triggers a response in another part of the system, that then communicates back to the initial part in such a way as to encourage more of the initial behaviour. Negative feedback, in contrast, occurs when the initial behaviour triggers a response such that the information communicated back leads to a decrease in that initial type of behaviour.

Page 115 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:01]</span></span>:
Pioneered by Claude Shannon and Warren Weaver, information theory articulated a technical, mathematical approach to communication. I can offer only the briefest of overviews of the technical aspects of information theory here, but what information

Page 116 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:02]</span></span>:
Shannon and Weaver aimed to identify ‘what was communicated in the messages flowing through feedback control loops that enabled all organisms, living and nonliving, to adapt to their environments’

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:05]</span></span>:
Indeed, as Kline notes (2015), over time there emerged a close relationship between the two definitions of information – as signal uncertainty and as meaningful or semantic content – and the idea of ‘the information age’ has its roots in both Shannon and Weaver’s work and in Wiener’s cybernetics.

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:10]</span></span>:
when discussing communication in relation to contemporary anarchist and radical left ideas of organisation what is of primary concern is digital platforms that facilitate manytomany communication. In other words, social media platforms.

Page 118 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:13]</span></span>:
As receivers of messages, then, noise is something we experience as a disruption that makes it difficult for us to understand content.

Page 118 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:14]</span></span>:
As we scroll through our timelines, we are engaging in something similar to deciphering a signal in order to determine meaningful content.

Page 118 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 08:14]</span></span>:
In effect, we experience on social media a mass of noise that must we must wade through in order to find the messages (tweets, posts and so on) that we really want to engage with.

Page 119 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 09:01]</span></span>:
The picture presented of noise so far here, as a disruption to a signal behind which the true meaning of the message lies, is not one that has gone without critique. There are two broad strands to such critiques: that such a conception of noise fails to recognise that human communication is an interactive process, in which the transmitter and the receiver negotiate meaning; and that, in this conception of noise, communication is reduced to something that is only concerned with functional effectiveness.

Page 119 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 09:03]</span></span>:
A similar critique has been applied to Beer’s cybernetics by Werner Ulrich (1981), who argued, in an important and insightful analysis of Project Cybersyn in Chile, that for cybernetics to form a part of participatory forms of organisation, individual actors in communication networks must be able to determine for themselves the meaning in the messages they receive.

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 09:04]</span></span>:
Platform for Change,

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 09:05]</span></span>:
The process of negotiation will involve sender and receiver not only constructing the meaning of the message but also navigating the presence of noise.

Page 120 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat 09:07]</span></span>:
identifying in noise a site of potential resistance to this command and control dynamic.

Page 121 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 01:16]</span></span>:
Gordon Pask’s conversation

Page 122 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 01:18]</span></span>:
Pask developed a machine he called Musicolour, that took a musical input and used it to activate a series of light banks.

Page 123 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 01:19]</span></span>:
Pask also worked on an architectural concept, called the Fun Palace, in which a physical environment could adapt to its inhabitants and change over time.

Page 125 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:01]</span></span>:
As the researchers involved in the 15M DatAnalysis group argue, pink noise is ‘an indicator of distributed selforganisation in a coherent whole: different parts of the system (with their characteristic frequencies) appear globally coordinated in a reciprocal influencing manner’

Page 126 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:03]</span></span>:
Organisations with communication processes in which pink noise is present are ones that meet the functional conditions spelled out by Beer in his Viable System Model.

Page 126 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:03]</span></span>:
noise and communication At a fundamental level, then, questions of noise and communication are questions of organisation. For cybernetics, organisation is indeed nothing other than nodes in

Page 126 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:06]</span></span>:
how communication can be organised so that noise is present in a form that is productive of participatory and democratic selforganisation. Pink noise reflects organisational and communication processes that can endure over time, quickly relay

Page 127 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:08]</span></span>:
Pask insisted that any interaction between humans and machines in communication processes should involve the structures framing these interactions being capable of experimentation and changing over time.

Page 127 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:09]</span></span>:
Fun Palace

Page 127 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:10]</span></span>:
actors). As Pickering puts it, drawing on Pask’s related work on computers, ‘the built environment and its inhabitants’ use of it coevolve openendedly in time in ways neither the architect, nor the computer, […] could have foreseen’

Page 127 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:14]</span></span>:
Turning to digital platforms that facilitate selforganisation, these too must be fundamentally openended and adaptable.

Page 127 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:15]</span></span>:
What a cybernetic approach to communication and selforganisation demands, in contrast, is something more akin to an open source platform, where users can craft the infrastructure as they engage with it and modify it over time.

Page 128 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:18]</span></span>:
Through combining the insights of both Pask and the 15M DatAnalysis group, what we can propose is that communication practices should be conversational and interactive, and should be characterised by the presence of pink noise. Beyond this, however, there may be little that can concretely be recommended

Page 128 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:18]</span></span>:
Conclusion

Page 129 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:19]</span></span>:
Pickering,
● whaţbook is this Pickering,

Page 129 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:20]</span></span>:
On this account, noise, rather than being something to be simply removed from signals, is understood in far more nuanced terms, as something that exists in different forms in communication processes and that is productive of different kinds of organisation.
● nkt sure how useful this anakysis of noise is, i like the idea to nkt see nise as wholly bad. and pasks conversation thêry. cnversation rather than ffient comms. but wat des it achieve? On this account, noise, rather than being something to be simply removed from signals, is understood in far more nuanced terms, as something that exists in different forms in communication processes and that is productive of different kinds of organisation.

Page 129 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:23]</span></span>:
Thus, instead of anarchist organisations seeking to remove noise, anarchist cybernetics proposes that pink noise, as an aspect

Page 130 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:24]</span></span>:
Social Media Communication is,

Page 130 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:25]</span></span>:
included the highly structured processes of the general assembly and spokes-council, as well as the conversations that camp members were having with one another throughout the days and weeks that the camp was in place.

Page 130 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:25]</span></span>:
this chapter, I want to build on the previous discussion to explore how functional communication with respect to selforganisation might be supported by technology.

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:30]</span></span>:
Alternative media

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:31]</span></span>:
At least since the Levellers, the egalitarian populist movement of mid17thcentury England, who pioneered the use of pamphlets as a means of communication, radical political groups have always valued the production of media.

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:31]</span></span>:
Often, these media have been characterised as ‘alternative media’. They are seen as separate from mainstream media, like television networks and large newspaper publishers, and are intended to communicate a group’s political message to the public.

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:32]</span></span>:
media that are alternative to, or in opposition to, something else: massmedia products that are widely available and widely consumed’

Page 131 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:32]</span></span>:
Identifying and critiquing the key characteristics of mainstream media, such as their corporate ownership, funding structure and their position in the matrix of political power, alternative media embody alternatives to such characteristics and attempt to manifest them in different structures. Specifically, anarchist discussions of alternative media highlight participation as one of the key aspects of
● i dont fully like ths definition by negation. Identifying and critiquing the key characteristics of mainstream media, such as their corporate ownership, funding structure and their position in the matrix of political power, alternative media embody alternatives to such characteristics and attempt to manifest them in different structures. Specifically, anarchist discussions of alternative media highlight participation as one of the key aspects of

Page 132 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:34]</span></span>:
For anarchist alternative media, then, the image of what media would look like in an ideal anarchist society ought to inform that which is created today.

Page 132 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:41]</span></span>:
Indeed, Jeppesen et al. define anarchist or antiauthoritarian media collectives as those that ‘establish economic and organizational forms that prefigure cooperative futures and build strong relationships with broader social movements while simultaneously creating counterhegemonic content and counterpublics around interlocking

Page 132 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:42]</span></span>:
In this chapter, I want to attempt just such an analysis of mainstream social media and a subsequent envisioning of alternative social media, a vision of the kind of digital communication platforms that might help facilitate selforganisation. Before outlining some of the necessary parameters for and features of alternative or anarchist social media, a critique

Page 132 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:43]</span></span>:
Essentially, as discussed elsewhere in this book, my interest here is in digital platforms that act as infrastructures for manytomany communication. Given this definition of social media, how do mainstream platforms shape or regulate this

Page 132 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:43]</span></span>:
Four critiques of social media

Page 133 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:44]</span></span>:
(Fuchs (2014) provides an interesting overview of the debates around privacy and social media).

Page 133 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:45]</span></span>:
Increasingly, however, concern around privacy and the collection of user data has been linked to political campaigning.

Page 134 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:46]</span></span>:
Such targeting online was not solely designed to encourage people to vote for a certain candidate; there is also evidence that it was used to suppress turnout in certain areas.

Page 134 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:46]</span></span>:
The personal data collected by Facebook in particular, it was suggested, allowed for massive and complex manipulation of voter behaviour and has been linked to the results of the 2016 presidential election in the US and the UK vote to leave the EU in the Brexit referendum of the same year.

Page 134 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:47]</span></span>:
The second critique of mainstream social media is one that focuses on political economy.

Page 135 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:49]</span></span>:
When Facebook employees code algorithms for data extraction, or develop protocols like the ‘Like’ button, they are effectively managing. They are guiding user behaviour in such a way that it is more likely to create marketable data, or generate content that will draw other users’ attention, which can subsequently be commodified via advertising. (Beverungen et al., 2015: 483)

Page 135 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:54]</span></span>:
Approaching the critique of social media with this type of political economic analysis in mind also serves to draw attention to the material aspects of digital media. Fuchs (2014: 119– 20) highlights the manual labour that goes into mining the minerals required for producing the physical devices used to access digital media, such as smartphones and tablet computers, as well as the conditions of labour in the factories assembling these devices. The material economic cost to human beings, in places like China and the Democratic Republic of Congo, of making digital media possible ought also to be taken into consideration when discussing the appropriateness of social media platforms for anarchist and radical organising. This is especially true if such organising is framed in terms of prefigurative politics. Can action aimed at bringing relationships of solidarity and mutual aid into being be based on such dire and exploitative working conditions?
● here s part f the link between restart and indeweb for me! READ FUCHS. Approaching the critique of social media with this type of political economic analysis in mind also serves to draw attention to the material aspects of digital media. Fuchs (2014: 119– 20) highlights the manual labour that goes into mining the minerals required for producing the physical devices used to access digital media, such as smartphones and tablet computers, as well as the conditions of labour in the factories assembling these devices. The material economic cost to human beings, in places like China and the Democratic Republic of Congo, of making digital media possible ought also to be taken into consideration when discussing the appropriateness of social media platforms for anarchist and radical organising. This is especially true if such organising is framed in terms of prefigurative politics. Can action aimed at bringing relationships of solidarity and mutual aid into being be based on such dire and exploitative working conditions?

Page 135 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:56]</span></span>:
The third critique of mainstream social media platforms identifies the potentially problematic nature of the kind of relationships social media produces between users.

Page 136 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:56]</span></span>:
Strong ties, it is argued, are required for stable political organisation, while weak ties are considered to be difficult to translate into offline relations and engagements that demand an obligation on the part of those involved.

Page 136 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-25 Sun 08:57]</span></span>:
The weak ties critique suggests that, with respect to mainstream social media, while communication takes place and so some form of system exists, this is not adequate for the creation and development of the form of organisation that Beer’s Viable System Model might apply to.

Page 136 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-27 Tue 21:00]</span></span>:
The subjectivity of users – that is, the agency and potential for thought and action individuals have – is, on mainstream social media platforms, conditioned towards individualism.

Page 137 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-27 Tue 21:02]</span></span>:
The subjectivity critique, then, contends that the way behaviour on social media is shaped, through how the structure of interactions is enabled and restricted, moulds individual users into the kind of individualist agents or subjects that are desired by neoliberal economic theory. By encouraging maximising behaviour in relation to the quantity of likes and shares, for example, the homo economicus (economic man [sic]) of neoliberal economics is produced.

Page 138 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-27 Tue 23:07]</span></span>:
Building alternative social media

Page 138 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-27 Tue 23:08]</span></span>:
When we explore alternative social media platforms that are designed specifically for selforganisation, we need to remember that these ought to be seen as aids for offline organising, and as such, as infrastructures that support facetoface discussion and action rather than as an entirely separate sphere that radical politics

Page 141 <span class="timestamp-wrapper"><span class="timestamp">[2021-07-31 Sat 19:24]</span></span>:
Through a userfriendly design process that provides various building blocks for effective digital platforms, a participatory design process can be put into practice that represents user’s solutions to their own problems.

Page 142 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-01 Sun 00:07]</span></span>:
the previous section argues that any design process ought to be participatory and democratic, responding to the specific needs of users and being capable of adapting and changing as those needs develop over time.
● bonfire the previous section argues that any design process ought to be participatory and democratic, responding to the specific needs of users and being capable of adapting and changing as those needs develop over time.

Page 143 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-01 Sun 09:31]</span></span>:
[t] he editorial decision is the biggest variety attenuator that our culture knows. […] [T]he cybernetic answer is to turn over the editorial function to the individual, which may be done by a

Page 143 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-01 Sun 09:32]</span></span>:
Linking alternative and mainstream platforms

Page 144 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-01 Sun 09:33]</span></span>:
An alternative platform would need to ensure that filtering for noise and overload was, at the very least, open to democratic oversight, if not the product of direct participatory engagement, for instance through popular content floating to the surface, as it were, through a voting procedure.

Page 146 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:48]</span></span>:
any alternative digital platform ought to have a direct link between whatever decisionmaking functions it houses and the facetoface decisionmaking procedures of the organisation using it.

Page 146 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:50]</span></span>:
should involve the development of architectures that are designed to move behaviour away from **a commodification of the activities**.

Page 147 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:51]</span></span>:
intense activity and responsibility, while still maintaining their social connections. Responsibility and commitment Another key way to achieve a balance between autonomy and organisational cohesion is to build mechanisms into an alternative digital platform that foster responsibility and commitment.

Page 147 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:52]</span></span>:
It ought to be made clear to all users, however, what type of actions would trigger such removal and the users of the platform must be in control of what is permitted on the platform and what is not.

Page 148 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:55]</span></span>:
More expansive than Crabgrass, Lorea networks allowed for customisable profile pages and dashboards, wikis, collaborative writing pads, blogs, task managers, status updates and private messaging, affiliations with and between groups and a federal structure for groups and networks.

Page 150 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:57]</span></span>:
And even for highly functional, yet easytouse platforms, like Loomio and Slack, the barrier of having to sign up and log in to yet another platform, even for activists, has proven exceedingly difficult to overcome. App versions of these platforms that run on smartphones mitigate this, to an extent, by having users always signed

Page 150 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:58]</span></span>:
Perhaps, rather than producing entirely new alternative platforms, the task for radical developers is to identify suites of existing platforms that are most useful to selforganisation and to then find ways of integrating these in ways that allow for coordination (for example, by being able to send and receive WhatsApp messages from a platform like Loomio).

Page 151 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 04:59]</span></span>:
The text beneath the image has the individual making a series of demands of the technology surrounding them, asking to know more about a news item, to pull up some historical information, to explain a scientific term, update a tax record, send a notification to a friend about a telecommunicated chess game, and so

Page 151 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:05]</span></span>:
Social media platforms, in so far as they facilitate interactive communication and organisation, have the potential to play a central role in anarchist cybernetics and thus anarchist (self) organisation more broadly.

Page 154 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:10]</span></span>:
Organising Radical Left Populism

Page 154 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:13]</span></span>:
hierarchies. The three central concepts that underpin this framework are selforganisation, functional hierarchy and manytomany communication. Selforganisation is a foundational concept that is common to both anarchist politics and cybernetics and has been identified by authors,

Page 156 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:16]</span></span>:
conversation theory,

Page 156 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:20]</span></span>:
Radical politics since 2011 and the electoral turn

Page 158 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:22]</span></span>:
Assemblyism’, as he calls it, ‘mistook consensus assemblies, which had emerged from quite specific circumstances and inheritances, for a new universal model of democracy, which at the very least prefigured the postcapitalist society to come’

Page 158 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:24]</span></span>:
Gerbaudo too considers the 2011 uprisings as ‘a fundamental historical turning and a foundational development for a new wave of progressive politics’ (2017:

Page 159 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:26]</span></span>:
Anarchism and radical left populism

Page 161 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-06 Fri 05:35]</span></span>:
Iceland’s ‘pots and pans revolution’ – so named because of the noise protestors made by banging kitchen implements together when they surrounded the parliament in Reykjavík – is, like Occupy, 15M and the Arab Spring, rooted in the financial crash of 2008

Page 164 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 00:07]</span></span>:
Coexistence and hybridity

Page 165 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 00:11]</span></span>:
Ultimately, the functions of organisation are arranged in ways that involve the leadership at the top taking on strategic and grand strategic functions, with those at the bottom of the hierarchy potentially possessing some tactical autonomy but essentially having no control over the functional constraints on that autonomy.
● how do you account for exPeRence?Ultimately, the functions of organisation are arranged in ways that involve the leadership at the top taking on strategic and grand strategic functions, with those at the bottom of the hierarchy potentially possessing some tactical autonomy but essentially having no control over the functional constraints on that autonomy.

Page 166 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 00:18]</span></span>:
A strategic anarchism

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:01]</span></span>:
It was precisely a disagreement over forming political parties (and the associated strategy of seizing state power) that caused the split in the First International between the Marxists and the anarchists.

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:01]</span></span>:
Is the attempt at bringing the interactive nature of horizontal movements together with the institutional structure of vertical parties and similar organisations like trade unions something that will only ever result in the institution overwhelming the interaction, thus reproducing the structural hierarchy and command and control system anarchists oppose?

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:02]</span></span>:
One of the key legacies of the alterglobalisation movement is a rejection of hegemonic politics in favour of a politics built around a proliferation of autonomous affinity groups.

Page 167 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:03]</span></span>:
Hegemony, as the concept has been used in radical theory, going back to the work of Antonio Gramsci, concerns the ideological dominance of a particular politics.

Page 168 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:06]</span></span>:
It is support for this type of approach that can be seen as underpinning the initial enthusiasm for the 2011 uprisings among anarchists and similar radical leftists. Rather than attempting to take control of the state through elections or other means, and bring about reform and indeed transformation that way, these movements were seen to be carving out spaces of autonomy within territories governed by the state.

Page 168 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:06]</span></span>:
The hope was that, eventually, these autonomous zones would link up and federate, and through doing so lead to the state and other forces of domination withering away, not through centrally managed policies but through their increasing irrelevance to people’s lives.

Page 168 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:09]</span></span>:
Examples, such as the participatory constitutionalising process in Iceland and the federated communes of Rojava, may point towards processes whereby radical and even anarchist politics can be counterhegemonic without being appropriated by the logics of domination that are bound up in the state. If anarchist cybernetics is to be useful now, and in the future, it is in such realms of possibility that it might be applied. By identifying

Page 169 <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat 06:10]</span></span>:
By separating organisational function from organisation form, anarchist cybernetics highlights how anarchist and radical left organising can maintain its commitment to selforganisation in new political terrains and, crucially, points towards how institutions can be reshaped to create space for genuinely democratic participation.

