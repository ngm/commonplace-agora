# Why It's Eco-Socialism or Collapse

Found at
: https://novaramedia.com/2021/04/29/why-its-eco-socialism-or-collapse-downstream/

[[Ecosocialism]] or [[collapse]].

> [[Climate change]] is better understood as [[climate systems breakdown]], and it’s only through the lens of the latter that we can grasp the scale of the crisis ahead. On this episode of Downstream, Aaron Bastani is joined by [[Mat Lawrence]] and Laurie Laybourn-Langton to discuss their new book, [[Planet on Fire: A Manifesto for the Age of Environmental Breakdown]].

