# The Tories keep on stymying onshore wind

The [[Tories]] keep on stymying [[onshore wind]].


## Unanimous consent

> Under a moratorium implemented by [[David Cameron]]’s government in 2015, onshore windfarms in England require unanimous consent from local communities, meaning they can easily be blocked.
> 
> &#x2013; [PM to put nuclear power at heart of UK’s energy strategy | Energy | The Guardian](https://www.theguardian.com/environment/2022/apr/06/pm-to-put-nuclear-power-at-heart-of-uks-energy-strategy) 


## Lobbying against onshore wind

> Pro-green cabinet ministers are frustrated by Boris Johnson’s decision to back away from ambitious onshore windfarm plans for England, as it emerged more than 100 Tory MPs are lobbying against the policy behind the scenes
> 
> &#x2013; [Boris Johnson blows cold on onshore wind faced with 100-plus rebel MPs | Wind&#x2026;](https://www.theguardian.com/environment/2022/apr/05/boris-johnson-blows-cold-on-onshore-wind-faced-with-100-plus-rebel-mps)

