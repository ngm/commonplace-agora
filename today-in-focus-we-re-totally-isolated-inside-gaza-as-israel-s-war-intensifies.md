# Today in Focus: ‘We’re totally isolated’: inside Gaza as Israel’s war intensifies

URL
: https://www.theguardian.com/news/audio/2023/oct/31/were-totally-isolated-inside-gaza-as-israels-war-intensifies-podcast

They speak to a journalist in [[Gaza]] who is trapped there along with his family.  You can hear the airstrikes happening during the interview.

