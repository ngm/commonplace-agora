# Reframing and simplifying the idea of how to keep a Zettelkasten

An
: [[article]]

Written by
: [[Chris Aldrich]]

Found at
: https://boffosocko.com/2022/06/10/reframing-and-simplifying-the-idea-of-how-to-keep-a-zettelkasten/

[[Commonplace books]], [[zettelkasten]].

Really nice summary of the minimum of what you need to do to keep a 'zettelkasten', cutting through a lot of the complexity that has appeared around this.

-   start with a commonplace book
-   add a way to find things again (could be an index, could be search)
-   keep reference / bibliography notes
-   summarise and rewrite ideas in your own words
-   link individual notes together
-   answer a question by finding your notes on the topic, following the links you've made, and pulling together your summaries

Similar in spirit to e.g. [[Collector to Creator]].  But I like that Chris frames collector as a perfectly valid and minimal friction starting point.

> collect interesting passages, quotes, and ideas as you read.

No stress to create just yet.

