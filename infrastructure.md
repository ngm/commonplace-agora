# infrastructure

> Infrastructure is the set of fundamental facilities and systems that support the sustainable functionality of households and firms. Serving a country, city, or other area, infrastructure includes the services and facilities necessary for its economy to function.
> 
> &#x2013; [Infrastructure - Wikipedia](https://en.wikipedia.org/wiki/Infrastructure) 

