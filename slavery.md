# Slavery

> American historian [[Howard Zinn]] described the capture and journey of Africans to slavery:
> 
> The marches to the coast, sometimes for one thousand miles, with people shackled around the neck, under whip and gun, were death marches, in which two of every five blacks died. On the coast, they were kept in cages until they were picked and sold … Then they were packed aboard the slave ships, in spaces not much bigger than coffins, chained together in the dark, wet slime of the ship’s bottom, choking in the stench of their own excrement… On one occasion, hearing a great noise from below decks where the blacks were chained together, the sailors opened the hatches and found the slaves in different stages of suffocation, many dead, some having killed others in desperate attempts to breathe. Slaves often jumped overboard to drown rather than continue their suffering. To one observer a slave-deck was “so covered with blood and mucus that it resembled a slaughter house.”
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Nearly all of Britain’s growing towns and cities flourished as a result of slavery—from seaport towns like [[Liverpool]], centered on trade of enslaved people, to manufacturing towns like [[Lancashire]] and [[Manchester]].
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The slave plantations and the colonial settlements were built upon over a billion and a half acres of land inhabited by Native American tribes. Their brutal dispossession and ethnic extermination were nothing short of genocidal
> 
> &#x2013; [[A People's Guide to Capitalism]]

