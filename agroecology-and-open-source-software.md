# Agroecology and open-source software

[[Agroecology]] and [[Open-source]].

> jeff - Looking forward to this new book by Dorn Cox and Courtney White:
> 
> The Great Regeneration:
> Ecological Agriculture, Open-Source Technology, and a Radical Vision of Hope
> 
> &#x2026;explores the critical function that #OpenSource tech can have in promoting healthy #agroecological systems, through data-sharing and networking&#x2026;.
> 
> brought together, there is potential to revolutionize how we manage food production&#x2026; decentralizing and deindustrializing [long dominant] structures&#x2026;"
> 
> https://www.chelseagreen.com/product/the-great-regeneration/
> 
> &#x2013; https://social.coop/@jeff/109558583982281354

