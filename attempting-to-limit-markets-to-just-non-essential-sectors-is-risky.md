# Attempting to limit markets to just "non-essential" sectors is risky

A
: [[claim]]

[[Markets]].

> Limiting them to the "non-essential" margins is a challenge because they tend to take on an expansionary nature.
> 
> &#x2013; [[Forest and Factory]]

<!--quoteend-->

> it is simply too dangerous to revive the use of money and markets even for "non-essential" sectors
> 
> &#x2013; [[Forest and Factory]]

<!--quoteend-->

> while these forms can and have existed beyond capitalist society, they are nonetheless extremely dangerous. Throughout history, market relations were largely insulated from the "essential sphere" (of local subsistence production) but also repeatedly ran up against this sphere. Eventually, markets and money broke through and established themselves as the basis of our social metabolism. Because of this, susceptibility to future spillovers is even higher. We might think of money and markets in much the same terms as a virus. Even if the virus is largely limited to some marginal vector species, repeated contact with that species can threaten a zoonotic leap which—if certain conditions are met—then allows the virus to propagate within the human population at a rapid pace. For this reason, we argue that it is simply too dangerous to revive the use of money and markets even for "non-essential" sectors—though the risk could, conceivably, decline hundreds or thousands of years into the future, after communist society has advanced so far that it achieves effective immunity.
> 
> &#x2013; [[Forest and Factory]]

<!--quoteend-->

> Those who claim that money and markets can be used within communist society must, then, account for how, exactly, these mechanisms would remain limited to their marginal uses without spilling over into the mesh of essential activities that compose the core of the social metabolism.
> 
> &#x2013; [[Forest and Factory]]

