# Boox handwriting recognition missing whole words out

I was noticing the handwriting recognition on [[Boox]] had gone really bad (on a full page - it's still great on handwriting input from the keyboard).

Similar issues documented by others:

-   https://www.reddit.com/r/Onyx_Boox/comments/14db4uf/problems_with_handwriting_recogition_on_boox_tab/?rdt=63793
-   https://www.reddit.com/r/Onyx_Boox/comments/10zoupk/missing_words_in_boox_notes_handwriting/

Someone suggesting that hyphens in your text is what really throws it. Maybe so, but that makes it pretty useless.

But! Thankfully it seems much better since the Nov 1st 2023 update ([[Boox Firmware v3.5]]).

