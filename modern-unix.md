# Modern Unix

URL
: https://github.com/ibraheemdev/modern-unix

Really handy list of improved shell utilities.  `tldr` is a great one - short and simple example-based man pages.

