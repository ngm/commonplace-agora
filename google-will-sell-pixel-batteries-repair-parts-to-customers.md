# Google Will Sell Pixel Batteries, Repair Parts to Customers

URL
: https://www.vice.com/en/article/93bjva/google-will-sell-pixel-batteries-repair-parts-to-customers

"[[Google]] becomes the latest major manufacturer to soften its stance on [[right to repair]], announcing it will sell [[spare parts]] on [[iFixit]]."

> Google’s parts program is slightly more expansive than that of other manufacturers: Notably, it will sell original replacement [[batteries]], which can have the effect of significantly extending the life of a smartphone. It will also sell cameras, replacement displays, “and more,” the blog post says. 

<!--quoteend-->

> What about extending the security updates for the same phones? 
> 
> The irony in the announcement is that @Google will make spare parts available for phones such as the Pixel 2 and 3 for which it has already discontinued software support
> 
> https://twitter.com/RestartProject/status/1514159570985308164 

