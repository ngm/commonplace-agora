# Extinction Rebellion

> Progressive intellectuals are not the only ones who deny that the climate crisis is political. Extinction Rebellion (XR), one of today’s most prominent environmental movements, argues that climate science speaks for itself and that politics gets in the way of action. The movement thus calls for a “move beyond politics.”20 The result is a denial of politics and a denial of responsibility.
> 
> &#x2013; [[Revolution or Ruin]]

