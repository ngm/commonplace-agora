# The Mechanic and the Luddite

A
: [[book]]

Written by
: [[Jathan Sadowski]]

Found at
: https://www.ucpress.edu/book/9780520398078/the-mechanic-and-the-luddite

Upcoming book from [[Jathan Sadowski]].

Subtitle: "A Ruthless Criticism of [[Technology and Capitalism]]"

