# DECODE

DEcentralised Citizen-Owned Data Ecosystems

> Experiments like DECODE have shown in practice that a different institutional configuration of digital infrastructure, public policy and citizen participation is possible and affordable.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> city projects like DECODE(DEcentralised Citizen-owned Data Ecosystems) provide open source public interest tools for community activities where citizens can access and contribute data, from air pollution levels to online petitions and neighborhood social networks, while retaining control over data shared. 
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

