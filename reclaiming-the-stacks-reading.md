# Reclaiming the stacks: reading

Reading books and other things on ereaders.

Also consider: reading paper books.


## Software/platforms


### Support/build

-   [[koreader]]
-   DRM-free [[epub]]s
-   [[Calibre]]
-   [[Calibre-Web]]


### Appropriate/resist

-   DRMed ebooks
    -   https://github.com/noDRM/DeDRM_tools


### Avoid/regulate

-   anything Amazon-related


## Hardware


### Support/build

-   there's some open-source readers like the [[Open Book]], but they don't have much functionality


### Appropriate/resist

-   koreader installed on a second hand [[Kobo]]
    -   better than a Kindle
-   hacking firmware of big tech ereaders


### Avoid/regulate

-   new ebook readers from big companies


## Labour / supply chains


## Energy

-   How much energy in the construction of an ebook reader?
-   How much energy in the production of epubs?
-   How much energy in the running of the various platforms/computers involved?

