# My information strategy

[[information strategy]]

I find most content to read via the big stream.


## Social reader / big stream

The big stream is basically a bunch of people that I follow.  

I don't use Twitter for my reader.  I use a social reader for this.  I follow people on their personal websites, or on Twitter, and read all these posts in the reader.

I wrote a bit more about how I organise feeds in my reader [here](https://doubleloop.net/2019/10/05/discovery-strategy/).


## Replying / storing

Usually what I see in the big stream are either ideas from someone that I might want to write a reply to, or a link to an article I might want to read later.  I save articles in to Wallabag.  A short reply I'll write there and then, a longer one I will probably save the post into my note-taking app.


## [[Note-taking]]

My fleeting notes I take with either [[org-capture]] or [[orgzly]], depending where I am.  With my literature notes I write in [[org-mode]] and try to do the [[progressive summarisation]] approach.  Permanent notes I use [[org-roam]].

