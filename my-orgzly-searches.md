# My orgzly searches

The searches that I have set up in [[orgzly]].

For reference for me for when I occassionally need to set them up again (when I switched to [[Orgzly Revived]] for example).

-   Agenda
    -   .it.done ad.1 .b.ZSC
-   Next 3 days
    -   .it.done s.ge.today ad.3
-   Scheduled
    -   s.today .it.done
-   To Do
    -   i.todo
-   Morning
    -   b.<sub>GTD</sub> t.morning s.today
-   Shopping
    -   t.shopping

