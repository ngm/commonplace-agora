# Hotel Bar Sessions: Whose Anthropocene?

A
: [[podcast]]

Part of
: [[Hotel Bar Sessions]]

Good discussion - is [[Anthropocene]] a useful term? Maybe [[Capitalocene]] works better? Maybe both have their uses. Good overview of the pros and cons of both.

There's a geological definition of Anthropocene (descriptive), which is interesting and all, but perhaps of more genuine use is it as a definition that motivates us to act to mitigate climate catastrophe (prescriptive).

