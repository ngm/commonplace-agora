# Nature Matters: Systems thinking and experts

URL
: https://www.open.edu/openlearn/nature-environment/nature-matters-systems-thinking-and-experts/

Free course related to systems thinking and the environment.

> **explores conceptual tools for assisting our thinking and deliberation on what matters**. The notion of 'framing' nature is introduced and three readings provide an understanding of **systems thinking for explicitly framing issues of environmental responsibility**.


## Introduction

-   **conceptual tools** for assisting our thinking and deliberation on what matters.

Thinking and deliberating on **what matters**.  That's an interesting idea.  How do we decide what matters?  What does it even mean, 'to matter'?


### Section 1

-   the **notion of 'framing' nature**, raising the perceived paradox of inevitably devaluing an aesthetically pleasing unframed entity.
-   Three further readings, two from [[Fritjof Capra]] and one from Werner Ulrick (all of which are quite short and markedly reduced from their original courses), provide **an understanding of systems thinking for explicitly framing issues of environmental responsibility**.
-   The development of **[[systems literacy]]** (referred to by Capra in terms of **[[ecoliteracy]]** and by Ulrich in terms of **[[critical systems thinking]]**) is explored to counter the sometimes debilitating dualistic positioning on environmental matters alluded to by writers such as Talbott, Light and Higgs amongst many others.


### Section 2

-   focuses more on how **conceptual tools** can help to inform better policy and action regarding environmental matters
-   Here, a reading by Robyn Eckersley critically explores **the importance and limitations of environmental pragmatism for informing policy**.
-   Finally, ideas of **cognitive justice** are explored in a reading by Shiv Visvanathan, who suggests a need for continually developing constructive space between scientific experts and lay experts in order to inform policy and action on what matters that reflects a wider constituency, and that is more specific to eco-cultural circumstances.


## Learning outcomes

-   understand why systems thinking might be useful and know something about how it can be applied in the context of environmental responsibility

**systems thinking in the context of environmental responsibility**

-   describe the significance of **environmental pragmatism** and **cognitive justice** as tools for supporting environmental policy and action.


## Framing nature matters: from language to systems thinking


### 1.1 Framing nature using language tools

> framing, I mean the structures and pre-assumptions that we consciously or unconsciously apply to a situation in order to make sense of it

<!--quoteend-->

> So are there any differences between the way in which we **frame nature in caring for environment** and the way in which we **frame it to provide accountability**?

<!--quoteend-->

> What significance might this have, and what tools might be used to **bridge the responsibilities of caring and accountability**?


#### Caring

> Caring for environment makes manifest the informal aspects of obligations (developing values regarding human and non-human nature) and entitlements (nurturing appropriate relationships amongst humans and between human and non-human nature).


#### Accountability

> Providing accountability for environmental harm focuses on the more formal issues of duties (as codified sets of obligations) and rights (as codified sets of entitlements).

<!--quoteend-->

> Higgs suggests nurturing practitioners in the field of environmental management who are conversant with the languages of both arts and science. 

[[Metaphor]].


#### e.e. cummings poem

[[O sweet sponatenous]]

**What's significance with respect to contemporary issues of climate change?**

-   humanity treats nature as something subordinate
-   a resource from which to take many things
-   nature not being treated with respect
-   with an element of slightly misguided 'care'

<!--listend-->

-   representing nature as separate from humans (the externalist view)
-   and also as something that will rebound by itself


#### Framing devices, semiotics, semantics

> Poetry uses language as a [[framing device]] – a particular way of perceiving the world that makes sense of other symbols and concepts within it and provides meaning to the person using the framing device. The study of the use of words as symbolic representations is known as [[semiotics]], while the way in which words and language ascribe meaning to the objects being represented is the focus of study in [[semantics]].

<!--quoteend-->

> The study of semiotics and semantics, and later [[philosophical pragmatism]], provided an important departure from ideas in mainstream science, **challenging the notion that our framing devices are direct representations of reality**.

<!--quoteend-->

> In contrast to the supposition that reality can be represented in a value-free way, semiotics suggests that **all languages and associated tools for representing reality come with values built in by the users of those language and conceptual tools.**

<!--quoteend-->

> Framing devices should be considered more as tools _towards_ enabling _an_ understanding of reality, helping to generate meaning and purpose in order that we may engage more responsibly with the real world.

<!--quoteend-->

> But **is there not something intrinsic in nature that matters separately from human perception** through framing informed by human values? This question is addressed by Ronald Moore in terms of a framing paradox, which is the subject of the next subsection.


### 1.2 A framing paradox: experiencing nature with cognitive tools

> [[Cognition]] refers to the way in which external information from the environment is processed. 

<!--quoteend-->

> Ronald Moore examines how we engage with, and bring to the foreground, matters of interest regarding nature in terms of aesthetic experiences, perhaps the most highly developed constituent of sentience.

<!--quoteend-->

> Aesthetic values are clearly human-centred; that is, they are subjective rather than objective. 

[[The framing paradox]]

> ‘The framing paradox’ and the poem ‘O sweet spontaneous’ both hint at the variety of ways in which we engage with environmental responsibility, and the purposes of our engagement. They also invoke **a tension between the desire to fully appreciate nature and the desire to make best use of it**.

