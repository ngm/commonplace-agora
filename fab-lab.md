# Fab Lab

> According to a Study by Cedifa (Center for Digital Fabrication), a Fab Lab (fabrication laboratory) can be opened within seven days and a basic investment of only US$5,000 if it relies on commons-oriented approaches, including the use of open source software.
> 
> &#x2013; [[Free, Fair and Alive]]

