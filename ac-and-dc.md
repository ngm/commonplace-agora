# AC and DC

-   You can get DC charging lines for a bunch of existing appliances, they use them in cars.
-   AC transformers step you up from power station, then back down again at other end. Can't (or harder?) to do that with DC.
-   But nowadays you can do much more local microgeneration for which DC is a lot more suitable.

