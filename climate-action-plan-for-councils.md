# Climate Action Plan for Councils

URL
: https://takeclimateaction.uk/take-action/our-campaign

[[Climate action]] for [[councils]].

"A 50-point plan to tackle the climate and nature emergencies and coronavirus recovery."

https://takeclimateaction.uk/sites/default/files/documents/2021-09/Climate_Action_Plan_for_councils_Sep_2021.pdf

> The Climate Action Plan for Councils contains 50 actions that councils can take to make their area more climate and nature friendly, and is split into key areas like:
> 
> -   Protecting our most vulnerable from the effects of climate change.
> -   Ensuring new builds are well insulated and have eco-aware fittings.
> -   Increasing and improving public transportation provisions.
> -   Enabling and supporting the use of renewable energy.
> -   Promoting sustainable consumption in order to to become a zero-waste area
> -   Ensuring everyone has access to green spaces.
> 
> Essentially, the goal of the plan is to ensure that climate and nature restoration goals are front and centre in all decision-making and investments.
> 
> &#x2013; [Climate Action Plan for councils | Climate Action](https://takeclimateaction.uk/download/climate-action-plan-councils)

