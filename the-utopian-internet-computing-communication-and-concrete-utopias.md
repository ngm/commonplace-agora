# The Utopian Internet, Computing, Communication, and Concrete Utopias

An
: [[article]]

URL
: https://www.triple-c.at/index.php/tripleC/article/view/1143

Author
: [[Christian Fuchs]]

Subtitle: Reading William Morris, Peter Kropotkin, Ursula K. Le Guin, and P.M. in the Light of Digital Socialism

[[Digital socialism]].

> What can we learn from literary communist utopias for the creation and organisation of communicative and digital socialist society and a utopian Internet?

<!--quoteend-->

> The paper recommends features of concrete utopian-communist stories that can inspire contemporary political imagination and socialist consciousness.

<!--quoteend-->

> To provide an answer to this question, the article discusses aspects of technology and communication in utopian-communist writings and reads these literary works in the light of questions concerning digital technologies and 21st-century communication. The selected authors have written some of the most influential literary communist utopias. The utopias presented by these authors are the focus of the reading presented in this paper: William Morris’s [[News from Nowhere]], Peter Kropotkin’s [[The Conquest of Bread]], Ursula K. Le Guin’s [[The Dispossessed]], and P.M.’s [[bolo'bolo]] and [[Kartoffeln und Computer]] (Potatoes and Computers). These works are the focus of the reading presented in this paper and are read in respect to three themes: general communism, technology and production, communication and culture. &#x2026; The themes explored include the role of post-scarcity, decentralised computerised planning, wealth and luxury for all, beauty, creativity, education, democracy, the public sphere, everyday life, transportation, dirt, robots, automation, and communist means of communication (such as the “[[ansible]]”) in digital communism. The paper develops a communist allocation algorithm needed in a communist economy for the allocation of goods based on the decentralised satisfaction of needs. Such needs-satisfaction does not require any market. It is argued that socialism/communism is not just a post-scarcity society but also a post-market and post-exchange society.

