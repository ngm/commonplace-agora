# "We can't rely on climate catastrophe to start the revolution" interview with Kai Heron

URL
: https://www.stitcher.com/show/manchester-green-new-deal-podcast/episode/we-cant-rely-on-climate-catastrophe-to-start-the-revolution-interview-with-kai-heron-86930808

Featuring
: [[Kai Heron]]


~00:32:59  [[Capitalist realism]] is no longer a thing. We can imagine plenty of other futures. What we need to do now is figure out how to get to them.


~00:34:30  Do we need somethng that goes a bit beyond secular rationality to pull people in? Something spiritual?


~00:35:38  Or it could be a psychoanalytical thing - based on desire.

