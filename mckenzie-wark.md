# McKenzie Wark

Author.

I came across [[McKenzie Wark]] via the latest issue of [[STIR]].  Her work on the [[commodification of information]] sounds really interesting.  Will have to check out [[A Hacker Manifesto]] and other works.  The identification of the '[[Vectoralist class]]' back in 2003 sounds pretty on point.


## Books

-   [[A Hacker Manifesto]]
-   [[Capital is Dead]]

