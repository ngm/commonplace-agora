# Green imperialism

> Climate change is the new justification for imposing policies on developing countries, policies that benefit capital while dismantling public sectors and impoverishing populations. The capitalist response to climate change is an intensified predatory green imperialism. It’s capitalism as civilizational collapse.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

