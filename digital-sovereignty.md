# Digital sovereignty

[[Digital]] [[Sovereignty]].


## A blueprint for digital sovereignty

From [[Against Digital Colonialism]].


### Public education for digital emancipation

[[Public education]] for [[digital emancipation]].

> The first point is to encourage the development of ecosystems of digital skills beyond basic coding. One of the areas of strategic intervention to prevent digital colonialism is education and access to knowledge that can create active participation in shaping the digital society.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> It is imperative to develop an education system that explains how technology is made, and how technology could be shaped to reflect other sets of values.
> 
> &#x2013; [[Against Digital Colonialism]]


### Public procurement to change the rules of the game and decentralise tech power

> When imagining a blueprint for the future, governments aiming at gaining back control of vital infrastructure should put public interest first when assessing new investments in technology.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> They should invest in their own infrastructure to transmit data, at least for government information and also information of strategic sectors.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> They should prioritise and create incentives for the creation of regional data centres and for local developers and local industries to provide services and equipment.
> 
> &#x2013; [[Against Digital Colonialism]]

I like this one.

> Furthermore, governments should invest more and better in decentralised platforms and services to provide citizens with a basic [[participatory infrastructure]].
> 
> &#x2013; [[Against Digital Colonialism]]

And this one.

> They should also pass comprehensive legislation to open all black boxes, prioritising sustainability and adaptability of the systems they deploy.
> 
> &#x2013; [[Against Digital Colonialism]]

[[Free software]], basically?

> Projects such as free laptops and large infrastructure projects should be evaluated as to whether they are giving more to a country than they are receiving in terms of data power. 
> 
> &#x2013; [[Against Digital Colonialism]]


### A different era of digital cooperation

> intergovernmental cooperation in [[public sector free software]] could grow and lead to the creation of pools of code, optimising investment and resilience
> 
> &#x2013; [[Against Digital Colonialism]]

[[Municipal FOSS]]

> The focus should be on developing local capacities inside public administrations and tech teams able to solve problems
> 
> &#x2013; [[Against Digital Colonialism]]

[[How would you include free software in community wealth building?]]

