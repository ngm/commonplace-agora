# Glamorous Toolkit

URL
: https://gtoolkit.com/

The 'testimonials' have my interested piqued:

> The first software system that I can seriously imagine to replace Emacs for me. Given that I practically live in Emacs, that’s not a small statement.
> 
> @khinsen

<!--quoteend-->

> GToolkit.com is the only environment that we think might be better for thinking in than us.
> 
> @RoamResearch

