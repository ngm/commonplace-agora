# Standard deviation and interquartile range

-   [Measures of Spread | How and when to use measures of spread | Laerd Statistics](https://statistics.laerd.com/statistical-guides/measures-of-spread-range-quartiles.php)
-   [What are some important differences between standard deviation and interquart&#x2026;](https://discuss.codecademy.com/t/what-are-some-important-differences-between-standard-deviation-and-interquartile-range/359402)
-   [Mean and standard deviation versus median and IQR (video) | Khan Academy](https://www.khanacademy.org/math/ap-statistics/summarizing-quantitative-data-ap/measuring-spread-quantitative/v/mean-and-standard-deviation-versus-median-and-iqr)

