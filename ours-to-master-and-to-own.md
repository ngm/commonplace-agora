# Ours to Master and to Own

URL
: 

https://www.haymarketbooks.org/books/366-ours-to-master-and-to-own

Subtitle
: Workers' Control from the Commune to the Present

[[Workers' control]]

[[unions]], [[strikes]] and [[coops]]

> From the dawning of the industrial epoch, wage earners have organized themselves into unions, fought bitter strikes, and gone so far as to challenge the very premises of the system by creating institutions of democratic self-management aimed at controlling production without bosses. With specific examples drawn from every corner of the globe and every period of modern history, this pathbreaking volume comprehensively traces this often underappreciated historical tradition. Ripe with lessons drawn from historical and contemporary struggles for workers’ control, Ours to Master and to Own is essential reading for those struggling to create a new world from the ashes of the old.

