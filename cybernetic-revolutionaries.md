# Cybernetic Revolutionaries

A
: [[book]]

Author
: [[Eden Medina]]

Primarily on the topic of [[Project Cybersyn]].  With plenty of good [[STS]] reflections along the way.

> this connection between cybernetics and Chilean socialism came about, in part, because Beer and Popular Unity, as Allende’s governing coalition was called, were exploring similar concepts, albeit in the different domains of science and politics

<!--quoteend-->

> Chile was not able to implement its political dream of democratic socialism or its technological dream of real-time economic management

<!--quoteend-->

> major theme in Beer’s writings was finding a balance between centralized and decentralized control, and in particular how to ensure the stability of the entire firm without sacrificing the autonomy of its component parts.

<!--quoteend-->

> how do you create a system that can maintain its organizational stability while facilitating dramatic change, and how do you safeguard the cohesion of the whole without sacrificing the autonomy of its parts

<!--quoteend-->

> brought together ideas from across the disciplines—mathematics, engineering, and neurophysiology, among others—and applied them toward understanding the behavior of mechanical, biological, and social systems.23

<!--quoteend-->

> for Beer, cybernetics became the “science of effective organization

<!--quoteend-->

> Keller instead argues that the cybersciences also emerged as a way to embrace complexity and “in response to the increasing impracticality of conventional power regimes

<!--quoteend-->

> He embraced complexity, emphasized holism, and did not try to describe the complex systems he studied, biological or social, in their entirety

<!--quoteend-->

> To put it another way, Beer was more interested in studying how systems behaved in the real world than in creating exact representations of how they functioned

-   [[There are different strands of cybernetics]]

