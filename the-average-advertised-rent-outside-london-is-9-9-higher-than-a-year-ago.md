# The average advertised rent outside London is 9.9% higher than a year ago

[[the year of the squeeze]]

as tenants making plans for a post-pandemic life jostle for properties 

[Private rents in Britain rise at fastest rate on record | Renting property | &#x2026;](https://www.theguardian.com/money/2022/jan/27/private-rents-in-britain-rise-at-fastest-rate-on-record)

