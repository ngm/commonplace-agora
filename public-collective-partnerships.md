# Public-collective partnerships

Same as? or very similar to, [[public-commons partnerships]].

-   accelerators of the commons

> In Public-Collective Partnerships (PCPs) a collective of citizens, sometimes as a cooperative, works together on an equal footing with public institutions such as a municipality in the management of local facilities or in goods and services provision.
> 
> &#x2013; [[Public-civic collaboration, coop incubators and stewarding digital infrastructure as a commons]]

Examples:

-   [[Stadtwerke Wolfhagen]]
-   a Chilean cleaning services cooperative - didn't catch the name, but possibly the one in [[Recoleta]]

Similar idea to [[Community wealth building]].

Interesting comment in the Q&amp;A: that [[Doughnut Economics]] is good, but depoliticised.  Which in some ways presumably makes it easier for governmental authorities to adopt something like that.

