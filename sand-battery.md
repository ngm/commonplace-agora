# Sand battery

A type of [[energy storage]].  Seems like a good one, if the primary constituent is sand.  Better than [[lithium]].

> Using low-grade sand, the device is charged up with heat made from cheap electricity from solar or wind.
> 
> The sand stores the heat at around 500C, which can then warm homes in winter when energy is more expensive.
> 
> &#x2013; [Climate change: 'Sand battery' could solve green energy's big problem](https://www.bbc.co.uk/news/science-environment-61996520)

That said, its storing heat, not electricity?  So not exactly a battery.

> Low-cost electricity warms the sand up to 500C by resistive heating (the same process that makes electric fires work).
> 
> This generates hot air which is circulated in the sand by means of a [[heat exchanger]].
> 
> &#x2013; [Climate change: 'Sand battery' could solve green energy's big problem](https://www.bbc.co.uk/news/science-environment-61996520)

<!--quoteend-->

> Sand is a very effective medium for storing heat and loses little over time. The developers say that their device could keep sand at 500C for several months.
> 
> So when energy prices are higher, the battery discharges the hot air which warms water for the [[district heating]] system which is then pumped around homes, offices and even the local swimming pool.
> 
> &#x2013; [Climate change: 'Sand battery' could solve green energy's big problem](https://www.bbc.co.uk/news/science-environment-61996520)

