# Open-value cooperatives

An extension of open cooperatives by [[DisCO]].

> Open-value cooperatives apply the logic of feminist economists like Marilyn Waring, to account for the care work vital to human prosperity and survival. They also acknowledge the value of intersectional approaches, currently ignored through the misleading narratives of our harmful economic systems.
> 
> &#x2013; [[DisCO Manifesto]] 

