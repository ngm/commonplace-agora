# The DisCO Elements

-   https://elements.disco.coop/

[[DisCOs]]

> the authors invite us to create templates for an economy that is based on equality, inclusive tech, collective  care  for  humans  and  the  planet  and  intelligent  ways  of  value  accounting.

<!--quoteend-->

> DisCO is about activism, not theorism – and we have to start with ourselves

<!--quoteend-->

> For all their disruption, the one thing Silicon Valley has not touched is the ownership and governance structure of a typical capitalist firm.

<!--quoteend-->

> DisCOs bring the self determination of technological disruption and its affordances to the space where many are expected to spend a third of their life: the workplace.

<!--quoteend-->

> According to mainstream economics, businesses are drivers of a process of enclosure, whereby resources are turned into commodities and relationships into services. DisCOs reverse this trend by actively generating decommodified, open-access resources. These commons can be digital (e.g., code, design, documentation, legal protocols and best practices) or physical (e.g., productive infrastructure, deliberation spaces, machinery).

<!--quoteend-->

> Corporations extract resources as if they were infinitely abundant, while restricting immaterial flows of knowledge, usually reproducible at marginal cost, through intellectual property laws and patents. Conversely, DisCOs support and provide a business model for the Design Global, Manufacture Local template.


## Principles

I like how the principles are tied to existing organisations and what they're doing.

[[Cooperation Jackson]] are one of the groups.

1.  Values-based accountability
2.  Building whole-community governance
3.  Active creators of commons
4.  Rebalancing the scale: rethinking global/local economics
5.  Care work is the core
6.  Reimagining the origins and flows of value
7.  Primed for federation

The split into livelihood work, love work, and care work is useful.

> Rather than big state or corporate solutions, social and environmental change is brought to life from below by federated collectives which actively educate in post- capitalist, feminist economics. Volunteer work is no longer ignored or undervalued, rather it is factored into the value equation.

How do DisCOs interact with the state?

Later on (Chapter 7):

> Additionally, the objective is for the State to provide the necessary infrastructure to empower and protect the creation and upkeep of DisCOs, but not direct the process of federated DisCO development.


## Chapter 4: Take your time, do it right: commons governance

> This type of share-holding is in contrast to that found in a corporation. While shareholders in a corporation accrue power through money, the DisCO model treats power differently. DisCOs value forms of power, understood as “shared capacity to act” and “collective strength” centered around work undertaken for the commons.

<!--quoteend-->

> A corporation employs wage labor to produce profit-maximizing commodities through privately owned and managed productive infrastructures. By contrast, DisCOs work together for social and environmental purposes while also creating commons and building community, locally and/or globally. The model allows members to choose to do work that they consider value-aligned, and therefore, worthwhile. This is how DisCOs model a practice of economic resistance.

<!--quoteend-->

> all members can gain income for both types of productive work, whether pro-bono or paid for by a client.

^ seems like that could be a good model for free software contributions.  Explicitly valueing of pro-bono work.  Not necessarily new to DisCOs, plenty of tech coops doing that already I think. 


## Chapter 5: The DisCO CAT and DisCO-Tech

> The DisCO CAT is a way to make the latter more fun and make sure everyone’s heard, that tech design is not led by investors or otherwise narrowly focused actors, and that our privacy, autonomy and the right to use our own energy and creativity towards socially and environmentally restorative ends is upheld and maintained with care.


## Chapter 6: Care before Code

>   Care always trumps “the numbers” in the end. Once
> we take pains to meet all fiscal/legal obligations, we’re left with human considerations and these take precedence. The DisCO Governance model and tech in development include ways to gift to others. Beyond that, we are also exploring the concept of UnDisCO: What happens when the collective lets go of protocols and ratios, and just shares based on abilities and needs?


## Chapter 7: DisCO Futures: Building Tracks


## Notes

I feel like the high-level values are excellent, but there's a lot of low-level nitty-gritty required.  Which of course makes sense, but can be lost when reading the top-level overview.

Much more of that fine grain seems to be present in the [[Guerrilla Media Collective]] [wiki](https://wiki.guerrillamediacollective.org/Clockify).

-   Time-tracking seems to be an important part of DisCOs.  Or at least [Guerilla Media Collective's implementation](https://wiki.guerrillamediacollective.org/Clockify) of it.  I seem to have an ingrained antipathy towards fine-grained time-tracking so it'll be good to read about its utility here - the main one being transparency and ensuring all work is recognised I think.
-   > tracking and revealing the often invisible, even dismissed, strands of value-producing labor is structurally different from purely quantifying work.

DisCO Elements is gorgeous and fun and uplifting, with a high-level manifesto and outlook that I am totally onboard with.  And that seems backed up by lots of practical knowledge and research based on daily life in Actually Existing Organisations.

I don't have any experience of working in a cooperative so can't speak to how different / novel / practical it is there.  But I love the focus on recognising and actively valuing care work and work that gives back to the commons.

You don't get a **huge** amount of the nitty gritty in this.  You need to delve in to the [Guerrilla Media Collective wiki](https://wiki.guerrillamediacollective.org/) and [DisCO Mothership](https://mothership.disco.coop/) for that I think.

