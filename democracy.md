# Democracy

-   Division of powers
-   Respect for peaceful protest
-   An independent judiciary
-   Building cross-party consensus
-   Winning arguments
-   Being held to account

