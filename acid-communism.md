# Acid communism

A focus on [[the role of culture in politics]]. [[Consciousness raising]].

Coined by [[Mark Fisher]] (with some input from Jeremy Gilbert? see [[#ACFM Trip 1: Out of the Box]])

> acid communism is not so much a political or economic programme as an alternative outlook offered to those wishing to break the grip of [[capitalist realism]] on our imaginations
> 
> &#x2013; [[Long Read Review: k-punk: The Collected and Unpublished Writings of Mark Fisher (2004-2016)]]

See [[#ACFM Trip 1: Out of the Box]] for a bit of discussion.

