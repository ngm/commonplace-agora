# What if it's a big hoax and we create a better world for nothing?

[[Climate change]].

> dx@social.ridetrans.it - The thing that drives me nutso about climate change is that many of the necessary changes could be awesome. A society with more leisure time, cleaner air, closer connections to friends and family who live nearer to you, more exercise, more technological innovation, more jobs, more fairness, more nature… most people would benefit from these changes.
> 
> &#x2013; https://social.ridetrans.it/@dx/109960190468821360

