# Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism

Also available as: [[pdf]] and [[odt]].

<div class="abstract">
  <div></div>

This research investigates the role that information and communication technology workers can play in a transition to an environmentally stable and socially equitable society.  Against a context of ICT's role in society, and the dominance of the capitalist mode of production in ICT, I explore alternative approaches in ICT and how they can contribute to an ecosocialist political programme.  I provide evaluative criteria for how ICT workers can engage in and support initiatives to help transition to these alternatives.

</div>


## Introduction

In the final synthesis of the sixth assessment report from the Intergovernmental Panel on Climate Change, the world's scientists presented a stark final warning to humanity: we are destroying the planet, and we need 'deep, rapid, and sustained' reduction in emissions in order to avoid irreversible loss and damage to nature and people ({IPCC}, 2023). Along with the report, UN Secretary-General António Guterres stated the need for climate action 'by every country and every sector and on every timeframe'({UN Press}, 2023). For those of us that work in the ever-growing sector of information and communication technology (ICT) we face the question: what action should we take?

The IPCC is unequivocal as to the cause of the crises of our times: human activity ({IPCC}, 2023 p. 4). We are altering the planet to such a degree that the change has been proposed as its own geological epoch: the Anthropocene (Syvitski, Jaia and Waters, Colin N. and Day, John and Milliman, John D. and Summerhayes, Colin and Steffen, Will and Zalasiewicz, Jan and Cearreta, Alejandro and Ga{\\l}uszka, Agnieszka and Hajdas, Irka and Head, Martin J. and Leinfelder, Reinhold and McNeill, J. R. and Poirier, Cl{\\'e}ment and Rose, Neil L. and Shotyk, William and Wagreich, Michael and Williams, Mark, 2020). Others hone this further, asserting that it is not the presence of humans per se that is the cause, but the very particular configuration of our recent social history: capitalism and growth (Hickel, Jason, 2020). We are in the Capitalocene, and only an alternative to capitalism will get us out of it (Moore, Jason W., 2017).

In all of this, one of the sectors with a major role to play is ICT. Computing and communication devices are integral to our society - from our phones to our computers to the servers and global infrastructure that networks them together. ICT has a significant environmental impact of its own through the lifecycle of its physical devices (Freitag, Charlotte and {Berners-Lee}, Mike and Widdicks, Kelly and Knowles, Bran and Blair, Gordon S. and Friday, Adrian, 2021), but also an enabling and structural effect on the impact of other sectors and society in general (Hilty, Lorenz M. and Aebischer, Bernard, 2015).

With such impact, ICT has the potential to be a liberatory technology at scale. Yet, much of ICT has been captured for capitalist ends. The \`Big Tech' corporations are some of the largest capitalist structures in the world. They represent the majority of companies to have ever attained over $1 trillion in market capitalization (Monica, Paul R. La, 2021).  The result is "an intensification of labour and environmental exploitation on a planetary scale" (Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel, 2022)[^fn:1]. The paradigm of digital capitalism needs to be undone if we are to stand a chance of combatting climate change. What can challenge its dominance?

This research investigates what an ecosocialist programme for ICT could look like, and how ICT workers can contribute to this programme.


## Sustainable development?

Sustainable development and the sustainable development goals are well-known organising principles that aim to put people and planet first ({United Nations}, 2015). Sustainable development is an approach to economic activity that explicitly recognises the interdependence of environmental and socio-economic issues, and the need to move beyond an economy that grows without considering the limits of the planet.

Hopwood, Bill and Mellor, Mary and O'Brien, Geoff (2005) acknowledge the positive direction of sustainable development's core messages, but criticise the lack of programmatic definition of how to achieve its goals. They map out various existing approaches to organising society and the economy, assessing their strength on the dual axes of environmental concerns and socio-economic equity. Depending on the strength for each of these concerns, the approaches are grouped into three areas of sustainable development - Status Quo, Reform and Transformation.  Figure fig:sdmap shows this mapping.

![[Ecosocialism_and_ICT/2023-05-08_20-32-56_screenshot.png "Mapping of sustainable development approaches (from Hopwood, Bill and Mellor, Mary and O'Brien, Geoff, 2005)."]]

_Status Quo_ approaches ignore the urgency of the crises, asserting that a strategy of \`business as usual' is sufficient, and that a continued pursuit of economic growth and technological development through markets will eventually resolve them. _Reform_ approaches believe that change is needed, but that such change can happen slowly over time, through gentle reforms to markets and government policies. _Transformation_ approaches are more radical, asserting that the solution lies only within deep societal transformation away from growth and capitalism.

Hopwood, Bill and Mellor, Mary and O'Brien, Geoff (2005) argue that status quo approaches have caused the current crises, and that reform alone will not be effective in the short window of time that we have. They argue that approaches based on transformation are essential, backed by pragmatic short-term reforms to facilitate transition.[^fn:2]  There is a substantial body of work making the detailed case for transformation (e.g. Klein, Naomi, 2014,  Hickel, Jason, 2020), with the global consensus of the IPCC being the latest, most mainstream addition to this.


### Transformative alternatives

What transformative approaches to sustainable development do we have? Hopwood, Bill and Mellor, Mary and O'Brien, Geoff (2005) situate a number of approaches within the transformation group: anti-capitalist and environmental justice movements; social ecology; ecofeminism; ecosocialism; and indigenous movements (top right of figure fig:sdmap). All contain important elements of transformative practice, and between them “there is a constant interchange of ideas and cross-fertilization” (Hopwood, Bill and Mellor, Mary and O'Brien, Geoff, 2005 p. 10).  Albert, Michael J. (2022) describes ecosocialism as the foremost present post-capitalist alternative, situating ideas such as degrowth and ecofeminism within it.  As such, in this research I will focus on ecosocialism, while recognising that similar transformative tendencies exist in and around it.

Ecosocialism is a political programme that melds socialist and environmental politics, with a strong anti-colonial and anti-capitalist sentiment.  Its tenets move beyond private profit to build forces and relations of production that are both ecologically-minded and socially-useful (Brownhill, Leigh, 2022). 


## ICT, capitalism, and sustainability

ICTs throughout history have had a significant impact on society, affecting politics, economy, labour, production, consumption and resource use (Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius, 2022).  This impact continues to this day, with the present incarnation of digital ICT ("digitalization") having a huge impact on the world ({The World in 2050}, 2019).

ICT is an industry that spans many layers of society. In a traditional, technical sense, it is a combination of software, hardware and communications infrastructure stacked upon one another. Broader definitions of ICT range philosophically further, viewing it as an ecology of design, materials, humans, nonhumans, values, politics, and more (e.g. Bratton, Benjamin H. (2015)). ICT facilitates the activities of much of modern society, even when viewed in a purely technical sense.  Viewed in the wider sense, ICT and digitalization is all-encompassing of everyday life.


### Capitalist ICT

Like any technology, embedded in society as they are, the structures of ICT can never be value-neutral (Hare, Stephanie, 2022).  They are imbued with a particular political economy by a particular social context.

Over the years, digital ICT has moved from a starting position of public good to one of private ownership (Tarnoff, Ben, 2022).  This enclosure has continued to such an extent that the consolidation of the ICT stack by Big Tech corporations represents the current apex of capitalism (Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel, 2022,  Wark, McKenzie, 2019).  ICT companies represent the majority of companies to have ever attained over $1 trillion in market capitalization (Monica, Paul R. La, 2021).  Google, Amazon, Facebook, Apple and Microsoft control vast tracts of society through software, hardware, networks, supply chains and manufacturing. They "span much of the terrain of ordinary experience" (Greenfield, Adam, 2017 p. 275), "mediate and monetize everyday life to the maximum possible extent" (Greenfield, Adam, 2017 p. 238).  They have resulted in “an intensification of labour and environmental exploitation on a planetary scale” (Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel, 2022 p. 12).  Capitalistic forms of ICT currently dominate the world.

If we accept that the stacks of Big Tech dominate capitalism, and that capitalism is negatively impacting nature and society, then as concerned ICT workers we should try to reclaim the stacks - engaging with and promoting alternatives to capitalist ICT that respect society and the environment.


### ICT for sustainability

The field of ICT for sustainability (ICT4S) explores the principles of sustainable development as applied to ICT (Hilty, Lorenz M. and Aebischer, Bernard, 2015). Yet, as with sustainable development as a whole, there are various approaches to ICT4S, and the majority either sustain the status quo or offer only mild reforms (Santarius, Tilman and Wagner, Josephin, 2023). They tend to focus on a notion of digital 'efficiency' (focusing on the operating efficiency of ICT, while still allowing its growth overall to continue) and ignore the concept of digital 'sufficiency' (the reduction of demand), and to ignore higher-level questions of structural transformation (Santarius, Tilman and Wagner, Josephin, 2023 p. 2).  Much more transformative strategies are needed to make the changes that we need in the timeframe that we have (Hopwood, Bill and Mellor, Mary and O'Brien, Geoff, 2005).  What could an ecosocialist ICT look like?


## An overview of ecosocialist ICT

A number of contemporary works have presented programmes for an ecological or socialist ICT (Lovink, Geert, 2020,  Schneider, Nathan, 2022,  Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius, 2022,  Muldoon, James, 2022,  Tarnoff, Ben, 2022,  Kwet, Michael, 2022,  Medina, Eden, 2014,  Smith, Hannah, 2022,  Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel, 2018,  Kleiner, Dmytri, 2010)).  In this section I review these through the lens of ecosocialism, looking for common principles, example initiatives and evaluative criteria that are contained within them.

An initiative is an existing, real-word example of an ecosocialist ICT that an ICT worker could support today. By outlining example initiatives, and identifying different criteria with which they can be evaluated, as ICT workers we can identify initiatives which are the most opportune for us to contribute to.


### Criteria for evaluating initiatives

We can evaluate initiatives for an ecosocialist ICT against a number of different criteria.  Knowing: the broad _domain of ecosocialism_; the _layer of the stack_; the _type of action_; and the level of _radicalism_ involved; can help identify which initiatives fit best with a particular ICT worker's skillsets and capacity.


#### Domain of ecosocialism

Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius (2022) outline three domains: planetary stability; social equity; and agency. _Planetary stability_ refers to issues such as energy, emissions, material use, land demand, and in general, the recognition of planetary boundaries and the need to stay within them. _Social equity_ refers to the fair distribution of benefits (and disbenefits) associated with digital technologies. Similarly, Smith, Hannah (2022) references the concepts of social foundations and planetary boundaries in an ICT-focused interpretation of the well-known framework of Doughnut Economics (Raworth, Kate, 2017).  _Agency_ refers to ownership and control and the ability to participate. The stewards of a technology should be the people who use and are affected by the technology. Schneider, Nathan (2022) identifies sovereignty and democracy as key aspects of agency.


#### Layer of the stack

Difference initiatives relate to different layers of the technological stack.
For example, 'down the stack', at the layer of the network, Tarnoff, Ben (2022) advocates for publicly and cooperatively owned networks such as community broadband and community mesh networks.  'Up the stack', at the layer of software and platforms, Tarnoff advocates for the protocolization of social media, highlighting alternative social media initiatives such as the Fediverse[^fn:3] and for platform cooperativism - the worker control and ownership of platforms. Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius (2022) discuss initiatives at the hardware layer, such as legislation against planned obsolescence.


#### Type of action / point of leverage

Initiatives fall into different types of action and the leverage points that they work with. Muldoon, James (2022) outlines the notions of _resist_, _regulate_ and _recode_.   Resistance is acts that directly confront the status quo, such as unionisation, strikes, protest and direct action.  Regulation is the advocacy for laws introduced by a state actor.  Recoding is the active building of alternatives, referencing the notion of Erik Olin Wright's concept of 'real utopias' (Wright, Erik Olin, 2010).  Similarly, Kwet, Michael (2022) discusses a mixture of alternative-building, use of regulation, and direct action. Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius (2022) observe that digitalisation can be well applied to the key leverage points in a system (rules and feedback, structures, goal setting and mindset shifts) as outlined by Meadows, Donella H. and Wright, Diana (2008).


#### Level of radicalism

The various programmes discuss initiatives with differing degrees of radicalism (the desire to break with the continuity of existing institutions). Some ICT workers may be more comfortable participating in initiatives that promote (non-reformist) reforms to existing structures, whereas others may be willing/able to participate in more radical action.

As seen, Hopwood, Bill and Mellor, Mary and O'Brien, Geoff (2005) give us a spectrum of status quo, reform and transformation.  Schneider, Nathan (2022) describes a criteria for governable stacks of "insurgency" - a direct defiance against attempts at rule. Kwet, Michael (2022) promotes an explicitly anti-colonialist digital ecosocialism, and calls for direct actions such as boycott, divestment and sanctions. Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel (2018) call for technology appropriation.


### General principles

Across the programmes, there are a number of general principles repeated.  These are _open knowledge_ (such as libre software and creative commons); _cooperativism_ (such as platform coops and tech coops); _commoning_ (such as data commons and knowledge commons); _degrowth_ (such as concepts of digital sufficiency); _socially useful / ecologically safe production_; _adversarial interoperability_; _municipal support_ (such as community wealth building and public-commons partnerships); _innovation from below_ (such as design justice); and _socialisation / deprivatisation_ (of physical and digital ICT infrastructure). These general principles can be used to identify an ICT initiative as ecosocialist.


## What can ICT workers do?

As outlined by Albert, Michael J. (2022), ecosocialists must go beyond simply defining a wish list of things that we want to achieve, and must critically discuss how we might go about achieving them.  In our domain, 
it is important to focus on steps of transition from digital capitalism to digital ecosocialism.

For the ICT worker, one approach to this is participating in praxis that supports initiatives of ecosocialist ICT.  Based on the criteria above, an ICT worker can find initiatives that lie close to intersection of their expertise and interests. The classic socialist slogan is to "educate, agitate and organise."  For any given initiative, an ICT worker can support it by learning about it; sharing it and teaching it to others; and directly contributing skills or financial support to the initiative.


### Example initiatives

For example, an ICT worker might identify the problem of early obsolescence and short upgrade cycles as a problem they wish to challenge, where devices such as laptops, tablets and smartphones are consumed at an unsustainable rate, due to aggressive policies of manufacturers in pursuit of growth, leading to e-waste, carbon emissions and labour exploitation.

This problem falls into the domain of _Planetary stability_, at the _Hardware_ layer.  Various initiatives might contribute to an ecosocialist response to this problem.  Campaigning for right to repair policy[^fn:4] aims for non-reformist _Reform_ through acts of _Regulation_.  Initiatives such as Fairphone[^fn:5] or Framework[^fn:6] are attempts to _Recode_ the way in which devices are produced, with fairer supply chains and repairable designs.  The reverse engineering and distribution of schematics of devices to enable repair would be a more _Transformative_ act of _Resistance_.

Another ICT worker might be concerned at the practices of social media platforms, driving addiction through the attention economy in the pursuit of advertising revenue.  Alternative social media initiatives such as the Fediverse aim to _Recode_ social media giving users more agency.  Initiatives such as Nitter[^fn:7] _Resist_ the hegemony of the big tech social media platforms through adversarial interoperability.  Campaigns to enforce network interoperability are a radical form of _Regulation_, whereas anti-trust regulation is reformist (Kwet, Michael, 2022).

Dependent on their skillsets, the ICT workers can choose to support these initiatives as best fits them.


## Conclusion

In order to prevent social and environmental disaster, all sectors of society need to engage in a rapid change towards sustainability.  ICT, with both a direct and indirect effect on emissions and societal activity, is integral in this.  Ecosocialist ICT is an alternative to capitalist ICT, and as concerned ICT workers we can contribute to the necessary transition by supporting ecosocialist ICT initiatives.

A programme of ecosocialist ICT combines resistance and regulation with the building of alternatives. Many building blocks and blueprints are here already: initiatives such as libre software and creative commons; platform coops and tech coops; federated and democratically governed social media platforms; data commons and data trusts; strike actions, worker empowerment and campaigns against exploitative practices; the deconstruction of intellectual property laws; regulation against monopolistic practices and early obsolescence; and many more.

We as ICT workers can contribute to a transition by building and supporting these initiatives.  In this research I have reviewed existing programmes and presented a set of criteria for ICT workers to pick out initiatives that they can best support.  Big Tech will not be challenged by maintaining the status quo; as concerned ICT workers we need to urgently agitate, organise and educate to foster transformative initiatives for an ecosocialist ICT.


### Future work

This research has looked at initiatives in relative isolation from one another.  It is important to also recognise the interconnectedness of these initiatives and to investigate how they interact in a broader strategy.  Future work could engage with systems thinking to do this.

A struggle for ecosocialist ICT is only one facet of an ecosocialist strategy.  Future work should investigate the connection points of ecosocialist ICT to the wider strategy of ecosocialist transition - for example, how it could support and be part of an ecosocialist Green New Deal.


## References

Albert, Michael J. (2022). _Ecosocialism for {{Realists}}: {{Transitions}}, {{Trade-Offs}}, and {{Authoritarian Dangers}}_, Capitalism Nature Socialism.

Bratton, Benjamin H. (2015). _The Stack: On Software and Sovereignty_, {MIT Press}.

Creutzig, Felix and Acemoglu, Daron and Bai, Xuemei and Edwards, Paul N. and Hintz, Marie Josefine and Kaack, Lynn H. and Kilkis, Siir and Kunkel, Stefanie and Luers, Amy and {Milojevic-Dupont}, Nikola and Rejeski, Dave and Renn, J{\\"u}rgen and Rolnick, David and Rosol, Christoph and Russ, Daniela and Turnbull, Thomas and Verdolini, Elena and Wagner, Felix and Wilson, Charlie and Zekar, Aicha and Zumwald, Marius (2022). _Digitalization and the {{Anthropocene}}_, Annual Review of Environment and Resources.

Engler, Mark and Engler, Paul (2021). _Andr\\'e {{Gorz}}'s {{Non-Reformist Reforms Show How We Can Transform}} the {{World Today}}_.

Freitag, Charlotte and {Berners-Lee}, Mike and Widdicks, Kelly and Knowles, Bran and Blair, Gordon S. and Friday, Adrian (2021). _The Real Climate and Transformative Impact of {{ICT}}: {{A}} Critique of Estimates, Trends, and Regulations_, {Elsevier}.

Greenfield, Adam (2017). _Radical Technologies: The Design of Everyday Life_, {Verso}.

Hare, Stephanie (2022). _Technology Is Not Neutral: A Short Guide to Technology Ethics_, {London Publishing Partnership}.

Hickel, Jason (2020). _Less Is More: How Degrowth Will Save the World_, {William Heinemann}.

Hilty, Lorenz M. and Aebischer, Bernard (2015). _{{ICT}} for {{Sustainability}}: {{An Emerging Research Field}}_, {Springer International Publishing}.

Hopwood, Bill and Mellor, Mary and O'Brien, Geoff (2005). _Sustainable Development: Mapping Different Approaches_, Sustainable Development.

{IPCC} (2023). _{{AR6 Synthesis Report}}: {{Summary}} for {{Policymakers Headline Statements}}_.

Kleiner, Dmytri (2010). _The {{Telekommunist Manifesto}}_.

Klein, Naomi (2014). _This Changes Everything: Capitalism vs. the Climate_, {Simon \\&amp; Schuster}.

Kwet, Michael (2022). _Digital {{Ecosocialism}}: {{Breaking}} the Power of {{Big Tech}}_.

Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel (2022). _The {{Stack}} as an {{Integrative Model}} of {{Global Capitalism}}_, tripleC: Communication, Capitalism \\&amp; Critique. Open Access Journal for a Global Sustainable Information Society.

Likav{\\v c}an, Luk{\\'a}{\\v s} and {Scholz-W{\\"a}ckerle}, Manuel (2018). _Technology Appropriation in a De-Growing Economy_, Journal of Cleaner Production.

Lovink, Geert (2020). _Principles of {{Stacktivism}}_, tripleC: Communication, Capitalism \\&amp; Critique. Open Access Journal for a Global Sustainable Information Society.

Meadows, Donella H. and Wright, Diana (2008). _Thinking in Systems: A Primer_, {Chelsea Green Pub}.

Medina, Eden (2014). _Cybernetic Revolutionaries: Technology and Politics in {{Allende}}'s {{Chile}}_, {MIT Press}.

Monica, Paul R. La (2021). _The Race to \\$3 Trillion: {{Big Tech}} Keeps Getting Bigger | {{CNN Business}}_, CNN.

Moore, Jason W. (2017). _The {{Capitalocene}}, {{Part I}}: On the Nature and Origins of Our Ecological Crisis_, The Journal of Peasant Studies.

Muldoon, James (2022). _Platform Socialism: How to Reclaim Our Digital Future from Big Tech_, {Pluto Press}.

Brownhill, Leigh (2022). _The {{Routledge}} Handbook on Ecosocialism_, {Routledge, Taylor \\&amp; Francis Group}.

Raworth, Kate (2017). _Doughnut Economics: Seven Ways to Think like a 21st-Century Economist_, {Random House Business Books}.

Santarius, Tilman and Wagner, Josephin (2023). _Digitalization and Sustainability: {{A}} Systematic Literature Analysis of {{ICT}} for {{Sustainability}} Research_, GAIA - Ecological Perspectives for Science and Society.

Schneider, Nathan (2022). _Governable {{Stacks}} against {{Digital Colonialism}}_, tripleC: Communication, Capitalism \\&amp; Critique. Open Access Journal for a Global Sustainable Information Society.

Smith, Hannah (2022). _What Is the Digital Tech Doughnut?_, Doing the Doughnut Tech.

Syvitski, Jaia and Waters, Colin N. and Day, John and Milliman, John D. and Summerhayes, Colin and Steffen, Will and Zalasiewicz, Jan and Cearreta, Alejandro and Ga{\\l}uszka, Agnieszka and Hajdas, Irka and Head, Martin J. and Leinfelder, Reinhold and McNeill, J. R. and Poirier, Cl{\\'e}ment and Rose, Neil L. and Shotyk, William and Wagreich, Michael and Williams, Mark (2020). _Extraordinary Human Energy Consumption and Resultant Geological Impacts Beginning around 1950 {{CE}} Initiated the Proposed {{Anthropocene Epoch}}_, {Nature Publishing Group}.

Tarnoff, Ben (2022). _Internet for the People: The Fight for Our Digital Future_, {Verso}.

{The World in 2050} (2019). _The {{Digital Revolution}} and {{Sustainable Development}}: {{Opportunities}} and {{Challenges}}. {{Report}} Prepared by {{The World}} in 2050 Initiative_, {International Institute for Applied Systems Analysis (IIASA)}.

{United Nations} (2015). _Transforming Our World: The 2030 {{Agenda}} for {{Sustainable Development}}_.

{UN Press} (2023). _Secretary-{{General Calls}} on {{States}} to {{Tackle Climate Change}} \`{{Time Bomb}}' through {{New Solidarity Pact}}, {{Acceleration Agenda}}, at {{Launch}} of {{Intergovernmental Panel Report}} | {{UN Press}}_.

Wark, McKenzie (2019). _Capital Is Dead_, {Verso}.

Wright, Erik Olin (2010). _Envisioning Real Utopias_, {Verso}.


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Wark, McKenzie (2019) proposes that digitalization and ICT has in fact led to a stage of information-based power consolidation even beyond and worse than capitalism.

<sup><a id="fn.2" href="#fnr.2">2</a></sup> In the language of André Gorz, "non-reformist reforms." (Engler, Mark and Engler, Paul, 2021)

<sup><a id="fn.3" href="#fnr.3">3</a></sup> https://fediverse.info/

<sup><a id="fn.4" href="#fnr.4">4</a></sup> https://repair.eu

<sup><a id="fn.5" href="#fnr.5">5</a></sup> https://www.fairphone.com/en/

<sup><a id="fn.6" href="#fnr.6">6</a></sup> https://frame.work/gb/en

<sup><a id="fn.7" href="#fnr.7">7</a></sup> https://nitter.net/about
