# Digital literacy

> Digital literacy includes the ability to find and use information (otherwise known as [[information literacy]]) but goes beyond this to encompass communication, collaboration and teamwork, social awareness in the digital environment, understanding of e-safety and creation of new information. Both digital and information literacy are underpinned by critical thinking and evaluation.
> 
> &#x2013; [About Digital and Information Literacy | Library skills framework](https://www.open.ac.uk/library-skills-framework/about-digital-and-information-literacy) 

