# My fish in termux setup

Things I've done to setup [[fish]] in [[termux]] on my phone.

```shell
pkg install fish
fish_config prompt save minimalist
fish_vi_key_bindings
pkg install python
fish_update_completions
abbr -a gac "git add . && git commit"
abbr -a gpl git pull
abbr -a gps git push
```

