# neoliberalism

-   Neoliberalism is a particular flavour of capitalism.  Contrast with e.g. Keynesianism, another flavour.
-   Key tenets: free markets, deregulation, privatisation, individual freedoms.
-   Popularised by Hayek; the Montpellerin Society.
-   First tried out in Chile following the coup.
-   Took hold in UK with Thatcher, US with Reagan.


## Definition

> The epithet ‘neoliberal’ is often a grenade lobbed with the pin attached, because this explosive term is rarely understood by those hurling it
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Departing from their eighteenth-century tradition, the neoliberals recognized that [[markets]] were hardly natural but rather needed nurturing and protection by a strong [[state]]. Markets deserved such care because they could concentrate knowledge diffused throughout society into the metric of [[price]]. The conference’s impresario, Friedrich Hayek, saw the price system as a mechanism not merely for exchanging goods but also ‘for communicating information’. Markets allowed people to act rationally as individuals without full knowledge of why prices change, which meant that society’s ‘optimal ignorance’ was surprisingly high
> 
> [[Half-Earth Socialism]]


## Donald Trump apotheosis of Neoliberalism

Failed business man running country, dismantling state and making money from it


## Modus operandi


### Neoliberalism ring fences common foods for profits


### Gives a fiction of choice when it has privatised essentials


### Individualism


## Criticisms


### Atomization and individualism seems stupidly inefficient


### Against individualism: if there's meaning in life it's in our relationships


### Self help can be a very individualistic pursuit sometimes


## Inconsistencies


### Neoliberalism relies heavily on a strong state to push trade agreements


### Neoliberalism makes massive use of public infrastructure


### Neoliberalism is not really a free market - it's a rigged market


## Resources


### George Monbiot: "Neoliberalism: the ideology at the root of all our problems"


#### https://www.theguardian.com/books/2016/apr/15/neoliberalism-ideology-problem-george-monbiot


### Radio Open Source: "Welcome to our neoliberal world"


#### http://radioopensource.org/welcome-neoliberal-world/


### Weekly Economics Podcast: "Beginner's Guide to Neoliberalism"


#### https://soundcloud.com/weeklyeconomicspodcast/neoliberalismthebasics

