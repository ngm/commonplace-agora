# 2020-09-20



## All migrated to GreenHost now

Moved my WordPress site over to my new VPS at [[GreenHost]] now.  That's everything then - can turn off the old Digital Ocean droplet now.  

The new VPS is Ubuntu 20.04, which is MySQL 8, so had a little [[bit of a faff with some errors]] as the old server was MySQL 5.7.

Here's my rough notes on the [[steps for setting up the new server]].


## 

Getting to the point with my setup where the 'manual til it hurts' publishing of the things in my stream to my org-roam static site first, then manually copying the content to my WordPress site, is starting to get just a tad painful&#x2026; can feel some blisters forming.

Why am I doing that way again?  Couple of reasons - I much, much prefer writing in Emacs than in WP's editor.  Second, I like the [[stream-first]] approach, where daily logs accrue as backlinks in my wiki and help build up its content.  But it's only by posting to WordPress that I'm getting the IndieWeb social goodness (including cross-posting to Mastodon, which is where I get most of the interaction right now to be honest).

Couple of ideas. First: add IndieWeb social goodness to my org-roam published site, removing the need for WordPress.  That's not going to happen anytime soon.  Second: use micropub to post straight to WP from within Emacs.  I've already got this partially set up in the past.  Just need to revisit and refine that a bit.

