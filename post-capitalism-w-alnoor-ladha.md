# Post Capitalism w/ Alnoor Ladha

A
: [[podcast]]

Part of
: [[Upstream]]

Featuring
: [[Alnoor Ladha]]

Fascinating discussion. Post Capitalism. Post here not meaning 'after' but 'in relation to'.

Pluralism.

Past and present examples: [[Zapatistas]]. [[Rojava]]. Indigenous worldviews.

[[Relational ontologies]] and [[OntoShift]].

