# Exporting highlights and notes from koreader

I read a lot of books and articles in [[koreader]] on my Kobo.  I take a lot of notes and highlights.

-   Currently using [[KoHighlights]], but a bit too much friction, and I feel like I'm losing notes that I've made (not literally losing them, I mean just not getting them processed and forgetting them).
-   I just quickly tried setting up a sync to readwise, but it failed first time around.
-   I could have a poke around in the .adds folder and check the settings there.
    -   OK, that worked, I had typed the token in wrong on the clunky on-screen keyboard, but fixed it in the settings file.
    -   I've supposedly exported some highlights to readwise, but now I can't see them there.
        -   OK, I just had to go through some badly designed onboarding process before I could actually see my highlights.
        -   Meh, it looks like it could be kind of handy, but:
            -   koreader fairly frequently crashes when exporting the highlights.
            -   I'll give it a go for a bit.
-   I wonder if I can do some kind of bulk export in KoHighlights.
    -   Yes, you can select all and do an export.  Either as 1 file for all or 1 file per book.  That's handy.
    -   I tried to a single CSV file, and even after tweaking it to be CSV separated, and to have the right column titles, readwise import still failed on import.  Will have to come back to that.

