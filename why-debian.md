# Why Debian?

I am very familiar with apt for package management.

I've used Ubuntu and Mint in recent years, which are built on top of Debian.

[[Debian]] seems to have good community guidelines and governance structure.

