# Foxconn

> Conditions are just as bad for workers at Foxconn, which, over the past decade, has found itself at the heart of a string of scandals. In 2010, 15 people committed suicide at the so-called “Foxconn City” in Shenzhen, while poor conditions and unpaid student labor have all made headlines. In September 2019, China Labor Watch, another non-governmental organization, reported ongoing worker rights violations at Foxconn. 
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

