# Revolutionary Leftism with Breht O'Shea

A
: [[podcast]]

URL
: https://www.upstreampodcast.org/conversations

Series
: [[Upstream]]

[[Breht O'Shea]] on the left in general, but [[Leninism]]  and a bit of [[Maoism]] in particular.

-   ~00:17:17 Leninism is very much focused on the 'how' of revolution.
-   Breht says something like - once you've realised capitalism is harmful; once you've realised that socialism is a better alternative; you might start thinking, how do we actually get socialism in place?  You'll pretty quickly then get to Leninism.
-   Discusses [[Maoism]].  [[Great Leap Forward]], [[Cultural Revolution]].  O'Shea suggests that portrayals of Mao as a genocidal leader are fallacious.  Definitely (huge) mistakes were made, but not as a deliberate program of terror.
-   And Stalinism.  Says sort of similar as above.  That Stalin was not simply a cartoon monster, and that a lot of what is said about him is propaganda.  But that he also oversaw many terrible things.  My gut finds O'Shea too sympathetic but hey I don't know the history well, so maybe I have just been propagandised.
-   ~01:06:51  The importance of theory, the important of organising, the need for both.
-   ~01:09:11  Is economics a science?  Crudely, [[historical materialism]] is the 'science' of Marxism.  [[Dialectical materialism]] is the philosophy.
-   ~01:21:09  Capitalism and fascism.
-   ~01:42:33  Your projects need to be situated in a bigger movement. [[Class consciousness]] etc.
-   ~01:45:54 Recommends [[Socialism: Utopian and Scientific]],  [[The State and Revolution]] and [[The Wretched of the Earth]].

