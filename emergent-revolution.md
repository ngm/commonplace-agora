# emergent revolution

The [[emergence]] of [[Revolution]].

A [[phase transition]] to the new way.

The ground must be prepared, the seeds sown and watered, for the harvest to come.

A global network of [[communities of praxis]].

Compare with [[protopia]].  Protopia is continuous improvement.  Emergence is phase transition.  Is phase transition predicated on continuous improvement?

Probably related to [[social tipping point]].

See: [[The Stigmergic Revolution]].

