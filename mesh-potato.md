# Mesh Potato

> The Mesh Potato is a device for providing low-cost telephony and Internet in areas where alternative access either doesn’t exist or is too expensive. It is a marriage of a low-cost wireless access point (AP) capable of running a mesh networking protocol with an Analog Telephony Adapter (ATA).
> 
> [Village Telco » Mesh Potato](https://villagetelco.org/mesh-potato/)

