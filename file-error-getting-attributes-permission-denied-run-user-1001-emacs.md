# File error: Getting attributes, Permission denied, /run/user/1001/emacs

> File error: Getting attributes, Permission denied, /run/user/1001/emacs

I seem to get this error if I'm logged in as two separate users.  I get it when starting emacs with the second user.

Seems odd, because why would they both be trying to access the same user id?

I'm logged in to user via X, the other user via a terminal.  On the first emacs is started with `emacs&`, on the second with `emacs -nw`.

Let's see which user 1001 is.

```shell
grep 1001 /etc/passwd
```

OK, restart is the user logged in via X, and the one that logged in first.

I guess it's probably related to me running emacs in server mode?

Let's just confirm that I am.

I have

```emacs-lisp
dotspacemacs-enable-server t
```

in my spacemacs config, and

```emacs-lisp
(boundp 'server-process)
```

so pretty sure that I am.

OK - how can I start an instance of emacs **without** connecting to the server?

Hmm, actually, isn't that what should already be happening?  I thought you had to explicitly invoke `emacsclient` to connect to the server.

Probably spacemacs is doing something behind the scenes.

If I invoke with

```emacs-lisp
emacs -Q
```

i.e. bypassing all my user init stuff, no error.

I can't figure out how start with my usual init, and **not** start a server though.

Hey ho.  Can come back to it.

