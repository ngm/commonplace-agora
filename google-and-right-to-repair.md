# Google and right to repair

[[Google]] and [[right to repair]].


## Spare parts

> Google’s parts program is slightly more expansive than that of other manufacturers: Notably, it will sell original replacement [[batteries]], which can have the effect of significantly extending the life of a smartphone. It will also sell cameras, replacement displays, “and more,” the blog post says. 
> 
> &#x2013; [[Google Will Sell Pixel Batteries, Repair Parts to Customers]]


## Software updates

> As of the Pixel 6, Google is promising three years of Android updates and five years of security updates, which could see the phones being used into late 2026. 
> 
> &#x2013; [[Google joins Samsung in working with iFixit on a self-repair program]]

