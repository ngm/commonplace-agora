# Monarchy

> In the UK, the symbolic death and rebirth of the state takes on a frightening literalism with the lifespan of an individual. **The nature of monarchy is that kings and queens, by definition, embody something more than themselves. They are the flesh-and-blood vehicle for a nation’s symbolic baggage.**
> 
> &#x2013; The Queen is Dead, Ash Sarkar, The Cortado newsletter

<!--quoteend-->

> For some, that means that Queen Elizabeth II was the personification of tradition, duty, deference, and every other conservative virtue you might care to name. For others – this author included – whatever personal graces Mrs Windsor may have had, **the crown can only ever represent the spectacular injustices of unearned wealth, inherited power, and the violence of [[colonialism]]**. Whether you’re a flag-waving monarchist, or a fervent republican, the one thing that the Queen’s death cannot mean is nothing.
> 
> &#x2013; The Queen is Dead, Ash Sarkar, The Cortado newsletter

The crown represents:

-   unearned wealth
-   inherited power
-   the violence of colonialism

