# Drew Pendergrass and Troy Vettese on Half Earth Socialism

URL
: https://www.futurehistories.today/episoden-blog/s02/e18-drew-pendergrass-and-troy-vettese-on-half-earth-socialism/

Another good interview with the [[Half-Earth Socialism]] authors.

This one has a strong focus on the planning aspects of it.

~00:02:18  Nice summary definition of Half-Earth Socialism.

~00:03:50 Critiques of [[BECCS]], [[nuclear power]], and [[Half-Earth]].

~00:30:09  Some good overview here on how the planning might actually work.

