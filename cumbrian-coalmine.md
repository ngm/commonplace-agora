# Cumbrian coalmine

[[Whitehaven]].

> UPDATE on the #Cumbria #coal mine decision: postponed again; now to be issued by 8 Nov 2022.
> Meanwhile, Port Talbot proposes moving to non-coal electric arc furnaces by early 2030s; British Steel can't use coal from Cumbria &amp; steel across the world is decarbonising #ClimateCrisis
> 
> https://twitter.com/estelledehon/status/1557399827675320321#m

<!--quoteend-->

> The new coalmine [is] expected to create 500 jobs, but [the Local Government Association] says [there is] potential for 6,000 green jobs in Cumbria by 2030,” Sharma tweeted. “[The Committee on Climate Change] has noted the mine would increase CO2 emissions by 0.4Mt [megatonnes] annually [with] clear implications for our legally binding carbon budgets.”
> 
> &#x2013; [Cumbria coalmine plan is ‘backward step’, says Alok Sharma | Coal | The Guardian](https://www.theguardian.com/environment/2022/dec/03/alok-sharma-cumbria-coal-mine-backward-step-government-climate-expert)[Cumbria coalmine plan is ‘backward step’, says Alok Sharma | Coal | The Guardian](https://www.theguardian.com/environment/2022/dec/03/alok-sharma-cumbria-coal-mine-backward-step-government-climate-expert)

