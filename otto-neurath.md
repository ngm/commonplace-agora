# Otto Neurath

[[in natura planning]]

> Otto Neurath, a remarkable but largely forgotten polymath of early twentieth-century Vienna. The kernel of his philosophical system was the rejection of ‘[[pseudorationality]]’ – the belief that any single metric could guide all decisions
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Neurath came to these conclusions by studying ancient and contemporary examples of economies based on ‘natural’ (or in natura) units of discrete physical things rather than money
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> After all, Neurath argued, there were no ‘war units’ to guide a battleship commander’s decisions. What mattered were incommensurate things: ‘the course of the ship, the power of the engines, the range of the guns, the stores of ammunition, the torpedoes, and the food supplies’.  In an emergency, prices fail to convey any information at all.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The war had usefully shown that profit could be disabled as the guiding metric for investment, a development that would be continued under socialism
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> In natura calculation would banish the ‘veiling concealments’ of money so that ‘everything becomes transparent and controllable
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Mises and especially Hayek undermined Neurathian socialism through powerful epistemic critique, which diverted the Left into pseudorational [[market socialism]]
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Neurath argued that plans based on labour time are as pseudorational as capitalist profit, in that both are based on a universal equivalent that obscured more than it clarified
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Despite his espousal of ‘total plans’ to co-ordinate society, Neurath was deeply sceptical of [[technocracy]]
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Rather than relying on a Platonic elite of logicians, Neurath thought that a visual language could democratize reason by making the essence of an economic problem apparent to non-experts
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Neurath was one of the original targets of Mises’s 1920 broadside against planning. He is remembered today as the theorist of [[total planning]]—a phrase that incorrectly conjures the image of social engineers running the economy from a control room. Nothing could be further from Neurath’s vision. On the contrary, Neurath argued that a socialist economy would have to be highly democratic—precisely because it could not be purely algorithmic
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> For Neurath, the algorithmic character of the price system was a problem to be overcome, rather than something that socialists should try to replicate. In a capitalist economy, managers are able to make clear-cut decisions about cost-effectiveness only because they are allowed to ignore all of the non-economic costs of their decisions, which include destroying communities, immiserating workers, depleting non-renewable resources, and filling the world with garbage. Economically rational decisions at the level of the firm add up to an increasingly irrational society
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Neurath laid out his version of a planning protocol—a term that he did not himself use—in “[[Economic Plan and Calculation in Kind]],” an essay he wrote in 1925.  Planning begins with expert planners reducing the “unlimited number of economic plans” that are “possible” down to a few “characteristic examples.” These planners do the algorithmic calculations, which clarify the options among which people must decide. People are then presented with these options for direct comparison. They evaluate a few different plans across multiple criteria and decide which they prefer: listening to comments, voicing their concerns, and taking a vote
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Towards the end of his life, Neurath spent years trying to determine how semi-literate peasants and urban workers could be incorporated into a planning protocol, via the distribution of simple graphical representations that he called [[isotypes]]
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Neurath hoped that councils, guilds, and other associations could find another way forward. In particular, he speculated that they might be able to use planning protocols to make their own direct comparisons between different “ways of working”—taking into account many and varied criteria that could not “be reduced to one single unit”—while collaborating with one another to help fulfill society-wide goals
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Marxists all tended to share the view that a socialist society would replace private ownership and market forces with social ownership and planned production, but few thought it necessary to explain how this would work in practice.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> One figure to have contributed to this task was Otto Neurath. Marxists, in his view, had been inattentive to how a rational system of social planning would be organised.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> His idea was that the allocation of society’s resources should be a matter consciously decided upon by the entire people and not left to the anarchic forces of the market or to the self-interested machinations of oligarchs.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> He thought people should consider different economic plans with various distributions of investment and choose the one that produced the maximum quality of life for them.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> He stressed that decisions about the allocation of resources called for ethical and political decisions about different forms of life
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> [[Democratic planning]] would give us the tools of public investment to adapt our economy through a green industrial strategy and rapidly transition away from harmful and extractive practices that are destroying the basis for natural life.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> We wouldn’t have to work out every precise detail or have a complete list of exactly how many products each economic unit should produce.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> The plan would enable us to be self-determining over the material conditions of our lives at a macro level without requiring the kind of centralised command economy of the Soviet Union.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Underlying Neurath’s work is the distinction between directive and indicative planning. In the former, a central authority would give orders as to how much every subordinate organisation needed to produce, whereas indicative planning is about setting targets and directing resources into broad sectors while allowing for a degree of autonomy in fulfilling these objectives.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> A plan would allow for a pluralist economy in which a diversity of economic forms could flourish based on democratic deliberation and decision-making.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> To best fulfil Neurath’s desire for democratic planning, complex societies would require monetary calculations to understand and interrogate the relative costs associated with different plans and judge between them
> 
> &#x2013; [[Platform socialism]]

