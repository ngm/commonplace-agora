# Using solar panels for the home



## Notes from Alan

-   14 panels
-   Generate 20 kWh in a good day in summer
-   Average of 5 / 6 kWh usage per day
-   About 1 kWh used one Sunday in April where they weren't home.

-   Excess can be sold back to the grid.
-   Storage batteries, e.g. Tesla Powerwall, are very expensive.

-   Use a low wattage kettle so that it doesn't end up taking a draw from the grid if you go over what panels are currently generating.
-   Some things you wouldn't expect use a fair chunk of power - e.g. heating the home, even though it's gas central heating
    -   the pumps in the oil-powered boiler use a fair bit
    -   and then the pump that pumps the water around

-   you need to be synchronised with the grid to feed back in to it
    -   which interestingly means you can't actually use your solar panels during a power cut

I liked how it seemed you get more in tune with the weather and nature - even though you're backed up by the grid, you still pay attention to good times to make use of your electricity.  Like you become more attuned to the fact that it isn't just endlessly abundant.


## Other stuff

> Solar panels convert energy from the sun into electricity. Stronger sunlight creates more electricity, which can then either be used in your home or exported to the national grid.
> 
> &#x2013; [Solar panels: a ray of hope as UK energy prices go through the roof | Money |&#x2026;](https://www.theguardian.com/money/2022/feb/28/solar-panels-a-ray-of-hope-as-energy-prices-go-through-the-roof) 

<!--quoteend-->

> Domestic systems are generally made up of between 10 and 15 panels, each of which generate between 200W and 350W of energy, according to the Energy Saving Trust, a charity promoting energy efficiency. The more panels on the roof, the higher the installation cost but also the potential for more energy.
> 
> &#x2013; [Solar panels: a ray of hope as UK energy prices go through the roof | Money |&#x2026;](https://www.theguardian.com/money/2022/feb/28/solar-panels-a-ray-of-hope-as-energy-prices-go-through-the-roof) 

<!--quoteend-->

> The average price for an installation of a 3.5kW system is £4,800, including labour. This tends to be about 12 panels.
> 
> &#x2013; [Solar panels: a ray of hope as UK energy prices go through the roof | Money |&#x2026;](https://www.theguardian.com/money/2022/feb/28/solar-panels-a-ray-of-hope-as-energy-prices-go-through-the-roof) 

