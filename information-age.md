# Information Age

> the idea of ‘the information age’ has its roots in both Shannon and Weaver’s work and in Wiener’s cybernetics.
> 
> &#x2013; [[Anarchist Cybernetics]]

