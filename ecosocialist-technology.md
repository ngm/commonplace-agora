# Ecosocialist technology

> Ecosocialist technology is technology guided by universal human need and by concern for the health of the natural environment.
> 
> &#x2013; [[Technology and Ecosocialism]]

<!--quoteend-->

> It contrasts with capitalist technology, which is driven above all by the imperatives of cost-reduction and profit-maximization within a market whose contours are shaped by the owners or state agents of capital. 
> 
> &#x2013; [[Technology and Ecosocialism]]

<!--quoteend-->

> The contrast between ecosocialist and capitalist technology appears across all sectors of production and services.
> 
> &#x2013; [[Technology and Ecosocialism]]

