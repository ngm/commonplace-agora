# Revolutionary transition

I came across this phrase first in [[Climate Leninism and Revolutionary Transition]].  I understand it to mean the actual nitty-gritty of how a revolution is birthed and enacted.

> Transition is revolution.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> At one extreme, some have rejected the question of transition entirely, imagining communism’s immediate implementation through ‘communizing measures.’ At the other, transition is prorogued in favor of the apparently more urgent task of fighting for survival within capitalism.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> They deny its duration or disavow the fact that transition is communism in the making. How we depart from capitalism shapes our destination. And we must depart from capitalism.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> Transition has become the question of our times, both for capitalism — as compounding ecological crises start to eat away at the fiction of capital’s compatibility with human and non-human flourishing — and for radical movements and revolutionaries
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> How do we get from here, from a world on fire, to there, to a world slowly but surely regenerating from centuries of violence, plunder and exploitation? What is our strategy? What are our immediate tactics? This is a problem that can’t be avoided.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

