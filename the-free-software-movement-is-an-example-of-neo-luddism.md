# The free software movement is an example of neo-Luddism

A
: [[claim]]

The [[free software]] movement is an example of [[neo-Luddism]].

First heard [[Hotel Bar Sessions: Breaking Things at Work (with Gavin Mueller)]].

Makes sense. Free software hackers are clearly not anti-technology. They just don't like the way im that software has been enclosed and commodified.

