# hypertexting

> ‘Constructing a body of [[hypertext]] over time—such as with blogs or wikis—with an emphasis on the strengths of linking (within and without the text) and rich formatting.’
> 
> A superset of blogging and wiki creation, as well as movements like the [[Indieweb]] and, to some degree, federated networks.
> 
> &#x2013; Kicks Condor, [hypertexting](https://www.kickscondor.com/hypertexting/) 

