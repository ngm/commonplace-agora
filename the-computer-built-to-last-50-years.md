# The computer built to last 50 years

URL
: https://ploum.net/the-computer-built-to-last-50-years/index.html

Introduces the [[Forever Computer]].

I liked this. I like the ideas around durable hardware and also the offline computing stull. Perhaps a bit too keyboard coursed for me (given how I'm enjoying handwriting with a stylus lately) but I appreciated the use of e- ink In general though, yes - all in favour of something that aims to drastically expand the lifespan of our digital technologies.

> When you are online, your brain knows that something might be happening, even without notification. There might be a new email waiting for you. A new something on a random website. It’s there, right on your computer. Just move the current window out of the way and you may have something that you are craving: newness. You don’t have to think. As soon as you hit some hard thought, your fingers will probably spontaneously find a diversion

<!--quoteend-->

> In some rural areas of the planet where broadbands are not easily available, such [[Delay Tolerant Networks]] (DTNs) are already working and extensively used, including to browse the web.

