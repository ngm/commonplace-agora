# Tiwa

> This is done through the Tiwa project, a non-profit Internet service exclusively available to civil society organizations, social movements and educational and research entities. Tiwa's services guarantee a protected infrastructure environment for innovations, experiments, debate and training processes, as well as Nupef's own services and those of its partners.
> 
> &#x2013; [About Nupef | Instituto Nupef](https://nupef.org.br/about-nupef) 

