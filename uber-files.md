# Uber Files

> At least six Conservative ministers, including the then chancellor, George Osborne, and the future health secretary Matt Hancock, did not declare secret meetings at which they were lobbied by Uber, leaked files reveal.
> 
> &#x2013; [Osborne, Hancock and other ministers did not declare secret Uber meetings | U&#x2026;](https://www.theguardian.com/news/2022/jul/11/osborne-hancock-tory-ministers-did-not-declare-secret-uber-meetings-leak-reveals) 

