# Reinventing How We Use Computers

URL
: https://ploum.net/2022-12-03-reinventing-how-we-use-computers.html

A bit of a follow-up on the [[Forever Computer]].  But focused more on the interface and the offline aspects. So a bit of an expansion upon just durability, but also other aspects of ethical approaches te computing. kind of around agency - so a large screen with no useful input device might suggest a focus on consumption rather than production. And the urgency of constant connectedness could be countered by more of an offline first approach.

I think it could be summarised perhaps as desiring of devices that support our intention rather than trying to take our attention.

