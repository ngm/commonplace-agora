# Capitalism

In a nutshell, the economic side of capitalism is:

-   private ownership of [[capital]] (trade, industry, the means of production)
-   with the intention of making profits in a competitive market economy
-   using wage labour (i.e. employment of workers who do not own the means of production)

But, capitalism stretches far beyond these basic economic ideas.

> Capitalism is not only about economy. It is about resources, it is about nature, it is about how we organize and live our life. Capitalism is colonization and centralization, is monopoly and monoculture, is domination over life and nature.
> 
> &#x2013; [About the climate strike and the dark sides of the “green new deal”](https://makerojavagreenagain.org/2019/09/19/about-the-climate-strike-and-the-dark-sides-of-the-green-new-deal/)

A lot of its ills stem from the pursuit of growth as the key measure of success and progress. ([[growthism]])

[[Capitalism is the root cause of the overshoot of planetary boundaries]] and [[Capitalism is the root cause of social inequity]].

I am an ecosocialist and thus I am an anti-capitalist.


## What is it

> Capitalism as a way of organizing economic activity has three critical components: private ownership of capital; production for the market for the purpose of making profits; and employment of workers who do not own the means of production
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

Other non-capitalist forms of economy exist in pockets around capitalism.  So not **everything** is capitalist, but it is hegemonic.

> Existing economic systems combine capitalism with a whole host of other ways of organizing the production and distribution of goods and services: directly by states; within the intimate relations of families to meet the needs of its members; through community-based networks and organizations; by cooperatives owned and governed democratically by their members; though nonprofit market-oriented organizations; through peer-to-peer networks engaged in collaborative production processes; and many other possibilities.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> We call such a complex economic system “capitalist” when capitalist drives are dominant in determining the economic conditions of life and access to livelihood for most people. That dominance is immensely destructive.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> with ever increasing concentrations of wealth in fewer and fewer hands, with capitalism roaming the globe in search of profits, with a deepening contradiction between the colossal growth of production and the failure to distribute its fruits justly.
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> Nevertheless, as Jacobin editor Bhaskar Sunkara wrote: “The core of the system he described is little changed. Capitalism is crisis-prone, is built on domination and exploitation, and for all its micro-rationality has produced macro-irrationalities in the form of social and environmental destruction.
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> At its core, capitalism was defined by Marx as a social relation of production. He meant that profits are not the result of good accounting or the inventive ideas of the superrich, but are instead the outcome of an exploitative relationship between two classes of people: bosses and workers
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Rather than a peasantry violently coerced to turn over goods to their lords, capitalism created a new underclass of wageworkers—a class of people theoretically free to work where and how they pleased, but who would in practice be compelled—by economic necessity—to produce a surplus for someone else nonetheless
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> capitalism is not simply an economic or a political structure, but a system of social relationships, whose foundation is the expropriation of masses of people from the land
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Where it came from

Feudalism morphed into capitalism.  AFAIU it was a long process (decades/centuries).  Not a global rupture out of nowhere.  Started in England, then was exported with colonialism (and maybe also started of its own accord elsewhere?)

> Classical economists like [[Adam Smith]] argued that capital came to be through a gradually evolving division of labor, where some people became traders, and some of these traders would eventually—through thriftiness or hard work—save enough wealth to build factories and employ workers
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> This process may have been punctuated by political upheavals and even revolutions, but rather than constituting a rupture in economic structures, these political events served more to ratify and rationalize changes that had already taken place within the socioeconomic structure
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> Although capitalism became a dominant society only in the past few centuries, it long existed on the periphery of earlier societies: in a largely commercial form, structured around trade between cities and empires; in a craft form throughout the European Middle Ages; in a hugely industrial form in our own time; and if we are to believe recent seers, in an informational form in the coming period.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> The division of society into haves and have-nots did not gently come to pass, and certainly not through the frugalness and intelligence of a small elite. It was the outcome of a violent upheaval, which forced large swaths of the population from their lands and traditional means of self-sufficiency
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The violence, coercion, legislation, and upheavals necessary for the birth of this new system evince just how unnatural and vicious the road to capitalism was
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> at capitalism’s dawn, the rising bourgeoisie depended heavily on the power of the state to enforce its collective will
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> So long as families could produce food and clothing for themselves, they did not have to work for a wage. Once the vast majority of people lost access to their lands, the organizing principles of society would change.
> 
> &#x2013; [[A People's Guide to Capitalism]]


### [[Primitive accumulation]]


## Why England?

Theory that the use of money for rent as tribute in England was one of reasons capitalism started there. Plenty of other places had a tribute system, but was it the use of money and rent relations that kicked off capitalism?

> England, where capitalism gained its first foothold, it did so on the basis of what’s referred to as the “[[Enclosure Movement]].” Millions of acres of common land were violently confiscated and turned into privately-owned plots during several centuries. Traditional rights to use common land for farming or grazing livestock were revoked, land was fenced in (enclosed) and restricted to private owners—whether through payment, theft, or law


## The problems it causes

> Capitalism is an inequality-enhancing machine as well as a growth machine.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

-   [[capital strike]]

> free market [&#x2026;] relentless drive to colonize every aspect of human existence, glossing over its incapacity to effectively manage common resources and its structural tendency to externalize a range of economic, social and environmental costs. 
> 
> &#x2013; [[Future Histories]] 

<!--quoteend-->

> As Marx puts it, “Capitalist production &#x2026; develops technology, and the combining together of various processes into a social whole, only by sapping the original sources of all wealth – the land and the labourer”.
> 
> &#x2013; [[The Entropy of Capitalism]]

<!--quoteend-->

> capitalism has always been intrinsically linked to an act of colonisation: both an internal colonisation of the commons, and an external one, of the global South.
> 
> &#x2013; [[The Entropy of Capitalism]]


### Externalities

Moore's Law, for example, was fuelled by underpaid workers working with toxic solvents.  As the transistors got smaller, the solvents got more toxic.
(Note: haven't verified this, just heard it on the radio.  Could do with a proper source&#x2026;.
)


## In crisis

Capitalism keeps on fucking up, with crashes and depressions.

-   bit more nuanced discussion: [Glossary of Terms: Crisis](https://www.marxists.org/glossary/terms/c/r.htm#crisis-of-capitalism)

-   see [Crisis theory](https://en.wikipedia.org/wiki/Crisis_theory)


## Anti-capitalism

-   [[Logics of resistance]]


## Post-capitalism

What exactly is post-capitalism?  Guess I'll have to read Paul Mason's book on it.  But it seems like an odd phrasing.  Value-neutral.  If it's just meaing 'whatever comes after capitalism', then what says that that is necessarily going to be better?  I think we should apply some indication of what we expect to be after capitalism; if not, what's to stop it becoming something else in the vacuum?


## [[Neoliberalism]]


## How will it end

> capitalism is not an eternal system embedded in our nature. It has a history and an origin, and therefore, it can have an end
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The power of the few to extract labor and profits from the many is based on a relationship of economic dependence, which is historically conditioned
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Misc

> We can foresee a time when the proletarian, whatever the color of his or her collar or place on the assembly line, will be completely replaced by automated and even miniaturized means of production that are operated by a few white-coated manipulators of machines and by computers.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Capitalism, in effect, has generalized its threats to humanity, particularly with climatic changes that may alter the very face of the planet, oligarchical institutions of a global scope, and rampant urbanization that radically corrodes the civic life basic to grassroots politics.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> The “prevailing life-motif” of modern capitalism and the liberal state, writes Greek social critic Andreas Karitzis: promotes the idea that a good life is essentially an individual achievement. Society and nature are just backdrops, a wallpaper for our egos, the contingent context in which our solitary selves will evolve pursuing individual goals. The individual owes nothing to no one, lacks a sense of respect for the previous generations or responsibility to future ones — and indifference is the proper attitude regarding present social problems and conditions.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> “It can and must,” as Wood insists, “constantly accumulate, constantly search out new markets, constantly impose its imperatives on new territories and new spheres of life, on all human beings and the natural environment.”
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> This was true the world over, in every kind of economy. But Marx had drawn a nightmare picture of what happened to human life under capitalism, when everything was produced only in order to be exchanged; when true qualities and uses dropped away, and the human power of making and doing itself became only an object to be traded. Then the makers and the things made turned alike into commodities, and the motion of society turned into a kind of zombie dance, a grim cavorting whirl in which objects and people blurred together till the objects were half alive and the people were half dead.
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> That was Marx’s description, anyway. And what would be the alternative? The consciously arranged alternative? A dance of another nature, Emil presumed. A dance to the music of use, where every step fulfilled some real need, did some tangible good, and no matter how fast the dancers spun, they moved easily, because they moved to a human measure, intelligible to all, chosen by all. Emil gave a hop and shuffle in the dust.
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Capitalism comes with an embedded form of coercion that drives growth: the threat of unemployment, homelessness, and starvation for those unwilling to sell their labour, and bankruptcy or hostile takeovers for any firm that fails to make the going rate of profit.
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Although critics of the Left often accuse socialists of magical thinking, the real fantasy is modelling a future where capitalism can be constrained within planetary boundaries
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Under capitalism, a firm becomes competitive in large part because of its ability, and that of the system as a whole, to not ‘pay’ for positive social and environmental contributions, and to leave the reparations of social and environmental damages to other actors, that is, mainly the citizenry or the state.
> 
> &#x2013; [[P2P Accounting for Planetary Survival]]

<!--quoteend-->

> capitalism relies on maintaining an [[artificial scarcity]] of essential goods and services (like housing, healthcare, transport, etc), through processes of [[enclosure]] and commodification.
> 
> &#x2013; [[Universal basic services: the power of decommodifying survival]]

