# Android apps I like

Probably only going to list ones available on F-Droid as much as possible.


## Media

-   NewPipe
-   RadioDroid
-   VLC
-   Vanilla Music
-   [[AntennaPod]] for podcasts.
-   [[Wallabag]]


## Utilities

-   DiskUsage: was really nice app for seeing where your storage space is being used, and freeing it up.  Sadly as of 2022 seems to no longer work with my version of Android, not properly scanning directories.  Trying Storage Analyzer &amp; Disk Usage instead for now.  Not libre sadly.
-   OpenLauncher
-   [[AnySoftKeyboard]]
-   [[KeePassDX]]
-   [[Syncthing]]
-   Simple Contacts
-   Simple File Manager
-   Simple Flashlight
-   Simple Gallery Pro
-   fdroid
-   ameixa
-   dimmer
-   open camera
-   Termux
-   Nextcloud
-   Tor Browser


## Maps

-   OsmAnd.  I found this pretty handy for offline routing when I was cycling in the Lake District.
-   Organic Maps.  More friendly than OsmAnd I've found.
-   StreetComplete.  For giving back to OpenStreetMap.


## Organisation

-   [[orgzly]]
-   Markor
-   Etar
-   AnkiDroid


## Social

-   Indigenous
-   Tusky
-   Frost
-   Signal
-   Conversations

