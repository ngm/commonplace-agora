# Shaun Chamberlin on David Fleming's Vision of Post-Capitalist Life

A
: [[podcast]]

URL
: https://david-bollier.simplecast.com/episodes/shaun-chaberlin-on-david-flemings-vision-of-post-capitalist-life

Series
: [[Frontiers of Commoning]]

Featuring
: [[Shaun Chamberlin]] / [[David Bollier]]

[[David Fleming]]. [[Lean Logic]].

