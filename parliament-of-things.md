# Parliament of things

An idea from [[Bruno Latour]].

> wherein natural phenomena, social phenomena and the discourse about them are not seen as separate objects to be studied by specialists, but as hybrids made and scrutinized by the public interaction of people, things and concepts.
> 
> &#x2013; [We Have Never Been Modern - Wikipedia](https://en.wikipedia.org/wiki/We_Have_Never_Been_Modern) 

