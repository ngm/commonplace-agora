# Incredibly, current climate pledges could keep heating below 2C – but our work isn’t over

URL
: https://www.theguardian.com/commentisfree/2022/apr/14/current-climate-pledges-heating-below-2c-net-zero

[[Climate change]].  [[2C]].  [[Net zero]].

> The battle to get countries and companies to sign up to net zero is being won. Now let’s keep pushing for more ambitious targets

<!--quoteend-->

> There is no target, agreement or technology that can abruptly resolve the issue. Instead of ends, we should see these as means: signposts we can organise around, contest and weave into an ever-widening mobilisation of society that builds greater momentum for change.

<!--quoteend-->

> More stringent targets provide the basis from which to ratchet more momentum. If net-zero targets are commonplace, demands move to the next level.

<!--quoteend-->

> The growing taboo over fossil fuel companies must spread to their financiers and media cheerleaders.

<!--quoteend-->

> Yet reducing carbon emissions to net zero demands we change everything: different ways of eating, travelling and living.

