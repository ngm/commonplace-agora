# Nowcasting

> AI-based “nowcasting” weather systems use high-resolution radar networks along with data from satellites and high-altitude weather balloons to provide accurate, detailed and contemporaneous pictures of wildfires, hurricanes, floods, storms and other extreme weather events as they happen. The services are often private sector concerns utilised by the energy, transport and agricultural business sectors, as well as by government departments. Nowcasting plays an increasingly important role in disaster risk management and prevention. 
> 
> &#x2013; Down to Earth newsletter

