# Climate Action Plan Explorer

URL
: https://data.climateemergency.uk

> A fully searchable database of UK local authority climate action plans, climate targets, and climate emergency declarations.

Really excellent service for finding out what kind of [[Climate Action Plan]]s different UK councils have.

By [[mySociety]] and [[Climate Emergency UK]].

