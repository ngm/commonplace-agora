# data

> Data is the foundational resource of the modern economy
> 
> &#x2013; [[Digital Capitalism online course]]

<!--quoteend-->

> UNCTAD projections from 2019 suggesting that data-intensive businesses will account for 70 percent of the new value generated in the global economy over the next decade
> 
> &#x2013; [[Digital Capitalism online course]]

Data is made through [[datafication]].

> While many lenses and frames exist to conceptualize data, framing data as a form of capital allows us to understand the ways in which value can be derived from data, thus helping us unpack the motives behind its use by corporations and why data extraction within has become central to the economy4. Data as a form of capital is distinct from, but has roots in economic capital. Much like social and cultural capital, data capital is convertible, under certain conditions to economic capital.
> 
> &#x2013; [[Digital Capitalism online course]]

