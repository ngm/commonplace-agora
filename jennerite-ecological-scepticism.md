# Jennerite ecological scepticism

Man messing with nature only causes problems.

[[Edward Jenner]].

> What is more relevant for us is Jenner’s explanation of why smallpox and its ilk existed at all. He argued that diseases were the result of humanity’s unnatural domination of animals: ‘The deviation of Man from the state in which he was originally placed by Nature seems to have proved to him a prolific source of Diseases
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Jenner recognized [[animal husbandry]] as one of the most consequential and dangerous ways humans shape life on Earth.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Jenner’s ecological scepticism can be applied more broadly across the span of the environmental crisis because zoonotic diseases can be engendered or spread by practically any ecological disturbance: animal testing (Marburg virus), cattle ranching (Junin virus), deforestation (malaria), factory farms (MRSA), factory farms and deforestation (Nipah virus), pets (psittacosis), biodiversity loss (West Nile virus), dams (schistosomiasis), habitat fragmentation (Lyme), and the exotic-animal trade (SARS).
> 
> &#x2013; [[Half-Earth Socialism]]

