# 3IR and 4IR

The third and fourth industrial revolutions.  Digital and cyber-physical advancements.  Will they be for good or bad?

I don't like the name themselves, to be honest.  Revolutions of industry is not what we need.  We need revolutions of society.  That said, the technologies themselves could be liberatory if not simply used for capital accumulation.

> they are rapidly changing civilization, for better or worse depending on one's position, and making dramatic new orientation to work and labor possible.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]

-   [[third industrial revolution]]
-   [[Fourth Industrial Revolution]]

