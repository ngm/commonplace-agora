# Solarpunk Action Week

-   https://solarpunkactionweek.tumblr.com/post/634954770038374400/solarpunk-action-week-2021
    
    > Solarpunk Action Week has been ongoing twice a year since 2019, with every week looking bigger and better than the last. People all over the world are planting gardens, learning new skills, building things, reducing waste, spreading information, taking direct action, and getting their neighborhoods and workplaces organized.

[[Solarpunk]].

