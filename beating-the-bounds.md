# Beating the bounds

> Beating the bounds, you may recall, is the practice used by many English villages of walking the perimeter of their land to identify any fences or hedges that had encroached upon their shared wealth.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In our times, beating the bounds may initially involve [[direct action]] resistance and [[civil disobedience]] against [[enclosure]]s, and attempts to "de-enclose" them.
> 
> &#x2013; [[Free, Fair and Alive]]

