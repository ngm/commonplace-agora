# Working class and green politics

> Climate change and other severe environmental problems demand working class solutions. The productivity and creativity of workers is vital to ecological alternatives.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

<!--quoteend-->

> Workers produce and can produce alternative sustainable futures, the concept of workers’ plans for ecological production is important
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

