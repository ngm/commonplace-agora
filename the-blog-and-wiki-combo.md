# The blog and wiki combo

At a very high-level, blogs and wikis are just both ways of helping me get my thoughts in some kind of order, one way or another.  They both happen to be forms [[hypertexting]]. 

> Blogs as thinking out loud and conversations (also with oneself). Wiki as its accumulated residue.
> 
> ---**Ton Zylstra** ([The Blog and Wiki Combo](https://www.zylstra.org/blog/2019/06/the-blog-and-wiki-combo/))

The lines between the two are blurry, but generally I think the blog is for streams of thoughts and discussion, and the wiki is (supposedly) the distillation and crystalisation of those.   We'll see - the more I'm exploring, the more I'm finding the two are interwoven and overlapping.

> -   process - weblogs are good to keep track of ideas unfolding; datestamp and preserving the original are important
> -   outcome - wikis are good for (collaborative) working on integrating, refactoring and connecting ideas
> -   connection between these two is essential - I'd like to see how bits of weblog posts turn into something more tangible
> 
> &#x2013; [Mathemagenic: learning and KM insights - Tuesday, June 08, 2004](http://blog.mathemagenic.com/2004/06/08.html#a1233) 

Martin Fowler calls the blog and wiki combo his [[bliki]].


## [[The Garden and the Stream]]


## [[Stock and flow]]


## How do they interlink?

> Ultimately blogs and wikis are not either stock or flow to me but can do both. Wikis also create streams, through recent changes feeds etc. Over the years I had many RSS feeds in my reader alerting me to changes in wikis. I feel both hemmed in by how my blog in its setup puts flow above stock, and how a wiki assumes stock more than flow. But that can all be altered. In the end it’s all just a database, putting different emphasis on different pivots for navigation and exploration. 
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/) 

In this sense, perhaps the terms wiki and blog are unhelpful, in that we have preconceptions about what each can and can't do.

> Wiki and blogs have two different cultures, two different idioms, two different sets of values.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/)

<!--quoteend-->

> And the real magic trick is to put them both together. To keep the ball bouncing with your [[flow]]—to maintain that open channel of communication—while you work on some kick-ass [[stock]] in the background. Sacrifice neither. The hybrid strategy.
> 
> &#x2013; [Stock and flow / Snarkmarket](http://snarkmarket.com/2010/4890) 

Twitter and Facebook are all flow and no stock.

The actuality of what I post where and when (and why) is still a bit fluid.

At first I saw the blog as being the more ephemeral of the two, the stream of consciousness, and the wiki being where thoughts go when they are fully baked. But that has not been entirely the case so far. Some things I will actually write first in my wiki, completely undercooked, and shortly afterwards post to my blog timeline once I’ve thought it through a bit more in (almost) privacy.

At the moment I think I see the wiki as being ‘the bits that I want to keep’ long-term, and the blog as being ‘the thoughts that I want to share’ in the here and now. I might piece some thoughts together on the wiki, then share them via the blog for interaction, and then polish up the thoughts on the wiki based on what I’ve learned. For me (at the moment) the blog is social and interactive, the wiki is (publicly…) private and introspective.

The main technical distinction at the moment is that I **expect** to edit the text on the wiki, whereas I generally never go back and edit the text of things I’ve posted to my stream (other than fixing typos, links, etc).  I'll 'edit' the ideas that are on my stream, for sure.  But that's by posting a new item to the stream, not editing the text of something previous.  So a thought I had a year ago could be quite out-of-date a year later.  I’m partly tempted to go the route of automatically making timeline posts older than X months become private.

Maybe I should think of them **both** as my commonplace book, taken together.


## [[My process]]

> Thinking of blogs vs. wikis to support thinking. For me blogging is easier - it shows how ideas unfold over time and somehow I don't have a problem when I create new page (I do think twice in wikis - because it increases navigation mess). Blogging is also about permalinking and hypertexting half-baked ideas&#x2026;
> 
> The problem is that at the certain moment there is a critical mass (critical mess ;) of bits related to a theme. At this moment you need a least an overview of all of them and then a way to construct something more coherent. Wikis are great for that. It's much easier to get an overview of ideas (if they collected on one page :), edit them into something better or even go for refactoring the whole thing.
> 
> But then you get the clarity of a final product and lose an overview of path that took you there. And I'm getting more and more convinced that process and artefacts on the way is as important as the final product.
> 
> Of course, some wiki/weblog combination can make life easier (but not those where weblog post is edited as a wiki - you lose the path then). 
> 
> &#x2013; [Mathemagenic: learning and KM insights - Thursday, April 08, 2004](http://blog.mathemagenic.com/2004/04/08.html#a1160) 

<!--quoteend-->

> I'm eventually planning to distinguish more between different kinds of notes - I am thinking of being able to tag something #blog [21st January, 2020] and have it automatically go into a blog feed, sorted chronologically, with RSS feed etc. Those would be much more narrative complete, and formatted without bullet lists. Then I can link from those to more "raw current thinking" posts. I'll even enable publishing of backlinks - for example on Zettelkasten I have about 80 linked references from my daily pages - I don't want to share my daily pages, but with roam-export I can share just those linked references for anyone who is interested.
> 
> &#x2013; Stian Håklev, Digital Gardeners

<!--quoteend-->

> I am really interested in mixing the time-linear with the accretive in terms of collaboration, I wrote a bit about it here https://notes.reganmian.net/e--incremental-brainstorming
> 
> &#x2013; Stian Håklev, Digital Gardeners

^ seems to me this is very much garden and stream.


## See

-   http://thoughtstorms.info/view/WikiLog
-   http://meatballwiki.org/wiki/WikiLog

