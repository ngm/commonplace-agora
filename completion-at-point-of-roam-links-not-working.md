# completion at point of roam links not working

After updating spacemacs and packages from Melpa, completion at point for org-roam titles just stopped working.

See: [[Using fuzzy links AKA wikilinks in org-roam]] for info on how I originally set it up.

It's using key-chord-mode.

I guess first check is that still working.

key-chord part is working.  But completion is not.

Directly running `M-x completion-at-point` doesn't work either.

I have company-backends including company-capf, so what's the issue?

If I add in company-dabbrev, for example, then completion at point is working fine.  So something with capf is not working with org-roam.

If I do `org-roam-node-insert` or `org-roam-node-find`, the completion list comes up OK.  So org-roam seems to be aware of the list of nodes seems fine.

Also trying to see when we get anything with `org-roam-completion-everywhere`&#x2026;  Nope, also not working.

Describing company-backends variable:

> Its value is (company-capf)

Describing company-backends variable:

> Its value is
> (org-roam-complete-link-at-point org-roam-complete-everywhere)

So it all looks fine in term of config.

If I turn on `toggle-debug-on-error`, no debug is entered into when running completion at point.

Bit stuck with this now. Will have to come back.

-   <span class="timestamp-wrapper"><span class="timestamp">[2023-08-13 Sun] </span></span> For some reason, adding `(org-roam-db-auto-sync-enable)` in to my startup file seems to have resolved this.  Doesn't really make sense?  But hey ho, it's working again.


## Resources

-   [How to to get {{Title of the target note}} working with inline autocomplete i&#x2026;](https://org-roam.discourse.group/t/how-to-to-get-title-of-the-target-note-working-with-inline-autocomplete-in-org-roam/782/47)

