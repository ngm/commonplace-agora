# Capital



## A thing

Stuff that someone owns, in order to make more stuff.

> For Marx, capital is less a thing than a set of social relations
> 
> &#x2013; [[Half-Earth Socialism]]

Capital is made by labour.

> If the helmsman of Spaceship Earth is capital – rather than the capitalist and certainly not the worker – then it becomes clear not only why we have entered the choppy waters of the environmental crisis but also why it seems impossible to change course
> 
> &#x2013; [[Half-Earth Socialism]]


## A book

[[Karl Marx]]. [[Capitalism]].

> Capital’s three volumes were written to provide a theoretical arsenal to a workers’ movement for the revolutionary overthrow of the system—and to do so on the most scientific foundation possible
> 
> &#x2013; [[A People's Guide to Capitalism]]

