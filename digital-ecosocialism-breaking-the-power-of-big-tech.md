# Digital Ecosocialism: Breaking the power of Big Tech

An
: [[article]]

Author
: [[Michael Kwet]]

URL
: https://longreads.tni.org/digital-ecosocialism

First time I saw an explicit mention of [[digital ecosocialism]].

> In the space of a few years, the debate on how to rein in [[Big Tech]] has become mainstream, discussed across the political spectrum. Yet, so far the proposals to regulate largely fail to address the capitalist, imperialist and environmental dimensions of digital power, which together are deepening global inequality and pushing the planet closer to collapse. We urgently need to build a ecosocialist digital ecosystem, but what would that look like and how can we get there?

<!--quoteend-->

> This essay aims to highlight some of the core elements of a digital socialist agenda — a [[Digital Tech Deal]] (DTD) — centered on principles of [[anti-imperialism]], class abolition, reparations and [[degrowth]] that can transition us to a 21st century socialist economy.

<!--quoteend-->

> [[Digital socialism]], made reality by a Digital Tech Deal, offers the best hope within the short time frame we have for drastic change, but will need to be discussed, debated and built. It is my hope that this article might invite readers and others to build collaboratively in this direction.

Good starting platform outline.  Perhaps not enough on how the [[Revolutionary transition]] might happen.  Goes all in on [[Degrowth]], maybe needs some nuance on the framing issues of degrowth and engagement of working class.

The Digital Tech Deal principles are good, although the 'eco' part of it is just included in one principle, really - staying within planetary boundaries.  I feel it has been maybe been a late addition to the other 9 digital socialist principles.  More scope to flesh out the more eco leaning principles.


## How not to deal with big tech

Outlines the current weak reformist approaches that we should avoid.

> Progressive criticisms of the tech sector are often drawn from a mainstream capitalist framework centered around [[antitrust]], human rights and worker well-being.

-   [[Anti-trust and big tech]]
-   [[intellectual property]]

> The likes of Google, Amazon, Meta, Apple, Microsoft, Netflix, Nvidia, Intel, AMD and many other firms are so big because they own the [[intellectual property]] and [[means of computation]] that is used across the world.

<!--quoteend-->

> Instead of opposing Big Tech corporations in principle, European policymakers are opportunists seeking to expand their own portion of the pie.

<!--quoteend-->

> Other proposed reformist capitalist measures, such as progressive taxation, the development of new technology as a public option, and worker protections still fail to address root causes and core problems.

<!--quoteend-->

> Progressive digital capitalism is better than neoliberalism. But it is nationalist in orientation, cannot prevent digital colonialism, and it retains a commitment to private property, profit, accumulation and growth.


## Big tech and climate change

Outlines how big tech is complicit in climate change and other ecological destructions.

> Other major blindspots for digital reformists are the twin crises of [[climate change]] and ecological destruction that imperil life on Earth.

<!--quoteend-->

> [[Degrowth]] must be implemented in the immediate future. Slight reforms to capitalism touted by progressives will still destroy the environment. Applying the precautioonary principle, we cannot afford to risk a permanent ecological catastrophe. The tech sector is not a bystander here, but now one of the leading drivers of these trends.

([[Big tech is complicit in climate breakdown]])

> According to a recent report, in 2019, [[digital technologies]] — defined as telecommunications networks, data centers, terminals (personal devices) and IoT (internet of things) sensors — contributed 4 percent of greenhouse gas emissions, and its energy use has increased by 9 percent per year.

^ worth double checking that report.

Interesting also to see digital technologies defined as "telecommunications networks, data centers, terminals (personal devices) and IoT (internet of things) sensors"

> Companies like Apple claim to be “carbon-neutral” by 2030, but this “currently includes only direct operations, which account for a microscopic 1.5 percent of its carbon footprint.”

-   [[Mining for minerals used in electronics is often ecologically destructive]]
-   [[Digital companies support other forms of unsustainable extraction]]

> resource use tracks tightly to GDP growth across history

<!--quoteend-->

> Researchers recently found that shifting economic activity to services, including knowledge-intensive industries, has limited potential to reduce global environmental impacts due to the increase in levels of household consumption by service workers.

<!--quoteend-->

> If capitalism is ecologically unsustainable, then digital policies must accommodate this stark and challenging reality.

-   [[Digital Socialism]]
    -   existing examples
        -   [[Libre software]]
        -   [[Creative Commons]]
        -   [[data commons]] like [[DECODE]]
        -   [[Platform coops]]
        -   [[Alternative social media]] such as the [[Fediverse]]
    -   policy support is needed
    -   socialisation of infrastructure

-   The [[Digital Tech Deal]]
    -   part of a planned transition from digital capitalism to digital socialism

> Big Tech corporations, intellectual property and private ownership of the means of computation are deeply embedded into the digital society, and cannot be turned off overnight. Thus, to replace [[digital capitalism]] with a socialist model, we need a planned transition to digital socialism.

<!--quoteend-->

> Environmentalists have proposed new “deals” outlining the transition to a green economy. Reformist proposals like the US Green New Deal and European Green Deal operate within a capitalist framework that retains the harms of capitalism, such as terminal growth, imperialism and structural inequality.

<!--quoteend-->

> However, neither these red nor green deals incorporate plans for the digital ecosystem, despite its central relevance to the modern economy and environmental sustainability. In turn, the digital justice movement has almost entirely ignored degrowth proposals and the need to integrate their assessment of the digital economy into an ecosocialist framework. Environmental justice and digital justice go hand-in-hand, and the two movements must link up to achieve their goals.

-   [[Digital Tech Deal]]
    -   values: [[Anti-imperialism]], [[sustainability]], [[social justice]], [[worker empowerment]], [[democratic control]], [[class abolition]]
    -   principles:
        -   keep digital economy within social and planetary boundaries
        -   phase out intellectual property
        -   socialise physical infrastructure
        -   replace private investment of production with public subsidies and production
        -   decentralise the internet
        -   socialise the platforms
        -   socialise digital intelligence and data
        -   ban forced advertising and platform consumerism
        -   Replace military, police, prisons and national security apparatuses with community-driven safety and security services
        -   end the digital divide


## How to make Digital Socialism reality

Transition.

**Education**.

> First, it is essential to raise awareness, promote education and exchange ideas within and across communities so together we can co-create a new framework for the digital economy. In order to do this, a clear critique of digital capitalism and colonialism is needed.

**Networks/organisation**.

> Second, we need to connect digital justice movements with other social, racial and environmental justice movements.

**Agitation**

> Third, we need to ramp up direct action and agitation against Big Tech and the US empire.

**Cooperative economy**

> Fourth, we must work to build tech worker cooperatives that can be the building blocks for a new digital socialist economy.

**Build alternatives**

> we should aim to abolish Big Tech and the system of digital capitalism altogether. And this will require building alternatives, engaging with tech workers, not to reform the unreformable, but to help work out a just transition for the industry.

**Tech professionals work with wider society**

> Finally, people from all walks of life should work collaboratively with tech professionals to develop the concrete plan that would make up a Digital Tech Deal. This needs to be taken as seriously as current green “deals” for the environment. With a Digital Tech Deal, some workers — such as those in the advertisement industry — would lose their jobs, so there would have to be a just transition for workers in these industries. Workers, scientists, engineers, sociologists, lawyers, educators, activists and the general public could collectively brainstorm how to make such a transition practical.

Not too different from [[resist, regulate, recode]] then.


## Log


### Reading

