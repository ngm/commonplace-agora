# Digitalisation and the Security State

Found at
: https://www.tni.org/en/video/digitalisation-and-the-security-state

Part of
: [[Digital Capitalism online course]]

[[Digitalisation]] and [[the Security State]].

> How is digitalisation being used by state systems of [[repression]] and [[control]]? Who is most impacted? What protections exist in liberal democratic regimes to protect [[civil liberties]] and what are their limitations? What examples exist of successful resistance against [[state digital surveillance]]?

