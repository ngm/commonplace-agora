# 10 year phone

https://10yearphone.com/

Smartphones should last at least 10 years.

> Key demands:
> 
> -   Phones that are actually repairable – easily openable, with parts that can be removed with accessible tools, and no software locks
> -   Batteries that are easily removable and replaceable without special tools.
> -   Software supports that last 10 years
> -   Spare parts and repair information that are accessible to everyone – not just professional repairers
> -   Repairs that are actually affordable and accessible – by addressing the cost of spare parts
> -   Information on how well a phone can be repaired compared to other phones on the market (repair score)

