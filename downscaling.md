# Downscaling

> Using supercomputers, scientists run models of the entire world at coarse resolutions, overlooking some of the small-scale dynamics of nature that are infeasible to include in such a huge simulation. Local scientists then use those large models to shape their base assumptions of how a smaller spatial area works, and run their own more detailed model on that region (e.g., a continent, a nation, or a city).  This system, which climate scientists call ‘downscaling’, recalls Beer’s [[viable system model]], in which there are interconnected and hierarchical modules that govern at different levels of complexity
> 
> [[Half-Earth Socialism]]

[[climate science]]

