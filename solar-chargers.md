# solar chargers

I was looking into solar chargers to charge my tablet and phone.

But then I realised we had an old one knocking around that will do the job.


## Resources

-   [Best solar chargers 2023: Battery packs and power panels for backpacking with&#x2026;](https://www.independent.co.uk/extras/indybest/gadgets-tech/phones-accessories/best-solar-chargers-panels-battery-b2108772.html)


## For cars

Just need to make sure the car has an OBD socket.

-   [ARE SOLAR BATTERY MAINTAINERS WORTH IT? - Halfords](https://blog.halfords.com/solar-battery-maintainer/)
-   [The best solar car battery charger | Car Maintenance | Car Magazine Products](https://products.carmagazine.co.uk/car-maintenance/car-care/best-solar-battery-chargers/)

