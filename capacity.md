# Capacity

> Our starting point is that there is a hidden potential within society which can be unleashed to substitute for physical energy. The most straightforward way we can represent this potential is by calling it capacity. This refers to people’s innate problem-solving ability; indeed we can go further and say there is an innate love of discovery, of posing and resolving problems.
> 
> &#x2013; [[The Entropy of Capitalism]]

<!--quoteend-->

> on the basis of a given population, if you can discover a social system which unleashes capacity, you will have released a latent free resource; by expanding this, you can in parallel reduce ecological depletion without any overall loss of welfare.

[[Prosperity Without Growth]]?

