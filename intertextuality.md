# intertextuality

> Since the late 1960s, postmodern literary theories, using notions such as intertextuality, started to question ideas of individual authorship and reveal the collective dimension of literary work.
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

