# Libraries

> @richdecibels Question for atheists: how else can we build a cathedral?
> 
> @ClearMask@merveilles.town
> 
> @richdecibels How about we use state funding to build non-denominational knowledge repositories and operate helpful public services out of them for society's under priveleged. We could enforce an attitude of hushed reverence in them to provide an environment conducive to concentration and reflection. We could call them&#x2026; Libraries?

[[public libraries]]

> Local libraries remain one of the most powerful examples of non-commodified local space and resourcesharing.
> 
> &#x2013; [[The Care Manifesto]]

