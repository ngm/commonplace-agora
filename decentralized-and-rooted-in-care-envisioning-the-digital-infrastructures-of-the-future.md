# Decentralized and rooted in care: envisioning the digital infrastructures of the future

URL
: https://branch.climateaction.tech/issues/issue-6/decentralized-and-rooted-in-care/

Authors
: Paola Mosso and Janna Frenzel

Looking at it as a kind of [[qualitative system dynamics model]] of a transition from big tech.

> One of the main points related to how we can rethink and rebuild connectivity beyond the extractive, privatized ‘Big Tech’ infrastructures that dominate our current moment. Power and resources were key to the conversation. Who owns, controls, and profits off digital infrastructures, and how might this power be reclaimed, distributed and integrated into local economies instead of benefitting CEOs and shareholders primarily in the Global North? How are financing mechanisms and organizational capacity for digital infrastructures shaped by global intellectual property law regimes and copyright? And how can these legal structures be challenged or suspended, especially in emergencies?

Deprivatization of digital infrastructures.

> Our conversations showed that, in order to get to the futures we want, the communities that hold and sustain digital infrastructures need to be able to maintain them, and be acknowledged in co-responsibility for their labor. Crucially, they also need to be able to change these infrastructures or phase them out, have them die and become compost. This is easier when some of these infrastructures are based on natural materials, such as bamboo antennas, which also advantage community maintenance.

Agency and democracy.  Convivial technologies.

> It is difficult for smaller communities or worker cooperatives to own and run large physical infrastructures, like undersea cables or data centers. But grassroots groups can create, access and use free and open source software and set up peer-to-peer networks. This requires maintenance, necessitating the building of specialized capacities for these ‘homemade’, volunteer-run infrastructures, and the navigation of risks such as burnout and fundraising.

Municipal infrastructure.  Noting the need for some larger scale structures.

Initiatives:

> Today we celebrate initiatives around the globe that are seeding these desired landscapes and bringing them to life. Among the organizations and projects that inspire us are networks such as [[Rede Mocambos]] which organizes quilombola, indigenous, artist and artisan communities, the movement behind community data centers, indigenous-led streaming platform [[Nhandeflix]], self-hosted and community-maintained mesh networks such as [[NYC Mesh]] and [[Freifunk]] Berlin, radio stations such as [[Noís Radio]] in Cali/Colombia, [[Sursiendo]], the [[Alianza Mesoamericana de Pueblos y Bosques]], the ‘[[Decolonizing Digital Rights]]’ initiative of European Digital Rights ([[EDRi]]) and the [[Digital Freedom Fund]], the [[Numun Fund]], [[The Engine Room]]’s support to organisations in justice-based technologies and data decisions, the [[Solar Media Collective]], [[Rhizomatica]], the [[Patio]] co-op network, the Local Networks ([[LocNet]]) initiative, participatory mapping tool [[Mapeo]], the [[Design Justice Network]], the project for internet services [[Tiwa]] (Instituto Nupef), the [[Traversal Network of Feminist Servers]], and many others.

