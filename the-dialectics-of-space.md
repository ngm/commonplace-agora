# The Dialectics of Space

URL
: https://tribunemag.co.uk/2022/05/the-dialectics-of-space/

> It is a bit of a joke in the high-speed flippancy of the modern left to split the movement into two camps: you’re either pro- or anti- space

<!--quoteend-->

> If socialism is about working out where the limits are, and how best to tame our primordial minds towards the ‘good life’, then space is a mirage, a waste of effort, but if it is about thinking up plans without bounds for what might be possible on the long scale, Carl Sagan’s ‘a way for the universe to know itself’, then humans have got no real choice but to boldly go . . .

I am awed by the fathomless beauty of space, but despite this, I would probably count myself as 'anti-space' in this article's terms.


## Anti-space

> Anti-space leftists argue that the human journeys out of the atmosphere in the last seventy years have been little more than militaristic displays, high-technology spectacles searching for — at least — new gadgets for capital expansion or worse, total social control

<!--quoteend-->

> Furthermore, why should anyone spend all that money to get out of orbit when there are struggles on Earth to deal with first — and anyway, space is fundamentally boring!


## Pro-space

> Pro-space leftists look back fondly to the history of socialism, to the more cosmic pronouncements of Trotsky and the early Bolsheviks, and the cosmism that fed into the space race. If socialism is all about transcending limitations and restrictions, then why should socialists be forever stuck in Earth’s gravitational well? Isn’t that a betrayal of the very promise of communism, to pass beyond the limits of capitalism and unleash humanity into greater realms of freedom? And besides, space is fundamentally cool!

<!--quoteend-->

> Within pro-space positions, Scharmen identifies two basic types, which we might think of as the optimistic and pessimistic.

<!--quoteend-->

> The optimists see the wonderful achievements of industrialisation and technical growth, and wish to seek new opportunities, materials, and energy sources to permit this. The pessimists see the small size of the planet, the risks of human annihilation, and wish to spread humanity out, to unburden the limits to growth that determine life on Earth.

<!--quoteend-->

> [[Bezos]] is roughly the former of these, while [[Musk]] pronounces himself the latter, while a figure like Gerard O’Neill, whose failed 1970s campaign for Senate funding for space settlements perhaps marked the end of the post-war burst of space enthusiasm, was both — committed to a certain corporate optimism, but working from within an ecologically pessimistic background, capturing the contradictions of both

