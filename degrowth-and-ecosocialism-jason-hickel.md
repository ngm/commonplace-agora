# Degrowth and Ecosocialism | Jason Hickel

A
: [[podcast]]

URL
: https://www.planetcritical.com/p/degrowth-and-ecosocialism-jason-hickel

[[Degrowth]] and [[ecosocialism]] with [[Jason Hickel]].

Nice interview with Jason Hickel - he's always very engaging and eloquent on the topic.  Nothing particularly new to me but good overview of degrowth.  The interviewer Rachel Donald is obviously fully on board with degrowth though so it's a pretty easy ride in terms of debate.

Talks about how quality of life would actually be better.

