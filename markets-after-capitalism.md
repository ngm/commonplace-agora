# Markets after capitalism

From [[Markets in the Next System]].

[[Markets]] after [[capitalism]].

-   [[There are multiple types of markets]]
-   [[Markets for goods and services are compatible with socialism]]
-   "But the other markets are not nearly so capable in the resource-allotment division."
-   "Capital markets are prone to careening wildly between booms, when they allot far too many financial resources, and busts, when they allot far too few."
-   "Labor markets are volatile because of this careening, but even in the boom times, commonly maintain a reserve army of unemployed workers amid back-breaking overwork."
-   [[Capital markets must be socialized]]
-   [[Labour markets must be socialized]]
-   [[Intellectual property markets must be socialized]]
-   [[Land markets must be socialized]]

> A socialism like this, with capital, labor, ideas and land brought out of the market and into the democratic sphere, could accommodate markets in goods and services without imposing on all of society the imperatives unique to capitalism.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> Mass liberation from capitalist market imperatives would effect reparations for the dispossession that incited the capitalist laws of motion. The system would no longer have the necessity, or perhaps even the means, to impose its relentless imperatives on you and me and the whole wide world. 
> 
> &#x2013; [[Markets in the Next System]]

