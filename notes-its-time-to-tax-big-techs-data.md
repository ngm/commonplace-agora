# Notes: It's time to tax big tech's data

&#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

This article argues that the data that we create should be treated as a commons, and that we should tax the firms harvesting it.  Especially at a time when the firms are making even bigger profits from it, and we are most likely going to endure more austerity measures as a result of the pandemic. 

I agree with the redistribution of the wealth that big tech is hoovering up, but I'd say we should be working against the enclosure of the commons, not just letting it happen and then taxing it, no?  Counter [[surveillance capitalism]] through [[open protocols]] and supporting the building of an [[open web]].

> Big Tech is the big winner from the coronavirus crisis, raking in record profits. But their revenue comes from data we create collectively – and we should tax it for the public good.

Inequality has grown hugely in our generation.

> The government is currently lending billions to put the economy on life support. Their message has been clear: once this is over it will need to be paid back. 

i.e. [[austerity]].

> But austerity has not been our only ‘shared experience’ as a generation, we are also the first to experience the brave new world in which **the internet has infiltrated every aspect of our daily life and formed a new digital economy estimated at three trillion dollars**.

<!--quoteend-->

> Whereas the coronavirus is a nightmare for most of us, with **internet usage growing by up to 50% during lockdown** it is a dream for internet companies and data firms.

<!--quoteend-->

> also Amazon whose shares have increased by a third with customers reportedly spending almost $11,000 a second on its products. Facebook have outlined in a blogpost how their usage figures have massively increased during the lockdown

<!--quoteend-->

> “the three largest [[data brokers]] – Experian, Equifax and Transunion – each bring in over a billion dollars annually.

<!--quoteend-->

> An estimated figure based on this suggests a modest 2% tax would raise over a billion pounds per year. However, more must be done by governments to demand transparency by companies profiting from their use of our data to make it more quantifiable.

<!--quoteend-->

> Data forms part of our ‘commons’ – that which we have created or own in common. 

<!--quoteend-->

> As internet companies rake in enormous profits during the lockdown, we should seize the opportunity to make a claim on what we have created and hold it as a public wealth to be invested in services that benefit us collectively.

