# Information literacy

> Information literacy incorporates a set of skills and abilities which everyone needs to undertake information-related tasks; for instance, how to discover, access, interpret, analyse, manage, create, communicate, store and share information.
> 
> &#x2013; https://infolit.org.uk/ILdefinitionCILIP2018.pdf

<!--quoteend-->

> But it is much more than that: it concerns the application of the competencies, attributes and confidence needed to make the best use of information and to interpret it judiciously. It incorporates critical thinking and awareness, and an understanding of both the ethical and political issues associated with using information.
> 
> &#x2013; https://infolit.org.uk/ILdefinitionCILIP2018.pdf

