# Rise of the Robots

Exhibition.

Some random notes&#x2026;


## Rise of the robots


### History


#### Hephaestus


##### Blacksmith to the Greek Gods


#### Mechanical Turk


### Present


#### Terminator


#### Meta physical grey area


#### Overtly robot?


#### Robots made in our image


#### Comical servant class


##### Robots are muscle, workers


#### Asimov laws deliberately ambiguous


##### To allow fiction


##### But proper understanding can stop robot misdemeanors


#### Robot rights?


##### Human rights still recent invention&#x2026;


### Future


#### Contrast between intelligence and consciousness


##### Artificial intelligence here already


##### Artificial consciousness not yet


### Conclusion


#### Fear and wonder


##### Want them to be our slaves but tremble at their powers


##### Design them to do our work but worry they'll take our jobs


## Different degrees of humanness


## New s reader robot - blinks


## Obey


## Dream


## Build


## Human body


## Imagine

