# The virtual class

> Although companies in these sectors can mechanise and sub-contract much of their labour needs, they remain dependent on key people who can research and create original products, from software programs and computer chips to books and tv programmes. These skilled workers and entrepreneurs form the so-called 'virtual class': '&#x2026;the techno-intelligentsia of cognitive scientists, engineers, computer scientists, video-game developers, and all the other communications specialists&#x2026;'
> 
> &#x2013; [The Californian Ideology](https://www.metamute.org/editorial/articles/californian-ideology) 

