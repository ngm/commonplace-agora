# Rishi Sunak's Spring Statement 2022 is useless

[[Rishi Sunak]]'s [[Spring Statement 2022]] is useless.


## Because

-   [[Lack of help from Rishi Sunak for struggling UK families will push 1.3m into poverty]]
-   It will be the first large increase in number of people pushed into poverty outside of a recession ([Lack of help from Rishi Sunak for struggling UK families will push 1.3m into &#x2026;](https://www.theguardian.com/politics/2022/mar/24/lack-of-help-from-rishi-sunak-for-struggling-uk-families-will-push-13m-into-poverty))
-   Two out of every three pounds from the chancellor’s giveaways would go to those in the richest half of the population ([UK’s most vulnerable face crunch as Rishi Sunak helps better-off | Spring sta&#x2026;](https://www.theguardian.com/uk-news/2022/mar/23/uk-cost-of-living-rishi-sunak-fuel-duty-income-tax-spring-statement))
-   It has not shielded pensioners and those dependent on state benefits from the [[cost of living crisis]]  ([UK’s most vulnerable face crunch as Rishi Sunak helps better-off | Spring sta&#x2026;](https://www.theguardian.com/uk-news/2022/mar/23/uk-cost-of-living-rishi-sunak-fuel-duty-income-tax-spring-statement))
-   Benefits will rise by just 3.1% for the coming financial year. Cost of living could well rise by 10%. ([UK’s most vulnerable face crunch as Rishi Sunak helps better-off | Spring sta&#x2026;](https://www.theguardian.com/uk-news/2022/mar/23/uk-cost-of-living-rishi-sunak-fuel-duty-income-tax-spring-statement))

