# Sustainable Development Index

> The Sustainable Development Index (SDI), designed by anthropologist and author Dr [[Jason Hickel]], calculates its results by dividing a nation’s “human development” score, obtained by looking at statistics on life expectancy, health and education, by its “ecological overshoot”, the extent to which the per capita carbon footprint exceeds Earth’s natural limits.
> 
> &#x2013; [Cuba found to be the most sustainably developed country in the world, new res&#x2026;](https://morningstaronline.co.uk/article/w/cuba-found-to-be-the-most-sustainably-developed-country-in-the-world-new-research-finds?fbclid=IwAR36RqEUws0RyRcis1IgRbTkjDV9QF_GYsogziiW0RvKEcrAmPID60wGQ-c) 

Aims to improve over [[Human Development Index]].

> The HDI considers life expectancy, education and gross national income per capita, but ignores environmental degradation caused by the economic growth of top performers such as Britain and the US.
> 
> &#x2013; [Cuba found to be the most sustainably developed country in the world, new res&#x2026;](https://morningstaronline.co.uk/article/w/cuba-found-to-be-the-most-sustainably-developed-country-in-the-world-new-research-finds?fbclid=IwAR36RqEUws0RyRcis1IgRbTkjDV9QF_GYsogziiW0RvKEcrAmPID60wGQ-c) 

<!--quoteend-->

> Britain, ranked 14th in 2018’s HDI, falls to 131st in the SDI, while the US, 13th in the HDI, is 159th out of 163 countries featured in the new system.
> 
> &#x2013; [Cuba found to be the most sustainably developed country in the world, new res&#x2026;](https://morningstaronline.co.uk/article/w/cuba-found-to-be-the-most-sustainably-developed-country-in-the-world-new-research-finds?fbclid=IwAR36RqEUws0RyRcis1IgRbTkjDV9QF_GYsogziiW0RvKEcrAmPID60wGQ-c) 

