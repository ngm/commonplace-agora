# utopian socialism

I am pro-Utopianism and prefiguration.  That said, you still need a plan for [[revolutionary transition]].

> Condescension towards utopianism is not only poetically impoverished but also a tactical mistake, because it limits the Left’s ability to implement a socialist programme upon taking power. 
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> This means reviving the utopian socialist tradition that has for far too long been marginalized by the ‘scientific socialism’ of Marxism
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> What was a nuanced critique of utopian socialism in Marx’s hands became a bludgeon wielded by his epigones who scorned post-revolutionary proposals as frivolous ‘recipes for the cookshops of the future’.   
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> By the twentieth century, the utopian socialist tradition was in retreat. [[George Orwell]] – every conservative’s favourite socialist – excoriated vegetarians and sandals to expunge the Left of effeminate ridiculousness
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Critical Marxist theories of technology make use of dialectical analysis in order to avoid the determinism inherent in revisionist and Stalinist theories. In light of the rise of authoritarian capitalism (Fuchs 2018; 2019), socialism needs not only brilliant analyses of what is wrong in society, but also visions of 21st-century socialism that show how society can be transformed and what it can look like in the future in order to inspire contemporary class struggles. Perhaps contemporary socialist thought is sometimes too analytic. We need new inspirations from concrete-utopian socialism, from stories,
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> In the age of authoritarian capitalism, it is important that we dream about, imagine, talk about, envision, communicate, discuss, and struggle for concrete socialist utopias.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]


## Rough notes

So what is utopian socialism? Good question. That's kind of the point of the node club. But so what do I know so far?

Historically there is this kind of contrast between utopian socialism and scientific socialism and as far as I understand, Marx and Engels were proponents of what they called [[scientific socialism]], which is something more grounded in reality of the present day.

I think (although someone has said otherwise here recently to me, need to confirm) that they were against utopian socialism, or they had their critiques of it, at least.

Utopian socialism being more about thinking along the lines of the future. Of how would a future socialist society look.  They have this famous phrase against producing [[recipes for the cookshops of the future]]. They didn't want to do that. They just wanted to focus on the present day. That's a very rudimentary understanding, and I'm sure it's more nuanced than that.

Personally, I think we do need these recipes for the cook shops of the future. We need to prefigure the way that which we want the world to be and in order to do that, we kind of need to think about the way that we want the future to be.

(That's an interesting question, actually, what's the distinction between [[prefiguration]] and the ideas in utopian socialism?)

The other thing to add here is that I feel there's quite a nice link between [[speculative fiction]] and science fiction and thinking about future societies. So there's a nice link there between utopian socialism and speculative fiction, which obviously I'm a fan of. 


## See also

-   [[Socialism: Utopian and Scientific]]

