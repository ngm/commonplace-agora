# Revolutionary Strategies on a Heated Earth

An
: [[article]]

URL
: https://spectrejournal.com/revolutionary-strategies-on-a-heated-earth/

Subtitle
: Responding to Non-linear Developments in Condensed Time

Author
: Christian Zeller

[[Revolutionary strategy]].

Author's summary:

-   The [[Earth system]] is changing sharply and abruptly.
-   These abrupt disruptions in the climate and Earth system make all political ideas of a gradual socio-ecological transformation an illusion.
-   Only a strategy of revolutionary rupture is adequate and realistic to face this challenge.
-   What that strategy ought to look like is only discernible in a blurred outline.

The article aims to flesh that strategy out looking at [[eco-socialism]] and [[dual power]] in particular.


## Notes

-   The recent weather extremes are all signs that the Earth system is changing sharply and abruptly.
-   

