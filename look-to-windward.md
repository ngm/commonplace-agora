# Look to Windward

A
: [[book]]

Author
: [[Iain M. Banks]]

A [[Culture]] novel.

I'm enjoying it.  An intriguing plot with a clever technique for revealing events over time.

Gives a few insights in to what citizens of the Culture get up to, when they want for nothing.  As well as some details on the epic Orbitals of the Culture.  Francis Spufford in [[Red Plenty]] described it as a good exposition of the [[Marxian idyll]] of the Culture.

Not necessarily in a pro sense, but the Culture has a kind of geoengineering / Fully Automated Luxury Communism tinge to it.  e.g. the Minds, the orbitals.  This book describes in a lot of detail some of the artificially constructed geography of Masaq' orbital.  It made me think of [[Biosphere 2]] and [[Half-Earth Socialism]]'s evaluation of that project - how basically ecosystems are so complex that even such a tiny one the creators couldn't keep alive.  How would you then successfully construct such an immense artificial ecosystem one on the orbitals?

