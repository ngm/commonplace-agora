# Distributed Cooperative Organisation

DisCOs are:

-   Oriented towards social and environmental ends
-   Multi-constituent in nature
-   Active creators of [[commons]]
-   Transnational in nature
-   Centered on [[care work]]
-   Prototypes for new flows of values
-   Designed to be federated

> &#x2026;a set of organisational tools and practices for groups of people who want to work together in a cooperative, commons-oriented, and feminist economic form.
> 
> &#x2013; [Last Night A Distributed Cooperative Organization Saved My Life: A brief introduction to DisCOs](https://hackernoon.com/last-night-a-distributed-cooperative-organization-saved-my-life-a-brief-introduction-to-discos-4u5cv2zmn)

<!--quoteend-->

> This basically means that all nodes/persons can engage in consented, fluid modes of governance (something that recent anthropological studies tell us was the norm before the advent of predominantly vertical systems), and that while “peers” in a network are expected to have different talents, personalities and preferences, all have the same rights to contribute to the network and participate in its decisions.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> DisCOs are our proposal to use the power of distributed ledger and peer-to-peer technologies to prioritize taking care of human beings. They constitute an affirmative, entirely feasible vision for new and radical forms of ownership, governance, entrepreneurship, and financialization to fight pervasive economic inequality. At the same time, they focus on building synergies among related but often siloed sectors that urgently need to build better strategic alliances to develop creative, inclusive solutions.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> DisCOs, and the networked interlinkages between them, are meant to facilitate the creation of an entire postcapitalist counter-economy within the interstices of the existing capitalist economy.
> 
> &#x2013;  [Center for a Stateless Society » Review: A DisCO Manifesto](https://c4ss.org/content/52450) 

<!--quoteend-->

> DisCOs are our proposal to use the power of distributed ledger and peer to peer technologies to prioritize taking care of human beings.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> What are the needs we’ve identified as part of the DisCO vision?
> 
> -   Socio-environmental mission
> -   Multiple constituents served
> -   Actively creates commons
> -   Networks transnationally
> -   Care work basis
> -   Complementary value metrics
> -   Aimed toward federation, not scaling
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> At the same time, we actively reject the notion of [[techno-solutionism]] or implying that tech will solve what is, and should remain, primarily negotiated by people.
> 
> To be clear, distributed cooperative practices should never be solely dependent on technology, protocols or governance models. These are only tools to facilitate and strengthen our collaborative culture.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 


## Contrast [[decentralized autonomous organisations]]

