# Degrowth

Economic degrowth and the abandonment of GDP as a metric is needed, for sure, but I do see the problem with framing. Degrowth should be a part of a programme for eco-socialist economics but this one word shouldn’t be the leading prong.

I think degrowth chimes well with a professional-managerial class of climate activists with excess material comfort and corresponding carbon guilt, but less so with a working class already on the breadline. If you’re already near rock bottom and the environmental movement is suggesting we all need less, that isn’t going to resonate. And given the majority of the world is the working class, we need a better rallying cry.

A positive message of equity and redistribution would be better.

> Briefly put, advocates of degrowth call for an end to the fetishization of growth in contemporary society, a reduction in energy use and material throughputs in the Global North, and a globally just distribution of wealth and resources.
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> This is necessary, they claim, to phase out fossil fuels, regenerate the planet’s damaged ecosystems and attain a decent quality of life for all. Such a programme would require reductions in resource and energy use for many in the Global North, yet this need not lead to asceticism. Rather, its supporters argue it would enable communal luxury within ecological limits.
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> Though disagreements exist within the degrowth movement, most of its adherents envision a future where food production is localized, people have democratic control over issues that affect them, renewable energy infrastructure is decentralized and collectively owned, and public transportation is commonplace.
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> It should be clear from the above that degrowth is best understood as an element within a broader struggle for ecosocialist (and anti-imperialist) transformation. We must achieve democratic control over finance, production, and innovation, as well as organize it around both social and ecological objectives. This requires securing and improving socially and ecologically necessary forms of production while reducing destructive and less-necessary output.
> 
> &#x2013; [[On Technology and Degrowth]]

<!--quoteend-->

> This vision for degrowth requires revolutionary transformation in how we live our lives. Rather than mediating the pursuit of human and non-human needs through the profit motive,  degrowth focuses on the need for democratically planned production to directly deliver what everyone and everything needs to survive and flourish. All of this, degrowthers argue, is not just desirable but essential to provide a secure ecological niche for human and non-human life.
> 
> &#x2013; [[Forget Eco-Modernism]]


## Criticisms

> For left eco-modernists such as Huber and Leigh Phillips, this is a decidedly middle-class agenda – a ‘politics of less’ which, by calling on workers to reduce their energy and resource use, is destined to be unpopular and unattainable. For Huber, the problem is not the consumption habits of the Global North’s proletarianized groups; it is rather the activities of a capitalist class that consumes too much and profits from planet-destroying fossil fuels. As such, the antidote is class struggle, stronger unions and a parliamentary path to a Green New Deal.
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> From this perspective, degrowth’s inclination towards the local and the particular, and its relative silence on class struggle, create insurmountable barriers to socialist emancipation. In contrast, eco-modernists propose largescale nuclear energy projects, hydroelectric dams and industrialized agriculture, arguing that this is what it means to think and act in the Marxist tradition. Capital’s large-scale industry and exploitation of the world’s producers sets the stage for its abolition through a working-class seizure of the means of production. In Huber’s words, ‘industrial capitalism makes emancipation and freedom possible for all of society. This vision of freedom through social control over industrial abundance is key to mobilizing the masses to the socialist fight.’
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> For left eco-modernists — as opposed to reactionary eco-modernists, or capitalists — degrowth is both unnecessary and politically poisonous. It’s unnecessary because technological advances in hydrogen fuel, carbon capture and storage, nuclear energy, and renewable energy systems means that a high-consumption lifestyle for all is possible providing capitalism is abolished and workers take control of the means of production. It’s politically poisonous because, as Cale Brooks writes in Damage Magazine, degrowth is a ’politics of less’ that cannot rally support among workers who are already struggling to make ends meet.
> 
> &#x2013; [[Forget Eco-Modernism]]


## Articles

-   [Economic Planning and Degrowth: How Socialism Survives the 21st Century](https://newsocialist.org.uk/economic-planning-and-degrowth/)
    -   Degrowth
    -   Through central economic planning
    -   Using the tech of e.g. Amazon but for managing transition and living within means
    -   Didn't get so much from the article. More concrete actions would have been good.

-   [Center for a Stateless Society » We Are All Degrowthers. We Are All Ecomodernists](https://c4ss.org/content/52500)


## Resources

-   [The best books on Degrowth from a fellow traveller](https://shepherd.com/best-books/degrowth-from-a-fellow-traveller)

