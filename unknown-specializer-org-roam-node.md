# Unknown specializer org-roam-node

My customisation of slug function stopped working after a spacemacs update.

```elisp
(cl-defmethod org-roam-node-slug ((node org-roam-node))
  (let ((title (org-roam-node-title node)))
    (commonplace/slugify-title title)))
```

In my error messages I have:

> (Spacemacs) Error in dotspacemacs/user-config: Unknown specializer org-roam-node

Searching around:

-   [jethrokuan/org-roam-guide#2 error: Unknown specializer org-roam-node](https://github.com/jethrokuan/org-roam-guide/issues/2)

The relevant bit of which seems to be:

> ;; This needs to be after the \`org-roam’ declaration as it is dependent on the
> ;; structures of \`org-roam'.

Not sure why this has changed with the update that I did.

But I'll try their `with-eval-after-load` solution, just wrapping that around the function so it loads at the right time.

```elisp
(with-eval-after-load 'org-roam
  (cl-defmethod org-roam-node-slug ((node org-roam-node))
    (let ((title (org-roam-node-title node)))
      (commonplace/slugify-title title)))
  )
```

OK, now I'll try creating a new node.

Yep, worked!

