# using the News app from f-droid to connect to Miniflux on Android

[[Miniflux]] has got a decent enough mobile-friendly webapp, but I generally prefer a native app where possible.

Trying out [News](https://f-droid.org/packages/co.appreactor.news/).  It specifically lists Miniflux as something it supports.

All fine so far - it works well.

