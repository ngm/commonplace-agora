# iFixit

> Our earliest allies might worry about iFixit’s impartiality. Can we stay committed to our calls for repairability while joining forces with the manufacturers we’ve been fighting for decades? We think so. iFixit is lending our experience to those late to the party, but we are not here to hand out stamps of approval. Implementing the Right to Repair will require not just forward-thinking legislators, international standards makers, and lawyers, but also voters and fixers like you—and yes, manufacturers willing to make changes. We will continue to score device repairability and take manufacturers to task for unrepairable designs. We’ve pledged to remain objective—we have processes in place to ensure it—and our partnerships have transparency in mind. iFixit will always remain iFixit—April Fools jokes aside. We want you to be part of the fight, and we need to maintain your trust to do so.
> 
> &#x2013; [[Manufacturers are Joining the Fight for Repair]]

