# Copyleft is the software equivalent of right to repair

[[copyleft]] is the software equivalent of [[right to repair]].

I endorse this sentiment.

> Hot take: copyleft is the software equivalent of #RightToRepair.
> 
> https://mastodon.technology/@rysiek/107791721166281962

