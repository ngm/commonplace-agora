# Dynamic knowledge repository

> The dynamic knowledge repository (DKR) is a concept developed by Douglas C. Engelbart as a primary strategic focus for allowing humans to address complex problems.
> 
> &#x2013; [Dynamic knowledge repository - Wikipedia](https://en.wikipedia.org/wiki/Dynamic_knowledge_repository)  

