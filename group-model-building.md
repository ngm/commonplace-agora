# Group Model Building

A form of [[participatory modelling]].

> GMB is an effective way of capturing the stakeholders’ perceptions and values [51]. Perceptions refers to the way in which people interpret the world by recognizing external stimuli and reacting to them with actions [51]. However, the ultimate goal of a GMB intervention is not only to develop a model of the researched system, but also to delineate a shared understanding of a problem and foster proposed solutions [23] as stakeholders might have different and ambiguous opinions about the problem [52]. GMB is useful when the problem is difficult to identify or when the situation is so complex that no entry point for solutions can be easily identified
> 
> &#x2013; [[Identifying Strengths and Obstacles to Climate Change Adaptation in the German Agricultural Sector: A Group Model Building Approach]]

Delineate a shared understanding and foster proposed solutions.

> With GMB it is possible to model individual as well as collective perspectives and to include them in the planning process
> 
> &#x2013; [[Identifying Strengths and Obstacles to Climate Change Adaptation in the German Agricultural Sector: A Group Model Building Approach]]

