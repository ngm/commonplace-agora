# Zoonotic disease

> It is now widely accepted that prior to the rise of [[animal husbandry]], humans suffered no [[disease]] apart from the occasional parasite or unlucky infection, suggesting that the pathogens that now plague humanity ultimately come from other animals
> 
> [[Half-Earth Socialism]]

