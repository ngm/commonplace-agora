# Global temperature

> Current temperatures globally are, on average, 1.1C above pre-industrial temperatures.
> 
> [IPCC: Half of global population 'highly vulnerable' to climate crisis impacts&#x2026;](https://www.edie.net/ipcc-half-of-global-population-highly-vulnerable-to-climate-crisis-impacts/)

