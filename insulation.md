# Insulation

> A second analysis has found high gas prices mean the energy bills of people living in poorly insulated homes will rise by up to £246 a year. [[Insulate Britain]], a protest group demanding a legally binding national plan to fund low-carbon retrofits of all homes by 2030, has blocked motorways, A-roads and the port of Dover in recent weeks
> 
> &#x2013; [UK’s home gas boilers emit twice as much CO2 as all power stations – study | &#x2026;](https://www.theguardian.com/environment/2021/sep/29/uks-home-gas-boilers-emit-twice-as-much-co2-as-all-power-stations-study) 

<!--quoteend-->

> The UK has some of the most poorly insulated housing stock in Europe, which means it takes more energy to keep us warm throughout the winter.
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]

<!--quoteend-->

> This month a Guardian investigation found that adding insulation to lofts, wall cavities and windows could more than halve a household’s energy bills and annual emissions.
> 
> &#x2013; [[Cost of Living Campaigns Should Fight for a Green Transition]]

-   [[The UK should have a national programme of home insulation]]

