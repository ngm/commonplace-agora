# principles for ethical technology



## Various sets of principles

-   [Ind.ie — Ethical Design Manifesto](https://2017.ind.ie/ethical-design/)
-   [Tech Pledge](https://www.techpledge.org/)
-   [[Declaration of Digital Autonomy]]
-   [[An ethics of agency]]
-   [[Information Civics]]
-   [[DWeb principles]]


## My ramblings

After reading [[New UK tech regulator to limit power of Google and Facebook]], some thoughts on a better framing for some of what the government thinks is needed in tech.

There are plenty of existing ethical frameworks out there, this is just me thinking things through myself (plus nobbing around with PlantUML).

```plantuml
node innovation
node competition
node "consumer choice" as consumer
node growth

node liberation
node cooperation
node agency
node sustainability

innovation --> liberation
competition --> cooperation
consumer --> agency
growth --> sustainability
```

innovation &#x2013;&gt; [[liberation]]
competition &#x2013;&gt; [[cooperation]]
consumer choice &#x2013;&gt; [[agency]]
growth &#x2013;&gt; [[sustainability]]

[[Nick Sellen]] [made](https://social.coop/@nicksellen/105287661314118764) a couple of good points about some of the words.  Are liberation and agency the same thing?  What does sustainability actually mean?

I also thought about [[Maintenance]] as the counter to innovation, a la [[Hail the maintainers]].


## Praxis

So in those broad areas, what is good [[Praxis for the hacker class]]?

