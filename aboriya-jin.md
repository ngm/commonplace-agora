# Aborîya Jin

> the economy branch of Kongra Star
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> In accordance with the leading role of the women’s liberation struggle in the political project of AANES, women’s cooperatives are promoted by the cooperative bureaus which exist in every region. They are also promoted by Aboriya Jin, the economy branch of Kongra Star, the umbrella organization of the women’s movement in NES. These cooperatives are founded to create employment opportunities for women and to support their economic independence, in the framework of collective economy.
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

<!--quoteend-->

> As for the women’s worker cooperatives established by Aboriya Jin, there are over 50 across all of NES. 32 of them are agricultural cooperatives (18 of which are located in the Jazira region alone). The other cooperatives are bakeries, clothing shops, restaurants or workshops producing conserves. Approximately 1500 women work in these cooperatives, of whom around 1000 work in agricultural cooperatives.
> 
> &#x2013; [[A Syrian democratic social economy in the making]]

