# Hegemony

> Hegemony, as the concept has been used in radical theory, going back to the work of [[Antonio Gramsci]], concerns the ideological dominance of a particular politics.
> 
> &#x2013; [[Anarchist Cybernetics]] 

