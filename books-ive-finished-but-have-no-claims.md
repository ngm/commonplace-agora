# Books I've finished but have no claims

Books that I've finished, but haven't yet added any [[claims]] from them to my garden.

(This query is a work in progress, so isn't including all books without claims right now, currently only those nodes tagged 'book' without claims.  There's a lot more of them. Also, a book might refer to an existing claim, not necessarily have made a new one, and that'll count it as having a claim.)

-   [[The Shock Doctrine of the Left]]

See [[Finding books without claims in org-roam using org-babel and sqlite]] for info on how I generate this info.

