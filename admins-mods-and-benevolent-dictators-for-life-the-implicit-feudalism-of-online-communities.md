# Admins, mods, and benevolent dictators for life: The implicit feudalism of online communities

URL
: https://osf.io/gxu3a/?view_only=11c9e93011df4865951f2056a64f5938

DOI
: https://doi.org/10.1177%2F1461444820986553

Author
: [[Nathan Schneider]]

[[implicit feudalism]].
[[Feudalism]].

> By implicit I mean that while platforms may not explicitly proclaim or seek to practice feudal ideology—to the contrary, many claim democratic ideals—a feudalism lurks latent in the available tools that guide and limit user behavior

<!--quoteend-->

> I do not use feudalism in a historically precise sense, as there is much to distinguish online communities from the medieval European regime of land tenancy and its lord-vassal relations. Rather, I use the word metaphorically to describe concurrent communities across a network, each subject to a power structure that is apparently absolute and unalterable by those who lack such power.

