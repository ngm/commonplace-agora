# Angela Davis

[[Feminist]], [[Marxist]], [[activist]].

> [[Marcuse]]’s most famous student
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> not only a tireless advocate of the great utopian project of today – [[prison abolition]] – but also a [[vegan]]
> 
> &#x2013; [[Half-Earth Socialism]]

