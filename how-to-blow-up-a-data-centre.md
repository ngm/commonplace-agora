# How to Blow Up a Data Centre

It would be interesting to see how the logic of climate activism in [[How to Blow Up a Pipeline]] (i.e. sabotage rather than pacifism) applies to the ICT sector.  Does non-violence and pacifism work?  What actually is non-violence and pacifism in the tech sector?

Noting: as with Malm's book, not actually blowing things up.  Don't be like this guy:

-   [Texas man who wanted to blow up Amazon data center sentenced to 10 years](https://www.nbcnews.com/news/us-news/texas-man-who-wanted-blow-amazon-data-center-sentenced-10-n1280615)
-   [A Far-Right Extremist Allegedly Plotted to Blow Up Amazon Data Centers | WIRED](https://www.wired.com/story/far-right-extremist-allegedly-plotted-blow-up-amazon-data-centers/)

See [[How to stop a data center]] for an example of a non-violent approach that seemed to work in Chile.

