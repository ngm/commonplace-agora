# Questions about my digital garden



## 1. What first inspired you to start your digital garden?

I have been writing online in one form or another since I was a teenager.  But, a few years ago, discovering software called [[org-roam]] led me to the present form of my digital garden.

Briefly, without wanting to bog us down in the tools themselves: org-roam is an extension to something called [[org-mode]], which is an extension to something called [[Emacs]], a long-standing free/open-source text editor.  I had been using org-mode to record my notes locally for a while, but feeling a bit dissatisfied with it.

When I discovered org-roam, things really started to click for my note-taking.  This is less so because of the tool, more so the patterns it encapsulates: freely writing and linking your notes together, helping you find them again, nurturing them to grow over time.

The development of org-roam itself was (I believe) inspired by [[Roam Research]] and [[Zettelkasten]], both of which had a bit of a moment in the sun a few years back.

Ultimately, though, the inspiration for keeping my digital garden is simply being interested in things, and wanted to both keep track of what I learn and encourage me to share it with others.  And partly a desire to avoid the [[illusion of knowing]] - rather than just reading a lot of things and forgetting about them, encouraging myself to engage more closely and actively.


## 2. When did you first start your garden (online or in private)?

In its current org-roam based incarnation, since March 2020.

However&#x2026; if you start to get into the weeds (sorry for the pun) of what a digital garden is, in some ways I've been doing it for longer.

As mentioned, I have been posting online in one form or another for a long time.  First in a few of those journal platforms that were around back then.  Then blogging on my own site for a while.  A brief, unfortunate detour into web 2.0 social media platforms.  Then, about 10 years ago, playing around with static site generators rekindled my interest in hosting and posting on my own website.  Not long after that, I discovered the [[IndieWeb]] community, and with that I really doubled down on doing things via my own website, not somebody else's platform.  That expanded into posting my notes to my own site, too, not just streaming/microblogging, which then became my digital garden.


## 3. How would you describe your digital garden to someone unfamiliar with the concept?

It's a place to inspire me to write, to learn and to create.  It's also a way to share those ideas without worrying about them needing to be polished or perfect.  It's a space of your own on the web, not subject to the algorithms and desires of a big tech company.  It's a mess, it's a maze, it's weird, it's wonderful.


## 4. How do you decide what content to include or how to organize it?

There's not really a solid decision process behind this, but it goes something like this:

I have two gardens - one public and one private.  (Bill Seitz refers to these as your inner and outer gardens (http://webseitz.fluxent.com/wiki/TendingYourInnerAndOuterDigitalGardens))
Anything related to my private life goes in my inner garden and will never be shared.  Everything else I put in my 'outer' garden, which I publish online.

The content of my public garden is just a sprawling mass of, essentially, Things That I Am Interested In (Or Have Been At Some Point).  I tend not to explicitly filter or organise this - for me the digital garden is a place of expression and exploration that grows organically.  There is a little bit of structure in that I tend to write firstly in 'journal' pages, which is more the stream of thoughts, and anything I'd like to crystallize a bit further I will push through into one of the other pages in the garden on that topic.

I occassionally tend my garden, remove the odd weed, but not that often.  While I always encourage people to have a wander around, it's more of a wildflower meadow than a show garden.


## 5. How does it reflect your personal philosophy or approach to learning and creativity?

My digital garden reflects my personal philosophy and approach to learning and creativity very closely.   I believe that the learning process should allow you to explore freely and make mistakes - a digital garden fosters this by removing the expectation that what I write should be perfect, or complete, before it is ever shared.   I also use it as a space for both divergent thinking, a grab bag of many threads of ideas, and convergent thinking, where I can narrow those ideas down into something cohesive.  Some people say that it's important to be a creator, not just a collector, but I have no issue if it sometimes just a space to collect snippets of knowlege - bookmarks, ideas and quotes from other people.


## 6. Has the process of maintaining your digital garden changed how you think about knowledge, organization, or growth?

Yes.  It has pushed me to appreciate knowledge more as a shared, evolving thing, rather than something you perfect in private before releasing to the world.  It has also made me double down on an appreciation of subjective, rather than objective knowledge.  On any given topic I am more interested in what someone in my trusted network has written about it before I go to Wikipedia for the 'objective' view on it.

Digital gardens certainly raise questions about organisation - is it better organise horizontally or vertically?  A lot of discussions around digital gardens emphasise more of an unstructured, organic, horizontal, rhizomatic approach to organisation, which I fully enjoy as a personal learning approach, but I don't wish to do this to the exclusion of vertical and narrative structure, threading thoughts together, which is occassionally vital as a way of communicating your ideas to others.

With regards to growth, I occassionally check in on how many pages of notes I have, which is fun but ultimately meaningless.   Better to keep track of how many meaningful notes you have ([[Andy Matuschak]] calls these [[Evergreen notes]]; [[Ton]] uses the terms [[notion]]s).  Andy says that the number of Evergreen notes written per day is the best indicator of your success as a knowledge worker.  I disagree - to me, steady output of units of knowledge isn't the end goal of my digital garden.  Learning and sharing is.  I [[prefer knowledge commoning over knowledge work]].  The goal is 


## 7. Do you see your digital garden as more of a personal space, or does it also act as a tool for community and connection?

While my digital garden is primarily a personal space, scratching my own itches, I keep it open and public in the hopes of connecting to like-minded others, and I fully intend for it to be part of a larger knowledge commons.  I like the idea of the [[Nested-I]] - individual identities (gardens) joining together to build a greater collective.

The simple act of publishing it online, and asking people to contact me via email if they find something interesting, has itself brought about lots of interesting connections (this one included!).

There are other ways of connecting gardens together, such as [[sister sites]] and [[twin pages]].  Everything that I add to my digital garden I also cross-post to Anagora (https://anagora.org), which is a community that is building a shared prosocial knowledge commons.


## 8. Do you see digital gardens as evolving archives or more as real-time, personal thinking spaces?

Both!  The line is blurry but I tend to use my garden as both garden and stream ([[The Garden and the Stream: A Technopastoral]]).  My journal pages are my real-time, personal thinking space, the notes from which tends to coalesce into more considered pages and thoughts, an evolving archive.

