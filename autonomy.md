# Autonomy

> To reapply this in a modern context, therefore: digital privacy—and its philosophical twin, freedom—involves anonymity, secrecy, and autonomy. Autonomy is not just evading surveillance. Autonomy means the freedom to act without being controlled by others or manipulated by covert influences.
> 
> &#x2013; [[Future Histories]] 

The [[IndieWeb]] as a space for online autonomy: [Autonomy Online: A Case For The IndieWeb — Smashing Magazine](https://www.smashingmagazine.com/2020/08/autonomy-online-indieweb/)


## Flashcard


### Front

What is autonomy?


### Back

The capacity to make an informed, uncoerced decision.

