# What’s the Value of Data? (ft. Salomé Viljoen)

A
: [[podcast]]

Found at
: https://www.patreon.com/posts/305-whats-value-95039111

Part of
: [[This Machine Kills]]


[[Social data]].

Has [[predictive value]].

Three ways to extract [[surplus value]] with it:
    -   1. Just sell it on, e.g. data broker
    -   2. Use it to exploit people based on knowledge from the data
    -   3. Use it to exert power (e.g. Uber's [[Greyball]] program)

