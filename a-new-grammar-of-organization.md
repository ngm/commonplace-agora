# A New Grammar of Organization

A
: [[podcast]]

URL
: https://roarmag.org/2021/06/03/spadework-a-new-grammar-of-organization/

Series
: [[Spadework]]

Featuring
: [[Rodrigo Nunes]]

Nice discussion with Rodrigo Nunes on [[Neither Vertical Nor Horizontal]].  Spadework comes at things from the perspective of practical organising.

-   Interesting definition of a radical from nunes on spadework
-   Political action needs to look closely at 'what is' rather than just 'what ought to be'
-   Technological affordances allow for spontaneous mass movements
-   We have the opportunity to combine mass movements with organisation

