# Notes: Big Tech Won't Save Us: The Case for Social Transformation over Coronavirus Surveillance

&#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

> Anxious about the upcoming economic fallout from an entire country on lockdown, state governments are more eager than ever to partner with tech companies to roll out technology to facilitate containment methods such as contact tracing.

[[Surveillance technology]] such as [[contact tracing]] being rolled out to track coronavirus.  By companies such as [[Palantir]].

> Many have likened the increased interest in surveillance as a response to the pandemic to the Patriot Act introduced in 2001 after 9/11.

<!--quoteend-->

> what much of the current coverage on this issue fails to consider is 1) the actual efficacy of this technology in the first place, and 2) the underlying structural issues that surveillance will only heighten if implemented.

<!--quoteend-->

> the purported benefits of the kinds of technology implemented for surveillance purposes are commonly overinflated

<!--quoteend-->

> Technologies in this field, much like the humans that create them, invariably have biases and inaccuracies, and these biases and inaccuracies disproportionately impact historically marginalized groups.

<!--quoteend-->

> It’s no coincidence that there are so many links between neo-nazis, massive digital surveillance, and the tech industry. 

<!--quoteend-->

> In the short and long-term, communities of color disproportionately bear the brunt of the structural flaws that plague the surveillance technologies being proposed today

<!--quoteend-->

> The important question facing society is whether a world in which movement is regulated through tech is actually what we want.

<!--quoteend-->

> The abuse of personal data post-crisis is well-documented in humanitarian interventions.

<!--quoteend-->

> Despite now being labeled as “essential” (what some consider mythmaking that sugarcoats exploitation), these workers have historically been deemed less valuable and receive low amounts of compensation

<!--quoteend-->

> COVID-mitigating applications like health surveys or **contact tracing only work if a critical mass of the population uses them**. 

<!--quoteend-->

> Palantir, the same company that supercharged mass deportation, is now contracting with the U.S. and the U.K. governments for COVID response.

<!--quoteend-->

> Integrating mobile apps that exploit location data to track whether someone may have come in contact with a COVID carrier **does not add more hospital beds, doctors or nurses in poverty stricken areas**. 

<!--quoteend-->

> Pouring funding into technology that can detect COVID infection from vocal signatures, **doesn’t provide the social safety nets that allow people to take time off from work when they are ill**. 

<!--quoteend-->

> The answer lies in not just an aggressive response to the pandemic, but **an aggressive response to structural inequality.**

<!--quoteend-->

> Experts across the board agree that social distancing is a necessary aspect of the pandemic response, but this is **only sustainable long term if people have the social safety nets available that can help incentivize staying home.**

<!--quoteend-->

> All in all, we need to dismantle the crisis of imagination in the global north, that **the solution for social problems must be more amenable to profits than the needs of the people**. 

