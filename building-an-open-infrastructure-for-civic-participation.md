# Building an open infrastructure for civic participation

URL
: https://opensource.com/article/21/4/open-source-cities

> What the deployment of Decide Madrid can teach us on creating a [[participatory culture]] to effectively engage with citizens.

<!--quoteend-->

> The challenge for Decide Madrid has been less about getting participation and more about getting the right kind of participation

Good article on some of the challenges of [[citizen participation platform]]s.  (Based on experiences from Decide Madrid / [[Consul]]).

-   [[A participatory culture requires a participatory infrastructure]].

The article suggests looking to open source software for examples of building [[participatory infrastructure]].  I get where that's coming from, but given OSS participation is often rife with problems, it might not be the best example.  Or maybe it is, for exactly that reason?

I feel it is slightly a classic tech view of the world - that we can look to open source to figure out how to govern the real world.

Related: [[free software governance]], [[What could governance online look like?]].

Funny how it mentions things like 'conversions' - e.g. getting a citizen visiting the platform to vote on a proposal.

[[infrastructure]].

