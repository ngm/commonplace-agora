# public-commons partnerships

I need to merge this and [[commons-public partnerships]].

-   the state as incubator, partner, supporter, broker

Some possible issues:

-   public-commons partnerships issue
    -   local authorities are large
        -   commons coop sector is small
            -   hard to find a common language that works well across that divide

-   local authorities are large
-   commons coop sector is small
-   hard to find a common language that works across that divide
-   e.g. how do you do catering for a huge hospital with PCP?
    -   one solution: cut up the tendering, cut up the services
    -   break it down in to smaller contracts, units
    -   let go of the idea of efficiency at scale
    -   adds complexity for the commissioning organisations, though
    -   digital works perhaps is able to scale better?

