# Digital divide

> Such an approach is a far cry from technocratic strategies to close the “digital divide” that aim merely at expanding access—and, further, imagine that doing so will solve poverty, a fantasy that scholar Daniel Greene calls “the access doctrine.” Connectivity is essential, but so is transforming how people connect
> 
> &#x2013; [[Internet for the People]]

