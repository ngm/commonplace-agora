# Sovereignty for the Commons

A
: [[podcast]]

URL
: https://theblockchainsocialist.buzzsprout.com/903769/10423365-ccg-chronicles-7-sovereignty-for-the-commons

[[Sovereignty]] for the [[Commons]].

Sarah Manski.

~00:06:03  Socio technical imaginary where you can imagine future you want and then build it.

~00:09:06  Sovereignty. In various spaces, nation, city, workplace, etc. Digital platforms usually pretty authoritarian.

~00:13:50 [[Gramsci]], [[interregnum]].

~00:32:25 I like her personal definition of [[cryptocommons]]: distributing wealth of planet to all people of planet using technology.

