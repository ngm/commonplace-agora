# Library of things

> Yet we can also move beyond books, to develop more ‘libraries of things’ and other forms of reuse and recirculation. In an era of imminent climate catastrophe, it is obscenely wasteful for people to buy hardware they might use only a few times a year, whether we are talking about power drills, expensive children’s toys or waffle makers. It’s possible to refuse the disastrous capitalist system of planned obsolescence and share objects within communities. As a result we would limit carbon emissions, save money, and develop our capacities to care not only for animate but also inanimate things.
> 
> &#x2013; [[The Care Manifesto]]


## Resources

-   Why Library of Things | Library of Things https://www.libraryofthings.co.uk/why
-   I want to start my own Library of Things! – Library of Things

https://libraryofthingshelp.zendesk.com/hc/en-gb/articles/9930922648337-I-want-to-start-my-own-Library-of-Things-

-   Grow the movement | Library of Things https://www.libraryofthings.co.uk/new-sites
-   LoT criteria for spaces November 2022 https://docs.google.com/document/u/0/d/1gxJNrTC9MwaxrGNaOYzmxW9Buwu3le7vEJKFAb0pkbk/mobilebasic
-   Can I donate items to Library of Things? – Library of Things https://libraryofthingshelp.zendesk.com/hc/en-gb/articles/9901064754193-Can-I-donate-items-to-Library-of-Things-

