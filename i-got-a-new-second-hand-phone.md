# I got a new (second-hand) phone

-   My [[Samsung Galaxy S5]] was just creaking too much.  It ran slow, I was permanently having to delete things, clear caches in order to make space.  16Gb storage is not enough space for me it turns out.  Photo quality was really poor.  I would have liked to have kept it - I like the form factor of it, nice size, physical button, back just pops off to replace the battery.  But hey ho.  It was released in 2014, I got it in 2019 and had it untill 2023, so it was almost a [[10 year phone]].
-   I've replaced it with a [[Google Pixel 5a]].  Mostly researched on [[sustaphones]] and some review sites.  Second-hand refurbished from [[Backmarket]].
    -   I got it because:
        -   Excellent support for custom ROMs (https://www.sustaphones.com/#barbet).
        -   6/10 repairability from iFixit (https://www.ifixit.com/Device/Google_Pixel_5a).
        -   Battery replacement is 'moderate' difficulty.  Meh.  At least it's not 'hard' (https://www.ifixit.com/Guide/Google+Pixel+5a+Battery+Replacement/148930).
        -   Screen replacement is 'moderate' difficulty too.  Meh. Often the case. (https://www.ifixit.com/Guide/Google+Pixel+5a+Screen+Replacement/148892)
        -   Spare parts provided by [[iFixit]] (https://www.ifixit.com/Parts/Google_Phone)
        -   It has a headphone jack.
    -   Initial thoughts:
        -   I don't like the form factor.  Stupidly long and thin, hard to use with one-hand (unless you turn on the stupid one-hand mode).
        -   Nice screen.
        -   Sooooooooooo. Muuuuuuuuuuch. Fasssssster than my old phone.
        -   Battery life is fantastic so far, compared to previous phone.
        -   Camera: a lot better.

