# Skills


<style>
table colgroup col:nth-child(1) {
    width: 30%;
}
table colgroup col:nth-child(2) {
    width: 10%;
}
</style>


## Tech


### Software engineering

f   


#### Concepts

| Skill                            | Experience points | Notes |
|----------------------------------|-------------------|-------|
| [[Behaviour-driven development]] |                   |       |
| Test-driven development          |                   |       |


### [[Coding]]

I used to code a lot, as it used to be my dayjob.  But I code less at work since 2017, and do the odd bits here and there both at work and in my spare time.


#### Languages

| Skill    | Experience points | Notes                                                                          |
|----------|-------------------|--------------------------------------------------------------------------------|
| PHP      | 50                | Work with it semi-regularly, but not day in day out.                           |
| Python   | 45                | Any glue scripts I'll use Python for them, but only semi-regularly.            |
| C#       | 35                | I used to write a lot of C#, but not since 2017, so would be fairly rusty now. |
| [[Lisp]] | 20                | Learning this on and off in my spare time.                                     |


#### Frameworks

| Skill         | Experience points | Notes |
|---------------|-------------------|-------|
| [[Laravel]]   |                   |       |
| [[WordPress]] |                   |       |
| ASP.NET MVC   |                   |       |


#### Tools

| Skill                 | Experience points | Notes            |
|-----------------------|-------------------|------------------|
| [[Emacs / spacemacs]] | 70                |                  |
| [[Git]]               | 70                | I use git a lot. |
| Vim                   | 60                |                  |
| Visual Studio         | 40                |                  |


### IT/SysAdmin

| Skill                                             | Experience points | Notes                                                            |
|---------------------------------------------------|-------------------|------------------------------------------------------------------|
| Linux/Apache/MySQL/PHP server maintenance         | 75                |                                                                  |
| Shell scripting                                   | 55                | Know my way around a terminal, but don't write a ton of scripts. |
| DNS configuration                                 | 70                |                                                                  |
| Nginx                                             | 45                |                                                                  |
| Let's Encrypt / Certbot                           | 50                |                                                                  |
| Backup and disaster recovery                      | 60                |                                                                  |
| SMTP and email deliverability                     | 50                |                                                                  |
| Configuration management / infrastructure as code | 30                | Toyed with Ansible a bit                                         |

