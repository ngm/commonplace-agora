# Space Cats Fight Fascism

A
: game

Made by
: [[Tesa Collective]]

Available at
: https://store.tesacollective.com/collections/games/products/space-cats-fight-fascism-the-board-game

A board/card game where you play as a bunch of cats to overcome a fascist regime.

We own this.  It's great fun.

![[images/space-cats.jpg]]

