# Jeff Bezos

> Jeff Bezos was a Wall Street hedge fund manager without a utopian bone in his body. He was pure calculation, relentless and methodical, the logic of capital personified. He saw a business opportunity with the internet and didn’t want to miss it
> 
> &#x2013; [[Internet for the People]]

