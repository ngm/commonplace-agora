# Actor model

> The actor model abstraction allows you to think about your code in terms of communication, not unlike the exchanges that occur between people in a large organization.
> 
> &#x2013; [How the Actor Model Meets the Needs of Modern, Distributed Systems • Akka Doc&#x2026;](https://doc.akka.io/docs/akka/current/typed/guide/actors-intro.html) 

The actor model is most applicable for only a certain set of problems, e.g. distributed systems.  The actor model is likely overkill if your program does not need to be distributed.
See e.g. https://clojure.org/about/state for good discussion.

