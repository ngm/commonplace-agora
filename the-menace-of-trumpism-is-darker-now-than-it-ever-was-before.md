# The menace of Trumpism is darker now than it ever was before

So sayeth this article in early 2022 - [The Trump menace is darker than ever – and he’s snapping at Biden’s heels | J&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/07/trump-biden-republicans-election-lies-midterms) 

Fucking depressing if true.

[[Donald Trump]], [[Trumpism]]


## Because

-   Joe Biden isn't popular
    -   Biden has the lowest approval rating of any US president at this stage of his term (barring Trump himself)
-   Republican politicians fear Trump and they fear his supporters
-   Two-thirds of Republicans still think the 2020 election was rigged
-   Republican-run states are rewriting electoral law to make it harder to vote


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

I don't know, but the article seems well reasoned.  Sigh.

