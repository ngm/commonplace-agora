# How Degrowth Will Save the World with Jason Hickel

A
: [[podcast]]

URL
: https://www.upstreampodcast.org/conversations

Featuring
: [[Jason Hickel]]

Great interview. Challenging the idea that economies need to continuously grow.  Hickel calls that outlook [[growthism]].  Believes we need [[degrowth]].  Presents a comprehensive view of the problems and solutions that seems to go beyond just how you might think of the debate on 'degrowth'.  See [[Less is More]] for more.

~00:00:00  What do we want the economy to actually achieve?

~00:06:31  The inputs to production are labour and nature.

~00:06:53  [[Capitalism]] must take more from [[labour]] and [[nature]] than it gives back. This is its core logic.

~00:13:04  Capitalism and growth is like an aeroplane that has to keep accelerating in order to stay in the air.

~00:21:01  Productivity gains should result in shorter working week.

~00:30:36  Describes [[green growth]], degrowth, [[post-growth]], [[steady-state economy]].

~00:40:40 Jason likes [[Doughnut Economics]].  DE presents itself as somewhat growth-agnostic, and Kate Raworth doesn't like the term degrowth.  He says that where degrowth comes in is that developed countries need degrowth to get back in the doughnut.  Developing countries need growth to get into the doughnut.  Then, in both cases, the aim would be to stay steady within the doughnut.

~00:39:34  GDP does not bring happiness or other useful social outcomes.

~00:40:45  [[Universal basic services]] and distribution of income more fairly are the key drivers of positive social outcomes. Also democratic equality.

~00:43:14  [[Right to repair]] as part of degrowth.  Good general bunch of policy proposals.

~00:50:11  Upstream, you can trace a lot of problems back to [[dualism]] and a mechanistic rather than an animistic worldview.  [[Animism]].

