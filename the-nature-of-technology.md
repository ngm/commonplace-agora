# The Nature of Technology

A
: [[book]]

Author
: [[W. Brian Arthur]]

Handy overview of the book: [The Nature of Technology: What It Is and How It Evolves (My Notes)](http://www.the-vital-edge.com/the-nature-of-technology/)

Three pr8nciples:

-   All technologies are combinations of elements.
-   These elements themselves are technologies.
-   All technologies use phenomena to some purpose

