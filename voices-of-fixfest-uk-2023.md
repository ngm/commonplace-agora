# Voices of Fixfest UK 2023

A
: [[podcast]]

URL
: https://therestartproject.org/podcast/fixfest-uk-2023/

Really nice episode.  Interviews with various community organisers at [[Fixfest UK 2023]].

Quite a few of them represent places that have physical high street locations for some of their repair cafes (as well as Libraries of Things often too).

