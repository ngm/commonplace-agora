# data broker

> These firms exist for the sole purpose to collect and sell data on the market which then informs the huge artificial intelligence market for machine learning which is vital for future growth in many industries.
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

<!--quoteend-->

> “the three largest [[data brokers]] – Experian, Equifax and Transunion – each bring in over a billion dollars annually.
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

<!--quoteend-->

> The data brokerage industry is thought to be worth $200 billion a year and the European Commission suggests that the market in Europe could be worth €106.8 billion. These firms exist for the sole purpose to collect and sell data on the market which then informs the huge artificial intelligence market for machine learning which is vital for future growth in many industries.
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

<!--quoteend-->

> Individual data in itself isn’t important, rather it is our collective data which drives larger profits. Collective data allows for more market research, greater trend and behavioural analysis on a large scale. 
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

<!--quoteend-->

> But there is something unjust about this whole market in data. We have collectively created this data through our actions and labour, but it is owned by private companies that make huge profits off it. Our collective wealth is in private hands.
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

<!--quoteend-->

> As Chris Hughes has written in the Guardian “we have all pitched in to create a new commonwealth of information about ourselves that is bigger than any single participant, and we should all benefit from it.” What our labour has created should be ours to broker.
> 
> &#x2013; [It’s Time to Tax Big Tech’s Data](https://tribunemag.co.uk/2020/05/its-time-to-tax-big-techs-data/)

