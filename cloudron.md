# Cloudron



## Notes

Seems to be that they wrap all of the apps in their own docker containers, and this is what you are deploying.

Most definitely not end-user ready.  You have to know what you're doing.  I mean, it's super smooth, and you just run one script - but just getting to that point requires a lot of knowhow.

Setting up apps is super simple.  I like the fact that you can easily setup robots.txt to discourage bot indexing of login pages.

For syncthing, which uses basic auth so doesn't work with the other cloudron stuff, they made it very clear to change the admin credentials.


## Questions

Do Cloudron pay it forward in any way?  Do they try to support the upstream projects that they include in their hosting?

https://blog.cloudron.io/cloudron-is-now-open-source/
https://news.ycombinator.com/item?id=22191416

> Yes, that's the core of our tech stack. All our packages are opensource (git.cloudron.io). Important thing for us is to have all the docker containers read-only and have a 100% reliable backup/restore mechanism because we do automatic updates for apps to give a SaaS experience to self-hosting. We also provide SSO for users across apps (so you can add your family or colleagues in a cloudron and do access control across apps easily).


## Error: Cloudron requires '/' to be ext4

I tried Cloudron first on [[GreenHost]] - that resulted in the error:

> Error: Cloudron requires '/' to be ext4

Which led me here: [Error on generic host | Cloudron Forum](https://forum.cloudron.io/topic/1561/error-on-generic-host)

Where someone is having the same problem (GreenHost use xfs it seems).  I saw a list of alternative VPS providers there, and saw the excellent looking 1984, and spun up a VPS with them and tried again.  Same problem.

I followed the workaround mentioned:

> I just commented out the /ext4 requirement and Cloudron installed fine, and as far as I could tell for the 4 months or so I used it, it ran fine

This did work, but doesn't fill one with confidence&#x2026;

There is a comment in that thread that

> Currently only ext4 is supported, mostly for reasons of keeping testing burden low.

So maybe it's fine, it's just not officially supported.

@jdaviescoates there recommends hetzner as his favourite, so presumably they work fine out-of-the-box.  They have excellent prices, and do use 100% renewable energy.  I'd still prefer to use 1984 or GreenHost though if I can, as they use renewable too but also care about privacy and free software (not saying that hetzner doesn't, but they're not explicit about it).  Maybe I'll stick with one of those for my VPS for web hosting, and hetzner for Cloudron.


## Pros

>  Nowadays I am running Cloudron for these use cases. Here the big plus in apps is that each app runs on docker and has to use the Cloudron docker vase image (with very few paths having write access), through that apps and the server can easily backed up and restored in exactly the same state (user, data, apps installed). 
> https://news.ycombinator.com/item?id=18000949 

YunoHost seems to have a bigger app list.  But Cloudron better maintained.

> The only thing that might discourage people with cloudron is the pricing model but thinking it through its a huge win win : all upgrades are tested before landing on each cloudron instances, makes the upgrade system at the cloudron layer or operating system underneath automated. Yunohost you get to have your hands in the dirt and you have an easier path to learn since it’s mostly bash scripting where cloudron is essentially an interface to manage docker apps in a United easy to catch environment 
> 
> https://edgeryders.eu/t/stackless-why-is-it-so-difficult-to-stay-out-of-google-microsoft-amazon-tools/9422/23

