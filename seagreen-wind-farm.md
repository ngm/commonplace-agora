# Seagreen wind farm

A big [[wind farm]] in Scotland, going live in 2023.

> When it goes live next year, Seagreen wind farm will break records: [[Scotland]]’s largest, the world’s deepest. And this week the scale of the project was revealed as photos emerged of the foundations at dock before they were towed out to the North Sea.  
> 
> &#x2013; [What went right: the garbage patch cleanup, plus more positive news - Positiv&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-30-of-2022/) 

<!--quoteend-->

> The same height as Big Ben, they will host turbines that will stand almost as high as The Shard building in London. SSE, one of the firms behind the project, said the £3bn wind farm will produce enough energy to power 1.6m homes. 
> 
> &#x2013; [What went right: the garbage patch cleanup, plus more positive news - Positiv&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-30-of-2022/) 

1.6m homes is a lot.  There's about 28 million in total in the UK.

> “We are entering a golden age for clean energy in this country,” said Alistair Phillips-Davies, SSE’s chief executive. “Seagreen shows that net zero isn’t just about climate change. It’s about creating jobs, delivering major clean infrastructure, regenerating rural communities and ultimately helping us secure our own energy future.”
> 
> &#x2013; [What went right: the garbage patch cleanup, plus more positive news - Positiv&#x2026;](https://www.positive.news/society/positive-news-stories-from-week-30-of-2022/) 

I like the [[Green New Deal]] sentiment.

