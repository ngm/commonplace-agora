# The Entropy of Capitalism

A
: [[book]]

URL
: https://brill.com/display/title/17387?language=en

> The project of applying general [[systems theory]] to social sciences is crucial in today’s crisis when social and ecological systems clash. This book concretely demonstrates the necessity of a Marxist approach to this challenge, notably in asserting [[agency]] (struggle) as against determinism. It similarly shows how [[Marxism]] can be reinvigorated from a systems perspective. Drawing on his experience in both international systems and low-input agriculture, Biel explores the interaction of social and physical systems, using the conceptual tools of [[thermodynamics]] and [[information]]. He reveals the early twenty-first century as a period when [[capitalism]] starts parasitising on the chaos it itself creates, notably in the link between the two sides of [[imperialism]]: [[militarism]] (the ‘war on terror’) and speculative [[finance capital]].

The [[entropy]] of [[capitalism]].

> Radicals often speak of ‘[[the system]]’ to signify the socio-economic entity which currently oppresses us. The term implies that some- thing (not just economic exploitation but ideological alienation, militarism etc.) has built up a momentum of its own and become a self- propagating force, severed from rational control and consuming the society which produced it. The premise of this book is that this intui- tion is exactly on the right lines, but will only reveal its true potential if we really push the systems notion to a point where we can be rigorous about its implications.

<!--quoteend-->

> The task of a systems critique of capitalism could validly have been posed at any point of its history, but has special significance today, entering as we do a crisis of a new type where two systems – human and ecological – come into conflict and capitalism now consumes not just society itself, but its physical environment, to a point where neither can regenerate.

<!--quoteend-->

> More recent forms of capitalism perform this trick somewhat differently: neo-liberalism exploits the proposition (true in itself) that self-generated order is better than designed, as an excuse to outlaw social projects or any attempt to better the human condition. Our answer to this would have to focus on a particular aspect of [[information theory]] which emphasises ‘information about the future’: in human systems, structural emergent order inevitably has a strong dose of agency. The future is not predetermined; we can choose it, on the basis of existing possibilities.

Finding it very interesting the way he is discussing topics of self-organisation.  I've always been drawn to self-organising, agent-based systems, etc, as opposed to top-down models.  But I've thought before how there's a tension for me in that neoliberal ideas have a somewhat self-organisation kind of bent, decentralised information exchange in markets, etc.  But neoliberalism is clearly devastatingly problematic.  He talks a bit about this and how self-organisation could be used from a socialist perspective.

He doesn't use the word ecosocialism at any point, but it's very ecosocialist. Discusses extensively both the social and the environmental.

While starts from more of a Marxist outlook, often feels like presents similar conclusions as to those in [[Anarchist Cybernetics]].  Harnessing the capacity of self-organisation while acknowledging the need for some coordination and planning about the future.

> A major focus of this work will be to establish the dialogue between Marxism and systems theory.

<!--quoteend-->

> Having established a systems reading of Marxism (or, a Marxist reading of systems theory), the book’s main task is to address a question of burning contemporary relevance: if we view capitalism as an adaptive system, how, might it adapt to the symptoms of its own decay (entropy)?

and hopefully also: how might be stop it adapting and replace it.

> In viewing capitalism as a complex adaptive system, the proposition of this book is that we can employ systems theory in a way consistent with Marxism and dialectics, thus revealing links between aspects of the crisis which are hard to understand by conventional thinking.

-   [[Capitalism is a complex adaptive system]]


## Raw highlights

> The Green movement recognises such external limits, but what we must emphasise is the drive from within pushing against them. This is where Marxism is essential. Our strategic goal of eventually stabilising humanity’s relations with its environment must never be confused with stabilising the capitalist mode of production: were we side-tracked into attempting the latter (which is impossible anyway), we would disastrously amplify the causes of the problem. Only Marxism posits this distinction clearly.

<!--quoteend-->

> In Leninist theory, the imperialist era somehow ‘prepares the groundwork’ for a post-capitalist mode of production, and reading this in a modernist way it sounds as though imperialist centralism would be superceded in turn by a still more organised centrally-planned socialism. However, if we read it the opposite way, we could say that capitalism opens up the emergent properties of self-organising structures, but only in such a limited and truncated form that they are ultimately useless; it is up to socialism to realise their potential. Through a detailed analysis we show just how limited capitalist-inspired emergence really is, notably with respect to human capacity.

<!--quoteend-->

> But we should be aware that there is also a contradictory approach on the part of oppressive systems, namely to play the complexity card: the notion of the whole being than the sum of parts is twisted to assert an ‘organic’ societal interest to which the individual must be sacrificed. The chapter counters this, both by asserting rights as an attribute of the individual, and opposing the ‘state of exception’ notion, which is precisely not exceptional, but rather leads to a generalised repression.

<!--quoteend-->

> Despite the co-optation risks, radicals must enter the ‘sustainable communities’ arena, since experiments with a new mode of production cannot be delayed; its building blocks must be assembled partly from what already exists.

-   [[self-organising systems should be the terrain of the Left]]

> While including as ‘found objects’ even those low-input systems which have matured within capitalism, the foundation will be grassroots regime solutions, such as [[worker co-operatives]].

<!--quoteend-->

> Perhaps the key to the practical agenda for change will be to link experimental systems, and even more importantly to link struggles. The revolutionary movement in the largest sense is itself a complex system, whose emergent characteristics cannot be predicted from the individual parts, and will become clear only once the spaces of struggle begin to unfold a worldwide linkage and interaction.

<!--quoteend-->

> Capitalism has acquired experience of manoeuvring within this lattice-work of issues. Problems which are getting too acute in the social realm can be exported into the ecological sphere: economic ‘growth’ has, at immense ecological cost, enabled social contradictions to be kept at bay to some extent.

<!--quoteend-->

> Nature presents us with various ‘resources’ , i.e. materials which, because they are strongly differentiated from their background environment, are the opposite of an undifferentiated ‘noise’ . So we can represent them as ‘negative entropy’ (negentropy, or, the term we will often employ, exergy).

<!--quoteend-->

> Emergent forms of order are more efficient (better at being low-input/low-output) than top-down, over-designed ones.

<!--quoteend-->

> The agent of change must be a social movement sufficiently radical in its ability to escape the pull of the ruling order. This does not mean that it rejects every feature of current society, because one cannot begin with a clean slate. But it must not be enslaved to the ruling order as a system. This is why Marxism is so important to us.

<!--quoteend-->

> Post-capitalist society cannot simply consider the issue of limits to be resolved by a change in the relations of production.

-   [[Capacity]]

