# Digitalization and the Anthropocene

URL
: https://www.annualreviews.org/doi/10.1146/annurev-environ-120920-100056


## Handwritten notes

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-01.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-02.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-03.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-04.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-05.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-06.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-07.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-08.jpg]]

> Digitalization has the ability to influence what Meadows highlighted as some of the strongest leverage points over system dynamics: those that alter information flows and controls, rules of the system, the power structures and dynamics that uphold existing rules, and the mindsets that define them.
> 
> &#x2013; page 19


### [[Leverage points]].


#### Rules and feedback

> The first leverage point is the reformalization of rules and feedback, as discussed in the preceding section. Important options include mandatory data sharing in big tech and with urban/local administrations as condition for license; default data management as data commons or data trusts; a requirement for circular electronics designed for increased life span, repair, and reuse; and the use of tax incentives and price signals as feedback to steer digital use cases toward low environ- mental impact.
> 
> &#x2013; page 19


#### Structures

> The second leverage point is the modification of structures and the creation of new ones. Digitalization can play a crucial role by offering a holistic data-based Earth system understanding (116), which would translate into an informational governance that leverages the use of information to drive innovations in governance mechanisms and institutions
> 
> &#x2013; page 20


#### Goals

> The third leverage point is the resetting of goals, at both individual and societal levels, away from aspirational resource-intensive consumption (e.g., private cars, mansions) toward positive outcomes for the commons and society.
> 
> &#x2013; page 20


#### Mindsets

> The fourth and broadest leverage point is a mindset paradigm shift away from disjointed economic, legal, natural, or cultural systems toward a synthetic consideration, as captured by the notion of the Anthropocene
> 
> &#x2013; page 21

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-09.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-10.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-11.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-12.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-13.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-14.jpg]]

![[files/handwritten-notes/digitalization-and-the-anthropocene/page-15.jpg]]

