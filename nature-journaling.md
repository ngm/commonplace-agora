# nature journaling

I'm trying to nature journal a bit of late.  To deepen my appreciation of nature and become more aware of ecosystems, through paying attention to the nature around me.

> Nature journaling is the process of recording your observations about birds, plants, trees &amp; other natural things on paper.
> 
> It’s a simple &amp; effective tool to help naturalists improve their observation skills and see big picture ecological patterns more clearly.
> 
> &#x2013; [Beginner’s Guide to Nature Journaling: 12 Tips For a Better Nature Journal](https://nature-mentor.com/nature-journaling/)

