# Socialism's DIY Computer

URL
: https://tribunemag.co.uk/2020/07/make-your-own-self-managed-socialist-microcomputer

> Because all the day’s computers, including [[Galaksija]], ran their programs on cassette, Regasek thought Modli might broadcast programs over the airwaves as audio during his show. The idea was that listeners could tape the programs off their receivers as they were broadcast, then load them into their personal machines.

^ thread on this https://social.coop/@neil/105340661478429619

