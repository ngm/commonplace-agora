# What is true digital inclusion?

A
: [[podcast]]

URL
: https://therestartproject.org/podcast/digital-inclusion/

Series
: [[Restart Radio]]

[[digital inclusion]]

Getting computers to people who need them during lockdown.

Comparing top-down and grassroots approaches.

