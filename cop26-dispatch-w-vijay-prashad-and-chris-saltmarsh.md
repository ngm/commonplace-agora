# COP26 Dispatch w/ Vijay Prashad and Chris Saltmarsh

A
: [[podcast]]

URL
: https://guerrillahistory.libsyn.com/cop26-dispatch-w-vijay-prashad-and-chris-saltmarsh

Series
: [[Guerrilla History]]

Featuring
: [[Vijay Prashad]]

[[COP 26]].

Not finished it yet but [[Vijay Prashad]] is an engaging speaker.  Gives interesting history of Glasgow and slavery and how the main COP was held in the Docklands part.

