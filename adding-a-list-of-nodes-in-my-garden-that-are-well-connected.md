# Adding a list of nodes in my garden that are well-connected

[[Gordon Brander]] has a list of well-connected nodes in his [pattern library](https://gordonbrander.com/pattern/).

I like the idea.  Statistics relating to links and connections feels more in line with a [[relational ontology]] than just having counts of nodes, etc.

So I've added a page that lists the top 40 nodes in my garden by number of [[backlinks]].

It's on the [[Well-connected]] page.

The source below is executed inline on that page when it's published.  It builds a SQL query to run against the org-roam DB, then formats the results.

```emacs-lisp
(let* ((sql [:select [title dest (funcall count :distinct source)]
		 :from links
		 :inner :join nodes
		 :on (= links:dest nodes:id)
		 :where (= type "id")
		 :group :by dest
		 :order-by [(desc (funcall count :distinct source))]
		 :limit 40])
	 (links (org-roam-db-query sql)))
    (cl-loop for link in links
	     collect (pcase-let ((`(,title ,dest ,count) link))
		       (format "- %s ← %d.\n" (org-link-make-string (concat "id:" dest) title) count))))
```

The query counts how many nodes are linking to a particular node, rather than how many total backlinks.  So node B could have multiple quotes in it that link to node A, but that only counts as one connection between A and B in the counts here.

