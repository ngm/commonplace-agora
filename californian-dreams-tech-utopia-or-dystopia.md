# Californian Dreams: Tech Utopia or Dystopia

URL
: https://cosmonautmag.com/2021/07/californian-dreams-tech-utopia-or-dystopia/

Featuring
: [[Richard Barbrook]]

Rambling but fun interview with Richard Barbrook.  A little bit on the [[The Californian Ideology]].

Some fun snippets: if he was to write it today, it would be the [[Shenzhen Ideology]].

He says a few times how 80s/90s America (California Ideology) was the last time there was really an air of America as being the potential blueprint for the future.

Something like how America was seen as a perfect implementation of the present, Soviet Union an imperfect implementation of the future.

[[Digital Democracy Manifesto]]

