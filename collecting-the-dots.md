# collecting the dots

A nice phrase for the ideas in [[building a second brain]], [[zettelkasten]], [[philosophy of tiddlers]], etc. 

> Creative research is all about collecting the dots. It’s more common to think of “connecting the dots” but the truth is that **you can’t connect the dots you can’t see**. And we can only hold a tiny number of things in our brains at once. So a space for collecting (and organizing) the dots is a crucial foundation for thinking, creativity and more.
> 
> &#x2013; [Building a digital garden](https://tomcritchlow.com/2019/02/17/building-digital-garden/) 

A [[digital garden]] is a space for 'collecting the dots.'

> "But ideas aren’t summoned from nowhere: they come from raw material, other ideas or observations about the world. Hence a two-step creative process: collect raw material, then think about it. From this process comes [[pattern recognition]] and eventually the insights that form the basis of novel ideas."
> 
> This quote from the wonderful piece by Ink and Switch: capstone, a tablet for thinking.
> 
> &#x2013; [Building a digital garden](https://tomcritchlow.com/2019/02/17/building-digital-garden/) 

See: [[connecting the dots]].

