# Markets

-   [[Market economy]]

[[There are multiple types of markets]].

-   [[Markets in the Next System]]
    -   [[Markets before capitalism]].
    -   [[Capitalist markets]].
    -   [[Markets after capitalism]].

[[Markets don't give you what you need, they give you what you can afford]].

