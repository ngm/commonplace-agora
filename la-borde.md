# La Borde

> La Borde was an experimental institution run along communist lines: doctors would help with manual labour, and patients and staff worked together to maintain the hospital.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

