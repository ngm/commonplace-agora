# The Cybersyn Revolution

An
: [[article]]

URL
: https://jacobin.com/2015/04/allende-chile-beer-medina-cybersyn/

Author
: [[Eden Medina]]

Publication
: [[Jacobin]]

Date read
: 2022-11-13 (most recent)

Read time
: 00:22:29

Processing time
: ~30m

Useful in terms of how to approach general questions of technology in society.

-   the state and its priorities shape how technology is designed
    -   also markets?
-   the systems of the future must be free of the biases of today
-   we can do more with less, and help the environment in the process
-   privacy protection can mean the difference between an abusive system and a system that protects and promotes human freedom.
-   we need to think big, because [[technology alone will not create a better world]]

> The current market for electronic products depends on planned obsolescence: old products quickly become outdated and unfashionable. But extending the life of our electronic devices helps to address the e-waste problem. Project Cybersyn showed that it is possible to create a cutting-edge system using technologies that are not state-of-the-art. It demonstrates that the future can be tied to the technological past.

-   [[It is possible to create cutting-edge systems using technologies that are not state-of-the-art]].


## Raw highlights

> the state plays an important role in technical design, and can help shape innovations that aim to benefit society and support marginalized groups rather than achieve narrow efficiency goals or single-mindedly increase profits

<!--quoteend-->

> we need to be vigilant about the ways in which design bias can limit the efficacy of technologies for increased democratic participation and inclusion.

<!--quoteend-->

> we need to think creatively about changing social and organizational systems if we want to get the most out of technology; technological innovation alone will not make the world a better place.


### The state and its priorities shape how technology is designed and used.

> he observed how technologies like computer-controlled machinery contribute to the automation of labor and lead to the deskilling of workers, even in highly specialized fields such as engineering

<!--quoteend-->

> Computers make office work increasingly routinized and give management an easy way to monitor the amount of labor each operator has put in. The increased speed of work has the potential to result in more layoffs.

<!--quoteend-->

> Beer saw computerization differently, not least because the Chilean state insisted that its socialist computer system be designed for different ends than the ones that Braverman described. This gave Beer the freedom to reconceptualize how technologies might shape work on the shop floor and to see computers as a means of empowering workers.

<!--quoteend-->

> Computer innovation wasn’t born with Silicon Valley startups, and it can thrive by taking on design considerations that fall outside the scope of the market.


### The systems of the future must be free of the biases of today.

> Left unchecked, technologies for increased democratic participation and improved human-machine interaction can also exclude and marginalize sectors of the population.


### We can do more with less, and help the environment in the process.

> The current market for electronic products depends on planned obsolescence: old products quickly become outdated and unfashionable. But extending the life of our electronic devices helps to address the e-waste problem. Project Cybersyn showed that it is possible to create a cutting-edge system using technologies that are not state-of-the-art. It demonstrates that the future can be tied to the technological past.

<!--quoteend-->

> But the Chilean network used fewer technical resources at a lower cost and proved highly functional nonetheless. Older technologies were creatively re-envisioned and combined with other forms of organizational and social innovation.

<!--quoteend-->

> Project Cybersyn also demonstrates that more can be done with less. The Chilean project did not try to copy the Soviets’ form of economic cybernetics, which collected a wealth of factory data and sent it to a centralized hierarchy of computer centers for further processing. It accomplished the same task by transmitting only ten to twelve indexes of production daily from each factory and having factory modelers spend more time thoughtfully identifying which indexes were most important.


### Privacy protection can mean the difference between an abusive system and a system that protects and promotes human freedom.


### We need to think big, because technology alone will not create a better world.

> We need to be thinking in terms of systems rather than technological quick fixes.

<!--quoteend-->

> must resist the kind of apolitical “innovation determinism” that sees the creation of the next app, online service, or networked device as the best way to move society forward

<!--quoteend-->

> Instead, we should push ourselves to think creatively of ways to change the structure of our organizations, political processes, and societies for the better and about how new technologies might contribute to such efforts.

