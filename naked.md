# Naked

> Mike Leigh’s apocalyptic 'Naked' was a terrifying picture of [[early 1990s Britain]], alone in the director’s oeuvre in its brutal pessimism. 
> 
> &#x2013; [Ways of Ending](https://tribunemag.co.uk/2022/03/ways-of-ending) 

