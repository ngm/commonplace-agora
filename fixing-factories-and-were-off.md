# Fixing Factories: And we're off

URL
: https://therestartproject.org/news/brent-fixing-factory/

Launch of first [[Fixing Factory]] in Brent.   Saturday 23rd April 2022.

> Visitors to the site will be able to donate devices, see how they’re repaired and find out about the project. Those keen to learn about [[repair]] will have a chance to volunteer on the site and get work experience. 

<!--quoteend-->

> We’ll also host regular weekend events with local partners, where visitors will be able to get involved in a more hands-on way. Ideas so far include workshops on [[laptop repair]], and an opportunity to bring your own laptop for fixing. 

