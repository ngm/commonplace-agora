# Boox Firmware v3.5

URL
: https://shop.boox.com/pages/firmware

[[Boox]] firmware update. Came out around November 1st 2023.

Happy with it. The Smart Scribe stuff is useful, and it seems to have fixed the issue with handwriting recognition on full pages ([[Boox handwriting recognition missing whole words out]]).

