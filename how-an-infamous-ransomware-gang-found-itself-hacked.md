# How an infamous ransomware gang found itself hacked

A
: [[podcast]]

Found at
: https://www.theguardian.com/news/audio/2024/mar/13/how-an-infamous-ransomware-gang-found-itself-hacked-lockbit-podcast

Part of
: [[Today in Focus]]

> LockBit was a sophisticated criminal operation, offering the tools needed to steal a company’s data and hold it to ransom. Then it was itself hacked. Alex Hern reports

Engaging bit of journalism about recent [[LockBit]] takedown, and also reflections on [[ransomware]] in general.

I did not know that [[WannaCry]] was by the North Korean state.

Interesting assertion that [[cryptocurrency]] was one of the recent enabling factors in the rise of ransomware.

