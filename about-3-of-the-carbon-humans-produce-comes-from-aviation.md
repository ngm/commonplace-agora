# About 3% of the carbon humans produce comes from aviation

About 3% of the [[carbon dioxide emissions]] humans produce comes from [[aviation]].

So sayeth [Do we need to stop flying to save the planet? We ask an expert](https://www.theguardian.com/lifeandstyle/2022/apr/15/do-we-need-to-stop-flying-to-save-the-planet-we-ask-an-expert).

