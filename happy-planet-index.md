# Happy Planet Index

> the Happy Planet Index, created by the New Economic Forum, which combines well-being as reported by citizens, life expectancy, and inequality of outcomes, divided by ecological footprint (by this rubric the US scores 20.1 out of 100, and comes in 108th out of 140 countries rated);
> 
> &#x2013; [[The Ministry for the Future]]

Sounds similar to [[Sustainable Development Index]].

Actually [[New Economics Foundation]] I think.

