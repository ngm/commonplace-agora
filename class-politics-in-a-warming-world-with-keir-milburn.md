# Class Politics in a Warming World with Keir Milburn

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/class-politics-in-a-warming-world-with-keir-milburn

Featuring
: [[Keir Milburn]]

I like Keir Milburn.  From the [[#ACFM]] podcast.  Interesting history.  Helped set up [[The Common Place]] in Leeds, where I went a couple of times when I was studying in Leeds.  Also talks about how they were infiltrated by [[spycops]].

This interview is ostensibly about the [[intersection of class politics and environmental politics]].  i.e. relates to ecosocialist politics.

Mentions [[Reclaim the Streets]].  Talks about the history of environmental movements discovering class politics.

I will explore more the [[Abundance]] think tank.

Touches on [[ecomodernism]] vs [[degrowth]] a little bit.

