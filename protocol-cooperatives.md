# Protocol cooperatives

[[Protocol cooperativism]]

> Protocol cooperatives are global open source repositories of knowledge, code and design, that allow humanity to create infrastructures for the mutualization of the main provisioning systems (such as food, habitat, mobility), and that are governed by the various stakeholders involved, including the affected citizenry.
> 
> &#x2013; [[P2P Accounting for Planetary Survival]]

