# Gilets jaunes

> The gilets jaunes shifted the model for how to proceed, away from May 68 or any fainter impressions of the Commune or 1848, not to mention 1793, which it has to be admitted is now like a vision from ancient history, despite the evident satisfactions of the guillotine for dealing with all the climate criminals sneaking off to their island fortress mansions. No, modern times: we had to get out into the streets day after day, week after week, and talk to ordinary people in their cars stuck in traffic, or walking past us on the sidewalks and metro platforms. We had to do that work like any other kind of work. It wasn’t a party, it wasn’t even a revolution. At least when we started.
> 
> &#x2013; [[The Ministry for the Future]]

