# Coronavirus and 3D printing

-   source: [Coronavirus and 3D printing - 3D Printing Media Network](https://www.3dprintingmedia.network/covid-19-3d-printed-valve-for-reanimation-device/)

> Italian hospital saves [[Covid-19]] patients lives by **[[3D printing]] valves for reanimation devices**. The **supply chain was broken, people and 3D printing rose to the occasion**.

<!--quoteend-->

> we are very far from understanding **what the long, medium and even short terms implications of the pandemic are going to be on global [[supply chains]]**.

<!--quoteend-->

> One of the biggest immediate problems that coronavirus is causing is the massive number of **people who require intensive care and oxygenation in order to live through the infection long enough for their antibodies to fight it**. 

<!--quoteend-->

> This means that the only way to save lives at this point – beyond prevention – is to have as many working reanimation machines as possible. And when they break down, maybe 3D printing can help.

<!--quoteend-->

> She explained that the hospital in Brescia (near one of the hardest-hit regions for coronavirus infections) urgently needed valves (in the photo) for an intensive care device and that **the supplier could not provide them in a short time**. 
> **Running out of the valves would have been dramatic and some people might have lost their lives.**

<!--quoteend-->

> As far as 3dpbm understands, **the model for the valve remains covered by copyright and patents**. Hospitals may have a right to produce these parts in an emergency (as in this case) but, **in order to legally obtain a 3D printable STL file, the hospital that requires the parts needs to present an official request**. We will continue to update this article as new information becomes available.

-   tags: [[reading]]

