# Permaculture

> Permaculture, originally 'Permanent Agriculture', is often viewed as a set of gardening techniques, but it has in fact developed into a whole design philosophy, and for some people a philosophy for life. 
> 
> &#x2013;  [What is permaculture? | Permaculture magazine](https://www.permaculture.co.uk/what-is-permaculture) 

^ That's interesting, I'll admit I did kind of think it was mostly about growing and agriculture.

> By thinking carefully about the way we use our resources - food, energy, shelter and other material and non-material needs - it is possible to get much more out of life by using less. We can be more productive for less effort, reaping benefits for our environment and ourselves, for now and for generations to come.
> 
> &#x2013;  [What is permaculture? | Permaculture magazine](https://www.permaculture.co.uk/what-is-permaculture) 


## Principles

They seem alright.  Kind of a [[Systems thinking]] feel.

> -   Observe and interact: Take time to engage with nature to design solutions that suit a particular situation.
> -   Catch and store energy: Develop systems that collect resources at peak abundance for use in times of need.
> -   Obtain a yield: Emphasize projects that generate meaningful rewards.
> -   Apply self-regulation and accept feedback: Discourage inappropriate activity to ensure that systems function well.
> -   Use and value renewable resources and services: Make the best use of nature's abundance: reduce consumption and dependence on non-renewable resources.
> -   Produce no waste: Value and employ all available resources: waste nothing.
> -   Design from patterns to details: Observe patterns in nature and society and use them to inform designs, later adding details.
> -   Integrate rather than segregate: Proper designs allow relationships to develop between design elements, allowing them to work together to support each other.
> -   Use small and slow solutions: Small and slow systems are easier to maintain, make better use of local resources and produce more sustainable outcomes.
> -   Use and value diversity: Diversity reduces system-level vulnerability to threats and fully exploits its environment.
> -   Use edges and value the marginal: The border between things is where the most interesting events take place. These are often the system's most valuable, diverse and productive elements.
> -   Creatively use and respond to change: A positive impact on inevitable change comes from careful observation, followed by well-timed intervention.
> 
> &#x2013; [Permaculture - Wikipedia](https://en.wikipedia.org/wiki/Permaculture#Design_principles) 


## Local growing

-   https://www.capitalgrowth.org/


## Criticism

> Its colonized itek but if people will actually do it, then its in improvement.
> 
> &#x2013; [SQU∄▲KY P▲Nᐊ▲K∄S: "@neil@social.coop Its colonized itek but if peopl…" - Sunb&#x2026;](https://sunbeam.city/@squeakypancakes/107067687343910639) 

