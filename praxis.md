# Praxis

> action oriented towards changing society
> 
> &#x2013; [Praxis (process) - Wikipedia](https://en.wikipedia.org/wiki/Praxis_(process)) 

-   [[Horizontalism and verticalism]]
-   [[Community organising]]
-   [[Solidarity]]
-   [[Prefiguration]]

[[Educate, agitate, organise]].


## Hannah Arendt

> In [[The Human Condition]], [[Hannah Arendt]] argues that Western philosophy too often has focused on the contemplative life (vita contemplativa) and has neglected the active life ([[Vita activa]]). This has led humanity to frequently miss much of the everyday relevance of philosophical ideas to real life.
> 
> &#x2013; [Praxis (process) - Wikipedia](https://en.wikipedia.org/wiki/Praxis_(process)#Hannah_Arendt) 

<!--quoteend-->

> For Arendt, praxis is the highest and most important level of the active life. Thus, she argues that more philosophers need to engage in everyday political action or praxis, which she sees as the true realization of human freedom. According to Arendt, our capacity to analyze ideas, wrestle with them, and engage in active praxis is what makes us uniquely human.
> 
> &#x2013; [Praxis (process) - Wikipedia](https://en.wikipedia.org/wiki/Praxis_(process)#Hannah_Arendt) 

<!--quoteend-->

> by viewing action as a mode of human togetherness, Arendt is able to develop a conception of participatory democracy which stands in direct contrast to the bureaucratized and elitist forms of politics so characteristic of the modern epoch.
> 
> &#x2013; [Hannah Arendt (Stanford Encyclopedia of Philosophy)](https://plato.stanford.edu/entries/arendt/#AreTheAct) 

