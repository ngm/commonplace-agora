# Setting up YunoHost

Notes on setting up [[YunoHost]].


## Installation

Run install script on your new server.


### Post-installation


#### SSH

Removes SSH root access during installation which is good.  However doesn't give much more info about logging in as admin.  That should be key-based only as well, no?  Out-of-the-box it remains password-based.

I changed it be key-based by copying the key over with `ssh-copy-id`.

If you remove root login via SSH, you should set a root password so that you can log in as root via the virtual KVM.  ([[Hetzner]] is handy in that it lets you do this at any time without needing a reboot.)


#### Security

I think they could do more to encourage good security practices.  "You are now about to define a new admin password. The password should be at least 8 characters - though it is good practice to use longer password (i.e. a passphrase) and/or to use various kind of characters (uppercase, lowercase, digits and special characters)."


#### DNS

Had to look up how to add a SRV record correctly - luckily iwantmyname has it in knowledge base https://help.iwantmyname.com/hc/en-gb/articles/360014961418-How-do-I-add-an-SRV-record

Still not quite sure if I've done them all right.  Not too fussed about xmpp so it doesn't matter.


#### TLS

Go to Domains / &lt;domain&gt; / Manage SSL Certificate to install via Let's Encrypt.


#### Reverse DNS

Need to do reverse dns and don't know how. https://yunohost.org/#/dns_config


#### Misc

lastname must be a valid lastname???  I got this validation error when creating a user and using just my initial.  Surely you should be able to go anon if you want to.


#### LDAP

Seem to be having trouble with LDAP and baikal.


### Apps


#### syncthing

syncthing seems to work pretty easily (although ynh doesn't give you the big warning about changing the admin password like cloudron does.)


#### anarchism docs

easy one :)


#### 

