# Chicago boys

> While the ‘Chicago boys’ have become infamous in the history of neoliberalism, it is less well known that the CIA’s goal of creating a ‘coup climate’ was largely the work of assistant secretary of defence Warren Nutter, who was Chicago School leader [[Milton Friedman]]’s first graduate student
> 
> [[Half-Earth Socialism]]

