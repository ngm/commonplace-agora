# Solar panels and gardening

[[Solar panels]] and [[gardening]].

> this is incredible. Hanging solar panels a few meters above crops of tomatoes and jalapeños multiplied their yield 2-3x, used substantially less water, controlled temperatures, and increased the output of the solar panels https://www.pnas.org/doi/10.1073/pnas.2301355120 #science #solar #environment #agriculture
> 
> &#x2013; https://hachyderm.io/@kellogh/109962304414884243

