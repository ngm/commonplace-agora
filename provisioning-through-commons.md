# Provisioning Through Commons

Part of the [[Triad of Commoning]].


## Patterns

-   Make &amp; Use Together
-   Support Care &amp; Decommodified Work
-   Share the Risks of Provisioning
-   Contribute &amp; Share
-   Pool, Cap &amp; Divide Up
-   Pool, Cap &amp; Mutualize
-   Trade with Price Sovereignty
-   [[Use Convivial Tools]]
-   Rely on Distributed Structures
-   Creatively Adapt &amp; Renew

