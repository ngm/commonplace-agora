# block-level transclusion

The ability to pull in segments of other pages into the current page.  If you change it in one place, other references will change.


## Why?

I'm still a little hazy on usage, but seems to be useful for building up arguments/articles built from various thoughts, without having having to have a full page for each of those separate thoughts.

> I am dabbling in the Zettelkasten method, with using Org-roam. I keep my notes in a repository where my network of notes reside (I guess people call them evergreen, concept, or permanent notes).
> 
> When I start writing something long-form, I want to have a writing project separately from my notes repository, assemble relevant notes to form a basis of the long-form material, and avoid having multiple copies of notes flying around.
> 
> Transclusion should let me do this.
> 
> &#x2013; [nobiot/org-transclusion](https://github.com/nobiot/org-transclusion) 


## Reference vs embed

> A block reference is one way, in the sense that it is a copy that links back to the original instance and is dependent on it. What I mean is, if you edit the original, the reference will change to, but not vice versa.
> 
> On the other hand, all block embeds of an original instance are related and interactive. Edit any one of them, and ALL instances change.
> 
> &#x2013; [What is block level transclusion and why is it important? : RoamResearch](https://www.reddit.com/r/RoamResearch/comments/gvbg1q/what_is_block_level_transclusion_and_why_is_it/) 


## Resources

-   [Block reference - Feature archive - Obsidian Forum](https://forum.obsidian.md/t/block-reference/674)
-   [What is block level transclusion and why is it important? : RoamResearch](https://www.reddit.com/r/RoamResearch/comments/gvbg1q/what_is_block_level_transclusion_and_why_is_it/)

