# systems literacy

> systems literacy can help us manage messes.
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> A good working vocabulary in systems includes around 150 terms.
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> It begins with learning:
> 
> -   [[system]], environment, boundary
> -   process, transform function
> -   stocks, flows, delay (lag)
> -   source, sink
> -   information (signal, message)
> -   open-loop, closed-loop
> -   goal (threshold, set-point)
> -   feedback, feed-forward
> -   [[positive feedback]], negative feedback
> -   reinforcing, dampening
> -   viscous cycle, virtuous cycle
> -   circular processes, circularity, resource cycle
> -   explosion, collapse, oscillation (hunting)
> -   stability, invariant organization
> -   balancing, dynamic equilibrium, homeostasis
> -   [[tragedy of the commons]]
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> As students progress, they learn:
> 
> -   behavior (action, task), measurement
> -   range, resolution, frequency
> -   sensor, comparator, actuator (effector)
> -   servo-mechanism, governor
> -   current state, desired state
> -   error, detection, correction
> -   disturbances, responses
> -   controlled variable, command signal
> -   control, communication
> -   teleology, purpose
> -   goal-directed, self-regulating
> -   co-ordination, regulation
> -   static, dynamic
> -   first order, second order
> -   essential variables
> -   variety, “[[requisite variety]]”
> -   transformation (table)
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> More advanced students learn:
> 
> -   dissipative system
> -   [[emergence]]
> -   [[autopoiesis]]
> -   constructivism
> -   recursion
> -   observer, observed
> -   controller, controlled
> -   agreement, (mis-)understanding
> -   “an agreement over an understanding”
> -   learning, conversation
> -   bio-cost, bio-gain
> -   back-talk
> -   structure, organization
> -   co-evolution, drift
> -   black box
> -   explanatory principle
> -   “organizational closure”
> -   self-reference, reflexive
> -   ethical imperative
> -   structural coupling
> -   “consensual co-ordination of consensual co-ordination”
> -   “conservation of a manner of living”
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> One course, 3 hours per week for 15 weeks is a bare minimum for a survey of systems thinking. Ideal would be three, semester-long courses
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> 1.  Introduction to Systems (covering systems dynamics, regulation, and requisite variety—with readings including Capra’s new [[The Systems View of Life]], Meadows’ [[Thinking in Systems]], and Ashby’s [[An Introduction to Cybernetics]]);
> 
> 2.  Second-Order Systems (covering observing systems, autopoiesis, learning, and ethics—with readings including Glanville’s “[[Second-order Cybernetics]],” von Foerster’s “[[Ethics and Second-order Cybernetics]],” and Maturana + Davila’s “[[Systemic and Meta Systemic Laws]]”); and
> 
> 3.  Systems for Conversation (covering co-evolution, co-ordination, and collaboration—with readings including, Pangaro’s “[[What is conversation?]],” Pask’s “[[The Limits of Togetherness]],” Beer’s [[Decision and Control]], and Maturana’s “[[Metadesign]]”).
> 
> &#x2013; [[A Systems Literacy Manifesto]]

<!--quoteend-->

> person with basic systems literacy should be fluent with these patterns: resource flows and cycles; transform functions (processes); feedback loops (both positive and negative); feed-forward; requisite variety (meeting disturbances within a specified range); second-order feedback (learning systems); and goal-action trees (or webs).
> 
> -   [[A Systems Literacy Manifesto]]


## Resources

-   [[A Systems Literacy Manifesto]]

