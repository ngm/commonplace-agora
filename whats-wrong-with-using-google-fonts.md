# What's wrong with using Google Fonts?

It's a [[GDPR]] violation apparently, when served from Google's servers.


## Resources

-   [Usage of Google Fonts violates GDPR - Termageddon](https://termageddon.com/google-fonts-violates-gdpr/)
-   [Google fonts on your website? Do not share IP addresses with Google because o&#x2026;](https://www.lexology.com/library/detail.aspx?g=8546d90b-61f5-4c96-8bd0-86d1676863c2)

