# The importance of social media

An internal reflection: why do I seem to care about [[social media]] so much?

[[Social media is important because everything is social]].

> Social media, in so far as it creates an environment in which decentralised and dispersed communication networks can function, is one of the key architectures that allow for [[many-to-many communication]]; and to the extent that these networked organisation forms mirror the kind of organisation anarchism is concerned with, social media can be explored as providing potential sites for anarchist politics.
> 
> &#x2013; [[Anarchist Cybernetics]]

[[Arab spring]].  [[Twitter and Tear Gas]].

> Social media platforms, in so far as they facilitate interactive communication and organisation, have the potential to play a central role in anarchist cybernetics and thus anarchist (self) organisation more broadly.
> 
> &#x2013; [[Anarchist Cybernetics]]

