# semi-permeable membrane

In relation to [[commoning]], an idea from [[Free, Fair and Alive]].

> The first of Ostrom’s famous [[eight design principles for successful commons]] is “clearly defined boundaries,” which are needed to delineate the boundaries of the resource system and the membership of the commons. While agreeing with this general idea that boundaries are essential to stewardship, we believe that a better term is “semi-permeable membrane.”
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> A living organism relies on semi-permeable membranes to allow it to access what is useful in the external environment and filter out what is harmful, all in ways congruent with its particular context.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Semi-Permeable Membranes are what the boundaries of a Commons should be. Like other living social organisms, Commons need to protect themselves from external forces that might harm them while remaining open to flows of nourishment and signals from the environment. Therefore, a commons functions best if it develops a semi-permeable membrane for itself rather than a tight, rigid boundary. This flexible skin, figuratively speaking, both assures its integrity by preventing Enclosure and other harms while allowing it to develop nourishing, symbiotic relationships with other living organisms.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> But a body of empirical work undertaken since Ostrom’s original publication suggests that in practice, rights of access and physical boundaries in a commoning community are almost always subject to negotiation, and in general remain far more fluid than the implacable, binary worldview encoded by a blockchain.
> 
> &#x2013; [[Radical Technologies]]

^ sounds like semi-permeable membrane

