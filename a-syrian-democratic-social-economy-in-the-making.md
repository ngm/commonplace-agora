# A Syrian democratic social economy in the making

URL
: https://www.commonsnetwork.org/2020/11/18/a-syrian-democratic-social-economy-in-the-making/

> In the war torn region of [[Rojava]], Syria, a communal economy has been steadily growing as people form cooperatives and local administrations actively support an economic transition. 

