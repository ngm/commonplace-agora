# peer-to-peer networks

> In peer-to-peer networks, we ‘seed’ for one another. We spread our bytes like weeds, criss-crossing fenced-in property and open meadows alike, in our effort to grow digital abundance.
> 
> &#x2013; [[Seeding the Wild]]

<!--quoteend-->

> the peer-to-peer model turns addicted ‘users’ into active peers, giving people more control over the systems they use.
> 
> &#x2013; [[Seeding the Wild]]

<!--quoteend-->

> Peer-to-peer applications cannot be monetised in the same way traditional client-server applications can. 
> 
> &#x2013; [[Seeding the Wild]]

^ why not?

