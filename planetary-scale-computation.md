# planetary-scale computation

> Planetaryscale computation” describes the shifting nature of cloud computation by the use of distributed systems. Production is no longer fixed to one location or to single pieces of machinery. Instead, it is realised through a flow of computational procedures within a distributed global infrastructure. Thus, planetary-scale computation represents a historically novel ‘skin’ enveloping the planet, aimed at recording, measuring, and facilitating human and non-human processes in the form of data. Data become the central means of mediation and coordination in economies worldwide, leading to a transformation of their institutional forms and their agents’ patterns of behaviour and practices.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

<!--quoteend-->

> From a political-economic perspective, planetary-scale computation represents a complex economic phenomenon. It involves a modular global production structure concerning interdependent feedback between software (knowledge as means of production) and hardware (material accumulation).
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

