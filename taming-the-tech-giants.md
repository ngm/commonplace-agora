# Taming the tech giants

-   large structural transformations required
-   hidden problem is: accumulated power and lopsided economy
-   they have too much power
-   they are solely profit-driven
-   difficult to tax them as they shift immaterial assets

"If, as David Harvey says, we can think of neoliberalism as a class project,
then Silicon Valley, is, in a way, the industrial manifestation of neoliberalism
applied to technology. It's the digital embodiment of neoliberal governance and
ideology. Which means that Silicon Valley, too, is fundamentally a class
project."

"Tackling the problem at the root requires that we **abolish** Silicon Valley."

-   Ad-based companies are compliciti - they get a cut in the commodities

"If Silicon Valley is a class project, then tackling it - abolishing it - means
changing the balance of class forces, tipping it away from capital and in favour
of labour."

-   change conception of what a 'tech worker' is
-   worldwide labour movements, worker organisation from below
-   start taking lucrative technologies out of the capital-accumulation process


## summary

If neoliberalism is a class project, then Silicon Valley is the industrial
manifestation of neoliberalism applied to technology. Silicon Valley is a class
project. To abolish it needs large structural transformation. We need to change
the balance of class forces, tipping it away from capital and in favour of
labour. Worker organisation from below, with a change in conception of what a
tech worker is. Start taking lucrative technologies out of the
capital-accumulation process.

"Tackling the problem at the root requires that we **abolish** Silicon Valley."

https://newsocialist.org.uk/beyond-taming-the-tech-giants/

