# Ubuntu 20.04 basic server setup

Rough notes on the steps I usually take when setting up a new VPS.  Things are a little bit different for Ubuntu 20.04.  Like MySQL 8 rather than 5.7.  And having to manually install bash-completion!?


## Server essentials

```bash
apt install bash-completion
apt install ufw
ufw allow ssh
ufw allow https
ufw enable
apt install fail2ban

apt install rsync

# set a password so can log in to virtual KVM switch console at host if ever need to
passwd
```


## Web server stuff

```bash
# for websites 
apt install apache2
apt install certbot
apt install python3-certbot-apache

# for wordpress
apt install mysql-server
mysql_secure_installation # note: [[id:beeb75e4-6038-4816-9870-ad2f98982e93][Setting up MySQL]] 
apt install php php-zip php-mysql php-gd php-xml php-xmlrpc
```


## Site configuration

Then configure the sites themselves in Apache.

