# gift economies build community

First came across this claim in the Wikipedia article, which links out to [[The Stubborn Vitality of the Gift Economy]].

> The emphasis in a gift exchange is on strengthening the bond between the givers and receivers.
> 
> It creates a kind of "positive debt," where two people or groups are now tied to one another - one party will always owe the other something. It's a purposeful way of entwining lives and communities together.
> 
> &#x2013; [The Gift Economy](https://maggieappleton.com/gift-economy)


## Because

-   The emphasis in a gift exchange is on strengthening the bond between the givers and receivers
-   A gift economy creates a kind of "positive debt," where two people or groups are now tied to one another


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Outlook good]]

Sounds plausible, with plenty of research behind it.  I couldn't argue it strongly though.

