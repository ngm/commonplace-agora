# Guy Debord

-   [[The Society of the Spectacle]]

Founding member of the [[Situationist International]].

> Debord was a founding member of the Situationist International (1957–1972), a group of avant-garde artists and political theorists united by their opposition to advanced capitalism. At varying points the group’s members included the writers Raoul Vaneigem and Michèle Bernstein, the artist Asger Jorn, and the art historian T.J. Clark. Inspired primarily by Dadaism, Surrealism, and Marxist philosophy, the SI rose to public prominence during the May 1968 demonstrations during which members of the group participated in student-led occupations and protests. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> It depressed him in his later years that [his] insight had long since ceased to be a revolutionary call to arms but the most accurate, if banal, description of modern life […] While Debord’s public life was predicated upon his revolutionary intentions, in private he sought oblivion in infamy, exile and alcoholism.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

