# Spotify

Fuck Spotify, join [[Resonate]].

> Spotify – boasts 124 million subscribers, taking in USD$7.44 billion in revenue but paying artists just USD$4.37 per 1,000 streams. 
> 
> &#x2013; [[This is Fine: Optimism and Emergency in the Decentralised Network]] 

<!--quoteend-->

> Over the last 7 years, [[Spotify]] has earned me 4.3% of what [[Bandcamp]] has. Over the last 3 months it's 1.6%.
> Getting playlisted by the Spotify lords and heard by 50,000 people is worth as much as ~20 people deciding to pay a fiver for a release on Bandcamp.
> 
> [@datassette](https://twitter.com/datassette/status/1280417867679313920) [[Datassette]]

[[Spotify is going to ruin podcasts]]

