# This Land is Our Land

A
: [[documentary]] [[film]]

[[David Bollier]] on the [[commons]]. What is it and why does it matter?

-   Polio.  Vaccine not patented.  Jonas Salk.
-   Contrast with HIV/AIDS. 20,000 a month dying in South Africa.  Because they couldn't afford the patented drugs.  South Africa made a generic version.  Drug industry called it piracy.  They won.  Patents caused many to die.
-   We need to bring back the commons.
-   We have an inate ability to cooperative.  Res publica and res communes.  [[Magna Carta]].  and the [[Charter of the Forest]].
-   The commons were in many ways a social safety net.
-   The [[Constitution of the United States]] had bits based on the Magna Carta.
-   Benjamin Franklin didn't patent things.  And he liked to reuse that which had gone before&#x2026;
-   [[Thatcher]], Reagan, brought out [[neoliberal capitalism]] in to the mainstream.
-   The 80s and 90s were awash with [[privatisation]].
    -   "Resulting in a shift of more than trillion dollars of state-owned enterprises to private interests."  (dunno if that's worldwide or US)
-   [[enclosure of the commons]].
    -   Studied by [[Karl Polanyi]].  "A revolution of the rich against the poor"
-   Branded bottled water.
    -   The market feeds off the commons.
-   Enclosure of broadcast airwaves. (valued today at roughly $0.5 trillion)
-   It used to be: if you take from the commons, you should give back to the commons.
-   Reagon and Clinton deregulated the airwaves.
-   Enclosure of the internet.
-   Girl scouts sued for singing puff the magic dragon.
-   Woody Guthrie didn't believe in copyright.  This machine kills fascists.
-   Hardin was never describing a commons.  He was describing a free-for-all.  Commons have rules.
-   World Social Forum.
-   Reclaming the commons.

