# 2,000-Watt Society

> The Swiss Federal Institute of Technology calls for the 2000-watt society. An energy budget, rather than a growth target, for everyone on the planet. 
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> This is endorsed by the earth scientist [[Vaclav Smil]] as the only plausible scenario for survival. And Smil is extremely cautious in his endorsements
> 
> &#x2013; [[For a Red Zoopolis]]

Don't know anything about Vaclav Smil, so don't know if that's a good thing or not.  Richard Seymour seems to think so?

> The 2,000 Watt Society, started in 1998 in Switzerland, calculated that if all the energy consumed by households were divided by the total number of humans alive, each would have the use of about 2,000 watts of power, meaning about 48 kilowatt-hours per day. The society’s members then tried living on that amount of electricity to see what it was like: they found it was fine. It took paying attention to energy use, but the resulting life was by no means a form of suffering; it was even reported to feel more stylish and meaningful to those who undertook the experiment.
> 
> &#x2013; [[The Ministry for the Future]]

