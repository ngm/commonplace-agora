# Collaborative memory palaces

I think I would like some kind of virtual experience where I have my own [[Memory Palace]], the method of loci, the visual virtual space in your head where you walk through as you recall things. But not just for recall, for construction too. I would invite other people in and they help me build it and I help on theirs too, we build them together.

Some kind of mashup at the intersection of hypermedia systems like [[HyperCard]], MOOs like LambdaMOO, [[commonplace books]], blogs, microblogs, [[personal wikis]].

A Memory MUD.  Or Memory Moo.

I wonder if this is some terrain where [[Spritely]] is going? That would be rad.

I like [Emsenn's website](https://emsenn.net), it has a bit of a MUD-feel.  They were also writing a personal MUD engine at one point where the aim was to recreate the personal wiki approach.

There are probably some linkages to [[OERs]] and [[MOOCs]], given the collaborative learning aspect of it that I want.

The [[Metaverse]].

Lo and behold!  There is a research paper on collaborative memory palaces, from nearly 20 years ago:  [iScape: A collaborative memory palace for digital library search results](https://pdfs.semanticscholar.org/6270/b3bc1fb6e5fc6b3acc1f8e63be76b3a94ba5.pdf).

![[2020-03-19_23-27-44_iScape.png]]

[[Wikity]].

[[Networked learning]].

[[Spatial software]].


## See also

-   [Spatial Graph Embeddings - Graphing Capabilities - Org-roam](https://org-roam.discourse.group/t/spatial-graph-embeddings/880)
-   https://twitter.com/hardy_lisa_a/status/1282014988442333185

