# Intellectual property markets must be socialized

> [[De-marketing intellectual property]] is especially exhilarating, because we can skip the socialist step of moving ideas from private to public ownership, and go straight to the communist state of non-ownership.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> Ideas such as pharmacological discoveries, software developments, and works of media are immaterial, their digital representations as sound, video, image, text, etc. files infinitely reproducible at negligible cost. In order, therefore, to effect their decommodification, the government only needs to take a laissez faire approach to enforcing their patents, copyrights, etc. Beyond possibly brief, non-transferable licenses for authors and inventors (as the founders intended), there is no need for the government to impose [[artificial scarcity]], without which, an infinitely abundant product will approach its “correct consumer price”: zero dollars.
> 
> &#x2013; [[Markets in the Next System]]

