# Stack

> The geek-colloquial meaning of stack, in the most relevant sense, is a set of interoperating hardware and software.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> Benjamin H. Bratton (2016, xvii) goes further, describing the stack as “a new architecture for how we divide the world into sovereign spaces”.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> A stack might include all that enables one to use a social media service, for instance: the server farms, the corporation that owns them, its investors, the software the servers run on, the secret algorithms that analyse one’s data, the mobile device, its accelerometer sending biometric data to the server farm, the network provider, the backdoor access for law enforcement, and so on.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> The layers of a stack might further include the sun or coal powering it, the wars fuelled by rare-earth mining, and the mythologies and rituals that dictate what people in it will tolerate.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> The urgency of forming [[governable stacks]] stems not only from tech giants and surveillance states. Groups dedicated to racism and authoritarianism have become particularly intentional about their network stacks, migrating to dedicated platforms such as Stormfront, Parler, and Gab as more mainstream networks remove them (Bev- ensee 2020). Will governable stacks become the speciality of reactionaries? The stack has already become a field of conflict, like the spinning wheel.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

