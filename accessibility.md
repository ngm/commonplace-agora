# Accessibility

> Accessibility is the practice of ensuring websites are equally available to people with disabilities so they have equal access to the goods and services those sites provide. It’s an integral part of professional web design and development
> 
> &#x2013; [16 Things to Improve Your Website Accessibility (Checklist) | WebsiteSetup.org](https://websitesetup.org/web-accessibility-checklist/) 

