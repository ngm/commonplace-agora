# Setting up a new box

Documenting the steps needed to set up a new Linux box.  I'd kind of ultimately like to get things smooth enough to the point where I'm fine doing a fresh install of the OS pretty regularly, like every couple of months.  Kind of a [[disaster recovery]] thing.


## Main goals

My main usage is:

-   [[org]]
-   [[org-roam]]
-   [[emacs]] for coding

So it's pretty much all about getting emacs set up.  The rest is just sugar on top after that, really.


## Install Debian

[[Debian]] as OS.  [[Why Debian?]]

Using Gnome 3 for desktop environment for now.  Not a huge fan though.
Using XFCE with MX Linux.


### Some minor quality of life things


#### Theme

Change to adawaita-dark via `Tweaks / Appearance`.

I could probably script this if I wanted:[ command line - How can I script the settings made by gnome-tweak-tool? - Ask &#x2026;](https://askubuntu.com/questions/971067/how-can-i-script-the-settings-made-by-gnome-tweak-tool) 


#### Guake

A heads up display for the terminal.

```shell
sudo apt install guake
```

-   Run `guake-preferences`, set `Keyboard shortcuts -> Toggle visibility` to `Ctrl+'`.


## In the beginning there was git

At present I have much of my stuff backed up in a git repo somewhere, so I need git to pull that in.


### add my user to sudoers

Already done on MX Linux.


### get git

Already installed on MX Linux.

```shell
sudo apt install git
```


## Passwords and ssh keys

Then I need my passwords and ssh keys.

Realising a slight flaw here in that in theory I need my keys in order to access gitlab.  In practice I can currently get it via password-based access, and I can remember my password.  Or I can copy from an existing device.  But assuming I lost all my devices, and also my memory, I would perhaps not be able to access (though I might be able to get emailed reminders - though what if I couldn't access my email?)  Anyway, upshot is, I should be storing my keys somewhere offline and always close to hand, too.


### keepassxc

I use [[keepassxc]] for my personal password manager.

```shell
sudo apt install keepassxc
git clone https://gitlab.com/ngm/keepass ~/Documents/keepass
```


### ssh keys

```shell
mkdir .ssh
cp ~/Documents/keepass/keys/* ~/.ssh/
chmod 600 ~/.ssh/*
```


## get my dotfiles

```shell
git clone https://github.com/ngm/dotfiles .dotfiles
```


## Install spacemacs


### Install emacs

Available from apt on MX Linux.

```shell
sudo apt install emacs
```

Only version 27.1 at current time of writing though.


#### or from source.

on straight debian, you don't have emacs 27 out of the box.  
so guess go with installing from source, hey ho.  that's fine, but takes a while.

[[Installing Emacs from source]]


### install spacemacs

```shell
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```


### setup my config

```shell
ln -s ~/.dotfiles/spacemacs.d ~/.spacemacs.d
```


### install source code pro

see: [software installation - How to use the Adobe Source Code Pro font? - Ask Ubuntu](https://askubuntu.com/questions/193072/how-to-use-the-adobe-source-code-pro-font) 

```shell
sudo wget --content-disposition -P /usr/share/fonts/opentype/source-code-pro https://github.com/adobe-fonts/source-code-pro/blob/29fdb884c6e9dc2a312f4a5e2bb3b2dad2350777/OTF/SourceCodePro-{Black,BlackIt,Bold,BoldIt,ExtraLight,ExtraLightIt,It,Light,LightIt,Medium,MediumIt,Regular,Semibold,SemiboldIt}.otf?raw=true 
```


### some of my libraries

```shell
git clone https://gitlab.com/ngm/commonplace-lib .emacs.d/private/commonplace-lib
```


### start emacs

Start it up, and spacemacs should then do its installation business.

See [[My Spacemacs User Config]] for more details.


### install icons

```elisp
M-X all-the-icons-install-fonts
```


## Set up org

org-mode itself is already set up from the spacemacs installation above.  This is just pulling in my own files.

```shell
git clone https://gitlab.com/ngm/org ~/org
```

Had to disable mu4e for some reason.


## Set up org-roam

Likewise, various bits of org-roam install and config are taken care of by spacemacs installation.

This is just pulling in my files.

```shell
git clone https://gitlab.com/ngm/commonplace ~/commonplace
```

For publishing to Agora:

```shell
git clone https://gitlab.com/ngm/ox-agora ~/.emacs.d/private/ox-agora
```

To get org-roam ready to rock:

```shell
M-X org-roam-db-sync
```


## Set up syncthing

I use [[syncthing]] to sync files between machines.  Most of what I sync I've already pulled in via git (which functions as the backup).  This just does real-time syncing of them when I make changes.


### Install

The default Debian (and MX Linux also) one is mega out of date, doesn't work with syncthing on other devices.

So use special repo. See https://apt.syncthing.net/

```shell
sudo apt install curl
sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
printf "Package: *\nPin: origin apt.syncthing.net\nPin-Priority: 990\n" | sudo tee /etc/apt/preferences.d/syncthing
sudo apt update
sudo apt install syncthing
```


### Connect to peers devices

Not sure of the recommended way of doing this.  They don't **all** need to be connected to each other.

I think probably best to connect to my phone first, as that's more likely to be on.  Or, connect to noodlemaps, as that's also potentially an always on peer.

That said, sometimes syncthing will handily pick up another box that's on the same physical network as it.


### Add shared folders for syncing

The main ones:

-   orgzly (my org files, but I've called the folder orgzly sometime in the past when I was first syncing to just phone)
-   keepass


### Add to startup programs

-   Gnome: Add 'Start Syncthing' to startup apps via Gnome Tweaks.
-   XFCE
    -   Session and Startup
    -   Add application
        -   Name: Syncthing
        -   Description: Start syncthing server
        -   Command: `/usr/bin/syncthing`
        -   Trigger: on login
        -


## Set up tmux

[[tmux]].

It's already installed on MX Linux.

```shell
ln -s .dotfiles/tmux.conf .tmux.conf
```


## Install Nextcloud

What's this for?  Calibre and photo syncing I think.


### download: https://nextcloud.com/install/#install-clients


### install

```shell
mv Downloads/Nextcloud-3.3.0-x86_64.AppImage Programs/
cd Programs
chmod u+x Downloads/Nextcloud-3.3.0-x86_64.AppImage
./Nextcloud-3.3.0-x86_64.AppImage
```


### Configure folders

In Nextcloud Settings, check InstantUpload, Multimedia, and Shared multimedia folders in the list of synced folders.


## Setup calendar

&#x2026;


## Setup work account


### create new user

```shell
sudo adduser restart
sudo usermod -aG sudo restart
```

login as new user.


### redo some basic config stuff

-   guake key
-   adawaita-dark theme

(is there a way to apply this across all users?)


### pull down org files

```shell
git clone <xxx>/restart.git ~/Documents/org
```


### setup 1password

While for personal I use keepassxc, for work we use 1password.

-   login, get key from emergency info pdf
-   add firefox extension


### get my dotfiles

```shell
git clone https://github.com/ngm/dotfiles .dotfiles
```


### set up spacemacs

like on other account.

```shell
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```


### setup my config

```shell
ln -s ~/.dotfiles/spacemacs.d ~/.spacemacs.d
```

I need to do something different here in order to have different configs for work and personal accounts.


### pull down my notes

```shell
git clone <xxx>/restart-kbase.git ~/Documents/kbase
```


### set up ssh

So I can access various servers.
At the moment, copy my keys via usb.  Need to work on this a bit though.

something like https://ferrario.me/using-keepassxc-to-manage-ssh-keys/


## Setup Apache

```shell
sudo apt install apache2
```


## Configure Firefox


### Settings

I could perhaps just use Firefox Sync here, but it's only a few settings and I don't particularly want to set up the account.

-   homepage and new windows - blank page
-   new tabs - blank page
-   search engine - YunoSearx


## Install Calibre


### Install

As per https://calibre-ebook.com/download_linux

Install script. (always be wary of running someone else's script!!!)

```shell
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
```


### Configure

Set `/home/neil/Nextcloud/Shared multimedia/eBook` as the library folder.

