# Resonate

URL
: https://resonate.is

A musician/listener/worker-owned streaming music platform coop.     

I love it. I'm a listener-owner.  

I became a co-owner sometime in November 2016 according to my logs.  But not used it regularly until 2022.

I got this funky badge at the time:

![[images/resonate-owner.png]]

