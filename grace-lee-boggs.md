# Grace Lee Boggs

> Grace Lee Boggs was long a fellow traveller with James in the factions and divisions of sectarian Marxism, a student and friend of Third World revolutionaries.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> She and James Boggs thought their way into a “politics of personal development” that rejected partisan orthodoxies in favour of a more iterative “dialectical humanism”, in which political visions and the peo- ple who hold them evolve together through struggle.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> From the systems thinker [[Margaret Wheatley]] came Boggs’s frequent affirmation of “critical connections” over “critical mass” – a conviction that the germ of seismic change lies in the thick relationality of how people choose to self-organise day to day. She drifted from Leninism, but the imperative of self-governance only deepened.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> Later in life, Boggs’s attention turned from achieving state communism to [[commoning]], the work of people continually discovering what they are seeking to achieve by stewarding shared projects and resources.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> She became a mentor to veterans of the 2011 [[Occupy Wall Street]] and its “leaderless” travails.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

