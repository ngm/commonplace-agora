# Hackerspace

> In an earlier article in the Journal, maxigas demonstrated how situating the distinct historical genealogies of hacklabs and hackerspaces in earlier [[autonomist]] movements improves appreciation of the strategic issues confronting spaces today
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

^ refers to  [[Hacklabs and hackerspaces – tracing two genealogies]]

