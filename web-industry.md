# Web industry

> If the commercial web is "industrial", you could say that the [[small web]] is "artisanal". 
> 
> &#x2013; [Rediscovering the Small Web - Neustadt.fr](https://neustadt.fr/essays/the-small-web/) 

-   [[Social industry]]
-   [[Culture industry]]

