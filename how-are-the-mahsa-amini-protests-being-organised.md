# How are the Mahsa Amini protests being organised?

[[Organising]]. [[Mahsa Amini protests]].

> Flashmob-style protests, images beamed onto tower blocks, water fountains dyed blood-red: young Iranians **armed with little more than their phones** have adopted a slew of tactics to give demonstrations over Mahsa Amini's death staying power.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]


## Video / citizen journalism

> Despite internet curbs cutting access to popular apps like Instagram and WhatsApp, savvy youngsters have still managed to get out videos of their protests.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

<!--quoteend-->

> Iran Wire has become an essential player using technological savvy and internet sleuthing to determine a death toll from the protests. Its live video footage is regularly shown on CNN. IranWire’s network of citizen journalists — everyday citizens wanting to hold the government accountable — help it break news on stories capturing global attention, from the fallout from Amini’s death to the punishment of Iranian climber Elnaz Rekabi for competing without a hijab.
> 
> &#x2013; [[Reporting in Iran could get you jailed. This outlet is doing it anyway.]]

<!--quoteend-->

> Since 2014, IranWire has trained roughly 6,000 Iranians on how to turn events they see firsthand into a piece of journalism that can be verified and hold up to scrutiny.
> 
> &#x2013; [[Reporting in Iran could get you jailed. This outlet is doing it anyway.]]


## Avoiding identification / surveillance

> Youths have in turn taken to wearing masks, switching their phones to "airplane mode" to avoid being located and packing extra clothes to replace those splattered with paint.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

Airplane mode to avoid being located.

> In a video shared on social media, protesters dismantled a surveillance camera high above a road in Sanandaj, a city in Amini's home province of Kurdistan.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

<!--quoteend-->

> Digital freedom activists are also trying to teach Iranians how to access the Tor browser, which lets users connect to normal websites anonymously so that their ISPs can’t tell what they’re browsing. 
> 
> &#x2013; [[Hacktivists seek to aid Iran protests with cyberattacks and tips on how to bypass internet censorship]]

Circumvention/dismantling of surveillance infrastructure.


## Structure

Flashmob-style protests.

> "Compared to previous protests this new round is more decentralised, without a particular leadership and organisation and a particular demand like a policy change," said Omid Memarian, senior Iran analyst at Democracy for the Arab World Now (DAWN).
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

Networked and decentralised.

> "The more organised and coordinated they become, the greater the chance they can broaden their base of support and present a clear, near-term challenge to the system," he told AFP.
> 
> "But the state's security apparatus excels at disrupting precisely that type of organised opposition, with a well-honed toolkit of violence, arrests, internet disruptions and intimidation.
> 
> "So, for the time being, the state and the protesters are in an unstable equilibrium, with neither able to overcome the challenge posed by the other, which suggests that these current protests and violence could persist for an extended period."
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

Tension between decentralised networked organisation and more central organisation.


## Hacktivism

> On the same day, activists from the Edalat-e Ali group hacked a state television live news broadcast, superimposing crosshairs and flames over an image of supreme leader Ayatollah Ali Khamenei.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

<!--quoteend-->

> Thousands of amateur hackers have organized online to orchestrate cyberattacks on Iranian officials and institutions, as well as share tips on how to get around curbs on internet access by using privacy-enhancing tools.
> 
> &#x2013; [[Hacktivists seek to aid Iran protests with cyberattacks and tips on how to bypass internet censorship]]

Hacktivism.


## Public imagery

> In a video taken one night, Amini's face was projected onto the side of a residential tower block in Ekbatan Town, in Tehran, as protesters chanted slogans from the safety of windows or rooftops.
> 
> &#x2013; [[The Bold Tactics That Have Kept Iran Protests Going]]

Projections.

