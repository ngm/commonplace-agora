# Fragment on Machines

> In the “Fragment,” Marx seems to sketch a future of fully automated production, “an automatic system of machinery,” 34 perhaps even a [[Fully Automated Luxury Communism]] driven by a “[[general intellect]]”—an assemblage of accumulated technical knowledge that could be an anticipation of the digital networks of the internet
> 
> &#x2013; [[Breaking Things at Work]]

