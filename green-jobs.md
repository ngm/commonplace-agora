# Green jobs

> Insulating homes in Britain and installing heat pumps could benefit the economy by £7bn a year and create 140,000 new jobs by 2030, research has found
> 
> &#x2013; [Energy-saving measures could boost UK economy by £7bn a year, study says | En&#x2026;](https://www.theguardian.com/environment/2022/sep/20/energy-saving-measures-could-boost-uk-economy-by-7bn-a-year-study-says)


## Bookmarks

-   https://policy.friendsoftheearth.uk/sites/files/policy/documents/2021-03/EMERGENCY_PLAN_GREEN_JOBS_FEB_2021.pdf

