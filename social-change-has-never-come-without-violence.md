# Social change has never come without violence

[[Social change]] has never come without [[violence]].

Joel Wainright said this in [[Climate Leviathan with Joel Wainwright and Geoff Mann]].  My history isn't good enough to know if this is true or not.  He's pretty clued up though.


## So

-   You might then believe that [[Social change will never come without violence]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

