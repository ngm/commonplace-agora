# For an anti-colonial, anti-racist environmentalism

URL
: https://theecologist.org/2018/may/14/anti-colonial-anti-racist-environmentalism

Author
: [[Robert Biel]]

> General [[Systems Theory]] can be a useful tool for understanding nature, and how society can exist in harmony of nature. The seminal book [[Limits to Growth]] used a systemic analysis - but was itself limited. Dr ROBERT BIEL examines how the systemic view can shed light on the colonial history of the North / West and the role it plays in the world's extractive present

[[Anti-colonialism]].  [[anti-racism]]. [[Environmentalism]].  [[Systems theory]].  [[The Limits to Growth]].

