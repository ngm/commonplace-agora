# Antilibrary

A private collection of unread books. Having unread books is not necessarily a bad thing.

> Unread books can be as powerful as the ones we have read, if we choose to consider them in the right light.
> 
> -   [Building an antilibrary: the power of unread books - Ness Labs](https://nesslabs.com/antilibrary)

I have an [[antilibrary of articles]].  I talked about this in [[Layer 0]] - but having just come across the word antilibrary, it might be a better term.  


## Resources

-   [Building an antilibrary: the power of unread books - Ness Labs](https://nesslabs.com/antilibrary)

