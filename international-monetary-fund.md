# International Monetary Fund

> July of 1944, the United States government convened a group of seven hundred delegates, from all the Allied countries, to design the postwar financial order. They met at the Mount Washington Hotel in Bretton Woods, New Hampshire, and there, after three weeks of meetings, they published recommendations that when ratified by the member governments resulted in the International Bank for Reconstruction and Development, and the International Monetary Fund. The intended results of these new entities included the establishment of open markets and the stability of member nations’ currencies
> 
> &#x2013; [[The Ministry for the Future]]

