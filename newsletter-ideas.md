# Newsletter ideas

Something around reclaiming the stacks.  Regular writing on that topic.  But don't want it to be restricted to only that.  Though that will likely be the focus.

Probably with some kind of what I've been reading / listening to aspect as well.  Those are always useful.

It could just as easily be a series of web posts.  But something about the newsletter aspect of it makes it more of a nudge to do it regularly.

Relatively informal.  Not polished articles.

Keep it fairly short - link out to anything longer.  People don't have time for huge lengthly newsletters.

A good way to build up a portoflio of structured writing.  (Wiki is fine, but it's not particularly structured.)

-   [[Newsletter: June 2023]]
-   [[Newsletter: July 2023]]

