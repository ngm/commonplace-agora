# Climate Leviathan with Joel Wainwright and Geoff Mann

A
: [[podcast]]

URL
: https://www.upstreampodcast.org/conversations

(Hmm, AntennaPod didn't seem to pick up timestamps when I shared for some reason.)

-   The political consequences of climate collapse.

-   [[Climate Behemoth]] sounds a bit like [[climate sadism]].

-   [[Climate Leviathan]] sounds a bit like the Ministry for the Future in [[The Ministry for the Future]].

-   Framing it as [[Anthropocene]] suggests that all of humanity is to blame. But it is primarily a specific part of humanity that is to blame - [[capitalism]].

-   New form of [[climate denialism]].

-   A large number who may seem reformist are not necessarily. They do not oppose radical action. When the wind changes, then they will be happy to participate.  Kind of a social [[tipping point]]?

-   The [[hegemony of liberalism]] and individualism extinguishes the imagination for collective action and structural change.

-   Bigs up [[Andreas Malm]] but critiques some of [[How to Blow Up a Pipeline]].

-   Good discussion on problems of [[eco-terrorism]], contrasts with necessity of [[eco-socialist]] organising.

-   Acknowledges that sadly [[Social change has never come without violence]].  But we must think how can we bring about change while **minimising** violence as much as possible.  [[When bringing change we must minimise violence]].

-   [[Judi Bari]].

-   [[The Monkey Wrench Gang]]

