# WCV2: Duncan Exley "Working class campaigners want to change things"

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/wcv2-duncan-exley-working-class-campaigners-want-to-change-things

Series
: [[Working Class Voices]]

Discussion on how the [[working class]] can get involved in the climate movement.

[[Working class and green politics]].

