# artificial cheapness of new products

It really, really shouldn't be cheaper to purchase an entire new product manufactured elsewhere in the world than it is to fix or replace one part locally.

Why are new products so cheap?

[[Worker exploitation in electronics manufacturing]].

