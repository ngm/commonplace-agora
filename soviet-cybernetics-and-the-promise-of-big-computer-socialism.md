# Soviet Cybernetics and the Promise of Big Computer Socialism

A
: [[podcast]]

URL
: https://cosmopod.libsyn.com/soviet-cybernetics-and-the-promise-of-big-computer-socialism

Series
: [[Cosmopod]]

> a discussion on the history of [[Soviet Cybernetics]] and the use of computers for [[socialist planning]]. We discuss the origins of [[Cybernetics]], its role as a reform movement in the sciences, and why cybernetics became attractive to the Soviet academy in the 50s, before moving to the biographies and projects of [[Anatoly Kitov]] and [[Viktor Glushkov]]. We reflect on the failures of [[OGAS]], and what could have been done better, as well as its positive legacy and finish by discussing the ways in which cybernetics was kept alive until the collapse of the USSR and the remaining possibilities for computerized planning.

OGAS was more an attempt at nationwide [[ERP]] than at a network for the people.

