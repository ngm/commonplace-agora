# Social and environmental issues are interconnected and inseparable

A
: [[claim]]

[[Metabolic rift]] probably relates to this.

> This ecological rift is the result of a social rift: the domination of humans over humans.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

Same as what [[Bookchin]] says. Domination of humans over humans leads to domination of humans over nature. Interesting to hear that Marx said this also.  But yes capitalism could be described as the domination of humans over humans.

-   [The critical intersection of environmental and social justice: a commentary |&#x2026;](https://globalizationandhealth.biomedcentral.com/articles/10.1186/s12992-021-00686-4)
-   [Interconnected Challenges of the 21st Century](https://globaldialogue.isa-sociology.org/articles/interconnected-challenges-of-the-21st-century)
-   [How Social Justice and Environmental Justice Are Intrinsically Interconnected](https://blog.pachamama.org/how-social-justice-and-environmental-justice-are-intrinsically-interconnected)

