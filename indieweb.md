# IndieWeb

URL
: https://indieweb.org

A social network made out of the World Wide Web.  Not locked in to big monolithic platforms (Facebook, Twitter).

> At the heart of the IndieWeb is an attempt to unify the ideas behind personal websites, blogs and social networks, but in a manner consistent with how the world wide web operates.
> 
> &#x2013; [Your Website Is Your Castle - Desmond Rivet](https://desmondrivet.com/2020/01/05/website-castle) 

My website at https://doubleloop.net is an IndieWeb site.

> Your website acts much like your wall on Facebook or your timeline on Twitter - it's your personal soapbox, your castle on the web.
> 
> &#x2013; [Your Website Is Your Castle - Desmond Rivet](https://desmondrivet.com/2020/01/05/website-castle) 

<!--quoteend-->

> [&#x2026;]one recreates, in a decentralized manner, the kinds of online interactions one has come to expect from private social networks.
> 
> &#x2013; [Your Website Is Your Castle - Desmond Rivet](https://desmondrivet.com/2020/01/05/website-castle) 


## Misc notes


### rss

-   rss-bridge: https://github.com/RSS-Bridge/rss-bridge


### The fedi apps are essentially social readers

-   readers with built in interaction
-   the problem is that they only implement the mastodon api
-   not AP removed from that
-   i can't use my AP enabled site and plug into an existing fed reader
    -   fed readers should implement AP, not mastodon APIs


### Offline/local/sneakernet IndieWeb

-   something like Secure Scuttlebutt, but using IndieWeb building blocks
-   quite a few related brainstorming sessions already taken place: 
    -   https://indieweb.org/local_first
    -   https://indieweb.org/offline_first
    -   https://indieweb.org/2019/Brighton/localoffline
-   I'd imagine would use static sites/git/locally-hosted webmention services (telegraph/webmention.io), something like that


## Misc

-   [Why The IndieWeb? (Webbed Briefs)](https://briefs.video/videos/why-the-indieweb/)

> “Solidarity grows through increasing liberty, not through constraint or obligation,” writes Ross. “Personal autonomy and social solidarity do not oppose each other, but instead reinforce each other.” In an age in which online spaces feel more divisive and polarized than ever, perhaps it is time to ponder how we can create conditions of personal autonomy that give rise to greater social solidarity.
> 
> -   [The Judgment of Paris | Lizzie O’Shea](https://thebaffler.com/salvos/the-judgment-of-paris-oshea)

<!--quoteend-->

> self hosting purity tests have no end - you can never be truly independent of everyone and everything else - so we sidestep that debate entirely by being a big tent and saying all you really need is domain and data portability, everything else is up to you
> 
> &#x2013; https://chat.indieweb.org/dev/2022-02-26#t1645893424908900

