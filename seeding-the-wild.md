# Seeding the Wild

URL
: https://one.compost.digital/seeding-the-wild/

Publication
: [[Compost]]

Author
: [[Magma Collective]]

> Energies toward peer-to-peer decentralisation are rising again. Riding this fresh breeze, we wonder if these nature analogies can lift us away from the monocultures of platform giants towards a flourishing technological biodiversity.

<!--quoteend-->

> This experimental essay **explores the similarities between the client-server network architecture and the historical [[expropriation of the commons]]**, both of which forced the creation of a working class and moneyed intermediation.

Love the illustrations.  Love the two main thrust of it: exploring the similarities between client-server network architecture and [[enclosure of the commons]], AND the idea of using nature analogies.  Should be good.

The anti-client-server theme will probably relate to ideas from [[Information Civics]] and [[Telecommunism]].

> How can I prevent my work from being expropriated without care if I don’t put a price on it? Must we guard our gardens with fences?

^ licenses such as [[Peer Production License]] might be an approach?

> The popular imagination seems stuck in taking revenge on the system by zealously surpassing it in new get-rich-fast games. “Have fun staying poor”, as bitcoiners like to say in the latest bitrush. This is hardly a call for change in a world tumbling into unprecedented inequalities.

<!--quoteend-->

> When the internet first came along it looked like an immensely liberating tool, poised to rebalance the power brought about by the press and consolidation of media ownership. People thought the internet would finally evolve [[the topology of information networks]]: from one-to-many to many-to-many.

The web began with so much potential, but the web did not liberate.

> Why did this tool, which allowed everybody to talk to everybody else, not lead to the creation of a thriving biodiversity of digital commons? How did giving everybody a voice open the door to data extractivism and digital monopoly monoculture?

<!--quoteend-->

> Power, politics, and plenty of other widely written about answers have been suggested. But **our interest here is in the role of the client-server network model**. Herein lies some of the technical context for how the network, digital and data commons have been born into servitude.

OK cool, so the particular focus of the article is how client-server has played a role in the consolidation of hierarchy on the web.

> In the early days of the World Wide Web, websites generally hosted content which was created by the people who set up the website. The client-server model made sense. Viewers interested in particular content, connected directly to the content creator’s website. In the era of the dot-com boom and early e-commerce, a website of a particular company generally offered products and services provided by that company. But with the advent of web platforms, the ‘clients’ of a website came to both provide and receive content, or in the case of e-commerce platforms, ‘clients’ can offer products and services as well as consume them.

So the [[client-server model]] became more of a problem with platforms.  It's not a big deal if the server is just representing one peer, essentially.

> The [[IP address space]] is limited, and home internet connections are often given dynamic (changing) IP addresses. This gave many of us our inferior role as ‘clients’.
> We can consume services but not provide them, as we are not addressable to others.

<!--quoteend-->

> We have drifted away from the idea of a website being the self-published expressions of a group or an individual. 

^ [[IndieWeb]], [[digital garden]] adjacent


## Summary

I liked it a lot.  Peer-to-peer networks, commons, enclosure, big tech, the topology of information networks. Hole punching.  A nice primer on how big tech enclosed the commons and how peer-to-peer might help take back some ground.

I think there's some space for reflection on the other aspect of the article, the use of nature analogies for technological concepts.  I like them (e.g. digital garden) but I think need to think about what they might hide or distort, too.

