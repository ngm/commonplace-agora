# Elinor Ostrom

-   https://doubleloop.net/?s=ostrom
-   [[Elinor Ostrom's Rules for Radicals]]: Cooperative alternatives beyond markets and states

> a resource arrangement that works in practice can work in theory

^ In a very small nutshell that sums up Ostrom's huge contribution to the commons.


## Building on Ostrom

> Ostrom worked from the perspective of the rational-actor school of economics and a focus on resource management at small scales. So she worked within a limited analytic framework and did not engage with political economy.  
> 
> &#x2013; [[David Bollier, P2P Models interview on digital commons]]

<!--quoteend-->

> To the extent that Ostrom and many other commentators, economists, and politicians focus on the Commons as chiefly a resource, it shifts attention to social and political questions of how a Commons organises itself into the background. It adopts the standard rational actor frame of Homo economicus and puts into the background the contestable, negotiable, circumstantial ways in which social governance emerges. 
> 
> &#x2013; [[David Bollier, P2P Models interview on digital commons]]

