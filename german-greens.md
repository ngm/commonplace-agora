# German Greens

> The story of the German Greens has been told many times: briefly, those on the left, involved in social movements, joined a platform to fight elections. Those who had been involved in the student movement, and some sympathetic to the Baader-Meinhof gang, joined environmentalists. At first the Greens were, in the words of an early leader figure Petra Kelly, ‘the anti-party party’ (Emerson, 2011: 55). Given the openness of the German electoral system, co-option was perhaps close to inevitable. Greens were elected on radical platforms but eventually joined coalition regional governments with the SPD, and the party over the decades has moved broadly to the centre right.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

