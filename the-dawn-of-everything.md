# The Dawn of Everything



## Criticism

> However, Graeber and Wengrow's overall narrative rejects basic materialist analysis in favor of a muddy historical idealism and often deploys questionable rhetoric in the place of rigorous argument, as Walter Scheidel has pointed out in: "Resetting History's Dial? A Critique of David Graeber and David Wengrow, The Dawn of Everything: A New History of Humanity",
> 
> &#x2013; [[Forest and Factory]]

