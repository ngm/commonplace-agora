# 42. TECH FOR GOOD

A
: [[podcast]]

URL
: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood

Series
: [[Reasons to be Cheerful]]


~00:09:43 Digital revolution has to be combined with democratic revolution

~00:15:25  [[Data commons]]

~00:15:50  [[Participatory democracy]] rather than just consultation

~00:19:52  [[Community mesh networks]] in [[Red Hook, Brooklyn]] and in Detroit

~00:22:26 Mesh was only network that worked in Red Hook during [[Hurricane Sandy]].

~00:23:01  Red Hook mesh works in emergency and to help those who have no connection otherwise

~00:36:30  [[Connectivity]] is fundamental to human life these days

~00:37:22 The deep value of something like WiFi is in empowering people (same true of repair cafes?)

~00:40:32  [[Bristol Approach]], damp sensing

