# The dream of superabundance is over

The idea of [[Red Plenty]] / [[fully automated luxury communism]] is over. For now.  We don't have the time - in the present we must focus on reduction.

> Traditionally, socialist utopias envisioned a society based on a superabundance of essential goods which could be treated as though they were free. Thus, markets would be eroded, and the compulsion of work would be reduced
> 
> &#x2013; [[For a Red Zoopolis]]

