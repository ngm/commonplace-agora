# John Oswald

> John Oswald, a Scottish [[Jacobin]] in the thick of the [[French Revolution]], believed in the congruence of political revolution and a revolution in humanity’s relation to nature: ‘the day is beginning to approach when the growing sentiment of peace and good-will towards men will also embrace, in a wide circle of benevolence, the lower orders of life
> 
> &#x2013; [[Half-Earth Socialism]]

