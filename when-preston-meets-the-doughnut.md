# When Preston Meets the Doughnut

Really enjoyed the When Preston Meets the Doughnut webinar.  Inspiring to hear more about what's going on in [[Preston]], and to see how Kate Raworth has been working on adaptations of [[Doughnut Economics]] to the city-level.

[[Community wealth building]], and the ideas of thriving but living within planetary boundaries, seem to work well together.

-   [When Preston Meets the Doughnut: Community Wealth Building &amp; C21 Economics Ti&#x2026;](https://www.eventbrite.co.uk/e/when-preston-meets-the-doughnut-community-wealth-building-c21-economics-tickets-131573386541)

