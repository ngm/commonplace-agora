# The Lucas Plan

I really like the Lucas Plan.  An example of how reskilling of skilled workers could be possible and could all come from the workers themselves, not a top-down decree.  It is a positive example of how reskilling could work under the [[Green New Deal]].

-   rampant [[Taylorism]] would make reorienting factories more difficult
    -   machines and people are so specialised to one product, they don't have general skills needed to be repurposed?


## overview

-   Workers plan from 1976 at Lucas Aerospace on how to avoid job losses by producing things useful to society
-   "the Combine argued that state support would be better put to developing products that society needed"
-   It included ideas of things the factory could make to reskill workers
-   these were all pretty compatible with current energy transition requirements
-   heat pumps, solar cell technology, wind turbines and fuel cell technology

> Like many in manufacturing in the UK at the time, workers at [[Lucas Aerospace]] were facing redundancy and the decline of their communities in the face of industrial restructuring by capital, international competition and relocation, and increasing technological automation in design and production. In January 1976 workers published an Alternative Corporate Plan for the future of Lucas Aerospace. This innovative measure anticipated management cuts to thousands of jobs. Instead of redundancy, workers argued their right to [[socially useful production]].
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The Lucas Plan was unusual in that, through careful analysis of skills, machinery, work organisation, and economic potential, workers themselves proposed innovative alternatives to closures.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The plan contained economic analysis; proposals for training that enhanced and broadened skills; a less hierarchical restructuring of work that broke divisions between practical shop floor knowledge and theoretical design engineering knowledge. It challenged fundamental assumptions about how design and innovation should operate.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> It took a year to put the Plan together, including designs for over 150 alternative products.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Half of Lucas Aerospace’s output supplied military contracts. This business area depended upon public funds – as did many of the firm’s other activities. Moreover, UK governments had since the 1960s been financing the “rationalisation” of manufacturing sectors, and paid the welfare benefits of those who became unemployed as a result of this restructuring. Activists argued state funds would be better put to investing in socially useful production. Arms conversion arguments attracted interest from the peace movement and social activists more widely. Additional proposals in the Plan, such as for human-centred technologies that enhanced skills rather than displaced labour, and for socialised markets for products, caught the attention of those associated with the Left. Here was a practical example for connecting new forms of trades
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]


## useful links

-   [The Plan](http://theplandocumentary.com/) film
-   [Story of the Lucas Plan](http://lucasplan.org.uk/story-of-the-lucas-plan/)

