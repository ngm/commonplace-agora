# apply: Symbol's value as variable is void: symbolp

Getting this error when I try to execute various projectile related commands after updating [[spacemacs]]. Things such as `SPC p p` (`helm-projectile-switch-project`), `SPC p b`, etc.

I've done `SPC f e d`.  So that's up to date.  I've done `SPC f e c`, so things are recompiled.

Helm still seems to work elsewhere.  It's just projectile?  Or helm-projectile?

OK, done what I had to do once before with switch - switch to `ivy` layer rather than `helm` for now.  This seems to make use of `counsel-projectile` rather than `helm-projectile`, and is working fine for now.


## UPDATE <span class="timestamp-wrapper"><span class="timestamp">[2022-10-28 Fri]</span></span>

Switched back to `helm` today, and this error is no longer a problem.

