# Strategies of Protest with Oscar Berglund

A
: [[podcast]]

URL
: 

https://www.gndmedia.co.uk/podcast-episodes/strategies-of-protest-with-oscar-berglund

I got a lot from this.  Good discussion of strategies and tactics of [[climate activism]].

> This week on the show we are joined by Dr Oscar Berglund (@berglund<sub>oscar</sub>) from Bristol University. Oscars research focuses on social movements and explores climate change activism particularly [[Extinction Rebellion]] (XR) and the use of [[civil disobedience]]. Oscar studies the strategies and discourses of civil disobedience in contemporary climate change activism.
> 
> We discuss where XR is now as a movement, how actions by the [[Tyre Extinguishers]] and [[Insulate Britain]] affect wider climate activism discourse,  is the media is pushing climate into the culture war? How car ownership has developed as an identify and the curious case of F1 driver Sebastian Vettel.

~00:10:44  [[Climate justice]] narrative as a means to link climate issues to here and now.

~00:12:38  Climate change is class politics and political economy.

~00:29:47  [[Cars]] as symbols. Of 'absolute freedom.' Of masculinity. They are very closely tied to capitalism.

~00:40:35  Does the same civil disobedience repeated over start to get ignored? Does it have to reinvent? Does it only get covered as part of culture war?

~00:43:40  Can the [[Policing bill]] et al actually stop protest?

~00:45:54  Reference to [[Colston Four]] and acquittal by jury.  Same thing could happen more in relation to climate activism - technically breaking the law, but acquitted by jury.

~00:50:39  Don't necessarily need majority to change something, often strong minorities and alliances can work.

~00:52:50  Climate justice movement doesn't need to be just about climate. It can be about justice elements to pull more people in.

~00:55:46  Root the climate movement in local community.

~00:57:26  [[Climate Leninism and Revolutionary Transition]].

