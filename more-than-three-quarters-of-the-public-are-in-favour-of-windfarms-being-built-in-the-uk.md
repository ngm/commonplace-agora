# More than three-quarters of the public are in favour of windfarms being built in the UK

More than three-quarters of the public are in favour of [[windfarms]] being built in the UK

-   [Three-quarters of Britons back expansion of wind power, poll reveals | Wind p&#x2026;](https://www.theguardian.com/environment/2022/apr/10/three-quarters-of-britons-back-expansion-of-wind-power-poll-reveals)

