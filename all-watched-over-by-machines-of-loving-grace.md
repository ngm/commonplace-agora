# All Watched Over by Machines of Loving Grace

-   [[Poem]]
-   [[Richard Brautigan]]

I originally came across the poem All Watched Over by Machines of Loving Grace by Richard Brautigan via the Adam Curtis documentary of the same name ([[All Watched Over by Machines of Loving Grace (Adam Curtis)]]).

It’s kind of fascinating. I like it.  I know it came from a period whose [[Technological utopianism]] certainly didn’t come to pass, and might have been a bit off-key in the first place, but its sweetly optimistic (…or bitingly critical, depending on what way you squint at it).

It was written in 1967.

<div class="verse">

**All Watched Over by Machines of Loving Grace**<br />
<br />
I like to think (and<br />
the sooner the better!)<br />
of a cybernetic meadow<br />
where mammals and computers<br />
live together in mutually<br />
programming harmony<br />
like pure water<br />
touching clear sky.<br />
<br />
I like to think<br />
(right now, please!)<br />
of a cybernetic forest<br />
filled with pines and electronics<br />
where deer stroll peacefully<br />
past computers<br />
as if they were flowers<br />
with spinning blossoms.<br />
<br />
I like to think<br />
(it has to be!)<br />
of a cybernetic ecology<br />
where we are free of our labors<br />
and joined back to nature,<br />
returned to our mammal<br />
brothers and sisters,<br />
and all watched over<br />
by machines of loving grace.<br />

</div>

If it were written today it must surely be ironic. But I wonder if it was heartfelt back in the 60s?

I find what it paints, this harmony of nature and technology, to be kind of a mixture of pleasantly bucolic and desireable, and weird and creepy all at once. Not sure if I want it or not. I like the idea of a cybernetic ecology, where we are free of our labours, and joined back to nature. Not entirely so keen on being watched over by machines of loving grace. (Though the benevolent AIs in [[Iain M. Banks]]’ Culture novels could be good role models if we did want machines of loving grace…)

The idea of technology being more in balance with nature is good.  Though the poem kind of has it backwards - a kind of [[accelerationism]] of the technology, rather than maybe a [[degrowth]] to natural boundaries.

It’s interesting that the poem doesn’t really make a case for technology, other than the nod towards a kind of [[fully automated luxury communism]] at the end. It just sort of assumes that tech is the route to liberation – I guess that’s the flavour of the time. I’m not a primitivist, but I’m not sure that an IoT meadow will have all that much better benefit than the analogue equivalent.

Obviously [[cybernetics]] was hot stuff back then.  I would say that the poem misuses the term.  I don't think that cybernetics is necessarily about a conjoining of technology and nature.  It's the study of how organisations function and maintain themselves, and it just spans organisms/structures in technology and nature.

Notice that the poem increases in scale over verses, from meadow to forest to ecology:

-   cybernetic meadow
-   cybernetic forest
-   cybernetic ecology

Curtis explores a lot of these ideas in [[The Use and Abuse of Vegetational Concepts]] part of his documentary.

