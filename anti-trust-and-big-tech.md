# Anti-trust and big tech

[[Anti-trust]] won't necessarily work in breaking up [[Big tech]].

> simply increasing competition amongst largish for-profit corporations with the same surveillance capitalist business model is unlikely to meaningfully address the major underlying problems and outcomes of the platform economy. In fact, in some instances, around things like data collection, surveillance, and privacy, increased competition may only make things worse.
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

<!--quoteend-->

> Antitrust reformism is particularly problematic because it assumes the problem of the digital economy is merely the size and “unfair practices” of big companies rather than digital capitalism itself.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> Antitrust advocates argue that monopolies distort an otherwise ideal capitalist system and that what is needed is a level playing field for everyone to compete.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

