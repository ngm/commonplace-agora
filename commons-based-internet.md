# Commons-based Internet

[[Commons]]-based [[Internet]].

> A commons-based Internet requires commons-based design principles and a commons-oriented society (Fuchs 2011b, chapters 8 and 9).
> 
> &#x2013; [[Social Media: A Critical Introduction]]

<!--quoteend-->

> Struggles for a commons-based Internet need to be connected to struggles for [[socialism]].
> 
> &#x2013; [[Social Media: A Critical Introduction]]

