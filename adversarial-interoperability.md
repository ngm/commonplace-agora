# adversarial interoperability

> “That’s when you create a new product or service that plugs into the existing ones without the permission of the companies that make them,” writes [[Cory Doctorow]], special advisor to the Electronic Frontier Foundation.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/)

<!--quoteend-->

> “Think of third-party printer ink, alternative app stores, or independent repair shops that use compatible parts from rival manufacturers to fix your car or your phone or your tractor.”
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/)

<!--quoteend-->

> Without adversarial interoperability, users have limited [[agency]] and innovation is stifled.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/)

<!--quoteend-->

> The writer Cory Doctorow talks about “adversarial interoperability,” which describes a situation where one service communicates with another without the latter’s permission, or perhaps only with grudging permission secured through legislation.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> For most of modern history, this kind of guerrilla interoperability, achieved through reverse engineering, bots, scraping and other permissionless tactics, were the norm. But a growing thicket of “IP” laws creates severe legal jeopardy for these time-honored traditions. Just one of these IP rules — the “anti-circumvention” provision in Section 1201 of 1998’s Digital Millennium Copyright Act — provides for a five-year prison sentence and a $500,000 fine for anyone who bypasses “an effective means of access control.” And that’s for a first offense!
> 
> &#x2013; [[Freeing Ourselves From The Clutches Of Big Tech]]

