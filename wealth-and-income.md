# wealth and income

[[Wealth]] and [[income]].  What's the difference?

> Personal wealth means a stock of valuable possessions: anything from cash under your mattress, through shares and bonds, to the value of your house or your car. Income, on the other hand, is a flow of money you receive, such as wages for employment.
> 
> &#x2013; [What’s the difference between wealth inequality and income inequality, and wh&#x2026;](https://positivemoney.org/2017/10/wealth-inequality/)

