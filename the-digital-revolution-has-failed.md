# The digital revolution has failed

An
: [[article]]

Found at
: https://disconnect.blog/the-digital-revolution-has-failed/

Written by
: [[Paris Marx]]

A very brief history of the internet and how it ended up where it is. From liberatory potential to libertarian frontier to enclosed and privatised.

Summary of the various ills of [[Big Tech]].

> as the web declines, we need to consider what a better alternative could look like and the political project it would fit within

Right on. That would be [[digital ecosocialism]] in my view.

