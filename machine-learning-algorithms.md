# Machine-learning algorithms

Reinforcing good behaviour, ignoring bad, and giving enough practice.

> You give the machine data, a goal and feedback when it's on the right track - and leave it to work out the best way of achieving the end.
> 
> &#x2013; [[Hello World]] 

