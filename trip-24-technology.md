# Trip 24: Technology

URL
: https://novaramedia.com/2022/05/22/trip-24-technology/

[[Technology]].

> What defines each era? Often, historians lean on terms that point to technology: bronze, steam, carbon, silicon. So is technology a fundamental aspect of being human? On this wide-ranging Trip, the gang take on one of their biggest topics yet.
> 
> Starting from the basis that technology is an application of knowledge for a practical purpose, Jeremy Gilbert, Nadia Idle and Keir Milburn propose the washing machine as the most important invention of the 20th century. They also theorize a fully automated luxury Keynesianism, consider what motivates the left’s apparent technophobia, and discuss techno-adjacent political formations from the Luddites to to the cyborg feminists.

[[A Cyborg Manifesto]].

