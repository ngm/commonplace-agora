# #ACFM

A
: [[podcast series]]

URL
: https://novaramedia.com/category/audio/acfm/

> The home of the weird left. [[Nadia Idle]], [[Jeremy Gilbert]] and [[Keir Milburn]] examine the links between left-wing politics, culture, music and experiences of collective joy.

I really enjoy #ACFM podcasts.  They take fairly everyday things and look at them through a leftist lens, and throw a bit of music in too.  They seem to like [[municipal socialism]].

