# Problem: Error running timer

Getting this type of message from [[helm]] all of a sudden:

> Error running timer: (error "In 'org-roam-find-file' source: &#x2026;

Basically it seems whenever the search fails.  Is it just related to org-roam, or is it elsewhere?

Dunno.  But it's a blocker for me using org-roam at the moment.  So I've switched to ivy temporarily (like I did last time I had an error from helm - see [[Problem: no such file or directory tramp-archive]]).

No errors from ivy at the moment, so will stick with it for now so I can get some work done&#x2026;

