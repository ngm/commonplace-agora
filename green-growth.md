# green growth

> In sum, green growth scenarios play loose with science, assume incredibly unjust arrangements, and gamble with the future of humanity—and all of life on Earth—simply to maintain ever-increasing levels of aggregate output in high-income countries, which, as we will see, is not even needed.
> 
> &#x2013; [[On Technology and Degrowth]]

