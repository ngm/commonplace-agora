# Counter-anti-disintermediation

Tactics to avoid the centralisation of things that were once decentralised.

I think Github centralising the world's source code on the back of an entirely disintermediated tool is probably a good example of anti-disintermediation.  By the use of lock-in and network effects.

The assumption here is:

-   Disintermediation = good
-   Anti-disintermediation = bad
-   Counter-anti-disintermediation = a means to get back to good

> Centralization is required to capture profit. Disintermediating platforms were ultimately reintermediated by way of capitalist investors dictating that communications systems be designed to capture profit. 
> 
> &#x2013; [Exploitation and Rent-Seeking Models in Social Media and P2P Exchange Platforms](https://wiki.p2pfoundation.net/Exploitation_and_Rent-Seeking_Models_in_Social_Media_and_P2P_Exchange_Platforms#Counter-Anti-Disintermediation)

<!--quoteend-->

> Going back to an early Internet architecture of cooperative, decentralized servers, as projects such as Diaspora, GNU Social, and others are attempting to do, will not work.[5] This is precisely the sort of architecture that anti-disintermediation was designed to defeat. Decentralized systems need to be designed to be counter-anti-disintermediationist. 
> 
> &#x2013; [Exploitation and Rent-Seeking Models in Social Media and P2P Exchange Platforms](https://wiki.p2pfoundation.net/Exploitation_and_Rent-Seeking_Models_in_Social_Media_and_P2P_Exchange_Platforms#Counter-Anti-Disintermediation)

<!--quoteend-->

> Central to the defeat of this particular peer-to-peer movement was that its infrastructure was vulnerable to [[weaponised design]], in which the network protocol directly empowers attackers through its design. For BitTorrent, this empowerment came as the protocol exposes every user’s participation in the network. This data was exploited to unmask users, ruin lives and provide justification for new legislation. 
> 
> &#x2013; [[This is Fine: Optimism and Emergency in the Decentralised Network]] 


## What to do

[[Dmytri Kleiner]] suggests that peer-to-peer is the way to go to counter anti-disintermediation. (i.e. 
[[No Servers!  No Admins!]])

> Central to the counter-anti-disintermediationist design is the End-to-End principle: platforms must not depend on servers and admins, even when cooperatively run, but must, to the greatest degree possible, run on the computers of the platform’s users. [&#x2026;] By keeping the computational capacity in the hands of the users, we prevent the communication platform from becoming capital, and we prevent the users from being instrumentalized as an audience commodity. 
> 
> &#x2013; [Exploitation and Rent-Seeking Models in Social Media and P2P Exchange Platforms](https://wiki.p2pfoundation.net/Exploitation_and_Rent-Seeking_Models_in_Social_Media_and_P2P_Exchange_Platforms#Counter-Anti-Disintermediation)


## Twin pages

-   https://wiki.p2pfoundation.net/Counter-Anti-Disintermediation

