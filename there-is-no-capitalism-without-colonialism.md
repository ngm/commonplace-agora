# There is no capitalism without colonialism

A
: [[claim]]

There is no [[capitalism]] without [[colonialism]].

Spoken by [[Nick Couldry]] in [[The dangers of digital capitalism]].

