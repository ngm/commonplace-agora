# synthesis of horizontalism and verticalism

See [[Horizontalism vs verticalism]] for some background.   There's long been tension on the left between the two.

But to be honest a synthesis of [[Horizontalism]] and [[Verticalism]] seems like a no-brainer.  A completely spontaneous and dislocated network seems  doomed to a short lifespan, while a totalising party form seems demonstrably prone to corruption.  But for whatever reasons there seems a long history of polarisation of the two.  But post-2011 there also seems to be a good faith desire to find a synthesis.

I've recently read [[Anarchist Cybernetics]] (AC) which provides a compelling synthesis.  Kind of apologetically, like "hey I know we're anarchists but here's some good reasons why we might want structure".  All made sense to me though.  Currently reading [[Neither Vertical Nor Horizontal]] which also does a compelling synthesis - feels more from the perspective of 'look this tension is getting us nowhere, here's something that might actually work'.  Possibly aimed more at convincing verticalists (e.g. early days it was referred to as 'networked Leninism').

Topics that are related to a synthesis:

-   [[Viable system model]]
-   [[Institutional Analysis and Development]]
-   [[Heterarchy]]
-   [[distributed action]]

> Experience with multitudinous networks has led their early enthusiasts to call for more self-ordering. [[Hardt and Negri]] (2017) clarify their embrace of leaderless move- ments by stressing the need for “the institutionalization of free and democratic forms of life”, organised enough to be “able to take hold of the common” (xx). To [[Zeynep Tufekci]] (2017), the networked “signal” of movements can be self-defeating without or- ganisational “capacity”.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> He [[[Frantz Fanon]]] held that spontaneous energies must find institutional cohesion.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> In order to make a revolution, large-scale, coordinated movements are necessary, and their formation is in no way counter to Anarchism. What Anarchists are opposed to is hierarchical, power-tripping leadership which suppresses the creative urge of the bulk of those involved, and forces an agenda down their throats. 
> 
> &#x2013; [[Anarchist vs. Marxist-Leninist Thought on the Organization of Society]]

