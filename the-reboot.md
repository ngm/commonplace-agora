# The Reboot

> The Reboot is a publication that critically examines the internet. We investigate today’s most urgent internet issues to inform the public about network technologies, the data economy, and their effects on society.

-   https://thereboot.com/

Lots of articles up my street on there.  Seem to have something of a focus on [[Decentralisation]].

-   [[Toward a Digital Economy That’s Truly Collaborative, Not Exploitative]]

Only note is that it is published by an organisation called [Dfinity](https://dfinity.org/) which seem to have a bit of a blockchain thing going on, so I am just aware of some possible bias there.

