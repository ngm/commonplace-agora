# Commons-based peer production

> radically decentralized, collaborative, and nonproprietary; based on sharing resources and outputs among widely distributed, loosely connected individuals who cooperate with each other without relying on either market signals or managerial commands.
> 
> &#x2013; [The Wealth of Networks » Chapter 3: Peer Production and Sharing](http://yupnet.org/benkler/archives/12) 

I feel like the [[IndieWeb]] is a great example of commons-based peer production, as per Benkler’s definition.

> It was Benkler (2002, p. 369) who, focusing on the [[digital commons]], postulated the emergence of a new mode of production, which he called commons-based peer production: “Its central characteristic is that groups of individuals successfully collaborate on large-scale projects following a diverse cluster of motivational drives and social signals, rather than either market prices or managerial commands”.
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

