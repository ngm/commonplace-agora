# Thomas Sankara

[[Burkina Faso]].

> Thomas Sankara recognized the revolutionary role of smallholder farmers. Immediately after coming to power, Sankara proclaimed the creation of the National Council of the Revolution and called on peasants and workers to form Popular Committees. The first of these emerged in poor neighborhoods in Burkina Faso’s capital before spreading to other towns and rural neighborhoods. A relation of accountability and shared struggle was established between the party and local democratic organizations. A dialectic of transition was formed. In his speech Imperialism is the Arsonist of our Fires and Savannas, Sankara shows how anti-imperialist struggle and ecological struggle are one and the same. In just over a month, Sankara’s government delivered basic courses in economic and environmental management to more than 35,000 peasants. Sankara’s Burkina Faso also planted millions of trees to push back the threat of desertification, presided over a successful vaccination and literary campaign, and achieved huge increases in agrarian productivity and irrigation. All this was possible because the party and the people worked at scale to realize a revolutionary transition.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

