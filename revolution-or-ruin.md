# Revolution or Ruin

URL
: https://www.e-flux.com/journal/110/335242/revolution-or-ruin/

Authors
: [[Kai Heron]] and [[Jodi Dean]]

[[Climate Leninism]].

-   [[eco-nihilism]]
-   [[the climate crisis is a space of class struggle]]
-   fundamental change is achieved through force, through class struggle, and through the agency of the oppressed
-   

