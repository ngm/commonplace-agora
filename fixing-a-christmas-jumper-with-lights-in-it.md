# Fixing a Christmas jumper with lights in it



## What is it?

A Christmas jumper with a circuit of lights in it.

It has a battery box that takes two CR2032 batteries, giving juice of 6v, connected by a JST connector(?) to the circuit that runs through the jumper.


## Problem

The lights in the jumper don't come on.


## Diagnosis

Checked that the connector was seated properly.  It was.

Tried with a new set of CR2032 batteries.  No difference.

Tested the batteries with a multimeter.  Showing 3.34 volts, so all good.

Tried checking the continuity of the output from the battery box.  Nothing coming out, but maybe just can't make a connection with the multimeter properly.

Done a bit more probing with the continuity tester.  Great tool.  Basically one of the wires from the battery box isn't working.  The crimp looks fine, so it must be a loose connection in the wire itself?  Which seems a bit strange, but honestly that what it looks like.

Could replace that wire, but I don't have the tool for redoing the crimp.  Probably easier to just buy a battery box.

I decided to trim out the middle bit, solder in a replacement wire, and put some heat shrink on.

So&#x2026; the battery pack now works.  Turn it on and&#x2026; only the first light comes on!  Bah humbug.

I cut a hole in the inner lining of the jumper, and it turns out that the wires seems to have come loose connecting to one of the lights.

