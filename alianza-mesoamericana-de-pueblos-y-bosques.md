# Alianza Mesoamericana de Pueblos y Bosques

URL
: https://www.alianzamesoamericana.org/

> We are Guardians of the Forest. We safeguard ancestral knowledge and combine it with innovative ideas. We protect the forests of Mesoamerica and seek solutions for a balanced coexistence with nature. We promote initiatives that combine technology, traditional wisdom and territorial governance for the well-being of our communities and the entire world.

