# détournement

> The mutual interference of two worlds of feeling, or the juxtaposition of two independent expressions, supersed[ing] the original elements and produc[ing] a synthetic organization of greater efficacy.
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The [[SI]] championed détournement as a means of interrupting the fabric of the everyday — whether it be repurposing old film reels, subverting iconic images or slogans, or devising literature inspired by the works of other writers. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The act of détournement imbues revered and historicized works of art and literature with new life, thereby overcoming their congealment at the hands of the spectacle. As Debord and Wolman write:
> 
> "Détournement not only leads to the discovery of new aspects of talent; in addition, clashing head-on with all social and legal conventions, it cannot fail to be a powerful cultural weapon in the service of real class struggle."
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> The device of detournement restores all their subversive qualities to past critical judgments that have congealed into respectable truths &#x2026; The defining characteristic of this use of detournement is the necessity for distance to be maintained toward whatever has been turned into an official verity &#x2026; Ideas improve.
> 
> &#x2013; [[Guy Debord]] (quoted in [[Capital is Dead]])

<!--quoteend-->

> Detournement is the antithesis of quotation, of a theoretical authority invariably tainted if only because it has become quotable, because it is now a fragment torn away from its context, from its own movement &#x2026; Detournement is, by contrast, the fluid language of anti-ideology &#x2026; Detournement founds its cause on nothing but its own truth as critique at work in the present.
> 
> &#x2013; [[Guy Debord]] (quoted in [[Capital is Dead]])

