# Critiques of social media



## Four critiques of social media

Outlined in [[Anarchist Cybernetics]]:


### privacy and the collection of data from users


### political economy

>    Approaching the critique of social media with this type of political economic analysis in mind also serves to draw attention to the material aspects of digital media. Fuchs (2014: 119–​20) highlights the manual labour that goes into mining the minerals required for producing the physical devices used to access digital media, such as smartphones and tablet computers, as well as the conditions of labour in the factories assembling these devices. The material economic cost to human beings, in places like China and the Democratic Republic of Congo, of making digital media possible ought also to be taken into consideration when discussing the appropriateness of social media platforms for anarchist and radical organising. This is especially true if such organising is framed in terms of prefigurative politics. Can action aimed at bringing relationships of solidarity and mutual aid into being be based on such dire and
> exploitative working conditions?
> 
> &#x2013; [[Anarchist Cybernetics]]


### the kinds of relationships social media produces between its users


### the forms of behaviour and of thinking that social media platforms produce and reinforce


## Misc

> Without persistent groups or organisations, [[Instagram]]’s eminent form of shared ex perience is the viral image, which circulates an affective impression of shared experi- ence. To spread, the image must be the kind of image that would spread, according to the tastes of the poster’s followers and the secret churning of the platform’s engineer- ing. An announcement for next week’s union meeting may not qualify. An organiser trying to strengthen workers’ bonds isn’t interested in infecting them like a virus.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> But the economy of virality does not bow to the drudgery or urgency of self-organisation. Platforms optimise for ‘engagement’ through chatter – not decision, resolution, or consensus. Community control is not in the specification.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> Social media anticipate a full sociality of human existence, but in their corporate form this potential is limited by capitalist structures of ownership and capital accumulation.
> 
> &#x2013; [[Social Media: A Critical Introduction]]

