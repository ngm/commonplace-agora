# Adrienne Buller, "The Value of a Whale: On the Illusions of Green Capitalism"

A
: [[podcast]]

URL
: https://newbooksnetwork.com/how-to-value-the-earth

Featuring
: Adrienne Buller

On the illusions of [[green capitalism]].

> examines the fatal biases that have shaped the response of our governing institutions to [[climate breakdown]] and environmental breakdown, and asks: are the 'solutions' being proposed really solutions?

<!--quoteend-->

> Tracing the intricate connections between financial power, economic injustice and ecological crisis, she exposes the myopic economism and market-centric thinking presently undermining a future where all life can flourish. 

The problems of treating the climate crisis as one solvable by market economics.

> examines what is wrong with mainstream climate and environmental governance, from [[carbon pricing]] and [[carbon offset markets]] to '[[green growth]]', the [[commodification of nature]] and the growing influence of the finance industry on environmental policy.

The influence of the finance industry on environmental policy.

> In doing so, it exposes the self-defeating logic of a response to these challenges based on creating new opportunities for profit, and a refusal to grapple with the inequalities and injustices that have created them. 

The mistaken logic of fixing climate breakdown with more growth.

-   [[Ecosystems services]] and [[natural capital]]. These are attempts to bring nature in to the market. Misguidedly trying to 'internalise the externalities'.
-   Remind me of also e.g. [[Half-Earth Socialism]], on the topic of deliberately keeping nature **out** of the market. (Interesting, Adrienne also references the book towards the end.)
-   Outlines the problems with asset management firms like [[Blackrock]] and [[Vanguard]].
-   Some have the idea that markets can magically bypass the thorny issue of doing politics.  [[Ludwig von Mises]] with 'one penny one vote' suggests that the market is the most democratic way of deciding that you can get.

