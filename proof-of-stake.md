# Proof of Stake

> "I know there’s ‘proof of stake’ but I don’t know if that can actually work unless everybody changes over to it. And even if it did, it doesn’t address the other issues that bother me.”
> 
> &#x2013; [Brian Eno is not a fan of NFTs](https://faroutmagazine.co.uk/brian-eno-not-a-fan-of-nfts/) 

