# PhotoPrism

URL
: https://photoprism.app/

> PhotoPrism® is an AI-Powered Photos App for the Decentralized Web.
> 
> It makes use of the latest technologies to tag and find pictures automatically without getting in your way. You can run it at home, on a private server, or in the cloud.  

I'd like to get it set up on YunoHost.

See:

-   https://forum.yunohost.org/t/photoprism-as-google-photos-replacement-my-experience/13510
-   https://blog.arkadi.one/p/photoprism-for-self-hosted-photo-management/
-   https://github.com/YunoHost-Apps/photoprism_ynh

