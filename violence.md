# violence

> “Then we have to change the laws.”
> “What about violence against the carbon burning itself? Would bombing a coal plant be too violent for you?”
> “We work within the law. I think that gives us a better chance of changing things.”
> 
> &#x2013; [[The Ministry for the Future]]

