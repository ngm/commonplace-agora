# Great Acceleration

> The rise in material use after 1945 reflects what scientists have called the Great Acceleration – the most aggressive and destructive period of the [[Capitalocene]]. Virtually every indicator of ecological impact has exploded as a result.
> 
> &#x2013; [[Less is More]]

