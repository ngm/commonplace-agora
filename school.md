# School

> When children attend schools that place a greater value on discipline and security than on knowledge and intellectual development, they are attending prep schools for prison.
> 
> &#x2013; [[Angela Davis]]

<!--quoteend-->

> Of course, complications arise and multiply as young children grow up. They learn that some people are not trustworthy and that others don’t reciprocate acts of kindness. Children learn to internalize social norms and ethical expectations, especially from societal institutions. As they mature, children associate schooling with economic success, learn to package personal reputation into a marketable brand, and find satisfaction in buying and selling.
> 
> &#x2013; [[Free, Fair and Alive]]

