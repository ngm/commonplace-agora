# Effecting change

> One-eight-billionth wasn’t a very big fraction, but then again there were poisons that worked in the parts-per-billion range, so it wasn’t entirely unprecedented for such a small agent to change things.
> 
> &#x2013; [[The Ministry for the Future]]

