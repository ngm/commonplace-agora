# Exit to community

> Giving startup businesses an alternative to selling out to private investors or nasty big companies ; they can users or a local community to purchase them.
> 
> &#x2013; David Bollier, Stir to Action Issue 30

-   [[Open Collective]]: [Early musings on "Exit to Community" for Open Collective](https://blog.opencollective.com/exit-to-community/)

