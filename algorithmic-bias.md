# Algorithmic bias

> Algorithmic bias reproduces pre-existing tendencies held by the companies or individuals that produce them.
> 
> That is why they sometimes reinforce racial, gender, anti-LGBTQ biases or focus solely on generating profits.
> 
> https://digitalcapitalismcourse.tni.org/mod/quiz/review.php?attempt=1096&cmid=8

One big problem being that we have a tendency to trust the decision made by a computer.  But we have to really aware of the biases in these systems.  Part of this bias is part of the bigger problem endemic in the tech industry - that's it's overrepresented by white men who have a very limited world view and a particular set of biases.  The system is often going to be made in the image of its creator.  


## Facial recognition

&#x2013; [How white engineers built racist code – and why it's dangerous for black people](https://www.theguardian.com/technology/2017/dec/04/racist-facial-recognition-white-coders-black-people-police)

> Recent studies by M.I.T. and the National Institute of Standards and Technology, or NIST, have found that while the technology works relatively well on white men, the results are less accurate for other demographics, in part because of a lack of diversity in the images used to develop the underlying databases.
> 
> &#x2013; [[Wrongfully Accused by an Algorithm]] 


## [[Search engines]]

&#x2013; [Engine Failure: Safiya Umoja Noble and Sarah T. Roberts on the Problems of Platform Capitalism](https://logicmag.io/justice/safiya-umoja-noble-and-sarah-t-roberts/)


## Translation

-   https://faculty.washington.edu/ebender/papers/Stochastic_Parrots.pdf
-   https://twitter.com/solarpunk_girl/status/1373245935241854976


## Cultural references

-   [[imago machines]]
-   [[From Apple to Anomaly]]

