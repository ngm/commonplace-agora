# Rule-based algorithms

Same as an expert system, right?

Deterministic.  Reproduces the decisions of a real person.

> their instructions constructed by a human and are direct and unambiguous.
> 
> &#x2013; [[Hello World]] 

