# Algorithmic feed

> The algorithmic feed was born out of the necessity of de-homogenizing what people saw, in an attemp to surface only things both relevant and interesting to each individual. But because on any given platform everyone gets the same algorithm, we now have the second order phenomenon where everyone sees whatever material is best at gaming the current iteration of the algorithm, and "youtuber voice".  
> 
> &#x2013; [[Rewilding the web]]

