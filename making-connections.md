# Making connections

> This is how it should be done. Lodge yourself on a stratum, experiment with the opportunities it offers find an advantageous place on it, find potential movements of deterritorialization, possible lines of flight, experience them, produce flow conjunctions here and there, try out continua of intensities segment by segment, have a small plot of new land at all times.
> 
> &#x2013; [[Deleuze &amp; Guattari]], [[A Thousand Plateaus]]

