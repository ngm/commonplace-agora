# Enclosure

> Enclosure is thus a profound act of dispossession and cultural disruption that forces people into both market dependency and market frames of thought.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The process of enclosure is generally driven by investors and corporations, often in collusion with the nation-state, to commodify shared land, water, forests, genes, creative works, and much else.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Enclosure is the opposite of commoning in that it separates what commoning otherwise connects — people and land, you and me, present and future generations, technical infrastructures and their governance, conservation areas and the people who have stewarded them for generations.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Enclosure means literally enclosing a field with a fence or a hedge to prevent others using it. This seemingly innocuous act triggered a revolution in land holding that dispossessed many, enriched a few but helped make the agricultural and industrial revolutions possible. It saw the dominance of private property as the model of ownership, as against the collective rights of previous generations.
> 
> &#x2013; [[In Our Time, The Enclosures of the 18th Century]]

<!--quoteend-->

> We know that enclosure enables monopolists to raise prices and maximize their profits (consider the rental market, the U.S. healthcare system, or the British rail system)
> 
> &#x2013; [[Universal basic services: the power of decommodifying survival]]

<!--quoteend-->

> When essential goods are privatized and expensive, people need more income than they would otherwise require to access them. To get it they are compelled to increase their labour in capitalist markets, working to produce new things that may not be needed (with increased energy use, resource use, and ecological pressure) simply to access things that clearly are needed, and which are quite often already there.
> 
> &#x2013; [[Universal basic services: the power of decommodifying survival]]

