# 2022 Week 15 Summary

-   A good chunk of my notes were [[climate change]] related.
-   [[The People vs Climate Change]], [[The Insect Crisis]], [[People like wind power]].
-   I'm starting adding notes from day-job related readings to my digital garden.  e.g. [[The era of fixing your own phone has nearly arrived]].
-   It's interesting to see some developments around [[manufacturers and the right to repair]].  Recent announcements from Samsung and Google on spare parts.
-   There is mention of [[repair and reuse in the IPCC Sixth Assessment Report]] part 3.  I feel happy that my day-job is in an area that is part of the picture of mitigation of climate breakdown.
-   Despite what I'd read in previous summaries, there is more to the report than just "renewable energy, electric vehicles, low-carbon technology."  Reducing consumer demand and changing industry practices is included for example.
-   I liked learning about [[energy storage]].  It seems key to a green future.
-   I liked learning about [[Climate Action Plan for Councils]].  I feel a local, municipal approach holds great promise for effecting change.  It obviously needs to join to a national and global effort too.
-   E's birthday was successfully orchestrated.  We had a lovely weekend away in [[Ambleside]].
-   I'm still reading [[Cybernetic Revolutionaries]].  Need to type up some notes - maybe today.

