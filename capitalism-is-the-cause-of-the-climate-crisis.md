# Capitalism is the cause of the climate crisis

[[Capitalism]] is the cause of the [[climate crisis]].


## Because

See [[Capitalism is the root cause of the overshoot of planetary boundaries]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

I mean yes I'm almost certain of this.  I'd just like to explore some counterpoints.  I guess not counterpoints as such - just more, what might some other reasons be.

