# ImageNet

A project of 14 million photos used as a training set for machine learning.  Those 14 million photos have been categorised by people on Mechanical Turk.

Suffers from [[algorithmic bias]], as highlighted in [[From Apple to Anomaly]].

