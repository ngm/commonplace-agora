# Labour markets must be socialized

> There are basically two strategies for [[de-marketing labor]], and they work best together.
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> The first is the aggressive encouragement of [[worker co-ops]], including buying out (not bailing out) failing firms and leasing them to workers, giving workers the right to buy out their shops as an alternative to closure, and providing public financial and technological support for start-up co-ops. **The more workers can become owners, the less they’ll have to work for wages, thus shrinking the labor market.**
> 
> &#x2013; [[Markets in the Next System]]

<!--quoteend-->

> The second strategy is to provide the option to exit the job market by filling out the [[welfare state]]: public health care, education, and “last resort” guaranteed employment, capped off with a [[basic income]] to subsidize culture- and community-production. The ability to survive without submitting to the dictates of the job market would incapacitate the capitalist imperative to compete with everybody else.
> 
> &#x2013; [[Markets in the Next System]]

