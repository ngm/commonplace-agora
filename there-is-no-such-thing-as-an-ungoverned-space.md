# There is no such thing as an ungoverned space

There is no such thing as an [[ungoverned space]].

[[Where there are people, there is governance]]. It just may not be in a form that appeals to the sensibilities of Western liberals.

I heard this from Adam Day in [[Adam Day, States of Disorder, Ecosystems of Governance]].

