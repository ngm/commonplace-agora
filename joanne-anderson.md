# Joanne Anderson

UK’s first directly elected black female mayor (in 2021).
Socialist.
[[Liverpool]].

-   [Liverpool chooses UK’s first directly elected black female mayor | Elections &#x2026;](https://www.theguardian.com/politics/2021/may/07/liverpool-chooses-uks-first-directly-elected-black-female-mayor)

