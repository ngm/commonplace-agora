# Rewilding

> conservation efforts aimed at restoring and protecting natural processes and wilderness areas. 
> 
> &#x2013; [Rewilding (conservation biology) - Wikipedia](https://en.wikipedia.org/wiki/Rewilding_(conservation_biology)) 

<!--quoteend-->

> Rewilding means not only allowing natural forests and grasslands of native species to replace pasture but also returning wild animals to these ecosystems
> 
> [[Half-Earth Socialism]]

