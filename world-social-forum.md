# World Social Forum

> a self-conscious effort to develop an alternative future through the championing of counter-hegemonic globalization
> 
> &#x2013;  [World Social Forum - Wikipedia](https://en.wikipedia.org/wiki/World_Social_Forum)

