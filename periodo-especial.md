# Período Especial

> In [[1990]] the [[Soviet Union]] stopped subsidizing petroleum imports to its socialist allies, and with little hard currency to buy it on the world market, [[Cuba]] had to decarbonize almost overnight.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> At the time, Cuba’s model of industrial cash-crop production left it more reliant on fossil-fuel inputs than US agriculture.  Getting by without petroleum or petroleum-based products (e.g., fertilizers and pesticides) forced the largest and most compressed experiment in organic and urban gardening in history.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Soon, there were 26,000 [[urban garden]]s in Havana alone, allowing the city to satisfy its own requirements for fresh vegetables.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> The government bought more than a million bicycles from [[China]] to replace the idling buses and cars.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Eating less meat and more vegetables, combined with pedalling or walking to work, led to improved health in the general population.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Despite an economic contraction and the tightening of the US embargo, [[universal health care]] and education were maintained and many indices even improved.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Cubans cultivated less land more intensively, returning about a third of farmland to wilderness.  This has helped Cuba maintain its incredible biodiversity (it is listed among Wilson’s top thirty biomes) and led the World Wild Fund for Nature to recognize it as the world’s only ‘sustainable’ country.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Cuba suffers less from common environmental problems such as invasive species, ‘colony collapse disorder’, and plastic pollution.
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Cuba’s transition to an ecological society has been difficult, to say the least, but if this poor, isolated island could refashion itself during a severe economic crisis into a novel form of [[eco-socialism]], then no rich country has an excuse for inaction. 
> 
> &#x2013; [[Half-Earth Socialism]]

