# End of the World, End of the Month - One Fight

URL
: https://www.resurgence.org/magazine/article5996-end-of-the-world-end-of-the-month-one-fight.html

> The movement on living standards can and must be linked to demands for energy-saving measures such as home insulation. The words '[[climate justice is social justice]]' can and must become actions.

<!--quoteend-->

> Gas, oil and coal prices started rising on world markets last year as economies recovered from the pandemic. Russia’s invasion of Ukraine pushed them further and caused a global food price shock.

<!--quoteend-->

> “End of the world, end of the month, same fight!” Jubilee Climate and other campaign groups are now mobilising around that principle.

<!--quoteend-->

> If we don’t confront the crisis of energy bills with a ‘same fight!’ approach, then the populist right will step in, attacking the government’s ‘net zero’ strategy with the false claim that decarbonisation will damage family incomes.

<!--quoteend-->

> Organising to protect families who cannot pay their fuel bills, as [[Fuel Poverty Action]] and the local groups it supports do.

<!--quoteend-->

> Linking climate protest with trade union action for a just transition.

<!--quoteend-->

> Interacting with local councils that have declared a climate emergency but failed to act on it.

