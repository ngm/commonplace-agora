# Exchange-value

> [[Use-value]]s only matter to capitalists insofar as they know they must produce something that can be sold, and useless things can’t be sold
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Put another way: the use-value of grain and whether there are people that need to eat is not the system’s main concern, rather its exchange-value is what matters—whether and for how much grain can be exchanged. And so capitalism is responsible for such grotesque events as dumping grain reserves into the ocean rather than selling at a loss in order to feed hungry people
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The question of how exchange-value is determined is, therefore, critical to understanding what makes capitalism tick
> 
> &#x2013; [[A People's Guide to Capitalism]]

