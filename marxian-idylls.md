# Marxian idylls

> Marx’s own hunting and fishing and criticising version is from The German Ideology (1845–6). For a late nineteenth-century elaboration of the idyll into a full utopia, see William Morris, [[News from Nowhere]]; for late twentieth-century Marxian idylls, try Ken Macleod’s [[The Cassini Division]] (London: Legend, 1998), and any of Iain M. Banks’s ‘Culture’ novels, especially [[Look to Windward]] (London: Orbit, 2000
> 
> &#x2013; [[Red Plenty]]

