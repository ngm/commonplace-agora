# social tipping point

> “The only way we can get anywhere near our global targets on carbon emissions and biodiversity is through positive tipping points.
> 
> &#x2013; [Identify A-ha moments to trigger fast climate action, say UK scientists | Cli&#x2026;](https://www.theguardian.com/environment/2022/feb/09/identify-a-ha-moments-fast-climate-action-tipping-points)

This feels like the weak end of the spectrum on  how to bring about transition.

> how tipping points can be triggered, for example through communication as shown by the change in attitudes to single-use plastic after the television show Blue Planet II.
> 
> &#x2013; [Identify A-ha moments to trigger fast climate action, say UK scientists | Cli&#x2026;](https://www.theguardian.com/environment/2022/feb/09/identify-a-ha-moments-fast-climate-action-tipping-points)

