# Reclaiming the stacks: August 2023 roundup

Welcome to the latest monthly roundup of my explorations at the intersection of ecosocialism and ICT.   I look at the problems of [[digital capitalism]] and positive actions that embody [[digital ecosocialism]], along with a look at the systemic side of things and how we can transition from (digital) capitalism to (digital) ecosocialism.

It all comes from the angle of [[reclaiming the stacks]] - expropriating information and communications technology from [[Big Tech]] and returning it to the people.


## Systems

"[[It's not about your footprint, it's about your point of leverage]]" says Simon Sharpe.  It's been clear for a while that [[carbon footprint]] isn't a good frame for how individuals can contribute to a climate transition.  In his article Sharpe reiterates that idea, and suggests that its better to think about one's [[leverage points]] instead.

Sharpe talks about leverage at a few different levels : the individual, the organisational, and the national, and, in another article, discusses leverage points at the sectoral level:

> In any individual sector, we can identify leverage points: actions that are relatively low cost or low difficulty but that have a high impact in accelerating the transition.
> 
> &#x2013; [[Super-leverage points]]

He also talks about **super**-leverage points:

> In the whole system, we can identify super-leverage points: we define these as actions that are high leverage in the sectors where they are taken, and that influence transitions in other sectors in a way that is positive in direction, high in impact, and reasonably high in probability.
> 
> &#x2013; [[Super-leverage points]]

Leverage points are a key part of [[systems thinking]].

> how do we change the structure of systems to produce more of what we want and less of that which is undesirable?
> 
> &#x2013; [[Thinking in Systems]]

What are the leverage points in ICT?  What are the super-leverage points?
How do we change the structure of ICT to produce more of what we want and less of that which is undesirable?

From an ecosocialist standpoint, we want more digital ecosocialism and less digital capitalism. We want to identify actions in ICT which can have a positive, impactful, and highly probable influence on agency, social equity and planetary stability.  I started to review work on this in [[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]].   Some sources of inspiration:

-   [[Digital Ecosocialism: Breaking the power of Big Tech]]
-   [[Platform Socialism]]
-   [[Internet for the People]]
-   [[Governable Stacks against Digital Colonialism]]
-   [[Digitalization and the Anthropocene]]
-   [[Leveraging Digital Disruptions for a Climate-Safe and Equitable World: The D<sup>2S</sup> Agenda]]

Together, these contain quite a number of actions and initiatives and possible leverage points[^fn:1].  There's a few common themes that stuck out - things like [[deprivatisation]]/(re)[[socialisation]] of infrastructure and platforms; [[knowledge commons]], [[tech cooperatives]] and [[public-commons partnerships]]; libre and open software, data and access.  And the notion of [[resist, regulate, recode]] as a triumvarate of types of leverage.  But it's not clear which of these, if any, are key leverage points, or even super-leverage points.  A systemic analysis of how they all connect together could help.

I recently came across "[[A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]]", which has an interesting methodology for surfacing leverage points within a sector.  It would be interesting to see how the same could be applied to ICT.  It all starts off with a [[qualititative system dynamics model]], so that will be where to look next.  The references above each contain their own informal, qualitative system dynamics model of ICT, so a starting point will be to look through them again with that lens.


## Problems

_Some problems from digital capitalism recently in the news.  To help map them out, I'm tagging them with some of the criteria I [[defined]] in my OU research._

-   [[ChatGPT Will Command More Than 30,000 Nvidia GPUs: Report]]. AI is driving the manufacture of a shit-ton of hardware.  And the negative environmental impact associated with that.  (A problem of [[AI]], [[growthism]] and [[overconsumption]], counter to [[planetary stability]], at the hardware layer of the stack).
-   [[The Generative AI Race Has a Dirty Secret]].  Not really a secret - LLMs use a vast amount of energy.  And this is set to increase as they get integrated into search engines.  (A problem of [[AI]], [[data centres]] and [[unnecessary technology]], counter to [[planetary stability]], at the software layer of the stack).
-   [[Meta report shows the company causes far more emissions than it can cover with renewable energy]]. Similarly to Microsoft ([[Microsoft's dirty supply chain is holding back its climate ambitions]]), Meta has huge emissions, most of which are scope 3 emissions from its ecosystem and supply chain, and apparently it doesn't know what to do about them.  Here's a couple of ideas: (a) consider degrowth; (b) use your considerable clout on your supply chain, rather than pretending that you are helpless to do anything about it.  (A problem of [[carbon emissions]], [[supply chains]] and [[scope 3 emissions]], counter to [[planetary stability]], at the software and hardware layers of the stack).


## Actions

> Philosophers have only interpreted the world, in various ways; the point, however, is to change it.
> 
> &#x2013; Karl Marx

_Some latest news on concrete actions that are part of an ecosocialist ICT movement._

-   [[Extreme heat prompts first-ever Amazon delivery driver strike]].  (A [[strike action]], supporting [[social equity]] and [[agency]], to resist [[worker exploitation]]).
-   [[Big win for right to repair with new EU rules for batteries - but legislators must get the implementation right]]. Good right to repair news - progress on battery removability and availability.  Though still lacking on the affordability of repair.  ([[Regulation]] on the [[right to repair]], supporting [[planetary stability]] and agency, to resist [[growthism]] and [[overconsumption]]).


## Inputs

_Finally, a few other things I've been reading, listening to, and watching that are adjacent to the topics of ecosocialism and ICT._


### Reading

-   [[How an eccentric English tech guru helped guide Allende’s socialist Chile]] (h/t [[Panda]])
    -   A short article on Chile, [[Project Cybersyn]] and [[Stafford Beer]], apropos [[Evgeny Morozov]]'s new podcast series on the topic, [[The Santiago Boys]].

-   [[ICT: A top horizontal priority in sustainable product policy]]
    -   A report on the need for horizontal measures for the regulation of various hardware products in the ICT sector to make them last longer.

-   [['A certain danger lurks there': how the inventor of the first chatbot turned against AI]]
    -   _"Perhaps his most fundamental heresy was the belief that the computer revolution, which Weizenbaum not only lived through but centrally participated in, was actually a counter-revolution. It strengthened repressive power structures instead of upending them."_


### Listening

-   [[Class Politics in a Warming World with Keir Milburn]]
    -   I like [[Keir Milburn]].    This interview is about the [[intersection of class politics and environmental politics]], and ranges across lots of interesting topics related to that, including Milburn's personal history in both of those movements.
-   [[Microdose: Californian Capitalism]].
    -   Interview with Malcolm Harris on his book [[Palo Alto: A History of California, Capitalism, and the World]].  Includes some interesting takes on [[The Californian Ideology]] and the left and technology.


## Until next time

That’s it! See you next month. Until then, you can find latest streams of thoughts over at my [website](https://doubleloop.net).


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Which I've partially documented in a ramshackle way in my digital garden at [[ways to reclaim the stacks]], and am currently trying to put a bit more structure around in Anytype.
