# Nhandeflix

[[community networks]].

> indigenous-led streaming platform
> 
> &#x2013; [[Decentralized and rooted in care: envisioning the digital infrastructures of the future]]

<!--quoteend-->

> an Indigenous-centred content platform that works on their own intranet.
> 
> &#x2013; [Seeding change: How Indigenous villages in Brazil built Nhandeflix, their own&#x2026;](https://www.apc.org/en/blog/seeding-change-how-indigenous-villages-brazil-built-nhandeflix-their-own-streaming-platform)

