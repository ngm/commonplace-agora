# Bonfire



## Signing up for the Bonfire test instance

-   I was a bit confused when I went to the sign up screen and didn't see any sign up boxes at first.  Maybe say 'Before signing up, please read our code of conduct'.
-   It looks great but its huge.  I'm busy, I just want to sign up.  I didn't read it all so would like to come back to it.
-   why do I need an email address?
    -   it's for account verification.
-   It seems quite slow.

