# Feedback

> If stocks and flows are a system’s core elements, then feedback loops are their interconnections, and in every system there are two kinds: reinforcing (or ‘positive’) feedback loops and balancing (or ‘negative’) ones. With reinforcing feedback loops, the more you have, the more you get. They amplify what is happening, creating vicious or virtuous circles that will, if unchecked, lead either to explosive growth or to collapse. Chickens lay eggs, which hatch into chickens, and so the poultry population grows and grows. Likewise, in the vengeful tit-for-tat of playground fights, a single rough shove can soon escalate into a full-blown bust-up. Interest earned on savings adds to those savings, increasing future interest payments, and so wealth accumulates. But reinforcing feedback can lead to collapse too: the less you have, the less you get. If people lose confidence in their bank and withdraw their savings, for example, it will start to run out of cash, deepening the loss of confidence and leading to a run on the bank
> 
> &#x2013; [[Doughnut Economics]]

<!--quoteend-->

> If reinforcing feedbacks are what make a system move, then balancing feedbacks are what stop it from exploding or imploding. They counter and offset what is happening, and so tend to regulate systems. Our bodies use balancing feedbacks to maintain a healthy temperature: get too hot and your skin will start sweating in order to cool you down; get too cold and your body will start shivering in an attempt to warm itself up. A household’s thermostat works in a similar way to stabilise room temperature. And in a playground scuffle, someone is likely to step in and try to break it up. In effect, balancing feedbacks bring stability to a system
> 
> &#x2013; [[Doughnut Economics]]

<!--quoteend-->

> Complexity emerges from the way that reinforcing and balancing feedback loops interact with one another: out of their dance emerges the system’s behaviour as a whole, and it can often be unpredictable
> 
> &#x2013; [[Doughnut Economics]]

<!--quoteend-->

> This can be used creatively. At a historic performance in 1969, legendary guitarist [[Jimi Hendrix]] gave a great example of the artistic use of feedback. It’s a mixture of science, art - and social commentary. The tune he plays is the Star-Spangled Banner, commenting on the Vietnam war, its blowback into US society, the feedback loops between different registers of violence and alienation.
> 
> &#x2013; [[How systems theory can help us reflect on the world]]

harnessed feedback

> A great applications of systems thinking is the work of [[James Lovelock]], and here too, feedback is a central theme. For example, global cooling could trigger the expansion of polar ice-caps, making the earth whiter and hence reflecting more heat and making it colder still. But the earth-system, like Hendrix, can play creatively with its feedback, maintaining some equilibrium and regulation. This is what we risk losing today, if there’s a tipping-point leading to global warming feedback.
> 
> &#x2013; [[How systems theory can help us reflect on the world]]

<!--quoteend-->

> [[Accumulation]] is in essence a feedback loop: grabbing portions of nature (land, resources) permits a minority to accumulate wealth, which in turn becomes an entitlement to accumulate more wealth.

<!--quoteend-->

> And from a social angle, [[laissez-faire]] typifies a dangerous manifestation of the principle of feedback. Wealth is an entitlement to accumulate more wealth, leading ultimately to its concentration in fewer and fewer hands.
> 
> &#x2013; [[What is wrong with a system of laissez-faire economics?]]

