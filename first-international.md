# First International

> the attempt in the mid-1860s to bring together a range of leftwing groups across the world
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> fell apart because of a conflict between those who followed [[Karl Marx]] and argued that taking over government and seizing state power are central to building communism and the anarchists who sided with [[Mikhail Bakunin]] and his belief that revolution could only come about by building the desired future society in the present.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> It was precisely a disagreement over forming political parties (and the associated strategy of seizing state power) that caused the split in the First International between the Marxists and the anarchists.
> 
> &#x2013; [[Anarchist Cybernetics]]

