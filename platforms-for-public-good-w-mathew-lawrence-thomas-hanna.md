# Platforms for Public Good w/ Mathew Lawrence & Thomas Hanna

A
: [[podcast]]

Part of
: [[Tech Won't Save Us]]

Featuring
: [[Mathew Lawrence]] / [[Thomas Hanna]]

Discussing the topics in [[A Common Platform: Reimagining Data and Platforms]].

The problems with the Big Tech platforms and the need for platforms for the public good. (See also [[Internet for the People]]&#x2026;)

Interesting idea that venture capitalism is a form of planning. The VCs are the exceedingly undemocratic planners, they get to say what firms live and die. (E.g. Uber never turning a profit yet being propped up by VC regardless)

[[Anti-trust]] and its shortcomings.

[[Digital community wealth building]]

At the end, they specifically frame it as the need to reclaim the platforms from Big Tech.
Thomas Hanna says something about for socialism to succeed there the need to reclaim whichever organisations are currently at the commanding heights of capitalism. And that's currently the big tech firms.

