# The Jakarta Method Comes to Latin America (Review)

Found at
: https://nacla.org/news/2020/05/27/jakarta-method-review

About the book by [[Vincent Bevins]]

The Jakarta Method.

CIA involvement in brutally violent suppression of communist societies.

> Bevins shows that the [[Indonesian Communist Party]] resisted calls from [[Mao]] and others to arm and prepare for insurrectionary resistance. The mass murder in Indonesia not only annihilated the communist movement in the country—it served as a warning to other leftist movements about the dangers of remaining unarmed

