# Build the new web in the shell of the old

See: [[Build the new world in the shell of the old]].

People currently say the [[IndieWeb]] is too difficult to use for most people right now - I don't disagree.  But it's a lot more nuanced than people make out.

To build a site with all the peer-to-peer interaction bells and whistles, is not straightforward.  But you can use [[micro.blog]], i.haza.website, etc, and this is all taken care of for you.  With a bit of extra work you can install some plugins on WordPress and have it all taken care of for you.

Regardless, to be part of the indie web you don't need all the advanced bells and whistles of micropub, webmentions, social readers, etc.  Not from day one (and not ever, if you prefer).  You just need your own space at your own address.

But anyway.  The point I wanted to capture here is that you've got to start somewhere.  We have to be working on alternatives, even if they're not 100% finished yet.  You need something ready to go for the next time Facebook does a Cambridge Analytica and people are looking for somewhere to go to.

> Designing a dream city is easy; rebuilding a living one takes imagination.
> 
> &#x2013; [[Jane Jacobs]]

I think that part of the rebuilding involves [[bridging all the things]] while you break away.

> Phreaks spotted the weakness in their dominant technological infrastructure; within those, they carved out communities, shared knowledge, and indulged in a larger spirit of exploration. We are far less able to conduct ourselves this way today because we’re penned into flat user interfaces and kept distracted while our behaviors are farmed for profit. It’s nearly impossible to develop our own sense of place because we’re not stakeholders, we’re products.
> 
> &#x2013; [[The Prototype for Clubhouse Is 40 Years Old, and It Was Built by Phone Hackers]]

In [[my social media usage]], I try to interact with people where they are, but avoid feeding the beasts beyond that.

Furthermore, [[I want a community, not an audience]].

