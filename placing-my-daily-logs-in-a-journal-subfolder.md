# Placing my daily logs in a journal subfolder

Very handy advice on how to do it here: [Keeping Org-roam daily notes in a separate folder - Jack Baty's Weblog](https://www.baty.net/2020/keeping-org-roam-daily-notes-in-a-separate-folder/)

Thread of org-roam discourse here: [Putting Daily Notes in a folder - Troubleshooting - Org-roam](https://org-roam.discourse.group/t/putting-daily-notes-in-a-folder/210) 

