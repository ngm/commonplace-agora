# Neil's Digital Garden



## [[Hello]]

Welcome!  You have stumbled upon my [[hyper commonplace garden wiki]] AKA [[digital garden]].

It is someplace between a notebook and a blog and a wiki.

![[2020-03-19_23-27-44_iScape.png]]

You are most welcome here.  It is something of a [[maze]] of technology, politics, nature and culture.  I hope you find something that you like!  


## Start here

As of Friday, March  7, 2025
, I have `6438` nodes in my garden.

Here are some entry points to my world:

-   look at the [[recent changes]] (automatically updated)
-   read my [[journal]] or my [stream](https://doubleloop.net) (manual log of things, up-to-date-ish - [[What's the difference between my journal and my stream?]])
-   see what's top o'mind - [[nowtions]].  Broader themes I'm thinking about. (textual, manual updates, occassionally up-to-date)
-   [[well-connected]] pages
-   see things that I agree with: [[Yes definitely]], [[Without a doubt]]
-   see things that [[I like]] or that [[I love]]
-   try this  [[interactive journey]]  (out of date)
-   engage the [[Captain's Log]] (this is the most fun, visual, and hopelessly out of date)
-   navigate [[the map]] (messy, out-of-date, visual)

Please feel free to click around here and explore.  Don't expect too much in the way coherence or permanence&#x2026; it is a lot of half-baked ideas, badly organised.  The very purpose is for snippets to percolate and  morph and evolve over time, and it's possible (quite likely) that pages will move around.

That said, I make it public in the interest of info-sharing, and occassionally it is quite useful to have a public place to refer someone to an idea-in-progress of mine.

Here's a graphical representation of the notes and connections in my garden.  It  doesn't mean a whole lot, but it looks nice.

![[images/my-garden.png]]

Some more info on the [[whats and the whys]].

