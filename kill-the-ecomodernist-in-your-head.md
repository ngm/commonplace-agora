# Kill the Ecomodernist in Your Head

A
: [[podcast]]

URL
: https://soundcloud.com/thismachinekillspod/unlocked-kill-the-ecomodernist-in-your-head

Series
: [[This Machine Kills]]

Good chat around the problems with [[ecomodernism]] as a philosophy.  They're generally pro-[[degrowth]].  And pro-[[Luddism]].

