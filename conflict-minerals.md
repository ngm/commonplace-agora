# conflict minerals

> Gold and tin are classified as “conflict minerals” by US legislation, a term that refers to resources originating from Congo and its neighboring countries.
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

<!--quoteend-->

> Working conditions at such mines are often pitiful, sometimes involving violence and child labor, and all for mercilessly little pay.
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

