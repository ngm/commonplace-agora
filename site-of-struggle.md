# site of struggle

> A discursive domain (e.g. class, gender, religion, or ethnicity) in which dominant discourses compete for ideological hegemony in an endless quest to fix meaning. This conception derives from Gramsci's notion of ideology as a site of struggle.
> 
> &#x2013; [Site of struggle - Oxford Reference](https://www.oxfordreference.com/view/10.1093/oi/authority.20110803100509105)

