# Revolutionary socialism

I would class myself as a revolutionary socialist.  

Just gonna quote Wikipedia extensively here:

> Revolutionary socialism is the [[socialist]] doctrine that [[social revolution is necessary in order to bring about structural changes to society]]. More specifically, it is the view that [[revolution is a necessary precondition for a transition from capitalism to socialism]].

Important to point out revolution doesn't explicitly mean armed uprising.  Unclear where I stand on the exact nature of the revolution. Not a fan of violent insurrection, but some do make strong argument that without it, your revolution is doomed.  So.  There's that.

> Revolution is not necessarily defined as a violent insurrection; it is defined as seizure of political power by mass movements of the working class so that the state is directly controlled or abolished by the working class as opposed to the capitalist class and its interests.

All good so far.

> Revolutionary socialists believe such a state of affairs is a precondition for establishing socialism and orthodox Marxists believe that it is inevitable but not predetermined.

Fair.

> Revolutionary socialism encompasses multiple political and social movements that may define "revolution" differently from one another. These include movements based on orthodox Marxist theory, such as De Leonism, impossibilism, and Luxemburgism; as well as movements based on Leninism and the theory of vanguardist-led revolution, such as Maoism, Marxism–Leninism, and Trotskyism. Revolutionary socialism also includes non-Marxist movements, such as those found in anarchism, revolutionary syndicalism, and democratic socialism.

Yeah, so this is where I'm not 100% sure of my stripes.  I see plenty of problems with vanguardism, and tend to lean towards anarchism, but really not well versed enough in all the theory to pick a tendency and wave my flag for it.  Happy to be somewhat cross-cutting, too, if that's where I sit.

See also: [[logics of resistance]].

> It is used in contrast to the reformism of social democracy and other evolutionary approaches to socialism. Revolutionary socialism is opposed to social movements that seek to gradually ameliorate the economic and social problems of capitalism through political reform. 

<!--quoteend-->

> If revolutionary socialism proposes that state power should be seized so that capitalism can be smashed, and social democracy argues that the capitalist state should be used to tame capitalism, anarchists have generally argued that the state should be avoided — perhaps even ignored — because in the end it can only serve as a machine of domination, not liberation.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

I'm confused - suggests that revolutionary socialism is explicitly about seizing and smashing.  Maybe Wright isn't necessarily equating 'smashing' with violent insurrection.


## Context

Some snippets from an ep of Rev Left [podcast](https://revolutionaryleftradio.libsyn.com/humanism):

-   Some societies, like the Russian revolution, had identifiable targets of power that could be overthrown.  You knew who they were, where there buildings were.  You can take a more vanguard approach and a small group overthrow these targets.  This is what the Bolsheviks did.

-   In other places and times, that's not so clear cut.  Maybe then you need to movement build and raise class consciousness for a popular uprising.


## Struggle

> The powerful few may kill one, two, or three roses, but they can never stop the spring from coming. And our struggle is for the spring.
> 
> &#x2013; [Lula Livre](https://tribunemag.co.uk/2019/11/lula-livre/), Tribune

”


## Misc

> the holy trinity of socialism: [[educate, agitate, organise]]
> 
> &#x2013; [None of Us Cracked the Corbyn Moment – but, Comrades, We Can All Build On It](https://www.weareplanc.org/blog/none-of-us-cracked-the-corbyn-moment-but-comrades-we-can-all-build-on-it/) 

