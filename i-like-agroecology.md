# I like agroecology

[[I like]] [[agroecology]].


## Because

-   Agroecology helps humanity relink itself with nature
-   Agroecology rebuts the predatory logic of nature's use as a commodity and production resource
-   Agroecology is not just alternative to green capitalism, but an alternative to capitalism itself
-   Agroecology is what allows movements of struggle for land to exist
-   Any serious struggle movement must have agroecology as its mode of production

the above claims come from [[Jayu]]'s subnode: https://anagora.org/@Jayu/agroecology


## And

-   dazinism likes it (https://social.coop/@dazinism/107628616319792941)
-   There's no agroecology without community
-   There's no agroecology without food sovereignty
-   There's no agroecology without preserving nature
-   [[Agroecology is an example of systems thinking]]


## Epistemic status

Type
: [[feeling]]

Strength
: 5

Only 5 because it's new to me and I have no personal experience of it.  I like it with my mind but not with my heart yet.

Also I cannot backup the claims yet but the fact dazinism likes it gives me good gut feeling.

