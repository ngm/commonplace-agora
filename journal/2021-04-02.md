# 2021-04-02

-   Been looking at [[orger]].  Looks pretty great - a way to [[PESOS]] a ton of personal data into [[org-mode]].
-   I think I'll try and pull in all my old tweets to my [[digital garden]] with it.
    -   [[Archiving my tweets in my garden with orger]]
-   I was thinking I'd like to get my toots on [[social.coop]] pulled in to my garden too.  But ideally, [[POSSE]]ing would be better than PESOS there.  So again maybe it comes down to sorting out my stream and webmentions etc in the garden.
-   That said - maybe there's an argument for PESOS over POSSE when it comes to a digital garden?  Use the interface of the apps that were specifically designed for the platform rather than try to reverse engineer and rebuild.  Just make sure you've got the data.
    -   [[POSSE vs PESOS]]
-   Trying to upgrade [[Invidious]] on [[YunoHost]].  It is giving the error.  I am trying to fix it.  [[YunoHost Invidious upgrade gives "Not enough free space"]]
-   Set my self-hosted [[Searx]] instance as my default search engine in Firefox.
-   [[Ray Tomlinson]]
-   Read: [Breaking Tech Open: Why Social Platforms Should Work More Like Email](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/)

