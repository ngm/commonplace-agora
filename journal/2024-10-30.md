# 2024-10-30

-   I'm [[microblurting]].

-   Blurt
    -   [[Data science]] is a combination of data analytics, statistics, and machine learning.
    -   [[Data analytics]] looks at past data and explores patterns and issues.  It is descriptive.
    -   Statistics can be descriptive but also predictive?
    -   [[Machine learning]] is primarily predictive?

-   Blurt
    -   Data science is multidisciplinary, encompassing data analytics, statistics and machine learning, among other areas.

-   I'm thinking that blurting is probably better semi private.
    -   It might not be of great interest for others to read half formed, quite possibly incorrect statements about various things.
    -   Social streams are more interesting when they have some personal, subjective opinion based element I feel, rather than attempted recall of facts.
    -   Though, it would be nice to receive feedback on some things. So perhaps semi private is good.

-   [[HeliBoard]] is going well.
    -   Some nice features. Very customisable.
    -   Being able to adjust the width of the one handed keyboard is very useful.
    -   swipe typing works pretty well, though I have a sense not quite so good as gboard?

