# 2021-05-17

-   [[Flancian]] asks a good question on the social.coop tech channel - why does [[social.coop]] not use [[Mastodon]] to coordinate [[governance]]?
    -   In my opinion because Mastodon is not very good for governance.  It only has polls and even that is fairly new.
    -   In fact I don't find Mastodon very good for even just conversation.
    -   [[There is a layer of governance infrastructure missing from the web]].
    -   That said, having two/more platforms to coordinate things is definitely a huge piece of friction to involving a community in governance.
    -   h's mockups from way back when, are interesting: https://social.coop/@h/1868600 https://social.coop/@h/1870490

-   Bookmark: [[The Web of Life]]
    -   Would like to read it as it gives an intro to [[Humberto Maturana]], [[Francisco Varela]], and [[autopoesis]].

