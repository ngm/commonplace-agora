# 2024-03-29

-   Late night [[listened]] to [[What’s the Value of Data? (ft. Salomé Viljoen)]]
    -   [[Social data]].
    -   Has [[predictive value]].
    -   Three ways to extract [[surplus value]] with it:
        -   1. Just sell it on, e.g. data broker
        -   2. Use it to exploit people based on knowledge from the data
        -   3. Use it to exert power (e.g. Uber's [[Greyball]] program)

-   Also listened to [[The Supermarket into Prison Pipeline]]

-   Pretty much always find [[This Machine Kills]] interesting, whatever the topic.

-   Thinking about how I would go about [[adding planted and last tended dates to pages in my digital garden]].

