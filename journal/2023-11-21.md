# 2023-11-21

-   Today at work I:
    -   Did the usual inbox trawls and day planning.
        -   Day planning I do with org-mode, org-agenda and org-timeline.
    -   Prepped for the meetings for the day.
        -   Mostly with mindmaps.
    -   Did some strategic planning for next year.
        -   Mindmaps and freeform writing.
    -   Some rote work
        -   processing incoming applications for things, updating website accordingly
        -   always good to think with this stuff how processes could be streamlined
    -   Minor website content change.
        -   Minor change, but thinking about the UX of it is always interesting.
        -   And how it affects client agreements/expectations, too.
    -   Planning and assigning work for my team.
        -   Bit of mindmapping combined with going through Jira.
    -   Reviewing new features.
        -   Code and functionality.  Code review is in Github.
        -   Testing I tend to build the feature branch locally.
    -   Meetings.
        -   Sometimes I jot things down on mindmap.
        -   Somethings I record things straight into knowledge base.
        -   Sometimes I log things straight into org as TODOs.
        -   It's a bit haphazard to he honest. Could be improved.
    -   Emailing external partners.
        -   Always interesting the amount of work that goes into crafting an email to get across all the nuances of your position on something.
    -   Distracting myself with Slack threads not really related to what I'm doing.

-   When I'm working, I don't log a lot in the journal, I noticed.
    -   So experimenting with logging thoughts on work activities.
    -   Not much detail on specifics, more reflections on activities and process.
    -   I quite enjoy it so far. Useful to reflect.

-   Listened: [[Hotel Bar Sessions: Revolutionary Mathematics]]
    -   So far, discussing frequentism and Bayesianism schools of thought in probability.

-   [Patient privacy fears as US spy tech firm Palantir wins £330m NHS contract | &#x2026;](https://www.theguardian.com/society/2023/nov/21/patient-privacy-fears-us-spy-tech-firm-palantir-wins-nhs-contract)
    -   Absolutely gutted by this. Despite all the campaigning by Foxglove and Just Treatment, fucking [[Palantir]] still awarded the contract with the NHS.
    -   Makes me sick. This is not the kind of organisation our health service should be in partnership with.

