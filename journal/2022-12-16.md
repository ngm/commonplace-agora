# 2022-12-16

-   Read: [[New EU rules for smartphones and tablets: still far from a true Right to repair]]

-   Reading: [[Platform Socialism]]. Liking the chapter on [[guild socialism]] and [[democratic planning]], heavily featuring [[G. D. H. Cole]] and [[Otto Neurath]].  And the subsequent chapter on [[civic platforms]], featuring [[platform cooperativism]], [[new municipalism]], [[public-commons partnerships]].

-   Re-read [[Radical Technologies]] by [[Adam Greenfield]] and it remains an absolute banger.  Thorough critique of the technologies you see bandied around as part of the 'Fourth Industrial Revolution' - smartphones, IoT, AR/VR, blockchain, digital fabrication, automation, ML/AI.  Fair, but mostly damning, and still right on point 5 years since publication.  The last two chapters on the Stacks, possible futures and tactics are gold.

