# 2023-11-13

-   Enjoying the [[This Machine Kills]] podcast.
    -   All the episodes I've listened to have been excellent discussions on socialism and digital technologies so far.

-   Having another attempt at getting RSS feed publishing working for commonplace.  This time without trying to use a tempdir, caused too many problems last time.

-   Listened: [[Kill the Ecomodernist in Your Head]]

-   Listened: [[No King But Ludd (ft. Brian Merchant)]]
    -   [[Blood in the Machine]] definitely sounds worth a read.

-   org-roam on the mobile with Termux is going well. Using it regularly.

-   Going to start posting my daily journal/log in the stream as well. So it's a bit more discoverable/subscribeable.

-   Been reading through [[Doughnut Economics]] again.  Appreciating the chapter on [[systems thinking]].

-   [[Hugo Blanco]] passed away.

-   Watching [[Captain Fantastic]]. A lot of fun. Points out the problems of American (Western) society.  Is what they have in the woods any better though?

