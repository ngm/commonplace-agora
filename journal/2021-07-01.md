# 2021-07-01

-   [[Parable of the Sower]]
    -   [[God is Change]]
    -   [[Debt slavery]]

-   [[Federated Education: New Directions in Digital Collaboration]]

| writing    | subject | tools  | dialectics |
|------------|---------|--------|------------|
| narrative  | I       | garden | thesis     |
| dialogic   | you     | stream | antithesis |
| exposition | we/it   | agora  | synthesis  |

-   

> Another way is to start at the end technology — in this case wiki — and look at what it would take to make it work better in the other stages, the I and the You, the personal and the dialogic.

^ This is huge.  And what I think I'm going for with my site.  Combine the garden and the stream there.  Actually not just there.  And then the agora (or interlinking wikis, one way or another) layered on top of that.

But YES - the goal is to combine garden, stream, and agora.  The way one does that might take many forms.  My chosen form is some kind of indieweb / fediverse / agora mashup.

-   [[dialogic]]

-   Away from reality for a few hours, I am drinking coffee, writing in my wiki, listening to Atlas by Bicep, and having galaxy brain moments.

-   [[What's the difference between my journal and my stream?]]

-   [[Chorus of voices]]

-   It could be interesting to have an action in [[Agora]] that forks (or merges) someone else's node in to my own content store.
    -   Similar to what one does in [[FedWiki]].
    -   In Agora, however, given the way nodes are all displayed together on one page, you might end up with a lot of duplicated content.

-   Similar to the above on wiki and dialogic: "Good _digital_ note taking tools easily blend into _online_ conversations"
    -   [[Bastien Guerry]]
    -   [[Future of Note Taking]]

