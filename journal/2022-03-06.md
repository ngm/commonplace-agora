# 2022-03-06

-   [[Nowtions]]:  [[synthesis of horizontalism and verticalism]]

-   In my readings on [[political organisation]] I'm noting a distinction:
    -   Those that are more theoretical, e.g.
        -   [[Anarchist Cybernetics]]
        -   [[Neither Vertical Nor Horizontal]]
    -   Those that are more practical, e.g.
        -   [[Free, Fair and Alive]]
        -   [[Jackson Rising]]
    -   It's good to have a bit of both I think.  I think the practical ones are the most inspiring, cos it's shit that's actually happening.  But the theory is good too and can give some pointers to practical action.  Some are a mix of both, e.g. FFA has plenty theory alongside examples.

-   Listened: [[Organising for Revolution With Rodrigo Nunes]]

-   [[Autonomous Design Group]] - rad posters with a Creative Commons license.

-   Added some nice [[Autonomous Design Group]] images to [[Educate, agitate, organise]].

