# 2022-04-27

-   We had a go with the [[telescope]] for the first time yesterday!  It was a lot of fun.  It turns out that distant stars are still tiny pricks of light even through a telescope, go figure, but you can see a whole lot more of them.  We looked at Vega, Arcturus, the Beehive Cluster, the Double Cluster, and we think we saw a UFO - to be confirmed.

-   [[Apple Self Service Repair]] finally arrived:     https://support.apple.com/self-service-repair

-   Read: [[Apple's Self-Repair Vision Is Here, and It's Got a Catch]]

