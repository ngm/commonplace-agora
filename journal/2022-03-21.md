# 2022-03-21

-   [[Following Twitter people via Nitter and Miniflux]]. Get 404s and 429s returned from Nitter via Miniflux fairly frequently.  I wonder if that's Nitter actually returning that, or Twitter?

