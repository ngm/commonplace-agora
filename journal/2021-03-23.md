# 2021-03-23



## <span class="timestamp-wrapper"><span class="timestamp">[2021-03-23 Tue 10:09]</span></span>

Trying out [[org-pomodoro]]. Not that keen so far to be honest&#x2026; it's often hidden for me in the modeline, and its 'bell' sound is annoying, and at a too loud volume compared to other things that are using sound.  Will probably stick with the gnome panel pomodoro app.

