# 2024-11-08

-   [[digital ecosocialism]]

-   [[Doughnut Economics]]
    -   I like the seven ways to think like a 21st century economist:
        -   Change the Goal
        -   See the Big Picture
        -   Nurture Human Nature
        -   Get Savvy with Systems
        -   Design to Distribute
        -   Create to Regenerate
        -   Be Agnostic about Growth

