# 2023-04-09

-   Reading: [[Internet for the People]].
    -   Really enjoying it.
    -   It's a great descriptor of historical process of the privatisation of what was once a public internet. As well as discussion of what (re)socialised alternatives might look like.
    -   It starts with a description of the pipes - the network infrastructure.  And outlines how community internet would work as an alternative.
    -   Now it's moving on to the platforms. Should be good.
    -   Very interesting point about the relative scales of the pipes and the platforms - Comcast worth about $260 billion, Google worth about $1.7 trillion.

-   Read: [[Envisioning real utopias from within the capitalist present]]
    -   To get an idea about [[Envisioning Real Utopias]].
        -   Sounds worth reading.

-   Listened: [[Robin Hahnel on Parecon (Part 1)]]

