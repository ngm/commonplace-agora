# 2021-09-19

-   Read: [[Exiting the Vampire Castle]]

-   I set up Agora locally and have contributed a couple of small CSS changes (for blockquotes, and headings typography) and [[Flancian]] has pulled them in and deployed them.  Feels good to contribute!

