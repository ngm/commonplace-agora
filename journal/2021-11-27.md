# 2021-11-27

-   Today for [[node club]] I am looking specifically at the [[Choose Commons-Friendly Financing]] pattern from the patterns for commoning.
    -   It's got stuff on: reducing the need for money and markets; collaborative financing and community currencies; and public-commons circuits of finance.

