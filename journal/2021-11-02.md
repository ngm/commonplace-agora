# 2021-11-02

-   How long until there's a [[GitHub Copilot]] but for your [[zettelkasten]].  Autocompleting your thoughts.

-   This week for [[node club]] I'll be noding [[Liberatory technology]].  Starting from [[Murray Bookchin]]'s take on it I think.
    -   https://anagora.org/node-club

