# 2022-02-23

-   This is great.   [Musicians Turning Trash into Musical Treasure | Bandcamp Daily](https://daily.bandcamp.com/lists/upcycling-musicians-list).  I'm loving Congotronics.  h/t James!
    -   "Using everything from old milk boxes or beat-up Volvos, here are six musicians or collectives embracing the act of upcycled instrumentation."

