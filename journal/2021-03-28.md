# 2021-03-28



## <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun 03:31]</span></span>

I compiled [[Emacs from source]].  I find it pretty fantastic that I can do that.  An absolutely amazing tool that I use daily, built by a community of people around the world, I can just download the code and build it myself.  Makes me feel positive.


## <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun 03:36]</span></span>

Would like to give [[Snikket]] a go.  Or XMPP of some kind, at least.

