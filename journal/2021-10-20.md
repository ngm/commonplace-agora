# 2021-10-20

-   [[Inner garden]]
    -   Like Bill Seitz describes here - http://webseitz.fluxent.com/wiki/TendingYourInnerAndOuterDigitalGardens - I have a completely private 'inner garden' that is for personal stuff.  It never gets published to my public digital garden.

-   [[Journal pages]]

-   [[Fleeting notes]]

-   I've got a problem with my [[org-publish]].  The export of tracks.org seems to be super slow and in fact causes the gitlab runner to kill the process.  tracks.org is a huge unordered list.  For now I've just excluded it from the publish in the :export option.  I guess I'll try and recreate with a minimal setup and then get help from the org list if it persists.

