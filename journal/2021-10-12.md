# 2021-10-12

-   We just watched [[Sisters With Transistors]] and it was brilliant.
    -   It's a documentary on some of the early electronic music pioneers - I knew [[Delia Derbyshire]] and [[Wendy Carlos]] already but got introduced to some amazing others.
    -   For example [[Daphne Oram]], [[Suzanne Ciani]] and [[Laura Spiegel]].

-   @bhaugen@social.coop's post (https://social.coop/@bhaugen/107088836725318619) made me think of the description of moving from network to [[community of practice]] in Margaret Wheatley and Deborah Frieze's paper - https://www.margaretwheatley.com/articles/

