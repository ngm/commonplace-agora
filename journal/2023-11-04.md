# 2023-11-04

-   [[Boox handwriting recognition missing whole words out]]

-   [[Boox Firmware v3.5]]

-   Starting having a go at [[an Iterative Enquiry diagram for digital ecosocialism]].  Definite work in progress.

-   Listened: [[Anthony Hodgson, "Ready for Anything: Designing Resilience for a Transforming World"]]

