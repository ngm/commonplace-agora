# 2024-11-07

-   Not sure how to process the US election 2024 results.
    -   It feels pretty devastating. The short term and long term repercussions seem catastrophic.
    -   Right now, avoiding all the news and analysis - too much to get lost in and I don't have the headspace for it at the moment.
    -   Finding ways to offer practical solidarity to threatened and affected groups in the US seems like the most productive action in the short term.

