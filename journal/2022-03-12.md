# 2022-03-12

-   [[Gabriel Boric]].  This feels like good news today. h/t [[Flancian]]
    -   [Gabriel Boric, 36, sworn in as president to herald new era for Chile | Chile &#x2026;](https://www.theguardian.com/world/2022/mar/11/gabriel-boric-chile-president-new-era)

-   Bit of noding of [[dual power]] for [[node club]].

