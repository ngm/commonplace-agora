# 2022-07-21

-   What actually is a [[system]]?
    -   [[Thinking in Systems]].

-   [[Zen and the Art of Motorcycle Maintenance]].  [[System change]]

> If a factory is torn down but the rationality which produced it is left standing, then that rationality will simply produce another factory. If a revolution destroys a government, but the systematic patterns of thought that produced that government are left intact, then those patterns will repeat themselves. . . . There’s so much talk about the system. And so little understanding.

-   via [[Thinking in Systems]]

<!--listend-->

-   [[The Limits to Growth]]

