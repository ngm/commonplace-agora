# 2022-08-30

-   [[Our feelings are not just biological, but also social and cultural and therefore historical]]

-   Noding my highlights from [[The Ministry for the Future]].  There's a lot.  For my reference, currently done up to page 357.

-   My [[YunoHost]] server seems to have run out of space, and now I can't remotely ssh in.  Sigh.  Will have to fix.
    -   Wow, even the KVM from withing Hetzner control isn't loading.  Hmm.
    -   Had to do a hard power cycle.
    -   Some stupidly large log files.  huginn.log and invidious.log, gigabytes large.
    -   Freed up some space.  But presume it will fill up again.  I think I'll just uninstall huginn for now, as I don't use it.

