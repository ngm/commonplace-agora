# 2022-02-13

-   Really enjoyed reading @ntnsndr's article [[Governable Stacks against Digital Colonialism]] for [[node club]].  Makes a ton of sense to me.
    -   The ability to self-govern feels key to any alternative to big tech that we build or use, and feels a lot like commoning.
    -   And the 'stack' element is vital - what use would a democratically-owned social media platform be, say, if it runs on a server owned by Amazon and you access it on a device locked down by Apple or Google over internet from a big telco.
    -   And take it further, long-term what use is any of it if at the very bottom layer of the stack is a capitalist society?

-   Cooperatives and commoning seem a good model for socialising the layers of the stack. I think [[Libre software]], [[libre hardware]], [[right to repair]], [[peer-to-peer networks]], [[Community broadband]] are all good projects to spend time on.

-   I loved this interview with [[Fritjof Capra]] on his new book [[Patterns of Connection]].
    -   https://newbooksnetwork.com/patterns-of-connection
    -   As you might expect, there's a lot about systems thinking, complexity science, science and sprituality, activism.
    -   Some random notes:
        -   Systems thinking is about patterns, relationships and contexts.
        -   The machine metaphor from 17th century science is still too prevalent. The network is a better metaphor to live by.
        -   The [[Battle of Seattle]] was the start of a global civil society.
        -   He recommends the [[Earth Charter]].
        -   There's generally conflict between science and religion, less so science and spirituality.

