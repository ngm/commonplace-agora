# 2022-01-21

-   [[Hobbesianism]]

-   I like [[cross-disciplinary learning]] as it is a way of harnessing [[tangential thinking]].
    -   Explains one reason I am drawn to [[cybernetics]] I guess.  I know it's one of the criticisms levelled against it, also - it's so cross-disciplinary that it can be abstracted to the point of meaningless.  But I like it as a kind of [[Oblique Strategy]] as opposed to an attempt at a theory of everything.

-   [[Expanding national parks and protected areas will not be enough to halt the destruction of nature]].  Say scientists.  Seems kind of obvious but I think it's a riposte to what the UN convention on biological diversity is proposing.

-   [[org-roam]] v2.2.0 is released.  Time to update and see what breaks!
    -   https://github.com/org-roam/org-roam/releases/tag/v2.2.0
    -   Seems to have gone OK&#x2026;

