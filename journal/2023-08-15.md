# 2023-08-15

-   Listened: [[Trip 34: The Outdoors]]
    -   I really enjoy #ACFM podcasts.  They take fairly everyday things and look at them through a leftist lens, and throw a bit of music in too.
    -   I like hiking, so listening to the political history of [[right to roam]] is fun.
    -   The [[National Clarion Cycling Club]] sound great: to "combine the pleasures of cycling with the propaganda of Socialism"

