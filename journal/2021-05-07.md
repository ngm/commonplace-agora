# 2021-05-07

-   Reading: [[Cybernetic Revolutionaries]]
    -   [[Salvadore Allende]]
    -   [[US intervention]]
    -   [[Chilean road to socialism]]
    -   [[Project Cybersyn]]
    -   [[cybernetics]]
    -   [[Stafford Beer]]

-   Listened: [[Looks Like New: Can Business Account for Care Work?]]
    
    -   [[Stacco Troncoso]]
    -   [[DisCO Manifesto]]
    -   [[DisCOs]]
    
    > Large-scale problems do not require large-scale solutions.
    > They require small-scale solutions within a large-scale framework.
    > 
    > &#x2013; [[David Fleming]]

