# 2023-02-12

-   [[Synchronized pomodoros]]
    -   [[Pomodoros]]
        -   [X] p0: reviewing presentation 2 for [[YXM830]]
            -   Internal distractions:
                -   searching for some music to listen to
                -   noding [[YXM830]]
        -   [X] p1: reviewing presentation 2 for [[YXM830]]
            -   Internal distractions:
                -   went on social.coop briefly
    -   People
        -   [[Flancian]] joined during p1

