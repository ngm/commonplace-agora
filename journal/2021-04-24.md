# 2021-04-24

-   Listened: [[Rethinking infrastructure: the case for social investment]]
    -   [[Care work is infrastructure]].

-   Pleased to be coming up with a few more [[notion]]s now.  In the interests of keeping track of what I'm thinking about, as a kind of mishmash of [[speculative outline]]s, now pages, and [[Spaced repetition]], I'm going to have a make a page called (awfully) [[nowtions]], which is a list of latest notions.

-   [[Citizen participation platform]]s: [[Decidim]], [[Your Priorities]], [[Consul]]

-   Could you describe Agora as a [[citizen participation platform]] for citizens of a virtual place?

-   [[musicForProgramming]] [mix #1](https://www.musicforprogramming.net/?one) by [[Datassette]] is rad.

-   Stumbled on a software suite for UK town and parish councils.  Managing allotments, playgrounds, cemeteries, planning, inspections, etc etc.  This stuff is kind of fascinating, e.g. https://www.edgeitsystems.com/wp-content/uploads/2020/01/AdvantEDGE-Allotments.pdf
    -   Sucks that this is one business somewhere and of course it all appears to run on Microsoft tooling.
    -   I think all this should be a libre stack, supported and set up locally by local tech coops, working together and committing back to the stack.

