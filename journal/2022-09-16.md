# 2022-09-16

-   I volunteered at the [[Ulverston Repair Cafe]] on Tuesday.
    -   It was very nice.  Big spacious venue, friendly people.
    -   I tried to fix a couple of printers that people had brought in.
    -   No joy - very old ink cartridges were possibly the problem.  Beyond that, they would need a full print head clean, which can be a bit 'kill or cure'.

-   Making a [[Pacman lantern]] for the [[Ulverston Lantern Festival]].
    -   Having fun doing a mini lights circuits with my [[Micro Bit]].

-   Some book scores in the local charity shops the last few weeks.
    -   [[Networks of Outrage and Hope]]
    -   [[Revolution in Rojava]]
    -   Common Wealth: For a free, equal, mutual and sustainable society
    -   Marx for Beginners (a cartoon book by Rius)
    -   The Lego Ideas Box
    -   365 Things to Do with LEGO Bricks

-   This spin on [[social ecology]] looks interesting - some kind of combo of [[Murray Bookchin]], [[Gregory Bateson]], [[Fritjof Capra]]. [Social Ecology: Applying Ecological Understanding to our lives and our Planet&#x2026;](https://www.hawthornpress.com/books/changemaking/social-ecology-change/social-ecology/)

