# 2022-02-11

-   Doing [[Node Club Tarot]] again.  [[Jayu]] did it too!  I like their use of 'art' 'method' and 'intended goals' meta data.

-   Fuck Spotify, join [[Resonate]].

-   [[Bill Seitz]] is doing some cool stuff with [[sister sites]] and dynamic [[twin pages]] -
    -   https://twitter.com/BillSeitz/status/1490447807567876096
    -   http://webseitz.fluxent.com/wiki/2022-02-06-AddingSisterSites
    -   If you combined it with simple inline transclusion, if wouldn't be far off what I kind of want from a [[garden reader]].

-   [[tripleC]] journal looks pretty great.
    -   Bookmarked: [[Teaching the Commons through the Game of Musical Chairs]]

-   [[Fixing my sites URLs]].

