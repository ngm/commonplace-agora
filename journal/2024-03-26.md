# 2024-03-26

-   Further to [[trying out org-timeblock]], I'm now [[trying out calfw-blocks]].
    -   As part of a general attempt to be able to do timeblocking in org-mode. ([[Using org-mode for timeblocking]]).
    -   I wouldn't say it's going swimmingly&#x2026; but I'm learning plenty about spacemacs layers, doom config, and use-package.  So that's something.

