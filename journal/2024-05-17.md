# 2024-05-17

-   Read: [[Critique of techno-feudal reason]]
    -   Well, sort of read it - actually half listened to it via Wallabag while taking the bubba out for an early walk.
    -   Dense and erudite, as you expect from Morozov.
    -   Didn't follow half of it this first time around to be honest, but I take away the basic message that he's critical of the general idea of [[techno-feudalism]].

-   [[There is no capitalism without colonialism]].

