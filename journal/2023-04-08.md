# 2023-04-08

-   Read: [[Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]

-   Listened:  [[The Week in Green Software: Netflix, Refurbishment and Anti-Greenwashing Laws]]
    -   Fun podcast. Enjoyed the discussion. Bits on recent EU right to repair legislation. And distributing stored energy in/from batteries. And the environmental benefit of buying 2nd hand.

-   Listened: [[Trebor Scholz on Platform Cooperativism]]
    -   Lots of examples of [[platform coops]] given. [[Kerala]] was mentioned.
    -   I enjoyed the discussion on [[scale]]. And particularly how coops tend to scale via federation. Some discussion of exploration of DAOs for distributed governance at scale, but with a healthy bit of scepticism.

