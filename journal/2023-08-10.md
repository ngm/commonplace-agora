# 2023-08-10

-   At Tuesday's repair cafe: [[Repairing a Canon Pixma TS3150]]

-   Listened: [[Cory Doctorow, "The Internet Con: How to Seize the Means of Computation"]]

-   Listened: [[Frank Jacob, "Wallerstein 2.0: Thinking and Applying World-Systems Theory in the 21st Century"]]

