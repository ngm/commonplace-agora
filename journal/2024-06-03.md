# 2024-06-03

-   Listened: [[Trump is guilty on all counts. So what happens next?]]
    -   Teflon Trump. He'll still run. And this will make no difference to people who already like [[Donald Trump]].
    -   Best hope is that maybe it will affect some swing voters.

-   Listened: [[Has there been a purge of the leftwing of the Labour party?]]
    -   Shitty treatment of [[Diane Abbott]] and [[Faiza Shaheen]].
    -   Attempts to stop them running as Labour candidate.
    -   General bad treatment of candidates from the left of the party.

