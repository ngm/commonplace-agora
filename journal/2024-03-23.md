# 2024-03-23

-   Listened: [[Yanis Varoufakis, "Technofeudalism: What Killed Capitalism"]]
    -   Great interview with [[Yanis Varoufakis]].
    -   [[Techno-feudalism]].
    -   Ultimate goal: socialisation of cloud capital (i.e. [[Big tech]])
    -   Only America and China have cloud capital.
    -   Glad they mention [[McKenzie Wark]] - Varoufakis says he agrees with all of it.

-   [[Listened]]: [[Democracy for sale, Europe’s first black leader + tea with Obama]]
    -   [[Political donations]].
    -   New first minister of Wales is first black leader of a country in Europe.
    -   [[Rwanda bill]].

-   [[Listened]]: [[Black Box: Episode 6 – Shut it down?]]
    -   [[Eliezer Yudkowsky]] with AI doonerism.
    -   Alternate positive view from Alex Hern.
    -   One disappointment with the series as a whole is there's no mention of environmental impact or questioning of who owns AI technology.

