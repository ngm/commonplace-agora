# 2021-06-19

-   [[Modern Unix]]. Really handy list of improved shell utilities.  `tldr` is a great one - short and simple example-based man pages.
    -   https://github.com/ibraheemdev/modern-unix

-   [[Branch]] issue #2

https://branch.climateaction.tech/issues/issue-2/

[[Solarpunk]], Big Tech resistance, AI policy, greenwashing, sustainable web craft, the [[commons]].

-   purr-driven development

-   [[Solar Protocol]]
    -   http://solarprotocol.net/
    -   A fun (art?) project that is building a network of solar-powered servers that work together to host a web site.  Wherever currently has the most sun takes over the hosting.
    -   Dunno how much of these things translate from art/fun to practical use, but good for raising questions and awareness about where we get our energy from.
    -   Would be interesting to combine Solar Protocol with distributed content on something like [[Hyperdrive]] or [[IPFS]].

-   [[Data Garden]]
    -   **URL:** https://cyrus.website/data-garden
    -

> By storing data nature’s way, in the DNA of plants, this work discusses the potential for truly green, carbon absorbing data storage, owned by the public rather than monopolistic corporations.

-   Another art project, not necessarily reality anytime soon, but I love this as an idea and a provocation.

