# 2022-12-24

-   Reading: [[Less is More]]
    -   Really good opening on all the current environmental crises currently occurring as part of the [[Anthropocene]] (or more accurately the [[Capitalocene]]).
    -   And a great history of [[Capitalism]].  Arguing that [[commoning]] was what brought an end to [[Feudalism]], and that capitalism, the [[Enclosure Movement]] and [[colonialism]] was the response.

-   Listening: [[Trip 30: Gifts]]
    -   Was listening while doing chores.  Enjoyed the [[Marxian analysis of Christmas]] and Santa Claus.

-   Listening: [[Benjamin Bratton on Synthetic Catallaxies, Platforms of Platforms &amp; Red Futurism]]
    -   Very interesting interview with [[Benjamin Bratton]].  Talking about [[socialist calculation debate]], [[cybernetics]].  This idea of [[Synthetic catallaxy]].

-   Listening (a while ago): [[42. TECH FOR GOOD]]

