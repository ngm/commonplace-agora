# 2024-11-04

-   Listening to [[Doughnut Economics]] now.
    -   I've recently read it, but seemed like a good choice to revisit - big ideas but palatably presented.
    -   Also I consider it reasonably [[ecosocialist]] in outlook.  The combo of social foundation and planetary boundaries.  Even though it presents itself somewhat apolitically.

-   [[Data commons]] are [[digital ecosocialism]].
    -   I work on a data commons - the [[Open Repair Alliance dataset]].

