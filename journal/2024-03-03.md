# 2024-03-03

-   How do you socialise something that has been privatised?
    -   i.e. how do you enact [[deprivatisation]].

-   Watched: [[Avengers: Age of Ultron]]

-   Listened: [[Hotel Bar Sessions: Breaking Things at Work (with Gavin Mueller)]]
    -   [[Luddism]].
    -   Claim: Free software movement was a form of Luddism.
    -   Great example. Hackers were clearly not anti-technology, they were concerned about the enclosure of technology for capitalism.

