# 2021-02-04

Really enjoyed the [[When Preston Meets the Doughnut]] webinar.  Inspiring to hear more about what's going on in [[Preston]], and to see how Kate Raworth has been working on adaptations of [[Doughnut Economics]] to the city-level.

