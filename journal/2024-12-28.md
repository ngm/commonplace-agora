# 2024-12-28

-   Reading [[Four Thousand Weeks]]
    -   The efficiency trap - the more efficient you get, the more things you will fit into to do list, ultimately not gaining any time.
    -   Productivity techniques are a way of facilitating avoidance of making hard choices.
        -   By claiming you can get more efficient and fit more in, you can avoid having to decide what not to do.
    -   Pay yourself first.
    -   Keep three things in progress.

