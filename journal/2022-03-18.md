# 2022-03-18

-   [[File error: Getting attributes, Permission denied, /run/user/1001/emacs]]

-   Awesome article: [[Markets in the Next System]].
    -   I don't have a ton of knowledge about [[markets]] but this makes plenty of sense to me.
    -   Particularly the outline for [[Markets after capitalism]].
    -   [[Markets vs planning]]
    -   [[Demarketisation]]

