# 2024-01-14

-   Listened: [[Trip 39: Protest]]
    -   On the topic of [[protest]].
    -   Individual, collective. Marches, non-violence, [[direct action]], boycotts etc.
    -   Whats effective and what isnt? Effective might mean different things, e.g. could be political change but could also be just connecting and energising a movement.

