# 2021-05-13

-   [[Space Cats Fight Fascism]]
    -   It arrived recently.  We've been playing it a lot and it is great fun.
-   Watched: [Nadia Eghbal: The Making and Maintenance of our Open Source Infrastructure](https://longnow.org/seminars/02020/nov/17/making-and-maintenance-our-open-source-infrastructure/)
    -   [[Free software economics]]
    -   [[Libre software]] as both a [[public good]] and as a [[Commons]].
-   [[How to get RSS feeds of Facebook pages]]
-   Listened: [[Dare to Repair: How We Broke the Future]]
-   Read: [[The Nature of Lisp]]
-   Reading: [[Free, Fair and Alive]]
    -   The insurgent power of the commons.
    -   [[commons-public partnerships]]
-   [[What is the relationship between cooperatives and the commons?]]

