# 2023-03-17

-   [[Mutual aid]]
-   [[Google Scholar alternatives]]
-   [[Solar panels and gardening]]
-   [[What if it's a big hoax and we create a better world for nothing?]]
-   [[Online Safety Bill]]
-   [[Agroecology and open-source software]]
-   [[Worker coops and consulting]]
-   [[Synergia MOOC]]
-   Setting up [[Automatic commonplace to Agora]] publishing on my server

