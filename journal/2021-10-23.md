# 2021-10-23

-   [[Public space]].
    -   Protect public spaces.

> "when public spaces are eliminated, so ultimately is the public; the individual has ceased to be a citizen capable of experiencing and acting in common with fellow citizens"
> 
> — [[Rebecca Solnit]]

-   For various reasons I only finally just got my brother's Christmas 2020 gift to me - it was [[Celebrate People's History: The Poster Book  of Resistance and Revolution]] and it is fantastic.  Over a hundred inspiring posters celebrating moments of resistance and revolution.

