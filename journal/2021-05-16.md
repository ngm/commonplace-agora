# 2021-05-16

-   I was going to do something and then my cat sat on me and now I can't.

-   [[Jan Boddez]] is working on a [[Microsub]] based feed aggregator/reader that's PHP/MySQL only.  Keen to look in to that - would be easier to set up on YunoHost than Aperture (which has dependencies on Watchtower, Beanstalk, and Redis).

-   [[Free, Fair and Alive]]  is rocking my socks right now.  It has a whole section on [[Federated wikis]] (specifically [[FedWiki]]) and how they can form a [[Commons]].  Add this to the [[complexity science]] and [[commons-public partnerships]] bits I've already read, it's really hitting all my sweet spots so far&#x2026;

-   Read: [[A Platform Designed for Collaboration: Federated Wiki]].  A section in the book [[Free, Fair and Alive]] on [[Federated wikis]] and their power in producing [[commons]]. Discusses [[FedWiki]] in particular but can apply to [[Federated wikis]] in general (e.g. [[Agora]], [[IndieWeb]]).

-   Funny that the [[digital garden]] metaphor makes an appearance in [[Free, Fair and Alive]]:

> To use another metaphor: people using Fedwiki sites are like gardeners or farmers. They can plant as many fields or gardens as they want, and reap the harvest from their own Fedwiki, but anyone else can also use someone’s harvest to enhance their own fields and gardens. Instead of toiling under a regime of private, competitive exclusion, the system encourages cooperative gains through commoning.

-   Read: [New BSL co-operative ‘could transform the future of interpreting’](https://www.disabilitynewsservice.com/new-bsl-co-operative-could-transform-the-future-of-interpreting/)
    
    > Leading Deaf figures are supporting a new co-operative ([[Signalise]]) that is developing an online platform that they hope will “revolutionise” the booking of interpreters, “take control” from profit-making agencies and deliver “real and long-lasting social change”.

