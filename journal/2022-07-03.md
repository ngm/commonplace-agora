# 2022-07-03

-   [[Technics]]

-   Listened: [[Turning the Earth into Money w/ John Bellamy Foster]]

-   [[carbon footprint]] is bunk

-   Environmentalism is presently primarily the preserve of the [[professional-managerial class]].  It needs to be inherently valuable to the [[working class]].
    -   [[Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"]]

-   Decarbonise electricity, then electrify everything.

-   [[Climate sadism]]

