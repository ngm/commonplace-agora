# 2024-03-31

-   Went for a walk and [[listened]] to [[The Silicon Empire in Eastern Europe (ft. Erin McElroy)]]
    -   [[Silicon Valley Imperialism]].

<!--listend-->

-   Following a stumble through the garden related to [[technology and political economy]], re-reading [[The Telekommunist Manifesto]].
    -   [[political economy of network topologies]]
    -   [[political economy of free software]]

-   Also plan to re-read [[The British Digital Cooperative: A New Model Public Sector Institution]].

