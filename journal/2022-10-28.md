# 2022-10-28

-   "The more I reflect on these facts, the more I perceive that the evolutionary approach to adaptation in social systems simply will not work any more. . . . It has therefore become clear to me over the years that I am advocating revolution"

&#x2013; Stafford Beer quoted in [[Cybernetic Revolutionaries]]

