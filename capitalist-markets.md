# Capitalist markets

[[Capitalist]] [[markets]].

There were [[Markets before capitalism]].

> Capitalism isn’t distinguished by its capacity to provide market opportunities, but by the imperatives the market places on its unique system of production.
> 
> &#x2013; [[Markets in the Next System]]

There will be [[Markets after capitalism]].

