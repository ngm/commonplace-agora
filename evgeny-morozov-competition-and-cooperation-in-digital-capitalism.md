# Evgeny Morozov: Competition and Cooperation in Digital Capitalism

Found at
: https://www.youtube.com/watch?v=FKOKjDQ662A

[[Evgeny Morozov]] on [[digital capitalism]] / [[digital socialism]].

His main point I think is that socialists should be bolder than simply trying to regulate digital capitalists.  Socialism historically was innovative and system changing. Rather than just regulating or even just building socialised copies, we should be building bold new systems.

