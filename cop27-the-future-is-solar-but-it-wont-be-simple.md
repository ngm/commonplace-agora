# Cop27: the future is solar, but it won't be simple

A
: [[podcast]]

URL
: https://www.theguardian.com/news/audio/2022/nov/11/cop27-the-future-is-solar-but-it-wont-be-simple-podcast

Some very interesting stuff about the conflicts over the [[Mammoth Solar]] [[solar farm]] in rural Indiana.  How even with national support from the Inflaction Reduction Act and whatnot, local resistance can stymy big solar projects.

Also Joe Biden at [[COP 27]] and the [[Inflation Reduction Act]].

