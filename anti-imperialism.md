# Anti-imperialism

-   socialism requires anti-imperialism (as imperialism is the principle part of capitalism)
-   there is no socialism that is not anti-imperialist
-   [[Rosa Luxemburg]] played a big part in its theorisation
-   Critique by Bukharin


## Anti-imperialism and ecology

> There are numerous movements that link anti-imperialism with ecological politics, stretching from indigenous social movements in Latin America to the Rojava Revolution.
> 
> &#x2013; [[Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]

