# Digital urban planning

In [[Future Histories]] Lizzie O’Shea is using urban planning as an analogy for thinking about how we could design our digital spaces. Riffing off [[Freud]]’s thoughts about the mind as a city, and [[Jane Jacobs]]’s work on cities and planning.

I’m liking this, I was thinking about it recently, with an online presence being like a person’s home on the web. Taking it up a layer you think about digital urban planning, how these homes (and other things) fit together to make a city. I like it as a frame.  (Probably because I’ve been living in a big city the last 10 years.)


## [[The vulgar and the strange (web)]]


## [BlogosphereAsACity](http://thoughtstorms.info/view/BlogosphereAsACity)

> If you think of the web as an analogue of the city environment in which we live, at present the urban master plan is imposed by Facebook , Instagram, Medium, Twitter and all the other social networks: the buildings must be of a certain height, with a predetermined number of windows per facade, and the colors that can be used are reduced to a minimum.
> 
> &#x2013; [Giardini digitali: come creare un blog davvero personale](https://www.vice.com/it/article/pky7vv/come-creare-blog-giardino-digitale) 

