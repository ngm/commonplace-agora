# carbon footprint

> They control us, they exclude us, and then – as in the ingenious concept of the ‘carbon footprint’, which is the equivalent of those “drink responsibly” warnings at the end of ads for beer – they scold and blame us.
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> British Petroleum, the second largest non-state owned oil company in the world, with 18,700 gas and service stations worldwide, hired the public relations professionals Ogilvy &amp; Mather to promote the slant that climate change is not the fault of an oil giant, but that of individuals.
> 
> It’s here that British Petroleum, or BP, first promoted and soon successfully popularized the term “carbon footprint" in the early aughts.
> 
> &#x2013; [The devious fossil fuel propaganda we all use | Mashable](https://mashable.com/feature/carbon-footprint-pr-campaign-sham)

[Big oil coined ‘carbon footprints’ to blame us for their greed. Keep them on &#x2026;](https://www.theguardian.com/commentisfree/2021/aug/23/big-oil-coined-carbon-footprints-to-blame-us-for-their-greed-keep-them-on-the-hook)

