# Independent Together: Building and Maintaining Values in a Distributed Web Infrastructure

URL
: https://dissertation.jackjamieson.net/

> This dissertation studies a community of web developers building the [[IndieWeb]], a modular and decentralized social web infrastructure through which people can produce and share content and participate in online communities without being dependent on corporate platforms.

<!--quoteend-->

> The purpose of this dissertation is to investigate how developers’ values shape and are shaped by this infrastructure, including how concentrations of power and influence affect individuals’ capacity to participate in design-decisions related to values.

<!--quoteend-->

> Individuals’ design activities are situated in a sociotechnical system to address influence among individual software artifacts, peers in the community, mechanisms for interoperability, and broader internet infrastructures.

