# Evo Morales

> The rise of Indigenous politician Evo Morales to the presidency of [[Bolivia]] revealed a similar lesson: even smart, well-intentioned electoral movements have trouble transcending the deep imperatives of state power because the state remains tightly yoked to an international system of capitalist finance and resource extraction.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Pablo Solón Romero, a long-time Bolivian activist and the former Ambassador of the Plurinational State of Bolivia to the United Nations (2009-2011), told a cautionary tale:
> 
> Fifteen years ago [in the early 2000s], we had a lot of [[commoning in Bolivia]]— for forests, water, justice, etc. To preserve this, when our enemy was the state and privatizing everything, we decided we would take the state. And we succeeded! And we were able to do good things. Now we have a plurinational state. That’s positive. But &#x2026; ten years later, are our communities stronger or weaker? They are weaker! We can’t do everything that we wanted to do via the state. The state and its structures have their own logic. We were naïve. We didn’t realize that those struc-
> tures were going to change us. 30
> 
> &#x2013; [[Free, Fair and Alive]]

