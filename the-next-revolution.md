# The Next Revolution

Author
: [[Murray Bookchin]]

Publisher
: [Verso](https://www.versobooks.com/books/1777-the-next-revolution)

Has a foreword from [[Ursula K. Le Guin]].


## Essays


### [[The Communalist Project]]


### The Ecological Crisis and the Need to Remake Society,

> The second essay, "The Ecological Crisis and the Need to Remake Society," elucidates the core insight of Bookchin’s [[social ecology]]—that the ecological and social crises are intertwined, indeed, that our domination of nature is a projection of domination of human by human in society. Rejecting ecological arguments that blame individual choices, technology, or population growth, Bookchin argues that the ecological crisis is caused by an irrational social system governed by the cancerous logic of capitalism, driven by its competitive grow-or-die imperative and its endless production directed not toward meeting human needs but accumulating profit. Arguing against the extremes of an authoritarian state or totally autonomous self-sufficiency, Bookchin offers Communalism as an emancipatory alternative capable of saving ourselves and nature at the same time.


### A Politics for the Twenty-First Century

> The first outlines how confederated assemblies can assert popular control over the economy in order to abolish it as a separate social realm, directing it to human needs rather than profit. 


### The Meaning of Confederalism

> “The Meaning of Confederalism” further elaborates on these themes and addresses specific objections to the concept of confederal [[direct democracy]]. It answers common questions such as, Is confederation feasible in a globalized world? How would local assemblies address bigger problems in a democratic manner? Would local communities cooperate or compete with each other, or could [[localism]] devolve to parochialism? 


### Libertarian Municipalism: A Politics of Direct Democracy

“Libertarian Municipalism: A Politics of Direct Democracy” traces the familiar historical trajectory from movements into parties—social democratic, socialist, and Green alike—which have consistently failed to change the world but instead are changed by it. By contrast, [[libertarian municipalism]] changes not only the content but also the form of politics, transforming politics from its current lowly status as what reviled politicians do to us into a new paradigm in which politics is something we, as fully engaged citizens, do for ourselves, thus reclaiming democratic control over our own lives and communities.


### Cities: The Unfolding of Reason in History

Exploring the unique liberatory potential of the city and the citizen throughout history, “Cities: The Unfolding of Reason in History” examines the degradation of the concept of “citizen”—from that of a free individual empowered to participate and make collective decisions to a mere constituent and taxpayer. Bookchin seeks to rescue the Enlightenment notion of a progressive, but not teleological, concept of History wherein reason guides human action toward the eradication of toil and oppression; or put positively, freedom.


### Nationalism and the 'National Question'

> The essays “Nationalism and the ‘National Question’ ” and “Anarchism and Power in the [[Spanish Revolution]]” elucidate a libertarian perspective on questions of power, cultural identity, and politcal sovereignty.
> 
> In the former, Bookchin places nationalism in the larger historical context of humanity’s social evolution, with the aim of transcending it, suggesting instead a libertarian and cosmopolitan ethics of complementarity in which cultural differences serve to enhance human unity.


### Anarchism and Power in the Spanish Revolution

> In “Anarchism and Power in the Spanish Revolution” he confronts the question of power, describing how anarchists throughout history have seen power as an essentially negative evil that must be destroyed. Bookchin contends that power will always exist, but that the question revolutionaries face is whether it will rest in the hands of elites or be given an emancipatory institutional form.


### The Future of the Left

> The concluding, previously unpublished, essay “The Future of the Left” assesses the fate of the revolutionary project during the twentieth century, examining the Marxist and anarchist traditions. Bookchin argues that Marxism remains trapped by a limited focus on economy and is deeply marred by its legacy of authoritarian statism. Anarchism, by contrast, retains a problematic individualism that valorizes abstract and liberal notions of “[[autonomy]]” over a more expansive notion of freedom, ducking thorny questions about collective power, social institutions, and political strategy. Communalism resolves this tension by giving freedom concrete institutional form in confederated popular assemblies. The essay concludes with a passionate defense of the Enlightenment and a reminder that its legacy of discerning the “is” from the “ought” still constitutes the very core of the Left: critique directed toward unlocking the potentiality of universal human freedom.

<!--quoteend-->

> He makes a crucial distinction between statecraft and politics. He sees the state as a force for domination and statecraft as the means by which it is sustained. Politics, by contrast, is “the active engagement of free citizens” in their own affairs.


## Reviews

> His writings on this theme were published posthumously in a book called The Next Revolution. You wouldn’t read it for pleasure. His style is stern, clunky and verbose, without warmth or humour. But his ideas are powerful.
> 
> &#x2013; [Enclaves of Democracy – George Monbiot](https://www.monbiot.com/2022/07/17/enclaves-of-democracy/)

<!--quoteend-->

> Even so, I don’t see Bookchin’s prescriptions as a panacea. I don’t believe he deals adequately with the problems of global capital, global supply chains, defence against aggressive states or the need for global action on global crises. But at the very least, we can create enclaves of democracy in a landscape of domination. 
> 
> &#x2013; [Enclaves of Democracy – George Monbiot](https://www.monbiot.com/2022/07/17/enclaves-of-democracy/)

