# An eco-socialist definition of abundance

An [[eco-socialist]] definition of [[abundance]].

> Abundance, not as supermarket cornucopia, but as the restoration of energy, free time, and mental and physical health.
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> Abundance, not of shareholder value, but of shared political power – currently blackboxed as capitalist and state power.
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> Abundance, not of goods made for planned obsolescence, but of low-carbon goods like care and services
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> The abundance of life itself
> 
> &#x2013; [[For a Red Zoopolis]]

