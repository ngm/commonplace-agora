# Alter-globalization

> One of the key legacies of the alterglobalisation movement is a rejection of hegemonic politics in favour of a politics built around a proliferation of autonomous affinity groups.
> 
> &#x2013; [[Anarchist Cybernetics]] 

