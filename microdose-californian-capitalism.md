# Microdose: Californian Capitalism

A
: [[podcast]]

URL
: https://novaramedia.com/2023/08/02/acfm-microdose-californian-capitalism-malcolm-harris-palo-alto/

Series
: [[#ACFM]]

Featuring
: Malcolm Harris / [[Keir Milburn]]

Interview with Malcolm Harris on his book [[Palo Alto: A History of California, Capitalism, and the World]].

> Ahead of an ACFM Trip about the internet, Keir Milburn is joined by Malcolm Harris to talk about the unique [[political history]] of his hometown of [[Palo Alto]] – the intellectual laboratory for a century of [[American hegemony]].
> 
> The Kids These Days author tells a story that connects the founding of [[California]], the violent removal of its native population, [[Stanford University's eugenicist agenda]] and the parallel emergence of [[Silicon Valley]] and the [[military-industrial complex]]. Is the entire project of [[personal computing]] and the [[internet]] tainted by this brutal history?

Interesting stuff.  One particular bit that caught my attention was how Harris questions the narrative of [[The Californian Ideology]].  Something about there being less of a connection between the tech-bros and the New Left than the narrative might have you believe.  Didn't fully get the nuance but would be interesting to delve into that a bit more.

Discusses the link between Stanford and military technology, and then later on by extension the link between network technology and militarism.  Milburn references [[Occupy]], [[Arab spring]], Lee Felsenstein, [[Richard Stallman]], examples of leftist hopes for technology and then how they ultimately panned out, and asks whether Harris can see much liberatory potential for the internet.  For Harris, given how imbricated the history of the internet is with militarism and oppresion, his answer seems pretty much 'no'.

At the end Keir Milburn mentions [[Lee Felsenstein]] and says he was a member of the "Berkeley People's Computer Club" - I can't find a reference to that.  I think it must be a conflation of [[Homebrew Computer Club]], [[People's Computer Company]], and [[Community Memory]] all of which Felsenstein seem to have some connection with.

