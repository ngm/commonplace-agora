# Re-ification

Reificiation, but for re-doing things. Nostalgia fetish.  Just bearing in mind that re-doing things might not necessarily be the best way to do things.  Rewilding.  Redecentralising.  etc.  Are we kidding ourselves that a past state actually existed as we think it did?  Are we wanting to make something concrete that wasn't?  Maybe things were better, but always evaluate critically.  Avoid primitivism.

See: [[reification]].

