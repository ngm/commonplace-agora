# carbon pollution

> Carbon pollution will have to contract 7.6 per cent every year in the 2020s to achieve the [[1.5C]] target, surpassing even the 5.5 per cent fall in emissions during the pandemic-induced recession of 2020
> 
> [[Half-Earth Socialism]]

