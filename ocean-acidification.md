# ocean acidification

> Ocean acidification and deoxygenation are other examples of things done by humans that we can’t undo, and the relation between this ocean acidification/deoxygenation and the extinction event may soon become profound, in that the former may stupendously accelerate the latter.
> 
> &#x2013; [[The Ministry for the Future]]

