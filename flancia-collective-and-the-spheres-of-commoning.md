# Flancia Collective and the spheres of commoning

As a fun thought experiment, I'd like to have a think about how [[Flancia Collective]] is doing stuff in each of the [[Spheres of commoning]].

Also to be thought about: what commons are we actually stewarding?  And who are the commoners?

We are creating digital commons of software and a knowledge commons, I guess.

Mike Hales' [handbook](https://meet-coop-1.gitbook.io/handbook-trial/) on meet.coop would be a good reference point I reckon.

I guess should look at each of the patterns through the lens of how does Flancia Collective do these things?  Or how do we do these things in relation to a particular commons?  Comes back to defining what actually are our commons.


## Social life ([[The Social Life of Commoning]])


### Cultivate Shared Purpose and Values

-   We kind of have this, but I don't know what it means to cultivate it, and whether we're doing that.


### [[Ritualize Togetherness]]

-   We have a few chat spaces on Element. This is async togetherness.
-   [[node club]] - kind of a shared 'custom' each week (/month&#x2026;).  Async togetherness also.  As a way of fleshing out the knowledge commons.
-   there is a weekly online [[flancia meet]], and a weekly [[Agora Meet]], to meet in person.


### Contribute Freely

-   We do this - both code and info.


### Practice Gentle Reciprocity


### Trust Situated Knowing


### [[Deepen Communion with Nature]]

-   Bit of a wild card one for digital commons.  But could link it in via [[sustainable tech]].


### Preserve Relationships in Addressing Conflicts


### Reflect on Your Peer Governance


## [[Peer governance]]


### Bring Diversity into Shared Purpose

-   Some diversity in views.  But not a huge amount of diversity in background.


### Create Semi-Permeable Membranes


### Honor Transparency in a Sphere of Trust


### Share Knowledge Generously

-   Check.


### Assure Consent in Decision Making

-   Happens roughly through Element chat.


### Rely on Heterarchy


### Peer Monitor &amp; Apply Graduated Sanctions


### Relationize Property


### [[Keep Commons &amp; Commerce Distinct]]

At present, we do not have any commercial activity associated with the commons that are produced by Flancia.


### [[Finance Commons Provisioning]] / [[Choose Commons-Friendly Financing]]

As of August 2022, [[Flancian]] has been donating and continues to donate around half of his discretionary monthly income to Flancia related projects.  This includes Give Directly / Effective Altruism funds donations, but also funding roughly 5 basic incomes directly month to month on 12 or 24-month basis. And some open source projects.  We also provide hosting for [[Anagora]], and [[moa]], and would like to provide the same for other ethical services.

So I guess this probably classes as [[collaborative financing]].

As stated by Flancian:

> the idea here is for the community to decide how we spend this money ethically :) this is why I started working on the Agora essentially &#x2013; to find ways to do this kind of work more intelligently/effectively/compassionately together
> 
> &#x2013; https://matrix.to/#/!WhilafaLxfJNoigHCj:matrix.org/$MID6gaTAhKL_6srQef9fMzkb8jrTLwfXQTQBr9cT86s


## Provisioning ([[Provisioning Through Commons]])


### Make &amp; Use Together


### Support Care &amp; Decommodified Work


### Share the Risks of Provisioning


### Contribute &amp; Share


### Pool, Cap &amp; Divide Up


### Pool, Cap &amp; Mutualize


### Trade with Price Sovereignty


### [[Use Convivial Tools]]

-   I think we do, yeah.  We tend to work with [[Free software]].


### Rely on Distributed Structures

-   We **like** distributed structures, for sure.  And we'd like Agora to be distributed (or at least federated).  Do we make use of them though?


### Creatively Adapt &amp; Renew

