# 'I spot brand new TVs, here to be shredded': the truth about our electronic waste

URL
: https://www.theguardian.com/environment/2023/jun/03/i-spot-brand-new-tvs-here-to-be-shredded-the-truth-about-our-electronic-waste

Expensive, fully-working electronic and electrical items shredded, for no good reason.  The problem of [[e-waste]] and [[consumption]], counter to [[planetary stability]], at the hardware layer of the stack.

