# Horizontalism

> The rise of ubiquitous social media rode on waves of protest, from the [[Battle of Seattle]] to the [[Arab Spring]]. Individual voices, linked with hashtags, seemed to herald collective liberation (Papacharissi 2015). And yet, despite the outpourings of promise and hope and near-term victories, those digitally mediated uprisings have fallen under the police of Mohamed Morsi and the bombs of Bashar al-Assad, the famines of the [[Yemeni civil war]] and the warlords of Libya. ‘Pirate’ political parties arising out of online protest have tended to collapse upon their first encounter with power, if they ever get there.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

