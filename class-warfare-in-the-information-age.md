# Class Warfare in the Information Age

Author
: [[Michael Perelman]]

> The mention of [[class struggle]] evokes images of a grimy age in which bygone captains of industry callously oppressed armies of overworked and underpaid proletarians. This dark portrait of class conflict stands in sharp contrast to the glorious promise of an [[information age]]. In Class Warfare in the Information Age, Michael Perelman shows how [[class conflict]] remains a contemporary issue. He challenges the notion that, with the help of modern computer and telecommunication technologies, we can look forward to life in a well-educated society in which anybody with even a modicum of intelligence and discipline can enjoy a more than comfortable existence. In a relatively jargon-free economic and political analysis, Perelman reveals how the efforts of business to profit from the sale of information will result in the reduction of access to information, rather than the increase. He demonstrates how the treatment of information as a commodity will cause it to be more regulated and less accessible. In the future, Perelman argues, it will still become a class-based privilege to access and afford information, and the rights of individuals will disintegrate as the power of the corporate sector grows. Class Warfare in the Information Age is a refreshingly critical work that forces readers to rethink the conventional hype surrounding the [[information superhighway]].

[[Information access]].

