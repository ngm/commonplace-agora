# Eco-modernism

"take over the machine, don't turn it off"

> Huber’s [[Climate Change as Class War]] has so far been the apogee of the eco-modernist position in a debate that has done much to further the discussion of desirable post-capitalist futures. The book’s emphasis on class struggle, thinking at scale, the state as a terrain of struggle, and the dynamics of transition are valuable contributions. Huber’s Sidecar essay reiterates many of these themes, stressing the need to imagine a green transition rooted in a Marxist study of the ‘historical economic conditions’, rather than abstract utopian speculation.
> 
> &#x2013; [[The Great Unfettering]]

<!--quoteend-->

> For left eco-modernists, the climate crisis is irresolvable under capitalism not because of ‘growth’ but because the law of value dictates investment decisions. If something isn’t profitable, it isn’t pursued. Under socialism, all kinds of technologies and ecological projects that are currently off the table would become possible. The high fixed-capital costs of nuclear power, for example, deters investment by private capital, but a workers’ state freed from the profit motive could invest the time and labour needed to make mass nuclear energy a reality and drive down emissions.
> 
> &#x2013; [[Forget Eco-Modernism]]


## Criticisms

> The very embrace of the ecomodernist label by a marxist is itself remarkable, given that ecomodernism is so clearly a case of bourgeois ideology.
> 
> &#x2013; [[First and Third World Ecosocialisms]]

<!--quoteend-->

> Heron charges ecomodernist marxism with inattention to imperialism, including its ecological dimensions.
> 
> &#x2013; [[First and Third World Ecosocialisms]]

