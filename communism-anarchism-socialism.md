# Communism, anarchism, socialism

The connection between socialism, anarchism and communism is confusing.

I take socialism to be the broader term, and communism and anarchism to be examples of revolutionary socialism.

But sometimes you hear socialism as being the precursor stage to communism. I think this is from Marxism-Leninism.

