# Data trust

> These new public and cooperative platforms would not engage in the same surveillance capitalist business models, where all human interactions, engagement, and activity across the platform is turned into data that can be bought, sold, and used to modify individual and group behavior, as private platform corporations. Rather, they should be connected to, and interact with, a network of public data trusts (organized sectorally or by location). These democratically governed data trusts would act as custodians over specific data sets, ensuring that they are collected and used safely and in accordance with what is collectively agreed upon to be in the public interest
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

