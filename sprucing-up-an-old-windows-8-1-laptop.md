# Sprucing up an old Windows 8.1 laptop

I'm honestly surprised by how crappy the Windows 8.1 experience is.  The number of things that **just don't work**.  I can't create a new user.  I can't turn on Family Safety.  It just comes back with random errors.  Honestly - I'm not a blind partisan, I am always pragmatic.  But Linux is much better than this, and has been for years.


## Adding a new child user

"We're sorry but something went wrong.  This user wasn't added to this PC.  Code: 0x800704c7."

I just. want. to. add. a. new. user.  WTF.


## Turning on Family Safety

I click "Manage settings on the Family Safety website".

Every time, I get the error "Sorry, the service isn't available at the moment.  Please try again later."

https://answers.microsoft.com/en-us/windows/forum/all/service-unavailable-error-when-manage-online/414e4f1d-ff27-49f2-8a2d-f557dec2d5fc

Some guy here says reinstalling Windows made it work again:https://answers.microsoft.com/en-us/windows/forum/all/error-message-family-safety-service-is-not/0788c51b-81b1-4509-88d3-c3ba587ae41e

> I heard this on another post and it WORKED for me too!  I logged into my live.com account and udated my name in my profile. 

<!--quoteend-->

> My steps:
> 
> 1  Log onto Windows Live - www.live.com
> 
> 2  Top right corner where you see your name or email click and choose account settings
> 
> 3  Basic Info tab: Top is "Display Name" choose edit (mine was blank) and rename.
> 
> 4  I logged out and went to https://familysafety.live.com and was able to log on.

live.com redirects to outlook.live.com these days.

I've run `DISM.exe /Online /Cleanup-image /Restorehealth`

I've run `sfc /scannow`.


## Creating an account via the web

![[Creating_an_account_via_the_web/2022-12-13_19-53-59_screenshot.png]]

