# Participatory economics

Formulated by [[Michael Albert]] and [[Robin Hahnel]].


## Basic principles

(summarised at beginning of [[Robin Hahnel on Parecon (Part 1)]])

-   self-governance through democratic councils of workers and consumers and their respective federations
-   jobs balanced for empowerment, desirability and caring labour
-   remuneration according to sacrifice or effort as judged by coworkers
-   economic coordination through a participatory planning procedure


## Comparison with capitalism

-   https://participatoryeconomy.org/comparison/


## Resources

-   https://participatoryeconomy.org/

