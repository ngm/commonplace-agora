# Global Warming Policy Foundation

> a prominent publisher of material questioning the consensus on climate science in the UK since it was launched by the former cabinet minister and long-time climate denier Nigel Lawson in 2009 .
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

