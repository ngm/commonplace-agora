# Sam Moore of The Radical Open Access Collective

A
: [[podcast]]

URL
: https://david-bollier.simplecast.com/episodes/sam-moore-of-the-radical-open-access-collective-lEnWaSDb

Series
: [[Frontiers of Commoning]]

[[Radical Open Access Collective]]

> [[Open access]] is a term used to describe academic books, journals, and other research that can be freely copied and shared rather than tightly controlled by large commercial publishers as expensive, proprietary product. Over the past 20 years, this vision has fallen far short of its original ambitions, however, as large publishers have developed new regimes to control the circulation of scientific and scholarly knowledge and charge dearly for it. Since 2015, the Radical Open Access Collective has been championing experimental, noncommercial and commons-based alternatives. In this interview, Sam Moore, an organizer of the Collective, takes stock of the state of open access publishing.

