# How We Show Up with Mia Birdsong

URL
: https://sites.libsyn.com/435210/how-we-show-up-with-mia-birdsong


00:23:56: Discusses the relationship between individual and collective freedom - reminds me of similar ideas in [[Anarchist Cybernetics]] ([[collective autonomy]]) and [[Free, Fair and Alive]] ([[Ubuntu Rationality]]).

