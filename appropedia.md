# Appropedia

> Appropedia is the site to develop and share collaborative solutions in sustainability, poverty reduction and international development through the use of sound principles and [[appropriate technology]], original research and project information. 

