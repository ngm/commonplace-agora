# Karl Polanyi

> best known for his book [[The Great Transformation]], which questions the conceptual validity of self-regulating [[markets]].
> 
> &#x2013; [Karl Polanyi - Wikipedia](https://en.wikipedia.org/wiki/Karl_Polanyi)

