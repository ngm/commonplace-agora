# Explained: How are the Myanmar protests being organized?

An
: [[article]]

URL
: https://indianexpress.com/article/explained/explainer-how-are-the-myanmar-protests-being-organized-7181610/

Year
: [[2021]]

[[Myanmar]].

> “This movement is leaderless — people are getting on the streets in their own way and at their own will,” said Thinzar Shunlei Yi, a prominent activist.

<!--quoteend-->

> Shortly after the return to direct military rule — which Myanmar experienced for five decades until 2012 — a Facebook page titled “Civil Disobedience Movement” started issuing calls for peaceful protests. The page now has more than 230,000 followers and hashtags associated with it are widely used by Myanmar Twitter users.

[[Networked protest]].

> One of the biggest challenges for protesters has been the military’s attempts at blocking communications.
> 
> Authorities first went after Facebook — which has more than 22 million users in Myanmar, or 40% of the population — but people simply moved to other platforms like Twitter.

<!--quoteend-->

> Making the rounds have been copies of safety protocol information sheets, some of them originally from Hong Kong, with instructions on how to encrypt communications and how to stay safe during protests.

<!--quoteend-->

> Over the weekend the military temporarily cut internet access and some phone services. Protestors were quick to adapt, with some even using phones registered in neighboring Thailand.

<!--quoteend-->

> “Even when the internet was completely cut off on Saturday for 24 hours, people were able to communicate within Myanmar by phone and SMS,” said Clare Hammond, a senior campaigner the rights group Global Witness.

<!--quoteend-->

> For some who don’t have phone service or internet access during blackouts, word of mouth and simply historical precedent has brought them to protest sites, many of which are the same as in previous uprisings against military rule.

