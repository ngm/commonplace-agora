# World Economic Forum

> Davos is one of my favorite parties. The World Economic Forum, held every year at the end of January. It’s touted as an international gathering of power-brokers, those “stateless elites” who come to congratulate themselves and talk about how their plans for the future will make everything all right, especially for the elites themselves, who sometimes get called “Davos Man,” that newly emergent subspecies of Homo sapiens, eighty percent male and in the top ten percent of the top one percent when it comes to personal wealth among other attributes.
> 
> &#x2013; [[The Ministry for the Future]]

