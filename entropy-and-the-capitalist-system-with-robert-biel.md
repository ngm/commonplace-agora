# Entropy and the Capitalist System with Robert Biel

A
: [[podcast]]

URL
: https://cosmopod.libsyn.com/entropy-and-the-capitalist-system-with-robert-biel

Series
: [[Cosmopod]]

Featuring
: [[Robert Biel]]

Seems like a very interesting chap.

Discussing the book [[The Entropy of Capitalism]].

[[Systems theory]], [[Capitalism]], [[Socialism]], [[Agroecology]].

-   Talks about what drew him into systems thinking.
    -   Mentions [[Edgar Morin]] for contributions to application of systems thinking and cybernetics to left-wing thought.
    -   Mentions that the field of [[international relations]] is very influenced by systems theory.
    -   [[Agroecology]] was an inspiration in [[systems thinking]] for Biel.

-   Talks about what entropy means in the context of his book.
    -   Two meanings.
    -   One: the linear flow of fossil fuels and raw materials into greenhouse gases and garbage.
    -   Two: the decay of the structure of the structure of capitalism itself.
    -   Something that links the two is that they are both in a sense related to the destruction of quality.

Talks about [[extractivism]].

Talks about importance of [[indigenous struggles]].

Talks about importance of [[care]].

References favourably [[The Tao of Physics]] and [[Fritjof Capra]].

References favourably [[Dialectics of Nature]].

References favourably [[Gaia theory]].

Talks about agroecology and [[agroforestry]].

Fundamental argument in the socialist movement is that capitalism is destroying human capacity.  Stunting creativity.  A resource.  Both individually and socially.  We should be nurturing forces which create networks which further capacities of self-organisation.  Means intersectionality and diversity is important.

Need an institutional structure which allows this - adaptive and capable of creative destruction.

Emergence in the human context allows for some intentionality and viosining.

Mentions that though obviously a pioneer of systems thinking, [[Donella Meadows]] presented some problematic ideas (e.g. on social housing) through the lens of systems thinking.  He mentions [[The Limits to Growth]] and the [[Club of Rome]] as coming from a kind of green capitalist angle.

Some of what he said around avoiding a helmsman of a system, and enabling a system to control its own direction, reminded me of [[Anarchist Cybernetics]] and [[Viable system model]].

"Imagination infrastructuring"

How entropy of capitalism connects to decline of the west.

Talks about how might the left dialogue with green trends _within_ capitalism. Interestingly, both have looked at ways of using systems theory.

-   Mention Carolyn Merchant's [[The Death of Nature]] as a big influence.

-   References [[panarchy]] favourably.

-   References [[Michael Perelman]], [[John Bellamy Foster]] favourably.

-   Activist solidarity. Scholar-activism.  [[Aziz Choudry]].

