# Elinor Ostrom's Rules for Radicals

A
: [[book]]

Author
: [[Derek Wall]]

Looks at the work of [[Elinor Ostrom]] and how it might apply to radical politics today.

Topics: [[commons]], [[ecology]], [[deep democracy]], [[feminism]] and [[intersectionality]], trust and cooperation, research and education, institutional transformation, and a Marxist critique of Ostrom’s work. 

-   https://doubleloop.net/2018/02/06/thoughts-starting-elinor-ostroms-rules-radicals/


## [[Thoughts on Ostrom's Rules for Radicals, Chapter 1]]


## [[Thoughts on Ostrom's Rules for Radicals, Chapter 2]]

