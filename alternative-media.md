# Alternative media

> At least since the [[Levellers]], the egalitarian populist movement of mid-17th century England, who pioneered the use of pamphlets as a means of communication, radical political groups have always valued the production of media.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Often, these media have been characterised as ‘alternative media’. They are seen as separate from mainstream media, like television networks and large newspaper publishers, and are intended to communicate a group’s political message to the public.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Indeed, Jeppesen et al. define anarchist or antiauthoritarian media collectives as those that ‘establish economic and organizational forms that prefigure cooperative futures and build strong relationships with broader social movements while simultaneously creating counterhegemonic content and counterpublics around interlocking
> 
> &#x2013; [[Anarchist Cybernetics]]

