# Carbon capture and sequestration

> Carbon capture and storage (CCS) is the name given to technologies that take carbon dioxide from major sources, such as fossil fuel power plants, and then liquefy the gas to be pumped underground for long-term storage, for instance in depleted oil or gas fields.
> 
> &#x2013; [Final warning: what does the IPCC’s third report instalment say? | Climate cr&#x2026;](https://www.theguardian.com/environment/2022/apr/04/final-warning-what-does-the-ipcc-third-report-instalment-say)

<!--quoteend-->

> carbon capture and sequestration (CCS), which removes carbon dioxide directly at the source, as it’s emitted from power plants, which critics say does little more than justify extending the life of dying fossil fuel facilities.
> 
> &#x2013; [[World's Largest Direct Air Carbon Capture System Goes Online]]

<!--quoteend-->

> The carbon that’s removed from these plants is frequently drilled into old oil wells to simulate further production—and the technology fails more often than not. (A recent study of 263 CCS plants undertaken between 1995 and 2018 found that most of them flopped or failed.)
> 
> &#x2013; [[World's Largest Direct Air Carbon Capture System Goes Online]]

[[Carbon capture]]

> the 2000s, BECCS’ simpler technological forebear ‘carbon capture and storage’ (CCS) flopped because carbon prices were far too low to justify the installation costs
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Such compensation strategies lead to an explosion in land demand. Industrial forests and plantations for CO2 sequestration come into competition with food production and thus aggravate hunger.
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

