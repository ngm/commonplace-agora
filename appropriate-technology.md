# appropriate technology

> Appropriate technology is a movement (and its manifestations) encompassing technological choice and application that is small-scale, affordable by locals, decentralized, labor-intensive, energy-efficient, environmentally sound, and locally autonomous
> 
> &#x2013; [Appropriate technology - Wikipedia](https://en.wikipedia.org/wiki/Appropriate_technology)

[[E. F. Schumacher]].

See also: [[Convivial Tools]].  


## Flashcard


### Front

What is appropriate technology?


### Back

A movement (and its manifestations) encompassing technological choice and application that is:

-   small-scale
-   affordable by locals
-   decentralized
-   labor-intensive
-   energy-efficient
-   environmentally sound
-   and locally autonomous

