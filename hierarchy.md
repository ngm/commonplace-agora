# hierarchy

> Hierarchy is emergent, not imposed from above. This is why we see hierarchy in nature—central nervous systems, keystone species, cells, organs, and organisms—even though there is no boss of nature.
> 
> However, a hierarchy, once established, tends to find ways to perpetuate itself.
> 
> &#x2013; [[Fragments: vertebrate technology]]

