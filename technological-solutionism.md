# technological solutionism

Blind faith in the ability of technology to solve problems.  Usually absent of any reflection on whether technology may have contributed to the problem in the first place.  Usually imposed unilaterally by someone with a vested interest in the technology, bypassing any democratic discussion.

> In the technological solutionism, the technical solution provided does not address the social, cultural, or economic reason of a problem, and therefore has limited capacity to solve it.
> 
> https://digitalcapitalismcourse.tni.org/mod/quiz/review.php?attempt=1096&cmid=8

