# offline by default



## Why

Primarily about being free of distraction. And using less power is nice too.

It's more about fulfilling my intention. It's not specifically about being offline for the sake of it. But it does, for me, help fulfil my intention. Just gives me a more focused mind in a lot of cases.  Less scattered. Ploum said it well here:

> When you are online, your brain knows that something might be happening, even without notification. There might be a new email waiting for you. A new something on a random website. It’s there, right on your computer. Just move the current window out of the way and you may have something that you are craving: newness. You don’t have to think. As soon as you hit some hard thought, your fingers will probably spontaneously find a diversion
> 
> &#x2013; [[The computer built to last 50 years]]


## How

Something like:

-   when online, save and sync things that I'm interested in to places where I can read them offline
    -   e.g. wallabag
    -   e.g. share social media posts to orgzly
-   when I'm offline, write responses in orgzly or in handwritten notes
-   when online again, sync these responses

