# Algorithms



## What's an algorithm?

> An algorithm is simply a series of logical instructions that show, from start to finish, how to accomplish a task. By this broad definition, a cake recipe counts as an algorithm.
> 
> &#x2013; [[Hello World]] 

<!--quoteend-->

> Usually, algorithms refer to something a little more specific. They still boil down to a list of step-by-step instructions, but these algorithms are almost always mathematical objects.
> 
> &#x2013; [[Hello World]] 


## Types of algorithm

There's no overall consensus on how to group algorithms, but you can think of four main categories ([[Hello World]]): 

-   [[prioritization]] (making an ordered list)
-   [[classification]] (picking a category)
-   [[association]] (finding links)
-   [[filtering]] (isolating what's important)

Most algorithms are some combination of the above.


## Algorithmic approaches

You can think of the approaches taken by algorithms as broadly fitting into two key paradigms.

-   [[Rule-based algorithms]]
-   [[Machine-learning algorithms]]

