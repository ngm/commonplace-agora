# EDRi

URL
: https://edri.org/

> The EDRi network is a dynamic and resilient collective of NGOs, experts, advocates and academics working to defend and advance digital rights across the continent. For over two decades, it has served as the backbone of the [[digital rights]] movement in Europe.

