# Corralling a chorus of voices

An [[emergent outline]].

-   I think that the [[IndieWeb]] wiki would be a great case study for examining the claim that [[Combining multiple personal wikis provisions a better commons than one central wiki]].
    -   There's multiple different voices in the community, sometimes agreeing, sometimes dissenting, and the wiki is an attempt to capture those views.
    -   IndieWeb wiki is essentially attempting to corral a chorus of voices in to one space.
    -   I occassionally see a criticism of something IndieWeb-related where the critiquer says something like 'I think IndieWeb is bad because they think X' and point at something in the wiki, with the underlying assumption being that everyone in the community thinks that.
    -   The IndieWeb wiki sometimes gives the illusion of a unified opinion where it doesn't always exist.
    -   The IndieWeb wiki seems like a good candidate for the distributed [[chorus of voices]] approach to a knowledge commons.
-   Perhaps the P2P Foundation wiki would be / would have been a good case study too.  Given that it's supposed to be peer-to-peer and all.

Don't forget this quote:

> Once again, the promise of a knowledge commons is best made evident in the disagreements and difficulties in determining who and how it should be managed
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

