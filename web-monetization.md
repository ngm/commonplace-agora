# Web Monetization

> it's a decentralized system that eliminates the middlemen, allowing consumers to pay Web content creators directly. No capitalists taking a cut of the earnings. 
> 
> &#x2013; https://social.coop/@datatitian/106063606616603432

