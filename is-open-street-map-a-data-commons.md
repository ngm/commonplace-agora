# Is Open Street Map a data commons?

Is [[Open Street Map]] a [[data commons]]?

It uses the [[Open Data Commons Open Database License]], so that's probably a hint.  Although that license feels more about open data than full-on commoning.

So what does it mean to be a data commons?

"data of the people, by the people, for the people" (https://datacommons.barcelona/en/)
"software platform along with a governance framework [&#x2026;] allow a community to manage, analyze and share its data" ([3 Key Steps to a Successful Data Commons - data.org](https://data.org/guides/3-key-steps-to-a-successful-data-commons/))

Seems like OSM does tick those boxes.

-   [X] data of the people - it's a map of the things in the world of people
-   [X] data by the people - it's collected by people
-   [X] data for the people - it's openly licensed to allow anyone to use it

I'm not completely sure what the governance of it is.  But it looks pretty open and community-oriented from what I've seen of various wikis and forums that I've stumbled on.

It has many excellent software platforms for editing and viewing the data.

So yeah, from my limited knowledge, it certainly feels like a data commons.

How would the [[Spheres of commoning]] apply?  Social life, governance, and provisioning?

