# Contribute Freely

> Contribute freely means giving without expecting to receive anything of equivalent value, at least not here and now. It also means that when people do receive something, it is without feeling the need to reciprocate in direct ways.
> 
> &#x2013; [[Free, Fair and Alive]]

