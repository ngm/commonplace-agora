# Arctic permafrost

> Arctic permafrost contained as much stored methane as all the Earth’s cattle would create and emit over six centuries, and this giant burp, if released, would almost certainly push Earth over an irreversible tipping point into jungle planet mode, completely ice-free;
> 
> &#x2013; [[The Ministry for the Future]]

