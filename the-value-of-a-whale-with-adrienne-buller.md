# The Value of a Whale with Adrienne Buller

A
: [[podcast]]

URL
: https://www.gndmedia.co.uk/podcast-episodes/the-value-of-a-whale-with-adrienne-buller

> The Worlds economy is fundamentally intertwined with the environmental and with that climate breakdown. fastidious bankers, hedge fund managers and capitalists are on the case trying to tackle the end of the world with the only tools they know how to use, finance and profit. So what happens when models of extraction are used to try and fix models of extraction?

Discussing the [[The Value of a Whale]].

A critique of [[green capitalism]],  [[ecosystems services]], [[natural capital]], all those market-led 'solutions' to the problem of the [[climate crisis]].

Attempting to bring the 'externalities' of nature in to the market as internalities by giving them a price.

