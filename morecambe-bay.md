# Morecambe Bay



## Heysham

![[photos/heysham-promenade.jpg]]

The promenade at Heysham.  Blue sky with clouds, flat sea, and concrete promenade, stacked on top of each other.  The tide was in.


## Ulverston


### Purple small-reed

August 2022.  I love the colour that a whole group of these make together.

![[photos/purple-small-reed-brick-kiln-road.jpg]]

