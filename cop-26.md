# COP 26

[[COP]] 26.  In Glasgow.  November 2021.

First '5 years after [[COP 21]]' stocktake on how commitments to [[Paris Agreement]] are coming along.

> Rather than being oriented toward a just transition, COP26 perpetuated [[imperialist]] and fossil capitalist interests.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> The geopolitical and energy transition COP imagines benefits the imperialist powers, not most of the planet.
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

<!--quoteend-->

> Second, the US, EU, UK, and Australia removed the [[loss and damage]]s facility from the final text of the Glasgow agreement. 
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

