# Utopia

> Socialist [[science fiction]] writer [[Kim Stanley Robinson]] (2018a) argues that the difference between utopias and dystopias is that “utopias express our social hopes, dystopias our social fears”. He argues that both utopias and dystopias often have an ideological character. Ideological dystopias tend to communicate that nothing can be changed and therefore to advance defeatism. Robinson therefore argues for the dialectical mediation of dystopias with socialist utopias: These days I tend to think of dystopias as being
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Abstract utopias outline impossible being. They remain stuck in “dreaminess” (1995, 146) and are “world-blind hope” (1995, 1039). Concrete utopias outline the not-yet of society, being that is possible but does not yet exist. 
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> Concrete Utopias give grounds for “militant optimism” (146) and the “active hope” (241; 1197) of class struggle.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> The work at hand reads and interprets concrete utopian elements in the analysed communist writings in the light of the concrete potentials that the means of communication and digital technologies pose in the 21st century. Through an engagement with communist fiction it identifies
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

