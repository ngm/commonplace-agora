# Becoming Organizers

A
: [[podcast]]

URL
: https://roarmag.org/2020/12/15/introducing-spadework-a-podcast-on-movement-building/

Series
: [[Spadework]]

The bit about the need for [[collective memory]] for political organisations seems like a potential use case for [[Agora]].

Good distinction made between analysis and skills.  You can have the most radical political analysis out there, but ain't gonna get you nowhere without some on-the-ground skills.

