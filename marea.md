# MAREA

> It is currently the highest-capacity submarine fiber-optic cable in the world. It is co-owned by Microsoft, Facebook, and a subsidiary of the Spanish telecom Telefónica, which has leased a portion of its capacity to Amazon
> 
> &#x2013; [[Internet for the People]]

