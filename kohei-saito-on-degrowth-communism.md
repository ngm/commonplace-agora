# Kohei Saito on Degrowth Communism

A
: [[podcast]]

URL
: https://futurehistories-international.com/episodes/s02/e55-kohei-saito-on-degrowth-communism/

Series
: [[Future Histories]]

[[Kohei Saito]].  [[Degrowth communism]].

Saito says something like: degrowth needs communism, communism needs degrowth.

Talks about [[Marx's theory of metabolism]] and [[metabolic rift]].

Here's a transcript: https://github.com/autonompost/podcasts-transcriptions/blob/main/podcasts/futurehistories/transcripts/115-2023-09-17-S02E55-Kohei-Saito-on-Degrowth-Communism.txt

