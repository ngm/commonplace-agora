# Cooperative ownership structures



## [[Worker cooperatives]]

> Worker-owned: the workers of the co-operative collectively own and manage the organisation. This could involve close collaboration in the day-to-day management of the collective or allowing a group of individuals to run the platform while workers gain increased benefits from their work due to their co-ownership of the platform.
> 
> &#x2013; [[The Co-operativist Challenge to the Platform Economy]] 


## [[Producer cooperatives]]

> Producer-owned: producers of the goods or services sold on the platform collectively own and manage the organisation. Producers of products such as music, photography and household goods can use a shared platform to pool resources and benefit from network effects without necessarily collaborating in the design and marketing of their products.
> 
> &#x2013; [[The Co-operativist Challenge to the Platform Economy]] 


## [[Consumer cooperatives]]

> Consumer-owned: businesses owned and managed by consumers with the aim of fulfilling their needs. There are many examples of non-digital consumer collectives from credit unions and electricity co-ops to food co-ops. A number of data co- operatives have emerged in which individuals pool their data to form a trust to be controlled democratically by its members.
> 
> &#x2013; [[The Co-operativist Challenge to the Platform Economy]] 


## [[Multi-stakeholder cooperatives]]

> Multi-stakeholder: An umbrella term which incorporates a range of different co-operatives that include workers, users, founders, service providers and broader community members as part of their ownership and governance structure. An example is [[Resonate]], a stream-to-own music platform in which artists (45%), listeners (35%) and workers (20%) all have a stake in the co-operative.
> 
> &#x2013; [[The Co-operativist Challenge to the Platform Economy]] 

