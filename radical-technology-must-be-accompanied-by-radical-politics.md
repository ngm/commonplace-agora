# Radical technology must be accompanied by radical politics

> As the planet slides further toward a potential future of catastrophic climate change, and as society glorifies billionaires while billions languish in poverty, digital technology could be a tool for arresting capitalism’s death drive and radically transforming the prospects of humanity. **But this requires that we politically organize to demand something different.**
> 
> &#x2013; [[Future Histories]] 

<!--quoteend-->

> Technology alone will not create the society we want; at a more fundamental level, we have to learn to work together.
> 
> &#x2013; [Protocol Cooperativism? | Platform Cooperativism Consortium](https://platform.coop/blog/protocol-cooperativism) 

