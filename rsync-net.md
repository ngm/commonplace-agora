# rsync.net



## <span class="timestamp-wrapper"><span class="timestamp">[2021-02-13 Sat]</span></span>

Bit disappointed they send a password in your first email.  I mean, I know there's nothing there yet, but come on guys.

> This email has been sent in clear text, so you should immediately change your password.  


## <span class="timestamp-wrapper"><span class="timestamp">[2021-02-21 Sun]</span></span>

Also, I couldn't connect to my backup storage.  They sorted it out.  Apparently there was a DNS error.  Not sure what to think.

The file transfer seems very slow.

