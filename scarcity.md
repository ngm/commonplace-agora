# Scarcity

> The "scarcity" of oil, land and water may seem self-evident, but, in fact, the term does not reflect any inherent property of a resource.  Oil, land, or water are merely finite.
> 
> &#x2013; [[Free, Fair and Alive]]

