# mutualization

> Mutualizing means to contribute and belong to a group enterprise with a larger, enduring social purpose; this association in turn entitles participants to specific individual benefits. However, members do not necessarily receive equal value or the same benefits in return for what they give, as in a market transaction. They typically receive some stipulated benefit based on need or other criteria. The benefits of mutualization are socially agreed upon, often based on differential shares and predetermined formulas.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> An insurance pool and social security fund are classic examples.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> However mutualization is structured, it is critical that everyone with a stake in the mutualized pool have a say in the agreement. It is a peer-determined reciprocity, a specific form of practicing Gentle Reciprocity.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Mutualization bears some resemblances to a commercial transaction. What makes it different is that participants generally have an interest in each other and goals that are not just monetary.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> mutualization is socially driven reciprocity; trading is a market-based reciprocity
> 
> &#x2013; [[Free, Fair and Alive]]

