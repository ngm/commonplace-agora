# Ways to reclaim the stacks

See also:

-   [[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]] for an article I wrote on this topic.
-   [[Reclaim the stacks]] for an index to other pages about this.

Here I'm going to list various ways from various places that I've seen.  They may be better or worse options for an ecosocialist ICT - I'll just list them all out to begin.

Further down the line I'll reorganise and systematise (it would be nice to get it in to some kind of a pattern language maybe?).  I want to particularly bear in mind: "[some] describe transitional steps in the struggle for ecosocialism, but these usually involve a wish list of things we want and hope to achieve in the near-term (e.g. “immediately” eliminating military spending, or an immediate “moratorium on economic growth in rich countries”), rather than how we might do so and the conditions that would make this possible" &#x2013; [[Ecosocialiism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]

So maybe there's a few ways of slicing it e.g.

-   domains
    -   e.g. agency, social equity, planetary stability
    -   e.g. sovereignty, democracy, insurgency from governable stacks?
-   leverage points
    -   e.g. Rules, Structures, Goals and Mindsets from Digitalization and the Anthropocene
    -   e.g. Resist, Regulate, Recode from Platform Socialism?
-   radicalism
    -   e.g. status quo, reform, and transformation?
-   position in LES model (direct, behavioural, systemic)
-   abstract or real utopianism
-   which parts of the stack does it relate to?
-   etc

Maybe the 'areas of action' can be common, but there will be differing levels of radicalism regarding the specific initiatives within those.


## [[Digitalization and the Anthropocene]]

See Table 3 of the paper.

They split it up into three different 'domains' - [[agency]], [[social equity]], and [[planetary stability]].

For each domain, they list a number of main areas for action, along with solution space options, and examples and initiatives.  Not sure exactly what the different between solution options and examples are - and how they decided what are the 'main areas of action'.

Some of it seems a bit vague from the table - but they do explain more in the text in some cases, and also reference out to papers that cover some of the things in more detail.


### Planetary stability


#### Regulations and circular economy practices

"[[Regulations]] and [[circular economy]] practices to avoid pressures on natural resources, including carbon sinks and mining pollution."

Solution space options:

-   durable designs
-   legislation against planned obsolescence
-   redesigned value chains for closed-loop production
-   zero e-waste circular economy

Examples and initiatives:

-   laws mandating durability and repair
    -   [[right to repair]]
-   targets for recycled content in new electronic products
-   electronics as a service


#### Energy proportionality test

"Energy proportionality test for comparing digital applications incurred energy and emissions"

Solution space options:

-   Promoting digital efficienty of end-users
-   Leaner code possibilities

Examples and initiatives

-   Regulators and authorities developing guidance on energy proportionality
-   Pay-for-performance programs, including digital options


#### Open data commitments

"Open data commitments to measure and manage rebound and intensification effects"

Solution space options

-   use of open-source datasets by regulators in the scope of their social license to operate

Examples and initiatives

-   opportunities for zero-carbon urban spaces, energy infrastructure, and public services


#### Mainstreaming carbon labeling

"Mainstreaming carbon labeling of ICT use and digital services"

Solution space options

-   Best-in-class standards
-   Carbon footprint and life cycle inventories

Examples and initiatives

-   carbon emission rankings
-   user analytics on impact in digital services in science-based targets


### Social equity


#### Regulating data-based monopoly power

"Regulating data-based monopoly power to increase social equity"

Solution space options:

-   Mandating data sharing as a function of market power
-   Taxing away data rents with taxation systems for the digital economy

Examples and initiatives

-   Data sharing obligation
-   Market-based digital service taxes on digital services and presence


#### Improving digital capabilities and governance

"Improving digital capabilities and governance"

Solution space options

-   Strategic investment to address the digital divide

Examples and initiatives

-   Human capital development for digital skills and literacy


### Agency


#### Empowering digital subjects

"Empowering digital subjects to become active agents of their data"

Solution space options

-   Redesigning data infrastructures through data cooperatives
-   Ensuring decentalized and secure private data

Examples and initiatives

-   Data exchange infrastructures
-   Other initiatives to direct digitalization for public purpose


## Platform Socialism

As per the book  [[Platform Socialism]].

I'll have to go back through the book to properly pull these out.  But let's see, in a nutshell.  I remember the breakdown into Resist, Regulate and Recode.  Would you class these as the leverage points maybe?

In terms of domains, it's likely agency and social equity - less focus on planetary stability here.


### Cultivating local cooperative economies

-   [[Platform coops]]
    -   [[Up&amp;Go]]


### Municipal infrastructure for the co-operative economy

-   [[Coòpolis]]
-   [[Mercado Justo]]
-   [[Space4]]
-   [[Wings]]
-   [[public-commons partnerships]]
-   [[BEG Wolfhagen]]


### Building a data commons

-   [[data commons]]
-   [[Data trust]]

[todo: plenty more]


## [[Digital Ecosocialism: Breaking the power of Big Tech]]

Here I'll list the ways they suggest not doing it too.  Makes sense to include but say why not recommended.

Leverage points: education / organisation / agitation?

-   anti-trust regulations (doesn't like / reform)
-   human rights regulations (doesn't like / reform)
-   worker protections (doesn't like / reform)
-   progressive taxation (doesn't like / reform)
-   development of new technology as a public option (doesn't like / reform)


### Phasing out of intellectual property regulation

-   in favor of a commons-based model of sharing knowledge


### Socialisation of physical infrastructure

-   community-run internet service providers
-   community-run wireless mesh netwokrs
-   international consortia for public good for submarine cables


### Public subsidies for production

-   [[British Digital Cooperative]]
    -   [[Popular assembly]]
    -   [[Open data]]
    -   [[Libre software]]
    -   [[Transparent algorithms]]
    -   [[Participatory planning]]


### Socialisation of platforms

-   socialisation of digital intelligence and data

-   ban forced advertising and platform consumerism

-   replace military, police, prisons and national security apparatuses with community-driven safety and security services

-   end the digital divide

-   degrowth


### Building blocks

-   Libre software
-   Creative Commons
-   [[Data commons]] / [[DECODE]]
-   Platform coops / [[Wings]]
-   Alternative social media / Fediverse


### Policy changes

-   interoperability
-   opening up of intellectual property
-   subsidise data hosting


## [[Governable Stacks against Digital Colonialism]]

The examples/actions/initiatives are grouped into areas of Sovereignty, Democracy, and Insurgency.

Are these new domains alongside e.g. Agency, Equity, Planetary Stability?  Sovereignty and Democracy are pretty close to Agency and Equity.  Insurgency could be a new one.  Or perhaps it's a different axis altogether?


### Sovereignty

When the stewards of the thing are the people who depend on the thing.

> What makes technology sovereign is when its stewards are the people who depend on it, protected from outside control by any legal or extra-legal means available. The data, the algorithms, and the interfaces are for their users, rather than acting surreptitiously against them.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]], p.10

-   Examples
    -   state-based
        -   [[Sovereign Cloud Stack]]
        -   government sponsorship of FLOSS (e.g. [[Matrix]], [[Nextcloud]])
        -   taxation on foreign social media platforms
        -   [[digital non-aligned movement]]
    -   non-state-based
        -   tribal broadband lines
        -   cryptocurrenciens
        -   data governance
        -   linguistic autonomy
        -   ethical dependencies in software
        -   cooperatives and assemblies
        -   municipal connectivity
        -   user-owned cooperatives
        -   [[Douglass]]
        -   The Markup privacy tax
        -   [[Archive of Our Own]]


### Democracy

The ability to participate in the governance of the platform.

> The other side of sovereignty is participatory democracy – its guarantor and its every- day practice. Here we resist the temptation of autocratic vanguardism by designing governable stacks to be accountable and alive.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]], p.10

<!--quoteend-->

> Digital democracy has the potential to evolve rapidly and creatively. People can participate in far more rapid and fine-grained ways than was possible when the pre- vailing regimes of corporate and state governance first appeared. Organisational de- signs that work well could become part of a governance commons, enabling other groups to adopt, adapt, and share them back into the common pool (Schneider et al. 2021). In this way, small-scale accountability can spread, and it can creep into larger and larger kinds of communities, demonstrating that colonial control was never neces- sary. The more we demand and practice the arts of self-governing, the harder we are for someone else to govern.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]], p.11

-   Examples
    -   [[Debian]] and its constitution
    -   [[Ethical Source Movement]]
    -   [[Loomio]], [[Decidim]], [[Polis]]
    -   [[Fediverse]]
    -   [[Platform coops]]
    -   [[Cobudget]]
    -   [[Democracy Earth]]
    -   Mexico City's crowdsourced constitution


### Insurgency

Defiance against attempts at rule.

> Insurgents might use colonial platforms for education and organising. They might spread viral messages and enjoy themselves. But if they have governable stacks to go back to, they are more than just subjects. They are maroons, with swamps and forests of their own.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]], p.11

-   Examples
    -   [[Indymedia]]
    -   precursors to Twitter used for street protests
    -   [[May First Technology Movement]] challenging government subpoena's of member data
    -   [[economic disobedience]]
    -   [[Tor]]
    -   [[Copwatch]]
    -   [[Driver's Seat Cooperative]]


## [[Internet for the People]]

Generally, deprivatisation/socialisation of all the things.


### Ownership of infrastructure

-   [[Community broadband]] / [[community networks]]


### Protocolize social media

-   [[ActivityPub]] / [[Fediverse]]


### [[Platform coops]]

-   [[Up&amp;Go]]


### Support from above

-   [[Community wealth building]]


### Innovation from below

-   [[Technology Networks]]
-   [[design justice]]

[todo: plenty more] 


## [[The Telekommunist Manifesto]]


## [[The British Digital Cooperative: A New Model Public Sector Institution]]

-   [[A socialist agenda for digital technology]]


## Misc


#### Infrastructure

-   [[Co-op Cloud]]

