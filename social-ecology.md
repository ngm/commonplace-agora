# Social ecology

-   [[Murray Bookchin]]
-   "The root causes of environmental problems are such as trade for profit, industrial expansion, and the identification of "progress" with corporate self-interest."
-   "The domination of nature by man stems from the very real domination of human by human"
-   "Social ecologists believe that things like racism, sexism, third world exploitation are a product of the same mechanisms that cause rainforest devastation"
-   [[Institute for Social Ecology]]

> social ecology maintains that an ecologically oriented society can be progressive rather than regressive, placing a strong emphasis not on primitivism, austerity, and denial but on material pleasure and ease.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Social ecology is an ecology not of hunger and material deprivation but of plenty; it seeks the creation of a rational society in which waste, indeed excess, will be controlled by a new system of values; and when or if shortages arise as a result of irrational behavior, popular assemblies will establish rational standards of consumption by democratic processes.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> In short, social ecology favors management, plans, and regulations formulated democratically by popular assemblies, not freewheeling forms of behavior that have their origin in individual eccentricities.
> 
> &#x2013; [[The Communalist Project]]

