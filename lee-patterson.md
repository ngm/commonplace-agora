# Lee Patterson

> Lee's artist talk explores how field recording work with hydrophones and contact microphones co-exists and has co-evolved with his live work.
> 
> Lee uses sound making and recording to devise performances with a selection of amplified objects, devices and processes, from rock chalk to springs, from burning nuts to vibrating metal.
> 
> With recorded examples alongside demonstrations of his unique, self built instruments
> 
> energy transferal and as such can be considered as musical instruments in-situ.
> 
> From singing bridges and sonorous railings to synthesising chalk and the chirping, whistling chorus of ponds, he proposes a new, [[weird music]], albeit one hidden within the fabric of our everyday existence.
> 
> https://fonfestival.org/fon_events/lee-patterson-piel-view-18-03-23/

