# Digital parties

Reimagination of the party structure through digital platforms.  Populism via digitality. 
Strategic organising online.

> emerging organisations such as Momentum, the Five Star Movement, Podemos, France Insoumise and many others as “digital parties”, or “platform parties,” [&#x2026;] these formations are transforming themselves, following the logic of the digital platforms of contemporary capitalism and mimicking operations like Facebook, Google, Apple and Amazon.
> 
> &#x2013; [Momentum, Labour and the Rise of the Platform Party](https://www.plutobooks.com/blog/momentum-labour-online-digital-activism-social-media-party/), Paolo Gerbaudo

-   [NovaraFM: Log On! the Rise of the Digital Party](https://novaramedia.com/2019/02/15/log-on-the-rise-of-the-digital-party/)
-   [Momentum, Labour and the Rise of the Platform Party](https://www.plutobooks.com/blog/momentum-labour-online-digital-activism-social-media-party/)
-   [All hail the hyperleaders – the bellicose insurgents using the web to seize control](https://www.theguardian.com/commentisfree/2019/jul/01/boris-johnson-hyperleader-insurgents-web-seize-control)
-   [Building the Brexit party: how Nigel Farage copied Italy's digital populists](https://www.theguardian.com/politics/2019/may/21/brexit-party-nigel-farage-italy-digital-populists-five-star-movement)

