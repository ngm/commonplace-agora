# Querying org-roam

I'd like something like [[org-ql]] for [[org-roam]].

Ideally I can just use org-ql.  My first concern is maybe it won't work so well on the large number of files you have in an org-roam project.

Guess I should just try it and see.


## Tests

```elisp
    (org-ql-query
    :select 'element-with-markers
    :from '("~/commonplace/gift-economies-build-community.org")
     :where '(and (heading "Epistemic status"))
)
```

^ this works

```elisp
(org-ql-query
:select '(org-get-heading t t t t)
:from (org-roam-list-files)
:where '(and (heading "Epistemic status"))
)
```

^ this is incredibly slow.


## org-ql and org-roam related links

-   [GitHub - natask/org-roam-search: org ql-like syntax for org roam search.](https://github.com/natask/org-roam-search) - doesn't look that powerful though, not close to org-ql
-   [alphapapa/org-ql#183 Using org-ql as powerfull filtering tool on top of org-r&#x2026;](https://github.com/alphapapa/org-ql/issues/183) - reyman asks for something similar
-   [alphapapa/org-ql#200 Delaying mode-hooks when opening Org files for searching&#x2026;](https://github.com/alphapapa/org-ql/pull/200) - discussion here about performance issues
-   [org-roam and tags for querying philosophy issues : emacs](https://www.reddit.com/r/emacs/comments/lhqwyw/orgroam_and_tags_for_querying_philosophy_issues/gn1k7pt/)


## Musings

I guess I could lean on backlinks and org-roam's backlink buffer view&#x2026;

