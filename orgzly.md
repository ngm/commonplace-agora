# orgzly

URL
: http://www.orgzly.com/

An awesome app.  One of my most used.  Allows me to view and manipulate my [[org-mode]] todos on my phone.


## Article

-   https://doubleloop.net/go-libre/go-libre-task-management-org-mode-orgzly/
-   [[org-mode and orgzly sync issues]]


## Searches

I lose these sometimes between installations, so documenting here.


### Morning

-   b.<sub>GTD</sub> t.morning

