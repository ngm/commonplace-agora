# European Union



## regulation vs directive

-   'regulation': means that it will be a legal act and enforceable in its entirety across all member states (e.g. GDPR)
-   'directive': allows each member state to introduce its own mechanisms for the law provided they match the spirit of the original directive

