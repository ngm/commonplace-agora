# Testing my org-export customisations with ERT

https://emacs.stackexchange.com/questions/50676/testing-emacs-lisp-code-involving-org-mode

First, for sanity, get a basic test working.

```emacs-lisp
(ert-deftest test-simple-sanity-check ()
  (should (string= "yo" "yo")))
```

Then a basic org-related test working.

\#+begin<sub>src</sub> emacs-lisp
; https://emacs.stackexchange.com/questions/50676/testing-emacs-lisp-code-involving-org-mode
(defun promote-next-heading ()
  (org-next-visible-heading 1)
  (org-metaright))

(ert-deftest test-simple-org-sanity-check ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert "


## test heading

")
      (goto-char (point-min))
      (promote-next-heading)
      (buffer-string))
    "


### test heading

")))
\#+end<sub>src</sub>

Next up is to test `org-export-to-buffer`.  I'll start with the ascii backend.

```emacs-lisp
(ert-deftest test-simple-export-to-buffer ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert "#+TITLE: Title\n")
      (insert "* Test heading")
      (setq org-export-with-author nil)  ; Turn off author name
      (setq org-export-with-toc nil)     ; Turn off table of contents
      (org-export-to-buffer 'ascii (current-buffer))
      (buffer-string))
    "                                _______\n\n                                 TITLE\n                                _______\n\n\n1 Test heading\n==============\n" 
    )))
```

Hmm.  I think it'll be better to test against an expected file output though.

The official org tests should be a good reference
https://git.savannah.gnu.org/cgit/emacs/org-mode.git/tree/testing/lisp

