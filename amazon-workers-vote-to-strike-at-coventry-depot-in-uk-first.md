# Amazon workers vote to strike at Coventry depot in UK first

URL
: https://www.theguardian.com/technology/2022/dec/16/amazon-workers-vote-strike-coventry-depot-uk-first

> [[Amazon]] workers at a vast depot in Coventry are poised to go on strike in the new year, demanding pay of £15 an hour, after securing a historic yes vote in a ballot for industrial action.

<!--quoteend-->

> This time the turnout was 63%, with 98% of those backing strike action, marking the first time Amazon workers in the UK have voted to do so.

<!--quoteend-->

> Amazon says it is unlikely to have an impact on customers because the plant is not a fulfilment centre, dispatching parcels. However, the GMB is hopeful of causing significant disruption.

