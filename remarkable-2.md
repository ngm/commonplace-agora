# reMarkable 2

All the reviews seem to suggest that the 'paper' experience is pretty excellent, for both writing and drawing.

Seems like at the nub of it, it comes down to whether you can justify paying the high price for a nicer and slightly more organised paper experience.  You could get a tablet for cheaper.  So do you view the lack of additional functionality and distraction as a feature not a bug.  

I like the long battery life idea though, and the distraction free idea.

> If you want to watch videos, play games or use apps, buy an iPad. But if you just want to draw all day long and work on the notes for your novel, buy the ReMarkable 2.
> 
> &#x2013; [ReMarkable 2 tablet review | TechRadar](https://www.techradar.com/uk/reviews/remarkable-2-tablet) 

<!--quoteend-->

> I want a device that feels like paper and lets my organise all my notes into folder, so I dont have to flick through 200 pages to find the notes I took on a particular subject back in February. 
> 
> &#x2013; comments, https://www.wired.com/review/remarkable-2/

You can get refurbished ones, which is nice.  Just the ones people have returned in the 100 day cooling off period.

Dead pricey when you add the accessories, you're getting up towards £500.


## Reviews

-   [ReMarkable 2 Review: Great for Taking Notes, But Not Much Else | WIRED](https://www.wired.com/review/remarkable-2/)
-   [reMarkable 2 review: an ambitious attempt at replacing paper - The Verge](https://www.theverge.com/21403489/remarkable-2-e-ink-paper-tablet-review)

