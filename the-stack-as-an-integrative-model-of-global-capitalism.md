# The Stack as an Integrative Model of Global Capitalism

Year
: 2022

Author
: Likavčan, L. and Scholz-Wäckerle, M.

URL
: https://www.triple-c.at/index.php/tripleC/article/view/1343

DOI
: https://doi.org/10.31269/triplec.v20i2.1343

Journal
: [[Communication, Capitalism &amp; Critique]]

Pretty dense, but worth it.  Long description of '[[the Stack]]' as an evolution of capitalism.  Felt a lot like [[Vectoralism]], though the authors don't mention it.  The payoff is at the end, when they describe possible places to intervene - which are quite heavily [[commons]]-based. And they reference [[Governable Stacks against Digital Colonialism]] as another possible intervention.

Seems to lean heavily on concepts from the book [[The Stack]].

> This article investigates recent transformations in global capitalism’s political economy as it relates to the evolution of globally integrated production and exchange apparatuses,

<!--quoteend-->

> Focus is thereby given to intensified real abstraction of labour induced by the capitalist appropriation of [[planetary-scale computation]], and the associated rise of platform sovereignty in opposition to the traditional sovereignties of states and markets.


## Introduction

> The organisational affordances of planetary-scale computation have made capitalist production and exchange more integrated on a global scale.

<!--quoteend-->

> aim of the article is to insert planetary-scale computation into a longue durée perspective of capitalist evolution, and to introduce the concept of the Stack as an integrative model of global capitalism.

Bit like [[Vectoralism]]?

> to sovereignty, we indicate that global capitalism is influenced by a novel kind of platform sovereignty that is regulating hegemony in cyberspace. SiliconValley-based platforms in particular have gained an intellectual monopoly that is subsuming the [[general intellect]] with the acquisition of user-based data.


## Planetary-Scale Computation and Capitalist Evolution

> Fuchs (2014) has identified and described the current transformational tendency of capitalist evolution as “transnational informational capitalism”

<!--quoteend-->

> Although capitalist evolution is shaped by co-existing varieties of capitalism (as well as non-capitalist agencies and structures), there is always a dominating regime, a global hegemon serving as an attractor in terms of accumulation and exchange.

<!--quoteend-->

> We are particularly interested in the latest US hegemonic systemic cycle of accumulation, whose material expansion relates to the period of “Fordism” (Gramsci 1971, 277ff.). In geographical terms, this expansion

<!--quoteend-->

> A world economy, that is an economy in which capital accumulation proceeds throughout the world, has existed in the West at least since the sixteenth [century], as Fernand Braudel and Immanuel Wallerstein have taught us. A global economy is something different: it is an economy with the capacity to work as a unit in real time on a planetary scale

<!--quoteend-->

> exploitation of environment and labour on behalf of unequal exchange (Suwandi 2019; Ricci 2019; Patel and Moore 2017; Hickel et al. 2022). PostFordism, far from representing a ‘postindustrial society’, thus appears as the period in which all branches of the economy are fully industrialised for the first time, to which one could further add the increasing automation of the sphere of circulation.


## The Model of the Stack and Its Complex Dynamics

> Such a definition allows Bratton to claim that alongside states (as traditionally centralising organisational forms) and markets (as traditionally decentralising organisational forms), platforms represent new institutional economic forms of resource, commodity, and service allocation

If platforms are alongside states and markets, maybe commons-based platforms/stacks could function as an alternative.

> The platforms’ novel institutional form enables a new kind of sovereignty: platform sovereignty (Bratton 2015, 21-22). Platforms are capable of drawing frontiers, creating their own sovereign territories and dividing the space of “deep address” into parcels of private property.

<!--quoteend-->

> Second, we argue that the model must be extended in political economic terms in order to yield insights into the nature of contemporary capitalist evolution. Its design is promising in mirroring actual contradictions in the world economy, such as the aforementioned (de)centralising tendencies. An inter-

● vectoralism again?

> the organisational structure of platforms such as Amazon: a company that presents itself as a ‘marketplace’ – a medium for free market exchange – while in fact it is meticulously planning exchange and coordination patterns between suppliers and demanders. This leads to a comparison of platforms such as Amazon with socialist planned economies, which is both intuitive and highlighted in the literature

<!--quoteend-->

> Therefore, we propose to complement the generic model of the Stack with an additional level of political economic analysis, preparing it for further uses in terms of the analysis of capitalist development. Such

<!--quoteend-->

> This involves articulating implications regarding the scope of agency and types of interplay between various actors in the economy and its reflexive social structures.

<!--quoteend-->

> Acknowledgement of the material realities of planetary-scale computation is central to Bratton’s notion of the Stack: “There is no planetary-scale computation without a planet, and no computational infrastructure without the transformation of matter into energy and energy into information”

<!--quoteend-->

> Seen from the vantage point of global capitalism, the politics of usership allow for the continuation of the subsumption of labour under capital.

<!--quoteend-->

> Historically, formal subsumption has shaped the hegemony of the Genoese and the Dutch cycle, real subsumption that of the British and the US cycle.

<!--quoteend-->

> Fuchs (2014, 198) summarises that the former “[…] entails quantitative changes in the mode of production, in the productive forces, whereas the real subsumption changes the productive forces qualitatively”, associating the former with the production of absolute surplus-value and the latter with the production of relative surplus-value

<!--quoteend-->

> The notion of the general intellect relates to a section in Notebook VI in Grundrisse (Marx 1858/1993, 690-695). Marx discusses particularly the question “to what extent fixed capital (machine) creates value”. All in all, these three stages of subsumption remind us of the different topologies of power and knowledge (which are influential for the reproduction of subjectivities) in a given phase of capitalist development.

<!--quoteend-->

> We compare the subsumption via general intellect with the concept of “machinic enslavement” developed by Deleuze and Guattari (1987, 456-458).

<!--quoteend-->

> Lazzarato (2014, 2526) characterises machinic enslavement as dividualization, where human life is turned into fragmented quanta of abstract labour.

<!--quoteend-->

> Understanding Recent Perspectives of Capitalist Evolution

<!--quoteend-->

> Before this, we analyse the current capitalist appropriation of the Stack and its role in the continuing abstraction of labour.

<!--quoteend-->

> there is a hypothesis of cognitive capitalism, which conceives that the law of labour value loses weight and may even sublate itself to “life value”

<!--quoteend-->

> It is true that the general intellect and life itself becomes ever more subordinated to capital, but this is essentially not a novel phenomenon.

<!--quoteend-->

> The increase in the US economy’s cognitive (immaterial or even digital) labour supply and demand affirms the transition from material to financial expansion in the current systemic cycle of accumulation of the US. The greatest share of surplus is still accumulated via material labour in global capitalism, via Fordist exploitation on assembly lines in Southeast Asia and extractivism in regions of the Global South.

<!--quoteend-->

> This labour ranges from the micro-level production of interfaces allowing users to make inputs to platforms, to the macro-level establishment of cloud infrastructures and data centres, as well as the maintenance and expansion of the physical infrastructure of the Internet itself. Without this surplus labour there is no platform after all.

<!--quoteend-->

> But how can we understand the enormous profit gains of platforms such as Google, Amazon, Microsoft, Apple or Facebook if they generate hardly any real value via accumulation? It seems intuitive to understand the origination of these gains from an intellectual monopoly perspective. In general, intellectual monopoly capitalism concerns the establishment of corporate market power through the protection of intellectual property for the production of intangible assets

<!--quoteend-->

> Vercellone (2007) argues that there is a global historical trend in opening up knowledge via increasing public education on the one hand and open access to knowledge archives on the other hand, spreading the general intellect widely (as with Wikipedia). This makes labour less material and less vulnerable, therefore enabling workers’ emancipation.

<!--quoteend-->

> Pagano (2014) argues otherwise, that the more important global historical trend lies in the appropriation of knowledge by the increased protection of intellectual property through the patenting activities of multinational corporations such as Big Pharma or Big Tech.9

<!--quoteend-->

> Durand and Milberg (2020) extend the framework by introducing the term “information rents”10, which allow multinationals to capture rents originating at different levels within global production networks.

<!--quoteend-->

> The bottom line that we would like to highlight here suggests that through the infrastructures of planetary-scale computation, platforms accrue profit through information rent-seeking rather than through capital accumulation.

<!--quoteend-->

> Bratton’s (2015, 328-331) argument suggests that even if platforms are organisational forms developed within global capitalism, their dual capacity for centralisation and decentralisation allows for the emergence of apparatuses that can be equally used for dispossession and for universal provision of value to users through technology appropriation and social provisioning (Srnicek and Williams 2015; Likavčan and ScholzWäckerle 2018).

<!--quoteend-->

> As for the possibility of counter-hegemonic alternatives, one avenue of exploration focuses on commons-based appropriation of the Stack that aims at negating money-based exchange mechanisms

<!--quoteend-->

> As far as “communication power” is a central means of coordination and control, it is significant to “reprogram communication networks” (Castells 2009, 299ff.) by building a “communication society as a society of the commons” (Fuchs 2020, 293), as similarly argued by Hanappi (2020b) and Gruszka et al. (2020).

<!--quoteend-->

> The development of emancipatory commons-based platforms would, on the one hand, diminish the increasing dividualization of subjectivities and could enhance the reproduction of critically reflective subjects along the lines of the “multitude”, understood as an aggregate, leaderless and nongovernable political subjectivity (Hardt and Negri 2000). On the other hand, it could sequentially replace the corporate appropriation of institutions and infrastructures within the global production network. While reflecting on the meaning of this double transformation induced by commons-based appropriation of the Stack, one can identify here a recuperation of a key value of autonomy as a practice of self-governance via planetary-scale computation (or self-legislation, following the Greek etymology of the term) (Likavčan 2019).

<!--quoteend-->

> In this respect, Schneider (2022a) discusses decolonial tactics of establishing governable stacks as a means of resisting those ideological appropriations of planetary-scale computation that tend to serve as means of “extension of whiteness” and of the neoliberal politics of dividualization.

<!--quoteend-->

> The idea of governable stacks acquires critical relevance particularly in relationship to another political category – sovereignty – widely discussed by both Bratton (2015) and Schneider (2022a). As far as platform sovereignty represents a historical drift away from state-based and market-based sovereignties, the potential for a variety of heterodox stacks with emancipatory goals emerges, ranging from red stacks (Terranova 2014) to anarchist/libertarian networks facilitated by Web 3.0 blockchain technologies in the form of decentralised autonomous organisations (DAOs).

<!--quoteend-->

> Schneider’s examples indeed cover the wide range of the technological and political spectrum of emancipatory activities, ranging from community platforms such as Catalonian Guifi.net (Schneider 2022a, 26), to commons-based technologies such as Sovereign Cloud Stack or NextCloud (2022a, 28), to blockchain projects such as Democracy Earth (2022a, 29).

<!--quoteend-->

> Self-help platform economies in South Africa or Kenya serve as other such examples (Rodima-Taylor 2022).

<!--quoteend-->

> This demonstrates how the model of the Stack works thanks to its metastable nature not just as an integrative model of global capitalism, but also as a map of possible futures and nodes of interventions – capitalist stacks are just as possible to imagine as communist, anarchist or any other emancipatory form of appropriation.

<!--quoteend-->

> For that reason, we highlight the need for further investigation of political economic aspects of planetary-scale computation, to rework and extend the model of the Stack.

<!--quoteend-->

> Moreover, we believe that it is possible to mobilise the model to understand what kind of correctives and regulations are appropriate to steer the Stack’s future into post-capitalism proper. In this respect, the more recent essay by Schneider (2022b) points at such reconstructive appropriations with respect to blockchain technologies, especially in terms of encoding user rights.


## Concluding Remarks

> The ubiquity of the latter depends on the geopolitics of effective global production networks, reproducing mixes of old models of political economy: extractivism, Fordist exploitation as well as rent-seeking, monopolisation, and financialisation. This

<!--quoteend-->

> Technology appropriation and the establishment of commons-based platforms for [[social provisioning]] seem to be a potential counter-hegemonic response.

YES

