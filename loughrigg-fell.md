# Loughrigg Fell

A lovely fell in the central [[Lakes]].  I've had quite a few walks here over the years, as it's near the caravan park.


## Loughrigg Tarn

There is a lovely little tarn, Loughrigg Tarn, where you can go down to the edge and sit next to the water.  Sitting by the water here was one of Dad's favourite spots.

![[photos/lakes-2020/loughrigg-tarn.jpg]]


## Views

It's only small, but you still get some stunning views from the top of Loughrigg. 

[[Grasmere]] from Loughrigg.

![[photos/lakes-2020/grasmere-from-loughrigg.jpg]]

[[Windermere]] from Loughrigg.

![[photos/lakes-2020/windermere-from-loughrigg.jpg]]

