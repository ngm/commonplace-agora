# Myanmar

> Insurgencies have been ongoing in Myanmar since [[1948]], the year the country, then known as Burma, gained independence from the United Kingdom.
> 
> &#x2013; [Internal conflict in Myanmar - Wikipedia](https://en.wikipedia.org/wiki/Internal_conflict_in_Myanmar) 

