# topological determinism

> Yes, but/and I also think we have to beware of what I call “topological determinism.” #Decentralized &amp; #distributed architectures don’t necessarily, invariably or even all that frequently drive #progressive political commitments. They have other effects, and those effects may well be desirable, but just as we can’t automate the achievement of justice, we can’t design a #network #topology that in and of itself manifests the revolution.
> 
> &#x2013; [[Adam Greenfield]]
> (https://social.coop/@adamgreenfield/109406054534226019)

