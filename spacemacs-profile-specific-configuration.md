# spacemacs profile-specific configuration

I need to find a way to share the bulk of my spacemacs config in both my personal and my work logins, but have some customisations in both spaces.  (I already sync it all to a git repo, so I have it available wherever I need it - I'm talking here about having profile-specific customisations, while reusing **most** of it.)

I think I can probably do it with a mix of `.spacemacs.env` and perhaps loading a per-profile file in `dotspacemacs/user-config`.

Things I need to tweak between profiles:

-   org-agenda file locations
-   org-agenda capture templates
-   org-roam file/db locations
-   &#x2026;


## Examples

This is interesting, here they have some functions to check this:

https://github.com/turbana/emacs-config/blob/2003077c60ffabd3b48835727af8953fc37067f7/README.org#initial-setup

```emacs-lisp
;; at_home/at_work are bash functions defined in ~/.bashrc
(defconst at-home (= 0 (call-process "bash" nil nil nil "-c" "source ~/.bashrc && at_home")))
(defconst at-work (= 0 (call-process "bash" nil nil nil "-c" "source ~/.bashrc && at_work")))
```

