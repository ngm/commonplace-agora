# We should socialise ICT infrastructure

A
: [[claim]]

Seen in [[Digital Tech Deal]], [[Internet for the People]], [[Governable Stacks against Digital Colonialism]].

> Physical infrastructure such as cloud server farms, wireless cell towers, fiber optic networks and transoceanic submarine cables benefit those who own it. There are initiatives for community-run internet service providers and wireless mesh networks which can help place these services into the hands of communities. Some infrastructure, such as submarine cables, could be maintained by an international consortium that builds and maintains it at cost for the public good rather than profit.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

