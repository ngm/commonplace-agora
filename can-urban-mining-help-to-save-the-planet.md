# Can urban mining help to save the planet?

URL
: https://www.opendemocracy.net/en/oureconomy/urban-mining-reuse-recycle-old-goods-electronics-save-planet-climate-crisis/
    
    Can [[urban mining]] help to save the planet?

> Recycling rare metals from our waste could help us to reduce extractive mining. But urban mining's value depends on how it’s done

