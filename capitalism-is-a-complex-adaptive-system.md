# Capitalism is a complex adaptive system

A
: [[claim]]

[[Capitalism]] is a [[complex adaptive system]].

> In viewing capitalism as a complex adaptive system, the proposition of this book is that we can employ systems theory in a way consistent with Marxism and dialectics, thus revealing links between aspects of the crisis which are hard to understand by conventional thinking.
> 
> &#x2013; [[The Entropy of Capitalism]]

<!--quoteend-->

> One of the subtlest and most interesting adaptations of capitalism might be to parasitise upon the forces of human renewal, while attempting of course to downplay their post-capitalist features.
> 
> &#x2013; [[The Entropy of Capitalism]]

