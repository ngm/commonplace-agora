# Capitalist Manufacturing // Manufacturing Communism – Part 1 (ft. Nick Chavez)

Found at
: https://www.patreon.com/posts/229-capitalist-1-78282106

On the need for an understanding of [[production]] in order to harness it for [[communism]].

That is - how do we actually manufacture things?

> We welcome returning champ Nick Chavez – mechanical engineer, marxist political economist – for a long conversation about his new essay examining the real nitty-gritty aspects of production and labor, knowledge and management. Time to step down from the ivory tower and onto the shop floor

Consider 'low mix high volume' and 'high mix low volume' styles of manufacturing.

We might favour high mix low volume in a transition to communism. Small manufacturing pockets. Low mix high volume ie mass manufacture often associated with capitalist exploitation. Avoid becoming localist though.

