# Digital welfare state

-   [‘Digital welfare state’: big tech allowed to target and surveil the poor, UN &#x2026;](https://www.theguardian.com/technology/2019/oct/16/digital-welfare-state-big-tech-allowed-to-target-and-surveil-the-poor-un-warns)

> In the UK, he notes, 12 million people, or one in five of the population, do not have essential digital skills needed for modern day-to-day life."

<!--quoteend-->

> Instead of inflicting misery on millions, digital technology could be used as a force for good. It could “ensure a higher standard of living for the vulnerable and disadvantaged, devise new ways of caring for those left behind. That would be the real digital welfare state revolution.

