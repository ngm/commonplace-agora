# Disaster capitalism

Disaster capitalism describes the way private industries spring up to directly profit from large-scale crises.

> Disaster profiteering and war profiteering isn’t a new concept, but it really deepened under the Bush administration after 9/11, when the administration declared this sort of never-ending security crisis, and simultaneously privatized it and outsourced it—this included the domestic, privatized security state, as well as the [privatized] invasion and occupation of Iraq and Afghanistan.
> &#x2013;  [Naomi Klein on Coronavirus and Disaster Capitalism - VICE](https://www.vice.com/en_us/article/5dmqyk/naomi-klein-interview-on-coronavirus-and-disaster-capitalism-shock-doctrine) 

<!--quoteend-->

> Political and economic elites understand that moments of crisis is their chance to push through their wish list of unpopular policies that further polarize wealth in this country and around the world.
> &#x2013;  [Naomi Klein on Coronavirus and Disaster Capitalism - VICE](https://www.vice.com/en_us/article/5dmqyk/naomi-klein-interview-on-coronavirus-and-disaster-capitalism-shock-doctrine) 

<!--quoteend-->

> Heritage Foundation met and came up with a wish list of “pro-free market” solutions to Katrina. We can be sure that exactly the same kinds of meetings will happen now— in fact, the person who chaired the Katrina group was Mike Pence. 
> &#x2013;  [Naomi Klein on Coronavirus and Disaster Capitalism - VICE](https://www.vice.com/en_us/article/5dmqyk/naomi-klein-interview-on-coronavirus-and-disaster-capitalism-shock-doctrine) 


## Examples


### During corovavirus

-   [Trump tried to poach scientists to ensure “exclusive” profits from coronaviru&#x2026;](https://www.salon.com/2020/03/16/trump-tried-to-poach-scientists-to-ensure-exclusive-profits-from-coronavirus-vaccine-germany-says/)
-   [[Coronavirus and 3D printing]]

