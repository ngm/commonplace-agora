# polluter elite

> polluter elite”: anyone with a net worth over $1m who reinforces the use of fossil fuel technologies through their high carbon consumption, investments in polluting companies and, most importantly, political influence. “The polluter elite have blocked an alternative history where the destruction of extreme weather events and air pollution could have been reduced,” he told the Guardian.
> 
> &#x2013; [[The great carbon divide]]

