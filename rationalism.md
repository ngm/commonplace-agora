# Rationalism

> The presumption that there is one right—rational—way. The application of a universal ideology to the local particular.
> 
> &#x2013; [[Lean Logic]] ([Rationalism - LEAN LOGIC](https://leanlogic.online/glossary/rationalism/))

