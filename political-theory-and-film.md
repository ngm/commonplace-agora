# Political theory and film

> If a movie seems to be low in terms of both political content and intent, it may still yieldinteresting reflections of social and political reality. For example, how does the movie portray characters of different races and gender? How are social classes depicted, if at all? Does the movie seem to judge certain kinds of characters because of their race, class, and so on? Is the movie consistent with prevailing ideas about the economic and social system, or does it question them?
> 
> -   [An Outtake: A Guide for the Political Analysis of Movies | 26 | Projec](https://www.taylorfrancis.com/chapters/mono/10.4324/9781315701493-26/outtake-guide-political-analysis-movies-terry-christensen-peter-haas)

<!--quoteend-->

> For example, you may examine a film’s support or rejection of individualism as an ideological value. You might also construct a spectrum of individualism versus communal action on either end and argue an interpretation of a film that puts its political message somewhere in the middle of that spectrum.
> 
> -   [An Outtake: A Guide for the Political Analysis of Movies | 26 | Projec](https://www.taylorfrancis.com/chapters/mono/10.4324/9781315701493-26/outtake-guide-political-analysis-movies-terry-christensen-peter-haas)

<!--quoteend-->

> It seeks to demonstrate that cinema can help critical theory repoliticize culture and society and affirm the theoretical and political impact of cinematic knowledge. After discussing how the [[Frankfurt School]] saw cinema as an instrument of capitalism use to promote the cultural and political regimentation of the masses, Vighi then proceeds to demonstrate that critical theory can in fact suggest a different verdict on the progressive potential of cinema. 
> 
> &#x2013; [Critical Theory and Film: Rethinking Ideology Through Film Noir: Critical The&#x2026;](https://www.bloomsbury.com/us/critical-theory-and-film-9781441111425/)

[[Theodor Adorno]] and [[Max Horkheimer]].


## Resources

-   [Political theory and film: From Adorno to Žižek | SpringerLink](https://link.springer.com/article/10.1057/s41296-018-0239-y)
-   [An Outtake: A Guide for the Political Analysis of Movies | 26 | Projec](https://www.taylorfrancis.com/chapters/mono/10.4324/9781315701493-26/outtake-guide-political-analysis-movies-terry-christensen-peter-haas)

