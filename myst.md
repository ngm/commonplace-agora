# Myst

Classic of [[interactive fiction]] (I think - is it counted as interactive fiction if it's a 3D world?)  We started playing this in November 2020.

![[images/myst.jpg]]

Playing the realMyst edition. Really enjoying it.  Loads of detail and backstory - all those pages in the books in the library!  I'd read in some reviews that the navigation on an iPad was a bit rubbish, but I found it totally fine.  I think we'll enjoy playing through the whole thing.

