# left-tech magazines

Magazines that critical evaluate current technology / propose better alternatives.  That I like to read, and eventually would like to get something published in&#x2026;

-   [[Branch]]
-   [[Compost]]
-   [[Logic]]
-   [[The Reboot]]


## Branch

-   [Repairing Our Relationship with Technology - Branch](https://branch.climateaction.tech/issues/issue-1/the-right-to-repair/): ~800 words
    -   Author: founder of charity, written plenty other blog posts
-   [One Vision, One World. Whose World Then? - Branch](https://branch.climateaction.tech/issues/issue-1/one-vision-one-world-whose-world-then/): ~2000 words
    -   Authors: indigenous leader, lawyer; PhD candidate
-   [#Tech4Bad: When Do We Say No? - Branch](https://branch.climateaction.tech/issues/issue-3/tech4bad-when-do-we-say-no/): ~2000 words
    -   Authors: ?

Average maybe around 1500?

Contributions: [Request for Contributions - Branch](https://branch.climateaction.tech/request-for-contributions/) 


## Logic

-   [[How to Make a Pencil]]: ~4000/5000 words.
    -   Author: university researcher, writer of a book
-   [Specter in the Machine](https://logicmag.io/commons/specter-in-the-machine/): ~3000 words
    -   Author: written for The Baffler, Dissnet, The Nation, etc
-   [[Informatics of the Oppressed]]: ~5000 words
    -   Author: PhD candidate in Science, Technology, and Society at MIT
-   [Agile and the Long Crisis of Software](https://logicmag.io/clouds/agile-and-the-long-crisis-of-software/): ~5500 words
    -   Author: assistant professor in information studies and digital humanities

Rough average of 4000 words.


## Compost

No issue yet in 2022.

-   [COMPOST Issue 02: Uncivilizing Digital Territories by Luandro](https://two.compost.digital/uncivilizing-digital-territories/): ~4000 words
    -   Author: technologist, forester, and admirer of originary cultures.
-   [[Seeding the Wild]]: ~2500 words
    -   Authors: members of a think and do cooperative
-   [COMPOST Issue 01: The Salt of the Cosmos by Tal Milovina](https://one.compost.digital/the-salt-of-the-cosmos/) : ~3500 words
    -   Author: editor of online magazine, other articles published elsewhere

Average maybe around 3000?


## The Reboot

Not very active in 2022.

