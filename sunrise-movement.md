# Sunrise Movement

> the US elected a president with the most ambitious climate plan of any candidate in history, directly shaped by the Sunrise Movement and the campaign for a Green New Deal.
> 
> &#x2013; [[Mish-Mash Ecologism]]

