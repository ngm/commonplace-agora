# Marxism

> the intervention to reappropriate [[surplus value]] through the seizure of the [[means of production]] and the capture of the state.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

^ in a nutshell

> Capitalism brought about an enormous development in technology and production: “The bourgeoisie has created more massive and more colossal productive forces than have all preceding generations together.” But workers were now nothing more than commodities, their lives subject to the domination of the market. And as capitalism becomes more and more obviously inadequate to control its own enormous growth, the working class will become the instrument for its replacement.
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> As workers become “a ruling class,” representing the vast majority of the nation, they will sweep away the conditions for the existence of all classes, “and will therefore have abolished its own supremacy as a class.” The climactic sentence of the first part of the Manifesto is profoundly important, repudiating any notion of a police state, and insisting on the ultimate goal of individual freedom: “In place of the old bourgeois society, with its classes and class antagonisms, we shall have an association, in which the free development of each is the condition for the free development of all.”
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> Marx’s economic theory and political method of analysis are critical tools for activists today. Marxism is a means to understand and dismantle the world of the 1 percent, a world that exploits, disenfranchises, oppresses, and dispossesses the many for the sake of the few; a world that may not make it to the next century with our planet and our humanity intact. I wrote this book for those of us who are attempting to make sense of the world in order to change it.
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Themes

> -   Modern work is alienated
> -   Modern work is insecure
> -   Workers get paid little while capitalists get rich
> -   Capitalism is very unstable
> -   Capitalism is bad for capitalists
> 
> [POLITICAL THEORY - Karl Marx - YouTube](https://www.youtube.com/watch?v=fSQgCy_iIcc)


## Misc

> We owe much to Marx’s attempt to provide us with a coherent and stimulating analysis of the commodity and commodity relations, to an activist philosophy, a systematic social theory, an objectively grounded or “scientific” concept of historical development, and a flexible political strategy. 
> 
> &#x2013; [[The Communalist Project]]


## See also

-   [[Alienation]]
-   [[Communist Manifesto]]
-   [[Marxist humanism]]
-   [[Revisionism]]
-   [[Caliban and the witch]]
-   [[Withering away of the state]]


## Criticism

> But for the most part, as we have seen, Marxism’s economic insights belonged to an era of emerging factory capitalism in the nineteenth century. Brilliant as a theory of the material preconditions for socialism, it did not address the ecological, civic, and subjective forces or the efficient causes that could impel humanity into a movement for revolutionary social change.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> By emphasizing the nation-state—including a “workers’ state”—as the locus of economic as well as political power, Marx (as well as libertarians) notoriously failed to demonstrate how workers could fully and directly control such a state without the mediation of an empowered bureaucracy and essentially statist (or equivalently, in the case of libertarians, governmental) institutions. As a result, the Marxists unavoidably saw the political realm, which it designated a workers’ state, as a repressive entity, ostensibly based on the interests of a single class: the proletariat.
> 
> &#x2013; [[The Communalist Project]]


## Misc

> Capitalism (he’d argued) created misery, but it also created progress, and the revolution that was going to liberate mankind from misery would only happen once capitalism had contributed all the progress that it could, and all the misery too. At that point, there would be so much money invested by capitalists desperate to keep their profits up, that the infrastructure for producing things would have attained a state of near-perfection. At the same time, the search for higher profits would have driven the wages of the working class down to the point of near-destitution. It would be a world of wonderful machines and ragged humans. When the contradiction became unbearable, the workers would act. They would abolish a social system that was absurdly more savage and unsophisticated than the production lines in the factories. And paradise would very quickly lie within their grasp, because Marx expected that the victorious socialists of the future would be able to pick up the whole completed apparatus of capitalism – all its beautiful machinery – and carry it forward into the new society, still humming, still prodigally producing, only doing so now for the benefit of everybody, not for a tiny class of owners. There might be a need for a brief period of decisive government during the transition to the new world of plenty, but the ‘dictatorship of the proletariat’ Marx imagined was modelled on the ‘dictatorships’ of ancient Rome, when the republic would now and again draft some respected citizen to give orders in an emergency. The dictatorship of Cincinnatus lasted one day; then, having extracted the Roman army from the mess it was in, he went back to his plough. The dictatorship of the proletariat would presumably last a little longer, perhaps a few years. And of course there would also be an opportunity to improve on the sleek technology inherited from capitalism, now that society as a whole was pulling the levers of the engines of plenty. But it wouldn’t take long. There’d be no need to build up productive capacity for the new world. Capitalism had already done that. Very soon, it would no longer be necessary even to share out the rewards of work in proportion to how much work people did. All the ‘springs of co-operative wealth’ would flow abundantly, and anyone could have anything, or be anything. 
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> ‘But I should like to answer properly, because this is a vital point. Naturally I do not doubt the great truth that all economic value is created by human labour. This is apparent even to mathematicians. The question is only, how this truth is to be best applied; how it is to be applied in a society where we are not aiming, like Marx, just to diagnose economic relationships, and to criticise them, but must manage them too; where we are obliged to be concrete and detailed in our thinking. 
> 
> &#x2013; [[Red Plenty]]

<!--quoteend-->

> Anyone who takes up the name of Marx to describe their politics must take into account that Marxism is a theory of struggle.
> 
> &#x2013; [[Breaking Things at Work]]

