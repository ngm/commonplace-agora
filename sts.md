# STS

Sometimes [[Science, technology and society]], sometimes [[Science and technology studies]].

Critical studies of the impact of science and technology on society.


## Technology and society

> technologies are the product not only of technical work but also of social negotiations
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> technologies are not value-neutral but rather are a product of the historical contexts in which they are made
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Technologies are historical texts. When we read them, we are able to read history
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> political innovation can spur technological innovation.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> technology can shape the path of political history by making certain actions possible
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> historical readings of technology can make visible the complexities internal to a political project
> 
> &#x2013; [[Cybernetic Revolutionaries]]

