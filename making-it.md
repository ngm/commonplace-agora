# Making It

An
: [[article]]

URL
: https://www.newyorker.com/magazine/2014/01/13/making-it-2

Author
: [[Evgeny Morozov]]

> The [[Arts and Crafts movement]] was spending far too much time on “rag-rugs, baskets, and . . . exhibitions of work chiefly by amateurs,” rather than asking the most basic questions about inequality

<!--quoteend-->

> Inspired by the technophilia of his intellectual hero Buckminster Fuller, Brand played a key role in celebrating the personal computer as the ultimate tool of emancipation

<!--quoteend-->

> He distinguished the hackers from the planners, those rigid and unimaginative technocrats, noting that “when computers become available to everybody, the hackers take over

this failed. paralll to social media.

> Like the Arts and Crafts movement—a mélange of back-to-the-land simplifiers, socialists, anarchists, and tweedy art connoisseurs

<!--quoteend-->

> Kelly isn’t jesting when he identifies the rise of makers with a third industrial revolution: many promoters of the maker movement believe that personal manufacturing will undermine the clout of large corporations

<!--quoteend-->

> The Maker Movement Manifesto.” “Every revolution needs an army. . . . My objective with this book is to radicalize you and get you to become a soldier in this army.

<!--quoteend-->

> Inequality here is not just a matter of who owns and runs the means of physical production but also of who owns and runs the means of intellectual production—the so-called “attention economy

<!--quoteend-->

> In the early nineteen-seventies, he helped launch Community Memory—a handful of computer terminals installed in public spaces in Berkeley and San Francisco which allowed local residents to communicate anonymously. It was the first true “social media

<!--quoteend-->

> Tools for Conviviality,” which called for devices and machines that would be easy to understand, learn, and repair, thus making experts and institutions unnecessary

<!--quoteend-->

> Convivial tools rule out certain levels of power, compulsion, and programming, which are precisely those features that now tend to make all governments look more or less alike,” Illich wrote

<!--quoteend-->

> Illich wanted to “retool” society so that traditional politics, with its penchant for endless talk, becomes unnecessary

<!--quoteend-->

> Then Steve Jobs showed up. Felsenstein’s political project, of building computers that would undermine institutions and allow citizens to share information and organize, was recast as an aesthetic project of self-reliance and personal empowerment

<!--quoteend-->

> Seeking salvation through tools alone is no more viable as a political strategy than addressing the ills of capitalism by cultivating a public appreciation of arts and crafts

<!--quoteend-->

> It didn’t make sense to speak of “convivial tools,” he argued, without taking a close look at the political and social structures in which they were embedded

