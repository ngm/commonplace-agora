# socialist calculation debate

Historical early 20th century debate broadly on the topic of [[Markets vs planning]]. Having a bit of a revival and a revisiting, especially in the era of [[information and communication technology]].

> The right-wing Austrian economist [[Ludwig von Mises]] kicked off the debate in 1920 with “[[Economic Calculation in the Socialist Commonwealth]],” a full-frontal assault on the feasibility of socialist planning
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Mises’ critical ‘[[Economic Calculation in the Socialist Commonwealth]]’ (1920) not only sparked the ‘socialist calculation debate’ about the possibility of economic planning but also was the first text of the nascent neoliberal movement
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> [[Mises]]’ defence of capitalism was constructed in the mirror image of Neurath’s critique. 
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Many socialists responded to Mises’s challenge by accepting his basic premise and then trying to write their own algorithm. In other words, they wanted to show that planners could create a substitute for the price system that could generate enough information to arrive at the correct production decisions for a socialist society
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Any serious attempt at socialist planning has to reckon with the problems posed by the “socialist calculation debate,” a decades-long argument that has influenced how generations of socialists have imagined a post-capitalist future
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Advocates of algorithmic socialism misunderstand Mises’s position in the socialist calculation debate, and thus fail to respond adequately to his criticisms. For Mises, the challenge is how to allocate intermediate goods to producers of final goods
> 
> &#x2013; [[How to Make a Pencil]]

<!--quoteend-->

> Some of our readers may be familiar with the ‘[[calculation debates]]’, of the 1930’s, which pitted liberal pro-market economists, like [[Hayek]], who favoured pricing mechanisms, against the socialist economists like Bauer, [[Neurath]] and ultimately, Polanyi, who argued the benefits of planning. 
> 
> &#x2013; [[P2P Accounting for Planetary Survival]]


## Points of debate


### Too much to calculate

> The first argument, attributed to Mises, is that “planning at scale wasn’t feasible” because of the sheer number of calculations required to work out an allocative plan for a ‘large’ economy
> 
> &#x2013; [[Review: People's Republic of Walmart]]

^ I believe that in the review from which the quote above comes, the author is saying that in [[The People's Republic of Walmart]] they mistakenly attribute this argument to Mises.

> They counter that the feasibility of planning at scale is established by the existence of capitalist enterprises like Walmart and Amazon that are “so vast that we should really call them centrally planned economies”. Like all capitalist enterprises, these buy and sell commodities (including labor services) in the market, but regulate their internal affairs through “planning” (rather than setting up internal markets for intermediate goods)
> 
> &#x2013; [[Review: People's Republic of Walmart]]

Bjorn Westergard isn't buying this.  He says they have achieved impressive logistical distribution of final goods, but they are not involved in the allocation of intermediate goods for production.

> Walmart and Amazon have not overcome a once insuperable barrier to centrally planned allocation - computational or otherwise - because Walmart and Amazon do not allocate production goods
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Amazon and Walmart’s “logistical genius” - which is genuinely novel and interesting - is in solving the problem of distributing goods to consumers through a network of warehouses connected by variety transportation mechanisms. This problem is at root a series of enmeshed inventory, routing, and scheduling problems
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> The animating question of the socialist calculation debate, on the other hand, was how a polity that had abolished private property in production goods (compare “capital”, “the means of production”, “higher order” goods) would allocate these production goods to different producing entities
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Solutions to the aforementioned logistical problems do not entail solutions to production good allocation problems or vice versa
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Amazon doesn’t allocate production goods to firms which manufacture for its virtual shelves. Those firms (and the firms they buy from, and the firms those firms buy from, etc.) turn to the market to acquire the inputs they believe will prove instrumental in their effort to turn a profit
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> If Amazon or Walmart were systematically acquiring the firms that sold goods through their virtual storefront, replacing market exchange with a deliberate allocative plan and technical division of labor as they moved up the supply chain, there would be something to the book’s claim that “we should really call them centrally planned economies” of increasing and potentially “vast” scale. They are not, so there isn’t.
> 
> &#x2013; [[Review: People's Republic of Walmart]]


### Too much data to collect

> The second argument, attributed to [[Hayek]], is that even if the calculations required to decide on an allocative plan were tractable, planners would not be able to collect “the data that goes into the equations"
> 
> &#x2013; [[Review: People's Republic of Walmart]]

<!--quoteend-->

> Phillips and Rozworksi counter that whatever the case may have been when Hayek wrote, the advent of “[[big data]]” in e-commerce, which “is testing the limits of how much granular information can be collected”, has eliminated any informational obstacles
> 
> &#x2013; [[Review: People's Republic of Walmart]]

Bjorn Westergard is not having this, either.

> In sum, the inventory, scheduling, and routing techniques of e-commerce giants do not offer solutions to the problems Austrians identified with the planning proposals of “[[market socialism]]” that Rozworski and Phillips implicitly defend

