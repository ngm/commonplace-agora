# Cultivate Shared Purpose and Values

> Shared purpose and values are the lifeblood of any commons. Without them, a commons loses its coherence and vitality.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> But shared purpose and values can only arise when people contribute from their own pas- sion and commitment, connect with each other, and share certain experiences.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> A commons does not necessarily **start** with shared purpose and values. These outcomes must be **earned** by commoners over time as they struggle to bring their diverse perspectives into greater alignment. This is important because the sense of shared purpose in a commons can’t be formally imposed or declared. It must arise organically through meaningful commoning over time. A rooted culture cannot be built overnight.
> 
> &#x2013; [[Free, Fair and Alive]]

