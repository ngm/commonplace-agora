# What Does It Mean to Care and Repair?



## Hosted by [[Repair Acts]]

-   Collecting repair stories
-   To build up a picture of everyday repair prctices
-   Repair economies, aesthetics, repair in relation to climate change


## Monai de Paula Antunes

-   [[Wild Design]]
    -   [[Gambiarra]] is the starting point

![[Tales_of_care_and_repair/2021-10-16_14-17-45_screenshot.png]]


## Alma Clavin

-   urban planning, well being
-   in the home
    -   repair is often cared out by a member of the family
    -   bond or gratitude between people helping with repair
-   repair in our own gardens
    -   trees and green space within 300m have most impact on mental wellbeing
    -   rewilding has lower energy inputs
-   into town
    -   repair and maintenance done by the local authority
-   in town
    -   planters
        -   denaturing of urban space
-   allotments
    -   seen as spaces of care and repair
    -   care of soil, natural environment, and humans (through foodgrowing)
-   repair of home and objects owned
    -   few formal repair shops in Alma's town
    -   employment opportunities outside of town
-   vacancy and decay in town
-   relocalising of repair
    -   repair value chains
-   members of local community came together to repair buildings
-   community repair shows us we need to be intergenerational
-   out into the wider landscape
    -   peat bogs
-   [[Just Transition Fund]]


## [[Ravi Agarwal]]

-   involved in policy in waste economy
-   artist in local communities
-   Repair and care
    -   not just words, integrated ethics of everyday life
    -   not only a human act
    -   also part of non-human-non-human interactions
-   Critical of circular economy / resource efficiency
    -   relegates communities
    -   ignores the provenance of the materials
    -   been co-opted by neoliberalism
-   indigenous knowledge, referenced a lot these days but people don't really know what they're doing with it
-   community and commons

