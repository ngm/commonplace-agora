# Beyond Waste: Essential Skills for a Greener Tomorrow

Found at
: https://www.circularonline.co.uk/briefing-papers/beyond-waste-essential-skills-for-a-greener-tomorrow-report/

[[Green jobs]]. [[Repair]]. [[Reuse]].

Table 5 on page 22 suggests significant increases are needed in repair and reuse related jobs all the way through to 2035.

They estimate 20,000 by 2030, another 40,000 by 2035, and another 80,000 by 2040.

> the real growth area in terms of scale of jobs is in reuse and repair (currently voluntary roles at best),

<!--quoteend-->

> Although we need more support in reuse and repair as mentioned earlier, this is still heavily reliant on volunteers and is not yet at a point of commercial viability for many items. It does, however, provide a good training ground for skills development and more employment routes are being made available.

<!--quoteend-->

> There are opportunities for reuse and repair in every large town and city, ranging from reuse shops and repair events through to reuse hubs such as the Renew Hub in Manchester where SUEZ, in partnership with Greater Manchester Combined Authority and nine local authorities are delivering re-use on an industrial scale.

<!--quoteend-->

> we also need to rethink what moving to a more industrialised model would be like, with dedicated technical specialists for reuse, repair, and
> refurbishment. This would need to consider a ‘per item’ estimate.
> 
> -   if everyone in the UK returned 1 item per year for repair, and it
> 
> takes 60 minutes to do the repair properly by technical specialists
> 
> -   56 million hours = 35 hours per week for 45 weeks = 35,600
> 
> employees needed
> 
> -   whilst for 2 items this could increase to 71,200 employees
> -   this would cost £1.3 billion – to cover wages and on-costs
> 
> (based on minimum wage levels etc).
> 
> Based on the estimations there is a significant number of jobs available within reuse and repair if the commercial model can be optimised to cover jobs and supporting infrastructure. The more items we can circulate into the reuse/repair/remanufacture loop, the more jobs we can create.
> 
> Supportive policy is a must for new job creation.

