# ICT: A top horizontal priority in sustainable product policy

URL
: https://eeb.org/library/ict-a-top-horizontal-priority-in-sustainable-product-policy/

Report on the need for horizontal measures across ICT and consumer electronics products.

> This study calls attention to the need for ICT ([[Information and Communications Technology]]) products to be prioritised in ESPR ([[Ecodesign for Sustainable Products Regulation]]) and Ecodesign by demonstrating significant material impacts in a number of broad product groupings that have yet to be properly addressed. The Commission is at risk of missing the opportunity to unlock sizeable savings by allowing unaddressed products in this area to fall between the boundaries of traditional Ecodesign regulations and ongoing discussions around priority areas for ESPR implementation. We detail exactly how horizontal measures going beyond the basic concepts of [[durability]] and [[reparability]] could be defined and provide an order of magnitude of the potential savings that could be achieved.

