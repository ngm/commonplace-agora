# Theory of collective action

Really interesting, I had not heard of the theory of collective action, another debunkable so-called ‘tragedy’ of the commons.

> Ostrom noted that Olson’s model of collective action[…] is another model which predicts that the commons will be ruined. Olson (1965) examined political and social movements and was critical of the idea that support for a particular ideology or set of policy demands was sufficient to persuade individuals to become politically active in a group.
> 
> &#x2013; [[Elinor Ostrom's Rules for Radicals]] 

