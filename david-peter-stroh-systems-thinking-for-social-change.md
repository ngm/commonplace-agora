# David Peter Stroh, "Systems Thinking For Social Change"

URL
: https://newbooksnetwork.com/david-peter-stroh-systems-thinking-for-social-change-chelsea-green-2015

Featuring
: David Peter Stroh

Topics
: [[Systems thinking]]

Gave some examples of how what might solve a problem in the short term, a fix for a symptom, might not get to the underlying problem.  For example, homeless shelters might perpetuate homelessness despite providing temporary relief.

See also [[Adam Day, States of Disorder, Ecosystems of Governance]] for similar discussion of this mistake of fixing symptoms rather than fixing systems.

