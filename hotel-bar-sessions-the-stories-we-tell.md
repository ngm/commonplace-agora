# Hotel Bar Sessions: The Stories We Tell

A
: [[podcast]]

Lives at
: https://hotelbarpodcast.com/podcast/episode-116-the-stories-we-tell/

Part of
: [[Hotel Bar Sessions]]

Really fun discussion on documentation vs narration.

> The HBS hosts explore what is lost when we choose documentation over narration

I really like how they mix in lots of cultural references throughout the discussion (films, TV, etc). Helps to grasp the ideas.

The importance of narration on top of documentation is very reminiscent of Evgeny Morozov's comments about how he pivoted from a documentarian mindset to a strong narrative focus midway through making The Santiago Boys ([[The Mythology of Cybersyn (ft. Evgeny Morozov)]]).

