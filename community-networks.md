# community networks

> Connectivity is essential, but so is transforming how people connect. Simply plugging more users into a privatized internet does nothing to change how the internet is owned or organized, much less how it is experienced. 
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> Community networks give us an opportunity to revisit, and to reverse, this history. They represent the rudiments of a real alternative. This alternative is being constructed in the cracks and edges of our corporatized system, as communities find creative ways to meet their needs. A democratic internet is not an ideal to which reality must reconcile itself, but something that is already being worked out in practice. It is not a schematic to be implemented from above, but a set of experiments emerging from below. These experiments are necessarily limited, however: if there is any hope of their cohering into a radically new arrangement, they must be defended, extended, and deepened.
> 
> &#x2013; [[Internet for the People]]

-   [[Community broadband]]
-   [[community mesh network]]

-   [[B4RN]]
-   [[Chattanooga Gig]]
-   [[Equitable Internet Initiative]]

> If community networks began to displace corporate ISPs, the broadband cartel might use its power to block their upstream path. Of the four firms that account for 76 percent of all internet subscriptions in the US, three also operate backbones. They could refuse to work with community networks, leaving them stranded. This is the legacy of privatization: a corporate dictatorship over critical infrastructure. Creating alternatives may be the only hope for community networks to achieve their full potential.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The [[Green New Deal]] might offer one possibility for doing so. Climate activists have been calling for a national electricity grid that could efficiently distribute renewable energy across the US. [&#x2026;] Such a grid could be made “smart” by laying fiber along it, which could in turn provide the foundation for a new deprivatized backbone serving commu nity networks nationwide. The backbone could be managed by a federal agency, or perhaps by a federation of cooperatives, each responsible for different aspects or branches of the network. If needed, its footprint could be enlarged further by acquiring the thousands of miles of unused “dark” fiber buried across the country, and extended overseas by taking a stake in a submarine cable.

