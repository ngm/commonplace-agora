# Use-value

> An item’s USE-VALUE is how it is used. The use-value of bread is that it provides nourishment. The use-value of a chair is that it can be sat upon. And so on
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Regardless of whether that item is sold on a market, it still has a use-value if it is useful to someone
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The changing and often subjective “utility” of commodities is one of the reasons why it cannot be the determinant of how much something is worth on the market
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Even accounting for average needs, or in cases where the hierarchy of utility is more fixed, it makes no sense as a measure of exchange-value. If it were, bread would be more expensive than cars, and certainly more so than diamonds
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> While you or I are primarily concerned with the use-value of items, whether we can sit on a chair or on a loaf of bread, a capitalist does not care what an item’s use is, so long as it will make him money
> 
> &#x2013; [[A People's Guide to Capitalism]]

