# Cross Bay Walk

I did this in July 2021 as part of the [High Sheriff of Cumbria Cross Bay Walk](https://cumbriafoundation.enthuse.com/cf/high-sheriff-of-cumbria-cross-bay-walk).

![[photos/cross-bay-walk.jpg]]

It was a lot of fun, walking out on [[Morecambe Bay]], starting from Arnside and ending at Kents Bank.  It's about 8 miles long.

There was probably a few hundred people doing it.

You get some lovely fews looking back towards the coast of the Bay.  It was fun walking barefoot on the sands, and the bits where you go through the river channels.  The first one came up to my shins, and for one tiny part had soft springy sand underneath, which was interesting&#x2026;

There were lots of lugworm castings, bits of cockle shells, and some other shell.

