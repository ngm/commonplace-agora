# technology and political economy

:ctime:    20231113140445

:END:

[[Technology]] and [[Political economy]].

[[Technology plays a significant role in political economy]].

> It is then essential to produce a critical understanding of political economy in order to comprehend emerging trends in network topology and their social implications.
> 
> &#x2013; [[The Telekommunist Manifesto]]

