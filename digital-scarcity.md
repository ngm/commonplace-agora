# Digital scarcity

> This is perhaps one of the reasons that the recent generation of peer-to-peer technologies are overwhelmingly about determining and enforcing forms of digital scarcity and private property through blockchains and smart contracts: to protect our efforts, creations and communities against expropriation. **Now we are liberated to make private property of our own data before it enters the market — a depressing response to data extractivism**.
> 
> &#x2013; [[Seeding the Wild]]

