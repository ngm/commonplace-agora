# social.coop Spring 2022 Strategy Session

[[social.coop]] Spring 2022 Strategy Session.

URL
: https://www.loomio.org/d/NOkM3pH9/spring-2022-strategy-session

Loads of good stuff.  See the above link for group notes.

One thing that stuck out for me amongst lots of good stuff, was Matt Noyes' mention of Mike Hales' assertion that social.coop is more of a commons than a coop.  I'd like to explore that more - found this: https://loomio-uploads.s3.amazonaws.com/documents/files/000/152/026/original/mh_2018_07_01_-_reading_group_notes.pdf

One of the sessions was 'rose, bud, thorn' which I thought was a good facilitation framing.


## My rose, bud, and thorn answers


### rose: what is making social.coop valuable to you at this point?

-   Dual power: owning our own tools, being part of a social media platform that is actively building an alternative to big tech
-   People: making connections through the people that are on it, a lot of like-minded people
-   Learning: seeing a functioning coop from the inside, learning how governance operates, etc


### bud: what potential do you see in social.coop? what kinds of things could we do, or do more of?

-   As an inspiration and a learning resource for how to run social media platform cooperatively, help other Mastodon instances work as a coop
    -   Use e.g. Agora - a collective knowledge management tool


### thorn: what makes social.coop uncomfortable or less useful to you?

-   funnily enough, the format of Twitter style microblogging.
-   I find Mastodon itself absolutely terrible for conversations / deeper engagement


## Actions

Some actions I took for myself to explore further.

-   action: wiki, encouraging documentation of what we do
-   action: investigate joining up of different social.coop platforms further
-   action: propose rekindling social.coop reading group
-   action: explore dashboards as an idea


### Me

-   been a member of social.coop since 2017
-   using mastodon and microblogging a little bit less of late
-   but keen to help out where I can


### My skills

-   I can help out with bits and bobs of system administration
-   software upgrades, restarting services, creating user accounts, etc


### noah

what was discussed at last


## list of jobs that need doing

-   


## list of steps for onboarding

-   request access to git
-   request email alias
-   request access to server
-   turn up next saturday
-   root
    -   if we have root, is there anything we don't have access to
        -   pretty all in the pass repo
            -   Noah doesn't have access to this, Nick does
            -   Nick will add Eduardo to the pass repo
            -   need to have everyones key in order to reencrypt


## create gpg key

