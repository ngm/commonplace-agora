# Micropub to git

I am looking for something that allows me to publish from micropub to a git repo.

It would be nice to post via micropub straight in to my wiki repository.

I guess I have two ways of doing it:

-   spin up something that is its own micropub endpoint
-   hook in to the WordPress micropub endpoint on my site, as a syndication target I think


## Options

Things that are already built:

-   https://github.com/drivet/micropub-git-server
    -   A small Flask application which listens for micropub create requests and commits the result in a git repository.
    -   This server will commit plain, minimally processed JSON files, representing micropub entries, into a configured location of your choice.

