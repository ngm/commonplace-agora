# deprivatisation of the internet

[[Deprivatisation]] of the [[Internet]].

To counter the [[privatisation of the internet]].

> If privatization meant creating an internet that served the principle of profit maximization, deprivatization means creating an internet organized by the idea that people, not profit, should
> 
> &#x2013; [[Internet for the People]]

See also: [[Socialize the internet]].

