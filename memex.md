# Memex

![[images/memex.jpg]]

-   [[Vannevar Bush]]
-   [[As We May Think]]

> the memex is a tool to think with, not a tool to publish with 
> 
> &#x2013; [The Garden and the Stream: A Technopastoral | Hapgood](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

-   https://www.youtube.com/watch?v=c539cK58ees

-   [[tools for thought]]

