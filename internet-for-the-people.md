# Internet for the People

A
: [[book]]

Found at
: https://www.versobooks.com/books/3927-internet-for-the-people

Written by
: [[Ben Tarnoff]]

> The internet has not always existed in its current form. It had to be made. This book is about how it was made, and what we can do to change it.

Very good book.  Core statement is generally on the need for the [[deprivatisation of the internet]].  Gives a history of the [[privatisation of the internet]]. Discusses it up and down the stack (although tends to miss out hardware, focusing on networking infrastructure and platforms.)

Full of examples, policy ideas, acknowledgement of difficulties, etc.  Will come back to this a lot.

As well as a history of how the pipes and the platforms got privatised, has some recommendations for things that can be leveraged to achieve  deprivatisation.

'Down the stack' - i.e. infrastructure - we need publicy and cooperatively owned networks.

[[We should socialise ICT infrastructure]].

'Up the stack' - i.e. platforms - we need to shrink the 'online malls' (i.e. big tech).  Anti-monopoly regulation can assist in this, but ultimately we need to be building the alternatives.

[[We should socialise the platforms]].

[[Technology Networks]] are interesting as a means of involving more people into the creation of alternatives to existing big tech offerings.  To widen the scope of inputs.  Also, [[design justice]] helps with this.  Everyone is an expert in something - it could be coding, it could be design, but it could be the activities of their daily lives - they know these best.  Technology Networks are also spaces of organising.

[[Community wealth building]] is mentioned as a means to foster this local activity - making use of [[anchor institutions]].

The [[Fediverse]] gets a shoutout.

References [[Luddites]] as an example of historical example of rethinking and reclaiming use of technology.


## Raw highlights - to process

> Connectivity is never neutral. The growth of networks was guided by a desire for power and profit. They were not just conduits for conveying information, but mechanisms for forging relationships of control.

<!--quoteend-->

> The internet reformers have some good ideas, but they never quite reach the root of the problem. The root is simple: the internet is broken because the internet is a business.

<!--quoteend-->

> While the issues are various and complex, they are inextricable from the fact that the internet is owned by private firms and is run for profit.

<!--quoteend-->

> To build a better internet, we need to change how it is owned and organized

<!--quoteend-->

> This book presents a few directions for doing so. It calls for **shrinking the space of the market** and **diminishing the power of the profit motive**. It calls for developing models of public and cooperative ownership that encode the principles of collective governance and popular participation.

<!--quoteend-->

> It was the absence of the profit motive and the presence of public management that made the invention of the internet possible

<!--quoteend-->

> All this wasn’t cheap, but it worked. Scholars Jay P. Kesan and Rajiv C. Shah have estimated that the subsidies to the regional networks, together with the cost of running the NSFNET backbone, came to approximately $160 million. Other public sources, such as state governments and state-supported universities, likely contributed more than $1.6 billion to the development of the internet during this period.

<!--quoteend-->

> It has become not just a mass medium but an essential infrastructure, analogous to electricity in the depth of its integration into billions of people’s lives.

<!--quoteend-->

> Content providers like Google, Facebook, Amazon, and Microsoft now own or lease more than half of undersea bandwidth.

<!--quoteend-->

> “Broadband is like water and electricity now, and yet it’s still being treated like a luxury.” As the pandemic powerfully illustrated, a good internet connection is a necessity. It is a prerequisite for full participation in social, economic, political, and cultural life, and one that many millions of Americans don’t possess.

<!--quoteend-->

> Elections are the minimum of democracy’s meaning. Democracy requires a richer set of practices and a wider sphere of control to be fully democratic. It must, as the theorist Stuart Hall writes, take place not just occasionally and within certain circumscribed zones but “across all the centres of social activity—in private as well as public life, in personal associations as well as in compulsory obligations, in the family and the neighbourhood and the nursery and the shopping centre as well as in the public office or at the point of production.” And, we might add, in the realm of the internet.

<!--quoteend-->

> The Institute for Local Self-Reliance estimates that more than nine hundred communities across the United States are served by publicly or cooperatively owned networks.

<!--quoteend-->

> To put people over profit, you need to create spaces where the people can rule.

<!--quoteend-->

> In one state after another, telecoms have lobbied legislators to pass laws that prevent communities from creating their own networks.

<!--quoteend-->

> Hospitals and universities also tend to loom large in local economies. A 2021 study found that they were the top employers in twenty-seven states. Sometimes these entities are public, sometimes private, but they all benefit from public money and tax breaks. What if, in exchange, they were required to purchase their internet access from community networks? Their subscription fees could help defray the costs of supplying households with free internet from the same fiber.

<!--quoteend-->

> Local control” has a long, racist history in American life. Under its banner, communities have resisted school desegregation, blocked affordable housing projects, and hoarded public money in the places that least need it. There are rich communities and poor communities, and the former tend to find ways of keeping their advantages out of the hands of the latter. Decentralization is not inherently democratizing: it can just as easily serve to concentrate power as to distribute

<!--quoteend-->

> We can’t transform the internet purely at the local level. This is because the internet is neither local nor national nor global but a complex combination of all three. It operates at a number of different scales, and so must any project to transform it

<!--quoteend-->

> This is the legacy of privatization: a corporate dictatorship over critical infrastructure. Creating alternatives may be the only hope for community networks to achieve their full potential.

<!--quoteend-->

> When it comes to deprivatizing the pipes of the internet, three concerns in particular are likely to arise. The first is the potential cost of such a venture. How will we pay for it?

<!--quoteend-->

> Handing public money to private firms tends to enrich the firms while yielding little in the way of public benefit.

<!--quoteend-->

> This is only a single report, of course, but it illustrates a broader point: competition works best for customers who are worth competing for.

<!--quoteend-->

> Rather than a choice, access to the internet has become so essential to most people’s lives that it more closely resembles housing and health care: that is, something people can’t choose not to consume. For this reason, it makes more sense to think of the costs associated with getting online as a tax—and a regressive one at that.

<!--quoteend-->

> This is a descriptive account of how competitive markets work under capitalism. But it’s also something else: a theory of power. It says that the best way for human beings to exercise power over the conditions of their lives is through their capacity as market actors. If they want better internet service, they can make different consumption choices. And if market conditions are sufficiently competitive, the signals that those choices transmit will eventually result in better internet service.
> 
> In reality, this is a weak way to exercise power. It consigns people to a passive role with a narrow range of action. They are, at best, semaphores whose signals can influence the production and investment decisions of firms at a distance.

<!--quoteend-->

> This is another example of neoliberalism’s impoverishing effect on our political imagination. As Wendy Brown explains, neoliberalism “configures human beings exhaustively as market actors, always, only, and everywhere as homo oeconomicus.” Yet there is another way to think about what it means to be human: Homo politicus. This is the human being as a political animal: what Aristotle had in mind when he wrote that man is “by nature an animal intended to live in a polis.”

<!--quoteend-->

> Homo politicus exercises power not indirectly and individually, as an isolated stream of market signals, but directly and collectively, as the co-legislator of its social world. People argue, debate, deliberate, and decide how to govern themselves together. Homo politicus is what makes democracy possible.

<!--quoteend-->

> Digital authoritarianism and private ownership are entirely compatible. There is no reason to believe that an internet with more municipally owned broadband networks, or even a federally owned backbone, would make authoritarians any happier than a completely privatized one.

<!--quoteend-->

> Laws and regulations are useful, but far from sufficient. The best way to guarantee that public institutions serve the people is the presence of the people themselves within those institutions.

<!--quoteend-->

> States tend to concentrate power, distributing decision-making authority upward. Democratization means making power flow in the opposite direction: downward and outward.

<!--quoteend-->

> Drawing on the state to make the internet’s pipes more democratic requires making a different kind of state: one that “is rooted in, constantly draws energy from, and is pushed actively by, popular forces,” in the words of Stuart Hall. Only such a state can help uproot the regime installed by privatization and put something better in its place.

<!--quoteend-->

> As I write, Comcast is worth more than $260 billion; Google is worth more than $1.7 trillion. The telecoms are big, but the so-called platforms are creatures of another scale.

<!--quoteend-->

> None of the metaphors we use to think about the internet are perfect, but “platform” is among the worst.

<!--quoteend-->

> first modern shopping mall was built in Edina, Minnesota, in 1956. Its architect, Victor Gruen, was a Jewish socialist from Vienna who had fled the Nazis and disliked American car culture. He wanted to lure midcentury suburbanites out of their Fords and into a place that recalled the “rich public social life” of a great European city. He hoped to offer them not only shops but libraries and theaters and community centers. Above all, his mall would be a space for interaction: an “outlet for that primary human instinct to mingle with other humans.” Unlike in a city, however, this mingling would take place within a controlled setting. The chaos of urban life would be displaced by the discipline of rational design

<!--quoteend-->

> Data is sometimes compared to oil, but a better analogy might be coal.

<!--quoteend-->

> . It has propelled the capitalist reorganization of the internet, banishing the remnants of the research network and perfecting the profit engine

<!--quoteend-->

> Page and Brin hated online advertising, sharing Pierre Omidyar’s distaste for the tacky commercialization of the dot-com era.

<!--quoteend-->

> If Google began as an attempt to interpret the abundant information of the web, its commercial viability would rest on interpreting the abundant information of its users.

<!--quoteend-->

> In The Age of Surveillance Capitalism, Shoshana Zuboff describes this moment as a turning point not only in the history of Google but in the history of capitalism.

<!--quoteend-->

> As the theorist Philip E. Agre once observed, computers must impose a “grammar” on human activity to make it intelligible,

<!--quoteend-->

> Only in exceptional circumstances—a high-profile user is banned, an obtrusive new feature is introduced—does the sovereign reveal himself.

<!--quoteend-->

> Widespread fraud, the rising popularity of ad blockers, a growing indifference to ads, and persistent problems with “ad viewability”—the fact that ads are not always loaded in places where someone will see them—all contribute to what Hwang calls a “subprime attention crisis”: the over-valuing of advertising relative to its underlying asset, attention.

<!--quoteend-->

> The machinery of online advertising “has enabled the bundling of a multitude of tiny moments of attention into discrete, liquid assets that can then be bought and sold frictionlessly in a global marketplace.” But the commodities that circulate in this marketplace may be far less valuable than tech firms lead advertisers to believe.

<!--quoteend-->

> “This is horrible,” Meg Whitman, eBay’s CEO, told Omidyar after taking a tour of an Amazon facility in 1998. “The last thing we’d ever want to do is manage warehouses like this.”

<!--quoteend-->

> The birth of the online mall, the rise of the cloud, the spread of the data imperative—these were the principal vectors for the deeper privatization of the internet in the decade or two after the dot-com crash. But there was another change that profoundly altered the shape of the network: its diffusion.

<!--quoteend-->

> The proliferation of “smartness” is aimed at making digital surveillance as deeply integrated into our physical world as it is in our virtual one

<!--quoteend-->

> Networking “supported organizations that wanted to divide their labor force geographically,” writes the political economist Joan Greenbaum.

<!--quoteend-->

> It “has created a massive globalized reservoir of human labor power for companies to tap into, as much or as little as needed: the ‘human cloud,’” writes scholar Gavin Mueller

<!--quoteend-->

> The goal is a world where labor power can be conjured as painlessly as computing power, scaled to meet demand, and then discarded—a human cloud of virtual machines in which the virtual machines are people.

<!--quoteend-->

> No tech company, in fact, has ever lost quite so much. The whole point of privatizing the internet is to profit from it. Yet Uber and its gig economy peers are singularly unprofitable.

<!--quoteend-->

> What Uber and other comparable companies offer, particularly in their early stages, are exceptionally lucrative linkages between financialization and the internet. Investors can ride a rising wave of paper wealth as the valuation of a firm grows, and then convert that paper wealth into real wealth during a “liquidity event” such as an IPO or an acquisition, regardless of whether the firm ever turns a profit. Data is an essential lubricant in this cycle, as it helps uphold investor confidence in the possibility of profitability.

<!--quoteend-->

> But however polarization is defined, there is little evidence that social media causes it.

<!--quoteend-->

> Joel Kaplan, a high-ranking Republican within Facebook who serves as the company’s vice president of global public policy, wields particularly broad power. Kaplan has repeatedly intervened to promote the circulation of right-wing content on the site, and to push for content moderation rules and algorithm tweaks that favor the Right.

<!--quoteend-->

> second trend aims at reducing the market power of the big firms. This is the focus of the “New Brandeisians,” a group of anti-monopoly advocates who have become influential within the Democratic Party and even among a number of Republicans

<!--quoteend-->

> The New Brandeisians are right that rulemaking is insufficient without also transforming how the internet is owned.

<!--quoteend-->

> Further, they recognize that the current configuration of ownership is not the natural order of things, but rather a contingent affair, constructed through public policy.

<!--quoteend-->

> Yet the political economy preferred by the New Brandeisians isn’t a particularly radical departure from the present.

<!--quoteend-->

> They still want an internet ruled by markets, albeit one where markets are competitive rather than concentrated. The pursuit of profit would remain the organizing principle, but profit would be pursued by smaller and more entrepreneurial firms. And they believe that such a restructuring would go a long way toward addressing the concerns raised by the techlash.

<!--quoteend-->

> Fortunately, rulemaking and anti-monopoly, or some combination of the two, aren’t the only choices available to us. There is another strategy: deprivatization.

<!--quoteend-->

> Making markets more regulated or more competitive won’t touch the deeper problem, which is the market itself.

<!--quoteend-->

> Up the stack, it’s not quite so straightforward. There is no main character. The greater diversity of forms one encounters at this altitude, the distinctness of the different online malls and their entanglements, requires a greater range of approaches.

<!--quoteend-->

> Davis and her abolitionist colleagues give us the basic blueprint for deprivatizing the upper floors of the internet. On the one hand, we can work to erode the power of the online malls. The anti-monopoly toolkit—breaking up tech giants, banning mergers and acquisitions, and other New Brandeisian methods—is valuable here. On the other hand, we can create a constellation of alternatives that, to use Davis’s phrase, “lay claim to the space” that online malls currently occupy. The former tactic is designed to open cracks in the enclosures. The latter tactic aims at seeding those cracks with all manner of invasive species.

<!--quoteend-->

> We must move away from “technology as an outcome to toolmaking as a practice,” in the words of sociologist Ruha Benjamin. Technology ceases to be something that is done to people, and becomes something they do together.

<!--quoteend-->

> This brings us to another virtue of decentralization: not only does it facilitate greater diversity, it also enables a degree of democracy. At a small enough scale, social media communities can become self- governing.

<!--quoteend-->

> Still, these alternatives remain relatively niche in comparison to the offerings of the tech giants. Making them more robust, and more of a threat, will require public investment.

<!--quoteend-->

> Any project to improve social media must make such workers more valued and more visible, and enable them to form what Bartkowski calls “affinities and solidarities” with the users whose communities they care for.

<!--quoteend-->

> Nick Srnicek imagines a publicly owned cloud service that “ensures privacy, security, energy efficiency and equal access for all.” Such a service might be carved out of a corporate provider like Amazon Web Services, which could be required to donate a portion of its capacity. Picture a “public lane” for the cloud, serving the digital infrastructure needs of a growing deprivatized sector.

<!--quoteend-->

> “platform cooperativism.” Such efforts need public support. Fortunately, there are many policy instruments available, such as grants, loans, contracts, and preferential tax treatment. One could also imagine municipal regulatory codes that only allow app-based services to be performed by worker-owned firms, as the writer Seth Ackerman suggests.

<!--quoteend-->

> Data is made collectively, and made valuable collectively. It follows that its governance should also be collective: that users should have the power “to shape the purposes and conditions of data production,” in Viljoen’s words. Toward that end, a number of proposals have been developed for “data trusts” and “data commons” of different kinds.

<!--quoteend-->

> The writer Evan Malmgren recommends a cooperatively owned data trust that grants voting shares to its members, who in turn elect a leadership that is empowered to negotiate over the terms of data use with other entities.

<!--quoteend-->

> These sketches are a good start, but they still bear the stamp of the internet they are trying to escape. The aspiration to create a decentralized Twitter or a worker-run Uber is a step in the right direction, but it remains imprisoned within an enemy paradigm.

<!--quoteend-->

> For such efforts to be successful, they must blur the line between technology’s creators and its users, and eventually aspire to make the two categories indistinguishable.

<!--quoteend-->

> On the other hand, they sketched an image of a new society, one in which “industrial growth should be regulated according to ethical priorities and the pursuit of profit be subordinated to human needs,” writes Thompson.

