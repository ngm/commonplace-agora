# Amstrad CPC 464

The first computer we had in our house.

-   CPU: Zilog Z80 @ 4 MHz
-   Memory: 64K
-   OS: AMSDOS
-   https://en.wikipedia.org/wiki/Amstrad_CPC_464

My Mum got it on hire purchase and was paying it off for a long time.

![[2020-03-21_22-13-19_serveimage.jpg]]

I remember typing out programs with my Mum from some big book of BASIC that came with it.  I remember doing the program where you make the background change colour a lot, and where you get the same piece of text to print out indefinitely.  

![[2020-03-21_22-11-58_serveimage.jpg]]

It had a tape deck and made awesome sounds when loading off the tape.

The games I remember playing the most are Galactic Plague, Harrier Attack, Sultan's Maze, and <span class="underline">possibly</span> Bard's Tale (but that one might have been on the Amiga).  Rampage?  Also Gauntlet!  Playing that with my brother, unless again it was for the Amiga.  But I've a feeling it was the Amstrad.

-   [[user manual]]

