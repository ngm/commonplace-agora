# Coronavirus is the perfect disaster for 'Disaster Capitalism'

-   source: [Naomi Klein on Coronavirus and Disaster Capitalism - VICE](https://www.vice.com/en_us/article/5dmqyk/naomi-klein-interview-on-coronavirus-and-disaster-capitalism-shock-doctrine)
-   tags: [[reading]]


## What is it?

[[Disaster capitalism]] "describes the way **private industries spring up to directly profit from large-scale crises**."


## What's going to happen with coronavirus?

coronavirus is a large scale crisis.

> It’s going to be exploited to **bail out industries that are at the heart of most extreme crises that we face**, like the climate crisis: the airline industry, the gas and oil industry, the cruise industry—they want to prop all of this up.


## What to do

> **we have to fight harder than ever before for universal health care, universal child care, paid sick leave** —it’s all intimately connected.

<!--quoteend-->

> So be aware of that and think about how, instead of hoarding and thinking about how you can take care of yourself and your family, you can pivot to sharing with your neighbors and checking in on the people who are most vulnerable.

<!--quoteend-->

> It’s the worst-case scenario, especially combined with the fact that the U.S. doesn’t have a national health care program and its protections for workers are abysmal.

