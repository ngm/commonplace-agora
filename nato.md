# Nato

> Since 1991, Nato has absorbed 11 eastern European countries and three former Soviet republics. Even before Vladimir Putin became president in 2000, [[Russia]] took a dim view of this.
> 
> [Is there any justification for Putin’s war? | Ukraine | The Guardian](https://www.theguardian.com/world/2022/mar/13/is-there-any-justification-for-putins-war)

