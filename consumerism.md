# consumerism

> As for consumption, the environmental movement has had a tendency to moralize at people for their consumer choices, but I want to underline what contemporary patterns of consumption take away from us.
> 
> &#x2013; [[For a Red Zoopolis]]

<!--quoteend-->

> the majority of us did not design systems of planned obsolescence, or fast fashion, or long carbon-intensive food supply chains. Nor did we design the marketing practices that, as Benjamin Barber writes in Consumed, deliberately teach us to think and desire as infants, with no talent for deferred gratification and no orientation to the future
> 
> &#x2013; [[For a Red Zoopolis]]

