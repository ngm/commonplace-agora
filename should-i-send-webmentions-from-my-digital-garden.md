# Should I send webmentions from my digital garden?

Contemplating whether I should send [[webmentions]] from my [[digital garden]].

Maybe, maybe not.

There's plenty of ways to send webmentions from a static site, and plenty of people doing it.

So I could, but I wonder if I **should**.

Perhaps webmentions should only be sent when I post a more considered long-form article, or when I post something to my stream.

Not sure.

