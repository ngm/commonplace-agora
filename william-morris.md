# William Morris

Was inspired by the [[Paris Commune]].

> William Morris (1834-1896) was a British artist, activist and communist. He inspired the arts and crafts movement, founded the Socialist League, and played a major role in the League’s newspaper The Commonweal.
> 
> &#x2013; [[The Utopian Internet, Computing, Communication, and Concrete Utopias]]

<!--quoteend-->

> William Morris, one of the wealthiest men in Victorian England, spent his life translating Viking sagas, weaving, writing, and fighting for a socialist revolution
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> [[Friedrich Engels]] once mocked William Morris as a ‘sentimental socialist’, an insult Morris wore as a badge of honour (‘I am a sentimentalist … and I am proud of the title
> 
> &#x2013; [[Half-Earth Socialism]]

