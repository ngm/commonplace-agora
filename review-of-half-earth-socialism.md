# Review of Half-Earth Socialism

Status
: [[seedling]]

My review of [[Half-Earth Socialism]].

Possible titles:

-   "I love it when a plan comes together: review of Half-Earth Socialism"


## intro

Can we geoengineer our way out of climate catastrophe?  It seems unlikely.  Our home, the biosphere, is the most complex of systems.  What possible hope do we have of riding such a beast without being thrown off?  Perhaps we shouldn't even try.  Instead, we should reign in ourselves and stop stressing the Earth to its limits. If we consider ourselves to be the helmspeople of Spaceship Earth, then perhaps the best thing we can do is to stop sticking our oar in.  Earth can ably look after itself, and us.  We just need to not break it.  The way that we can reign ourselves in is by planning a different complex system altogether - the global economy.  We restrain Moloch rather than Gaia.  Given that this is a monster of our own creation, perhaps we've got a better chance of changing its path. Hands off the Earth; hands on the economy.

This is roughly the premise of Half-Earth Socialism.  It is an audacious book stuffed full of ideas.  It is radical and erudite.  I thoroughly enjoyed it, and would recommend it as an addition to any eco-socialist reading list.  I love it when a plan comes together.


## The philosophical, the material, the technical, and the imaginative


### Inventing a future

The book is unabashedly utopian, quite happily presenting itself as a recipe for the cookshop of the future.

> We invite everyone from all liberatory traditions to join us in the revolutionary kitchen to think up many new recipes and work together to realize them

At both ends of the book are passages of speculative futures.  Two futures: one dystopia and one utopia.  The dystopia is not so much Mad Max, more Don't Look Up.  It's the mundane yet dangerous reality of where we and the planet will be if toothless environmentalism allows the current path of money-worship to continue.  Lacking in either the appetite or the bite needed for radical systemic change, by 2047 this [[green capitalism]], content with business as usual, leads us to a very unjust and unstable future. Both material resources and the effects of climate breakdown are meted out inequitably. The reification of growth, and the 'fixing' of the climate crisis by turning it in to a commodity to be fixed by market forces do not end well.

Another future is possible, and is presented at the end of the book.  Inspired by [[News from Nowhere]], the speculative protagonist awakens unexpectedly one day in a very different 2047.  In this future, [[eco-socialism]] is dominant.  There's a central planning bureau in Cuba, overseeing the global economy to provide prosperity for all while staying within planetary boundaries.   States of America are being rewilded, everyone's a vegan, and all have time to fit self-actualisation around a little bit of democratically allocated work. We prosper, and so does the Earth.

Utopian - Yes.  Possible?  It has to be.  Another world is necessary.  How?  That is the question of our times.

While it may be utopian, following [[Otto Neurath]] Vettese and Pendergrass present a "[[scientific utopia]]" - a dream, perhaps, but one with scientific rigour.  Vettese and Pendergast present a very well-reasoned proposition, with bold ideas for how we might plan to reach this utopia, and some bold proposals of the radical changes one version of this plan might require.


### Philosophies of nature

Before getting in to the material aspects of the Half-Earth Socialist programme, the authors present their overarching philosophy, in particular exploring the idea of the 'knowability of nature'.

They explore three historical threads of environmentalism - [[Prometheanism]] (the humanisation of nature) vs [[Malthusianism]] (the dehumanisation of humans) vs [[Jennerite ecological scepticism]] (the respect of nature).

The authors choose the Jennerite approach:  keep our oars out.  Too much interference with nature (whether it be 'negative' through exploitation or 'positive' through geoengineering), an unknowable system, can only cause problems. They present a litany of these problems - climate change, biodiversity loss, [[Zoonotic disease]]s, [do they mention inequity between humans?]

[[Biosphere 2]] is a great example of the complexity of trying to control nature.  As an attempt at running a complex natural system, one just a sliver of a fraction of the size of its namesake, Biosphere 2 failed entirely.  To try to control one orders of magnitude larger must surely be folly.  And it's not so much that the attempt to control it failed - it's that all attempts at geoengineering invariably made things worse. 

If we can't control nature, what then do we do?  We try to reign in the only part of it that we have much control over - ourselves.  We slay Moloch.  We plan human activity to not overstep the mark of planetary boundaries, and we trust that Gaia, the great homeostat, can take care of itself.


### Material

What solutions would reduce our impact on the biosphere?  Building on the Jennerite philosophy, the authors favour those which keep our oars out of it.

Main nub of it:

-   [[Rewilding]] half the earth - [[Half-Earth]]
-   [[Veganism]]
-   A rapid transition to [[renewable energy]]

They are very critical [[geoengineering]] - the act of trying to control the environmental through technology - a form of [[Prometheanism]].

In a nutshell: if the authors were the helmspeople of Spaceship Earth, then their plan would be to stop sticking our oar in as much as possible.


### I love it when a plan comes together

[[Neoliberalism]] would suggest that planning human economic activity is not possible. The authors delve in to the [[socialist calculation debate]], with a tour through it including [[in natura planning]] and [[Otto Neurath]], [[Leonid Kantorovich]] and [[Linear programming]], [[Socialist Chile]], [[Project Cybersyn]] and the [[Viable system model]] and the latest state-of-the-art in meteorology and environmental modelling.

That's how we plan.  The authors say once decided on certain constraints, it can be democratically decided as to what we should actually do to meet them.  Muliple different approaches may be taken here - e.g. some may take a path of accelerationism,  geoengineering, techno-solutions, etc.  There is scope for debate.  But the authors also present strongly some of their own ideas - this is the [[Half-Earth Socialism]] of the title.


## Transition

As Vettese and Pendegrass make clear, they are quite happy to be utopian and focused on a programme for the future.  The book contains very little on a [[revolutionary transition]].  Beyond a call for a broad coalition, a "movement of movements", and a few sentences on how that coalition might gain power, the reader will need to go elsewhere for strategy and tactics on how to bring forth the programme outlined.


## pros

-   Clear proposals for how to avoid climate catastrophe.


## cons

-   issues around rewilding?  They cover it well but the link to Half-Earth needs explaining.
-   the game begins following a revolution.  the book doesn't tell us how this revolution will come about (i don't recall, but should reread)
    -   perhaps this is a pitfall of the utopian thinking.  how do we bring about the path we wish to take?
-   isn't deciding not to plan nature kind of a plan of sorts?  I guess it's a question of level of detail.


## summary

