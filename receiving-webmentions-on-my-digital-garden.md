# Receiving webmentions on my digital garden

Would be fun to be able to receive comments/annotations to my digital garden via webmentions.

As it is a static site, I'll do it with webmentions.io and webmentions.js


## webmention.io

OK, so webmentions.io uses web log-in.

Alrighty, I'll do that via relme auth.

First I'll need to add some rel=me links.

Ha, sweet, I didn't realise rel me login can work via email address!

That's so cool.  OK, so I added

```nil
<link rel="me" href="mailto:my@email.foo" />
```

to my head section for  commonplace.doubleloop.net

and then signed in at webmention.io.

Now I can add

> &lt;link rel="webmention" href="https://webmention.io/commonplace.doubleloop.net/webmention" /&gt;
> &lt;link rel="pingback" href="https://webmention.io/commonplace.doubleloop.net/xmlrpc" /&gt;

to my head section.

ok, let's give it a spin!


## sending a webmention

i can send webmentions from my existing indieweb blog at doubleloop.net

I sent one:

![[sending_a_webmention/2022-03-04_18-10-09_screenshot.png]]

and it arrived!

![[sending_a_webmention/2022-03-04_18-09-34_screenshot.png]]

It doesn't work with my wikilink rewriting stuff.  So I need to use the full URL (or fix the rewriting stuff) if I want to mention from my own site.  Which wasn't really the point, but actually could be interesting&#x2026;


## displaying webmentions

ok so now i'll try displaying received webmentions with webmentions.js

I followed the instructions at https://github.com/PlaidWeb/webmention.js

-   include script
-   add a div for webmentions
-   pop a teeny bit of styling in

![[displaying_webmentions/2022-03-04_18-20-27_screenshot.png]]

And it works!

That took about 40 minutes in total, super cool.

Needs some styling and tweaking and adding to all pages now.  But that's pretty epic.

Some thoughts:

-   how do I moderate mentions
-   I wonder if you can self host webmention.io

