# Agent-based modelling of predator-prey dynamics

My final year project of my undergraduate degree was comparing the Lotka-Volterra equations to an agent-based model of predator-prey dynamics.  

It wasn't so much predator-prey dynamics that interested me - it was the [[agent-based modelling]].  Though I do like when software has some [[link back to nature]], one way or another.

I really enjoyed it, from memory.  I definitely had more of an affinity for the agent-based way of doing things, rather than the differential equation approach.

I got an award for best final year project.

