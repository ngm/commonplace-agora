# Hans Widmer

> P.M. is the pseudonym of the Swiss author Hans Widmer (born in 1947). Besides writing utopian novels, other literature, theatre performances, and radio plays, P.M. has also been an activist in autonomous and [[eco-socialist]] projects and movements. [[bolo'bolo]] is P.M.’s most wellknown book. Reminiscent of Kroptokin, bolo’bolo outlines how a future grassroots communist society without capital and the state could look. [[Kartoffeln und Computer]] (Potatoes and Computers) is an update of bolo’bolo’s vision written almost thirty years later.

