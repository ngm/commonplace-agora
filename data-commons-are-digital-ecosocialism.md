# Data commons are digital ecosocialism

A
: [[claim]]

[[data commons]] are examples of [[digital ecosocialism]].


## Because

-   they prioritise public interest over commercial exploitation
-   they utilise data for social and environmental benefits
-   they promote collective governance and sustainable management of data

