# org-brain

I tried org-brain a few times for my personal wiki, but it never really clicked for me.  Plus seemed to add quick a bit of extra cruft that locks you into using brain a bit.  And they broke my org-publish. But the concept mapping idea seems close to what I want, so I'll keep keeping my eye on it.  Breaking down ideas into smaller interlinked thoughts, like TiddlyWiki's ideas.  

[[org-roam]] has worked better for me so far.

