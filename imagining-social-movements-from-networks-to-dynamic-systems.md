# Imagining social movements: from networks to dynamic systems

Found at
: https://www.opendemocracy.net/en/opendemocracyuk/imagining-social-movements-from-networks-to-dynamic-systems/

Written by
: [[Graham Jones]]

> In a world of increasingly tangled social networks, it was inevitable that this would begin to shape how we see social movements. From the ecology of organisations ([[organisational ecology]]) in the Spanish anti-austerity struggle, to the rhetoric around [[Momentum]] and the Corbyn campaign, the words 'grassroots' and 'network' have gained in currency. Yet beyond this we still seem to lack models for understanding the dynamics of these systems that might help us to organise them more effectively. It's vital therefore that we develop analytical tools capable of dealing with their complexity, that remain accessible to those outside of niche academic circles.

