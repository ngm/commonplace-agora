# Ethical consumerism

The need for system change doesn't obviate anyone from individual responsibility.


## [We Can't Do It Ourselves](https://solar.lowtechmagazine.com/2018/07/we-cant-do-it-ourselves.html)

> How to live a more sustainable life? By placing responsibility squarely on the individual, attention is deflected away from the many institutions involved in structuring possible courses of action.

This is a very nice analysis of the shortcomings of behaviour change at the level of the individual.  Better to focus on systemic failings than guilt-tripping people for making a wrong choice, when it very often isn’t really a choice at all.

> When the focus is on practices, the so-called “value-action gap” can no longer be interpreted as evidence of individual ethical shortcomings or individual inertia. Rather, the gap between people’s attitudes and their “behaviour” is due to systemic issues: individuals live in a society that makes many pro-environmental arrangements rather unlikely.


## [Newco Shift | Our Consumption Model Is Broken. Here’s How To Build A New One.](https://shift.newco.co/2017/10/18/Our-Consumption-Model-Is-Broken--Heres-How-To-Build-A-New-One-/)


### Summary

Our consumption patterns have huge environmental, social and health impacts.
Consumption is a corporate strategy.
We need a systemic change, not just tweaks to consumerism.


### Thoughts

Good article, backed up by plenty of stats.  But it’s stronger on the “our consumption model is broken” part, a bit weak on “here’s how to build a new one.” The plan for system change doesn’t feel very fleshed out, with some loose suggestions, and not much as to how we actually achieve the suggestions.  Maybe that’s explored further elsewhere.


### Notes

Consumption is causing ecological overspend.

We’re heading towards 2 planets worth of consumption for 2030 (where does this figure come from?)

Our modern lifestyle is root cause of climate crisis.

“global livestock industry produces more emissions than all cars, planes, trains, and ships combined”

“By 2025, two-thirds of the world’s population may face water shortages.”

“5,300 gallons of water to product 1kg of cotton”

“by 2050, oceans will contain more plastic than fish”

A lot of bleak statistics.  Places the blame with consumerism.

“consumption accounts for 70 percent of US GDP”

Consumerism is “an aggresive device of corporate survival”.

We have way more stuff than we need.

Our excesses exploit people around the world.

Basic biology is tricked to make us consume even when we may know it’s not in our best interest.

“Alternatives such as ethical consumerism or minimalism are unlikely to impact enough people.”

“Choosing sustainable options requires an investment in time and money that only a small minority of people can afford.”

