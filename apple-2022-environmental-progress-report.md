# Apple 2022 Environmental Progress Report

[[Apple]].

> We believe long-lasting products are best for the environment. **We also believe products that minimize the need for repair or replacement encourage our customers to come back to Apple**. We design our products with this goal in mind.

"We also believe products that minimize the need for repair or replacement encourage our customers to come back to Apple."  Hmm.

> Each improvement is part of our effort to keep our products in use and out of the repair shop.

<!--quoteend-->

> We design our products to minimize the need for repair in the first place.

<!--quoteend-->

> Our goal is to avoid the interruption a repair may cause for our customers, which is why we optimize designs for durability.

