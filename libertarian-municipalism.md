# Libertarian municipalism

> Libertarian municipalism is an integral part of the Communalist framework, indeed its [[praxis]], just as [[Communalism]] as a systematic body of revolutionary thought is meaningless without libertarian municipalism.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Citizens should be freed of their particularistic identity as workers, specialists, and individuals concerned primarily with their own particularistic interests
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Communalism seeks to recapture the meaning of politics in its broadest, most emancipatory sense, indeed, to fulfill the historic potential of the municipality as the developmental arena of mind and discourse.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> marked contrast to the various kinds of communitarian enterprises favored by many self-designated anarchists, such as “people’s” garages, print shops, food co-ops, and backyard gardens, adherents of Communalism mobilize themselves to electorally engage in a potentially important center of power—the municipal council—and try to compel it to create legislatively potent neighborhood [[assemblies]].
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Freed of domination as well as material exploitation—indeed, re-created as a rational arena for human creativity in all spheres of life—the municipality becomes the ethical space for the good life
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> In its libertarian municipalist program, Communalism resolutely seeks to eliminate statist municipal structures and replace them with the institutions of a libertarian polity
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> It seeks to radically restructure cities’ governing institutions into popular democratic assemblies based on neighborhoods, towns, and villages. In these popular assemblies, citizens—including the middle classes as well as the working classes—deal with community affairs on a face-to-face basis, making policy decisions in a direct democracy and giving reality to the ideal of a humanistic, rational society
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Municipal assemblies would become not only vital arenas for civic life and decision-making but centers where the shadowy world of economic logistics, properly coordinated production, and civic operations would be demystified and opened to the scrutiny and participation of the citizenry as a whole
> 
> &#x2013; [[The Communalist Project]]

