# Repairability standard

-   tags:[[right to repair]] | [[iFixit]]

Develop a repair index that includes pricing as a criteria and covers a wide range of products to allow consumers to be informed about the repairability of products at the time of purchase


## French repairability index

A repairability index in place in France since January 2021.

> As of January 2021, France will implement a repair index on smartphones, TVs, laptops, washing machines and lawn moaners to inform consumers of how repairable a product is. The score will be calculated based on criteria such as: ease of disassembly, price and availability of spare parts and access to repair information. Next year, we'll be fighting to bring a similar tool everywhere in Europe! 
> 
> &#x2013; [Right to Repair: why all the fuss? - Right to Repair Europe](https://repair.eu/right-to-repair-why-the-fuss/?link_id=0&can_id=af30c14aafa580e36c211df17849c3b4&source=email-join-our-webinar-on-the-french-repairability-index&email_referrer=email_1048617&email_subject=join-our-webinar-on-the-french-repairability-index) 

<!--quoteend-->

> When [[Samsung]] commissioned a study to check whether the French repairability scores were meaningful, it didn’t just find the scorecards were handy — it found a staggering 80 percent of respondents would be willing to give up their favorite brand for a product that scored higher.
> 
> &#x2013; [[The era of fixing your own phone has nearly arrived]]

<!--quoteend-->

> “There have been extensive studies done on the scorecard and it’s working," says Wiens. “It’s driving behavior, it’s shifting consumer buying patterns."
> 
> &#x2013; [[The era of fixing your own phone has nearly arrived]]


### Links

-   [Fixers Know What ‘Repairable’ Means—Now There’s a Standard for It - iFixit](https://www.ifixit.com/News/35879/repairability-standard-en45554)

-   [French repairability index: what to expect in January? - Right to Repair Europe](https://repair.eu/news/french-repairability-index-what-to-expect-in-january/)

-   [Restart Podcast Ep. 61: Introducing the French Repairability Index - The Rest&#x2026;](https://therestartproject.org/podcast/french-repairability-index/)


### Limitations


#### Manufacturers are not required to be fully transparent

> This is because the finer detail of how the scores were reached is not available to the public. Vasseur argues that more information needs to be available to consumers for them to come to purchasing decisions – information that will relate to their real-life experience regarding repair.
> 
> &#x2013; [Restart Podcast Ep. 61: Introducing the French Repairability Index - The Rest&#x2026;](https://therestartproject.org/podcast/french-repairability-index/) 


#### Self-declaration

> The government decided the rating criteria of the index. However, it is the manufacturers themselves that report how their product stacks up. At the moment, there is no formal system in place to check how truthful companies are being.
> 
> &#x2013; [Restart Podcast Ep. 61: Introducing the French Repairability Index - The Rest&#x2026;](https://therestartproject.org/podcast/french-repairability-index/) 

