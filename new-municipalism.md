# new municipalism

> We can also draw inspiration from the ‘new municipalism’ movement represented by cities across the world who gathered for the Fearless Cities summits.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> This movement argues that municipality is a strategically important site for experimenting with how citizens can take back power
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> One of its core demands is for a decentralisation and devolution of power away from the nation state and towards local authorities who can respond better to the specific needs of their citizens. However, this is not simply a parochial ‘localism’ that forgoes addressing global problems and the international dimensions of corporate power. It is a transnational movement that aims to demonstrate how local issues that immediately affect citizens are connected to similar issues affecting others across the globe.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> New municipalists do not simply want to transfer power from one geographic location to another – they aim to transform how that power operates. The movement advocates creating a qualitative shift towards more participatory forms of self-governing community life.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Both the Cleveland and [[Preston models]], like [[Cooperation Jackson]] in the US and [[Barcelona en Comú]] (Barcelona in Common), are examples of what has been called ‘the new municipalism’ or ‘remunicipalism’.
> 
> &#x2013; [[The Care Manifesto]]

<!--quoteend-->

> While there are political complexities to these forms, the key feature of the new municipalism is that it breaks with the neoliberal system of siphoning off public money to feed remote multinational corporations.
> 
> &#x2013; [[The Care Manifesto]]

