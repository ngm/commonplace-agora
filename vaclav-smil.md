# Vaclav Smil

> But what do we really have to lose? As Smil points out, a vast amount of current energy use in affluent countries is wasted on environmentally destructive activities whose elimination would only improve living standards
> 
> &#x2013; [[For a Red Zoopolis]]

