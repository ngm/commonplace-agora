# Glitch and détournement

[[Détournement]] being a way of viewing things afresh.  I guess it is more of a manual hijacking though.  [[Glitch]] is perhaps embracing the accidental hijacking of something, something that can also show something in new lights.

