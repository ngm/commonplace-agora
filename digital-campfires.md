# digital campfires

Somewhere in between the [[stream]] and the [[garden]]?  A place for conversations.

> Connection forming, thinking out loud, self referencing and connection forming. This builds muscle, helps me articulate my thinking and is the connective tissue between ideas, people and more. 
> 
> https://tomcritchlow.com/2018/10/10/of-gardens-and-wikis/

