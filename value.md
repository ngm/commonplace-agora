# Value

> The predominant explanation of value in mainstream economics today is the neoclassical theory of “[[marginalism]].” It assumes that a commodity’s worth is determined by laws of [[supply and demand]], mediated by what they term “marginal utility"
> 
> &#x2013; [[A People's Guide to Capitalism]]

[[Labour theory of value]].

> One type of value does not cause the other (how useful an item is does not determine its value on the market; nor does its value on the market determine how useful an item may be to us
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> In fact, it’s precisely the fact that an item has no use-value to its producer that allows it to be an exchange-value
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Simply put, you can either live in your house, or you can sell your house. You can’t have your cake and eat it too.
> 
> &#x2013; [[A People's Guide to Capitalism]]

^ what about non-rivalrous goods?

> The world was sweaty, the world was dusty, but it all made sense, because beneath the thousand thousand physical differences of things, [[economics]] saw one substance which mattered, perpetually being created and destroyed, being distributed, being poured from vessel to vessel, and in the process keeping the whole of human society in motion. It wasn’t [[money]], this one common element shining through all its temporary disguises; money only expressed it. It wasn’t [[labour]] either, though labour created it. It was value. Value shone in material things once labour had made them useful, and then they could indeed be used or, since value gave the world a common denominator, exchanged for other useful things; which might look as dissimilar from one another as a trained elephant did from a cut diamond, and consequently as hard to compare, yet which, just then, contained equal value for their possessors, the proof being that they were willing to make the exchange.
> 
> &#x2013; [[Red Plenty]]

