# Energy company profits

> The UK firm [[BP]] was accused of "unfettered profiteering" after it said on Tuesday underlying profits had tripled to $8.5bn (£6.9bn) between April and June, thanks to high oil prices. It was its biggest quarterly profit in 14 years and BP said it would hand out nearly £4bn to shareholders as a result.
> 
> &#x2013; [Big oil’s quarterly profits hit £50bn as UK braces for even higher energy bil&#x2026;](https://www.theguardian.com/business/2022/aug/02/big-oil-profits-energy-bills-windfall-tax)

Such huge profits during the [[cost of living crisis]] is unconscionable.

