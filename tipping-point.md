# Tipping point

> This is a threshold beyond which lie changes that may be irreversible. A relatively small change could catalyse a shift from one system to a completely new state. An ice sheet collapse in West Antarctica for instance could raise sea levels, causing coastal flooding in other parts of the world. Similarly, deforestation or drought could spark a dieback of trees and nudge a moist tropical climate into a drier state no longer able to support a rainforest
> 
> &#x2013; Down to Earth newsletter

<!--quoteend-->

> The [[climate crisis]] has driven the world to the brink of multiple “disastrous” tipping points, according to a major study
> 
> &#x2013; [World on brink of five ‘disastrous’ climate tipping points, study finds | Cli&#x2026;](https://www.theguardian.com/environment/2022/sep/08/world-on-brink-five-climate-tipping-points-study-finds)

<!--quoteend-->

> The nine global tipping points identified are: the collapse of the Greenland, west Antarctic and two parts of the east Antarctic ice sheets, the partial and total collapse of Amoc, Amazon dieback, permafrost collapse and winter sea ice loss in the Arctic.
> 
> &#x2013; [World on brink of five ‘disastrous’ climate tipping points, study finds | Cli&#x2026;](https://www.theguardian.com/environment/2022/sep/08/world-on-brink-five-climate-tipping-points-study-finds)

<!--quoteend-->

> Every fraction of a degree that we stop beyond 1.5C reduces the likelihood of hitting more tipping points.”
> 
> &#x2013; [World on brink of five ‘disastrous’ climate tipping points, study finds | Cli&#x2026;](https://www.theguardian.com/environment/2022/sep/08/world-on-brink-five-climate-tipping-points-study-finds)

