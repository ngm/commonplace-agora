# How not to deal with big tech

> Progressive criticisms of the tech sector are often drawn from a mainstream capitalist framework centered around [[antitrust]], human rights and worker well-being.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> Formulated by elite scholars, journalists, think tanks and policymakers in the Global North, they advance a US-Eurocentric reformist agenda that assumes the continuation of capitalism, Western imperialism and economic growth.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

