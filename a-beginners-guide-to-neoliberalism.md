# A Beginner's Guide To Neoliberalism

URL
: https://neweconomics.org/2019/05/a-beginners-guide-to-neoliberalism

Nice intro series to [[neoliberalism]].

What it is, where did it come from, how did it take hold, where we're at with it.

Some things I remember off the top of my head:

-   Neoliberalism is a particular flavour of capitalism.  Contrast with e.g. Keynesianism, another flavour.
-   Key tenets: free markets, deregulation, privatisation, individual freedoms.
-   Popularised by Hayek; the Montpellerin Society.
-   First tried out in Chile following the coup.
-   Took hold in UK with Thatcher, US with Reagan.

