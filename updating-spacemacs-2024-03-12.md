# Updating spacemacs 2024-03-12

Various errors, culminating in a requirement of Emacs 28 from org-projectile.

I'm on Emacs 27.2.

Apparently last time I installed from source ([[Installing Emacs from source]]).

Maybe I'll give that a quick go.

Alternatively I can get it from a Mint PPA.

But that might require me to upgrade to Mint 21 (currently on 20.3).

Ho hum.  Things are just about working, so I'll just leave it for now.

Update: following updating Emacs to 28 ([[Updating to Emacs 28 on Linux Mint]]), all seems to be working again.

