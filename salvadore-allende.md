# Salvadore Allende

> Allende proposed a political third way, something different from the politics and ideology of either superpower. Allende wanted to make Chile a socialist nation, but he also wanted change to occur peacefully and in a way that respected the nation’s existing democratic processes and institutions
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Moving property ownership from foreign multinationals and the Chilean oligarchy to the state, redistributing income, and creating mechanisms for worker participation were among the top priorities of the Allende government
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Allende has also been portrayed as a villain who destroyed the Chilean economy and brought on widespread consumer shortages. Other interpretations have portrayed the former president as a conflicted and contradictory figure who loved women and bourgeois luxuries even as his political dream called for the creation of a more just society. Allende’s presidency exacerbated political and class divisions already present in Chilean society, and members of these different groups experienced the Allende period, and the Pinochet dictatorship that followed, in different ways. The scars from these memories have yet to heal completely and continue to shape interpretations and understandings of Allende’s presidency
> 
> &#x2013; [[Cybernetic Revolutionaries]]

