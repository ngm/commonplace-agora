# Inner garden

Like Bill Seitz describes here - http://webseitz.fluxent.com/wiki/TendingYourInnerAndOuterDigitalGardens - I have a completely private 'inner garden' that is for personal stuff.  It never gets published to my public digital garden.

It's really more of a personal todo system, not so much a garden.  But I do use org-roam for parts of it.

