# Base-building

> One proposed solution for reaching “the masses” which has recently gained popularity is the concept of “base-building.” Sometimes presented as a revolutionary strategy, other times as a way to “build working-class power” in the abstract, base-building has become a buzzword in DSA and among other groupings on the left.
> 
> &#x2013; [Base-building or Bolshevism? | Other | History &amp; Theory](https://www.marxist.com/base-building-or-bolshevism.htm)

