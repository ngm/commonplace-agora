# digital psychogeography

Thanks to a couple of recent [[thinking prompts]] from [[Panda]] and [[Graham]], [[psychogeography]] was on my mind.  And lazily putting the word 'digital' in front of another always seems to be on my mind&#x2026;

&#x2026;So I have been thinking about the idea of digital psychogeography, [[drifting]] through [[hypertext]].  Perhaps as a means of rediscovering lost history, and maybe as a way to break out of self- and algorithmically-prescribed [[filter bubbles]].

And well what do you know, there's already a [rich](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) [vein](https://www.nytimes.com/2012/02/05/opinion/sunday/the-death-of-the-cyberflaneur.html) of [articles](https://www.theatlantic.com/technology/archive/2012/02/the-life-of-the-cyberfl-neur/252687/) out there on the idea of being a [[cyberflâneur]].

(When I think cyberflâneur, I think '[[Kicks Condor]]').

See: [[psychogeography]].

