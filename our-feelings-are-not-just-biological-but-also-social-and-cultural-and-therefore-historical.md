# Our feelings are not just biological, but also social and cultural and therefore historical

> “If things feel” like this or that: these feelings too are linked to periodization, because our feelings are not just biological, but also social and cultural and therefore historical. [[Raymond Williams]] called this cultural shaping a “[[structure of feeling]],” and this is a very useful concept for trying to comprehend differences in cultures through time.
> 
> &#x2013; [[The Ministry for the Future]]

