# Radical Technologies

A
: [[book]]

Author
: [[Adam Greenfield]]

URL
: https://www.versobooks.com/books/2742-radical-technologies

Publisher
: [[Verso]]

Really a fantastic book.  An absolute banger.  Gorgeously written, erudite and thoughtful on modern technologies.

Thorough, fair critique of the technologies you see bandied around as part of the ‘Fourth Industrial Revolution’ – smartphones, IoT, AR/VR, blockchain, digital fabrication, automation, ML/AI.

Looks for the positives but mostly damning - the power structures behind the technologies very rarely allow them to be liberatory. All still right on point 5 years since publication.

The last two chapters on the Stacks, possible futures and tactics are gold.
https://www.versobooks.com/books/2742-radical-technologies

The technologies covered:

-   [[smartphones]]
-   [[Internet of Things]]
-   [[augmented reality]]
-   [[digital fabrication]]
-   [[cryptocurrency]]
-   [[Blockchain]]
-   [[automation]]
-   [[machine learning]]
-   [[artificial intelligence]]


## Digital fabrication

Adam is very positive about the potential of decentralised means of production possible through digital fabrication.  However, also very aware of the barriers that need overcoming before we get there.


## Cryptocurrency and blockchain

Re-reading in 2022, still gives the best intro to cryptocurrency for me - where it came from, why it was created, the political / economic motivations behind the creators, what are its problems, etc.  And with blockchain also - what the promises made of second-generation blockchains for governance and democracy will likely not live up to the hype.


## Automation


## Raw notes

> Insight into their functioning is distributed unequally across society, as is the power to intervene meaningfully in their design.

Technology needs to be more democratic.  Governable.

> And we’ll pay particular attention to the ways in which these allegedly disruptive technologies leave existing modes of domination mostly intact, asking if they can ever truly be turned to liberatory ends.

Page 48 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:46]</span></span>:
Everyone with a smartphone has, by definition, a free, continuously zoomable, self-updating, high-resolution map of every part of the populated surface of the Earth that goes with them wherever they go, and this is in itself an epochal development.

Page 48 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:46]</span></span>:
Most profoundly of all—and it’s worth pausing to savor this—they are the first maps in human history that follow our movements and tell us where we are on them in real time.

Page 48 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:47]</span></span>:
It’s dizzying to contemplate everything involved in that achievement. It fuses globally dispersed infrastructures of vertiginous scale and expense—
● mapping and geolocation is a megastack.vi feel like most stacks bottom out with vast state or corporate ownership.

Page 50 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:51]</span></span>:
the networked condition

Page 50 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:52]</span></span>:
we become reliant on access to the network to accomplish ordinary goals.

Page 51 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 05:52]</span></span>:
the performance of everyday life as mediated by the smartphone depends on a vast and elaborate infrastructure that is ordinarily invisible to us.
● agency is being able to function while disconnected and offline. or at least p2p. reclaiming the stack.

Page 51 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:03]</span></span>:
the moment-to-moment flow of our experience rests vitally on the smooth interfunctioning of all the many parts of this infrastructure—an extraordinarily heterogeneous and unstable meshwork, in which cellular base stations, undersea cables, and microwave relays are all invoked in what seems like the simplest and most straightforward tasks we perform with the

Page 51 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:03]</span></span>:
The very first lesson of mapping on the smartphone, then, is that the handset is primarily a tangible way of engaging something much subtler and harder to discern, on which we have suddenly become reliant and over which we have virtually no meaningful control.

Page 52 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:04]</span></span>:
many users will continue to experience the technics of everyday life as bewildering, overwhelming, even hostile.

Page 53 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:05]</span></span>:
our sense of the world is subtly conditioned by information that is presented to us for interested reasons, and yet does not disclose that interest.

Page 54 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:07]</span></span>:
the seamless, all-but-unremarked-upon splicing of revenue-generating processes into ordinary behavior,

Page 54 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:08]</span></span>:
For in the world as we’ve made it, those who enjoy access to networked services are more capable than those without.

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:09]</span></span>:
impossible to use the device as intended without, in turn, surrendering data to it and the network beyond

Page 58 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:11]</span></span>:
not, we are straightforwardly trading our privacy for convenience

Page 59 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:13]</span></span>:
turn, that data will be captured and leveraged by any number of parties, including handset and operating system vendors, app developers, cellular service providers, and still others; those parties will be acting in their interests, which may only occasionally intersect our own; and it will be very, very difficult for us to exert any control over any of this
● libre alternatives less likely to do so.

Page 60 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:15]</span></span>:
The individual networked in this way is no longer the autonomous subject enshrined in liberal theory, not precisely. Our very selfhood is smeared out across a global mesh of nodes and links; all the aspects of our personality we think of as constituting who we are—our tastes, preferences, capabilities, desires—we owe to the fact of our connection with that mesh, and the selves and distant resources to which it binds us.

Page 61 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:18]</span></span>:
This is one of the costs of having a network organ, and the full-spectrum awareness it underwrites: a low-grade, persistent sense of the world and its suffering that we carry around at all times, that reaches us via texts and emails and Safety Check notices.

Page 62 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:19]</span></span>:
network connectivity now underwrites the achievement of virtually every other need on the Maslovian pyramid, to the extent that refugees recently arriving from warzones have been known to ask for a smartphone before anything else, food and shelter not excluded.

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:23]</span></span>:
It remains to be seen what kind of institutions and power relations we will devise as selves fully conscious of our interconnection with one another, though the horizontal turn in recent politics might furnish us with a clue.

Page 65 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:30]</span></span>:
The internetof things
A planetary mesh ofperception and response

Page 66 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:32]</span></span>:
the colonization of everyday life by information processing.

Page 67 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:32]</span></span>:
the ambition to raise awareness of some everyday circumstance to the network for analysis and response

Page 68 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-15 Tue 06:34]</span></span>:
the primary scales at which it appears to us: that of the body, that of the room, and that of public space in general

Page 68 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-16 Wed 00:21]</span></span>:
The quest to instrument the body, monitor its behavior and derive actionable insight from these soundings is known as the “quantified self”; the drive to render interior, domestic spaces visible to the network “the smart home”; and when this effort is extended to municipal scale, it is known as “the smart city.”

Page 72 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-16 Wed 00:40]</span></span>:
Soylent4, the flavorless nutrient slurry

Page 82 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-16 Wed 06:21]</span></span>:
I don’t think it’s unfair to say that at this moment in history, internet-of-things propositions are generally imagined, designed and architected by a group of people who have completely assimilated services like Uber, Airbnb and Venmo into their daily lives, at a time when Pew Research Center figures suggest that a very significant percentage of the population has never used (or even heard of) them.12 And all of their valuations get folded into the things they design. These propositions are normal to them, and so become normalized for everyone else as well.
● ict workers should ensure denocracy in democratic choices

Page 94 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-17 Thu 04:59]</span></span>:
The temperature in Amazon’s fulfillment center, passing-out hot in the summer months because leaving the doors open to let in a breeze would also admit some possibility of theft. The prevalence of miscarriages and cancers among workers in the plant where the gallium arsenide in the LED was made.
● intnl solidaity with workers is required

Page 96 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-17 Thu 05:03]</span></span>:
“smart city.” This is a place where the instrumentation of the urban fabric, and of all the people moving through the city, is driven by the desire to achieve a more efficient use of space, energy and other resources.

Page 104 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-17 Thu 05:14]</span></span>:
We might think of it as an unreconstructed logical positivism, which among other things holds that the world is in principle perfectly knowable, its contents enumerable and their relations capable of being meaningfully encoded in the state of a technical system, without bias or distortion.

Page 110 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 00:12]</span></span>:
In urban planning, the idea that certain kinds of challenges are susceptible to algorithmic resolution has a long pedigree.
● interedting to compsre to docialidt democratic plsnning

Page 111 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 00:19]</span></span>:
Quite simply, we need to understand that the authorship of an algorithm intended to guide the distribution of civic resources is itself an inherently political act.

Page 112 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 00:22]</span></span>:
“systems analysis” or “operations research,” was first applied to New York in a series of studies conducted between 1973 and 1975, in which RAND used FDNY incident response-time data to determine the optimal distribution of fire stations.

Page 117 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 03:21]</span></span>:
The internet of things in all of its manifestations so often seems like an attempt to paper over the voids between us, or slap a quick technical patch on all the places where capital has left us unable to care for one another.

Page 123 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 03:36]</span></span>:
Augmentedreality
An interactive overlayon the world

Page 128 <span class="timestamp-wrapper"><span class="timestamp">[2022-11-18 Fri 03:37]</span></span>:
AR has its conceptual roots in informational displays developed for military pilots in the early 1960s, when the performance of enemy fighter aircraft first began to overwhelm a human pilot’s ability to react to the environment in a sufficiently timely manner. In the fraught regime of jet-age dogfighting, even a momentary dip of the eyes to a dashboard-mounted instrument cluster could mean disaster. The solution was to project information about altitude, airspeed and the status of weapons and other critical aircraft systems onto a transparent pane aligned with the field of vision: a “head-up display.”


## Critical review


### what's the main argument?

That we need to look at technologies with a very critical eye.  That they are never neutral, and particularly in a neoliberal hegemony they will rarely end up being used for liberatory ends.


### what are the author's underlying assumption and implicit arguments?

There's an underlying assumption that capitalism is bad, and that technology that supports it is to be avoided.


### What theoretical perspectives do they use?

"What system or school of ideas based on critical analysis of previous theories and research?"

There were a good few references in the book.  I would say that politically Adam draws on anarchism / municipalism politically; comes from a critical STS viewpoint with regards to views of technology.  

