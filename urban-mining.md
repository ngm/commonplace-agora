# urban mining

Urban [[mining]].

> When we talk about urban mining, we're talking about mining what we have already made and brought into an urban context,” explains Jessika Richter, a researcher at Lund University, Sweden. “We can mine many of these same materials out of our products, so not all of them end up in landfill.”
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> “What enables urban mining is well-sorted [[waste streams]],” continues Richter. “We should sort our streams at collection, as the more it is separated when we get it back, the lower the cost. Same with [[landfill]] – the more sorted the landfill, the easier it is to mine.”
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> Elements found in e-waste, such as gold, silver, platinum, indium and gallium, are not only expensive but essential for greener future technology, including [[wind turbines]], [[solar panels]] and [[electric cars]]. If they end up in landfill, these materials can be highly hazardous, poisoning land and waterways. In 2019, a record 53.6 million metric tonnes of electronic waste was generated worldwide.
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> While traditional mining for raw materials tends to be energy and capital intensive, urban mining is labour intensive. 
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> “The cooperative movement in countries such as Colombia, Brazil, Argentina and elsewhere may be a source of inspiration for policymakers to turn to for lessons on how to improve urban mining,” Dias continues. “Support for organising, social protection and the design of inclusive urban policies is key to prevent exploitation of waste pickers.”
> 
> &#x2013; [[Can urban mining help to save the planet?]]

<!--quoteend-->

> Can those places be turned into sites of beauty and value, can the community have governance over those processes, as opposed to just a company coming in yet again?”

[[Self-governance]] is a part of it then.

