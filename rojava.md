# Rojava

I support the Kurds in Rojava who are building an egalitarian society in dire circumstances.  

It is based on [[communalism]] and [[social ecology]].

![[images/rojava.jpg]]

CC BY-NC-SA Autonomous Design Group   https://www.weareadg.org/rojava

> People around the world have been enormously inspired by the achievements of the Kurdish-led movement in Rojava, in particular, the gains in women’s liberation, LGBTQ liberation struggles, experiments in direct democracy and campaigns for ecological justice.
> &#x2013; [Solidarity against the Turkish invasion](https://www.rs21.org.uk/2019/10/11/solidarity-against-the-turkish-invasion/)    


### Turkish occupation

> If Turkey occupies Northern Syria, the social progress made in the region, including advances in women’s liberation and popular self-government, would be destroyed. We have already seen Turkey and the Islamist militias it backs reverse these gains in Afrin.
> 
> &#x2013; [Turkey’s War on Rojava](https://tribunemag.co.uk/2019/10/turkeys-war-on-rojava)


### Critiques

> This is notwithstanding criticisms of the ways in which the Kurdish PYD-YPG has suppressed other political groups among Syrian Kurds, making seemingly limited attempts at meaningful power-sharing in Arab-majority territory under its control, and its record of collaboration with both US and Russian military imperialism in Syria.
> 
> &#x2013; [Solidarity against the Turkish invasion](https://www.rs21.org.uk/2019/10/11/solidarity-against-the-turkish-invasion/)    


## Raqqa

-   Bookmarked: [The Women of Raqqa Are Rebuilding Their Future](https://jacobinmag.com/2021/05/raqqa-women-blooming-in-the-desert-film/)
    
    > Four years since [[Raqqa]] was liberated from ISIS, women are playing a leading role in rebuilding the [[Syria]]n city. Their activism shows that socialist feminism isn’t just about gender parity in top jobs — it’s about women taking control of their own lives.
    
    -   https://jacobinmag.com/2021/05/raqqa-women-blooming-in-the-desert-film/


## Resources

-   [Rojava: Trump Betrays the Kurds](https://novaramedia.com/2019/10/08/rojava-trump-betrays-the-kurds/)
-   [[The Philosophy of Murray Bookchin: An Interview with Debbie Bookchin]]

