# turning an old laptop keyboard into a portable usb keyboard

Looks a bit more difficult than [[converting an old laptop screen into a portable monitor]].


## Resources

-   [GRYNX » Converted laptop keyboard](http://www.grynx.com/projects/converted-laptop-keyboard/)
-   [How to Make a USB Laptop Keyboard Controller : 23 Steps - Instructables](https://www.instructables.com/How-to-Make-a-USB-Laptop-Keyboard-Controller/)

