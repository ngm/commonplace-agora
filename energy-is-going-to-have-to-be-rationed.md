# Energy is going to have to be rationed

> Most essential goods, from manufactured artefacts to foodstuffs, are energy-intensive. And energy is going to have to be rationed, either by price, queue, or economic plan
> 
> &#x2013; [[For a Red Zoopolis]]

[[2,000-Watt Society]].

