# Anti-trust

> Antitrust laws were created in the United States to promote competition and restrain the abusive practices of monopolies (then called “trusts”) in the late 19th century.
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

<!--quoteend-->

> Enter the trustbusters, led by Senator John Sherman, author of the 1890 Sherman Act, America’s first antitrust law. In arguing for his bill, Sherman said to the Senate: “If we will not endure a King as a political power we should not endure a King over the production, transportation, and sale of the necessaries of life. If we would not submit to an emperor we should not submit to an autocrat of trade with power to prevent competition and to fix the price of any commodity
> 
> &#x2013; [[The Internet Con]]

