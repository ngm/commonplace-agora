# Tools for thought: the first 300,000 years

URL
: https://subconscious.substack.com/p/tools-for-thought-the-first-300000

Author
: [[Gordon Brander]]

A nice timeline of [[tools for thought]] throughout history.

