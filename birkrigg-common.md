# Birkrigg Common

A lovely place.

Stunning views over [[Morecambe Bay]] and over to the [[Lake District]].


## Views


### Over Ulverston to the Lake District

![[photos/birkrigg-common-view-to-lake-district.jpg]]


### Across bay inlet

![[photos/birkrigg-common-view-over-bay-inlet.jpg]]


## Plants


### Gorse

I don't know which type of gorse though. August 2022.

![[photos/gorse-on-birkrigg.jpg]]


### Harebell

August 2022.

![[photos/harebell-on-birkrigg.jpg]]

