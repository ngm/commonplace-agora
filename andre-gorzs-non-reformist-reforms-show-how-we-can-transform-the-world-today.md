# André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today

URL
: https://jacobin.com/2021/07/andre-gorz-non-reformist-reforms-revolution-political-theory

Overview of [[André Gorz]]'s ideas on [[non-reformist reform]]s.

> In mapping a path forward for social movements, he disagreed with social democrats who believed that the harms of capitalism could be ameliorated simply through electoral politics and parliamentary dealmaking. Yet he also criticized radicals who perennially predicted a revolution that was nowhere on the horizon.

<!--quoteend-->

> “It is no longer enough to reason as if socialism were a self-evident necessity,” he argued. “This necessity will no longer be recognized unless the socialist movement specifies what socialism can bring, what problems it alone is capable of solving, and how. Now more than ever it is necessary to present not only an overall alternative but also those ‘intermediate objectives’ (mediations) which lead to it and foreshadow it in the present.”

<!--quoteend-->

> He recognized that putting together a near-term program could not simply be a matter of coming up with the most radical demands possible.

<!--quoteend-->

> The great strength of Gorz’s theory is not that it offers easy answers but that it provides a framework through which we can weigh the costs and benefits of pursuing any given demand or accepting any given compromise. It creates an orientation toward action that forces us to balance revolutionary vision with a hardheaded assessment of present

Seems very important to be honest. Green New Deal described as an example of this.

> For Gorz, non-reformist reforms seek to undermine the established order. “Structural reforms should not be conceived as measures granted by the bourgeois State at the end of a compromise negotiated with it, measures which leave its power intact. They should rather be considered as cracks created in the system by attacks on its weak points,” he writes.

