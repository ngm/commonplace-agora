# New EU Rules: Smartphones and Tablets will follow new ecodesign requirements by June 2025!

URL
: https://repair.eu/news/new-eu-rules-smartphones-and-tablets-will-follow-new-ecodesign-requirements-by-june-2025/

[[Ecodesign]] requirements for [[smartphone]]s and [[tablet]]s.

> On 31 August, the first EU Ecodesign and Energy Labelling rules on mobile phones and tablets were published in the Official Journal of the European Union.


## Good

> By June 2025, these products will have to be designed to be longer lasting and more repairable, marking a new era for the sustainability of electronic products.

-   ensuring access to spare parts ([[Spare parts for repair should be easily accessible]]) and access to repair information ([[The information necessary for repair should be easily accessible]]) for at least 7 years **after the end of the distribution** of a product in the market.
-   manufacturers will have to make compatible software updates available for at least 5 years.
-   smartphones will have to withstand at least 45 accidental drops without functional impairment
-   maintain at least 80% of their battery capacity after undergoing 800 charging cycles.
-   Tablets are to follow the same rules, but only for their battery capacity.
-   better access to information about the overall repairability of smartphones thanks to a repair index.


## Needs more work

> the affordability of repair is not tackled as the price of spare parts is neither limited nor considered in the calculation of the repair index.

<!--quoteend-->

> Given that the price of repair is one of the main factors that influences the choice of end-users to repair a product or not, it is regrettable that manufacturers will only have to indicate the pre-tax prices of the spare parts, prices to which manufacturers will not have to commit. 

<!--quoteend-->

> Furthermore, the text poorly tackles the matter of part-pairing, one of the main barriers to repairing products for end-users and independent repairers. Professional repairers will have access to information and tools to substitute and repair serialised parts, which is a significant win. However, an outright ban of part-pairing would have been a more fundamental step towards a universal right to repair, and we regret that the opportunity was not seized.

<!--quoteend-->

> Finally, we believe the legislation could have gone further in terms of facilitating self-repair. Manufacturers will still have the option not to provide spare batteries to end-users, under the condition that they respect certain longevity and waterproofness requirements. This creates a false dichotomy between repairability and durability, especially given that a number of waterproof electronic products with replaceable batteries (including smartphones) are already on the market. We also regret the significantly limited number of spare parts available to end-users, as compared to the list that concerns professional repairers. 

