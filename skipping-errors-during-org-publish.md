# Skipping errors during org-publish

When publishing my knowledge base, sometimes a particular file fails to publish.

e.g.

> Publishing file /mnt/HC<sub>Volume</sub><sub>21098789</sub>/commonplace-to-agora/commonplace/stock-and-flow-diagram.org using ‘org-html-publish-to-html’
> Unable to resolve link: "system dynamics"

'Unable to resolve link' seems the most common.  But sometimes other issues.

Trouble is, this stops the whole publish process.

I'd prefer to just log and continue.

Something like this: [org mode - Org-publish: Ignore errors when publishing and report them later](https://emacs.stackexchange.com/questions/63631/org-publish-ignore-errors-when-publishing-and-report-them-later)

For now, I'd see how far I get with `:with-broken-links`.  But I'd really like to be able to do the full ignore and log thing.

Worth noting that there seems to be a broken links 'mark' option: [org mode - How can I use the #+OPTIONS: broken-links: option to ignore broken&#x2026;](https://emacs.stackexchange.com/questions/48646/how-can-i-use-the-options-broken-links-option-to-ignore-broken-links-in-a-s) 

Other resources:

-   [org mode - can publish org ignore errors and push on - Emacs Stack Exchange](https://emacs.stackexchange.com/questions/26084/can-publish-org-ignore-errors-and-push-on)
-   [org mode - org-publish-project fails because of defective org-links - repair &#x2026;](https://emacs.stackexchange.com/questions/52232/org-publish-project-fails-because-of-defective-org-links-repair-links-automati)

