# Buurtzorg

> the Buurtzorg social care co-operative in the Netherlands, which works with the needs of the client, is rated extremely highly by users and employees, and moreover saves 40 per cent in costs to the national healthcare system by prioritising quality and need over profit
> 
> &#x2013; [[The Care Manifesto]]

