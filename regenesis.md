# Regenesis

A
: [[book]]

Subtitle
: "Feeding the World Without Devouring the Planet"

Author
: [[George Monbiot]]

About the [[global food system]], the need to reduce the amount of [[farming]] we do, and alternatives.

Listened to George on a few podcasts now.  This article gives a good overview of his arguments in this book I think: [Rod Oram: 'If everybody ate the average NZer’s diet we'd need another planet &#x2026;](https://www.newsroom.co.nz/rod-oram-if-everybody-ate-the-average-nzers-diet-wed-need-another-planet-to-sustain-us)


## Reviews

-   [Book review – Regenesis: Feeding the World Without Devouring the Planet | The&#x2026;](https://inquisitivebiologist.com/2022/06/01/book-review-regenesis-feeding-the-world-without-devouring-the-planet/)

