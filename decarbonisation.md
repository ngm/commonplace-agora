# Decarbonisation

> The process of reducing [[carbon dioxide emissions associated with energy and electricity use, industrial processes and transport]], with a view to their eventual elimination.
> 
> &#x2013; Down to Earth newsletter

<!--quoteend-->

> A fully decarbonised economy would require the use of 100% [[renewable energy]] and electricity, with correspondingly high degrees of [[energy efficiency]], [[energy storage]], a lower end use of energy, and a more expansive [[carbon sink]]s capable of rebalancing any residual greenhouse gas outputs from agriculture or other sources
> 
> &#x2013; Down to Earth newsletter

^ we should be reducing need for agriculture, also.  not just carbon sinking it.

> It is estimated that decarbonizing the US power grid alone will cost $4.5 trillion.
> 
> &#x2013; [[Mish-Mash Ecologism]]

