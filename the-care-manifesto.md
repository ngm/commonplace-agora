# The Care Manifesto

Saw it recommended by [[Adam Greenfield]] on Mastodon.

> The Care Manifesto offers a queer–feminist–[[anti-racist]]– [[eco-socialist]] political vision of ‘[[universal care]]’. Universal care means we are all jointly responsible for hands-on care work, as well as engaging with and caring about the flourishing of other people and the planet. It means reclaiming forms of genuinely collective and communal life, adopting alternatives to capitalist markets, and reversing the marketisation of care infrastructures. It also means restoring and radically deepening our welfare states, both centrally and locally. And, finally, it means creating [[Green New Deal]]s at the transnational level, caring international institutions and more porous borders, and cultivating everyday cosmopolitanism.

Enjoyed this a lot.  It looks at care at the level of kin, the community, the state,  economic systems, and the world as a whole.  Definitely recommend it.  It's a short and punchy read.  Lots of good stuff in the manifesto: [[Commons]], [[Ecosocialism]], [[participatory democracy]].

(Also really very interesting to learn about the history of [[Southbank Centre]] as a public space.)

At kin level, they have the idea of [[promiscuous care]] - something that spans  just traditional family ties.

At community level it's things like [[public spaces]], shared resources,  participatory democracy, [[cooperatives]], [[Libraries]], [[Platform coops]]. [[new municipalism]].

At state level it's about moving beyond [[welfare state]] to a caring state.
At economic level it is about [[Commons]], [[demarketisation]], [[Digital commons]].

At global level: interdepedendence, global alliances, porous borders. '[[Every billionaire is a policy failure]]'.

Essentially: we live in a world of carelessness.  This is as a result of neoliberal capitalism.  Recentring 'care' as a pillar around which lots of other nice ideas fit.


## Highlights

> Dependence on care has been pathologised, rather than recognised as part of our human condition

<!--quoteend-->

> this necessarily involves creating and defending the commons: collectively owned, socialised forms of provision, space and infrastructure.

<!--quoteend-->

> We argue that there are four core features to the creation of caring communities: mutual support, public space, shared resources and local democracy.

<!--quoteend-->

> The strength and historical popularity of the co-operative form is often underplayed, but it is a potent and crucial instance of mutual support in communities and, as we will see, of constructing caring economies.

<!--quoteend-->

> This means prioritising green spaces and public transport over cars and roads, and creating the resources to cultivate caring communities based on a notion of the commons: owning and sharing together.

<!--quoteend-->

> These should be maintained through digital infrastructures that we co-own: thus, instead of platform capitalism there would be platform co-operativism.

<!--quoteend-->

> Finally, wherever possible, markets should also be locally embedded.

<!--quoteend-->

> Likewise, ‘platform co-operativism’ – as a counterproposal to capitalist innovations such as Facebook, YouTube, Uber and Airbnb – is fundamental to creating a caring economy.

<!--quoteend-->

> As with offline commons, we need to insist that our online or digital commons are democratised, publicly and collectively owned and managed modes of production, involving peer-to-peer production (P2P).

<!--quoteend-->

> Constructing and nurturing the commons, and collectivising spheres of production and consumption, are key to creating an eco-socialist economy that is able to care.

