# Medium Anxiety

A
: [[podcast]]

Found at
: https://www.patreon.com/posts/premium-325-100272867

Part of
: [[This Machine Kills]]

Prevailing sense of [[anxiety]] in current generation. And a fight, flight, or freeze response to modern life. E.g. 30 year olds saying 'adulting is hard' (freeze).

