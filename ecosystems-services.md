# Ecosystems services

> response to the destruction of nature, there are increasing efforts to also consider nature or so-called “ecosystem services” as capital. This conceptual extension of a colonial relation to nature does not protect ecosystems, but rather creates a new asset class
> 
> &#x2013; [[Revolutionary Strategies on a Heated Earth]]

Bringing nature in to the market does not help nature.

