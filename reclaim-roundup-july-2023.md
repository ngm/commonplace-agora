# Reclaim Roundup: July 2023

Welcome!

This is the second in a series of regular monthly updates exploring the intersection of ecosocialism and ICT.   In it I look at the problems of [[digital capitalism]] and positive actions we can support now that embody [[digital ecosocialism]].  I also explore readings and thoughts on the theoretical side of things, particularly with an eye on transition: how we get from (digital) capitalism to (digital) ecosocialism.


## First, some theory

Here I report on some explorations around revolutionary theory, through a lens as to how it might relate to ICT.  How can we transition ICT from digital capitalism to digital ecosocialism?  And how can ICT play a part in a wholesale transition to an ecosocialist society?


### Systems theory and ICT

Continuing on with the theme of systems theory and how that might apply to ecosocialist ICT, I've been reading [[The Entropy of Capitalism]] by Robert Biel.  In it, Biel applies systems theory to capitalism in order to better understand it, and, ultimately, how to break from it.  It's full of nuggets of wisdom, and Biel is very ecosocialist in outlook.  (The book is pretty heavy going though, so a good place to start is Biel's much simpler series of [articles from The Ecologist](https://theecologist.org/profile/robert-biel).  Biel was also interviewed recently on Cosmopod: [[Entropy and the Capitalist System with Robert Biel]]).

As per the title, a key concept is [[entropy]].  Biel describes how capitalism tends towards disorder, but also how it staves it off through exploiting its environment.  Another core concept is that of human '[[capacity]]', its suppresion by capitalism and its potential unleashing, at great scale, by socialism. Biel sees self-organisation as part of the key to this great unfettering, and explores how self-organisation has been co-opted by neoliberalism and how it could be reappropriated for the left.  (But, like [[Rodrigo Nunes]], Biel clearly recognises the need for both horizontal and vertical political organisation: an ecology of both networked activism and the party form).

While it's quite heavy on diagnosis of the problem, and less (so far) on the solution, I'd like to think more about applying Biel's systems theory analysis to ICT.  How does it apply to digital capitalism and digital ecosocialism? In Biel's description capacity is very much knowledge-based - an almost free source of productivity - so could relate well to knowledge work (or, better, [[knowledge commoning]]).  Self-organisation in the tech sphere relates to questions of federation, decentralisation, peer-to-peer networks, etc.  And the spaces opened up by the entropy of digital capitalism can be seen as leverage points for building alternatives, as we see in the next article.


### Dialectics and alternative building

I've also been reading [[The technologies of all dead generations]], a long-read article from [[Ben Tarnoff]].  Tarnoff reflects on how to build alternative, liberatory digital technologies.  He calls for anyone building alternatives to maintain a healthy critical eye on what they're building, and to recognise that we are always building in a historical context.  For example: Mastodon may be a non-capitalist alternative to Twitter, but it is still connected to that history and tradition.  As such Tarnoff thinks that we can't simply build our way out of the problems, not without a strong dose of dialectics.

First Tarnoff describes two existing 'waves of algorithmic accountability', and proposes a third:

-   1st wave: harm reduction: trying to make existing systems less bad
-   2nd wave: abolition: trying to ban the systems
-   3rd wave: alternatives: trying to build alternative, better systems

I think these map fairly well to [[resist, regulate, recode]] from [[Platform Socialism]].

-   harm reduction = regulate
-   abolition = resist
-   alternatives = recode

Interestingly, Tarnoff also briefly mentions entropy, while referring to the drift away from the intended use of a technology.

> [Langdon] Winner calls it “drift”: the process whereby a technology imposes requirements of its own, pulling us ass-backwards along an unchosen course.

The spaces opened up by drift could be harnessed for our alternatives.  But Tarnoff emphasises the importance of dialectics (thesis, antithesis, synthesis), and wants us to be sure to always consider the antithesis of what we're building.

Coincidentally (or perhaps not, given the Marxist angle of both) Biel also makes much mention of dialectics.  Dialectics for Biel is a keystone of systems theory, representative of change, and something that helps us avoid a purely mechanistic worldview. So: a dialectical approach to ICT seems like a fruitful avenue to further explore.


## What's the problem?

Now, closer to the ground: some problems from digital capitalism recently in the news.  To help map them out, I'm tagging them with some of the criteria I defined in my OU research ([[Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]]).

-   [[Inside the AI Factory: the humans that make tech seem human]]. The exploitation of people to train AI to make other people lots of money.  (A problem of [[worker exploitation]], counter to [[social equity]], at the software layer of the stack).

-   [['We're mowed over': colossal data centers are taking over the US countryside]].  Lots of data centres being built in Virginia, US.  In the article, two opposing sides are pitched as those wanting to preserve the history of the area, and those who welcome the stimulus to the local economy.  No mention though of **why** so many new data centres are being built. (A problem of [[land use]] and [[growthism]], counter to [[planetary stability]], at the network layer of the stack).

-   [['I spot brand new TVs, here to be shredded': the truth about our electronic waste]].  Expensive, fully-working electronic and electrical items shredded, for no good reason.  (A problem of [[e-waste]] and [[consumption]], counter to [[planetary stability]], at the hardware layer of the stack).

-   [[Microsoft's dirty supply chain is holding back its climate ambitions]]. 96.71% of Microsoft's emissions are scope 3, i.e. lifecycle emissions from products. Emissions from many of their suppliers have in fact been going up.  Mostly, they seem to say, as a result of increase in demand. Seems like a reduction in demand (i.e. degrowth) would be a good way to go. (A problem of [[carbon emissions]], counter to [[planetary stability]], at all layers of the stack).

-   [[The World's Largest Technology Companies In 2023: A New Leader Emerges]].  Alphabet, Apple and Microsoft all in the top 10.  By market capitalization, Apple and Microsoft are the biggest companies in the world.  (A problem of [[domination]], counter to [[democracy]] and [[agency]], at all layers of the stack).

-   Vision Pro launched.  One thing I couldn't find anywhere in all the hoo-hah: what's the environmental impact of the Vision Pro?  It doesn't have a lifecycle assessment yet: https://www.apple.com/environment/.  For something that has zero genuine necessity for it to exist, it's a lot of kit.  At $3,500, you'd expect it last a long time and be easy to repair, but who knows with Apple.  (A problem of [[growthism]], counter to [[planetary stability]], at the hardware layer of the stack).


## And now, take action

And in response to the problems of digital capitalism, here's some latest news on concrete actions that are part of an ecosocialist ICT movement.

-   [[Amazon employees plan to walk off the job as tech worker tension rises]].  "In response to frustration over layoffs and the return-to-office mandate, as well as concerns about Amazon’s climate commitments". (A [[strike action]], supporting [[social equity]] and [[planetary stability]], to resist [[worker exploitation]] and [[carbon dioxide emissions]]).

-   [Why the Battle to Unionise Amazon Matters](https://tribunemag.co.uk/2023/06/why-the-battle-to-unionise-amazon-matters). (An action of [[unionsiation]], supporting [[social equity]] and [[agency]], to resist [[worker exploitation]]).


## Inputs

Finally, a few other things I've been reading, listening to, and watching in the last month or so that are adjacent to the topics of ecosocialism and ICT.


### Listening

-   [[Californian Dreams: Tech Utopia or Dystopia]]
    -   Richard Barbrook on the Californian Ideology, the prototypical link between tech and neoliberalism.
-   [[The Mute Compulsion of Capitalism]]
    -   Why do we seem so stuck with capitalism, when it is clearly failing us?
-   [[The death of the Unabomber: will his dangerous influence live on?]]
    -   Ted Kaczynski: the legacy of his anti-technology manifesto.
-   [[After the Robots: Aaron Benanav on Work, Automation and Utopia]]
    -   Regardless the impressiveness of AI and automation, it isn't doing anything useful for us economically.
-   [[Third Wave Internet with Ben Tarnoff]].  Ben Tarnoff on his ideas around the need for critical thought in third wave 'algorithmic accountability' (see above).


## Until next time

That's it!  See you in next newsletter. Until then, you can find latest streams of thoughts over at [my website](https://doubleloop.net).

Neil

