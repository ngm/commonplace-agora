# WordPress: Error connecting to database

Since moving my [[WordPress]] site to [[DigitalOcean]], I started receiving "Error connecting to database" pretty regularly.

It seems to be [[MySQL]] going down for some reason.  Restarting MySQL will sort it out.

```shell
service mysql restart
```

Never had it on my previous VPS.

Don't know why it would be Digital Ocean specific.

But anyway.  One common issue seems to be xmlrpc attacks.

https://www.digitalocean.com/community/tutorials/how-to-protect-wordpress-from-xml-rpc-attacks-on-ubuntu-14-04

You can check in your logs for it:

```shell
grep xmlrpc /var/log/apache2/access.log
```

I don't use Jetpack, so more than happy to disable xmlrpc.

```nil
<files xmlrpc.php>
  order allow,deny
  deny from all
</files>
```

Once this is set up, any access attempts you see in your logs should now be 403s.

The error still seems to happen though periodically.  It seems Apache makes MySQL fall over?

Quick and dirty cron job to restart MySQL regularly:

```nil
*/20 * * * * /usr/sbin/service mysql restart
```

A better one is a script to check if MySQL is down, and restart it if so.

There's also some tweaks you can make to Apache and MySQL conf.

