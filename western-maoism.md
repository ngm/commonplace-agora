# Western Maoism

[[Maoism]] in the West.

> Western Maoism was not just a movement of white students, as the stereotype holds, and the example of China as a beacon of ‘Third World’ socialism held appeal to many Black and Brown radicals in Britain. Mao’s China was a key reference point for [[Claudia Jones]] and the militant [[Caribbean Workers' Movement]], several British Black Power organisations, and the great [[Indian Workers Association]].
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

