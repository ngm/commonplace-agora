# Authority in networks

> When we examine a large-scale social computing network, we need to discover how power is assigned within the network. Who is given authority to do what, and under which circumstances? This kind of information will help us understand the dynamics of the network. As authority may be used for personal, political, and commercial advantage, we need to think carefully about its assignment.
> 
> &#x2013; [[Information Civics]] 

<!--quoteend-->

> Admins punish other users for transgressions with the tactics of dictatorships, like censorship and exile. Ordinary users have no means of holding admins themselves accountable, short of asking for interventions from whatever corporation owns the platform. Yes, users can leave and choose another group, but often that is not as easy as it sounds, if the people they need to interact with are all there. Admins are free to make decisions capriciously and with no consultation. We treat this as acceptable.
> 
> &#x2013; [[Online Communities Are Still Catching Up to My Mother's Garden Club]]

