# Sovereignty

> Mary shook her head. Even if market and state were two parts of a single system, that single system was ruled by law; and the laws were made by the nation-states; they could therefore change the laws, that was sovereignty, that was where seigniorage and legitimacy and ultimately social trust and value resided. The market was constructed by, and parasitic on, that structure of laws.
> 
> &#x2013; [[The Ministry for the Future]]

