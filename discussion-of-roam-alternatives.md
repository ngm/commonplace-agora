# Discussion of Roam alternatives

There's a good discussion on Roam alternatives at Darius' toot here: https://friend.camp/@darius/104898659326177512

Touches on some of the nice features of [[Roam]] too - such as [[fully-transcluded backlinks]] (plus filtering) and [[block-level transclusion]].  It's good as it's making me wonder if I can do them and would find them useful in org-roam too.

