# Gross National Happiness

> Bhutan’s famous Gross National Happiness, which uses thirty-three metrics to measure the titular quality in quantitative terms
> 
> &#x2013; [[The Ministry for the Future]]

