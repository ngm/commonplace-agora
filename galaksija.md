# Galaksija

> The Galaksija computer was a craze in 1980s Yugoslavia, inspiring thousands of people to build versions in their own homes. The idea behind them was simple – to make technology available to everyone.
> 
> &#x2013; [[Socialism's DIY Computer]]

<!--quoteend-->

> In 1948, Yugoslavia was expelled from Cominform, the Soviet information agency, in retaliation for its "non-aligned" status; deprived of information-processing capacity, the country created its own IT industry from scratch.
> 
> 1/
> 
> https://twitter.com/doctorow/status/1289932294023610370

