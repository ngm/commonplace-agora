# I dislike big tech



## Because

-   [[Big tech is complicit in climate breakdown]]
-   [[Big tech is a huge part of capitalism]] and [[I dislike capitalism]]
-   [[Big tech exploits the victims of economic collapse]]
-   [[For big tech, user benefits are secondary to business model]]


## But

-   This does not mean I dislike people who use the big tech platforms.
-   Nor the people who work there.
-   Big tech is a systemic problem, not just a matter of individual choices.


## Epistemic status

Type
: [[feeling]]

Strength
: 7

7 because yeah for sure I recognise the problem.  But not a 10 because I don't feel physically angry about it (yet).  It's not emotive in that way.

