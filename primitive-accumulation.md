# Primitive accumulation

Comes as this point of transition to [[capitalism]] from late [[feudalism]], when land is enclosed etc. Land property wealth into hands of capitalist class, that's what made them capitalists.  Violence.

See [[enclosure of the commons]].  An artificial construction of an urban workforce to support capitalism.

See [[Caliban and the witch]].


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, &amp; Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)

