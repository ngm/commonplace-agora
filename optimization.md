# Optimization

> Optimization precludes and narrows the capacity for citizenship that uses technology to bring forward diverse knowledges in diverse ways and submits civic decision-making to narrow corporate interest
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Viewed alongside the longer history of co-optation of changes in the communication environment by powerful commercial actors, the drive toward optimization of service delivery through data analytics is part of a trajectory toward private interests in the public sphere
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

