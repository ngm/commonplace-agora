# 2020-48



## Notes

-   **Coding**.  Looking at the notes from the week, it seems like hobby-time has mostly been related to working on my PKM / digital garden system ([[Flock]]) and tinkering around with [[Emacs]].  Coding and debugging in [[elisp]], and tweaking a bit of the look of the site.
    -   [[Problem with org-roam's file-level graphing]].
    -   [[Fixing PlantUML in org-babel]].
    -   [[Setting up a spacemacs literate config file]].
    -   [[Adding timestamps to org-roam notes]].
    -   [[Made myself a logo with PlantUML]].

-   **Writing**.  Which is all totally fine - I enjoy it! And learn plenty in the process.  But just a point to note that left unattended, my attention tends to go towards tweaking and coding, less towards writing.  Although writing about what I've tweaked and coded still counts, right?

-   **Reading**. I read [[New UK tech regulator to limit power of Google and Facebook]], which made me think a bit about some [[principles for ethical technology]] and related [[praxis for the hacker class]].
    -   Planning to re-read [[Hail the maintainers]] too.

-   Only managed to catch a couple of talks from [[EmacsConf 2020]], but nice to see such a good community around [[Emacs]].  For such a long-lasting program it seems very much alive and well.

-   Enjoyed muchly [[The Central Memory]] - AI-assisted cyborb pop.

-   I had a bit more of a play at [[getting knowledge maps in to my weeknotes]], this time with an org-roam generated graph.  It works as a nice easy-to-generate visual aide memoire for me, but sadly not really visually compelling for anyone else.


## Graph

![[weeknotes-2020-48.svg]]


## Journals

-   [[2020-11-29]]
-   [[2020-11-28]]
-   [[2020-11-27]]
-   [[2020-11-26]]

