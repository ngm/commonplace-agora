# Some small experiments in 'microblurting'



## Context

-   [[blurting]]
-   [[microblurting]]
-   [[my blurts]]


## Thoughts

Microblurting feels similar to using flashcards. Which makes sense, as they're both forms of [[active recall]].

But the micro aspect of it makes it closer to flashcards than doing a bigger blurt.

I've found it beneficial so far. Helping to refine and remember my definitions of some concepts that I'm interested in.

It also feels somewhat similar to doing [[code kata]], in that I'm refining my writing tools and my use of them while repeating and rewriting simple definitions. e.g.[[making it easier to open the session drawer in Termux in right-handed one-handed mode]], [[issue with evil-escape in Doom on Termux]] both arose from microblurting on my phone.

I find the words blurt and microblurt pleasingly comical, but I imagine there's a more sensible term for the practice in learning science.

I'm not certain in the utility yet of publishing your microblurts, but I find it somehow motivational, and as long as you're not overloading a public stream that people are expecting to be something else, I see no harm in it.

Should it stand the test of time, I could imagine a microblurt having its own microformat.

