# Shock Doctrine as a Service

> The design of the Apple Watch, Safety Check and particularly COVID–19 Contact Tracing APIs must be understood as whitelabelled crisis response – Shock Doctrine as a Service – employing dominant, market-driven design methodologies to drive mass adoption of products and services that are then easily reconfigured during moments of disaster.
> 
> &#x2013; [[This is Fine: Optimism and Emergency in the Decentralised Network]] 

[[Shock Doctrine]].

