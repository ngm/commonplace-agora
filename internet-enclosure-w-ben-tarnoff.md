# Internet Enclosure w/ Ben Tarnoff

A
: [[podcast]]

URL
: https://tribunemag.co.uk/2022/09/95-internet-enclosure-w-ben-tarnoff

> On this week's podcast, Grace speaks to [[Ben Tarnoff]], author of [[Internet for the People]]. They discuss the history of the web's enclosure and privatisation – and how we could build a different model for the future.

Nice discussion around Ben's outline of a program for [[deprivatisation of the internet]].

Ben talks a bit about avoiding separating [[forces of production]] and [[relations of production]].  Says they are entwined.  So simply nationalising Facebook, for example, wouldn't necessarily make it any better.

