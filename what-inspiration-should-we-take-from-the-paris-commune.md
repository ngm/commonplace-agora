# What inspiration should we take from the Paris Commune?

The [[Paris Commune]] crops up frequently in things I'm reading.

As an actually existing society organised along very different lines from our current one.

I don't know much about it and haven't really unpicked what it is about it that one might want to recreate though.

Possible avenues:

-   decentralisation and participation ([[The Radical Imagination of the Paris Commune]])

