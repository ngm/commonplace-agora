# planned obsolescence

> Large swathes of manufacturing become rationalized when ‘planned obsolescence’ itself is made obsolete
> 
> [[Half-Earth Socialism]]

