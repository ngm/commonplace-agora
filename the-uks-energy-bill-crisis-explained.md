# The UK's energy bill crisis explained

A
: [[podcast]]

URL
: https://www.theguardian.com/news/audio/2022/aug/10/the-uks-energy-bill-crisis-explained-podcast

[[UK energy bill crisis]].

Citizens advice bureau in Brighton. At capacity, but calm before the storm.

~00:07:02  Price cap. Introduced during May government. Wholesale prices have gone up and cap then removed.

Citizen's advice seeing more calls from those struggling with energy in June/July than usually see in winter. Disproporrianetely affecting low income and disabled people.

Citizens advice practically advising to not pay. People running out of options.

Liz truss suggesting cutting green levies&#x2026;

Most countries are feeling price shocks. Over governments are helping to absorb them.  UK is where it affects those on lowest income the most.

Renewable energy would have vastly increased our resilience to these shocks.

