# Repairing a Canon Pixma G3510

Reported problem: doesn't print anymore.

-   Black ink light flashing - manual doesn't specify what this means, but will assume out of ink.
-   Colour ink light solid - suggests low ink.
-   Detects paper and paper size fine.
-   Try printing a test page.
-   Paper pickup doesn't seem to be working - starts pulling it in, but then stops, and gives error about no paper.
-   Paper pickup cleaning maintenance mode doesn't work.

