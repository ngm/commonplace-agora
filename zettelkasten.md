# zettelkasten

-   tags: [[note taking]]

"box of notes"

> He wrote only on one side of each card to eliminate the need to flip them over, and he limited himself to one idea per card so they could be referenced individually.
> 
> &#x2013; [How To Take Smart Notes: 10 Principles to Revolutionize Your Note-Taking and &#x2026;](https://fortelabs.co/blog/how-to-take-smart-notes/)

-[Zettelkasten — How One German Scholar Was So Freakishly Productive](https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125) 

-   [Zettelkasten: It’s Like GTD for Writing and Here’s Why You Should Consider It](https://writingcooperative.com/zettelkasten-its-like-gtd-for-writing-and-here-s-why-you-should-consider-it-7dddf02be394)

It feels a bit like the philosophy of [[tiddlers]], to me.

> The purpose of recording and organising information is so that it can be used again. The value of recorded information is directly proportional to the ease with which it can be re-used.
> 
> &#x2013; [Philosophy of tiddlers - TiddlyWiki](https://tiddlywiki.com/#Philosophy%20of%20Tiddlers) 


## Notes / steps

1.  Literature notes
2.  Reference notes
3.  Permanent notes
4.  Review and repeat


## Digital Zettelkasten

I use [[org-roam]] for this.

Does it make sense to have a timestamp in the filename?

-   [How important is to have datetime info in the filenames? - Random - Org-roam](https://org-roam.discourse.group/t/how-important-is-to-have-datetime-info-in-the-filenames/1149)
-   [A digital approach to Luhmann’s Zettelkasten (Slip Box) | Tevin Zhang](https://tevinzhang.com/digital-zettelkasten/)

