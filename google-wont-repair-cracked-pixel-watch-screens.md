# Google won't repair cracked Pixel Watch screens

URL
: https://www.theverge.com/23874281/google-pixel-watch-cracks-no-repairs-warranty
    
    > Just as California passes a new right-to-repair act, Google has confirmed it currently offers no repair options if your Pixel Watch screen cracks.

[[Google]] offers no [[repair]] options for cracked Pixel Watch screens.

> “At this moment, we don’t have any repair option for the Google Pixel Watch. If your watch is damaged, you can contact the Google Pixel Watch Customer Support Team to check your replacement options,” Google spokesperson Bridget Starkey confirmed to The Verge. 

So just replace it, they're basically saying.

