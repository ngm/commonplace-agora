# lithium

> Lithium is produced by drilling holes into salt flats — usually found in massive crater-like lakes — and pumping brine to the surface. The important bit, lithium carbonate, is subsequently extracted through a chemical process. In recent years, pollution from the extraction process has led to the death of animals and crops, severely impacting local communities in countries like Argentina and China. The lithium that makes our controllers wireless is just another material that scars not only the landscape but the lives of those who call it home. 
> 
> &#x2013; [[The environmental impact of a PlayStation 4]]

