# Appropriate Measures

URL
: https://reallifemag.com/appropriate-measures/

Summary
: "Changing the tech we use is not enough to mitigate the environmental and social harm of mass technology."

Critique of [[appropriate technology]].

