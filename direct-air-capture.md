# Direct air capture

[[Carbon capture]].

> DAC proposes to slow [[climate change]] by sucking greenhouse gasses like CO2 directly from the atmosphere, DAC has splintered environmentalists, some of whom laud it as a potential savior, while others call it as a costly, risky distraction from meaningful emissions distractions.
> 
> &#x2013;  [[World's Largest Direct Air Carbon Capture System Goes Online]]

