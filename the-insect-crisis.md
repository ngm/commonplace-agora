# The Insect Crisis

> In more disturbing passages Milman conjures a silent world without insects, where faeces and corpses dot the landscape and humans survive on a bland diet of staples, such as rice, that can be pollinated by the wind. Humans, or, rather, rich humans, muddle through, but it is an existence that is bland, colourless, and miserable.
> 
> [The Insect Crisis by Oliver Milman review – strange and fragile beauty | Scie&#x2026;](https://www.theguardian.com/books/2022/jan/14/the-insect-crisis-by-oliver-milman-review-strange-and-fragile-beauty)

