# Percy Shelley

> Shelley – a poet whom Marx considered ‘one of the advanced guard of socialism’ – not only admired Oswald’s pamphlet but authored the vegetarian essay A Vindication of Natural Diet. There, he made the Platonic argument that ‘devouring an acre at a meal’ led to war, and included the Jennerite insight that disease itself was historical and ‘flowed from unnatural diet’, which included meat
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> He was married to [[Mary Shelley]], the daughter of [[Godwin]] and [[Mary Wollstonecraft]]

