# nowtions

Things I'm currently thinking about.  (like a /now page, but for [[notion]]s&#x2026; sorry).  Similar idea to e.g. [[Andy Matuschak]]'s  [§What’s top of mind](https://notes.andymatuschak.org/%C2%A7What%E2%80%99s_top_of_mind).

See also [[projects and activities]].

-   [[Ecosocialism]] and [[digital ecosocialism]]
-   [[Reclaim the stacks]]
-   [[synthesis of horizontalism and verticalism]]
    -   [[Neither Vertical Nor Horizontal]]
    -   [[Anarchist Cybernetics]]

