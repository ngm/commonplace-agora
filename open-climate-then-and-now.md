# Open Climate Then and Now

URL
: 

https://branch.climateaction.tech/issues/issue-4/open-climate-then-and-now/

[[Open Climate]].

Outlines 5 areas in which open practices (e.g. open: science, hardware, knowledge, infrastructure) can strengthen climate action.


## 1. Reclaim a robust knowledge commons

> The climate crisis is also an information crisis. The knowledge commons can be an important tool to fight back.

<!--quoteend-->

> The open movement can help clean up the information environment by:
> 
> -   Foregrounding the viewpoints of frontline communities. [&#x2026;]
> -   Distributing information in formats that communities need and understand. [&#x2026;]
> -   Improving the quality of the information, not just the quantity. [&#x2026;]

Chimes with [[political education]].  [[Knowledge commoning]].


## 2. Address the environmental impact of digital infrastructures

> The open movement can reduce the environmental impact of technology by:
> 
> -   Leveraging openness as a tool for replicability, transparency, and impact. [&#x2026;]
> -   Aiming open source at the target of achieving a fossil-fuel-free Internet by 2030. [&#x2026;]
> -   Foreground the perspective of indigenous communities because sustainability depends on social arrangements. [&#x2026;]

For sure, reducing the impact of technology itself is import.  I would place things such as [[right to repair]] in this bracket also.  The openness of repair information.


## 3. Downscale climate science to local levels

> Going from highly centralized science or policymaking to communities that can act requires attention to the needs at the most localized possible level, whether by using local languages or training local communities.

Sounds like [[subsidiarity]], I am strongly onboard with this.

> The open movement can empower the smallest possible policy-maker by:
> 
> -   Integrating local information and knowledge with climate risk and forecasting. [&#x2026;]
> -   Supporting shifts in individual choices and personal knowledge. [&#x2026;]
> -   Recognizing the importance of “openness” beyond licensing and open technology. [&#x2026;]


## 4. Faciliate free access to climate information


## 5. Offer value to other movements by operating intersectionally

```plantuml
@startmindmap
+ open climate
++ knowledge commoning
++ impact reduction
++ subsidiarity
-- open access
-- intersectionality
@endmindmap
```

