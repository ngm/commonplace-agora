# Popular assembly

> A popular assembly (or people's assembly) is a gathering called to address issues of importance to participants. Assemblies tend to be freely open to participation and operate by [[direct democracy]]. Some assemblies are of people from a location, some from a given workplace, industry or educational establishment others are called to address a specific issue. 
> 
> &#x2013; [Popular assembly - Wikipedia](https://en.wikipedia.org/wiki/Popular_assembly) 

<!--quoteend-->

> From the quartiers of the [[Paris Commune]] to the general assemblies of [[Occupy Wall Street]] and elsewhere, these self-organized democratic councils run like a red thread through history up to the present. 
> 
> &#x2013; [[The Next Revolution]] 

<!--quoteend-->

> which [[Hannah Arendt]] called the “lost treasure” of the revolutionary tradition
> 
> &#x2013; [[The Next Revolution]] 

