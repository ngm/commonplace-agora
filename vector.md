# vector

> The vectoralist class owns and controls the vector, a concept I use to describe in the abstract the infrastructure on which information is routed, whether through time or space.
> 
> &#x2013; [[Capital is Dead]]

