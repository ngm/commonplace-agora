# Markets in the Next System

URL
: https://thenextsystem.org/markets-in-the-next-system

Great article. I don't have a ton of knowledge about [[markets]] but this seems to give a good overview.  Though it seems to miss out the [[socialist calculation debate]] in its history.

The outline for markets after capitalism is good.

-   [[Markets before capitalism]].
-   [[Capitalist markets]].
-   [[Markets after capitalism]].

