# What's new in Emacs 28?

I've just updated to Emacs 28.  ([[Updating to Emacs 28 on Linux Mint]]).

What's new and of interest to me?

A couple of references:

-   from the manual: [New in Emacs 28 (GNU Emacs FAQ)](https://www.gnu.org/software/emacs/manual/html_node/efaq/New-in-Emacs-28.html)
-   the mother of all annotations of the release info: [What's New in Emacs 28.1? - Mastering Emacs](https://www.masteringemacs.org/article/whats-new-in-emacs-28-1)

The main thing seems to be the addition of native compilation, which various sources state will give you a big speed boost.  That'd be nice, although I've never really had a problem with speed.

LATER&#x2026;: I'll be honest, it doesn't feel any faster, and possibly slower?  Maybe it's because I'd compiled it from source previously and that was faster?  

Oh: inspecting `system-configuration-options` in Emacs shows that it's not compiled with native compilation available.  And then, looking at https://launchpad.net/~kelleyk/+archive/ubuntu/emacs, I see there's packages specifically for the nativecomp version.  I wonder why it isn't the default.  I'll try it.

OK, so the nativecomp packages seems way **slower** than the standard package.  Not sure why, not got time to look right now.  Back to the standard package then!

