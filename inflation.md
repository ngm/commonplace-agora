# inflation

The general increase of the prices of goods and services.

Stuff costs more; your money doesn't go as far as it did. The cost of living goes up. 

> Attitudes towards inflation have long been seen as a dividing line between left and right. Most Keynesians saw rising prices as a small price to pay for an otherwise healthy economy, given that tackling inflation tended to require higher interest rates, which curtail investment, output, and employment over the short term
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> [[Neoliberals]], on the other hand, saw inflation as public enemy number one. First, rising prices eroded the real value of assets — if consumer prices are rising while house prices are stagnant, your house is worth less in real terms. Second, keeping interest rates low reduces returns from lending — important for profits in the finance sector — and low unemployment made it harder to discipline labour
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> The question we should therefore be asking is not ‘is inflation bad?’ The questions we should be asking are ‘which prices are rising, why are they rising, and who is bearing most of the impact
> 
> &#x2013; [[Tribune Winter 2022]]

<!--quoteend-->

> This can happen for a number of reasons: a run on a country’s currency that increases prices of imported goods; a large stimulus to the economy when the system is already at or near full capacity; a strongly unionised labour force exercising its bargaining power; a sudden shortage of labour (following Brexit, for example) or international cost-push factors outside an individual country’s direct control.
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)

<!--quoteend-->

> The French, for example, have forced their energy companies to keep electricity price increases to households to no more than 4% this year, which has kept inflation lower in France than in other European countries.
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)

<!--quoteend-->

> The problem now is that demand-pull inflation, which we experienced when Covid restrictions were lifted and the economy surged, has turned into cost-push inflation, determined not by wage growth but by the war in Ukraine, which has sent energy and food prices soaring.
> 
> &#x2013; [Economics made simple: 10 experts on where the cost of living crisis came fro&#x2026;](https://www.theguardian.com/politics/2022/jul/16/economics-made-simple-experts-cost-of-living-crisis-causes)

