# Silke Helfrich

Researcher and activist of the [[commons]] and [[commoning]].  Sadly passed away in 2021.

> She loved thinking of [[commoners]] as the [[mycelium]] out of which like [[mushrooms]] the [[commons]] grow.
> 
> &#x2013; http://www.bollier.org/comment/2150#comment-2150

