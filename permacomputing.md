# Permacomputing

> how to give computers a meaningful and sustainable place in a human civilization that has a meaningful and sustainable place in the planetary biosphere.
> 
> -   [Permacomputing | viznut](http://viznut.fi/texts-en/permacomputing.html)

