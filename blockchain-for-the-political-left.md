# Blockchain for the political left

Could blockchain be a useful tool for those on the anti-capitalist political left?

[[The Blockchain Socialist]] would say so.

Adam Greenfield is very skeptical:

> Given their success in such a wide variety of contexts, it makes sense to ask how easily these principles, and the other structures and practices fundamental to Ostrom’s understanding of commoning, might be encoded in a DAO. And equally, given the immediate applicability to the many local struggles for self-definition now being waged around the planet, it makes sense to ask whether the understanding of the commons reflected in contemporary participatory-democratic movements might be similarly inscribed. And what we learn when we undertake this challenge, even as a thought experiment, is that the DAO—at least as Buterin and his colleagues now describe—it is poorly suited to either context.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> It’s unclear how a DAO entrusted with the management of a common-pool resource would handle the entire range of easily foreseeable situations such a community might face—a nonmember allowed temporary access to a common good, but only under supervision, or a short-term and informal exchange of prerogatives among members of two adjacent collectives.
> 
> &#x2013; [[Radical Technologies]]

<!--quoteend-->

> So until some propensity to support collectives, cooperatives and communes is demonstrated, I think those of us who share this set of values are best advised to sit in their skepticism.
> 
> [[Radical Technologies]]

[[DisCO]]s have some small linkages to blockchain. In [[Free, Fair and Alive]] they discuss [[Holochain and commoning]].

[Commoning with Blockchain. Free, Fair and Alive | by Will Szal | Regen Networ&#x2026;](https://medium.com/regen-network/commons-discos-3fb4c707f68b)

