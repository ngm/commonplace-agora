# Extreme heat prompts first-ever Amazon delivery driver strike

URL
: https://grist.org/labor/extreme-heat-california-amazon-delivery-driver-strike/

[[Amazon]].

> Last August, after the drivers prepared a list of demands around pay, safety, and extreme temperatures, Amazon responded by offering workers two 16-ounce bottles of water a day. 

