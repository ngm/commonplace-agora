# strike action

Work stoppage caused by the mass refusal of employees to work.

> Total and indefinite stoppages were commonplace in the 1970s. Contemporary unions prosecute their case by taking targeted and intermittent action, as rail workers have been doing for many months.
> 
> &#x2013; [This winter of discontent will harden the feeling that the Tories have broken&#x2026;](https://www.theguardian.com/commentisfree/2022/dec/04/winter-of-discontent-harden-feeling-tories-have-broken-britain) 

