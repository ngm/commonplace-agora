# class composition

> Class composition, then, is a rebuke to the notion of class as a preexisting empirical category—an idea you might encounter in a basic sociology textbook, where you simply look at someone’s job or income and determine their class. Rather, class in the Marxist sense is forged through struggle itself. As the writers of 1970s journal Zerowork put it, “For us, as Marx long ago, the working class is defined by its struggle against capital and not [merely] by its productive function
> 
> &#x2013; [[Breaking Things at Work]]

[[Solidarity]]

