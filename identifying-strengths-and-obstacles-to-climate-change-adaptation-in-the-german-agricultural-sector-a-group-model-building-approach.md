# Identifying Strengths and Obstacles to Climate Change Adaptation in the German Agricultural Sector: A Group Model Building Approach

[[Participatory modelling]].

> Through participatory modeling, we mined the mental database of stakeholders who hold institutional memory regarding system structures, policies, and decision-making.

<!--quoteend-->

> One way of implementing participatory modeling is through GMB

[[Group Model Building]].

> We have implemented changes to the original method described in Vennix, (1996) [23] and developed a novel approach to GMB (Figure 4) by adding a preliminary model based on individual models and a triangulation process after the group workshop.

Denzin's [[triangulation in qualitative research]].

