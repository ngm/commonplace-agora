# The commons enables people to enact fairness without bureaucratic control

A claim from [[Free, Fair and Alive]].


## Epistemic status

Type
: [[claim]]

Gut feeling
: 6

Confidence
: 4

