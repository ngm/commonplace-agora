# Professional-managerial class

> The term professional–managerial class (PMC) refers to a social class within capitalism that, by controlling production processes through superior management skills, is neither proletarian nor bourgeois. 
> 
> &#x2013; [Professional–managerial class - Wikipedia](https://en.wikipedia.org/wiki/Professional%E2%80%93managerial_class) 

<!--quoteend-->

> From what I've seen from this presenter previously, I sense there's a professional appropriation of the commons ethos going on. Better architecture, more equity. Beware the professional-managerial class! It's good though, to see commoning being situated in a context of Black equity and property relations. Of course it's relevant :) Power to the commons
> Just - never entirely trust a professional. **Yours, in and against the professional-managerial class** 🙄  Vernaculars of the world, unite!
> 
> -   https://social.coop/@mike_hales/107013946335060605

I think I am also in and against the professional-managerial class.

