# Post-Growth Living

A
: [[book]]

URL
: https://www.versobooks.com/en-gb/products/929-post-growth-living

Author
: Kate Soper

Subtitle: For an Alternative Hedonism.

Starts off critiquing [[capitalism]].

As well as the notion of the [[Anthropocene]], because it places the locus of blame in the wrong place.

