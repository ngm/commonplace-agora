# world-systems theory

[[Immanuel Wallerstein]].  He actually called it world-systems analysis.

> World-systems analysis crystallized in the aftermath of [[1968]]. People across the globe took to the streets, fighting against US military aggression and for a renewal and reconfiguration of the founding principles of the [[Russian Revolution]]. The worldwide upsurge was short-lived, but its legacy was unexpectedly powerful, particularly in the academy
> 
> &#x2013; [[The Brilliant Immanuel Wallerstein Was an Anticapitalist Until the End]]

<!--quoteend-->

> By the end of the sixties, widespread dissatisfaction with dominant modes of thinking had spread among radical scholars. Indeed, world-systems analysis was just one of a number of dissident frameworks (dependency theory, international political economy, historical sociology) that emerged around that time
> 
> &#x2013; [[The Brilliant Immanuel Wallerstein Was an Anticapitalist Until the End]]

