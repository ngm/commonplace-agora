# Anchor institutions

> an anchor institution is an organisation which has a key stake in a place. It will have significant levels of spend and numbers of jobs, and is extremely unlikely to leave due to market forces. Anchor institutions typically include: local authorities, universities, further education colleges, hospital trusts, and housing organisations.
> 
> &#x2013; [Creating a good local economy: the role of anchor institutions](https://www.preston.gov.uk/media/819/The-role-of-Anchor-Institutions/pdf/Anchor-institutions.pdf?m=636934398910770000) 

<!--quoteend-->

> The key to a good local economy is ensuring that the capital and general activity associated with the day to day operation of the anchor institutions is retained within the local economy, as much as possible.  
> 
> &#x2013; [Creating a good local economy: the role of anchor institutions](https://www.preston.gov.uk/media/819/The-role-of-Anchor-Institutions/pdf/Anchor-institutions.pdf?m=636934398910770000) 

<!--quoteend-->

> Indeed, anchor institutions should have a keen interest in their local economies: they recruit from and serve local communities; they have a profile in that local economy; and they can contribute to wider outcomes including better health and crime reduction.
> 
> &#x2013; [Creating a good local economy: the role of anchor institutions](https://www.preston.gov.uk/media/819/The-role-of-Anchor-Institutions/pdf/Anchor-institutions.pdf?m=636934398910770000) 

-   https://www.preston.gov.uk/media/818/Community-Wealth-Building-through-Anchor-Institutions/pdf/Community-Wealth-Building-through-Anchor-Institutions-01-02-17.pdf?m=636934398536430000
-   

> The model works by identifying ‘anchor institutions’ – such as schools and hospitals which lots of money flow through – and getting these institutions to change their suppliers to local co-operatives and small businesses.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]

