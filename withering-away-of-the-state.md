# Withering away of the state

> with realization of the ideals of socialism, the social institution of a state will eventually become obsolete and disappear as the society will be able to govern itself without the state and its coercive enforcement of the law.

