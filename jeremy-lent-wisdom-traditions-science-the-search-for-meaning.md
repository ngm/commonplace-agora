# Jeremy Lent: Wisdom Traditions, Science & the Search for Meaning

A
: [[podcast]]

URL
: https://david-bollier.simplecast.com/episodes/jeremy-lent-wisdom-traditions-science-the-search-for-meaning

Series
: [[Frontiers of Commoning]]

Featuring
: [[Jeremy Lent]] / [[David Bollier]]

I like the sound of [[Jeremy Lent]]'s work.
Discusses [[The Patterning Instinct]] and [[The Web of Meaning]].

