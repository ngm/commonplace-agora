# Hypertext

> It's a concept that was developed by [[Ted Nelson]] and [[Doug Engelbart]] in the sixties. The basic idea is this: if we're trying to do research on any kind of a subject, the subject matter exists in all kinds of different places. It can be books, magazines, tape recordings, Compuserve, and if we can somehow link all this stuff electronically, so that if we click on Beethoven, we can all of a sudden jump from one to the next&#x2026; that's what hypertext is all about.
> 
> &#x2013; [30-plus years of HyperCard, the missing link to the Web | Ars Technica](https://arstechnica.com/gadgets/2019/05/25-years-of-hypercard-the-missing-link-to-the-web/) 

-   [[Memex]]
-   [[HyperCard]]
    
    > I like books because they don’t have hyperlinks, I like the web because it does.
    > 
    > https://prtksxna.com/2020/04/13/books-hyperlinks/


## Hypertexts

I guess there's a distinction between hypertext as the mechanism for linking things together, and a hypertext as in a particular piece of work that is composed of lots of hypertextual ways of navigating through it.  Like [hypertext fiction](https://en.wikipedia.org/wiki/Hypertext_fiction) but non-fiction.

