# Social media is important because everything is social

> Well, our reasoning is that everything is social. Political activities are social. Economic activities are social. Artistic activities are social. The way we work and coordinate activities&#x2026; is social. 
> 
> &#x2013; [Not Just 'Yet Another Microblog'](http://bonfirenetworks.org/posts/not_just_yet_another_microblog/) 

