# Smalltalk and commonplace books

> I'm starting to understand the attraction of a VM image a la [[Smalltalk]] as the platform for a [[commonplace book]], a digital workbench, a prototype fit to the person who lived it into its current state.  A Time Lord as _your_ companion, since you can call on its past generations.
> 
> &#x2013; Jeff Miller, https://matrix.to/#/!ORfrUEFeWFcHAMLFLr:matrix.org/$1645305348236000pXAwI:matrix.org

