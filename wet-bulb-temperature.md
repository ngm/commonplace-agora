# Wet-bulb temperature

Taking humidity in to account when talking about temperature.

> But the headline figures do not give the whole story when it comes to the impact of high temperatures on humans, because humidity, which is not factored into these figures, plays a huge role in how we actually experience heat
> 
> &#x2013; [Why you need to worry about the ‘wet-bulb temperature’ | Climate science | Th&#x2026;](https://www.theguardian.com/science/2022/jul/31/why-you-need-to-worry-about-the-wet-bulb-temperature) 

<!--quoteend-->

> Recent research has found that we may actually already be nearing the threshold values for human survivability of temperature and humidity for short periods in some places of the world – a measure known as the “wet-bulb” temperature – and that this threshold may actually be far lower than previously thought.
> 
> &#x2013; [Why you need to worry about the ‘wet-bulb temperature’ | Climate science | Th&#x2026;](https://www.theguardian.com/science/2022/jul/31/why-you-need-to-worry-about-the-wet-bulb-temperature) 

This is exactly the kind of event that happens at the beginning of [[The Ministry for the Future]].

> Wet-bulb temperature (WBT) combines dry air temperature (as you’d see on a thermometer) with humidity – in essence, it is a measure of heat-stress conditions on humans.
> 
> &#x2013; [Why you need to worry about the ‘wet-bulb temperature’ | Climate science | Th&#x2026;](https://www.theguardian.com/science/2022/jul/31/why-you-need-to-worry-about-the-wet-bulb-temperature) 

<!--quoteend-->

> The [wet-bulb] temperature reading you get will actually change depending on how humid it is,” says Kristina Dahl, a climate scientist at the Union of Concerned Scientists. “That’s the real purpose, to measure how well we’ll be able to cool ourselves by sweating.”
> 
> &#x2013; [Why you need to worry about the ‘wet-bulb temperature’ | Climate science | Th&#x2026;](https://www.theguardian.com/science/2022/jul/31/why-you-need-to-worry-about-the-wet-bulb-temperature) 

