# forces of production

> refers to the combination of the means of labor (tools, machinery, land, infrastructure, and so on) with human labour power.
> 
> &#x2013; [Productive forces - Wikipedia](https://en.wikipedia.org/wiki/Productive_forces) 

Quite concomitant to a broad concept of simply '[[technology]]', but:

> Looking closely at the forces of production is not quite the same thing as the study of technology. The difference is that the former asks questions about [[agency]], and in particular [[class agency]].
> 
> &#x2013; [[Capital is Dead]]

