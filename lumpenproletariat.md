# lumpenproletariat

> Karl Marx and Friedrich Engels coined the word in the 1840s and used it to refer to the unthinking lower strata of society exploited by reactionary and counter-revolutionary forces, particularly in the context of the revolutions of 1848. They dismissed the revolutionary potential of the Lumpenproletariat and contrasted it with the proletariat. 
> 
> &#x2013; [Lumpenproletariat - Wikipedia](https://en.wikipedia.org/wiki/Lumpenproletariat) 

yet:

> the most spontaneous and the most radically revolutionary forces of a colonized people
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

