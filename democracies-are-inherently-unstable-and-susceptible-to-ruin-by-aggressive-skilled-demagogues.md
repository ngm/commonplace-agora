# Democracies are inherently unstable and susceptible to ruin by aggressive, skilled demagogues

A claim by [[Hannah Arendt]] (according to this article [Trump has birthed a dangerous new ‘Lost Cause’ myth. We must fight it | David&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/08/trump-has-birthed-a-dangerous-new-lost-cause-myth-we-must-fight-it))

[[Democracy]], [[demagogue]].

Because&#x2026;?


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

Confidence
: 2

Sounds plausible.  What are the becauses though?

