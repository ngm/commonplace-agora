# Arctic ice cap

> This was a [[feedback loop]] with teeth. The Arctic ice cap, which at its first measurement in the 1950s was more than ten meters thick, had been a big part of the Earth’s [[albedo]]; during northern summers it had reflected as much as two or three percent of the sun’s incoming insolation back into space. Now that light was instead spearing into the ocean and heating it up.
> 
> &#x2013; [[The Ministry for the Future]]

