# To Talk of Organization – on Nunes’ Neither Vertical nor Horizotal

An
: [[article]] and [[podcast]]

URL
: https://cosmonautmag.com/2022/02/to-talk-of-organization-on-nunes-neither-vertical-nor-horizontal/

Series
: [[Cosmopod]]

Nice overview and review of Rodrigo Nunes' book [[Neither Vertical Nor Horizontal]] on political organisation.  Also applies some of Nunes' ideas to the strategies of English Left post-Corbyn (stay and fight, form a new party, focus on [[base-building]]?)

