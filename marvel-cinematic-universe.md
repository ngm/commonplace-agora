# Marvel Cinematic Universe

Huge sprawling franchise.

Covers lots of interesting themes and ideas beneath the flashy action.

However, at root has a very American view of the world even when attempting to question that view.

