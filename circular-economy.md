# Circular economy

Alternative to [[linear economy]].

> Kenneth Boulding’s 1966 paper, [[The economics of the coming spaceship earth]], is often cited as the origin of the circular economy concept.
> 
> &#x2013; [[Degrowth, green energy, social equity, and circular economy]]

<!--quoteend-->

> MacArthur concluded that our economic system is fundamentally flawed. Her foundation promotes three principles: eliminate waste and pollution, keep products and materials in use (e.g., “[[right to repair]]”), and regenerate natural systems.
> 
> &#x2013; [[Degrowth, green energy, social equity, and circular economy]]

<!--quoteend-->

> David Suzuki, in “Circular economy is too important to be co-opted by industry,” warns that Canadian corporations are “greenwashing” using circular economy jargon
> 
> &#x2013; [[Degrowth, green energy, social equity, and circular economy]]


## Articles

-   Is unclear and lacks substance. [Circular economy is ‘unclear and lacks substance’, new report finds](https://www.circularonline.co.uk/news/circular-economy-is-unclear-and-lacks-substance-new-report-finds/).

