# integrated assessment models

> Systems engineers at key institutions have built massive supercomputer programmes called ‘Integrated Assessment Models’ (IAMs) which combine physics, chemistry, biology, and economics into a single simulation of the world for the next 300 or so years
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> Anytime one hears a prediction about climate projections for 2100, an engineer with an IAM has probably been tinkering behind the scenes with variables such as pollution taxes, probability of technological breakthrough, spatial patterns of agriculture and biofuels, global food demand, the makeup of energy systems, and the sensitivity of the climate and biosphere to all these social changes
> 
> [[Half-Earth Socialism]]

<!--quoteend-->

> The specialist modelling groups (referred to as Integrated Assessment Modelling, or IAMs) have successfully crowded out competing voices, reducing the task of mitigation to price-induced shifts in technology – some of the most important of which, like so-called “negative emissions technologies”, are barely out of the laboratory
> 
> &#x2013; [[IPCC's conservative nature masks true scale of action needed to avert catastrophic climate change]]

