# Sino-Soviet split

> So there is a certain sense that the Sino-Soviet split represented the living versus the decayed aspects of socialism, the vibrant sense of ‘tradition’ versus the dead one.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

<!--quoteend-->

> From the standpoint of the revolutionary left, a more specific impact was made by the Chinese Communist Party’s polemics against Nikita Khrushchev’s ‘revisionist’ notion of peaceful coexistence with capitalism during the Sino-Soviet split.
> 
> &#x2013; [['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]

