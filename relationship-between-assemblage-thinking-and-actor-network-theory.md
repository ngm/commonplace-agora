# Relationship between assemblage thinking and actor-network theory

> The similarities between assemblage thinking and ANT are striking. Both have a relational view of the world, in which action results from linking together initially disparate elements. Both emphasise [[emergence]], where the whole is more than the sum of its parts. Both have a topological view of space, in which distance is a function of the intensity of a relation. And both underscore the importance of the socio-material, i.e. that the world is made up of associations of human and non-human elements.
> 
> &#x2013; [Assemblage thinking and actor‐network theory: conjunctions, disjunctions, cross‐fertilisations](https://rgs-ibg.onlinelibrary.wiley.com/doi/full/10.1111/tran.12117) 

