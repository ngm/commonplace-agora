# Spinach and mushroom risotto

&#x2013; source: [Mushroom and Spinach Risotto - Hungry Healthy Happy](https://www.hungryhealthyhappy.com/mushroom-and-spinach-risotto/) 


## Ingredients needed to make a Mushroom and Spinach Risotto (serves 2)

-   2 tbsp olive oil
-   150g/0.75 cup risotto rice
-   1 celery, diced
-   4 shallots, peeled and diced
-   2 garlic cloves, crushed
-   30g dried mushrooms, soaked for 30 minutes
-   500ml/2 cups of homemade vegetable stock
-   Juice of half a lemon
-   4 tbsp fresh parsley, chopped
-   A pinch of sea salt and black pepper
-   2 handfuls of spinach
-   20g shaved parmesan for serving (optional)


## How To Make Mushroom and Spinach Risotto - Step by Step


### One: Add the olive oil to a large pan and then add the shallots, celery and garlic

Gently cook for 3 minutes.


### Two: Add the rice, salt and pepper

Stir and cook for 1 minute.


### Three: Drain the mushrooms and add to the pan with a ladle of the stock.

Keep adding a ladle of stock at a time and waiting until the rice has absorbed the stock before adding another until you have used all the stock. This should take around 20-25 minutes.


### Four: Add the spinach and lemon juice

Stir it all in.


### Five: Serve

With fresh parsley and Parmesan on top.


## Notes

It was alright, not loads of flavour.  Maybe the stock didn't have a ton of flavour.  Also I didn't have any lemon juice, that probably would have given it more of a zing.

I didn't use parmesan, just some vegan cheese, which I stirred in rather than serving on top.

