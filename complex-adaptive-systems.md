# complex adaptive systems

I did a Masters degree in [[Evolutionary and adaptive systems]] which included complex adaptive systems.

These days I am very interested in their application to leftist politics.  [[Complexity and the left]].

> Complex adaptive systems are special cases of [[complex systems]] that are adaptive in that they have the capacity to change and learn from experience. Examples of complex adaptive systems include the stock market, social insect and ant colonies, the biosphere and the ecosystem, the brain and the immune system, the cell and the developing embryo, the cities, manufacturing businesses and any human social group-based endeavor in a cultural and social system such as political parties or communities.
> 
> &#x2013; [Complex system - Wikipedia](https://en.wikipedia.org/wiki/Complex_system) 

