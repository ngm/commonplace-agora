# Syndicalism

> building revolutionary [[trade unions]] to confront management in the workplace.
> 
> &#x2013; [Dual power - Wikipedia](https://en.wikipedia.org/wiki/Dual_power)

