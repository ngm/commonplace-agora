# Loss and damage

> “Loss and damage” is the phrase for the destruction already being wreaked by the climate crisis on lives, livelihoods and infrastructure. It has become a critical issue at [[Cop 26]], with the potential to make or break an ambitious deal in Glasgow.
> 
> Vulnerable and poor countries, which did little to cause the climate crisis, arrived with a determination to win a commitment from rich nations to compensate them for this damage.
> 
> It has become perhaps the most bitterly fought-over issue of all, with the low-income nations believing they have a moral right to this money – some call it compensation or reparations. Rich parties such as the US and EU are very reluctant to comply, fearing exposure to unlimited financial liabilities.
> 
> &#x2013; [What is ‘loss and damage’ and why is it critical for success at Cop26? | Cop2&#x2026;](https://www.theguardian.com/environment/2021/nov/13/what-is-loss-and-damage-and-why-is-it-critical-for-success-at-cop26) 

<!--quoteend-->

> Submitted by all 138 developing countries, this facility is the financial support the richest countries owe the poorest. A similar loss and damages facility demanded by island nations was likewise removed from the final agreement by “developed” countries fearful that such clauses could lead to legal liability for past emissions and open the door to calls for reparations
> 
> &#x2013; [[Climate Leninism and Revolutionary Transition]]

