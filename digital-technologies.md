# Digital technologies

> defined as telecommunications networks, data centers, terminals (personal devices) and IoT (internet of things) sensors
> 
> &#x2013; [[Digital Ecosocialism: Breaking the power of Big Tech]]

