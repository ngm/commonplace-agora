# Nature, ‘Post-Truth’ and Platform Capitalism (With Bram Büscher)

-   A [[podcast]]
-   

https://thefirethisti.me/2021/08/20/86-environmentalism-post-truth-and-platform-capitalism-with-bram-buscher/

Publisher
: [[The Fire These Times]]

Featuring
: [[Bram Büscher]] / [[Joey Ayoub]]


## Topics Discussed:

> -   Meaning of ‘[[post-truth]]’ and [[platform capitalism]]
> -   [[Environmentalism]], political action and [[social media]]
> -   Mediating knowledge and politics through [[new media]] platforms
> -   “Doom and gloom” versus “being optimistic”
> -   Temporality on social media and the urge of the ‘now’
> -   New media platforms are not neutral platforms
> -   [[Alienation]], politics and new media
> -   Can it be good?
> -   The role of new media in the conservation and environmental movements
> -   South Africa’s Kruger National Park, new media and racial politics
> -   The difference between understanding and knowledge, and how new media plays into that

