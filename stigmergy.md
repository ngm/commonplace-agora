# Stigmergy

I think I learned about this first during my [[Evolutionary and adaptive systems]] degree.  Might have read about it earlier through personal interest.

> Mark Elliot, whose doctoral dissertation is probably the best study on the subject to date, applied the term “stigmergy” to any form of human socialization in which coordination is achieved not by social negotiation or administration or consensus, but entirely by independent individual action against the background of a common social medium.
> 
> &#x2013; [[The Stigmergic Revolution]]

