# Metamodernity

I don't understand it yet.  But if it's kind of an attempt at some synthesis of [[Modernism]] and [[postmodernism]] that sounds worth looking in to more.

See [[metamodernism]].

> The short answer is metamodernity is a systems perspective; "it is about seeing the world as a process and not as fixed circumstances, a world in which there are not isolated phenomena but where everything is interconnected and interdependent&#x2026;"
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> More than a cultural trend (or 'ism'), metamodernity is a meaning-making code—one that encompasses cultural codes from every epoch of the human experience.
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> Andersen argues that "we need metamodern minds that can relate to the intimate indigenous, the existential premodern, the democratic &amp; scientific modern, and the deconstructing postmodern simultaneously" (p. 128).
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

Interesting, so a synthesis of [[Indigenous and traditional ecological knowledge]], [[Modernism]] and [[postmodernism]]?  And premodern as well?

> It is only through this synthesis and adoption of the metamodern code, she stresses, that we'll have the capacity to make good decisions to guide the necessary changes to our current systems and institutions.
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> The 'hyper-modern' alternative is not a good one; think: much an exaggerated version of what will turn out to be mere glimpses of what we're seeing right now—such as rise in authoritarianism, surveillance society, extreme inequality, and of course, climate change. 
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> Metamodernity provides us with a framework for understanding ourselves and our societies in a more complex way.
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> Metamodernity is a way of strengthening local, national, continental, and global cultural heritage among all.
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

<!--quoteend-->

> It thus has the potential to dismantle the fear of losing one's culture as the global economy as well as the internet and exponential technologies are disrupting our current modes of societal organization and governance.
> 
> &#x2013; [[Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]

