# Participation

> Participation is a term often used to describe Citizen involvement in government, community life, and organizations.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Today the term is usually invoked in a positive way to imply that citizen participation (in hearings, decision-making, or [[participatory budgeting]] programs) fulfills democratic ideals and confers popular legitimacy on the outcomes. This is precisely the deficiency of the term “participation,” however: it is often confined within a predetermined, top-down set of policy options and implementation strategies. The public does not really initiate and show sovereign political [[agency]] in a fuller sense. It merely “participates” in public debates and processes on terms that politicians, regulators, and other state officials have already found acceptable, giving the ultimate decisions a veneer of legitimacy. By contrast, [[Commoning]] is a more robust, independent act of [[political agency]].
> 
> &#x2013; [[Free, Fair and Alive]]

