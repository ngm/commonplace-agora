# Seize the means of computation

Seize the [[means of computation]].
Based on, of course, "seize the means of production.""

[[The Internet Con]] is subtitled "how to seize the means of computation".

I believe we should. My take: [[Reclaim the stacks]].

