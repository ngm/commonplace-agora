# koreader

URL
: https://github.com/koreader/koreader

An absolutely fantastic piece of [[libre software]] for reading ebooks.

I have it installed on my [[Kobo]].

The top features for me:

-   Night mode.  White text on a black background for reading at night.  Amazingly you don't get this on default OS for the Kobo.
-   Wallabag plugin.  You can download all your articles from Wallabag as epubs to read.
-   OPDS catalog plugin.  You can connect to a remote OPDS catalog and download books from there.  I connect it to my [[Calibre-Web]] instance.
-   Highlights.  I use this a lot.  Nothing too fancy but it does the job.  It's quite nice the way it handles when a highlights spans multiple pages.  I use [[KoHighlights]] to get the highlights on to my computer for processing.
-   Book Map is also nice.

