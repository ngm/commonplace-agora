# Smart cities

> The origins of the ‘smart city’ framework lie in a project championed by the likes of IBM and Cisco to optimise urban infrastructure through digital sensors that provide a variety of technological solutions to urban planning problems.
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Jathan Sadowski has shown that this movement has been underpinned by an ideology of importing entrepreneurialism into town halls to enable corporations to monetise urban spaces.
> 
> &#x2013; [[Platform socialism]]

-   Seoul city machine
    -   Saw it at the Centre for Chinese Contemporary Art
    -   Shows the tension between smart city looking after us vs controlling us
    -   https://vimeo.com/312092226


## Links

-   [Toronto swaps Google-backed, not-so-smart city plans for people-centred vision](https://www.theguardian.com/world/2021/mar/12/toronto-canada-quayside-urban-centre)

