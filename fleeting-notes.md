# Fleeting notes

All the half-baked ideas that pop into your mind.  Have an easy way to capture them.

I think it's a thing from Ahrens and/or Zettelkasten.

> These are all the ideas that pop into your mind. Always have a notebook or a note taking app with you to capture these. They can be very short—just one word for instance. The goal is just to remember them for a very short amount of time, until you can sit down and write a proper note
> 
> &#x2013; [How to take smart notes - Ness Labs](https://nesslabs.com/how-to-take-smart-notes)

If I'm at my computer, I capture with org-capture.  If I'm somewhere else, orgzly new note is where my fleeting notes go.  So they first go into my [[Inner garden]].  Which is really more of a personal to-do system, not so much a garden.

When I tidy my inner garden up I move the relevant fleeting notes (i.e. the ones I think might be worth putting public at some point) either in to my journal, or in to [[To FILE]] in my public garden.  They clog up my inner garden otherwise with random quotes and shit, which get in the way of the things I **actually** need to be doing with my life.

