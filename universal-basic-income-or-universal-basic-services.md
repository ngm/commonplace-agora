# Universal Basic Income or Universal Basic Services?

A
: [[podcast]]

URL
: https://neweconomics.org/2018/04/weekly-economics-podcast-universal-basic-income-universal-basic-services

Bit of a ding dong on [[universal basic income]] vs [[universal basic services]].  I found it a bit confrontational and not that illuminating.

-   UBS is kind of extension and sanctification of public services.
-   Against UBI: costs too much? Gives government one easy lever to switch off. Rich people get it. Perpetuates markets and capitalism.
-   Against UBS: &#x2026; Also too expensive? Removes ability to do/choose things oneself?

