# Britain's welfare system is "unfit for purpose" and in urgent need of reform

[[Britain's welfare system]].

[Britain’s welfare system ‘unfit for purpose’ with millions struggling, expert&#x2026;](https://www.theguardian.com/society/2022/jan/23/britains-welfare-system-unfit-for-purpose-urgent-reform-experts-regardless)


## Because

-   [[The basic rate of out-of-work benefits is at its lowest for 30 years]]
-   Britain has soaring prices of food
-   Britain has soaring prices of rent
-   Britain has soaring [[energy bills]]
-   These are forcing families to choose between basic essentials such as food and heat - [[Heat or eat]]
-   Growing numbers are being forced into debt and relying on food banks


## And

-   Millions more families will struggle to make ends meet amid the dual pressures of the pandemic and the spiralling cost-of-living crisis


## So

-   We need rapid reforms to the social security system to protect low-income families from extreme hardship
-   We need a fundamental reform of universal credit
-   We need a cut to VAT on energy bills
-   We need the expansion of the warm home discount scheme


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Don't really know enough in detail, but I believe all of it. 

