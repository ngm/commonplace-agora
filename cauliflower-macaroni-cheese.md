# Cauliflower macaroni cheese

-   source: https://www.bbcgoodfood.com/recipes/cauliflower-macaroni-cheese

-   Prep: 10 mins
-   Cook: 20 mins
-   Difficulty: Easy
-   Serves 4


## Ingredients

-   250g macaroni
-   1 head cauliflower, broken into pieces
-   25g butter
-   2 tbsp plain flour
-   2 tsp English mustard powder
-   450ml milk
-   100g cheddar, grated


## Method

-   Cook the macaroni following pack instructions, adding the cauliflower for the final 4 mins.

-   Melt the butter in a pan, then stir in the flour and mustard powder and cook for 2 mins. Gradually add the milk, stirring all the time to get a smooth sauce. Add three-quarters of the cheese and some seasoning to the sauce.

-   Drain the macaroni and cauliflower and stir into the cheese sauce. Transfer to an ovenproof dish, then sprinkle over the remaining cheese and flash under a hot grill until golden and bubbling. Serve with a green salad, if you like.


## Notes

This was indeed pretty easy, and tasted pretty decent.

I used Violife vegan cheese and oat milk and it seemed fine.

I didn't have any mustard powder so I chucked in a bit of paprika.

