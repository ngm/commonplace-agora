# The Green Transition – The Problem with Green Capitalism Part 1

(Documentary) Starting from: 00:30:22  Episode webpage: https://www.upstreampodcast.org/greentransitionpt1  Media file: https://traffic.libsyn.com/secure/bb336368-b933-4b06-88b6-e51256ac6d81/Green_New_Deal_Pt1.mp3?dest-id=3632352#t=1822

:CREATED:  <span class="timestamp-wrapper"><span class="timestamp">[2022-12-04 Sun 17:43]</span></span>

URL
: https://www.upstreampodcast.org/greentransitionpt1

Series
: [[Upstream]]

.

-   ~ 00:30:22 [[Thomas Sankara]], debt is neocolonialism.
-   ~00:43:50 [[Right to repair]], [[tool libraries]], as part of [[degrowth]].

