# THE FUTURE OF FOOD: How to achieve radical change with George Monbiot

A
: [[podcast]]

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/georgemonbiot

Featuring
: [[George Monbiot]]

George again, talking about the [[global food system]], the trouble with most [[farming]], [[precision fermentation]], etc.

Also some good stuff on his [[theory of change]] and behaviour change versus system change, [[social tipping point]]s, etc.  I like his take that there is quite often a latent desire for change, and that sometimes a technological advancement will come along that allows that desire to be realised.

