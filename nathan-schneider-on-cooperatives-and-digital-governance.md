# Nathan Schneider on Cooperatives and Digital Governance

URL
: https://david-bollier.simplecast.com/episodes/nathan-schneider-on-cooperatives-and-digital-governance

Contributors
: [[David Bollier]] / [[Nathan Schneider]]

So much great stuff in Nathan's work. Some of my takeaways: Resistance movements need alternative building.  Coops can be a means towards economic justice and social justice.  Coops can go awry without a culture of commoning.  Incubators and exit to community for growing coops.  

Distinct lack of principles when it comes to the governance of most software commons.   The culture and the tools suck.  Feminist economics can help a lot.

-   [[Resistance movements must also build alternatives]].
-   [[Coops can turn in to bad actors if there is no culture of commoning]].
-   [[Economic justice goes hand in hand with social justice]].
-   [[Coop incubators]] and [[exit to community]] are some ways of fostering coops.
-   [25:35] [[Free software governance]] is rife with [[Implicit feudalism]]. [[The software commons is one of the great commons of modern times]] but [[Most libre software projects are entirely lacking in the principles of governing a commons]].
-   [28:55] There is hidden and undervalued labour and [[The Tyranny of Structurelessness]] and other pitfalls that could have easily been avoided with some attention paid to [[Feminist economics]].
-   [00:30:37] [[There is a layer of governance infrastructure missing from the web]].  It should easily enable significant commoning.
-   [00:37:49] Not trying to turn governance into some techno-deterministic improvement project with blockchain etc. First of all simply want just to have online platforms catch up with existing history of governance!

