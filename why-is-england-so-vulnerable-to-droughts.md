# Why is England so vulnerable to droughts?

URL
: https://www.theguardian.com/news/audio/2022/aug/16/why-is-england-so-vulnerable-to-droughts-podcast

[[UK water shortages 2022]]

> Eight areas of England are officially in a drought.
> 
> It’s been a summer of extremely hot weather and very little rain. And the climate crisis has made these kinds of events much more likely, and much more severe when they happen.
> 
> But, as the Guardian’s environment reporter Helena Horton tells Michael Safi, it’s not the only thing making England so vulnerable to extreme heat. No substantial reservoir has been constructed in England since the Kielder Water dam was built in 1981. And across England and Wales, nearly 3bn litres of water are lost to leakage every day: two-fifths of the total. At the same time, huge dividends have been paid to water company shareholders, while their chief executives have been generously rewarded for their work.
> 
> Is it time for England to rethink its relationship with water?

