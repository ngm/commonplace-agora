# Bit Tyrants

https://revolutionaryleftradio.libsyn.com/bit-tyrants

Listened to this interview discussing the book "[[Bit Tyrants]]: The Political Economy of [[Silicon Valley]]" with Rob Larson on Rev Left Radio.  The bit tyrants here being the 5 big ones, Amazon, Google, Apple, Microsoft and Facebook.

The discussion touches on the "two guys in a garage" origin myth of lots of big tech, and the narrative that these new corporations are somehow better than the oil and steel monopolies of old, that they're 'good [[capitalism]]' with no violence.  But there's plenty of violence and exploitation - just hidden away somewhere in the [[supply chain]].

They also talk about how they got where they were mainly from network effects built off of the back of research from the public sector.

The proposed solution of "online socialism" seemed a bit barebones - just focusing on organising and unionising of tech workers.  Probably expanded upon more in the book, but here at least there wasn't any mention of building or using alternatives.

Also its apparently one chapter at the end of the book.  Fair enough, it's good to have scene-setting and an evidence-base of what the problem is, but I'm more interested these days in ideas for the solution.  

