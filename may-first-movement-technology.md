# May First Movement Technology

&#x2013; https://mayfirst.coop/en/

> May First Movement Technology is a non-profit membership organization that engages in building movements by advancing the strategic use and collective control of technology for local struggles, global transformation, and emancipation without borders.

