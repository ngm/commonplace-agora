# partition of India

> India’s bloody partition in August [[1947]], [&#x2026;] led to the deaths of at least 1 million Indians and the displacement of around 15 million
> 
> &#x2013; [‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen&#x2026;](https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy)

<!--quoteend-->

> The borders for two independent states were drawn on religious lines: Hindu-majority India, and Muslim-majority West Pakistan and East Pakistan (now [[Bangladesh]]).  In just a few months, thousands of years of cultural exchange and co-existence between India’s Hindu, Muslim and Sikh communities, nightmarishly unravelled into panic, then terror, with millions rushing for the hastily established new borders as violence erupted.
> 
> &#x2013; [‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen&#x2026;](https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy)

<!--quoteend-->

> In what was to become the British Raj’s swan song after two centuries of colonial rule, Cyril Radcliffe, a British judge who had never visited colonial India before, was appointed in July 1947 to carve through the ancient land within weeks.
> 
> &#x2013; [‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen&#x2026;](https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy)

