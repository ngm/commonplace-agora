# Utrecht

-   was a Roman outpost
-   on a river
-   st Martin, he cut his coat in half, that's where the coat of arms comes from
-   Pope Adrian, only Dutch pope
-   didn't last long, was not popular, tried to paint the Sistine chapel white
-   Holland has been invaded by the Vikings, the Spanish and the French (napoleon)
-   Pope Adrian taught Charles 5
-   domtower is 112m, no building allowed to be taller
-   there's a light walk that runs through the city
-   real estate in Utrecht a good idea&#x2026;. it's above sea level.
-   Parts of Rotterdam are 6m below
-   surnames and street names only arrived with Napoleonic invasion

