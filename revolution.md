# Revolution

> the institutional transformation of society by society
> 
> &#x2013; [[Common: On Revolution in the 21st Century]]

I'm on board for a [[social revolution]] in the 21st century.  My revolutionary flavour of choice is [[revolutionary socialism]].  [[Eco-socialism]], specifically, as [[to be a 21st century socialist is to be an eco-socialist]].  I am desiring of a revolution that leads to something akin to an [[ecological civilization]].

How to get from here to there?

> revolution never occurs just through the behaviour of a particular group, however big or small. It happens because masses of people, many of whom have never considered the matter before, demand change and put themselves at the centre of political events
> 
> &#x2013; [[Revolution in the 21st century - Chris Harman]]

**Strategy**: I hope that [[prefiguration]], education, agitation, and organisation ([[educate, agitate, organise]]) may lead us to a [[social tipping point]] that tips us towards harmony.  I believe you need both some of the spontaneous network and the party form to make this happen.   See [[Neither Vertical Nor Horizontal]].  A bit of both [[Climate Mao]] and [[Climate X]] if you will. 

**Tactics**: With revolution there is the thorny question of violence and struggle.  [[When bringing change we must minimise violence]].  I hope the minimum = 0 and that [[direct action]], [[civil disobedience]] and enacting positive alternatives in there here and now can suffice.   Probably naive.  But unlike in [[To Posterity]], I hope that we who wish to lay the foundations of kindness, can ourselves be kind.

**Demands**: TBC.  see e.g. [[Half-Earth Socialism]], [[ecological civilization]].

