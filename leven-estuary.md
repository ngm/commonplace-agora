# Leven Estuary



## Flora


### Sea aster

August 2022.

![[photos/sea-aster-at-estuary.jpg]]


### Common cordgrass

August 2022.  According to Wikipedia, this is not so desirable.  Seems to have adverse affect on salt marsh habitats.

![[photos/common-cordgrass-at-estuary.jpg]]


### Sowthistle

August 2022.  Don't know whether it's a field or marsh variety.  Likely marsh, given its location?

![[photos/sowthistle-at-estuary.jpg]]


## Fauna


### Heron and little egret in the estuary

![[photos/heron-and-little-egret.jpg]]

