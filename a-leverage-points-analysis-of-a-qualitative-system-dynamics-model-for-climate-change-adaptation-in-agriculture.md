# A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture

URL
: https://www.sciencedirect.com/science/article/pii/S0308521X21000056

[[Qualitative system dynamics modeling]]. [[Leverage points analysis]].  [[Climate change]]. [[Agriculture]].

> In this paper, we developed a method to analyze model behavior by identifying LPs within a QSDM.

<!--quoteend-->

> We applied a novel approach that is based on the classification of LPs into twelve levels of intervention following Meadows (1999) in order to address the first objective of our study, i. e. identification and analysis of potential LPs within the QSDM.

<!--quoteend-->

> Key parameters within the QSDM are identified according to the problem formulation. Subsequently, feedback loops are explored and associated with typical system archetypes (Senge, 1990). ‘Deeper’ levels of intervention that are related to the design and intent of the model are selected by incorporating informal knowledge identified in the interviews. LPs are identified and classified based on the QSDM analysis. 

<!--quoteend-->

> The visualization in form of a clock plot helps to evaluate the potential of an LP at a glance: The ‘shallow’ levels of intervention that are easy to change but have limited effect on system behavior usually indicate parameters and feedbacks.

<!--quoteend-->

> In contrast, the ‘deep’ levels of intervention with high potential for transformational change mainly reflect changes in the design and intent of the QSDM. We introduced a weighting system that provides a quantitative measure for the potential of an LP to become an effective adaptation measure.

<!--quoteend-->

> To achieve the second objective of our study, the LPs were translated into specific climate adaptation measures. We assessed the potential of stakeholders to implement adaptation measures corresponding to the respective LP on different time scales in a qualitative way.

Kind of seems like:

-   do a [[causal loop diagram]]
-   find interesting feedback loops within the diagram.  Use [[system archetypes]] to help with this.
-   situate each of those on the clock model of leverage points

