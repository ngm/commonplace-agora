# We should have public ownership of water

We should have [[public ownership]] of the UK [[water industry]].


## Because

-   [[Water privatisation]] has been bad for the UK water industry
    -   Lack of investment in infrastructure
    -   Profits over utility
-   Public ownership would improve the UK water industry


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

