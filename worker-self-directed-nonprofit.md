# Worker self-directed nonprofit

[- Workplace Democracy in Nonprofit Organizations - Sustainable Economies Law Ce&#x2026;](https://www.theselc.org/workplace_democracy_in_nonprofit_organizations)

> Guerrilla Media Collective is constituted as a non-profit and socially oriented worker owned cooperative legally registered in Andalusia, Spain. 
> 
> &#x2013; [[The DisCO Elements]]

-   [Guerrilla Media Collective S.Coop.And de Interés Social - Guerrilla Media Col&#x2026;](https://wiki.guerrillamediacollective.org/Guerrilla_Media_Collective_S.Coop.And_de_Inter%C3%A9s_Social)

