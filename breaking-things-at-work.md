# Breaking Things at Work

A
: [[book]]

Subtitle
: The [[Luddites]] are right about why you hate your job

Written by
: [[Gavin Mueller]]

More in-depth than its read-baity title might suggetst.  Gives a historical overview of [[Luddism]] and [[Marxism]] in relation to various struggles through history.

> In this book, I aim to show how technology developed by capitalism furthers its goals: it compels us to work more, limits our autonomy, and outmaneuvers and divides us when we organize to fight back. In response, a flourishing class struggle will necessarily target the machines of the day, and I document the moments where it has.

<!--quoteend-->

> There is a palpable rise in Luddite sentiments, as well as anti-capitalist ones. As the following chapters show, these attitudes complement each other, and hold the key to the future of radical politics.

<!--quoteend-->

> Our history is the Luddites’ as well, and their insight—that technology was political, and that it could and, in many cases, should be opposed—has carried down through all manner of militant movements, including those of the present.

<!--quoteend-->

> In the complaints of Turkle and Wu, we might hear echoes of Martin Heidegger, who criticized technology for alienating us, through its disenchanting and instrumentalizing nature, from the mystical experience of Being

<!--quoteend-->

> the problem of technology is its role in capitalism.

<!--quoteend-->

> “Twitter revolutions” in the Middle East have been ground into dust.


## Luddism

> While their manifesto specified that they did not oppose technology as such, the neo-Luddites’ opposition to everything from genetic engineering to television, computers, and “electromagnetic technologies” belied a debt to anti-civilization anarcho-primitivist politics.

<!--quoteend-->

> The wealthy and powerful understood machines as a method to accumulate power, and so too did the toiling classes over whom they wished to exert it.

<!--quoteend-->

> But Hobsbawm suggests something further: that through machine breaking itself, the Luddites composed themselves as a class by creating bonds of solidarity.

<!--quoteend-->

> Workers are organized and exploited according to their technical composition; they then develop the forms of struggle necessary for overcoming their divisions and fighting their exploitation.

<!--quoteend-->

> In thinking about the politics of machine breaking as a form of solidarity —a means of [[class composition]]—Linebaugh knits together a host of disparate yet contemporaneous struggles connected to the onslaught of primitive accumulation of the early nineteenth century.

<!--quoteend-->

> “Machinery hurtful to Commonality”

-   Marx and the Luddites

> Luddism is intellectually compatible with Marxism

<!--quoteend-->

> For Negri and his collaborator Michael Hardt, this self-valorizing and self-organizing character of immaterial labor implies an egalitarian future: “Immaterial labor thus seems to provide the potential for a kind of spontaneous and elementary communism.”


## Marxism-Morrisism

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-21 Fri 18:19]</span></span>:
They are called “labour-saving” machines—a commonly used phrase which implies what we expect of them; but we do not get what we expect. What they really do is to reduce the skilled labourer to the rank of the unskilled, to increase the number of the “reserve army of labour”—that is, to increase the precariousness of life among the workers and to intensify the labour of those who serve the machines (as slaves their masters).

Page 55 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-21 Fri 18:22]</span></span>:
Morris’s definition of socialism as a qualitative restructuring of work and society—a top-to-bottom reconceptualization of social relations, rather than simply a more equitable redistribution of existing work and goods—placed him in diametrical opposition to another utopian of his time, Edward Bellamy.

Page 56 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-21 Fri 18:24]</span></span>:
Socialistic bourgeois want all the advantages of modern social conditions without the struggles and dangers necessarily resulting therefrom. They desire the existing state of society, minus its revolutionary and dis-integrating elements. They wish for a bourgeoisie without a proletariat

Page 57 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-21 Fri 18:32]</span></span>:
Technology is an important site of these struggles: not only is militant opposition to technology a historical fact, but it can suggest a more liberatory politics of work and technology—one that is more easily supported by Marx’s work than are contemporary post-work utopias.

Page 58 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-21 Fri 18:33]</span></span>:
Furthermore, these struggles point toward the only vehicle for a liberation from capitalism: the composition of a militant struggling class that attacks capital in all its manifold dominations, including the technological.


## Tinkerers, Taylors, Soldiers, Wobs

-   [[Scientific management]]

-   [[Second International]]

Page 75 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-22 Sat 04:38]</span></span>:
Socialism cannot arise from a crippled and stagnant capitalism, but only from a capitalism carried to its highest point of productivity,” he wrote in 1924. An entire Marxist tradition of endlessly deferred action and disdain for the struggles in the “periphery” of capitalist production has followed in its wake. 


## Wobblies and the Technocrats

-   [[Sabotage]]

