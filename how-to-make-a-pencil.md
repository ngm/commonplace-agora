# How to Make a Pencil

URL
: https://logicmag.io/commons/how-to-make-a-pencil/

Author
: [[Aaron Benanav]]

[[Socialist economy]].  [[Planning]].  [[Otto Neurath]].

> The digital socialist focus on algorithms presents a serious problem. It risks constraining the decision-making processes of a future socialist society to focus narrowly on optimization: producing as much as possible using the fewest resources

<!--quoteend-->

> After all, the societies of the future will want to do more than just produce as much as possible using the fewest resources. They will have other goals, which are more difficult to quantify, such as wanting to address issues of justice, fairness, work quality, and sustainability—and these are not just matters of optimization

<!--quoteend-->

> This is where planning protocols come in. They streamline decision-making by clarifying the rules by which decisions are made. Deployed in concert with algorithms, protocols enable a range of considerations—besides those available to an optimization program—to enter into the planning process

<!--quoteend-->

> Putting both algorithms and protocols to work, people can plan production with computers in ways that allow their practical knowledge, as well as their values, ends, and aims, to become integral to production decisions. The result is something that neither capitalism nor Soviet socialism allowed: a truly human mode of production

[[Socialist calculation debate]]

> Of course, the prices that pencil makers use to make production decisions are not just random numbers. They are expressions of a living market society, characterized by decentralized decision-making, involving large numbers of producers and consumers

<!--quoteend-->

> As Mises’s student [[Friedrich Hayek]] later emphasized, an economy is not a set of equations waiting to be solved, either with a capitalist price system or a socialist computer. It is better understood as a network of decision-makers, each with their own motivation, using information to make decisions, and generating information in turn. Even in a highly digitally mediated capitalist economy, those decisions are coordinated through market competition. For any alternative system to be viable, human beings still need to be directly involved in making production decisions, but coordinated in a different way

<!--quoteend-->

> Mises and Hayek were correct to observe that people’s participation in decision-making will remain essential for any economy to function. Yet their vision also sets strict limits on who has the opportunity to exercise this agency

<!--quoteend-->

> In a socialist society, however, the entire population would control production. Decision-making power would be democratized, and this would almost certainly lead to different kinds of decisions being made

<!--quoteend-->

> Efficiency, whether calculated in terms of energy use, resource consumption, or labor time, would remain a concern, but it would no longer be the sole concern. It would simply be one of many. Other considerations—dignity, justice, community, sustainability—would also enter the picture

<!--quoteend-->

> We need to be able to make planning decisions on the basis of multiple, incommensurable criteria, and to coordinate these decisions across society. To do this, we must have agreed-upon procedures for making such decisions collectively—protocols

<!--quoteend-->

> During World War I, masses of workers joined militant rank-and-file movements demanding workplace democracy, including the [[Industrial Workers of the World]] in the US, the [[Shop Stewards Movement]] in the UK, the councilists in Germany, and the anarcho-syndicalists in Spain, France, and Italy

<!--quoteend-->

> We do not want software to substitute for the price mechanism

<!--quoteend-->

> To function at all, a society that replaces the single-minded focus on cost control with multi-criteria decision-making must use algorithms to help clarify the choices to be made and protocols to help structure the way it makes these choices

