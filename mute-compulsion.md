# Mute Compulsion

A
: [[book]]

Found at
: https://www.versobooks.com/en-gb/products/2759-mute-compulsion

Written by
: [[Søren Mau]]

Subtitle
: A Marxist Theory of the Economic Power of Capital

[[Marxism]]. [[Capital]].

In addition to coercion ([[violence]]) and consent ([[ideology]]), there is the mute compulsion of [[economic power]].

> offers a foundational analysis of [[power]], [[value]], [[capital]], and [[social reproduction]].


## Praise

> The book is likely the best single summary of contemporary Marxist thought and serves as an invaluable resource for introducing the uninitiated to many of the foundational themes of communist critique and the character of economic power within capitalism
> 
> &#x2013; [[Forest and Factory]]

Jathan Sadowski highly rates it.

> Mau’s book is written with a real analytical clarity that advances our critical, theoretical understanding of the relations and operations of those things in society and our lives.
> 
> &#x2013; [[TMK BC5: Mute Compulsion, Introduction]].

