# Updating to Emacs 29 on Linux Mint



## Why?

Mainly so as to be able to use `--init-directory`.

But I'm sure there's other useful things that'll come with it.

Let's have a look: [What's New in Emacs 29.1? - Mastering Emacs](https://www.masteringemacs.org/article/whats-new-in-emacs-29-1).

tree-sitter will probably have some benefits that I may or not notice. And pixel scroll precision will be nice.

I wonder if the native SQLite support will muss up org-roam (which makes use of SQLite) or whether it'll be fine out of the box.


## How

Let's have a look at [[Updating to Emacs 28 on Linux Mint]] for reference&#x2026;

Hmm, so that used a PPA.  Has that PPA not got 29?

Nope&#x2026; https://github.com/kelleyk/ppa-emacs/issues/34

There is another PPA: https://launchpad.net/~ubuntuhandbook1/+archive/ubuntu/emacs

However, if I use that, it gives an error 
dpkg: error processing archive /tmp/apt-dpkg-install-g9nwl5/03-emacs-common<sub>1</sub>%3a29.4+1-0build1~focal<sub>all.deb</sub> (&#x2013;unpack): trying to overwrite '/usr/include/emacs-module.h', which is also in package emacs28 28.1~1.git5a223c7f2e-kk3+20.04
So - remove the previous package?

Clearly trying to write over something from the other PPA.

And that's broken apt set up now, great.  emacs-common has unment dependencies.

Fixed that by removing some of the packages installed by that PPA.

OK, maybe I'll try a flatpak install&#x2026;

```shell
flatpak install https://dl.flathub.org/repo/appstream/org.gnu.emacs.flatpakref
alias emacs29='flatpak run org.gnu.emacs'
```

It gets an error at startup.  Seems to be this one:

https://github.com/syl20bnr/spacemacs/issues/16122

The suggested fix, of adding

```nil
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
```

didn't work for me.

I imagine if I update the spacemacs .emacs.d, then it'll work.  But I was installing Emacs 29 to be able avoid amending my .emacs.d&#x2026;

Bloody hell.  I'll stick with 28 for now.

Checking that 28 still works OK after all this faffing&#x2026;

