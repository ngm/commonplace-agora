# energy storage

Energy storage tends to refer to energy storage at the big scale.  Thinking about how energy can be stored at the grid scale.  Often related to [[renewable energy]], which can be less consistent in supply.

> Renewables are, by their nature, variable: the wind doesn’t always blow; the sun’s intensity isn’t always consistent. So backup supplies are essential.
> 
> &#x2013; [A key piece of the puzzle for a more energy-secure UK? Storage, and more of i&#x2026;](https://www.positive.news/society/a-key-piece-of-the-puzzle-for-a-more-energy-secure-uk-storage-and-more-of-it/)

Quite often linked to [[grid balancing]], where storage is used to smooth out supply and demand.

> With a growing proportion of our electricity coming from renewable sources, notably wind and solar, the fine art of [[grid balancing]] is even more important today. 
> 
> &#x2013; [A key piece of the puzzle for a more energy-secure UK? Storage, and more of i&#x2026;](https://www.positive.news/society/a-key-piece-of-the-puzzle-for-a-more-energy-secure-uk-storage-and-more-of-it/)


## Battery plants

> But the beauty of small-scale battery plants is that they can be rolled out surprisingly quickly. The Feeder Road plant only starts construction this spring, but will be online in the autumn. (Compare that to the decade – or longer – lead times of a nuclear plant.) And multiple battery storage projects across the country can effectively operate as a single, distributed power plant, offering greater resilience compared to single-site generators.
> 
> &#x2013; [A key piece of the puzzle for a more energy-secure UK? Storage, and more of i&#x2026;](https://www.positive.news/society/a-key-piece-of-the-puzzle-for-a-more-energy-secure-uk-storage-and-more-of-it/)

<!--quoteend-->

> Like most batteries, Thrive’s depend on [[lithium]], the sourcing of which can be controversial. For its part, says Clayton, Thrive does a lot of due diligence with its suppliers, making sure as far as possible that they stand up to ethical scrutiny. Meanwhile, he points to the potential for sourcing significant supplies of lithium closer to home, from the brine water produced as part of Thrive’s planned geothermal plant in Cornwall. “Carbon-free Cornish lithium is a really exciting prospect!”
> 
> &#x2013; [A key piece of the puzzle for a more energy-secure UK? Storage, and more of i&#x2026;](https://www.positive.news/society/a-key-piece-of-the-puzzle-for-a-more-energy-secure-uk-storage-and-more-of-it/)

<!--quoteend-->

> Right now, most batteries are made with lithium and are expensive with a large, physical footprint, and can only cope with a limited amount of excess power.
> 
> But in the town of Kankaanpää, a team of young Finnish engineers have completed the first commercial installation of a battery made from sand that they believe can solve the storage problem in a low-cost, low impact way. 
> 
> [Climate change: &amp;#x27;Sand battery&amp;#x27; could solve green energy&amp;#x27;s big &#x2026;](https://www.bbc.co.uk/news/science-environment-61996520)

See [[Sand battery]]

