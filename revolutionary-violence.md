# Revolutionary violence



## Counter-revolutionary violence

> Nearly a century later, this prediction was borne out by the massacre in Chile in 1973. As historian Eric Hobsbawm observed in its bloody aftermath, ‘the Left has generally underestimated the fear and hatred of the Right, the ease with which well-dressed men and women acquire a taste for blood’. One should not expect the neoliberals to meekly accept defeat
> 
> &#x2013; [[Half-Earth Socialism]]

