# Commoning is based on a very different ontology from capitalism

[[Commoning]] is based on a very different ontology from [[capitalism]].

> This is not widely appreciated because many people continue to view the commons through archaic ontological perspectives — which is to say, through the normative lens of modern, Western culture. 
> 
> &#x2013; [[Free, Fair and Alive]]

"The normative lens of modern, Western culture".  What is that?  It must surely be hard to pin down such a concept as 'Western culture'.  I'll take it here as a shorthand for [[rationalism]], [[individualism]], [[the humanization of nature]], etc.

Capitalism's ontology:

> They have internalized the language of separation and methodological individualism. They view objects as having fixed, essential attributes and being disconnected from their origins and context.
> 
> &#x2013; [[Free, Fair and Alive]]

Separation. Individualism. Fixed.

Commoning's ontology:

> Commoning has a different orientation to the world because its actions are based on a deep relationality of everything. It is a world of dense interpersonal connections and interdependencies. Actions are not simply matters of direct cause-and-effect between the most proximate, visible actors; they stem from a pulsating web of culture and myriad relationships through which new things emerge.
> 
> &#x2013; [[Free, Fair and Alive]]

Relationships.  Relationality.  Connection.  Interdependence.

