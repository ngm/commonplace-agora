# Energy price freeze

> Liz Truss has set out plans to freeze energy bills at an average of £2,500 a year for two years, as part of a package of support for homes and businesses that marks one of the biggest government interventions since the financial crisis
> 
> &#x2013; [Liz Truss to freeze energy bills at £2,500 a year average, funded by borrowin&#x2026;](https://www.theguardian.com/politics/2022/sep/08/liz-truss-to-freeze-energy-bills-price-at-2500-a-year-funded-by-borrowing)

<!--quoteend-->

> The freeze does not go far enough. The typical household will still be facing a £600 increase in its domestic energy bills, bringing its bills close to double what they were this time last year. 
> 
> &#x2013; [We’ve Won an Energy Bills Freeze. Now Let’s Demand More | Novara Media](https://novaramedia.com/2022/09/08/weve-won-an-energy-bills-freeze-now-lets-demand-more/) 

<!--quoteend-->

> The fairest thing to do to meet this cost is to tax UK energy production. The Treasury has estimated that UK gas and electricity producers will make £170bn in excess profits over the next two years. This is more than the cost of the price freeze. Every single penny of those profits will be taken from the pockets of households and businesses in the rest of the economy. It is completely fair to take them back.
> 
> &#x2013; [We’ve Won an Energy Bills Freeze. Now Let’s Demand More | Novara Media](https://novaramedia.com/2022/09/08/weve-won-an-energy-bills-freeze-now-lets-demand-more/) 

<!--quoteend-->

> But a windfall tax is only a short-term solution. If crises are worsening, we need to build systems to deliver essentials like energy (and water and food) resiliently and equitably. Britain’s privatised energy systems are not up to the task. They should be brought into public ownership.
> 
> &#x2013; [We’ve Won an Energy Bills Freeze. Now Let’s Demand More | Novara Media](https://novaramedia.com/2022/09/08/weve-won-an-energy-bills-freeze-now-lets-demand-more/) 

