# Fixing PlantUML in org-babel

I was receiving this error:

> Exception in thread "main" java.awt.AWT
> Error: Can't connect to X11 window server using ':1' as the value of the DISPLAY variable.

Seems to be because it isn't running headless.  Which, from `plantuml-java-args`, it looks like it should be.  However, when running via babel, it appears those args aren't used?

I made a fix by editing `\~/.emacs.d/elpa/26.3/develop/org-plus-contrib-20201116/ob-plantuml.el` and adding in "-Djava.awt.headless=true" to the args passed to the jar.

Changing:

```emacs-lisp
(t (list java
	 "-jar"
	 (shell-quote-argument (expand-file-name org-plantuml-jar-path))))))
```

to 

```emacs-lisp
(t (list java
	 "-jar -Djava.awt.headless=true"
	 (shell-quote-argument (expand-file-name org-plantuml-jar-path))))))
```

But this is temporary - it will be overwritten when org-plus-contrib package is updated.

And how come it was working a couple of weeks ago - what changed?


## <span class="timestamp-wrapper"><span class="timestamp">[2022-06-04 Sat]</span></span>

Shared by Liban via email:

> I was digging around trying to find out how to get PlantUML to work in
> org-babel in headless mode and came across your blog post. It prompted
> me to have a look at the source and found that you can add java
> arguments in the plantuml babel header[1], so something like:
> 
> \#+begin<sub>src</sub> plantuml :file diagram.svg :java -Djava.awt.headless=true
> 
> worked for me. Theres also a org-babel-default-header-args:plantuml
> variable to set things as default but I haven't tried that yet (only
> the one diagram for me so far).

