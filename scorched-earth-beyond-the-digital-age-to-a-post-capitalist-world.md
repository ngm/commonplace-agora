# Scorched Earth: Beyond the Digital Age to a Post-Capitalist World

A
: [[book]]

URL
: https://www.versobooks.com/books/3965-scorched-earth

> In this uncompromising essay, Jonathan Crary presents the obvious but unsayable reality: our ‘digital age’ is synonymous with the disastrous terminal stage of global capitalism and its financialisation of social existence, mass impoverishment, ecocide, and military terror. Scorched Earth surveys the wrecking of a living world by the internet complex and its devastation of communities and their capacities for mutual support.

Sounds worth a read.  Though I get the sense it doesn't explore [[alternative social media]].

