# digital inclusion

> Digital inclusion is much more than access to hardware. It also includes access to the internet and equipping people with the tools to use resources critically. 
> 
> &#x2013; [[What is true digital inclusion?]]

-   1.9 million UK households are digitally excluded. ([[The Reboot Playbook 2020]])
-   11 million used devices lie in homes and offices around the UK.  ([[The Reboot Playbook 2020]])

> In our communities, there are school children with no means of doing schoolwork or keeping in touch with their peers. Hidden from sight are elderly people with no way of connecting with their loved ones. Held back by circumstance are adults keen to improve their job prospects but unable to access free online training or search the internet for more fulfilling positions.
> 
> &#x2013; [[The Reboot Playbook 2020]]

<!--quoteend-->

> Unfortunately, these people – the ones who need the technology the most - are those least likely to be able to afford new devices and connections.
> 
> &#x2013; [[The Reboot Playbook 2020]]

<!--quoteend-->

> It would be impossible to buy the 1.9m new devices needed to get each digitally excluded household in the UK online. However, 11 million unused devices lie unused in homes and offices. By rehoming just 10% of these unused devices, we can narrow the digital divide.
> 
> &#x2013; [[The Reboot Playbook 2020]]

