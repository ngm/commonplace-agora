# BECCS

Bioenergy with [[carbon capture and storage]].

The idea being you grow some biofuel (e.g. trees) that captures carbon as it grows.  You then use that biofuel to produce energy, but then you capture and store that carbon (e.g. burying it underground).  Supposedly making the whole process carbon negative.

HES authors are very critical of BECCS.

> An effective BECCS programme that could sequester several gigatonnes of carbon would need at least 350 million hectares – an area larger than India
> 
> &#x2013; [[Half-Earth Socialism]]

