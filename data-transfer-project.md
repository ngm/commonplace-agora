# Data Transfer Project

> The Data Transfer Project is a good example of how companies pretend to believe in and work on [[interoperability]], without ever getting close to the promised outcome
> 
> &#x2013; [Redecentralize Digest — February 2022 — Redecentralize.org](https://redecentralize.org/redigest/2022/02/) 

