# When bringing change we must minimise violence

When bringing [[social change]] we must minimise [[violence]].

You may accept that [[social change has never come without violence]] and therefore that [[social change will never come with without violence]].  (I do not know if I do).

Still, if you do, you have to at least try and minimise it.

Of course, what is a 'minimum' is wide open for debate.  Perhaps also is what violence actually means.

