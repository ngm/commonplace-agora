# The death of the Unabomber: will his dangerous influence live on?

URL
: https://www.theguardian.com/news/audio/2023/jun/19/death-unabomber-ted-kaczynski-will-dangerous-influence-live-on-podcast

> [[Ted Kaczynski]], the Harvard-educated mathematician who ran a 17-year bombing campaign that killed three people, died in prison earlier this month. But his manifesto promoting violent rebellion against the modern world continues to inspire copycat attacks

