# For a Red Zoopolis

An
: [[article]]

URL
: https://www.midnightsunmag.ca/for-a-red-zoopolis/

Author
: [[Richard Seymour]]

Very good.  On [[eco-socialism]].  Felt pretty similar in outlook to [[Half-Earth Socialism]].  I like the description of what [[abundance]] could actually mean.

Also introduced me to the idea of a [[Zoopolis]].

-   [[The dream of superabundance is over]].
-   [[Energy is going to have to be rationed]].
-   [[Renewable energy cannot meet current demand]].
-   [[Degrowth is required to reduce demand]].
-   [[An eco-socialist definition of abundance]]

Also touches on [[ecofascism]], [[sixth extinction]], [[Meat consumption]], [[consumerism]].  Skeptical of the [[carbon footprint]].

His idea of a red zoopolis is interesting.  Though sounds similar to [[deep ecology]]?  Which has had its own accusations of ecofascism&#x2026;

> planet which is not a death-trap, and in which humans are not the self-loathing, guilty Anthropos of climate literature, but at long last the responsible stewards, and the agents of collective liberation.

