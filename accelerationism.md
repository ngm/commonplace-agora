# accelerationism

There's a few different versions of it.

> The new wave of acceleration began with Deleuze and Guattari’s book, [[Anti-Oedipus]] (1972), and in its most charmingly delirious form, Jean-François Lyotard’s Libidinal Economy (1974).8 It took a right-accelerationist turn in the writings of Nick Land, collected in the book Fanged Noumena (2011). I offered a left-accelerationist version in [[A Hacker Manifesto]] (2004).
> 
> &#x2013; [[Capital is Dead]]

<!--quoteend-->

> What I think of as a centrist accelerationism emerged later in Nick Srnicek and Alex Williams’s [[Inventing the Future]].
> 
> &#x2013; [[Capital is Dead]]

<!--quoteend-->

> The accelerationists are, as they often point out, subscribing to a prevalent view from within the Marxist tradition. Historically, Marxists have not been critical of technology, even when that technology is deployed in the workplace in ways that seem detrimental for workers. For many Marxists, technology is at worst neutral: it is not the technology itself, but who controls it, labor or capital. And for some of them, technology, even when wielded by capitalists, is a boon to socialism, creating the conditions of radical transformation right under the bosses’ noses. This means that a socialist movement should treat technological development, even if it has negative consequences in the short term, as something positive.
> 
> &#x2013; [[Breaking Things at Work]]

<!--quoteend-->

> Similarly, contemporary accelerationists such as [[Paul Mason]] and [[Aaron Bastani]] continue to refer to the passage as a harbinger to a future utopia
> 
> &#x2013; [[Breaking Things at Work]]

