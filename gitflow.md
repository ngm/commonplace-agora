# Gitflow

A [[git]] workflow.


## Do you need it?

If you're doing a continuously deployed web app, maybe not.  The gitflow creator noted 10 years after its inception, that it doesn't fit all project types: https://nvie.com/posts/a-successful-git-branching-model/


## Details

> This workflow doesn’t add any new concepts or commands beyond what’s required for the Feature Branch Workflow. Instead, it assigns very specific roles to different branches and defines how and when they should interact. 
> 
> &#x2013; [Gitflow Workflow | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 

<!--quoteend-->

> In addition to feature branches, it uses individual branches for preparing, maintaining, and recording releases.
> 
> &#x2013; [Gitflow Workflow | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 

<!--quoteend-->

> The master branch stores the official release history, and the develop branch serves as an integration branch for features. 
> 
> &#x2013; [Gitflow Workflow | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 

The develop branch contains the complete history of the project, whereas master contains an abridged version.  &lt;- why/in what way is master abridged?

> feature branches use develop as their parent branch. When a feature is complete, it gets merged back into develop.
> 
> &#x2013; [Gitflow Workflow | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 

<!--quoteend-->

> feature branches combined with the develop branch is, for all intents and purposes, the Feature Branch Workflow. But, the Gitflow Workflow doesn’t stop there.
> 
> &#x2013; [Gitflow Workflow | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 

