# commoning

The stuff [[commoner]]s do to enjoy, curate and steward a [[commons]].

> [[commoner]]s are engaged in in "world-making in a [[pluriverse]]" because that phrase captures the core purpose of commoning: the creation of peer-generated, context-specific systems for free, fair and sustainable lives.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> commoning is primarily about creating and maintaining _relationships_ - among people in small and big communities and networks, between humans and the nonhuman world, and between us and past and future generations.
> 
> &#x2013; [[Free, Fair and Alive]]

See the [[Spheres of commoning]].

