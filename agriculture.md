# Agriculture

> The advent of agriculture opened up the possibility of producing a surplus of food items, which could be stored for future need. Rather than the precariousness of hand-to-mouth living, the safety of that surplus on hand meant that communities could settle and grow, and some members of the group could specialize in the production of non-food items
> 
> &#x2013; [[A People's Guide to Capitalism]]

