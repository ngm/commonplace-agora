# The Social Dilemma

> One of my problems with "The Social Dilemma" is that it makes the same mistake a lot of tech observers are making: it treats social media as if its cigarettes &#x2013; something that's addictive and bad with no value at all. The Internet is more like sex, drugs and rock &amp; roll (thread)
> 
> -   [Thread by @evan<sub>greer</sub> about The Social Dilemma on Thread Reader](https://threadreaderapp.com/thread/1306341560548757505.html)

