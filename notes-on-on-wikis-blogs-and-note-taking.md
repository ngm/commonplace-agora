# Notes on "On Wikis, Blogs and Note Taking"

&#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

Ton has been thinking about, and using, blogs and wikis for a **long** time.  (At least 15 years).

> Ultimately **blogs and wikis are not either stock or flow to me but can do both**. Wikis also create streams, through recent changes feeds etc. Over the years I had many RSS feeds in my reader alerting me to changes in wikis. I feel both hemmed in by how my blog in its setup puts flow above stock, and how a wiki assumes stock more than flow. But that can all be altered. **In the end it’s all just a database, putting different emphasis on different pivots for navigation and exploration.**

In this sense, perhaps the terms wiki and blog are unhelpful, in that we have preconceptions about what each can and can't do.  Perhaps thinking at the level of values/requirements, about something which can give us both stock and flow, can help us then better think about what tool is appropriate.

> I often struggle with the assumed path of small elements to slightly more reworked content to articles. It smacks of the DIKW pyramid which has no theoretical or practical merit in my eyes. Starting from small crumbs doesn’t work for me as most thoughts are not crumbs but rather like Gestalts. Not that stuff is born from my mind as a fully grown and armed Athena, but notes, ideas and thoughts are mostly not a single thing but a constellation of notions, examples, existing connections and intuited connections.

Really interesting.  I really like this idea of patterns and Gestalts.  ("Gestalt psychologists emphasized that organisms perceive entire patterns or configurations, not merely individual components.")  For my wikiblog to help me learn and grow my thoughts, it definitely needs to help me see these Gestalts.  I do sometimes wonder about the merit of making every concept as small as possible. Perhaps in the right context, yes, but I don't feel that doing it dogmatically will be helpful to me.

> In those constellations, the connections and relations are a key piece for me to express. In wiki those connections are links, but while still key, they are less tangible, not treated as actual content and not annotated. **Teasing out the crumbs of such a constellation routinely constitutes a lot of overhead I feel, and to me the primary interest is in those small or big constellations, not the crumbs.** 

<!--quoteend-->

> The only exception to this is having a way of visualising links between crumbs, based on how wiki pages link to each other, because such **visualisations may point to novel constellations** for me, emerging from the collection and jumble of stuff in the wiki. That I think is powerful.

I've been thinking about how best to make use of org-roam's [[mapping]] features, and this might be part of it.

> A question that came up for me, musing about the conversation is what it is I am trying to automate or reduce friction for? If I am trying to automate [[curation]] (getting from crumbs to articles automagically) then that would be undesirable. Only I should curate, as it is my learning and agency that is involved. Having [[sensemaking]] aids that surface patterns, visualise links etc would be very helpful. Also in terms of timelines, and in terms of shifting vocabulary (tags) for similar content.

