# Cloud apps are problematic

> When you do some work with local-first software, your work should continue to be accessible indefinitely, even after the company that produced the software is gone.
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html)

<!--quoteend-->

> On the other hand, cloud apps depend on the service continuing to be available: if the service is unavailable, you cannot use the software, and you can no longer access your data created with that software
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html)

<!--quoteend-->

> However, in the cloud, ownership of data is vested in the servers, not the users, and so we became borrowers of our own data. The documents created in cloud apps are destined to disappear when the creators of those services cease to maintain them. Cloud services defy long-term preservation. No Wayback Machine can restore a sunsetted web application. The Internet Archive cannot preserve your Google Docs
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html) 

