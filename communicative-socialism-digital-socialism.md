# Communicative Socialism/Digital Socialism

Author
: [[Christian Fuchs]]

URL
: https://www.triple-c.at/index.php/tripleC/article/view/1144

Page 7 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:43]</span></span>:
3.2. Production and Labour

Page 7 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:44]</span></span>:
Toil is for Marx and Engels incompatible with a socialist society. They see the reduction of necessary labour and the abolition of hard labour as an important aspect of

Page 8 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:46]</span></span>:
In a high-tech socialist society, where the division of labour is abolished, humans are enabled to freely choose their work activities and to be active as well-rounded individuals who engage in many different activities. This means that humans can freely use their capabilities. Labour turns into free activity without a struggle for survival and coercion by the market.

Page 8 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:47]</span></span>:
In a socialist society, production takes place based on the principle of human need and not based on the principle of profit (the need of capital). This requires some form of planning of production:

Page 8 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:49]</span></span>:
Part of the reason state communism failed was that the central planning of human needs and the economy failed. Economies are complex and have unpredictable features.

Page 8 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 07:56]</span></span>:
Economic planning needs to be decentralised, which in a networked and computerised society can take on the form of a decentralised collection of the goods that individuals and households require. This information can then be sent to production units that thereby know how many goods are required during a certain period of time. Networking of production within industrial sectors enables the comparison of the available production capacities and productivity levels, which enables the production of the right amount of goods.
● not quite so simple&#x2026;

Page 8 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:38]</span></span>:
For Marx and Engels, socialism includes a democratic economy, in which the workers own the means of production in common.

Page 9 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:40]</span></span>:
348). 3.3.

Page 9 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:41]</span></span>:
Marx and Engels distinguish between two phases of a socialist society: crude communism as the first stage and fully developed communism as the second stage.

Page 9 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:42]</span></span>:
In the first stage, there is the elimination of capital, profit and the private property of the means of production, but not necessarily the abolition of wage-labour, money, exchange, and commodities. The means of production are collectively owned, but the productive forces are not yet developed to the stage that allows the abolishment of necessary labour.

Page 9 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:44]</span></span>:
In the higher, fully developed form of communism, the means of production are highly developed so that necessary labour and exchange are abolished:

Page 10 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:45]</span></span>:
From each according to their abilities, to each according to their needs” is a central communist principle. In a fully developed communist society, there is no wage-labour and no compulsion to work.

Page 10 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 14:45]</span></span>:

1.  Socialist Politics

Page 10 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 15:03]</span></span>:
Freedom is an important principle of democratic societies. For Marx, socialism means the abolition of alienation and the realisation of true freedom that allows humans to fully develop their potentials. A socialist society is a democracy and a humanism, in which the freedom of all interacts with individual freedom.

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:34]</span></span>:
Progressive politics that socialists support include, for example, a “heavy progressive or graduated income tax”, the “[e]xtension of factories and instruments of production owned by the State”, the “[c]ombination of agriculture with manufacturing industries; the “gradual abolition of all the distinction between town and country by a more equable distribution of the populace over the country”, or the “[f]ree education for all children in public schools”

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:35]</span></span>:
of the populace over the country”, or the “[f]ree education for all children in public schools” (Marx and Engels 1848, 505). 5. Socialist Culture

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:35]</span></span>:
5.1. Togetherness

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:35]</span></span>:
Whereas capitalist culture through the logic of commodity consumption advances a culture of isolation and individualisation focused on the individual consumption of commodities and the competition for reputation, socialist culture means the development of a common culture, where humans associate and produce and consume culture together:

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:36]</span></span>:
5.2. The Family

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:37]</span></span>:
Socialist society also changes personal relations and the family. It reduces the dependence and power relations within the family and thereby advances equality:

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:37]</span></span>:
5.3. Internationalism

Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:37]</span></span>:
and nationalism in a socialist society.

Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:38]</span></span>:

1.  A Model of Socialism

Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:38]</span></span>:
For Marx (1844, 296), communism as revolutionary socialism is the “reintegration or return of man to himself, the transcendence of human self-estrangement”, “the real appropriation of the human essence”, “fully developed humanism”

Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-17 Mon 21:40]</span></span>:
There are three dimensions of a socialist society: the socialist economy, socialist politics, and socialist culture.

Page 14 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:24]</span></span>:
Discussions about socialism in the 21st century have foregrounded the commons. The basic argument is that neoliberal capitalism has resulted in the commodification and privatisation of common goods that are either produced by all humans or that all humans need in order to exist.1

Page 14 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:26]</span></span>:
Michael Hardt and Antonio Negri (2017, 166) argue that there are two major forms of the commons: the social and the natural commons. These forms are divided into five subtypes: the earth and its ecosystems; the “immaterial” common of ideas, codes, images and cultural products; physical goods produced by co-operative work; metropolitan and rural spaces that are realms of communication, cultural interaction and cooperation; and social institutions and services that organise housing, welfare, health, and education (2017, 166).

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:28]</span></span>:

1.  Communicative/Digital Socialism

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:29]</span></span>:
7.1. Socialism and Technology

Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:29]</span></span>:
7.1.1. The Antagonism of Productive Forces and Relations of Production

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 00:37]</span></span>:
In the 21st century, information technology and the Internet are founded on an antagonism of class relations and the now networked productive forces. A good example is that the Internet allows the free sharing of information via peer-to-peer platforms and other technologies, which on the one hand questions the capitalist character of culture and so makes the music and film industry nervous, but on the other hand within capitalism can also constitute problems for artists who depend on deriving income from cultural commodities.

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 13:47]</span></span>:
Networks are a material condition of a free association, but the cooperative networking of the relations of production is not an automatic result of networked productive forces.

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 13:48]</span></span>:
7.1.2. The Antagonism Between Productive Forces and Relations of Production in the Grundrisse’s “Fragment on Machines”

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 13:48]</span></span>:
necessary labour time – the annual labour-time a society needs in order to

Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 13:49]</span></span>:
The capitalist antagonism between productive forces and relations of production is an antagonism between necessary labour (that technology ever more reduces) and surplus labour (that capital tries to ever more increase):

Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 13:50]</span></span>:
7.1.3. Technology and Time in Capitalism and Socialist Society

Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:08]</span></span>:
But its tendency always, on the one side, to create disposable time, on the other, to convert it into surplus labour.

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:33]</span></span>:
7.1.4. Fully Automated Luxury Communism?

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:34]</span></span>:
Bastani rightly reiterates Marx’s insight that communism requires material and technological foundations and can therefore only be a high-tech communism.
● debateable

Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:41]</span></span>:
A communist society requires prospective, critical technology assessment and communist tech ethics and tech policies that regulate new technologies. Technology is not just an economic issue, but also has political and cultural dimensions that are based on but not reducible to the economy.

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:43]</span></span>:
requires technological foundations, but high tech alone is not enough. Communism’s guiding principle is neither technology nor love of technology, but love.

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:48]</span></span>:
Bookchin reminds us that not all technologies are liberating. Technologies need to be combined with and shaped by environmentalism and communalism. Bookchin argues for an “ecological approach to technology that takes the form of ensembles of productive units, energized by solar and windpower units” (46).

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:48]</span></span>:
(convivial technologies), André Gorz (post-industrial socialism), Ernst Bloch (alliance technology), etc.

Page 19 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:48]</span></span>:
The list of theorists of socialist technology could be continued with names such as Herbert Marcuse (post-technology), Erich Fromm (humanised technology), Ivan Illich

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:49]</span></span>:
Fully Automated Luxury Communism is, despite its tendencies of idealist utopianism and historical blindness, good food for thought about the foundations of communism.

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:49]</span></span>:
7.2. Socialism and Communication

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:50]</span></span>:
and Communication 7.2.1.

Page 20 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 14:53]</span></span>:
Alienated and socialist forms of knowledge and communication 7.2.2. Public Service Media and Community Media

Page 21 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 15:11]</span></span>:
The political economist of communication Graham Murdock (2011, 18) argues that the three political-economic possibilities in the media and cultural sector are ownership by capital (the commodity form of communications), by the state (the public service form of communications) and by civil society (communications as gifts/commons).
● podcasts as gift economy / commons

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:30]</span></span>:
Community media are, in capitalism, often based on voluntary, self-exploitative, unpaid or lowpaid labour. The history of alternative and community media is a history of self-determined but precarious labour and resource precarity.

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:32]</span></span>:
In the 20th century, many states held monopolies over telecommunications, broadcasting networks, railways, and the postal service. These are large infrastructures of communication. Organising them as public services enables fair, universal, affordable access. Today, there is a need for public service Internet platforms, such as a public service YouTube run by a network of public service companies, in order to challenge

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:33]</span></span>:
Alternatives to communication services that store and process lots of personal data, such as Facebook, should be organised as self-managed community platforms because too much involvement by the state poses a certain danger of state surveillance potentials directed towards citizens.

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:34]</span></span>:
7.2.3. Rosa Luxemburg: The Freedom of the Press in Socialist Society

Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:34]</span></span>:
Rosa Luxemburg on the one hand supported the need to replace the Czarist regime by a socialist society and on the other hand stresses the need for the democratic character of such a society.

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:35]</span></span>:
Luxemburg criticised the curtailment of the freedom of expression in Russia under Lenin.

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:36]</span></span>:
service media organisations and self-managed media organisations. 7.2.4. Democratic Communications

Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:38]</span></span>:
In his book Communications, Raymond Williams (1976, 130-137) distinguishes between authoritarian, paternal, commercial and democratic communication systems (communications). The first three communication systems are political,

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:39]</span></span>:
7.2.5. Socialist Journalism

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:51]</span></span>:
Fogarasi implicitly applies Georg Lukács’ (1971) critique of reified consciousness to the capitalist press. In the capitalist press, the focus is often not on the dialectic of totality, particularity and individuality, but merely on individual, isolated pieces of news. According to Fogarasi, strategies of the capitalist press include reporting a multitude of isolated facts that quench the readers’ thirst for knowledge; de-politicisation and sensationalism that work “systematically in the service of such diversion” (1921/1983, 150); and pseudo-objectivity. In contrast, the socialist press tries to advance the consciousness of society as totality and of the relation of single events with each other and broader contexts, the unmasking of the capitalist press, and the participation of readers as producers of reports
● stream vs gsrden

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 16:52]</span></span>:
7.2.6. Digital Commons

Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:01]</span></span>:
Given that computing has become a central resource in modern society, the use of computers for organising cognition, communication and cooperation has become a human need.

Page 25 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:02]</span></span>:
being loved and recognised), communicative needs (such as friendships and community) and co-operative needs (such as working together with others in order to achieve common goals) in all types of society. In a digital and information society, computers are a vital means for realising such needs. But given that computers are always used in societal contexts, computer use as such does not necessarily foster the good life, but can also contribute to damaging human lives.

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:04]</span></span>:

1.  Conclusion: Ten Principles of Communicative/Digital Socialist Politics

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:07]</span></span>:
both public services and civil society as the realms from where alternatives emerge. There are ten principles of communicative/digital socialist politics: 1. Techno-dialectics:

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:07]</span></span>:
How can technology and society be shaped in manners that benefit all humans, workers and citizens and develop the positive potentials of society and humanity?

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:07]</span></span>:
Instead, it asks: How can technology and society be shaped in manners that benefit all humans, workers and citizens and develop the positive potentials of society and humanity? 2. Radical

Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:08]</span></span>:
workers: Communication corporations exploit different kinds of workers. Alternatives to communicative

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:08]</span></span>:
Collective control of the means of communication as means of production:
● governable stacks

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:18]</span></span>:

1.  Break-up of communication monopolies:

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:18]</span></span>:
privacy: Public and commons-based communications should respect users’ privacy and minimise their economic and political surveillance as well as other forms of surveillance. Personal

Page 27 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:19]</span></span>:

1.  Public service media and communications co-operatives:

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:20]</span></span>:

1.  Democratic, public sphere media:

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:21]</span></span>:
Alternatives decelerate information flows (slow media), foster informed political debate and learning through collective creation, and participation in spaces of public communication that are ad-free, non-commercial, and not-for-profit.

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:55]</span></span>:
classist, fascist, racist, xenophobic and sexist discourse. 9. Political and protest communication: Communication technologies are not the cause of protests, rebellions and revolutions, but they are an important part of

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:56]</span></span>:
Socialist communication politics seeks to use communication technologies for spreading socialist politics to a broad public.

Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2022-10-18 Tue 17:56]</span></span>:
Self-managed, democratic governance:

