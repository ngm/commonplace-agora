# software obsolescence

> "software obsolescence" occurs when the manufacturer of a device discontinues the support for software that is necessary to run the device properly.
> 
> &#x2013; [Upcycling Android - FSFE](https://fsfe.org/activities/upcyclingandroid/upcyclingandroid.en.html) 

<!--quoteend-->

> It becomes particularly problematic when the old version of the software is no longer supported and at the same time a supported successor version can no longer be executed on the existing hardware. In this case, the manufacturer runs consumers into the dilemma of either buying new hardware or living with outdated software and potential security problems.
> 
> &#x2013; [Upcycling Android - FSFE](https://fsfe.org/activities/upcyclingandroid/upcyclingandroid.en.html) 

<!--quoteend-->

> Software obsolescence is a major concern throughout the [[Android]] world
> 
> &#x2013; [Upcycling Android - FSFE](https://fsfe.org/activities/upcyclingandroid/upcyclingandroid.en.html) 

