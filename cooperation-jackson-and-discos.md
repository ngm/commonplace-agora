# Cooperation Jackson and DisCOs

[[Cooperation Jackson]] and [[DisCO]]s.

> CJ have been a substantial inspiration for DisCO and are now in the process of becoming a series of federated DisCO LABS within Jackson.
> 
> &#x2013; [Cooperation Jackson – DisCO.coop](https://disco.coop/labs/cooperation-jackson/)

<!--quoteend-->

> They have agreed to be a DisCO LAB and we will be working closely with them to a) learn from their experience and b) co-create DisCO models for some of their projects. We will also pay attention to their intra-project [[ValueFlows]].
> 
> &#x2013; [Cooperation Jackson – DisCO.coop](https://disco.coop/labs/cooperation-jackson/)

