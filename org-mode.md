# org-mode

Find some more info in [[My Spacemacs User Config]].

See also [[org-mode and GTD]].


## Publishing a knowledge base with org-mode

This personal wiki is [[written in org-mode]] style plain text and exported to HTML.


### Useful links

-   [Publishing Org-mode files to HTML](https://orgmode.org/worg/org-tutorials/org-publish-html-tutorial.html)
-   [Blogging with Org publishing](https://vicarie.in/posts/blogging-with-org.html)


## org-download

Oh man. Just discovered org-download.  So useful!

Works best with 'import %s' to my mind.  gnome-screenshot blocks the window, whereas with import I can still tab around and select which window to capture and all that.


## org-super-agenda

I use this to get a better layout in org-agenda.


## org-agenda

Dang, the marking and performing actions of multiple items in the agenda is super useful.  Especially to think you can write your own arbitrary function to run, too.


### Archiving

[[Speeding up bulk archiving in org-agenda]] 


## PDF Tools

I added this via the pdf layer from spacemacs.  It's handy for sure.

