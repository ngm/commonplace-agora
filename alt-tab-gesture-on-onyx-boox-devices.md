# Alt-tab gesture on Onyx Boox devices

I'd really like a quick alt-tab gesture in my [[Boox]] Note Air Plus 2.

The quickest currently is to open up the navigation circle, select the task switcher, then choose the app.

https://www.reddit.com/r/Onyx_Boox/comments/xnu6z6/alttab_gesture_on_note_air_2/

^ someone asking the same thing there, with no answer.

Actually, at least they mention there that you can set a gesture to open the task switcher with a swipe from the bottom of the screen - that's one step better than using the navigation circle.

