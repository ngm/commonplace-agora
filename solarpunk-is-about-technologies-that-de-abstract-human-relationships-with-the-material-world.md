# Solarpunk is about technologies that de-abstract human relationships with the material world

[[Solarpunk]]

Heard from [[Andrew Dana Hudson]] on [[The Political Economy of Solarpunk (with Andrew Dana Hudson)]]


## Because

&#x2026;


## Contrast

-   [[Cyberpunk was about technologies that abstract human relationships with the world]]


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Signs point to yes]]

I like this.  The idea of de-abstracting relationships with the material world.  Similar to why some of the ideas from [[modern paganism]].

