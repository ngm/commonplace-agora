# Money

> Value—which is just a crystallization of abstract labor—is represented by money
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Money conveniently stores value over time, which its owner can dispense of as he or she sees fit.
> 
> &#x2013; [[A People's Guide to Capitalism]]

