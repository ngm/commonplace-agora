# Policing bill

-   https://bills.parliament.uk/bills/2839

[[I strongly dislike the policing bill]].

> The bill includes powers for the home secretary to ban marches and demonstrations that might be “seriously disruptive” or too noisy; a criminal offence of obstructing infrastructure such as roads, railways, airports, oil refineries and printing presses; jail sentences for attaching or locking on to someone or something; bans on named individuals from demonstrating or going on the internet to encourage others; police stop-and-search powers to look for protest-related items; and up to 10 years’ prison for damaging memorials or statues
> 
> &#x2013; [Thursday briefing: ‘A man without shame’ | | The Guardian](https://www.theguardian.com/world/2022/jan/13/thursday-briefing-a-man-without-shame)

<!--quoteend-->

> Tucked away in the government’s 300-page police, crime, sentencing and courts bill, are various clauses which will have serious implications for the right to protest. The bill seeks to quietly criminalise “serious annoyance”, increase police powers to restrict protests, and give the home secretary discretion over what types of protests are allowed.
> 
> &#x2013; [The police bill is not about law and order – it’s about state control | Joshu&#x2026;](https://www.theguardian.com/commentisfree/2021/aug/09/police-bill-not-law-order-state-control-erosion-freedom) 

<!--quoteend-->

> A new policing bill that will be debated this week risks deepening racial and gender disparities in the justice system while forcing professionals to betray the trust of vulnerable people, hundreds of experts and a report have warned.
> 
> &#x2013; [Policing bill will deepen racial and gender disparities, say experts | Police&#x2026;](https://www.theguardian.com/uk-news/2021/sep/13/policing-bill-will-deepen-racial-and-gender-disparities-say-experts) 

<!--quoteend-->

> As home secretary, Priti Patel has been explicit that the draconian anti-protest provisions of the Police, Crime, Sentencing and Courts Bill are directed against new environmental and racial equality movements in particular.
> 
> &#x2013; [[Tribune Winter 2022]]

