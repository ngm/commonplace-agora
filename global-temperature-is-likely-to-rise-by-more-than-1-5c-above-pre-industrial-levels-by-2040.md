# Global temperature is likely to rise by more than 1.5C above pre-industrial levels by 2040

[[Global temperature]] is likely to rise by more than [[1.5C]] above pre-industrial levels by [[2040]]

So sayeth the [[IPCC Sixth Assessment Report]].

> concluding that it is “more likely than not” that the global temperature increase will hit 1.5C by 2040. Current temperatures globally are, on average, 1.1C above pre-industrial temperatures.
> 
> [IPCC: Half of global population 'highly vulnerable' to climate crisis impacts&#x2026;](https://www.edie.net/ipcc-half-of-global-population-highly-vulnerable-to-climate-crisis-impacts/)

<!--quoteend-->

> Global temperatures were likely to top 1.5C above pre-industrial levels in the next two decades
> 
> &#x2013; [Worst polluting countries must make drastic carbon cuts, says Cop26 chief | C&#x2026;](https://www.theguardian.com/environment/2021/aug/09/worst-polluting-countries-must-make-drastic-carbon-cuts-says-cop26-chief) 

<!--quoteend-->

> Within the next two decades, temperatures are likely to rise by more than 1.5C above pre-industrial levels, breaching the ambition of the 2015 Paris climate agreement, and bringing widespread devastation and extreme weather.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn)

