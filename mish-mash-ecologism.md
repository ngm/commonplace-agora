# Mish-Mash Ecologism

URL
: https://newleftreview.org/sidecar/posts/mish-mash-ecologism

Author
: [[Matthew Huber]]

Review / criticisms of [[Half-Earth Socialism]] and [[The Future is Degrowth]].

Calls for a socialist [[eco-modernism]].

In a nutshell, Huber's general thesis is:

> In this context, the climate left does not lack for utopian imaginaries, which can make for productive (and enjoyable) exercises. But such utopianism can too easily avoid the material realities of the world as it exists. **We need a climate politics that aims outward, beyond the already converted – towards the exploited and atomized working class.**

I strongly agree with that last bit - "We need a climate politics that aims outward, beyond the already converted – towards the exploited and atomized working class."  But I don't think it needs to be a polarising either/or.

He sees both [[Half-Earth Socialism]] and [[The Future is Degrowth]] as too utopian, and only of appeal to the [[professional-managerial class]] of the environmental movement - not to the working class.

> The Future of Degrowth envisions a ‘pluriverse’ of diverse and localized alternatives, letting a thousand degrowth flowers bloom, Half Earth Socialism is much bolder, imagining nothing less than planetary-scale ecological planning.

Riffs on [[Socialism: Utopian and Scientific]] to critique these current tendencies in [[eco-socialism]].

> Two hundred years on – and in the wake of the defeated aspirations of the 20th-century revolutions – utopian eco-socialists appear to be repeating the same pattern. A new ecological order will be conjured up out of their brains, trialed in micro-experiments – as in The Future of Degrowth – or, as in Half Earth Socialism, ‘imposed from without by propaganda’.

<!--quoteend-->

> What is missing here is any analysis of the concrete class relationships that both inhibit such transformations or might bring them about. For Engels, winning real socialism hinges on class struggle: ‘Socialism was no longer an accidental discovery of this or that ingenious brain, but the necessary outcome of the struggle between two historically developed classes – the proletariat and the bourgeoisie.’

<!--quoteend-->

> Hard not to think of Engels when one reads The Future of Degrowth’s evocation of the ‘pluriverse’ or ‘mosaic of alternatives’ which will supposedly overwhelm the tightly defended capitalist interests of the ‘Global North’.

The mish-mash of the title comes from Engels:

> A mish-mash of such critical statements, economic theories, pictures of future society by the founders of different sects, as excite a minimum of opposition; a mish-mash which is the more easily brewed the more definite sharp edges of the individual constituents are rubbed down in the stream of debate, like rounded pebbles in a brook.
> 
> &#x2013; [[Socialism: Utopian and Scientific]]

<!--quoteend-->

> Planetary proletarianization should be a central issue for eco-socialism: capitalism produces an urbanized majority with no direct relation to the ecological conditions of existence. The most pressing question of our times is how we can solve ecological problems while restructuring production to provision a society largely torn from the land.

<!--quoteend-->

> A socialist eco-modernism should make the transformation of production and the productive forces the fulcrum of any new relation to the planet.

<!--quoteend-->

> In sum, solving climate change requires new social relations of production that would develop the productive forces toward clean production.

<!--quoteend-->

> While the utopian eco-socialists would likely scoff at these as ‘[[techno-fixes]]’ – technological solutions which don’t challenge capitalist social relations – an eco-modern socialist perspective would insist these technologies will not be developed unless we challenge capitalist social relations.

Kai Heron's top-level summary of the article:

> Matthew Huber claims to present a ‘Marxist alternative’ to the ‘mish-mash ecologism’ and dead-end ‘utopianism’ that he says afflicts parts of the climate left. Finding evidence of these maladies in two recently published books – The Future is Degrowth by Aaron Vansintjan, Andrea Vetter, and Matthias Schmelzer, and Half-Earth Socialism by Drew Pendergrass and Troy Vettese – Huber makes an impassioned plea for the left to walk away from utopian arcadias and embrace the more realistic option of a ‘socialist eco-modernist… transformation of production’.
> 
> &#x2013; [[The Great Unfettering]]

