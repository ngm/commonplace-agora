# quick function to help extract bold sections from text into bullet points

Most narrative text is usually just verbose prose around a few relevant points.

So when I'm parsing some text, I bold the relevant bits.

Then I pull those out to review them as bullets.

This function helps quickly pull the bold text into bullets.

Love the fact you can so easily configure Emacs to do this kind of thing!

```elisp
(defun ngm/bold-to-bullets ()
  "Extract bolded sections from the selected region and place them after it in a bullet list."
  (interactive)
  (if (region-active-p)
      (let ((bolded-sections '())
	    (case-fold-search nil)
	    (region-start (region-beginning))
	    (region-end (region-end)))
	(goto-char region-start)
	(while (re-search-forward "\\*\\([^*]+\\)\\*" region-end t)
	  (push (match-string 1) bolded-sections))
	(setq bolded-sections (reverse bolded-sections))
	(goto-char region-end)
	(insert "\n- ")
	(insert (mapconcat 'identity bolded-sections "\n- "))
	(newline))
    (message "No region selected.")))
```

So for example, selection the following quote:

> Nullam eu ante vel est **convallis dignissim**.  Fusce suscipit, wisi nec facilisis facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.  Nunc aliquet, augue nec adipiscing interdum, **lacus tellus malesuada massa**, quis varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.  Curabitur vulputate vestibulum lorem.  Fusce sagittis, **libero non molestie mollis**, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  **Nam a sapien**.

And running `(ngm/bold-to-bullets)`, outputs the following:

-   convallis dignissim
-   lacus tellus malesuada massa
-   libero non molestie mollis
-   Nam a sapien

Also see [[progressive summarisation]].

