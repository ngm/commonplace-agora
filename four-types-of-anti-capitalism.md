# Four types of anti-capitalism

This is a handy frame from Erik Orin Wright.

> Historically, anticapitalism has been animated by four different logics of resistance: smashing capitalism, taming capitalism, escaping capitalism, and eroding capitalism.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


## [[Smashing capitalism]]


## [[Taming capitalism]]


## [[Escaping capitalism]]


## [[Eroding capitalism]]


## Flashcard


### Front

What are the four types of anti-capitalism?


### Back

-   Smashing
-   Taming
-   Escaping
-   Eroding

