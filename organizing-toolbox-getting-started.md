# Organizing Toolbox: Getting Started

A
: [[podcast]]

URL
: https://roarmag.org/2021/03/30/spadeworks-organizing-toolbox-getting-started/

Series
: [[Spadework]]

Mediation of making bodies comfortable and getting stuff done.

We're embodied. Bodies are important. All types of bodies. Bodies need to be comfortable.

~[00:17:34]

If we're dissenters, why we using facilitating that aims to quiet dissent. If we're exploited workers, why we using extractive work-style facilitation.

~[00:34:39] 

[[Issue identification]].

~[00:40:54]

[[Turning issues into demands]].

