# Read feeder

[[Ton]] made a [post](https://www.zylstra.org/blog/2020/04/federated-bookshelves/) recently about federated bookshelves, sparked by a [post](https://tomcritchlow.com/2020/04/15/library-json/) from [[Tom]].  It's an idea that [[Gregor]] has done a good bit of [thinking about](https://gregorlove.com/2020/04/a-lot-of-interesting-ideas/) from an IndieWeb perspective.

Book recommendations is something I'm always interesting in.  At base, all it needs is a feed you can follow just of what people have been reading.  I've set up a channel in my [[social reader]] called 'Good Reads', and subscribed to Ton's [list of books](https://www.zylstra.org/blog/category/myreads/), as the sci-fi focus looks right up my street.  If anyone else has a feed of read books, let me know!

![[Read_reader/2020-05-03_13-40-11_Y2gD4mv.png]]

I am keeping my own list of [[books I've read]] in my wiki - sadly not marked up in any useful way at present - something for me to do there.

