# Heterarchy

I like it. In terms of organisation, sounds similar to [[Institutional Analysis and Development]], [[Viable system model]], [[Horizontalism vs verticalism]], etc. 


## In commoning

> In a heterarchy, different types of rules and organizational structures are combined.  They may include, for example, top-down hierarchies and bottom-up participation (both of which are vertical), and peer-to-peer dynamics (which are horizontal).
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In a heterarchy, people can achieve socially mindful autonomy by combining _multiple types of governance_ in the same system.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Heterarchy brings together top-down and bottom-up (both hierarchical), and peer-to-peer dynamics. One can think of it as reconciling distributed networks and hierarchies.
> 
> &#x2013; [[Free, Fair and Alive]]


## In knowledge management

> Traditional note taking methods, including outliners (Workflowy, Dynalist) necessitate creation of preconceived hierarchies, built “top-down”. In contrast, the heterarchy of Zettelkasten is built organically from “bottom-up” via gradual forming of links between the notes.
> 
> &#x2013; [Heterarchy - Neuron Zettelkasten](https://neuron.zettel.page/heterarchy)

