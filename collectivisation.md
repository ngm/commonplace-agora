# Collectivisation

> Collectivisation saw to all these problems at once. It killed several million more people in the short term, and permanently dislocated the Soviet food supply; but forcing the whole country population into collective farms let the central government set the purchase prices paid for crops, and so let it take as large a surplus for investment as it liked
> 
> &#x2013; [[Red Plenty]]

