# Technology Networks

Established in the ’80s by the left-wing [[Greater London Council]].  Democratic, community-based technology spaces.  Some lessons for Hackspaces, Makerspaces, etc, in the current day.

> These were prototyping workshops, similar to “makerspaces” or “hackerspaces” today. People could walk in, get access to machine tools, receive training and technical assistance, and build things.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> Technology Networks were community-based prototyping workshops supported by the Greater London Council from 1983 until 1986. They emerged out of a movement for [[socially useful production]].
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Amongst [[GLEB]]’s first acts was the creation of Technology Networks. These community-based workshops shared machine tools, access to technical advice, and prototyping services, and were open for anyone to develop socially useful products. GLEB’s aim was to bring together the “untapped skill, creativity and sheer enthusiasm” in local communities with the “reservoir of scientific and innovation knowledge” in London’s polytechnics ([[Greater London Enterprise Board]] 1984a, 9-10). In keeping with the political ideals underpinning the initiative, representatives from trade unions, community groups, and higher education institutes oversaw workshop management.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Technology Network participants developed various prototypes and initiatives; including, electric bicycles, small-scale wind turbines, energy conservation services, disability devices, re-manufactured products, children’s play equipment, community computer networks, and a women’s IT co-operative. Prototype designs were registered in an open access product bank freely available to others in the community; and innovative products and services were linked to GLEB programmes for creating co-operative enterprises. Similar workshops were created in other Left-controlled cities in the UK
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Technology Networks were an attempt to recast innovation and inscribe it with a radical vision for society.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Ideas and enthusiasm for these workshops drew upon a wider movement for socially useful production, which in turn drew together strands of thought and activism from broader social movements, old and new. These included, [[workplace democracy]] and alternative industrial plans, community development activism, left environmentalist networks, radical scientists and alternative technologists, and, to a lesser degree, feminism. Workshops were conceived in movement terms of providing humancentred, skill-enhancing machine tools; developing socially useful products; and democratising design and production.

<!--quoteend-->

> Features in Technology Networks are not only relevant to [[FabLab]]s, [[Hackerspace]]s and other workshops, but also to current ideas and practices in [[participatory design]] and [[critical making]].
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> Technology Networks, reflecting the wider movement for socially useful production, contained tensions in terms of social purpose, cultures of knowledge production, and [[political economy]].
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The social tension was between spaces for product-oriented design activity, and spaces for network-oriented social mobilisation.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The cultural tension was between professional and codified technical knowledge and the tacit knowledge and experiential expertise of community participants.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> And tensions in political economy – between socialism-in-one-space and the neo-liberal turn nationally and internationally – meant insufficient (public) investment was available to develop initiatives into significant economic activity, and especially without transforming the initiative into capitalist form.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> A key lesson from this history is that radical aspirations invested in workshops, such as democratising technology, will need to connect to wider social mobilisations capable of bringing about reinforcing political, economic and institutional change.
> 
> &#x2013; [[Technology Networks for Socially Useful Production]]

<!--quoteend-->

> The designs for the things they built went into a shared “product bank” that other people could draw from, and which were licensed for a fee to for-profit firms to help finance the Networks. The innovations that emerged included wind turbines, disability devices, children’s toys, and electric bikes. Energy efficiency was an area of special emphasis.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The Networks themselves were governed by a mix of local residents, trade unionists, tenant organizers, and academics.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> One purpose of these spaces was to democratize the design and development of technology. This meant creating a participatory process whereby working-class communities could obtain the tools and the expertise they needed to make their own technologies.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> It meant producing to satisfy human need—what organizers at the time called “socially useful production”—rather than to maximize profit. Satisfying human need necessitated the direct involvement of the particular humans whose needs were to be satisfied, since they were the ones who knew their needs best.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> Sadly, the Networks were short-lived. Margaret Thatcher eliminated the GLC in 1986, and the Networks lost most of their funding.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> The Technology Networks are useful for thinking about how to abolish the online malls. It’s conceivable that municipalities could create something like the Networks today, places where large numbers of people could contribute to the imaginative work of remaking the upper floors of the internet. They could team up with designers and developers to build alternative online services. Some might be hyperlocal; others might become regional, national, or even international. Some might be informal and volunteer-run; others might be placed under public ownership, or serve as the basis for new cooperatives.
> 
> &#x2013; [[Internet for the People]]

<!--quoteend-->

> Here too the Technology Networks of 1980s London can offer inspiration. They weren’t just places where people built things. They were also organizing spaces. The act of prototyping products in a workshop could serve as a starting point for a broader conversation about what kinds of transformations would be needed to create a more equitable society. In the process of trying to solve their problems with technology, people came to realize that technology often fell short of solving their problems. Politics was needed. Along these lines, one of the Networks kick-started a campaign called “Right to Warmth” that involved organizing community energy efficiency initiatives, creating local energy cooperatives, and pressuring Margaret Thatcher’s government into putting more money toward energy conservation measures.
> 
> &#x2013; [[Internet for the People]]

