# hyper commonplace garden wiki

I'm not sure what to call it.  I think it's

-   a digital [[commonplace book]]
-   a [[digital garden]]
-   a [[personal wiki]]
-   a [[hypertext]]
-   an [[exobrain]]
-   a [[personal homepage]]

some combo thereof.

Because everyone loves a colophon: some more info on how I make this place [[here]]. 

