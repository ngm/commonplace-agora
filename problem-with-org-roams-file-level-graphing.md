# Problem with org-roam's file-level graphing

Slight problem at the mo being that org-roam's file-level graphing doesn't exclude the nodes in your exclude matcher.  I started a [thread to ask about that](https://org-roam.discourse.group/t/graph-nodes-not-excluded-on-file-level-graphs/1002).  

First thing that I tried was putting ~                   ,@(org-roam-graph&#x2013;expand-matcher 'file t)~ in `org-roam-graph--build-connected-component`.  That didn't work, because

```elisp
(files (or (if (and max-distance (>= max-distance 0))
	       (org-roam-db--links-with-max-distance file max-distance)
	     (org-roam-db--connected-component file))
	   (list file)))
```

already includes everything in `files`, so saying 'and not these other files' after the fact doesn't help.  So next I've just simply hacked the SQL in `org-roam-db--links-with-max-distance`.  Literally changes the raw SQL query.  Obviously not sustainable, but gets me through what I want to do for today, and something to return to.

