# Horizontalism vs verticalism

On the left, a long standing problem is: how do you go about 
[[Political organisation]].  With _very_ broadly speaking a historical split between [[horizontalism]] (being a more distributed [[anarchist]] approach) and  [[verticalism]] (being a more [[communist]] approach with a strong party form).

I first came across the sniping between camps in [[Inventing the Future]] I think.  They lay into "folk politics", their name for horizontalism.  Kevin Carson gives a [lengthy rebuttal](https://c4ss.org/content/50849) of their critique.

See [[critiques of horizontalism]] and [[critiques of verticalism]], but I am most interested in the [[synthesis of horizontalism and verticalism]].

