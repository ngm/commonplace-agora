# Data Garden

URL
: https://cyrus.website/data-garden

> By storing data nature’s way, in the DNA of plants, this work discusses the potential for truly green, carbon absorbing data storage, owned by the public rather than monopolistic corporations.

Art project, not necessarily reality anytime soon, but I love this as an idea and a provocation.

[[Solarpunk]].

