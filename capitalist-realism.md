# Capitalist Realism

![[2020-06-07_16-17-57_Capitalist_Realism_cover.jpg]]

Capitalist Realism is an extension of Jameson's [[Postmodernism, or, The Cultural Logic of Late Capitalism]].

> Emancipatory politics must always destroy the appearance of a 'natural order', must reveal what is presented as necessary and inevitable to be a mere contingency, just as it must make what was previously deemed to be impossible seem attainable

<!--quoteend-->

> Since 2009, when Fisher first defined capitalist realism as “the widespread sense that not only is capitalism the only viable political and economic system, but also that it is now impossible to even imagine a coherent alternative to it,” his theory has inspired similar periodizations.
> 
> &#x2013; [[Capitalist Catastrophism]]

<!--quoteend-->

> Capitalist realism, he explained, was more pernicious than Mason realized. Many of those who consciously oppose capitalism have often unconsciously accepted it as the unalterable backdrop to their anti-capitalism. From this perspective the Arab Spring and Occupy Movement look exactly how we should expect capitalist realist protests to look. Both struggles knew what they were against but they were incapable of articulating a vision of the future that broke with the capitalist horizon. Their demands, if they made any at all, were for an extension of bourgeois democracy and for a more equal distribution of capitalism’s ill-gotten gains. Capitalist realism, in other words, persisted.
> 
> &#x2013; [[Capitalist Catastrophism]]

<!--quoteend-->

> Capitalist realism had three essential features. First, that it was easier to imagine the end of the world than the end of capitalism; second, that the future was cancelled; and third, that the capitalist realist condition was a class project in need of constant reinforcement by the bourgeoisie. 
> 
> &#x2013; [[Capitalist Catastrophism]]

