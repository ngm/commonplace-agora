# A long overdue moment? The UK greens pushing for the nuclear option

An overview of [[ecomodernism]]. Contrasting its uptake in UK and elsewhere. [[Nuclear energy]]. [[Precision fermentation]]. That kind of stuff. ht @adamgreenfield@socialcoop

I guess there’s degrees of ecomodernism… If it’s coming at it from a position of ‘we need to hold our noses and do this to avoid planetary catastrophe – then later we’ll do something better’ I can engage with it. If it’s green capitalism (which the mention of decoupling growth from material usage hints at) then definitely at odds with it.

The article isn’t very nuanced how it kind of pitches only ecomodernists as pro-technology, and everyone else… not. I’m pro some technology, against some others, dependent on context.

Re: nuclear in particular I think Half-Earth Socialism did a pretty good job of arguing against.

