# The technologies of all dead generations

URL
: https://bentarnoff.substack.com/p/the-technologies-of-all-dead-generations

Author
: [[Ben Tarnoff]]

Reflections on how to build alternative, liberatory (digital) technologies.  (More philosophical than practical reflections).

> If there are technologies that propagate selfishness, why can’t we make technologies that propagate solidarity?

Calls for anyone building alternatives to maintain a healthy critical eye on what they're building, and recognising that we are always building in a historical context.

> Critique is how we make the map of the terrain that the creativity of the third wave must traverse.

<!--quoteend-->

> Capitalism introduces what appears to be a natural separation between the political and the economic, with the latter understood to be a space of impersonal, automatic “market forces.”

Describes three waves of algorithm accountability:

> The law professor Frank Pasquale says there have been two waves of research and activism around “algorithmic accountability.” The first wave was about “improving existing systems,” while the second wave was about asking whether certain systems “should be used at all—and, if so, who gets to govern them.” One of his examples is facial recognition software. A first-waver would point out that such software typically does a bad job with faces that aren’t white. A second-waver would ask, “if these systems are often used for oppression or social stratification, should inclusion really be the goal?” Why not abolish them, or at least severely restrict their use?

<!--quoteend-->

> The third wave, by contrast, not only makes technology an object of politics but a terrain of politics. It wants to do politics through technology. And this means that the third-waver must develop an analysis of the specificities of technology as a site of political struggle, in the same way that feminists had to develop an analysis of the specificities of the household as a site of political struggle. To do politics through technology, we need to understand how technologies do politics.

In a nutshell they are (from the slide in the article)

-   1st wave: harm reduction
-   2nd wave: abolition
-   3rd wave: alternatives

I wonder if in some sense these waves map to [[resist, regulate, recode]].

-   1st wave = regulate
-   2nd wave = resist
-   3rd wave = recode

> This is the famous formulation of the theorist [[Langdon Winner]], who points out that the political qualities of a technical object—which is to say, how it alters the existing distribution of social power—are, at least in part, internal to the object itself

<!--quoteend-->

> Steam power stuck because its political qualities helped advance the interests of a rising industrial capitalist class, who could use it to strengthen control over labor and increase profits.

<!--quoteend-->

> How do you take a technological inheritance that has been engineered for empire, racial subordination, and capital accumulation, and turn it against itself? How do you pursue a politics of liberation through a medium that is permeated with a politics of domination? If the technologies of all dead generations weigh like a nightmare on the brain of the living, then how do we wake up from the nightmare?

<!--quoteend-->

> Mastodon, in other words, has inherited the basic grammar of social media as configured by Twitter. And the question that we have to consider is to what extent this inheritance complicates, or perhaps even obstructs, the political program that has been (quite consciously) encoded into Mastodon as a technical artifact. Does it matter that Mastodon so closely resembles a platform whose guiding principles—maximization of user engagement in order to monetize user attention, centralized and opaque content moderation practices—are so antithetical to it? And if so, how precisely does it matter—what effects does it have?

<!--quoteend-->

> Doing politics through technology is hard. You have about as much control as someone piloting a hot air balloon; it’s hard to know exactly where you’re going to land.

<!--quoteend-->

> As the third wave of algorithmic accountability tries to build a new world in the shell of the old, all I’m asking is that we be mindful of the shell, that we keep faith with the traditions of critique that can help clarify the conditions within which our creativity must operate, that we cultivate some dialectical combination of humility and recklessness, and that we do our best to make something new in history without holding on to any hope of escaping it.

