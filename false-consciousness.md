# False consciousness

> False consciousness is a term used—primarily by [[Marxist]] sociologists—to describe ways in which material, ideological, and institutional processes are said to mislead members of the proletariat and other class actors within capitalist societies, concealing the exploitation intrinsic to the social relations between classes. 

Mentioned on [Episode 170: Guy Debord’s “Society of the Spectacle” (Part One)](https://partiallyexaminedlife.com/2017/08/14/ep170-1-debord/), in the context of half-hearted critique of [[the Spectacle]] just you being part of the Spectacle, feeding it.

