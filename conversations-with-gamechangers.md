# Conversations with Gamechangers

URL
: 

https://www.solidarityeconomy.coop/projects/conversations-with-gamechangers

> A series of conversations with radical grass-roots solidarity economy organisations across the world who are breaking new ground in their own contexts while building power in their communities.

"solidarity economy strategies for a new and better world"

-   webinars with different grassroots organisations
-   aim: internationalise and share experience from people in different contexts
-   of networks organising using solidarity economy, building from the grassroots
-   organising for a better life and the freedom of their society
-   SEA: small coop based in the UK

<!--listend-->

-   [[Conversations with Gamechangers: Cooperation Jackson]]
-   [[Aborîya Jin]]
-   [[Grassroots Liberation]]
-   [[Zameen Prapti Sangharsh Committee]]
-   [[Guerrilla Media Collective]].

