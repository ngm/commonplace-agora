# Earth after climate collapse

> Evolution itself will of course eventually refill all these emptied ecological niches with new species. The pre-existing plenitude of speciation will be restored in less than twenty million years.
> 
> &#x2013; [[The Ministry for the Future]]

