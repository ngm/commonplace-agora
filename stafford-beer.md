# Stafford Beer

> Beer’s admirers view his intelligence, breadth of knowledge, and willingness to think in unconventional ways as signs of misunderstood genius. On the other hand, his detractors paint a picture of a self-promoter who made grandiose claims that were not backed by his actual accomplishments
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Beer was an unusual cybernetician in his deep commitment to democratic control systems
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> He was not a typical socialist but looked the part of a 1970s business hippie – being a management consultant with a beard of biblical proportions – but he was far more like [[Neurath]] and [[Kantorovich]] than like Stewart Brand or Ed Bass
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> "The more I reflect on these facts, the more I perceive that the evolutionary approach to adaptation in social systems simply will not work any more. . . . It has therefore become clear to me over the years that I am advocating revolution"
> 
> &#x2013; Stafford Beer quoted in [[Cybernetic Revolutionaries]]

