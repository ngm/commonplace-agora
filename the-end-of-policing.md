# The End of Policing

![[2020-06-15_20-26-25_9781784782924-eee743ba813f726f0ef78c27fdea782c.jpg]]

There’s a pretty dark history to the origins of policing – intertwined with [[colonialism]], [[slavery]], and industrial [[capitalism]].  That legacy still exists in the police today, as well as new issues , like the militarisation of the police force.  (Does the local police really need grenade launchers?)

> The origins and functions of the police are intimately tied to the management of inequalities of race and class. 
> 
> — The End of Policing, Alex Vitale

-   [Alex Vitale - The End of Policing (In Conversation)](https://soundcloud.com/upstreampodcast/alex-vitale)

