# Reclaim

Test index for the Agora on Reclaiming the Stacks.

> Technology appropriation and the establishment of commons-based platforms for social provisioning seem to be a potential counter-hegemonic response.
> 
> &#x2013; [[The Stack as an Integrative Model of Global Capitalism]]

-   . #pull [[Reclaiming the stacks: the role of ICT workers in a transition to ecosocialism]]

-   [[Ways to reclaim the stacks]]
-   [[Reclaiming the stacks: reading]]


## Resources


### Nodes

-   . #pull [[ICT and planetary boundaries]]


### Sites

-   . #pull [[Doing the Doughnut.tech]]


### Research papers

-   [[Technology appropriation in a de-growing economy]]
-   [[Digitalization and the Anthropocene]]

