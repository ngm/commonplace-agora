# Solidarity economy

> A solidarity economy is based on efforts that seek to increase the quality of life of a region or community through local business and not-for-profit endeavors. It mainly consists of activities organized to address and transform exploitation under capitalist economics and the large-corporation, large-shareholder-dominated economy, and can include diverse activities
> 
> &#x2013; [What is a Solidarity Economy? | Kirklees Solidarity Economy Network](https://www.ksen.org.uk/node/3) 

<!--quoteend-->

> SSE refers to enterprises and organizations (cooperatives, mutual benefit societies, associations, foundations and social enterprises) which produce goods, services and knowledge that meet the needs of the community they serve, through the pursuit of specific social and environmental objectives and the fostering of solidarity.
> 
> &#x2013; [Social and Solidarity Economy](https://www.ilo.org/global/topics/cooperatives/news/WCMS_546299/lang--en/index.htm)

I generally see the solidarity economy as a good thing.  Tends to involve [[worker cooperatives]], community banks, just generally community-based activity.


## Arguments against

This was an interesting article - [The Real Movement: Against The Solidarity Economy - Regeneration Magazine](https://regenerationmag.org/the-real-movement-against-the-solidarity-economy/).  As I understand it from first listen (listened to it read on Rev Left Radio), it has two main arguments - one being that the original proposal of a tweak to Marxist Theory to fit in the solidarity economy is theoretically flawed.  The theory is a bit beyond me at present, but main conclusion seems to be that Marxism just doesn't need the tweak proposed.  Fair enough, I think its good that the theory can be critiqued and defended like that, it makes it more rigorous.  I don't know if it counts as an argument against the solidarity economy though.

The second point is that the solidarity economy is simply not a good organising tactic - that it's not revolutionary, and thus can never bring down capitalism.  Or that it will work, but far too slowly to do what needs to be done for averting climate breakdown. That's a lot more to chew on I think.  I feel like it could be a bit less polarised - like maybe the things entailed in a solidarity economy are part of a revolutionary strategy.

But there's a lot of good stuff in Jackson Rising / [[Cooperation Jackson]], who use a lot of solidarity economy tactics in their daily organising.  And they seem pretty revolution oriented.

See also: [[types of organising]].


## Predistribution

"Pre-distribution was the idea that the state should try to prevent inequalities occurring in the first place rather than ameliorating them later through the tax and benefits system."


## Misc

> productive relationships where we consciously and deliberately depend upon each other
> 
> &#x2013; paraphrasing [[Kali Akuno]] in [[Conversations with Gamechangers: Cooperation Jackson]]


## Resources

-   [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]] [The Real Movement: Against The Solidarity Economy - Regeneration Magazine](https://regenerationmag.org/the-real-movement-against-the-solidarity-economy/)

