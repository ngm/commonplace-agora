# Net Zero Scrutiny Group

A nasty bunch.

"Arguing for an end to green levies and calling for more fossil fuel extraction."

> A loose but hugely influential hard-right coalition has cohered, buoyed up by our exit from the EU, and now determined to use questions about living costs and energy security to destroy any meaningful prospect of [[climate action]]
> 
> [Nigel Farage’s hard-right faction won Brexit. Now net zero is in its sights |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/13/nigel-farage-hard-right-faction-brexit-net-zero-tory)


## What do they want

They present it as generally being against the 'green agenda' because that they say environmental policies are costly to those affected by the cost of living crisis.  But quite blatantly they care about preserving their own (and their buddies') financial interests in investments in fossil fuels, etc, and are just attacking whatever jeopardises that.

Basically they want: 

-   the government to cut back its 'green agenda' (this is the Tory government mind, so it's barely an agenda)
-   [[fracking]] to be resumed in the UK
    -   'in order to boost fossil fuel production'
    -   'and help curb fuel price increases'
-   no raise in national insurance payments
-   no green levies
-   to cut taxes
-   to slow down the rate at which we impose carbon emission cuts

> Even before Russia launched its invasion and triggered a leap in fuel prices, some Conservative backbench MPs had been pressing for the government to cut back its green agenda, a move that has since been followed with calls for fracking to be resumed in the UK in order to boost fossil fuel production and help curb fuel price increases
> 
> [The Observer view on Ukraine and the climate emergency | Observer editorial |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency)

<!--quoteend-->

> which they say would be better addressed not by raising national insurance payments and imposing green levies but by cutting taxes, resuming UK shale gas extraction, and slowing down the rate at which we impose carbon emission cuts.
> 
> [The Observer view on Ukraine and the climate emergency | Observer editorial |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency)


## Capitalising on the cost of living crisis

> They have tried to blame the government’s green agenda for a cost-of-living crisis
> 
> [The Observer view on Ukraine and the climate emergency | Observer editorial |&#x2026;](https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency)

Compare: [[Net zero policies would reduce the cost of living]].

> as gas prices soared and concern about a growing [[cost of living crisis]] escalated, it appears Baker and Mackinlay sensed an opportunity.
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

<!--quoteend-->

> Halfon said: “Millions are now being hit desperately hard by the cost of living crisis with heating and fuel bills soaring. We cannot sacrifice any further their ability to cope on the altar of climate change.”
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 


## Who is it

[[Steve Baker]].  [[Mark Jenkinson]].

> But at the beginning of January, their efforts appeared to bear fruit when 19 Tories, including the former cabinet minister Esther McVey, the senior backbencher Robert Halfon and several red wall MPs, identified themselves as members of the group, signing a letter to the Sunday Telegraph arguing for an end to green levies and calling for more fossil fuel extraction.
> 
> &#x2013; [‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero &#x2026;](https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda) 

