# Soviet Union

Union of Soviet Socialist Republics. [[1922]] - [[1991]].

Comprised of [[Russia]], Estonia, Latvia, Lithuania, Belarus, Ukraine, Moldova, Georgia, Armenia, Azerbaijan, Kazakhstan, Uzbekistan, Turkmenistan, Kyrgyzstan, Tajikistan.

> But that system died at the end of [[1991]], replacing a world of [[shortages]] (full pockets, empty shops) with a world of poverty and [[hyperinflation]] (empty pockets, full shops) overnight
> 
> &#x2013; [Red Plenty by Francis Spufford | Fiction | The Guardian](https://www.theguardian.com/books/2010/aug/08/red-plenty-francis-spufford)

<!--quoteend-->

> We realised there were saboteurs and enemies among us, and we caught them, but it drove us mad for a while, and for a while we were seeing enemies and saboteurs everywhere, and hurting people who were brothers, sisters, good friends, honest comrades. Then the Fascists came, and stamping on them was bloody, nobody could call what we did then sweetness and light, wreckage everywhere, but what are you going to do when a gang of murderers breaks into the house? And the Boss didn’t help much. Wonderful clear mind, but by that time he was frankly screwy, moving whole nations round the map like chess pieces, making us sit up all night with him and drink that filthy vodka till we couldn’t see straight, and always watching us: no, I don’t deny we went wrong, in fact if you recall it was me that said so. But all the while we were building. All the while we were building factories and mines, railroads and roads, towns and cities, and all without any help, all without getting the say-so from any millionaire or bigshot. We did that. We taught people to read, we taught them to love culture. We sent tens of millions of them to school and millions of them to college, so they could have the advantages we never had. We created the boys and girls who’re young now. **We did the dirty work so they could inherit a clean world**.
> 
> &#x2013; [[Red Plenty]]

^ [[Khrushchev]]'s words. Reminds me a bit of [[To Posterity - Bertolt Brecht]].

> Some comrades seemed to think that fine words and fine ideas were all the world would ever require, that pure enthusiasm would carry humanity forward to happiness: well excuse me, comrades, but aren’t we supposed to be materialists? Aren’t we supposed to be the ones who get along without fairytales? If communism couldn’t give people a better life than capitalism, he personally couldn’t see the point
> 
> &#x2013; [[Red Plenty]]

[[Khrushchev]] again.

> Politics gave the orders, in the economy of the USSR, and economists were allowed to find reasons why the orders already given were admirable
> 
> &#x2013; [[Red Plenty]]

