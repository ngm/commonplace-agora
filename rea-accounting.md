# REA accounting

accounting for [[Resources, Events, Agents]]

I liked this quick intro: [Lynn Foster from Valueflo.ws - presentation at Open Climate Collabathon 2020 &#x2026;](https://www.youtube.com/watch?v=vymAHXGSM14)

> allows actors to see their transactions as part of an ecosystem of collaboration, which is ‘flow accounting’ rather than a vision based on the accumulation of assets in a single firm.
> 
> &#x2013; [[P2P Accounting for Planetary Survival]]

