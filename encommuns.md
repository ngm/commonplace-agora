# EnCommuns

> French city council offered to pay EnCommuns, a commons-based network of database programmers, to do some work, it suddenly introduced external performance pressures, trumping the project’s original goals and its self-determined work rhythms.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In response, EnCommuns created a [[semi-permeable membrane]], an ingenious way to preserve the spirit of its commoning while engaging with commercial actors.
> 
> &#x2013; [[Free, Fair and Alive]]

