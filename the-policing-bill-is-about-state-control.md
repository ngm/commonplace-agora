# The policing bill is about state control

The [[Policing bill]] is about [[state control]].

[The police bill is not about law and order – it’s about state control | Joshu&#x2026;](https://www.theguardian.com/commentisfree/2021/aug/09/police-bill-not-law-order-state-control-erosion-freedom)


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Concentrate and ask again]]

Seems like it, but this is a strong opinion that I personally need a bit more evidence for.

