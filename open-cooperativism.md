# Open cooperativism

> Beyond critiques of the Silicon Valley-style “sharing economy”, Open Cooperativism questions the dominance of capital in the free and open source software economy, and suggests [[P2P]]-empowered digital solutions in order to lower the transactional costs of networked cooperative production. As a corollary to issues of democratic ownership and governance raised by [[Platform Cooperativism]], Open Cooperativism asks a straight question: “What do we want to produce?”
> 
> &#x2013; [From Platform to Open Cooperativism - Commons Transition](http://commonstransition.org/from-platform-to-open-cooperativism/) 

