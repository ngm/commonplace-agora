# Leighton Moss

https://www.rspb.org.uk/reserves-and-events/reserves-a-z/leighton-moss/


## <span class="timestamp-wrapper"><span class="timestamp">[2022-11-20 Sun]</span></span>

We went today, and saw lots of ducks.  I learned about ‘dabbling’ ducks. Dabblers feed only at the surface – they don’t dive deeper. Makes sense!

My favourite dabblers so far are the Shovellers. Because:

a) they have comically large bills
b) they sound like a 17th century politcal movement

