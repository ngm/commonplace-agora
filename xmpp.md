# XMPP

> It’s worth noting that Facebook and Google both previously integrated a decentralized and interoperable protocol (XMPP) for their respective chat features, allowing users to communicate across services in a manner similar to email. But once Facebook’s market dominance was complete, it killed this feature in April 2014, forcing everyone on the service to use the Facebook messenger web interface or app.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

