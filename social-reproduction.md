# Social reproduction

> the reproduction of social structures and systems, mainly on the basis of particular preconditions in demographics, education and inheritance of material property or legal titles (as earlier with aristocracy). Reproduction is understood as the maintenance and continuation of existing social relations.
> 
> &#x2013; [Social reproduction - Wikipedia](https://en.m.wikipedia.org/wiki/Social_reproduction)

<!--quoteend-->

> Originally formulated by Karl Marx in Das Kapital, this concept is a variety of Marx's notion of [[economic reproduction]].
> 
> &#x2013; [Social reproduction - Wikipedia](https://en.m.wikipedia.org/wiki/Social_reproduction)

