# drought

> A drought occurs when an area experiences an unusually low level of rainfall or snow, affecting everything from moisture in the soil to water levels in rivers.
> 
> While droughts can be naturally occurring events, there is consensus among scientists that human-driven [[climate change]] is increasing the probability of drought events.
> 
> &#x2013; [UK drought: What caused the water shortage and what happens now? - The Big Issue](https://www.bigissue.com/news/environment/uk-drought-what-caused-water-shortage-what-can-i-do/)

<!--quoteend-->

> Droughts occur as a result of low rainfall, which can be driven by a variety of factors like climate change, ocean temperature and changes in a local landscape.
> 
> In the UK, the extremely hot July that just passed and an unusually dry spring has contributed to the likely onset of drought.
> 
> Another heatwave this week has further exacerbated the situation. 
> 
> The way that water is managed and used can also have an impact on an area’s ability to cope with drought conditions. 
> 
> Water companies in England have been heavily criticised for failing to meet targets on reducing leakage in its infrastructure.
> 
> &#x2013; [UK drought: What caused the water shortage and what happens now? - The Big Issue](https://www.bigissue.com/news/environment/uk-drought-what-caused-water-shortage-what-can-i-do/)

<!--quoteend-->

> Analysis by The Times shows that water companies are leaking up to a quarter of their supply a day, amounting to around 2.4 billion litres of water.
> 
> &#x2013; [UK drought: What caused the water shortage and what happens now? - The Big Issue](https://www.bigissue.com/news/environment/uk-drought-what-caused-water-shortage-what-can-i-do/)

