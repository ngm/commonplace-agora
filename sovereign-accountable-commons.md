# Sovereign Accountable Commons

> we often see DAOs fall short with their aversion to the messy parts of human organizing and community building.
> 
> &#x2013; [DAOs: Why Sovereign Accountable Commons Might Be Better - Holochain Blog](https://blog.holochain.org/daos-why-sovereign-accountable-commons-might-be-better/)

