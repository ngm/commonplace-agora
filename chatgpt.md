# ChatGPT

> For inference (i.e., conversation with ChatGPT), our estimate shows that ChatGPT needs a 500-ml bottle of water for a short conversation of roughly 20 to 50 questions and answers, depending on when and where the model is deployed. Given ChatGPT’s huge user base, the total water footprint for inference can be enormous.
> 
> &#x2013; [The Secret Water Footprint of AI Technology – The Markup](https://themarkup.org/hello-world/2023/04/15/the-secret-water-footprint-of-ai-technology)

