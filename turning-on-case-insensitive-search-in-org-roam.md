# Turning on case-insensitive search in org-roam

Not sure if this has happened just recently, or I've been living with it for a long time, but: case sensitive node search in [[org-roam]] was starting to bug me.

It's actually not anything specific to org-roam - it comes from the completion engine.


## Spacemacs

Turns out that it's [[Helm]] that is doing it, when set to the 'smart' completion algorithm.

I just need to `(setq helm-case-fold-search t)` and all is good - much smoother completion experience.

See: https://org-roam.discourse.group/t/v2-case-insensitive-completion-and-backlinks-not-working-as-expected/1701/17


## Doom

In Doom, out of the box the completion engine is vertico.

```elisp
(after! org-roam
  (setq read-file-name-completion-ignore-case t
	read-buffer-completion-ignore-case t
	completion-ignore-case t))
```

See https://github.com/minad/vertico/issues/356

