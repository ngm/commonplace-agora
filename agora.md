# Agora

-   https://anagora.org/

This looks like a really cool way of aggregating digital gardens into one place.  To produce a community garden (or, an agora).  Interesting to contrast with how a solely P2P way of connecting gardens might work, no central aggregator.  

> An Agora is a distributed, goal-oriented social network centered around a cooperatively built and maintained [[knowledge graph]]. The implementation you are currently looking at tries to assemble such a graph out of a collection of digital gardens.
> 
> &#x2013;  [GitHub - flancian/agora](https://github.com/flancian/agora)

See also [[sister sites]].

See: [[What do I think about the Agora?]]

