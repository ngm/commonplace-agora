# Architecture of participation

> systems that are designed for user participation.
> 
> &#x2013; [[The Architecture of Participation]]

<!--quoteend-->

> [[social media]] platforms – which have been described as 'architectures of participation' 
> 
> &#x2013; [[Anarchist Cybernetics]]


## Resources

-   [HOWTO: Create an Architecture of Participation for your Open Source project |&#x2026;](https://blog.weareopen.coop/howto-create-an-architecture-of-participation-for-your-open-source-project-a38386c69fa5)

