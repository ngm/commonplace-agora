# The role of technology in eco-socialism

A [[review article]].  I plan to give a critical overview of existing work on what ecosocialism is, why it is needed, what the various strands are.  Then: what are its programmes and what are the existing currents for a transition to it, and how could ICT support both of these.


## Outline

![[project-wbs.png]]


## Intro

What is the role of information and communications technology (ICT) in supporting a transition to ecosocialism?

Both the climate crisis and global inequality are pressing concerns of our time (, ). Ecosocialism is a merging of socialist and environmental politics that strives for equity of resources and a fair standard of living for all, all while staying within planetary boundaries.

ICT underpins much of the infrastructure and operation of modern society.  As a sector of industry, it has a significant environmental impact of its own; it also supports other industries; and it has an ever-increasing influence on how society communicates and is organised.

The way in which society uses ICT will undoubtedly play a role in how we transition to a more sustainable and equal world. For example, during the global protests of 2011 (the Arab Spring, Occupy, Movement of the Squares, etc) much was made of the use of social media to both agitate and organise ("Twitter revolutions", etc).  These protests ultimately failed to change the dominant capitalist paradigm under which the majority of the world operates.  What role could ICT play to transition to an ecosocialist future?  How can ICT workers act to bring this transition about?


## Background


### Eco-socialism

To give context and a point of reference, you can situate ecosocialism within a wider space of the sustainable development debate.  Within this space are a number of different tendencies with varying levels of concerns regarding the environment and socio-economic wellbeing and equality. These trends fall in to different bands of 'Status Quo', 'Reform', and 'Transition'.  Those which are most eco-centred and most desiring of socio-economic well-being and equality, are [[ecofeminism]], [[ecosocialism]], [[social ecology]] and indigenous/'south' movements.  They all fall into the 'Transformation' band. [[[Sustainable development: mapping different approaches]]]


#### Socialism


#### Environmentalism


#### Climate crisis

-   [[climate crisis]]
-   [[Biodiversity loss]]
-   [[sixth extinction]]


#### Overview of different strands of ecosocialism

-   e.g. [[Half-Earth Socialism]]
-   [[Climate Change as Class War: Building Socialism on a Warming Planet]]
-   [[Climate Leviathan]]
-   [[Social ecology]]
-   [[Degrowth]]
-   FALC
-   [[Andreas Malm]]
-   [[Biocommunism]]
-   [[Climate Leninism and Revolutionary Transition]] gives a bit of an overview
-   etc

In terms of their ideas on:

-   transition
    -   strategy and tactics
-   future
    -   programme
    -   governance structures


#### Resources

-   [[Red-Green Revolution]]
-   on ecosocialism and technology
-   has strand of what role technology plays throughout it.  (with ICT mentioned explicity a few times)
-   to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism


### Information and communication technology and society


#### Technology and society in general

-   [[ICT is a fundamental part of modern society]]
-   General stance that [[technology alone will not create a better world]].  Against [[technological determinism]]. [[We need to be thinking in terms of systems rather than technological quick fixes]].
-   Draw on some of the STS stuff in [[Cybernetic Revolutionaries]]


#### ICT definition


#### Capitalism and ICT


##### Concepts

-   [[The Stack]]


##### Claims


##### Resources

-   [[The Stack as an Integrative Model of Global Capitalism]]


## Eco-socialism and ICT


### Socialism and ICT


#### Concepts

-   [[Liberatory technology]]
-   [[Technology Networks]]
-   [[Project Cybersyn]]
-   [[Governable stacks]]
-   [[Convivial tools]]
-   [[Telecommunism]]
-   [[Libre software]]
-   [[Digital socialism]]
-   [[Digital Tech Deal]]


#### Possible claims

-   [[Modern information technology is one of the main conditions for socialist revolution]]
-   A socialist ICT will depend on governable stacks
    -   Libre software facilitates governable stacks
        -   "Governable stacks should prioritise community accountability alongside individual freedom" ([[Governable Stacks against Digital Colonialism]])


#### Related

-   [[How not to deal with big tech]]


#### Resources

-   [[Information Technology and Socialist Construction]]
-   [[Technology of the Oppressed]]
-   [[Towards a Liberatory Technology]]
-   [[Tools for Conviviality]]
-   [[Anarchist Cybernetics]]
-   [[Governable Stacks against Digital Colonialism]]
-   [[Technology Networks for Socially Useful Production]]
-   [[The Telekommunist Manifesto]]
-   [[Cybernetic Revolutionaries]]
    -   **Relation to question:** ICT in practice for facilitating socialism.  The book also reflects on the STS questions of how technology relates to society.
    -   **How to use it:** Conclusion is actually good place to start.
-   [[Breaking Things at Work]]
    -   **Overview:** Luddism over time and its relationship to Marxism.
    -   **Relation to question:** explores relationship between technology and capitalism, and how technology might be used in socialism.
    -   **How to use it:** pick out anything that suggests how we might use technology for anti-capitalist / pro-socialist purposes.

-   [[Governable Stacks against Digital Colonialism]]
    -   **Overview:** Digital self-governance as a push back against digital colonialism.
    -   **Relation to question:** Identifies governance of 'the stack' - a key part of ICT - as crucial in anti-colonial (and hence anti-capital, hence socialist) struggle
    -   **How to use it:** see how ICT workers may use/build governable stacks

-   [[Capital is Dead]]
    -   **Overview:** Expansion and update of ideas from [[A Hacker Manifesto]] e.g. [[Vectoralist class]] and [[Hacker class]].
    -   **Relation to question:** [[Hacker class]] is a class representation of ICT/knowledge workers?
    -   **How to use it:** see if it gives useful outline of how ICT workers can be part of the struggle, any suggestions of [[Praxis for the hacker class]].


### Environmentalism and ICT


#### Concepts

-   [[right to repair]]


#### Claims

-   [[Extending the life of electronic devices helps to addresses the e-waste problem]].
-   [[It is possible to create cutting-edge systems using technologies that are not state-of-the-art]].
-   


#### Resources

-   [[Doing the Doughnut.tech]]
-   [[Server Infrastructure for Global Rebellion]]


### Ecosocialism and ICT

What do the existing eco-socialist tendencies say about technology and specifically [[information and communication technology]] ([[big tech]] etc)


#### Various prongs

The material impacts prong has more coverage at present.  I want to focus more on the political/organisational impacts.


##### Material impacts

-   The Materialist Circuits and the Quest for Environmental Justice in ICT’s Global Expansion
-   [Report: Fog of Enactment - The Green Web Foundation](https://www.thegreenwebfoundation.org/publications/report-fog-of-enactment/)


##### Control impacts, surveillance, hegemony


##### Organisational impacts

-   how can we use it for organisation and transition?
-   how does it get co-opted (e.g. cambridge analytica, trump, brexit, etc)


#### Concepts

-   [[Ecosocialist technology]]
-   [[The Lucas Plan]], [[Just transition]], [[Green New Deal]],
-   [[Governable stacks]]
-   perhaps also looking at how technology might be used in some of the climate leviathan quadrants, as counterpoints


#### Claims


#### Resources

-   [[Digital Ecosocialism: Breaking the power of Big Tech]]

-   [[Technology and Ecosocialism]]
    -   **Overview:** comparison of ecosocialist and capitalist technology, sectoral overview of ecosocialist technologies.
    -   **Relation to question:** has strand of what role technology plays throughout it.
    -   **How to use it:** to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism
    -   **Relation to question:** seems very relevant.

-   [[Red-Green Revolution]]
    -   **Overview:** on ecosocialism and technology
    -   **Relation to question:** has strand of what role technology plays throughout it.  (with ICT mentioned explicity a few times)
    -   **How to use it:** to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism
    -   **Relation to question:** seems very relevant.


## Points to intervene

With regards to systems thinking.  What are the points to intervene in a system with regards to a transition to eco-socialism - earth system, political systems, economic systems?  Through the lens of technology, presumably.

-   [[We need to be thinking in terms of systems rather than technological quick fixes]]


## Misc

-   Network technology (social media) was discussed heavily as part of the 2011 uprisings, though all of those ultimately did not bring a socialist outcome long-term. Another political approach needed, maybe another technology tool too?
-   Perhaps self-governance is a part of that. c.f. [[Governable stacks]].  Take back the stacks.
-   "These instructions assume you are using Microsoft Word, but other word-processing software should work in a similar way."  we are pushed down a certain path.
-   [[Solarpunk]] maybe


### Unsorted

-   technology is a site of struggle
-   focus on digital technology / ICT
-   [[digital activism]] will be part of a transition presumably
    -   [[From Cyber-Autonomism to Cyber-Populism: An Ideological Analysis of the Evolution of Digital Activism]]
-   [[Communicative capitalism]]
-   [[Vectoralism]]
-   Look at  existing overlaps of ICT and socialism/communism. Existing mergings of ICT and environmentalism. Mergings of socialism and environmental.
-   Matrix of different uses of ICT - e.g. in protest, in economic planning, in organisation, etc. Other can be  how governable, how libre, etc.
-   Look at 3IR and 4IR for ideas of tech to look at - maybe via Coop Jackson
    -   but critique Scwab's framing
        -   see e.g. "The Impact of 4IR Digital Technologies and Circular Thinking on the United Nations Sustainable Development Goals"
-   strongly critique much of the SDGs and technology papers
    -   e.g. Unleashing the convergence amid digitalization and sustainability towards pursuing the Sustainable Development Goals (SDGs): A holistic review
    -   [[Radical Technologies]] provides good general critique of the type of technologies referenced
    -   ICT and Sustainability: Looking Beyond the Anthropocene looks very good.


### Learning plan and log


#### Plan


#### Log

-   [[The Stack as an Integrative Model of Global Capitalism]]
    -   2022-11-06
    -   2h
    -   3
    -   Useful to learn about the idea of the Stack, i.e. big tech platforms and their control over whole vertical sections of ICT.  Additionally, provides some commons-based options as ways to intervene.

-   [[First and Third World Ecosocialisms]]
    -   **Date read:** 2022-11-11
    -   **Time spent:** 1h 15m
    -   **Level:** 2
    -   **Reflection:** Useful for further understanding of the debate on different aspects of ecosocialism.  Peripheral mention of technology in the abstract, but not specifically much to do with ICT (one mention of repair, though)

-   [[The Cybersyn Revolution]]


## Writing time log

