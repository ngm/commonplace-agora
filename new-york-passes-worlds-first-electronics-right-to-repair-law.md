# New York Passes World's First Electronics Right to Repair Law

URL
: https://www.ifixit.com/News/60893/new-york-passes-worlds-first-electronics-right-to-repair-law

It's a big win for the right to repair.

It will now go to the state’s Governor, who is expected to sign it into law. 

This seems very positive:

> The New York law includes provisions for resetting the software locks that some manufacturers use to tie parts to the device’s motherboard or serial number. Manufacturers will have to find some way to make parts pairing reset tools available to the public.

Re: [[Part pairing]]. I wonder if that means your parts won’t need to be paired via the manufacturer at all? i.e. you won’t be tied to using official parts. Or you still will, but you can pair it yourself, rather than only authorised repairers being able to.  The former would be the better of course.

