# North Walney Nature Reserve

A nature reserve on [[Walney Island]].


## A pool in the reserve

A tiny bit of the Irish Sea in the background.

![[photos/north-walney-nature-reserve-pool.jpg]]


## Black Combe from the reserve

![[photos/north-walney-nature-reserve-black-combe.jpg]]

