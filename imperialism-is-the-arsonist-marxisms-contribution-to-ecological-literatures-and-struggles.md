# Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles

An
: [[article]]

URL
: http://www.abstraktdergi.net/imperialism-is-the-arsonist-marxisms-contribution-to-ecological-literatures-and-struggles/

Author
: [[Derek Wall]]

The title is a reference to [[Imperialism is the Arsonist of our Fires and Savannas]].

Brief overviews of [[green politics]], [[Marxism and ecology]], [[eco-socialism]].  Suggests that [[Eco-Leninism]] is important.

-   Overview of [[green politics]].  Sometimes it has started radical left and shifted rightwards (e.g. the [[German Greens]]).  Sometimes the other way around, e.g. the UK Greens.
-   Gives a brief overview of [[Marxism and ecology]].  There is an argument that Marxism is Promethean, industrial, and dominating of nature.  There is a counter-argument that Marx and Engels clearly took the care and stewardship of nature into account.
-   Currently existing eco-socialism: [[Hugo Blanco]].  Cuba as an exemplar - [[Green Cuba]].
-   Important factors:
    -   The [[Commons]].  As something beyond both market and state.
    -   [[Anti-imperialism]].
    -   [[Working class and green politics]], workers' plans (like e.g. [[The Lucas Plan]]?)
-   [[Eco-Leninism]] as a recent and for Derek very important strand. Specifically the contribution on how to make revolution happen in a specific context.

