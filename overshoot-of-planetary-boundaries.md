# overshoot of planetary boundaries

Overshoot of [[planetary boundaries]].

> The [[chemical pollution]] planetary boundary is the fifth of nine that scientists say have been crossed, with the others being [[global heating]], the [[destruction of wild habitats]], [[loss of biodiversity]] and excessive [[nitrogen and phosphorus pollution]].
> 
> &#x2013; [Chemical pollution has passed safe limit for humanity, say scientists | Pollu&#x2026;](https://www.theguardian.com/environment/2022/jan/18/chemical-pollution-has-passed-safe-limit-for-humanity-say-scientists?s=09)

