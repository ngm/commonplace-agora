# Wide data

> Wide Data sets are much smaller than Big Data sets, making them easier to manage, but the key defining characteristic is that wide data is made up of data that comes from many disparate and unconnected data sources.
> 
> The move to Wide Data is only just beginning, but technology research firm Gartner predicts that 70% of organisations will shift their focus from Big to Small and Wide Data by 2025.
> 
> https://charitydigital.org.uk/topics/topics/what-is-wide-data-10127

I mean taking data from a wide variety of sources makes sense, but labelling it as 'Wide Data' (capitalised&#x2026;) sounds like a bit of a marketing thing.
I don't think many charities (especially not small charities) ever worked with [[Big Data]] either, not the scale that 'Big Data' really is.

