# Free association of producers

> a relationship among individuals where there is no state, social class, hierarchy, or private ownership of means of production. 
> 
> &#x2013; [Free association of producers - Wikipedia](https://en.wikipedia.org/wiki/Free_association_of_producers) 

<!--quoteend-->

> individuals are no longer deprived of access to [[means of production]], thus enabling them to freely associate without social constraint to produce and reproduce their own conditions of existence and fulfill their individual and creative needs and desires.
> 
> &#x2013; [Free association of producers - Wikipedia](https://en.wikipedia.org/wiki/Free_association_of_producers) 

Seizing the means of production, basically?

> Communist or “associated” production is planned and carried out by the producers and communities themselves, without the class-based intermediaries of wage-labor, market, and state
> 
> &#x2013; [[Marx's Vision of Sustainable Human Development]]

