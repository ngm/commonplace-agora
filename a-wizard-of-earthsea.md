# A Wizard of Earthsea

A
: [[book]]

Author
: [[Ursula K. Le Guin]]

About a young wizard, learning magic and learning lessons around the use of magic.

Has a strong "don't mess with nature" subtext for me.  From what I understand, that might be rooted more in Taoism, balance, equilibrium, etc.  But that applies pretty well to nature.


## Map of Earthsea

![[images/earthsea-map.jpg]]


## Themes


### Balance, equilibrium, humility

The opening passage is gorgeous:

<div class="verse">

Only in silence the word,<br />
Only in dark the light,<br />
Only in dying life:<br />
Bright the hawk's flight<br />
On the empty sky<br />
<br />
&#x2013; The Creation of Éa<br />

</div>

> the lessons of Jennerite socialism might be best expressed in her novel A Wizard of Earthsea. Like Morris’ [[News from Nowhere]], it describes a society predicated on a humble and incomplete domination of nature. Everything in Earthsea is divided between surface and essence, a boundary that only magic can overcome
> 
> &#x2013; [[Half-Earth Socialism]]

<!--quoteend-->

> Ogion let the rain fall where it would

Ogion, though powerful, doesn't try to dominate nature.

> But you must not change one thing, one pebble, one grain of sand, until you know what good and evil will follow on that act. The world is in balance, in Equilibrium. A wizard’s power of Changing and of Summoning can shake the balance of the world. It is dangerous, that power. It is most perilous. It must follow knowledge, and serve need. To light a candle is to cast a shadow . .

<!--quoteend-->

> The weatherworker’s and seamaster’s calling upon wind and water were crafts already known to his pupils, but it was he who showed them why the true wizard uses such spells only at need, since to summon up such earthly forces is to change the earth of which they are a part. “Rain on Roke may be drouth in Osskil,” he said, “and a calm in the East Reach may be storm and ruin in the West, unless you know what you are about

<!--quoteend-->

> From that time forth he believed that the wise man is one who never sets himself apart from other living things, whether they have speech or not, and in later years he strove long to learn what can be learned, in silence, from the eyes of animals, the flight of birds, the great slow gestures of trees

The point that Ged finally gets it.


### Power and domination and the consequences

> Duny laughed and shouted it out again, the rhyme that gave him power over the goats. They came closer, crowding and pushing round him. All at once he felt afraid of their thick, ridged horns and their strange eyes and their strange silence

First hint of power over nature going a bit too far&#x2026;

> what was the good of having power if you were too wise to use it

Ged is impatient&#x2026;

> Press a mage for his secrets and he would always talk, like Ogion, about balance, and danger, and the dark. But surely a wizard, one who had gone past these childish tricks of illusion to the true arts of Summoning and Change, was powerful enough to do what he pleased, and balance the world as seemed best to him, and drive back darkness with his own light

<!--quoteend-->

> Like [[Frankenstein]], hubris drives Ged to try to resurrect the dead. Because he only partially understands the spell, a ‘shadow’ leaps through the opening created by the spell and attacks him. Our shadows are zoonoses, climate change, the hole in the ozone layer, and unknown future horrors as [[the humanization of nature]] hurtles blindly forward
> 
> &#x2013; [[Half-Earth Socialism]]


## Science, knowledge, understanding

> When you know the fourfoil in all its seasons root and leaf and flower, by sight and scent and seed, then you may learn its true name, knowing its being: which is more than its use

Not entirely sure what the true name stuff means at present.  It seems to partly come from study and knowledge.  But I think also it comes from another kind of understanding, something a bit less rational.

> In that moment Ged understood the singing of the bird, and the language of the water falling in the basin of the fountain, and the shape of the clouds, and the beginning and end of the wind that stirred the leaves: it seemed to him that he himself was a word spoken by the sunlight

<!--quoteend-->

> So if some Mage-Seamaster were mad enough to try to lay a spell of storm or calm over all the ocean, his spell must say not only that word inien, but the name of every stretch and bit and part of the sea through all the Archipelago and all the Outer Reaches and beyond to where names cease. Thus, that which gives us the power to work magic sets the limits of that power

<!--quoteend-->

> mage can control only what is near him, what he can name exactly and wholly. And this is well. If it were not so, the wickedness of the powerful or the folly of the wise would long ago have sought to change what cannot be changed, and Equilibrium would fail. The unbalanced sea would overwhelm the islands where we perilously dwell, and in the old silence all voices and all names would be lost.

