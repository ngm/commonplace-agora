# Debating Eco-Socialist Futures

URL
: https://www.youtube.com/watch?v=9MNwY_6X1ZI

> Join Haymarket and Verso for a discussion on left climate strategy that assesses where we are and what we should be fighting for.
> 
> ***
> 
> What are the most useful frameworks to help the Left to organize our climate justice movements? What demands should we prioritize, and what strategies can we borrow from history and from other social movements? How can utopian thinking expand our horizons in what must be a massive fight for a more sustainable future?
> 
> Centering class struggle, transitioning from fossil fuels to renewable energy, anti-capitalist economic alternatives like degrowth and socialist planning: can all of these ideas (and more!) be woven into a clear message and a blueprint for change?
> 
> Join a panel of environmental thinkers to discuss left climate strategy and to assess where we are and what could be possible.
> 
> A conversation with [[Drew Pendergrass]], co-author of [[Half-Earth Socialism]]: A Plan to Save the Future from Extinction, Climate Change and Pandemics, [[Matthew Huber]], author of [[Climate Change as Class War: Building Socialism on a Warming Planet]], Andrea Vetter, co-author of The Future Is Degrowth: A Guide to a World Beyond Capitalism, and Olúfẹ́mi O. Táíwò, author of Reconsidering Reparations and Elite Capture. Moderated by Thea Riofrancos.

