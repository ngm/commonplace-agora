# Combination Laws

> The new Combination Laws, which outlawed [[trade union]] activity, severely limited collective action by textile weavers.
> 
> &#x2013; [[Breaking Things at Work]]

