# Using ICTs to support ecosocialist governance

Governance Process	Supporting ICT
Participatory planning	Collaborative planning and decision-making tools, such as online forums, wikis, and voting systems
Democratic control of resources	Database and tracking systems, such as blockchain and distributed ledger technologies, to support the transparent and accountable management of resources
Collective ownership and management of the means of production	Collaborative work management and coordination tools, such as project management software, to support the collective ownership and management of the means of production
Decentralization and regional self-sufficiency	Geographic information systems (GIS) and mapping tools, to support the development of decentralized and self-sufficient regional economies
Environmental and social justice	Data analysis and visualization tools, to support the assessment of the environmental and social impacts of different policies and practices

