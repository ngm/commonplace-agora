# Insulate Britain

Protest group spun out of [[Extinction Rebellion]], protesting about the paucity of [[insulation]] of Britain's housing stock.


## Articles

-   [The Guardian view on Insulate Britain: the art of protest | Editorial | The G&#x2026;](https://www.theguardian.com/commentisfree/2021/oct/05/the-guardian-view-on-insulate-britain-the-art-of-protest)

