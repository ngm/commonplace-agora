# The right to repair movement is an example of neo-Luddism

A
: [[claim]]

The [[right to repair]] movement is an example of [[neo-Luddism]].

First heard [[Hotel Bar Sessions: Breaking Things at Work (with Gavin Mueller)]].

