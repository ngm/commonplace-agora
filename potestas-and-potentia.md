# Potestas and potentia

> Apart from exceptional situations, the powerful always have what [[Baruch Spinoza]] called potestas to ensure that, when push comes to shove, people will do their bidding: the police, the army, the press, the wage relation, the accumulated fear and passive consent of the majority, all sorts of things that could be described as ‘power over’ or grouped under the vague expression the powers that be. The weak, on the other hand, have nothing but their capacity to act – their power to do things, to affect and be affected by each other, which Spinoza called potentia
> 
> &#x2013; [[Neither Vertical Nor Horizontal]]

